@echo off
rem
rem  q.bat -- quick actions for HOL
rem
rem for version.bat
set PACKING=q.bat std.prelude *.hol *.java Shell.*

if "%1"=="" goto help
if "%1"=="h" goto h%2
goto %1

-----------------------------------------------------------------------
HOL (Higher Order Logic)
http://hol.sourceforge.net/
-----------------------------------------------------------------------

:help
echo Usage: q build            build HOL Launcher
echo        q run              run HOL Launcher
echo        q doc              goto HOL Documentation
echo        q h build          build HOL
echo        q h run            runs HOL interactively
echo        q h run [script]   runs HOL with [script]
echo        q clean            restores to original
echo.
goto end

:build
echo Building ShellServer application ...
javac *.java
echo Main-Class: ShellServer> manifest
jar -cvfm ShellServer.jar manifest *.class
del manifest
del *.class
dir ShellServer.jar | findstr "ShellServer.jar"
goto end

:run
echo Running ShellServer application ...
setlocal
set MOSMLLIB=%MOSMLHOME%\lib
set PATH=%MOSMLHOME%\bin;%JAVA_HOME%\bin
set TERM=
rem Note: although start /b allows Textpad to capture the logs (from stderr)
rem       it is difficult to stop the server loop (which is interrupted by ^C)
rem       Textpad sends only 'ctrl-break', does not work.
rem This starts HOL
start "ShellServer" java -jar ShellServer.jar %HOL_HOME%\bin\hol.bat
rem start "ShellServer" java -Dport=8080 -jar ShellServer.jar %HOL_HOME%\bin\hol.bat
rem This starts command prompt
rem start "ShellServer" java -jar ShellServer.jar
endlocal
goto end

:doc
echo HOL4 Documentation ...
call go.bat q ds %HOL_HOME%\doc
goto end

:sml -- Run SML in browser
echo Run MOSML from Shell server (client at http://localhost/Shell.html) ...
setlocal
set MOSMLLIB=%MOSMLHOME%\lib
set PATH=%MOSMLHOME%\bin;%JAVA_HOME%\bin
start "MOSML Server" java -jar ShellServer.jar mosml
endlocal
goto end

:hbuild
echo Building HOL from ML source ...
setlocal
echo Step 1: Empty [bin] folder (not remove)
if exist bin rd /s/q bin
mkdir bin
dir bin
echo Step 2: Smart Configuration
set MOSMLLIB=%MOSMLHOME%\lib
set PATH=%PATH%;%MOSMLHOME%\bin;
mosml < tools\smart-configure.sml
rem Moscow ML changes DOS settings, pipe will not work properly.
echo Step 3: Show [bin] directory
rem this puts 4 HOL executables (.bat) in [bin] directory
dir bin
endlocal
goto end

:test
setlocal
set MOSMLLIB=%MOSMLHOME%\lib
set PATH=%PATH%;%MOSMLHOME%\bin;
rem "C:\jc\www\ml\hol\bin\unquote" -P
rem "C:\jc\www\ml\hol\bin\unquote" -P
"C:\jc\www\ml\hol\bin\unquote"
endlocal
goto end

=================================================================
DOS> q test
Usage:
  C:\jc\www\ml\hol\bin\unquote.exe [<inputfile> <outputfile>]

DOS> q test
abc `abc`
abc [QUOTE " (*#loc 1 7*)abc"]
``...``
(Parse.Term [QUOTE " (*#loc 2 3*)..."])
^Z
=================================================================

:hrun
if "%3"=="" goto hruni
:hruns -- Run HOL with script
setlocal
set MOSMLLIB=%MOSMLHOME%\lib
set PATH=%PATH%;%MOSMLHOME%\bin;
bin\hol.bat < %2
endlocal
goto end

:hruni -- Run HOL interactively
echo Running HOL (exit with CTRL-Z enter) ...
setlocal
set MOSMLLIB=%MOSMLHOME%\lib
set PATH=%PATH%;%MOSMLHOME%\bin;
start "HOL" bin\hol.bat
endlocal
goto end

:
:clean
if exist bin rd /q/s bin
echo Done.
goto end

:end
