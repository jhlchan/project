(* Tutorial Chapter 4: Euclid's Theorem *)
(* also in examples/euclid.sml *)

(*===========================================================================*)
(* Euclid's theorem: for every prime, there is another one that is larger.   *)
(* This proof has been excerpted and adapted from John Harrison's proof of   *)
(* a special case (n=4) of Fermat's Last Theorem.                            *)
(*                                                                           *)
(*===========================================================================*)

(* Make all ML bindings from HOL arithmetic theory available at top level *)
open arithmeticTheory;
(*PAUSE*)

(* --- Formalization --- *)

(* In order to define the concept of prime numbers, first need to define the divisibility relation: *)
val divides_def =
 Define
  `divides a b = ?x. b = a * x`;

(* treat <i>divides</i> as a (non-associating) infix: *)
set_fixity "divides" (Infix(NONASSOC, 450));

(* Next define the property of a number being <i>prime</i>: *)
val prime_def =
 Define
   `prime p = ~(p=1) /\ !x. x divides p ==> (x=1) \/ (x=p)`;

(* These are all the definitions to be made. Now we <i>just</i> have to prove that there are infinitely many prime nubmers. *)
(*PAUSE*)

(* In this example, we often <i>find</i> proofs by using the rewriter RW_TAC to unwind definitions and perform basic simplications, often reducing a goal to its essence. *)

RW_TAC;

(* --- Divisibility --- *)

(* Start by proving a number of theorems about the <i>divides</i> relation. *)
g `!x. x divides 0`;

(* expand the definition of <i>divides</i> and note the &alpha;-converion. *)
e (RW_TAC arith_ss [divides_def]);

(* It is of course quite easy to instantiate the existential quantifier *)
e (EXISTS_TAC ``0``);

(* Then a simplication step finishes the proof.*)
e (RW_TAC arith_ss []);
(*PAUSE*)

(* If the three interactions are joined together with THEN to form a single tactic, we can try the proof again from the beginning (using the <i>restart</i> function) and this time it will take just one step: *)
restart();

e (RW_TAC arith_ss [divides_def]
   THEN EXISTS_TAC ``0``
   THEN RW_TAC arith_ss []);
(*PAUSE*)

(* This is one way to prove the theorem. Another one: let METIS_TAC expand the definition of <i>divides</i> and find the required instantiation for x' fromthe theorem MULT_CLAUSES. *)
restart();

e (METIS_TAC [divides_def, MULT_CLAUSES]);
(*PAUSE*)

(* In any case, having done our proof inside the goalstack package, we now want to have access to the theorem value that we have proved. We use the <i>top_thm</i> function to do this, and use <i>drop</i> to dispose of the stack: *)
val DIVIDES_0 = top_thm();

drop();
(*PAUSE*)

(*---------------------------------------------------------------------------*)
(* A sequence of basic theorems about the "divides" relation.                *)
(*---------------------------------------------------------------------------*)

val DIVIDES_0 = store_thm
 ("DIVIDES_0",
  ``!x. x divides 0``,
  METIS_TAC [divides_def,MULT_CLAUSES]);

val DIVIDES_ZERO = store_thm
 ("DIVIDES_ZERO",
  ``!x. 0 divides x = (x = 0)``,
  METIS_TAC [divides_def,MULT_CLAUSES]);

val DIVIDES_ONE = store_thm
 ("DIVIDES_ONE",
  ``!x. x divides 1 = (x = 1)``,
  METIS_TAC [divides_def,MULT_CLAUSES,MULT_EQ_1]);

val DIVIDES_REFL = store_thm
 ("DIVIDES_REFL",
  ``!x. x divides x``,
  METIS_TAC [divides_def,MULT_CLAUSES]);

val DIVIDES_TRANS = store_thm
 ("DIVIDES_TRANS",
  ``!a b c. a divides b /\ b divides c ==> a divides c``,
  METIS_TAC [divides_def,MULT_ASSOC]);

val DIVIDES_ADD = store_thm
("DIVIDES_ADD",
 ``!d a b. d divides a /\ d divides b ==> d divides (a + b)``,
 METIS_TAC[divides_def,LEFT_ADD_DISTRIB]);

val DIVIDES_SUB = store_thm
 ("DIVIDES_SUB",
  ``!d a b. d divides a /\ d divides b ==> d divides (a - b)``,
  METIS_TAC [divides_def,LEFT_SUB_DISTRIB]);

val DIVIDES_ADDL = store_thm
 ("DIVIDES_ADDL",
  ``!d a b. d divides a /\ d divides (a + b) ==> d divides b``,
  METIS_TAC [ADD_SUB,ADD_SYM,DIVIDES_SUB]);

val DIVIDES_LMUL = store_thm
 ("DIVIDES_LMUL",
  ``!d a x. d divides a ==> d divides (x * a)``,
  METIS_TAC [divides_def,MULT_ASSOC,MULT_SYM]);

val DIVIDES_RMUL = store_thm
 ("DIVIDES_RMUL",
  ``!d a x. d divides a ==> d divides (a * x)``,
  METIS_TAC [MULT_SYM,DIVIDES_LMUL]);
(*PAUSE*)

(* This is a lemma about divisiblity that doesn't succumb to a single invocation of METIS_TAC. Let's see how this is proved: *)
g `!m n. m divides n ==> m <= n \/ (n = 0)`;

(* The easiest way to start is to simplify the definition of <i>divides</i>: *)
e (RW_TAC arith_ss [divides_def]);

(* With this goal, bascially 3 choices:
<ol>
<li>find a collection of lemmas that together imply the goal and use METIS_TAC</li>
<li>do a case split on <i>m</i></li>
<li>do a case split on <i>x</i></li>
</ol>

(1) doesn't fit the 'shape' of any pre-proved theorems. (2) will be rejected inthe end, let's try it anyway.
*)
e (Cases_on `m`);

(* the first subgoal (the last one printed) is trivial: *)
e (RW_TAC arith_ss []);

(* Let's try RW_TAC again *)
e (RW_TAC arith_ss []);
(* Now this works in HOL! *)
(*PAUSE*)

val DIVIDES_LE = top_thm();
drop();

(* Rather than use <i>top_thm</i> and <i>drop</i> the goalstack, we can bypass it and use the <i>store_thm</i> function: *)
val DIVIDES_LE = store_thm
 ("DIVIDES_LE",
  ``!m n. m divides n ==> m <= n \/ (n = 0)``,
  RW_TAC arith_ss  [divides_def]
     THEN Cases_on `x`
     THEN RW_TAC arith_ss  [MULT_CLAUSES]);
(*PAUSE*)

(* --- Divisibility and factorial --- *)

(* Factorial is found at arithmeticTheory.FACT: *)
FACT;

(* Lemma DIVIDES_FACT: every number x such that 0 < x <= n divides the factorial of n. *)
g `!m n. 0 < m /\ m <= n ==> m divides (FACT n)`;

(* Instead of directly inducting on n-m, we will induct on a witness variable obtained by the use of theorem LESS_EQ_EXISTS. *)
LESS_EQ_EXISTS;

e (RW_TAC arith_ss [LESS_EQ_EXISTS]);

(* Now we induct on p: *)
e (Induct_on `p`);

(* The first goal (last written) can obviously be simplified: *)
e (RW_TAC arith_ss []);

(* Now we can do a case analysis on m: if it is 0, we have a trivial goal. If it has a successor, then we can use the definition of FACT and the theorem DIVIDES_RMUL and DIVIDES_REFL. *)

e (Cases_on `m`);
(*PAUSE*)

(* Here the first sub-goal has an assumption that is false: 0 < 0. We can demonstrate this to the system by using the DECIDE function to prove a simple arithmetic fact: *)
e (METIS_TAC [DECIDE ``!x. ~(x < x)``]);

(* Using the theorems identified above the remaining sub-goals can be proved with RW_TAC. *)
e (RW_TAC arith_ss [FACT, DIVIDES_LMUL, DIVIDES_REFL]);

(* This last step, namely the invocation of RW_TAC, could also have been accomplished with METIS_TAC. We use backup b(), and do this step again. *)
b();

e (METIS_TAC [FACT, DIVIDES_RMUL, DIVIDES_REFL]);

(* Now we have finished the base case of iinduction. *)
(*PAUSE*)

(* We move to the next step case. An obvious thing to try is simplication with definition. *)
e (RW_TAC arith_ss [FACT, ADD_CLAUSES]);

(* and now, by DIVIDES_RMUL and the inductive hypothesis, we are done. *)
e (METIS_TAC [DIVIDES_RMUL]);

(* We have finished the search for the proof, and now turn to the task of making a single tactic out of the sequence of tactic invocations we have just made.

e (RW_TAC arith_ss [LESS_EQ_EXISTS]);
e (Induct_on `p`);
// 1
e (RW_TAC arith_ss []);
e (Cases_on `m`);
// 1.1
e (METIS_TAC [DECIDE ``!x. ~(x < x)``]);
// 1.2
e (RW_TAC arith_ss [FACT, DIVIDES_LMUL, DIVIDES_REFL]);
// 2
e (RW_TAC arith_ss [FACT, ADD_CLAUSES]);
e (METIS_TAC [DIVIDES_RMUL]);

Note the branchings ... *)
restart();

e (RW_TAC arith_ss [LESS_EQ_EXISTS]
    THEN Induct_on `p` THENL
        [RW_TAC arith_ss [] THEN Cases_on `m` THENL
            [METIS_TAC [DECIDE ``!x. ~(x < x)``],
             RW_TAC arith_ss [FACT, DIVIDES_LMUL, DIVIDES_REFL]],
         RW_TAC arith_ss [FACT, ADD_CLAUSES] THEN METIS_TAC [DIVIDES_RMUL]]);

(* For many users, this is the end of dealing with this proof. However, another user might notice that this tactic could be improved. *)
(*PAUSE*)

(* To start, both arms of the induction start with an invocation of RW_TAC. The semantics of THEN allow us to merge the occurrences of RW_TAC above the THENL. The recast tactic is: *)
restart();

e (RW_TAC arith_ss [LESS_EQ_EXISTS]
    THEN Induct_on `p`
    THEN RW_TAC arith_ss [FACT, ADD_CLAUSES]
    THENL [Cases_on `m` THENL
           [METIS_TAC [DECIDE ``!x. ~(x < x)``],
            RW_TAC arith_ss [FACT, DIVIDES_LMUL, DIVIDES_REFL]],
           METIS_TAC [DIVIDES_RMUL]]);
(*PAUSE*)

(* Now, recall that the use of RW_TAC in the base case could be replaced by a call to METIS_TAC. Thus it seems possible to merge the two sub-cases of the base case into a single invocation of METIS_TAC: *)
restart();

e (RW_TAC arith_ss [LESS_EQ_EXISTS]
    THEN Induct_on `p`
    THEN RW_TAC arith_ss [FACT, ADD_CLAUSES]
    THENL [Cases_on `m` THEN
           METIS_TAC [DECIDE ``!x. ~(x < x)``,
                       FACT, DIVIDES_RMUL, DIVIDES_REFL],
           METIS_TAC [DIVIDES_RMUL]]);
(*PAUSE*)

(* Finally, pushing the revisionism nearly to its limit, we'd likt there to be only a single invocation of METIS_TAC to finish the rpoof off. The semantics of THEN and ALL_TAC come to our rescue: we will split on the construction of m in the base case, as in the current incarnation of the tactic, but we will let the inductive case pass unaltered through the THENL. This is achieved by ALL_TAC, which is a tactic that acts as an identity function on the goal. *)
restart();

e (RW_TAC arith_ss [LESS_EQ_EXISTS]
    THEN Induct_on `p`
    THEN RW_TAC arith_ss [FACT, ADD_CLAUSES]
    THENL [Cases_on `m`, ALL_TAC]
    THEN METIS_TAC [DECIDE ``!x. ~(x < x)``,
                    FACT, DIVIDES_RMUL, DIVIDES_REFL]);
(*PAUSE*)

(* Are we now done? Not necessarily. For example, the explicit case split, namely Cases_on `m`, can be replaced by providing cases theorem for natural numbers (num_Cases) to METIS_TAC. With this, the case split on m will be automatically generated by METIS_TAC as it searches for a proof. *)
num_CASES;

restart();

e (RW_TAC arith_ss [LESS_EQ_EXISTS]
   THEN Induct_on `p`
   THEN METIS_TAC [DECIDE ``!x. ~(x < x)``,
                   FACT, num_CASES,
                   DIVIDES_RMUL, DIVIDES_LMUL,
                   DIVIDES_REFL, ADD_CLAUSES]);

(* We have now finished our exercise in tactic revision. Certainly, it would be hard to foresee that this final tactic would prove the goal. The required lemmas for the final invocation of METIS_TAC have been found by an incremental process. *)
(*PAUSE*)

(* 4.1.2 Divisibility and factorial -- again! *)

(* In the previous proof, we made an initial simplication step in order to expose a variable upon which to induct. However, the proof is really by induction on n-m. Can be express this directly? *)

(* The answer is a qualified yes: the induction can be naturally stated, but it leads to somewhat less natural goals. *)
restart();

e (Induct_on `n-m`);

(* This is slightly hard to read, so we use STRIP_TAC and REPEAT to move the antecedents of the goals to the assumptions. Use of THEN ensures that the tactic gets applied in both branches of the induction. *)
b();  (* backup *)

e (Induct_on `n-m` THEN REPEAT STRIP_TAC);

(* Looking at the first goal, we reason that if 0 = n - m and m <= n, then m = n. We can prove this fact, using DECIDE_TAC and add it to the hypotheses by use of the infix operator "by": *)
e (`m = n` by DECIDE_TAC);

(* We can now use RW_TAC to propagate the newly derived equality throughtout the goal. *)
e (RW_TAC arith_ss []);

(* At this point in the previous proof, we did a case analysis on m. However, we already have the hypothesis that m is positive (along with two other useless hypotheses). Thus we know that m is the successor of some number k. We might wish to assert this fact with an invocation by "by" as follows:

`?k. m = SUC k` by <tactic>

But what is the tactic? No suitable ones.

When such situation occur, it is often best to start a new proof for the required lemma. *)
g `!m. 0 < m ==> ?k. m = SUC k`;

(* Our new goal can be proved quite quickly. Once we have proved it, we can bind it to an ML name and use it in the previous proof, by some sleight of hand with the "before" function. *)
e (Cases THEN RW_TAC arith_ss []);

val lem = top_thm() before drop();
(*PAUSE*)

(* Now we can return to the main thread of the proof. The state of the current sub-goal of the proof can be displayed using the function "p". *)
p();

(* Now we can use lem in the proof. Somewhat opportunistically, we will tack on the invocation used in the earlier proof at (roughly) the same point, hoping that it will solve the goal: *)
e (`?k. m = SUC k` by
   METIS_TAC[lem] THEN RW_TAC arith_ss [FACT, DIVIDES_LMUL, DIVIDES_REFL]);

(* It does! That takes care of the base case. For the induction step, things look a bit more difficult than in the earlier proof. However we can make progress by realizing that the hypotheses imply that 0 < n and so, again by lem, we can transform n into a successor, thus enabling the unfolding of FACT as in the previous proof: *)
e (`0 < n` by DECIDE_TAC THEN `?k. n = SUC k` by METIS_TAC [lem]);

(* The proof now finishes in much the same manner as the previous one: *)
e (RW_TAC arith_ss [FACT, DIVIDES_RMUL]);

(* We leave the details of stitching the proof together to the interested reader. *)
val DIVIDES_FACT = top_thm() before drop();

(*PAUSE*)

(* 4.2 Primality *)

(* Now we move on to establish some facts about the primality of the first few numbers: 0 and 1 are not prime, but 2 is. Also, all primes are positive. These are all quite simple to prove. *)
g `~prime 0`;
e (RW_TAC arith_ss [prime_def, DIVIDES_0]);
val NOT_PRIME_0 = top_thm() before drop();

g `~prime 1`;
e (RW_TAC arith_ss [prime_def]);
val NOT_PRIME_1 = top_thm() before drop();

g `prime 2`;
e (RW_TAC arith_ss [prime_def]
   THEN METIS_TAC [DIVIDES_LE, DIVIDES_ZERO,
                   DECIDE ``~(2=1)``, DECIDE ``~(2=0)``,
                   DECIDE ``x <= 2 = (x=0) \/ (x=1) \/ (x=2)``]);
val PRIME_2 = top_thm() before drop();

g `!p. prime p ==> 0 < p`;
e (Cases THEN RW_TAC arith_ss [NOT_PRIME_0]);
val PRIME_POS = top_thm() before drop();
(*PAUSE*)

(* 4.3 Existence of prime factors *)

(* Now we are in position to prove a more substantial lemma: every number other than 1 has a prime factor. The proof proceeds by a complete induction on n. *)

(* Complete induction is necessary since a prime factor won't be the predecessor. After induction, the proof splits into cases on whether n is prime or not. The first case (n is prime) is trivial. In the second case, there must be an x that divides n, and x is not 1 or n. By DIVIDES_LE, n = 0 or x <= n. If n = 0, then 2 is a prime that dives 0. On the other hand, if x <= n, there are two cases: if x < n, then we can use inductive hypothesis and by transitivity of divides we are done; otherwise, x = n and then we have a contradiction with the fact tha x is not 1 or n. *)
g `!n. ~(n = 1) ==> ?p. prime p /\ p divides n`;

(* We start by invoking complete induction. This gives us an inductive hypothesis that holds at every number m < n: *)
e (completeInduct_on `n`);

(* We can move the antecedent to the hypotheses and make our case split. Notice that the term given to Cases_on need not occur in the goal: *)
e (RW_TAC arith_ss [] THEN Cases_on `prime n`);

(* As mentioned, the first case if proved with the reflexivity of divisibility: *)
e (METIS_TAC [DIVIDES_REFL]);

(* In the second case, we can get a divisor of n that isn't 1 or n (since n is not prime): *)
e (`?x. x divides n /\ ~(x=1) /\ ~(x=n)` by METIS_TAC [prime_def]);

(* At this point, the polished tactic simply invoke METIS_TAC with a collection of theorems. We shall attempt a more detailed exposition. Given the hypotheses, and by DIVIDES_LE, we can assert x < n or n = 0 and thus split the proof into two cases: *)
e (`x < n \/ (n=0)` by METIS_TAC [DIVIDES_LE, LESS_OR_EQ]);

(* In the first subgoal, we can see that the antecedents of the inductive hypothesis are met and so x has a prime divisor. We can then use transitivity of divisibility to get the fact that this divisior of x is also a divisor of n, thus finishing this branch of the proof: *)
e (METIS_TAC [DIVIDES_TRANS]);
(*PAUSE*)

(* The remaining goals can be clarified by simplification: *)
e (RW_TAC arith_ss []);

(* examine DIVIDES_0 *)
DIVIDES_0;

(* and apply it *)
e (RW_TAC arith_ss [it]);

(* The two steps of exploratory simplification have led us to a state where all we have to do is exhibit a prime. And we already have one on hand: *)
e (METIS_TAC [PRIME_2]);

(* Again, work now needs to be done to compose and perhaps polish a single tactic from the individual proof steps, but we will not describe it. Instead, we move forward, because our ultimate goal is in reach. *)
val PRIME_FACTOR = top_thm() before drop();
(*PAUSE*)

(* 4.4 Euclid's Theorem *)

(* Theorem: Every number has prime greater than it.
   [Informal proof]
   Suppose the opposite; then there's an n such that all p greater than n are not prime. Consider FACT(n)+1: it's not equal to 1 so, by PRIME_FACTOR, there's a prime p that divides it. Note that p also divides FACT(n) because p <= n. By DIVIDES_ADDL, we have p = 1. But then p is not prime, which is a contradiction.
   [End of proof]
*)

(* Let's start the HOL proof. *)
g `!n. ?p. n < p /\ prime p`;

(* A proof by contradiction can be started by using the bossLib function SPOSE_NOT_THEN. With it, one assumes that negation of the current goal and then uses that in an attempt to prove falsity (F). The assumed negation:

not (for-all n, there-exist p. n < p /\ prime p)

is simplified a bit into:

there-exists n. for-all p. n < p if not prime p

and then this is passed to the tactic STRIP_ASSUME_TAC. This moves its argument to the assumption list of the goal after eliminating the existential quantification on n. *)
e (SPOSE_NOT_THEN STRIP_ASSUME_TAC);

(* Thus we have the hypothesis that all p beyond a certain unspecified n are not prime, and our task is to show that this cannot be. At this point we take advantage of Euclid's great inspiration and we build an explicit term from n. In the informal proof we are asked to 'consider' the term FACT n + 1. This term will have certain properties (i.e. it has a prime factor) that lead to contradiction.

Question: how do we 'consider' this term in the formal HOL proof?
Answer: by instantiating a lemma with it and bringing the lemma into the proof. The lemma and its instantiation are: *)
PRIME_FACTOR;

val th = SPEC ``FACT n + 1`` PRIME_FACTOR;

(* It is evident that the antecedent of th can be eliminated. In HOL, one could do this in a so-called forward proof style (by proving |- FACT n + 1 &ne; 1) and then applying modus ponens, the result of which can then be used in the proof), or one could bring th into the proof and simplify it in situ. We choose the latter approach. *)
e (MP_TAC (SPEC ``FACT n + 1`` PRIME_FACTOR));

(* The invocation MP_TAC(|- M) applied to a goal (&delta;g) returns the goal (&delta;,M if g). Now we simplify: *)
e (RW_TAC arith_ss []);
(*PAUSE*)

(* We recall that zero is less than every factorial, a fact found in arithmeticTheory under the name FACT_LESS. Thus we can solve the top goal by simplification: *)
e (RW_TAC arith_ss [FACT_LESS, DECIDE ``!x. ~(x = 0) = 0 < x``]);

(* Notice the 'on-the-fly' use of DECIDE to provide an ad hoc rewrite. Looking at the remaining goal, one might think that our aim, to prove falsity, has been lost. But this is not so: a goal not P or not Q is logically equivalent to P implies Q implies F. In the following invocation, we use the equality |- A => B = not A or not B as a rewrite rule oriented right to left by use of GSYM. *)
IMP_DISJ_THM;

e (RW_TAC arith_ss [GSYM it]);

(* We can quickly proceed to show that p divides (FACT n), and thus that p = 1, hence p is not prime, at which point wer are done. This can all be packaged into a single invocation of METIS_TAC: *)
e (METIS_TAC [DIVIDES_FACT, DIVIDES_ADDL, DIVIDES_ONE, NOT_PRIME_1, NOT_LESS, PRIME_POS]);

(* Euclid's theorem is now proved, and we can rest. *)
val EUCLID = top_thm() before drop();
(*PAUSE*)

(* However, this presentation of the final proof will be unsatisfactory to some, because the proof is completely hidden in the invocation of the automated reasoner. *)

(* Well then, let's try another proof, this time employing the so-called 'assertional' style. When used uniformly, this can allow a readable linear presentation that mirrors the informal proof. The following proves Euclid's theorem in the assertional style. We think it is fairly readable, certainly much more so than the standard tactic proof just given. *)

val EUCLID_AGAIN =
    prove (``!n. ?p. n < p /\ prime p``,
    CCONTR_TAC THEN
    `?n. !p. n < p ==> ~prime p`  by METIS_TAC[]                  THEN
    `~(FACT n + 1 = 1)`           by RW_TAC arith_ss [FACT_LESS,
                                     DECIDE ``~(x = 0) = 0 < x``] THEN
    `?p. prime p /\
         p divides (FACT n + 1)`  by METIS_TAC [PRIME_FACTOR] THEN
    `0 < p`                       by METIS_TAC [PRIME_POS]    THEN
    `p <= n`                      by METIS_TAC [NOT_LESS]     THEN
    `p divides FACT n`            by METIS_TAC [DIVIDES_FACT] THEN
    `p divides 1`                 by METIS_TAC [DIVIDES_ADDL] THEN
    `p = 1`                       by METIS_TAC [DIVIDES_ONE]  THEN
    `~prime p`                    by METIS_TAC [NOT_PRIME_1]  THEN
    METIS_TAC[]);

(* 4.5 Turing the script into a theory *)
