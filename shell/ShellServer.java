/*
 * ShellServer.java
 *
 */

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * A Shell Server for executables with stdin input and stdout output.
 *
 * Customized to process GET and POST requests from Shell.html.
 *
 * Note:
 * String output by DataOutputStream will mess up with unicode, as no encoding is specified.
 * String output by PrintStream can preserver unicode characters, by specifying "UTF-8" encoding.
 */
public class ShellServer extends Thread {

    // the shell command
    public static String[] SHELL = {""}; // to be supplied by args[0]
    public static int DELAY = 100; // 100 milliseconds delay before sending response
    public static boolean running = true;

    // main action
    public static void main (String args[]) throws Exception {
        if (args.length == 0) { usage(); return; }
        SHELL = args; // arguments are to run
        DELAY = Integer.parseInt(System.getProperty("delay", "10"));
        String ip = System.getProperty("ip", "127.0.0.1"); // HTTP ip
        int port = Integer.parseInt(System.getProperty("port", "80")); // HTTP port
        int backlog = Integer.parseInt(System.getProperty("backlog", "10"));
        // backlog - the maximum length of the queue.
        // The maximum queue length for incoming connection indications (a request to connect)
        // is set to the backlog parameter. If a connection indication arrives when the queue is full,
        // the connection is refused.
        ServerSocket Server = new ServerSocket(port, backlog, InetAddress.getByName(ip));
        System.out.println("Shell Server Waiting for client on port " + port);
        // server listens in a loop
        while (running) {
            Socket connected = Server.accept();
            (new ShellServer(connected)).start();
        }
        closeLaunchers();
        System.out.println("Shell Server shutdown.");
    }
    // log a message
    public static void log(String s) {
        // log goes to stderr, since Launcher will grab all stdout
        System.err.println(s);
    }
    // how to use this application
    public static void usage() {
        System.out.println("Usage: java [options] -jar ShellServer.jar [exec]");
        System.out.println("");
        System.out.println("where [exec] is an executable with stdin input and stdout output.");
        System.out.println("For example, cmd for Windows or /bin/bash in Linux/Unix systems.");
        System.out.println("             or path/to/hol.bat for interactive HOL in browser");
        System.out.println("");
        System.out.println("The above command starts the web-server.");
        System.out.println("The web-client is any browser at URL: http://localhost/Shell.html.");
        System.out.println("");
        System.out.println("Available options (in the form -Doption=value) are:");
        System.out.println("-Dip=value      IP address for server, default: 127.0.0.1");
        System.out.println("-Dport=value    HTTP port for server, default: 80");
        System.out.println("-Ddelay=value   Milliseconds to wait before response, default: 100");
        System.out.println("-Dbacklog=value Queue length for incoming connections, default: 10");
        System.out.println("");
    }

    // a map of UID to Launcher and Console loggers
    private static HashMap<String, Launcher> map = new HashMap<String, Launcher>();
    private static HashMap<String, PrintStream> logMap = new HashMap<String, PrintStream>();
    private static int mapCount = 0;

    // get Launcher of uid
    private static Launcher getLauncher(String uid) {
        Launcher launcher = map.get(uid);
        if (launcher == null) {
            mapCount++;
            log("new Launcher for ["+uid+"], mapCount="+mapCount);
            //if (mapCount > 2) throw new RuntimeException("Too Many!");
            launcher = new Launcher(SHELL); // now very fast
            map.put(uid, launcher); // put the launcher into map, quick.
            launcher.initialize(); // this can take some time
            // after here, launcher grabs all System.out
        }
        log("Launcher for ["+uid+"], mapCount="+mapCount);
        return launcher;
    }
    // close all Launchers
    private static void closeLaunchers() {
        Iterator<Launcher> j = map.values().iterator();
        while (j.hasNext()) {
            j.next().closeFrame();
        }
    }

    // constants
    private static final String HTML_START =
        "<html>" + "<title>Shell Server in java</title>" + "<body>";
    private static final String HTML_END = "</body>" + "</html>";
    private static final String HTML_NOT_FOUND = "The Requested resource cannot be found: ";

    // attributes
    private Socket client;

    // constructor
    public ShellServer(Socket client) {
        this.client = client;
    }

    public void run() {
        String session = client.getInetAddress() + ":" + client.getPort();
        log("Client " + session + " - connected");
        try {
            // get input stream
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            // parse header
            String headerLine = in.readLine();
            StringTokenizer tokenizer = new StringTokenizer(headerLine);
            String httpMethod = tokenizer.nextToken();
            String httpQueryString = tokenizer.nextToken();
            log(headerLine); // echo header
            if (httpMethod.equals("GET")) doGet(httpQueryString, in); // GET request
            if (httpMethod.equals("POST")) doPost(httpQueryString, in); //POST request
        }//end-try
        catch (Exception e) {
            e.printStackTrace();
        }
        log("Client " + session + " - done (HTTP!)");
    }

    // do GET request
    public void doGet(String httpQueryString, BufferedReader in) throws Exception {
        log("GET request: " + httpQueryString);
        // skip the Request Header
        while (in.ready()) {
            log("GET: "+in.readLine());
        }
        // retrieve the file
        try {
            // remove the leading /
            String filename = httpQueryString.substring(1, httpQueryString.length());
            sendResponse(200, filename, true);
        }
        catch (Exception e) {
            sendResponse(404, "<b>" + ShellServer.HTML_NOT_FOUND + httpQueryString + "</b>", false);
        }
    }

    // do POST request
    public void doPost(String httpQueryString, BufferedReader in) throws Exception {
        log("POST request: " + httpQueryString);
        String currentLine;
        String contentType = "unknown";
        int contentLength = 0;
        String uid = "none";
        // parse the Request Header
        while (in.ready()) {
            currentLine = in.readLine();
            log("POST: "+currentLine);
            if (currentLine.equals("")) break;
            if (currentLine.indexOf("Content-Type:") == 0)
                contentType = currentLine.split(" ")[1];
            if (currentLine.indexOf("Content-Length:") == 0)
                contentLength = Integer.parseInt(currentLine.split(" ")[1]);
            if (currentLine.indexOf("Client-Uid:") == 0)
                uid = currentLine.split(" ")[1];
        } //end-while
        log("contentType="+contentType);
        log("contentLength="+contentLength);
        log("uid="+uid);
        Launcher launcher = getLauncher(uid);
        PrintStream logging = logMap.get(uid);
        boolean logClose = false;
        boolean frameClose = false;
        if (contentLength > 0) {
            // get the text content in POST
            char[] cb = new char[contentLength];
            in.read(cb, 0, contentLength);
            String input = new String(cb);
            // String response = input;  // if no Launcher
            // pass input to cmd Process, or Launcher, etc.
            if (httpQueryString.equals("/rpc/command/")) {
                if (logging != null) logging.print(input);
                // remove last \n, if any, to put into TextField
                if (input.endsWith("\n")) input = input.substring(0,input.length()-1);
                launcher.putTextField(input);
                launcher.activateTextField(); // line-oriented, maybe just \n
            }
            else if (httpQueryString.equals("/rpc/control/")) {
                if (input.startsWith("^")) launcher.sendKey(input);
                else if (input.equals("$")) frameClose = true; // close frame after sendResponse
                else if (input.equals("!")) logClose = true; // close logging after sendResponse
                else if (logging == null) loggingOpen(uid, input); // ignore file if already logging
                if (input.equals("^C")) running = false; // will shutdown server
            }
            else {
                // unknown protocol
                log("POST: Unknown protocol: "+httpQueryString);
            }
        }
        // sleep for 10 milliseconds
        this.sleep(DELAY); // wait for response from application
        // get response from application Process, or Launcher.
        String response = launcher.getResponse();
        sendResponse(200, response, false);
        if (logging != null && response.length() > 0) logging.print(response);
        // check after sendResponse
        if (logClose || frameClose) {
            if (logging != null) loggingClose(uid, logging);
        }
        if (frameClose) {
            launcher.closeFrame();
            map.remove(uid);
        }
    }

    // send response
    public void sendResponse(int statusCode, String responseString, boolean isFile) throws Exception {
        String contentTypeLine = null;
        String contentLengthLine = null;
        FileInputStream fin = null;

        String statusLine = statusCode == 200 ? "HTTP/1.1 200 OK\r\n" :
                                                "HTTP/1.1 404 Not Found\r\n";

        if (isFile) {
            String fileName = responseString;
            fin = new FileInputStream(fileName);
            contentTypeLine = "Content-Type: " + getContentType(fileName) + "\r\n";
            contentLengthLine = "Content-Length: " + Integer.toString(fin.available()) + "\r\n";
        }
        else {
            contentTypeLine = "Content-Type: " + getContentType("") + "\r\n";
            contentLengthLine = "Content-Length: " + responseString.length() + "\r\n";
        }
        // set up PrintStream with autoflush = false, encoding = "UTF-8"
        PrintStream out = new PrintStream(client.getOutputStream(), false, "UTF-8");

        out.print(statusLine);
        out.print("Server: Java ShellServer\r\n");
        out.print(contentTypeLine);
        out.print(contentLengthLine);
        out.print("Connection: close\r\n");
        out.print("\r\n");

        if (isFile) {
            sendFile(fin, new DataOutputStream(out));
        }
        else {
            out.print(responseString);
        }

        out.flush();
        out.close();
    }

    // send file, using DataOutputStream to write buffer bytes
    public void sendFile(FileInputStream fin, DataOutputStream out) throws Exception {
        byte[] buffer = new byte[1024] ;
        int bytesRead;
        while ((bytesRead = fin.read(buffer)) != -1 ) {
            out.write(buffer, 0, bytesRead);
        }
        fin.close();
    }

    // get the content type from fileName
    private String getContentType(String fileName) {
        if (fileName.endsWith(".txt")) return "text/plain";
        if (fileName.endsWith(".css")) return "text/css";
        if (fileName.endsWith(".html")) return "text/html";
        if (fileName.endsWith(".htm")) return "text/html";
        if (fileName.endsWith(".js")) return "application/x-javascript";
        return "text/plain; charset=utf-8";
    }

    // open logging file for client
    private void loggingOpen(String uid, String filename) throws IOException {
        log("Logging for uid=" + uid + " to: " + filename);
        // PrintStream with autoflush = true, charset = "UTF-8"
        logMap.put(uid, new PrintStream(new FileOutputStream(filename), true, "UTF-8"));
    }

    // close logging file of client
    private void loggingClose(String uid, PrintStream logging) throws IOException {
        log("Logging for uid=" + uid + " closed.");
        try {
            logging.flush();
            logging.close();
        }
        finally {
            logging = null;
            logMap.remove(uid);
        }
    }
}
