/**
 *  Launcher.java -- console for redirect standard input and output with error
 *
 *  A Shell Launcher.
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Launcher extends Frame {

    // main action
    public static void main(String args[]) {
        new Launcher(args);
    }

    // attributes
    private Process mainProcess;
    private RedirectThread threadInput;
    private RedirectThread threadError;
    private OutputStream stdin;

    private String[] cmdLines;
    private boolean ready;
    private boolean echo = true;

    // Constructor
    public Launcher(String[] args) {
        cmdLines = args;
        ready = true;
    }

    private TextField textField;
    private TextArea textArea;
    private Label lblInput;
    private Label lblProcess;
    private Label lblStatus;
    private Button btnTerminate;

    // build the GUI frontend
    private void buildGUI() {
        GridBagLayout gridbaglayout = new GridBagLayout();
        setLayout(gridbaglayout);
        setVisible(false);
        setSize(672, 408);
        setBackground(new Color(0xc0c0c0));

        lblProcess = new Label("Process:");
        lblProcess.setBounds(4, 4, 66, 23);
        GridBagConstraints gridbagconstraints = new GridBagConstraints();
        gridbagconstraints.gridx = 0;
        gridbagconstraints.gridy = 0;
        gridbagconstraints.anchor = 17;
        gridbagconstraints.fill = 0;
        gridbagconstraints.insets = new Insets(4, 4, 2, 0);
        ((GridBagLayout) getLayout()).setConstraints(lblProcess, gridbagconstraints);
        add(lblProcess);

        lblStatus = new Label("STATUS");
        lblStatus.setBounds(74, 5, 60, 23);
        gridbagconstraints = new GridBagConstraints();
        gridbagconstraints.gridx = 1;
        gridbagconstraints.gridy = 0;
        gridbagconstraints.anchor = 13;
        gridbagconstraints.fill = 2;
        gridbagconstraints.insets = new Insets(4, 4, 0, 4);
        ((GridBagLayout) getLayout()).setConstraints(lblStatus, gridbagconstraints);
        add(lblStatus);

        textField = new TextField();
        textField.setBounds(71, 384, 600, 23);
        gridbagconstraints = new GridBagConstraints();
        gridbagconstraints.gridx = 1;
        gridbagconstraints.gridy = 2;
        gridbagconstraints.gridwidth = 2;
        gridbagconstraints.weightx = 1.0D;
        gridbagconstraints.anchor = 17;
        gridbagconstraints.fill = 2;
        gridbagconstraints.insets = new Insets(2, 1, 1, 1);
        ((GridBagLayout) getLayout()).setConstraints(textField, gridbagconstraints);
        add(textField);

        textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setBounds(1, 31, 670, 350);
        textArea.setFont(new Font("MonoSpaced", 0, 12));
        textArea.setBackground(SystemColor.text.brighter());
        gridbagconstraints = new GridBagConstraints();
        gridbagconstraints.gridx = 0;
        gridbagconstraints.gridy = 1;
        gridbagconstraints.gridwidth = 3;
        gridbagconstraints.weightx = 1.0D;
        gridbagconstraints.weighty = 1.0D;
        gridbagconstraints.fill = 1;
        gridbagconstraints.insets = new Insets(2, 1, 1, 1);
        ((GridBagLayout) getLayout()).setConstraints(textArea, gridbagconstraints);
        add(textArea);

        lblInput = new Label("Input");
        lblInput.setBounds(4, 384, 41, 23);
        gridbagconstraints = new GridBagConstraints();
        gridbagconstraints.gridx = 0;
        gridbagconstraints.gridy = 2;
        gridbagconstraints.anchor = 17;
        gridbagconstraints.fill = 0;
        gridbagconstraints.insets = new Insets(2, 4, 1, 4);
        ((GridBagLayout) getLayout()).setConstraints(lblInput, gridbagconstraints);
        add(lblInput);

        btnTerminate = new Button();
        btnTerminate.setLabel("Terminate");
        btnTerminate.setBounds(601, 5, 70, 23);
        btnTerminate.setEnabled(false);
        gridbagconstraints = new GridBagConstraints();
        gridbagconstraints.gridx = 2;
        gridbagconstraints.gridy = 0;
        gridbagconstraints.anchor = 13;
        gridbagconstraints.fill = 0;
        gridbagconstraints.insets = new Insets(4, 4, 0, 1);
        ((GridBagLayout) getLayout()).setConstraints(btnTerminate, gridbagconstraints);
        add(btnTerminate);

        setTitle("Shell Console, 2011.");
        setForeground(SystemColor.textText);
        setBackground(SystemColor.text);

        addWindowListener(new LauncherWindowAdaptor());
        btnTerminate.addActionListener(new TerminateActionListener());
        textField.addActionListener(new TextFieldActionListener());

        setVisible(true);
    }

    // initialize the Launcher
    public void initialize() {
        buildGUI();
        // All system output (stdio and stderr) will go to TextArea
        // Now: only capture stdout, leave out stderr to console
        //System.setErr(new TextPrintStream(System.err));
        System.setOut(new TextPrintStream(System.out));

        try {
            Runtime runtime = Runtime.getRuntime();
            mainProcess = runtime.exec(cmdLines);

            threadInput = new RedirectThread(mainProcess.getInputStream()); // stdout of process
            threadError = new RedirectThread(mainProcess.getErrorStream()); // stderr of process
            stdin = mainProcess.getOutputStream();  // stdin for process

            lblStatus.setText("Running...");
            btnTerminate.setEnabled(true);
            //new ProcessThread(mainProcess).start();
            if (threadInput != null) threadInput.start();
            if (threadError != null) threadError.start();
            if (stdin != null) textField.addKeyListener(new TextFieldKeyAdapter());
        }
        catch (Exception ex) {
            lblStatus.setText("Error");
            System.err.println(ex);
            ex.printStackTrace();
        }
    }

    // close this Launcher
    public void closeFrame() {
        setVisible(false);
        if (mainProcess != null) {
            try {
                mainProcess.destroy();
                mainProcess = null;
                cleanUp();
            }
            catch (Exception ex) {
            }
        }
        dispose(); // of the Frame
    }

    // cleanup before it dies
    private void cleanUp() {
        ready = false;
        btnTerminate.setEnabled(false);
        lblStatus.setText("Finished");

        try {
            stdin.close();
            threadInput.discard();
            threadError.discard();
            return;
        }
        catch (IOException _ex) {
            return;
        }
    }

    // fill up TextField
    public void putTextField(String s) {
        textField.setText(s);
    }

    // activate TextField
    public void activateTextField() {
        String line = textField.getText();
        if ("echoOn".equals(line)) { echo = true; return; }
        if ("echoOff".equals(line)) { echo = false; return; }
        //System.out.println(line); // echo to textField output
        if (echo) putTextArea(line+"\n"); // echo by directly putting to textField
        try {
            // Note: the \n is critical for HOL_HOME/bin/unquote.exe to respond
            stdin.write((line+"\n").getBytes()); // put to stdin
            stdin.flush(); // send it
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
    }

    // send a control key
    public void sendKey(String cmd) {
        // convert control character to byte
        // ASCII: ^A=1, ^B=2, ...., ^Z=26
        byte b = (byte) (cmd.codePointAt(1) - 64); // 65 = charCode of "A"
        //System.err.println("JC: sending "+cmd+" to stdin, byte="+b);
        putTextField(cmd); // echo control key in textfield
        try {
            stdin.write(new byte[]{b}); // put to stdin
            stdin.flush(); // send it
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
    }

    // write to TextArea
    private synchronized void putTextArea(String s) {
        textArea.append(s);
    }

    // modify CR not followed by LF as single LF.
    private void modifyBuffer(StringBuffer s) {
        for (int j = 0; j < s.length(); j++) {
            if (s.charAt(j) < ' ') {
                int h = s.codePointAt(j);
                int k = j == s.length()-1 ? 0 : s.codePointAt(j+1);
                // 13 = \r = 0x0D = CR, 10 = \n = 0x0A = LF
                if (h == 13 && k == 10) {
                    if (j != s.length()-1) j++;
                }
                else {
                    // System.out.println("JC: CR not followed by LF");
                    s.setCharAt(j, '\n');
                }
            }
        }
    }

    // for RPC response
    private StringBuffer response = new StringBuffer();

    // get response
    public String getResponse() {
        String s = response.toString();
        response.delete(0, s.length()); // clear response
        //System.err.println("JC: get response [["+s+"]]");
        return s;
    }
    // put response
    private void putResponse(StringBuffer sb) {
        //System.err.println("JC: put response [["+sb+"]]");
        response.append(sb);
    }

    // ------------------- Inner Classes ---------------------------------------

    class RedirectThread extends Thread {

        InputStream in;

        public RedirectThread(InputStream inputstream) {
            in = inputstream;
        }

        public void discard() {
            try {
                in.close();
                return;
            }
            catch (IOException ex) {
                return;
            }
        }

        public void run() {
            try {
                // Note: set "UTF8" for Unicode
                InputStreamReader reader = new InputStreamReader(in, "UTF8");
                Launcher redirect = Launcher.this;
                while (redirect.ready) {
                    // Put stdout to TextArea
                    while (reader.ready()) {
                        // read from reader is char[], read from in is byte[]
                        // new String(byte[]), new StringWriter uses char[]
                        int j = -1;
                        int max = 1024; // 1024 or 50 for testing
                        char[] buffer = new char[max];
                        while ((j = reader.read(buffer, 0, max)) != -1) {
                            StringWriter sw = new StringWriter();
                            sw.write(buffer, 0, j);
                            putResponse(sw.getBuffer());
                            modifyBuffer(sw.getBuffer());
                            System.out.print(sw.toString()); // buffer has \n
                            //System.out.println("1* ["+sw.toString()+ "], j="+j+
                            // ", sw.length="+sw.getBuffer().length());
                        }
                    }
                    try {
                        Thread.currentThread();
                        Thread.sleep(50L);
                    }
                    catch (InterruptedException ex) {
                        System.out.println("Interrupted.");
                    }
                }
                System.out.println("done.");
                return;
            }
            catch (Exception _ex) {
                System.err.println("_ex: "+_ex);
                return;
            }
        }
    }

    class TerminateActionListener implements ActionListener {

        public void actionPerformed(ActionEvent actionevent) {
            Launcher redirect = Launcher.this;
            if (redirect.mainProcess != null) {
                redirect.mainProcess.destroy();
                cleanUp();
                lblStatus.setText("Killed");
            }
        }
    }

    class TextFieldActionListener implements ActionListener {

        public void actionPerformed(ActionEvent actionevent) {
            putTextField("");
        }

    }

    class TextFieldKeyAdapter extends KeyAdapter {

        public void keyPressed(KeyEvent keyevent) {
            int kc = keyevent.getKeyCode();
            if (kc == KeyEvent.VK_ENTER) { // ENTER
                activateTextField();
            }
            else if (kc == KeyEvent.VK_C && keyevent.isControlDown()) { // CTRL-C
                sendKey("^C");
            }
            else if (kc == KeyEvent.VK_Y && keyevent.isControlDown()) { // CTRL-Y
                sendKey("^Y");
            }
            else if (kc == KeyEvent.VK_Z && keyevent.isControlDown()) { // CTRL-Z
                sendKey("^Z");
            }
        }
    }

    class LauncherWindowAdaptor extends WindowAdapter {

        public void windowClosing(WindowEvent windowevent) {
            closeFrame();
        }
    }

    class TextPrintStream extends PrintStream {

        public TextPrintStream(PrintStream printstream) {
            super(printstream);
        }
        public void print(String s) {
            putTextArea(s);
        }
        public void println(String s) {
            putTextArea(s + "\r\n");
        }
        public void print(Object obj) {
            print(obj.toString());
        }
        public void println(Object obj) {
            println(obj.toString());
        }
    }
}
