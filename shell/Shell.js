/*
 * Shell.js -- for Shell in Browser
 *
 */

// global variables
var _in,   // the input
    _out,  // the output
    _info; // the info

// initialization (called by HTML <body onload>)
function init() {
    _in = document.getElementById("input");
    _out = document.getElementById("output");
    _info = document.getElementById("info");
    Input.clear();
    Input.refocus();
    Command.help(10000); // suggest doHelp for 10 seconds.
    RPC.More(0); // reset delay, starts background server polling.
}

// keep Textbox in focus (called by <html onclick>)
function keepFocusInTextbox(e) {
    var g = e.srcElement ? e.srcElement : e.target; // IE vs. standard
    while (!g.tagName) g = g.parentNode;
    var t = g.tagName.toUpperCase(); // examine the tag
    if (t == "A" || t == "INPUT") return; // clicking a link, or typing input: no focus
    // if selecting something, no focus
    if (window.getSelection) { // Mozilla
        if (String(window.getSelection())) return;
    }
    else if (document.getSelection) { // Opera? Netscape 4?
        if (document.getSelection()) return;
    }
    else { // IE
        if (document.selection.createRange().text) return;
    }
    Input.refocus(); // must focus
}

// *** Input ********************************************************

var Input = new function() {

    // public methods ---------------------------

    // handle the event e when key is down (called by HTML <input> field)
    this.keyDown = function(e) {
        if (e.shiftKey && e.keyCode == 13) { // shift-enter
            // don't do anything; allow the shift-enter to insert a line break as normal
        }
        else if (e.keyCode == 13) Command.run(getInput()); // enter
        // just pressing the CTRL key itself is an event
        else if (e.ctrlKey && e.keyCode > 64) Command.send(e.keyCode); // all CTRL-code pass to server
        else if (e.keyCode == 38) History.go(true); // up
        else if (e.keyCode == 40) History.go(false); // down
        else {
            // alert(e.keyCode); // no tab complete: e.keyCode == 9
        }
        setTimeout(recalculateInputHeight, 0); // expand <input> for any key, e.g. paste by CTRL-V
    }
    // refocus on <input> field
    this.refocus = function() {
        _in.blur(); // needed for Mozilla to scroll correctly.
        _in.focus();
    }
    // clear input
    this.clear = function() {
        _in.value = ''; // for print() output to go in the right place.
        recalculateInputHeight();
        Input.refocus(); // cannot use: this.refocus(), why? RPC.exit() can use 'this'.
    }

    // private methods --------------------------

    // get input
    function getInput() {
        var s = _in.value;
        setTimeout(Input.clear, 0); // do this later, so \n will be echoed first for <textarea>
        return s;
    }

    // recompute input height for visibility and selection
    function recalculateInputHeight() {
        var rows = _in.value.split(/\n/).length // many lines when input is pasted
                   + (window.opera ? 1 : 0); // leave room for scrollbar in Opera
        if (_in.rows != rows) _in.rows = rows; // expand number of rows
    }
}

// *** History ********************************************************

var History = new function() {
    // internal attributes ---------------------

    var histList = [""]; // list of commands
    var histPos = 0; // pointer along histList

    // public methods ---------------------------

    // get last command from history
    this.get = function() {
        return histPos == 0 ? '' : histList[histPos-1];
    }
    // put command in history
    this.put = function(cmd) {
        histList[histList.length-1] = cmd;
        histList[histList.length] = '';
        histPos = histList.length - 1;
    }

    // get command from history, depending on up/down
    this.go = function(up) {
        // histList[0] = first command entered, [1] = second, etc.
        // type something, press up: thing typed is now in "limbo"
        // (last item in histList) and should be reachable by pressing down again.
        var L = histList.length;
        if (L == 1) return;

        if (up) {
            if (histPos == L-1) {
                // Save this entry in case the user hits the down key.
                histList[histPos] = _in.value;
            }
            if (histPos > 0) {
                histPos--;
                // Use a timeout to prevent up from moving cursor within new text
                // Set to nothing first for the same reason
                setTimeout(function() {
                    _in.value = '';
                    _in.value = histList[histPos];
                    var caretPos = _in.value.length;
                    if (_in.setSelectionRange) _in.setSelectionRange(caretPos, caretPos);
                },0);
            }
        }
        else { // down
            if (histPos < L-1) {
                _in.value = histList[++histPos];
            }
            else if (histPos == L-1) {
                // Already on the current entry: clear but save
                if (_in.value) {
                    histList[histPos++] = _in.value;
                    _in.value = '';
                }
            }
        }
    }
}

// *** TextBuffer (Input Simulation) **********************************

var TextBuffer = new function() {
    // internal attributes ----------------------

    var buffer = []; // text buffer
    var ready = true; // the textBuffer ready flag (not ready if wait or pause)
    var wait = false; // wait for response
    var pause = false; // pause for response
    var name = ''; // name of file

    // public attributes ------------------------
    this.doPause = true; // true = honour "no pause"

    // public methods ---------------------------

    // is textBuffer empty
    this.empty = function () {
        return buffer.length == 0;
    }
    // clear textBuffer
    this.clear = function() {
        buffer = [];
        name = '';
        RPC.showInfo('');
    }
    // do a file loading for input
    this.load = function(file) {
        if (!file) file = prompt('File to load', 'euclid.sml');
        if (file != null) RPC.getFile(file); // callback loadRun
        name = file;
    }
    // load text file into buffer and run (callback from GET)
    this.loadRun = function(text) {
        buffer = text.split('\n');
        RPC.showInfo(RPC.info + ' ('+buffer.length+' lines)');
        ready = true;
        if (buffer.length > 0) {
            this.run();
        }
        else {
            RPC.More(); // no extend of delay
        }
    }
    // do run, or continue to run the buffer after PAUSE
    this.run = function() {
        pause = false; // no more pausing
        ready = true; // continue to run buffer
        setTimeout(function() {
            RPC.showInfo('Loading input from '+name);
        }, 100); // wait 100ms for previous doCommand(line) to complete.
        RPC.More(0); // reset delay
    }
    // get lines from text buffer to command
    this.get = function() {
        if (!ready) return;
        if (buffer.length == 0) return; // no coming back
        // pass lines to inBuffer, where RPCMore() will pick up when ready
        var line = buffer.shift();
        if ((line.indexOf('(*WAIT') == 0) ||
            (!this.doPause && line.indexOf('(*PAUSE') == 0)) { // PAUSE = WAIT if doPause = false
            if (wait && Command.echo) { // prompt not seen, in echo mode
                // do WAIT
                ready = false;
                return;
            }
            else { // prompt seen, or not in echo mode
                line = buffer.shift(); // process next line
            }
        }
        if (this.doPause && line.indexOf('(*PAUSE') == 0) {
            // do PAUSE
            pause = true;
            ready = false;
            setTimeout(function() {
                RPC.showInfo('Paused, type CTRL-L to continue loading '+name);
            }, 100); // wait 100ms for previous doCommand(line) to complete.
            return;
        }
        if (line.indexOf('(*BEGIN') == 0) {
            // run a BEGIN...END block
            while (buffer.length > 0) {
                line = buffer.shift();
                if (line.indexOf('(*END') == 0) break;
                Command.put(line);
            }
        }
        else {
            wait = true; // when wait turns to false, prompt is seen
            Command.put(line); // do one line
        }
        if (this.empty()) this.clear(); // do showInfo() there
    }
    // set wait to false (callback from prompt seen)
    this.noWait = function() {
        wait = false;
        // for PAUSE, keep ready false until next run() by CTRL-L
        if (!pause) ready = true;
    }
}

// *** Command Handling ***********************************************

var Command = new function() {
    // internal attribute -----------------------

    var inBuffer = []; // the input buffer
    var inComment = false; // the flag for pretty-printer

    // private methods --------------------------

    // pretty print input s
    function prettyPrint(s) {
/*
        // allows HTML tags <nospace>
        s = s.replace(/ < /g, ' &lt; ').replace(/ > /g, ' &gt; ') // for innerHTML
             .replace(/\n$/, '<br>')  // \n at end to <br>
             .replace(/\n/g, '\\n') // other \n to '\n'
             .replace(/\t/g, '\\t'); // \t to '\t'
        s = toUnicode(s);
*/
        if (!inComment) {
            var k = s.indexOf('(*');
            inComment = k != -1;
            if (k > 0) { // split before and after comment
                Printer.printInput(s.slice(0,k)); // before is normal
                s = s.slice(k); // after is comment
            }
        }
        if (inComment) {
            var k = s.indexOf('*)');
            if (k != -1) {
                Printer.printComment(s.slice(0,k+2)); // before is comment
                Printer.printInput(s.slice(k+2)); // after is normal
            }
            else Printer.printComment(s);
        }
        else Printer.printInput(s);
        if (inComment) inComment = s.indexOf('*)') == -1;
    }

    // unicode conversion
    function toUnicode(s) {
        var sb = [];
        for (var j = 0; j < s.length; j++) {
            if (s.charCodeAt(j) < 128) sb.push(s.charAt(j));
                     else sb.push('&#'+s.charCodeAt(j)+';');
        }
        return sb.join('');
    }

    // display help information
    function doHelp() {
        var sb = [];
        sb.push('<h4>Special commands in Input line</h4>');
        sb.push('<table>');
        sb.push('<tr><td>doEcho</td><td>&nbsp;&nbsp;&nbsp;</td><td>echo Input to console (default).</td></tr>');
        sb.push('<tr><td>noEcho</td><td></td><td>no echo of Input to console.</td></tr>');
        sb.push('<tr><td>doMore</td><td></td><td>start "more" in background (default).</td></tr>');
        sb.push('<tr><td>noMore</td><td></td><td>stop "more" in background.</td></tr>');
        sb.push('<tr><td>doLoad [filename]</td><td></td><td>load text input from [filename].</td></tr>');
        sb.push('<tr><td>noLoad</td><td></td><td>discard all inputs from text.</td></tr>');
        sb.push('<tr><td>doPause</td><td></td><td>respect (*PAUSE*) on text input.</td></tr>');
        sb.push('<tr><td>noPause</td><td></td><td>ignore (*PAUSE*) on text input.</td></tr>');
        sb.push('<tr><td>doLog [filename]</td><td></td><td>start console logging to [filename].</td></tr>');
        sb.push('<tr><td>noLog</td><td></td><td>stop console logging.</td></tr>');
        sb.push('<tr><td>cls</td><td></td><td>clear console Output.</td></tr>');
        sb.push('<tr><td>doHelp</td><td></td><td>display this Help.</td></tr>');
        sb.push('<tr><td>doExit</td><td></td><td>close this Session.</td></tr>');
        sb.push('</table>');
        sb.push('<h4>Special keys for Input line</h4>');
        sb.push('<ul>');
        sb.push('<li>UP: retrieve last command from History.</li>');
        sb.push('<li>DOWN: next command from History when doing UP.</li>');
        sb.push('<li>SHIFT-ENTER: soft-enter for mutli-line input (input will not send).</li>');
        sb.push('<li>CTRL-L: prompt for filename for text input, or continue from pause.</li>');
        sb.push('</ul>');
        Printer.printInfo(sb.join(''));
    }

    // public attribute -------------------------
    this.echo = true; // the command echo (false for DOS)

    // public methods ---------------------------

    // check if inBuffer is empty
    this.isEmpty = function() {
        return inBuffer.length == 0;
    }

    // put command line
    this.put = function(line) {
        // delete CR at end, if  any
        if (line.indexOf('\r')+1 == line.length) line = line.slice(0,-1);
        if (this.echo) prettyPrint(line+'\n'); // append \n for line
        // put line in buffer
        inBuffer.push(line);
    }

    // get content of inBuffer
    this.get = function() {
        var m = inBuffer.length; // get the current length
        if (m == 0) return ''; // really empty buffer
        var sb = [];
        for (var j = 0; j < m; j++) sb.push(inBuffer.shift()); // shift only m times
        return sb.join('\n')+'\n'; // line inputs, even last line has \n
    }

    // run a command
    this.run = function(cmd) {
        if (cmd == 'cls') shellCommands.cls();
        else if (cmd == 'doEcho') this.echo = true;
        else if (cmd == 'noEcho') this.echo = false;
        else if (cmd == 'doMore') RPC.more = true;
        else if (cmd == 'noMore') RPC.more = false;
        else if (cmd.indexOf('doLoad') == 0) TextBuffer.load(cmd.substring('doLoad '.length));
        else if (cmd == 'noLoad') TextBuffer.clear();
        else if (cmd == 'doPause') TextBuffer.doPause = true;
        else if (cmd == 'noPause') TextBuffer.doPause = false;
        else if (cmd.indexOf('doLog') == 0) RPC.logging(cmd.substring('doLog '.length));
        else if (cmd == 'noLog') RPC.logging('!');
        else if (cmd == 'doHelp') doHelp();
        else if (cmd == 'doExit') RPC.exit();
        else {
            History.put(cmd);
            Command.put(cmd);
            RPC.More(0); // reset delay
        }
        RPC.showInfo();
        setTimeout(Input.clear, 0); // for print() output to go in the right place.
    }

    // send a control code
    this.send = function(code) {
        if (code == 76) { // CTRL-L for load/run to input
            setTimeout(Input.refocus, 1000); // FF/Chrome: CTRL-L =  openLocation
            return TextBuffer.empty()? TextBuffer.load() : TextBuffer.run();
        }
        else if (code == 67 || code == 86) { // CTRL-C is copy, CTRL-V is paste
            return; // no need to send to server (some server process takes CTRL-C as interrupt)
        }
        else if (code == 89 || code == 90) { // CTRL-Y is redo, CTRL-Z is undo
            return; // no need to send to server (some server process takes CTRL-Z as end-of-file)
        }
    /*
        else if (code == 67) doControl('^C'); // CTRL-C (copy)
        else if (code == 77) doControl('^M'); // CTRL-M
        else if (code == 86) doControl('^V'); // CTRL-V (paste)
        else if (code == 89) doControl('^Y'); // CTRL-Y (FF redo)
        else if (code == 90) doControl('^Z'); // CTRL-Z (FF undo)
    */
        _in.value = ''; // for print() output to go in the right place.
        RPC.sendControl('^'+String.fromCharCode(code));
    }

    // show help given duration
    this.help = function(duration) {
        var oldInfo = RPC.info;
        RPC.showInfo('Type "doHelp" for help');
        setTimeout(function() { RPC.showInfo(oldInfo); }, duration);
    }
}

// *** Remote Procedure Call ******************************************

var RPC = new function() {
    // IE does not define XMLHttpRequest by default, use a wrapper
    if (typeof XMLHttpRequest == 'undefined') {
        XMLHttpRequest = function() {
            try { return new ActiveXObject('Msxml2.XMLHTTP.6.0');} catch (e) { }
            try { return new ActiveXObject('Msxml2.XMLHTTP.3.0');} catch (e) { }
            try { return new ActiveXObject('Msxml2.XMLHTTP');    } catch (e) { }
            try { return new ActiveXObject('Microsoft.XMLHTTP'); } catch (e) { }
            throw new Error('');
        };
    }

    // internal attribute -----------------------
    var request = new XMLHttpRequest();
    // the unique identifier of this client
    var uid = 'Shell'+ (new Date()).getTime() + Math.floor(Math.random()*100);

    // public methods ---------------------------

    // get a file via Server
    this.getFile = function(file) {
        this.showInfo('Loading from '+file);
        if (timer) clearTimeout(timer); // terminate the previous more's
        request.open('GET', '/'+file, true);
        request.setRequestHeader('Cache-Control', 'no-cache');
        request.setRequestHeader('Client-Uid', uid);
        request.setRequestHeader('Content-Type', 'text/plain; charset=utf-8');
        request.onreadystatechange = function() {
            if (request.readyState == 4) { // XHR_LOADED
                TextBuffer.loadRun(request.responseText);
            }
        };
        request.send(null);
    }

    // Sending a Control Request by POST
    this.sendControl = function(cmd) {
        request.open('POST', '/rpc/control/', true);
        request.setRequestHeader('Cache-Control', 'no-cache'); // This line sometimes has nonfatal error, why??
        request.setRequestHeader('Client-Uid', uid);
        request.setRequestHeader('Content-Type', 'text/plain; charset=utf-8');
        request.setRequestHeader('Content-Length', content.length);
        request.onreadystatechange = function() {
            if (request.readyState == 4) { // XHR_LOADED
                Callback(request.responseText);
            }
        };
        request.send(cmd);
    }

    // contants and flags
    var DELAY_MIN = 100, // 100ms = 0.1s
        DELAY_MAX = 1000000; // 1000 seconds = 16.7 minute
    var delay = DELAY_MIN; // the delay interval for more
    var timer;             // the timer for more

    // public attributes ------------------------
    this.info = '';   // the info line
    this.more = true; // the more flag, if false, RPCMore() will not run
    this.capture = false; // the capture flag, to reflect Console logging

    // background process of more exchanges with server
    this.More = function(amount) {
        if (!this.more) return;
        if (amount != undefined) {
            delay = amount;
            if (delay > DELAY_MAX) delay = DELAY_MAX;
            if (delay < DELAY_MIN) delay = DELAY_MIN;
        }
        this.showInfo();
        if (timer) clearTimeout(timer); // terminate the previous more's
        timer = setTimeout(sendCommand, delay);
    }

    // show info
    this.showInfo = function(s) {
        if (s != undefined) this.info = s;
        _info.innerHTML = (this.info == '' ? '' : '['+this.info+'] ')+
            (this.capture ? 'log ['+this.capture + '] ' : '') +
             'echo ['+Command.echo+'], more ['+this.more+'], delay ['+delay/1000+'s]';
    }

    // Console Logging ----------------------------------------

    // send controls for logging, file == '!' means stop logging
    this.logging = function(file) {
        if (!file) file = prompt('File to log', 'logging.txt');
        if (file != null) {
            if (file == '!' && this.capture) {
                this.sendControl('!');
                this.capture = false;
            }
            else if (file != '!' && !this.capture) {
                this.sendControl(file);
                this.capture = file;
            }
            else {
                Printer.printError('Log request ignored.');
            }
            this.showInfo();
        }
    }

    // close this client on server
    this.exit = function() {
        RPC.more = false;
        if (timer) clearTimeout(timer); // terminate the previous more's
        this.sendControl('$');
        this.showInfo('No more Input - close or refresh this browser Tab.');
        Printer.printInfo(
            (function (x) { return (x > 6) && (x < 20) })(new Date().getHours()) ?
            'Goodbye.' : 'Goodnight.');
    }

    // private methods --------------------------

    // Sending a Command Request by POST (no arg for setTimeout)
    function sendCommand() {
        request.open('POST', '/rpc/command/', true);
        request.setRequestHeader('Cache-Control', 'no-cache');
        request.setRequestHeader('Client-Uid', uid);
        request.setRequestHeader('Content-Type', 'text/plain; charset=utf-8');
        //request.setRequestHeader('Content-Length', content.length); // will set by agent
        request.onreadystatechange = function() {
            if (request.readyState == 4) { // XHR_LOADED
                Callback(request.responseText);
            }
        };
        request.send(Command.get());
    }

    // process response
    function Callback(sHTML) {
        if (sHTML != '') {
            // apply post-processing for HTML
            sHTML = postProcess(sHTML);
            if (Command.echo) {
                // eliminate prefix same as last command
                var prefix = History.get()+'<br>';
                if (sHTML.indexOf(prefix) == 0) {
                    sHTML = sHTML.slice(prefix.length);
                }
            }
            showResponse(sHTML); // show the response
            // use continuation style to let response show
            setTimeout(function() {
                if (sHTML == '- ' || sHTML.indexOf('<br>- ')+6 == sHTML.length) {
                    TextBuffer.noWait(); // prompt is seen, no more waiting
                }
                RPC.More(DELAY_MIN); // reset delay
            }, 0); // just continuation style
        }
        else {
            // nothing from server, so check if there is input
            // if Command is empty, try to get lines from textBuffer
            if (Command.isEmpty()) TextBuffer.get(); // get lines from TextBuffer
            // if Command is now nonempty, run it
            if (!Command.isEmpty()) RPC.More(DELAY_MIN); // do inBuffer
            else RPC.More(delay*2); // extend delay by doubling
        }
    }
    // pretty print output s
    function showResponse(s) {
        // SML/HOL response output detection
        var k = s.indexOf('!');
        if (k != -1 && (s.indexOf('! Toplevel') == 0 || s.indexOf('! Uncaught') >= 0)) { // print error
            if (k != 0) { // k == 0 is starting with !
                k = s.indexOf('! Uncaught');
                if (k != -1) { // before ! is OK, after ! is error
                    var j = s.indexOf('Type inference failure'); // i.e. HOL unify error
                    if (j != -1) {
                        j = s.indexOf('unify failed');
                        Printer.printInfo(s.slice(0,j+12)); // 12 = 'unify failed'.length
                        s = s.slice(j+12);
                    }
                    else {
                        j = s.indexOf('&lt;&lt;HOL'); // i.e. <<HOL
                        if (j != -1) {
                            j = s.indexOf('&gt;&gt;', j); // i.e. detect >>
                            Printer.printInfo(s.slice(0,j+8)); // 8 = '&gt;&gt;'.length
                            s = s.slice(j+8);
                        }
                        else {
                            Printer.printOutput(s.slice(0,k));
                            s = s.slice(k);
                        }
                    }
                }
            }
            k = s.indexOf('- '); // the MOSML prompt
            if (k > 4) { // last prompt is not error
                Printer.printError(s.slice(0,k-4)); // 4 = '<br>'.length
                Printer.printOutput(s.slice(k));
            }
            else {
                Printer.printError(s);
            }
        }
        else {
            k = s.indexOf('&lt;&lt;HOL'); // i.e. <<HOL
            if (k != -1) {
                k = s.indexOf('&gt;&gt;', k); // i.e. detect >>
                Printer.printInfo(s.slice(0,k+8)); // 8 = '&gt;&gt;'.length
                Printer.printOutput(s.slice(k+8));
            }
            else {
                Printer.printOutput(s);
            }
        }
    }
    // post processing of response for HTML display
    function postProcess(s) {
        // String s may contain unicodes, but there is not need to convert them to &#...;
    /*
        // treat unicode: e.g. convert charCode 8743 to &#8743;
        var sb = []; // cannot skip this even when charset=UTF-8 (maybe due to innerHTML)
        for (var j = 0; j < s.length; j++) {
            if (s.charCodeAt(j) < 128) sb.push(s.charAt(j));
                     else sb.push('&#'+s.charCodeAt(j)+';');
        }
        s = sb.join('');
    */
        // replace characters for HTML display (order is important)
        return s.replace(/</g, '&lt;')    // angular bracket open
                .replace(/>/g, '&gt;')    // angular bracket close
                .replace(/\r\n/g, '<br>') // CRLF to break
                .replace(/\n\r/g, '<br>') // LFCR to break
                .replace(/\n/g, '<br>')   // LF to break
                .replace(/\r/g, '<br>')  // CR to break
                .replace(/\|-/g, '&#8866;')  // |- to right-tack (symbol implies, &#8866;)
                .replace(/-\|/g, '&#8867;')  // -| to left-tack (symbol back-implies, &#8867;)
             // (these are not visually appealing)
             // (to test: type in HOL shell input: (* symbol: &#8596; *)
             // .replace(/&lt;-&gt;/g, '&#8596;')  // <-> to double-arrow (symbol &#8596; )
             // .replace(/--&gt;/g, '&#8594;')  // --> to right-arrow (symbol &#8594;)
             // .replace(/&lt;--/g, '&#8592;') // <-- to left-arrow (symbol &#8592;)
                .replace(/--&gt;/g, '&#8212;&gt;')  // -- to one line (symbol &#8212;)
                .replace(/&lt;--/g, '&lt;&#8212;'); // -- to one line (symbol &#8212;)
    }
}

// *** Shell Commands *************************************************

var shellCommands = {

    // shortcut to clear screen
    cls: function() {
        this.clear();
    },

    // clear screen
    clear: function() {
        var CHILDREN_TO_PRESERVE = 0; // 3 for original header
        while (_out.childNodes[CHILDREN_TO_PRESERVE]) {
            _out.removeChild(_out.childNodes[CHILDREN_TO_PRESERVE]);
        }
    }
}

// *** Utilities ******************************************************

var Printer = new function() {

    // private method ---------------------------
    function print(s, type) {
        if ((s = String(s))) { // re-assign to ensure String, follow by non-null check
            var span = document.createElement("span");
            span.innerHTML = s; // so that s can be HTML
            span.className = type;
            _out.appendChild(span);
            return span;
        }
    }

    // public methods ---------------------------
    this.printInput = function(s) {
        print(s, "input");
    }
    this.printOutput = function(s) {
        print(s, "output");
    }
    this.printInfo = function(s) {
        print(s, "info");
    }
    this.printComment = function(s) {
        print(s, "comment");
    }
    this.printError = function(s) {
        print(s+'<br>', "error");
    }
}
