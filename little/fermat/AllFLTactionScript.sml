(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Combinatorial proof.                            *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by J. Petersen in 1872:

Take p elements from q with repetitions in all ways, that is, in q^p ways.
The q sets with elements all alike are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted in sets of p. Hence p divides q^p - q.

This is a combinatorial using Group action, via Fixed Points Congruence of Zp.
With Group action, there is no need to consider multicoloured necklaces --
they are hidden in the theory.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "AllFLTaction";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* Theorem: If n > 0, ((n - x) MOD n + x) MOD n = 0  for x < n. *)
val MOD_ADD_INV = store_thm(
  "MOD_ADD_INV",
  ``!n. 0 < n /\ x < n ==> (((n - x) MOD n + x) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  `((n - x) MOD n + x) MOD n = ((n-x) MOD n + x MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = ((n-x) + x) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  RW_TAC arith_ss []);

(* Theorem: Addition is associative in MOD: if x, y, z all < n,
            ((x + y) MOD n + z) MOD n = (x + (y + z) MOD n) MOD n. *)
val MOD_ADD_ASSOC = store_thm(
  "MOD_ADD_ASSOC",
  ``!n. 0 < n ==> (!x y z. x < n /\ y < n /\ z < n ==>
                 (((x + y) MOD n + z) MOD n = (x + (y + z) MOD n) MOD n))``,
  REPEAT STRIP_TAC THEN
  `((x + y) MOD n + z) MOD n = ((x + y) MOD n + z MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (((x + y) + z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  `_ = ((x + (y + z)) MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (x MOD n + (y + z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* Factor of prime and prime exponents.                                      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (the missing arithmeticTheory.EQ_MULT_RCANCEL)
   !m n p. (n * m = p * m) <=> (m = 0) \/ (n = p)  *)
val EQ_MULT_RCANCEL = store_thm(
  "EQ_MULT_RCANCEL",
  ``!m n p. (n * m = p * m) <=> (m = 0) \/ (n = p)``,
  METIS_TAC [arithmeticTheory.EQ_MULT_LCANCEL, arithmeticTheory.MULT_SYM]);

(* Theorem: [Factors of prime] For prime p, p = x*y ==> (x = 1 and y = p) or (x = p and y = 1) *)
val FACTORS_OF_PRIME = store_thm(
  "FACTORS_OF_PRIME",
  ``!p x y. prime p /\ (p = x*y) ==> ((x = 1) /\ (y = p)) \/ ((x = p) /\ (y = 1))``,
  REPEAT STRIP_TAC THEN
  `divides x p \/ divides y p` by METIS_TAC [dividesTheory.divides_def] THENL [
    `(x = 1) \/ (x = p)` by METIS_TAC [dividesTheory.prime_def] THEN1
    SRW_TAC [][] THEN
    `0 <> p` by METIS_TAC [dividesTheory.PRIME_POS, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [arithmeticTheory.EQ_MULT_LCANCEL, arithmeticTheory.MULT_RIGHT_1],
    `(y = 1) \/ (y = p)` by METIS_TAC [dividesTheory.prime_def] THEN1
    SRW_TAC [][] THEN
    `0 <> p` by METIS_TAC [dividesTheory.PRIME_POS, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [EQ_MULT_RCANCEL, arithmeticTheory.MULT_LEFT_1]
  ]);

(* Theorem: For a 1-1 map f: s -> s, s and (IMAGE f s) are of the same size. *)
val CARD_IMAGE = store_thm(
  "CARD_IMAGE",
  ``!s f. (!x y. (f x = f y) <=> (x = y)) /\ FINITE s ==> (CARD (IMAGE f s) = CARD s)``,
  Q_TAC SUFF_TAC
    `!f. (!x y. (f x = f y) = (x = y)) ==> !s. FINITE s ==> (CARD (IMAGE f s) = CARD s)`
    THEN1 METIS_TAC [] THEN
  GEN_TAC THEN STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][]);

(* Theorem: If !e IN s, CARD e = k, SIGMA CARD s = CARD s * k. *)
val SIGMA_CARD_CONSTANT = store_thm(
  "SIGMA_CARD_CONSTANT",
  ``!k s. FINITE s ==> (!e. e IN s ==> (CARD e = k)) ==> (SIGMA CARD s = k * (CARD s))``,
  STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][] THEN1 METIS_TAC [SUM_IMAGE_THM] THEN
  `CARD e = k` by SRW_TAC [][] THEN
  `SIGMA CARD (e INSERT s) = CARD e + SIGMA CARD (s DELETE e)` by SRW_TAC [][SUM_IMAGE_THM] THEN
  `s DELETE e = s` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `SUC (CARD s) = 1 + CARD s` by RW_TAC arith_ss [] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  RW_TAC arith_ss []);

(* Theorem: When the partitions are equal size of n, CARD s = n * CARD (partition of s) *)
(* Proof:
   FINITE (partition R s)               by FINITE_partition
   CARD s = SIGMA CARD (partition R s)  by partition_CARD
          = n  * CARD (partition R s)   by SIGMA_CARD_CONSTANT
*)
val equal_partition_CARD = store_thm(
  "equal_partition_CARD",
  ``!R s n. R equiv_on s /\ FINITE s /\
    (!t. t IN partition R s ==> (CARD t = n)) ==>
    (CARD s = n * CARD (partition R s))``,
  METIS_TAC [partition_CARD, FINITE_partition, SIGMA_CARD_CONSTANT]);

(* Note: There is LENGTH_NIL, but no LENGTH_NON_NIL *)
(* Theorem: a non-NIL list has positive LENGTH. *)
val LENGTH_NON_NIL = store_thm(
  "LENGTH_NON_NIL",
  ``!l. l <> [] ==> 0 < LENGTH l``,
  REPEAT STRIP_TAC THEN
  `LENGTH l <> 0` by SRW_TAC [][LENGTH_NIL] THEN
  RW_TAC arith_ss []);

(* DROP and TAKE *)

(* Note: There is TAKE_LENGTH_ID, but no DROP_LENGTH_NIL *)
(* Theorem: DROP the whole length of list is NIL. *)
val DROP_LENGTH_NIL = store_thm(
  "DROP_LENGTH_NIL",
  ``DROP (LENGTH l) l = []``,
  Induct_on `l` THEN SRW_TAC [][]);

(* Theorem: TAKE 1 of non-NIL list is unaffected by appending. *)
val TAKE_1_APPEND = store_thm(
  "TAKE_1_APPEND",
  ``!x y. x <> [] ==> (TAKE 1 (x ++ y) = TAKE 1 x)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 of non-NIL list is mildly affected by appending. *)
val DROP_1_APPEND = store_thm(
  "DROP_1_APPEND",
  ``!x y. x <> [] ==> (DROP 1 (x ++ y) = (DROP 1 x) ++ y)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 more time is once more of previous DROP. *)
val DROP_SUC = store_thm(
  "DROP_SUC",
  ``!n x. DROP (SUC n) x = DROP 1 (DROP n x)``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: TAKE 1 more time is a bit more than previous TAKE. *)
val TAKE_SUC = store_thm(
  "TAKE_SUC",
  ``!n x. TAKE (SUC n) x = (TAKE n x) ++ (TAKE 1 (DROP n x))``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: DROP is non-NIL when the number is less than LENGTH. *)
val DROP_NON_NIL = store_thm(
  "DROP_NON_NIL",
  ``!n l. n < LENGTH l ==> DROP n l <> []``,
  Induct_on `l` THEN SRW_TAC [][] THEN
  `n - 1 < LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Unit-Length Lists *)

(* Theorem: A unit-length list has its set a singleton. *)
val LIST_1_SET_SING = store_thm(
  "LIST_1_SET_SING",
  ``!l. (LENGTH l = 1) ==> SING (set l)``,
  SRW_TAC [][SING_DEF] THEN
  Cases_on `l` THEN1
   (`LENGTH [] = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `1 <> 0` by RW_TAC arith_ss [] THEN METIS_TAC []) THEN
  `SUC (LENGTH t) = 1` by METIS_TAC [LENGTH] THEN
  `LENGTH t = 0` by RW_TAC arith_ss [] THEN
  `t = []` by METIS_TAC [LENGTH_NIL] THEN
  FULL_SIMP_TAC (srw_ss())[] THEN METIS_TAC []);

(* Mono-list Theory: a mono-list is a list l with SING (set l) *)

(* Theorem: A non-mono-list has at least one element in tail that is distinct from its head. *)
val NON_MONO_TAIL_PROPERTY = store_thm(
  "NON_MONO_TAIL_PROPERTY",
  ``!l. ~SING (set (h::t)) ==> ?h'. h' IN set t /\ h' <> h``,
  SRW_TAC [][SING_INSERT] THEN
  `set t <> {}` by METIS_TAC [LIST_TO_SET_EQ_EMPTY] THEN
  `?e. e IN set t` by METIS_TAC [MEMBER_NOT_EMPTY] THEN
  FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
  METIS_TAC []);

(* Part 2: General Theory -------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Theory of Necklaces                                                       *)
(* ------------------------------------------------------------------------- *)

(* Define necklaces as lists of length n, i.e. with n beads *)
val Necklace_def = Define `
  Necklace n a = {l | (LENGTH l = n) /\ (set l) SUBSET (count a) }
`;

(* Theorem: Zero-length necklaces of whatever colors is the set of NIL. *)
val Necklace_0 = store_thm(
  "Necklace_0",
  ``!a. Necklace 0 a = {[]}``,
  SRW_TAC [boolSimps.CONJ_ss][Necklace_def, LENGTH_NIL]);

(* Theorem: A necklace of length n <> 0 has an order, i.e. non-NIL. *)
val NECKLACE_NONNIL = store_thm(
  "NECKLACE_NONNIL",
  ``!n a l. (0 < n) /\ (0 < a) /\ (l IN (Necklace n a)) ==> (l <> [])``,
  SRW_TAC [][Necklace_def] THEN
  `LENGTH l <> 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [LENGTH_NIL]);

(* Theorem: If l in (Necklace n a), LENGTH l = n  and colors in count a. *)
val NECKLACE_PROPERTY = store_thm(
  "NECKLACE_PROPERTY",
  ``!n a l. l IN (Necklace n a) ==> (LENGTH l = n) /\ (set l SUBSET count a)``,
  SRW_TAC [][Necklace_def]);

(* Theorem: Relate (Necklace (n+1) a) to (Necklace n a) for induction. *)
val Necklace_SUC = prove(
  ``!n a. Necklace (SUC n) a = IMAGE (\(e,l). e :: l) (count a CROSS Necklace n a)``,
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss]
    [Necklace_def, count_def, SUBSET_DEF, EXTENSION, pairTheory.EXISTS_PROD, LENGTH_CONS] THEN
  METIS_TAC []);

(* Theorem: The set of (Necklace n a) is finite. *)
val FINITE_Necklace = store_thm(
   "FINITE_Necklace",
  ``!n a. FINITE (Necklace n a)``,
  Induct_on `n` THEN SRW_TAC [][Necklace_0, Necklace_SUC]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (Necklace n a) = a^n.                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Size of (Necklace n a) = a^n. *)
val CARD_Necklace = store_thm(
  "CARD_Necklace",
  ``!n a. CARD (Necklace n a) = a ** n``,
  Induct_on `n` THEN
  SRW_TAC [][Necklace_0, FINITE_Necklace,
             Necklace_SUC, CARD_IMAGE, FINITE_COUNT, CARD_CROSS,
             pairTheory.FORALL_PROD, arithmeticTheory.EXP]);

(* ------------------------------------------------------------------------- *)
(* Monocoloured Necklace - necklace with a single color.                     *)
(* ------------------------------------------------------------------------- *)

(* Define mono-colored necklace *)
val monocoloured_def = Define`
  monocoloured n a = {l | (l IN Necklace n a) /\ (l <> [] ==> SING (set l)) }
`;

(* Theorem: A monocoloured necklace is indeed a Necklace. *)
val monocoloured_Necklace = store_thm(
  "monocoloured_Necklace",
  ``!n a l. l IN monocoloured n a ==> l IN Necklace n a``,
  SRW_TAC [][Necklace_def, monocoloured_def]);

(* Theorem: The monocoloured set is FINITE. *)
val FINITE_monocoloured = store_thm(
  "FINITE_monocoloured",
  ``!n a. FINITE (monocoloured n a)``,
  METIS_TAC [SUBSET_DEF, monocoloured_Necklace, SUBSET_FINITE, FINITE_Necklace]);

(* Theorem: Unit-length monocoloured set consists of singletons. *)
val monocoloured_1 = prove(
  ``!a. monocoloured 1 a = {[e] | e IN count a}``,
  SIMP_TAC bool_ss [monocoloured_def, Necklace_def, count_def, arithmeticTheory.ONE, LENGTH_CONS] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION]);

(* Theorem: Unit-length necklaces are monocoloured. *)
val necklace_1_monocoloured = store_thm(
  "necklace_1_monocoloured",
  ``!a. Necklace 1 a = monocoloured 1 a``,
  SRW_TAC [][Necklace_def, monocoloured_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [LIST_1_SET_SING]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (monocoloured n a) = a.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Relate (monocoloured (SUC n) a) to (monocoloured n a) for induction. *)
val monocoloured_SUC = prove(
  ``!n. 0 < n ==> (monocoloured (SUC n) a = IMAGE (\l. HD l :: l) (monocoloured n a))``,
  SRW_TAC [][EXTENSION, EQ_IMP_THM, monocoloured_def] THEN
  FULL_SIMP_TAC (srw_ss()) [Necklace_def, LENGTH_CONS, count_def, SUBSET_DEF, SING_DEF] THENL [
    Cases_on `l'` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
    `x <> [] /\ MEM h x /\ MEM h' x` by SRW_TAC [][] THEN
    `?k. !z . MEM z x <=> (z = k)` by SRW_TAC [][] THEN
    `h = h'` by METIS_TAC [] THEN SRW_TAC [][] THEN
    Q.EXISTS_TAC `x'` THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [] THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN METIS_TAC []
  ]);

(* Theorem: Size of (monocoloured n a) = a *)
val CARD_monocoloured = store_thm(
  "CARD_monocoloured",
  ``!n a. 0 < n ==> (CARD (monocoloured n a) = a)``,
  REPEAT STRIP_TAC THEN
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n` THENL [
    SRW_TAC [][monocoloured_1] THEN
    `{[e] | e < a} = IMAGE (\n. [n]) (count a)` by SRW_TAC [][EXTENSION] THEN
    SRW_TAC [][CARD_IMAGE],
    SRW_TAC [][monocoloured_SUC] THEN
    Q.MATCH_ABBREV_TAC `CARD (IMAGE f s) = a` THEN
    `!x y. (f x = f y) <=> (x = y)` by SRW_TAC [][EQ_IMP_THM, Abbr`f`] THEN
    `FINITE s` by SRW_TAC [][FINITE_monocoloured, Abbr`s`] THEN
    SRW_TAC [][CARD_IMAGE]
  ]);

(* ------------------------------------------------------------------------- *)
(* Multicoloured Necklace                                                    *)
(* ------------------------------------------------------------------------- *)

(* Define multi-colored necklace *)
val multicoloured_def = Define`
  multicoloured n a = (Necklace n a) DIFF (monocoloured n a)
`;

(* Theorem: multicoloured is a Necklace *)
val multicoloured_Necklace = store_thm(
  "multicoloured_Necklace",
  ``!n a l. l IN multicoloured n a ==> l IN Necklace n a``,
  SRW_TAC [][multicoloured_def, Necklace_def]);

(* Theorem: multicoloured set is FINITE *)
val FINITE_multicoloured = store_thm(
  "FINITE_multicoloured",
  ``!n a. FINITE (multicoloured n a)``,
  SRW_TAC [][multicoloured_def, FINITE_Necklace, FINITE_DIFF]);

(* Theorem: mutlicoloured 1 a = EMPTY *)
val multicoloured_1_EMPTY = store_thm(
  "multicoloured_1_EMPTY",
  ``!a. multicoloured 1 a = {}``,
  SRW_TAC [][multicoloured_def, Necklace_def, necklace_1_monocoloured]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (multicoloured n a) = a^n - a.                              *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A multicoloured necklace is not monocoloured. *)
val MULTI_MONO_DISJOINT = store_thm(
  "MULTI_MONO_DISJOINT",
  ``!n a. DISJOINT (multicoloured n a) (monocoloured n a)``,
  SIMP_TAC bool_ss [multicoloured_def, monocoloured_def, Necklace_def,
                    SING_DEF, SUBSET_DEF, DISJOINT_DEF] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION]);

(* Theorem: A necklace is either monocoloured or multicolored. *)
val MULTI_MONO_EXHAUST = store_thm(
  "MULTI_MONO_EXHAUST",
  ``!n a. Necklace n a = (multicoloured n a) UNION (monocoloured n a)``,
  SIMP_TAC bool_ss [multicoloured_def, monocoloured_def, Necklace_def,
                    SING_DEF, SUBSET_DEF, UNION_DEF] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION] THEN
  METIS_TAC []);

(* Theorem: Size of (multicoloured n a) = a^n - a *)
val CARD_multicoloured = store_thm(
  "CARD_multicoloured",
  ``!n a. 0 < n ==> (CARD (multicoloured n a) = a**n - a)``,
  REPEAT STRIP_TAC THEN
  `FINITE (multicoloured n a)` by SRW_TAC [][FINITE_multicoloured] THEN
  `FINITE (monocoloured n a)` by SRW_TAC [][FINITE_monocoloured] THEN
  `CARD (multicoloured n a) + CARD (monocoloured n a) =
      CARD ((multicoloured n a) UNION (monocoloured n a)) +
      CARD ((multicoloured n a) INTER (monocoloured n a))` by SRW_TAC [][CARD_UNION] THEN
  `CARD ((multicoloured n a) INTER (monocoloured n a)) = 0`
    by METIS_TAC [MULTI_MONO_DISJOINT, DISJOINT_DEF, CARD_EMPTY] THEN
  `CARD ((multicoloured n a) UNION (monocoloured n a)) = a**n`
    by METIS_TAC [MULTI_MONO_EXHAUST, CARD_Necklace] THEN
  `CARD (monocoloured n a) = a` by SRW_TAC [][CARD_monocoloured] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* Theory of Cycles                                                          *)
(* ------------------------------------------------------------------------- *)

(* Define cycle in view of action from Z_n to necklace n a *)

(* Cycle as nth iterate of rotate: DROP 1 ++ TAKE 1 *)
val cycle_def = Define `
  cycle n l = FUNPOW (\l. DROP 1 l ++ TAKE 1 l) n l
`;

(* Theorem: cycle 0 l = l *)
val CYCLE_0 = store_thm(
  "CYCLE_0",
  ``!l. cycle 0 l = l``,
  SRW_TAC [][cycle_def]);

(* Theorem: Cycle (n+1) of a list is cycle once more of cycle n list.
            cycle (SUC n) l = cycle 1 (cycle n l) *)
val CYCLE_SUC = prove(
  ``!l n. cycle (SUC n) l = cycle 1 (cycle n l)``,
  METIS_TAC [cycle_def, arithmeticTheory.FUNPOW_SUC, arithmeticTheory.FUNPOW_1]);

(* Theorem: Cycle is additive, (cycle n (cycle m l) = cycle (n+m) l  *)
val CYCLE_ADD = store_thm(
  "CYCLE_ADD",
  ``!l n m. cycle n (cycle m l) = cycle (n+m) l``,
  SRW_TAC [][cycle_def, arithmeticTheory.FUNPOW_ADD]);

(* Theorem: Cycle keeps LENGTH (of necklace), or LENGTH (cycle n l) = LENGTH l *)
val CYCLE_SAME_LENGTH = store_thm(
  "CYCLE_SAME_LENGTH",
  ``!l n. LENGTH (cycle n l) = LENGTH l``,
  `!l. LENGTH (cycle 1 l) = LENGTH l`
      by (SRW_TAC [][cycle_def] THEN Cases_on `l` THEN
          SRW_TAC [ARITH_ss][]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* Theorem: Cycle keep set (of colors), or set (cycle n l) = set l *)
val CYCLE_SAME_SET = store_thm(
  "CYCLE_SAME_SET",
  ``!l n. set (cycle n l) = set l``,
  `!l. set (cycle 1 l) = set l`
      by (Cases_on `l` THEN SRW_TAC [][cycle_def, EXTENSION, DISJ_COMM]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* ------------------------------------------------------------------------- *)
(* To show:  cycle x (cycle y l) = cycle ((x + y) MOD n) l, n = LENGTH l.    *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For n <= LENGTH l, cycle n l = DROP n l ++ TAKE n l *)
val CYCLE_EQ_DROP_TAKE = prove(
  ``!l n. n <= LENGTH l ==> (cycle n l = DROP n l ++ TAKE n l)``,
  REPEAT STRIP_TAC THEN
  Cases_on `l = []` THEN1 (
    `LENGTH l = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `n = 0` by RW_TAC arith_ss [] THEN
    SRW_TAC [][CYCLE_0]) THEN
  Induct_on `n` THEN1 SRW_TAC [][CYCLE_0] THEN
  SRW_TAC [][CYCLE_SUC] THEN
  `n < LENGTH l` by RW_TAC arith_ss [] THEN
  SRW_TAC [ARITH_ss][] THEN
  SRW_TAC [][cycle_def] THEN
  SRW_TAC [][DROP_SUC, TAKE_SUC, DROP_1_APPEND, TAKE_1_APPEND, DROP_NON_NIL]);

(* Theorem: Cycle through length gives original, or cycle (LENGTH l) l = l. *)
val CYCLE_BACK = store_thm(
  "CYCLE_BACK",
  ``!l. cycle (LENGTH l) l = l``,
  SRW_TAC [][CYCLE_EQ_DROP_TAKE, DROP_LENGTH_NIL, TAKE_LENGTH_ID]);

(* Theorem: cycle (n*LENGTH l) l = l. *)
val CYCLE_BACK_MULTIPLE = store_thm(
  "CYCLE_BACK_MULTIPLE",
  ``!l n. cycle (n * LENGTH l) l = l``,
  Induct_on `n` THEN
  SRW_TAC [][CYCLE_0] THEN
  `SUC n * LENGTH l = LENGTH l + n * LENGTH l` by RW_TAC arith_ss [arithmeticTheory.MULT_SUC] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK]);

(* Theorem: if l <> [], cycle n l = cycle (n MOD (LENGTH l)) l *)
val CYCLE_MOD_LENGTH = store_thm(
  "CYCLE_MOD_LENGTH",
  ``!l n. l <> [] ==> (cycle n l = cycle (n MOD (LENGTH l)) l)``,
  REPEAT STRIP_TAC THEN
  `0 < LENGTH l` by SRW_TAC [][LENGTH_NON_NIL] THEN
  `n = n DIV LENGTH l * LENGTH l + n MOD LENGTH l` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = n MOD LENGTH l + n DIV LENGTH l * LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK_MULTIPLE]);

(* Theorem: cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l *)
val CYCLE_ADDITION = store_thm(
  "CYCLE_ADDITION",
  ``!l x y. l <> [] ==> (cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l)``,
  METIS_TAC [CYCLE_ADD, CYCLE_MOD_LENGTH]);

(* Theorem: cycle 1 (h::t) = t ++ [h] *)
val CYCLE_1_EQ = store_thm(
  "CYCLE_1_EQ",
  ``!h t. cycle 1 (h::t) = t ++ [h]``,
  SRW_TAC [][cycle_def]);

(* Theorem: (t ++ [h] <> h::t) if (set t) has some element h' <> h. *)
val CYCLE_1_NEQ = store_thm(
  "CYCLE_1_NEQ",
  ``!h h' t. h' IN set t /\ h' <> h ==> (t ++ [h] <> h::t)``,
  SRW_TAC [][] THEN
  Induct_on `t` THEN SRW_TAC [][] THEN
  METIS_TAC []);

(* Theorem: [inverse of SING_HAS_CYCLE_1]
            !l. (cycle 1 l = l) ==> SING (set l)  *)
val CYCLE_1_NONEMPTY_MONO = store_thm(
  "CYCLE_1_NONEMPTY_MONO",
  ``!l. l <> [] /\ (cycle 1 l = l) ==> SING (set l)``,
  REPEAT STRIP_TAC THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* ------------------------------------------------------------------------- *)
(* Theory of Groups                                                          *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Group Definition.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Set up group type as a record
   A Group has:
   . a carrier set (set = function 'a -> bool, since MEM is a boolean function)
   . an identity element
   . an inverse function (unary operation)
   . a product function called multiplication (binary operation)
*)
val _ = Hol_datatype`
  group = <| carrier: 'a -> bool;
                  id: 'a;
                 inv: 'a -> 'a;
                mult: 'a -> 'a -> 'a
           |>`;

(* set overloading  *)
val _ = overload_on ("*", ``g.mult``);
val _ = overload_on ("|/", ``g.inv``);
val _ = overload_on ("#e", ``g.id``);

(* Group Definition:
   A Group is a set with elements of g of type 'a group, such that
   . g.id is in the carrier set (e in G)
   . (g.mult x y) is in the carrier set (closure: x*y in G)
   . (g.inv x) is in the carrier set (existence of inverse: x^-1 in G)
   . (g.mult (g.inv x) x) = (g.id)  (property of inverse and identity: (x^-1)*(x) = e)
   . (g.mult (g.mult x y) z) = (g.mult x (g.mult y z)) (associativity: (x*y)*z = x*(y*z))
*)
(* Define Group by predicate *)
val Group_def = Define`
  Group (g: 'a group) =
    #e IN g.carrier /\
    (!x y:: (g.carrier). x * y IN g.carrier) /\
    (!x:: (g.carrier). |/x IN g.carrier) /\
    (!x:: (g.carrier). #e * x = x) /\
    (!x:: (g.carrier). |/x * x = #e) /\
    (!x y z:: (g.carrier). (x * y) * z = x * (y * z))
  `;

(* Finite Group: a Group with a finite carrier set. *)
val FiniteGroup_def = Define`
  FiniteGroup (g: 'a group) =
    Group g /\ FINITE g.carrier
  `;

(* ------------------------------------------------------------------------- *)
(* Basic theorem from definition.                                            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Group identity is an element. *)
val group_id_carrier = store_thm(
  "group_id_carrier",
  ``!g. Group g ==> #e IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: Group inverse is an element. *)
val group_inv_carrier = store_thm(
  "group_inv_carrier",
  ``!g. Group g ==> !x :: (g.carrier). |/ x IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group closure] Group product is an element. *)
val group_mult_carrier = store_thm(
  "group_mult_carrier",
  ``!g. Group g ==> !x y :: (g.carrier). x * y IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left Identity] e x = x *)
val group_lid = store_thm(
  "group_lid",
  ``!g. Group g ==> !x :: (g.carrier). #e * x = x``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left inverse] x' x = e *)
val group_linv = store_thm(
  "group_linv",
  ``!g. Group g ==> !x :: (g.carrier). |/x * x = #e``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group associativity] (x y) z = x (y z) *)
val group_assoc = store_thm(
  "group_assoc",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y) * z = x * (y * z)``,
  SRW_TAC [][Group_def]);

(* Export rewrites after using Group_def, don't use Group_def anymore *)
val _ = export_rewrites ["group_id_carrier", "group_inv_carrier", "group_mult_carrier"];

(* ------------------------------------------------------------------------- *)
(* Theorems in basic Group Theory.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Group right inverse] x x' = e *)
val group_rinv = store_thm(
  "group_rinv",
  ``!g. Group g ==> !x :: (g.carrier). x * |/x = #e``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * |/x = #e * (x * |/x)`       by SRW_TAC [][group_lid] THEN
  `_ = (#e * x) * |/x`             by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x * x * |/x`    by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (|/x * x) * |/x`  by SRW_TAC [][GSYM group_assoc] THEN
  `_ = |/(|/x) * #e * |/x`         by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (#e * |/x)`       by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x`              by SRW_TAC [][group_lid] THEN
  `_ = #e`                         by SRW_TAC [][group_linv] THEN
  SRW_TAC [][]);

(* Theorem: [Group right identity] x e = x *)
val group_rid = store_thm(
  "group_rid",
  ``!g. Group g ==> !x :: (g.carrier). x * #e = x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * #e = x * (|/x * x)` by SRW_TAC [][group_linv] THEN
  `_ = (x * |/x) * x`      by SRW_TAC [][group_assoc] THEN
  `_ = #e * x`             by SRW_TAC [][group_rinv] THEN
  SRW_TAC [][group_lid]);

(* Theorem: [Solve left unknown] x y = z  <=> x = z y' *)
val group_lsolve = store_thm(
  "group_lsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (x = z * |/y)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_rid]);

(* Theorem: [Solve right unknown] x y = z  <=> y = x' z *)
val group_rsolve = store_thm(
  "group_rsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (y = |/x * z)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_lid]);

(* Theorem: [Left cancellation] x y = x z <=> y = z *)
val group_lcancel = store_thm(
  "group_lcancel",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y = x * z) = (y = z)``,
  METIS_TAC [group_inv_carrier, group_mult_carrier, group_assoc, group_rsolve, group_linv, group_lid]);

(* Theorem: [Left inverse unique] x y = e <=> x = y' *)
val group_linv_unique = store_thm(
  "group_linv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (x = |/y)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_lsolve, group_linv, group_lid]);

(* Theorem: [Right inverse unique] x y = e <=> y = x' *)
val group_rinv_unique = store_thm(
  "group_rinv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (y = |/x)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_rsolve, group_rinv, group_rid]);

(* Theorem: [Inverse of inverse] x'' = x *)
val group_inv_inv = store_thm(
  "group_inv_inv",
  ``!g. Group g ==> !x :: (g.carrier). |/(|/x) = x``,
  METIS_TAC [group_inv_carrier, group_rinv, group_linv_unique]);

(* Theorem: [Inverse of product] (x y)' = y' x' *)
val group_inv_mult = store_thm(
  "group_inv_mult",
  ``!g. Group g ==> !x y :: (g.carrier). |/(x * y) = |/y * |/x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `(x * y) * (|/y * |/x) = x * (y * |/y) * |/x` by SRW_TAC [][group_assoc] THEN
  `_ = #e`                                      by SRW_TAC [][group_rinv, group_rid] THEN
  POP_ASSUM MP_TAC THEN
  SRW_TAC [][group_rinv_unique]);

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Theory of Subgroups                                                       *)
(* ------------------------------------------------------------------------- *)

(* A Subgroup is a subset of a group that's a group itself, keeping id, inv, mult. *)
val Subgroup_def = Define `
  Subgroup h g =
    Group h /\ Group g /\
    h.carrier SUBSET g.carrier /\
    (h.id = #e) /\ (h.inv = |/) /\ (h.mult = g.mult)
`;

(* set overloading *)
val _ = overload_on ("<=", ``Subgroup``);

(* Theorem: subgroup property *)
val subgroup_property = store_thm(
  "subgroup_property",
  ``!g h. Group g /\ h <= g ==> (Group h) /\ (h.mult = g.mult) /\ (h.id = g.id) /\ (h.inv = g.inv)``,
  SRW_TAC [][Subgroup_def]);

(* ------------------------------------------------------------------------- *)
(* Cosets of a subgroup.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Define coset of subgroup with an element a. *)
val coset_def = Define `
  coset g X a = IMAGE (\z. g.mult a z) X
`;

(* Theorem: For x IN (coset h g.carrier a) ==> ?y IN g.carrier /\ a*y = x *)
val SUBGROUP_COSET_ELEMENT = store_thm(
  "SUBGROUP_COSET_ELEMENT",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN coset g h.carrier a ==> ?y. y IN h.carrier /\ (x = a*y)``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For all x IN h.carrier, a*x IN coset g h.carrier a. *)
val SUBGROUP_COSET_PROPERTY = store_thm(
  "SUBGROUP_COSET_PROPERTY",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN h.carrier ==> a*x IN coset g h.carrier a``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For h <= g, x IN coset g h.carrier x *)
val SUBGROUP_COSET_NONEMPTY = store_thm(
  "SUBGROUP_COSET_NONEMPTY",
  ``!g h x. Group g /\ h <= g /\ x IN g.carrier ==> x IN coset g h.carrier x``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rid, group_id_carrier]);

(* Theorem: For h <= g, y IN coset g h.carrier x ==> ?z IN h.carrier /\ x = y*z *)
val SUBGROUP_COSET_RELATE = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier /\ y IN coset g h.carrier x ==>
   ?z. z IN h.carrier /\ (x = y*z)``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rinv, group_rid, group_inv_carrier, group_assoc]);

(* Theorem: For h <= g, |/y * x in h.carrier ==> coset g h.carrier x = coset g h.carrier y. *)
val SUBGROUP_COSET_EQ1 = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   (|/y * x) IN h.carrier ==> (coset g h.carrier x = coset g h.carrier y)``,
  SRW_TAC [][coset_def, Subgroup_def, SUBSET_DEF, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THENL [
    `|/y * (x * z) = (|/y * x)* z` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    `|/y * (x * z) IN h.carrier` by METIS_TAC [group_mult_carrier] THEN
    `y * (|/y * (x * z)) = x * z` by METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier] THEN
    METIS_TAC [],
    `|/x * (y * z) = (|/x * y)* z` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    `|/(|/y * x) = |/x * y` by METIS_TAC [group_inv_mult, group_inv_inv, group_inv_carrier] THEN
    `|/x * (y * z) IN h.carrier` by METIS_TAC [group_mult_carrier, group_inv_carrier] THEN
    `x * (|/x * (y * z)) = y * z` by METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier] THEN
    METIS_TAC []
  ]);

(* Theorem: For h <= g, coset g h.carrier x = coset g h.carrier y ==> |/y * x in h.carrier. *)
val SUBGROUP_COSET_EQ2 = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   (coset g h.carrier x = coset g h.carrier y) ==> (|/y * x) IN h.carrier``,
  REPEAT STRIP_TAC THEN
  `y IN coset g h.carrier y` by SRW_TAC [][SUBGROUP_COSET_NONEMPTY] THEN
  `y IN coset g h.carrier x` by SRW_TAC [][] THEN
  `?z. z IN h.carrier /\ (x = y*z)` by SRW_TAC [][SUBGROUP_COSET_RELATE] THEN
  METIS_TAC [group_rsolve, Subgroup_def, SUBSET_DEF]);

(* Theorem: For h <= g, coset g h.carrier x = coset g h.carrier y iff |/y * x in h.carrier *)
val SUBGROUP_COSET_EQ = store_thm(
  "SUBGROUP_COSET_EQ",
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   ((coset g h.carrier x = coset g h.carrier y) <=> (|/y * x) IN h.carrier)``,
  METIS_TAC [SUBGROUP_COSET_EQ1, SUBGROUP_COSET_EQ2]);

(* Theorem: There is a bijection between subgroup and its cosets. *)
val subgroup_to_coset_bij = store_thm(
  "subgroup_to_coset_bij",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==>
   BIJ (\x. a*x) h.carrier (coset g h.carrier a)``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY, SUBGROUP_COSET_ELEMENT, group_lcancel, Subgroup_def, SUBSET_DEF]);

(* Theorem: All cosets of subgroup are of the same size as the subgroup *)
val CARD_subgroup_coset = store_thm(
  "CARD_subgroup_coset",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier /\ FINITE h.carrier ==>
   (CARD (coset g h.carrier a) = CARD (h.carrier))``,
  REPEAT STRIP_TAC THEN
  `BIJ (\x. a*x) h.carrier (coset g h.carrier a)` by SRW_TAC [][subgroup_to_coset_bij] THEN
  `FINITE (coset g h.carrier a)` by METIS_TAC [coset_def, IMAGE_FINITE] THEN
  METIS_TAC [FINITE_BIJ_CARD_EQ]);

(* ------------------------------------------------------------------------- *)
(* Langrange's Theorem by Subgroups and Cosets                               *)
(* ------------------------------------------------------------------------- *)

(* Define b ~ a  when  b IN (coset g h.carrier a) *)
val inCoset_def = Define `
  inCoset g h a b = b IN (coset g h.carrier a)
`;

(* Theorem: inCoset is Reflexive. *)
val INCOSET_REFL = store_thm(
  "INCOSET_REFL",
  ``!g h. Group g /\ h <= g ==> !a. a IN g.carrier ==> inCoset g h a a``,
  METIS_TAC [inCoset_def, SUBGROUP_COSET_NONEMPTY]);

(* Theorem: inCoset is Symmetric. *)
val INCOSET_SYM = store_thm(
  "INCOSET_SYM",
  ``!g h. Group g /\ h <= g ==>
   !a b. a IN g.carrier /\ b IN g.carrier
       ==> (inCoset g h a b ==> inCoset g h b a)``,
  SRW_TAC [][inCoset_def] THEN
  `?z. z IN h.carrier /\ (b = a * z)` by METIS_TAC [SUBGROUP_COSET_ELEMENT] THEN
  `|/ z IN h.carrier` by METIS_TAC [Subgroup_def, group_inv_carrier] THEN
  `z IN g.carrier /\ |/ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `b * |/ z = (a * z) * (|/ z)` by SRW_TAC [][] THEN
  `_ = a * (z * |/ z)` by SRW_TAC [][group_assoc] THEN
  `_ = a` by SRW_TAC [][group_rinv, group_rid] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is Transitive. *)
val INCOSET_TRANS = store_thm(
  "INCOSET_TRANS",
  ``!g h. Group g /\ h <= g ==>
   !a b c. a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
       ==> (inCoset g h a b /\ inCoset g h b c ==> inCoset g h a c)``,
  SRW_TAC [][inCoset_def] THEN
  `?y. y IN h.carrier /\ (b = a * y) /\ ?z. z IN h.carrier /\ (c = b * z)` by SRW_TAC [][SUBGROUP_COSET_ELEMENT] THEN
  `y IN g.carrier /\ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `c = (a * y) * z` by SRW_TAC [][] THEN
  `_ = a * (y * z)` by SRW_TAC [][group_assoc] THEN
  `y*z IN h.carrier` by METIS_TAC [subgroup_property, group_mult_carrier] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is an equivalence relation. *)
val INCOSET_EQUIV_ON_CARRIER = store_thm(
  "INCOSET_EQUIV_ON_CARRIER",
  ``!g h. Group g /\ h <= g ==> inCoset g h equiv_on g.carrier``,
  SRW_TAC [][equiv_on_def] THEN METIS_TAC [INCOSET_REFL, INCOSET_SYM, INCOSET_TRANS]);

(* Define coset partitions of g.carrier by inCoset g h. *)
val CosetPartition_def = Define `
  CosetPartition g h = partition (inCoset g h) g.carrier
`;

(* Theorem: For FINITE Group g, h <= g, compute CARD g.carrier by partition. *)
val CARD_CARRIER_BY_COSET_PARTITION = store_thm(
  "CARD_CARRIER_BY_COSET_PARTITION",
  ``!g h. Group g /\ h <= g /\ FINITE g.carrier ==>
    (CARD g.carrier = SIGMA CARD (CosetPartition g h))``,
  METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, partition_CARD]);

(* Theorem: Elements in CosetPartition are cosets of some a In g.carrier *)
val COSET_PARTITION_ELEMENT = store_thm(
  "COSET_PARTITION_ELEMENT",
  ``!g h. Group g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> ?a. a IN g.carrier /\ (e = coset g h.carrier a)``,
  SRW_TAC [][CosetPartition_def, inCoset_def, partition_def] THEN
  Q.EXISTS_TAC `x` THEN
  SRW_TAC [][coset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [Subgroup_def, SUBSET_DEF, group_mult_carrier]);

(* Theorem: For FINITE group, CARD element in CosetPartiton = CARD subgroup. *)
val CARD_COSET_PARTITION_ELEMENT = store_thm(
  "CARD_COSET_PARTITION_ELEMENT",
  ``!g h. FiniteGroup g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> (CARD e = CARD h.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?a. a IN g.carrier /\ (e = coset g h.carrier a)` by SRW_TAC [][COSET_PARTITION_ELEMENT] THEN
  `FINITE h.carrier` by METIS_TAC [Subgroup_def, SUBSET_FINITE] THEN
  METIS_TAC [CARD_subgroup_coset]);

(* Theorem: (Lagrange Identity) For FINITE Group g and subgroup h,
            (size of group) = (size of subgroup) * (size of coset partition). *)
val LAGRANGE_IDENTITY = store_thm(
  "LAGRANGE_IDENTITY",
  ``!g h. FiniteGroup g /\ h <= g ==>
   (CARD g.carrier = CARD h.carrier * CARD (CosetPartition g h))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `FINITE (CosetPartition g h)` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  METIS_TAC [CARD_CARRIER_BY_COSET_PARTITION, SIGMA_CARD_CONSTANT, CARD_COSET_PARTITION_ELEMENT, FiniteGroup_def]);

(* ------------------------------------------------------------------------- *)
(* Theory of Group action                                                    *)
(* ------------------------------------------------------------------------- *)

(* An action from group G to a set X is a map f: GxX -> X such that
   (0)   [is a map] f (x in G)(z in X) in X
   (1)  [id action] f (e in G)(z in X) = z
   (2) [composable] f (x in G)(f (y in G)(z in X)) =
                    f ((mult in G)(x in G)(y in G))(z in X)
*)
val action_def = Define `
   action f g X = !z. z IN X ==>
     (!x :: (g.carrier). f x z IN X) /\
     (f #e z = z) /\
     (!x y :: (g.carrier). f x (f y z) = f (x * y) z)
`;

(* Theorem: For action f g X /\ x IN X, !a IN g.carrier, f a x IN X  *)
val ACTION_CLOSURE = store_thm(
  "ACTION_CLOSURE",
  ``!f g X x a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier ==> f a x IN X``,
  SRW_TAC [][action_def]);

(* Theorem: For action f g X /\ x IN X, !a,b IN g.carrier, f a (f b x) = f (a*b) x  *)
val ACTION_COMPOSE = store_thm(
  "ACTION_COMPOSE",
  ``!f g X x a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier /\ b IN g.carrier ==> (f a (f b x) = f (a*b) x)``,
  SRW_TAC [][action_def]);

(* Theorem: For action f g X /\ x IN X, f #e x = x  *)
val ACTION_ID = store_thm(
  "ACTION_ID",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==> (f #e x = x)``,
  SRW_TAC [][action_def]);
(* This is essentially REACH_REFL *)

(* Theorem: For action f g X /\ x IN X /\ y IN X, !a IN g.carrier, f a x = y ==> f |/a y = x.  *)
val ACTION_REVERSE = store_thm(
  "ACTION_REVERSE",
  ``!f g X x y a. Group g /\ action f g X /\ x IN X /\ y IN X /\ a IN g.carrier ==>
   (f a x = y) ==> (f (|/ a) y = x)``,
  REPEAT STRIP_TAC THEN
  `|/ a IN g.carrier` by SRW_TAC [][group_inv_carrier] THEN
  `f (|/ a) y = f (|/ a) (f a x)` by SRW_TAC [][] THEN
  `_ = f (|/a * a) x` by METIS_TAC [ACTION_COMPOSE, group_mult_carrier] THEN
  METIS_TAC [group_linv, ACTION_ID]);
(* This is essentially REACH_SYM *)

(* Theorem: For action f g X /\ x IN X /\ y IN X, z IN X,
            if f a x = y /\ f b y = z, then f (b*a) x = z.  *)
val ACTION_TRANS = store_thm(
  "ACTION_TRANS",
  ``!f g X x y a. Group g /\ action f g X /\ x IN X /\ y IN X /\ z IN X /\
      a IN g.carrier /\ b IN g.carrier ==>
   (f a x = y) /\ (f b y = z) ==> (f (b * a) x = z)``,
  METIS_TAC [ACTION_COMPOSE, group_mult_carrier]);
(* This is essentially REACH_TRANS *)

(* ------------------------------------------------------------------------- *)
(* Group action induces an equivalence relation.                             *)
(* ------------------------------------------------------------------------- *)

(* Define reach to relate two action points x y in X *)
val reach_def = Define`
  reach f g x y = ?z. z IN g.carrier /\ (f z x = y)
`;

(* Theorem: [Reach is Reflexive] !x in X, reach f g x x.  *)
val REACH_REFL = store_thm(
  "REACH_REFL",
  ``!f g X. Group g /\ action f g X ==> (!x. x IN X ==> reach f g x x)``,
  METIS_TAC [reach_def, ACTION_ID, group_id_carrier]);

(* Theorem: [Reach is Symmetric] !x y in X, reach f g x y ==> reach f g y x. *)
val REACH_SYM = store_thm(
  "REACH_SYM",
  ``!f g X. Group g /\ action f g X ==>
    (!x y. x IN X /\ y IN X ==> reach f g x y ==> reach f g y x)``,
  METIS_TAC [reach_def, ACTION_REVERSE, group_inv_carrier, group_linv]);

(* Theorem: [Reach is Transitive] !x y z in X, reach f g x y /\ reach f g y z ==> reach f g x z. *)
val REACH_TRANS = store_thm(
  "REACH_TRANS",
  ``!f g X. Group g /\ action f g X ==>
    (!x y z. x IN X /\ y IN X /\ z IN X ==> reach f g x y /\ reach f g y z ==> reach f g x z)``,
  SRW_TAC [][reach_def] THEN
  Q.EXISTS_TAC `z''*z'` THEN
  METIS_TAC [ACTION_TRANS, group_mult_carrier]);

(* Theorem: Reach is an equivalence relation on target set X. *)
val REACH_EQUIV_ON_TARGET = store_thm(
  "REACH_EQUIV_ON_TARGET",
  ``!f g X. Group g /\ action f g X ==> reach f g equiv_on X``,
  SRW_TAC [][equiv_on_def] THEN
  METIS_TAC [REACH_REFL, REACH_SYM, REACH_TRANS]);

(* ------------------------------------------------------------------------- *)
(* Partition by Group action.                                                *)
(* ------------------------------------------------------------------------- *)

(* Define partitions of Target set X by reach f g. *)
val TargetPartition_def = Define `
  TargetPartition f g X = partition (reach f g) X
`;

(* Theorem: For e IN (TargetPartition f g X), e <> EMPTY *)
val TARGET_PARTITION_ELEMENT_NONEMPTY = store_thm(
  "TARGET_PARTITION_ELEMENT_NONEMPTY",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> e <> {}``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, EMPTY_NOT_IN_partition]);

(* Theorem: Elements in Element of TargetPartition are in X. *)
val TARGET_PARTITION_ELEMENT_ELEMENT = store_thm(
  "TARGET_PARTITION_ELEMENT_ELEMENT",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> !a. a IN e ==> a IN X``,
  SRW_TAC [][TargetPartition_def, partition_def] THEN
  FULL_SIMP_TAC (srw_ss())[]);

(* Theorem: TargetPartition elements are subset of target *)
val TARGET_PARTITION_ELEMENT_SUBSET_TARGET = store_thm(
  "TARGET_PARTITION_ELEMENT_SUBSET_TARGET",
  ``!f g X. Group g /\ action f g X ==>
   !e. e IN TargetPartition f g X ==> e SUBSET X``,
  METIS_TAC [TARGET_PARTITION_ELEMENT_ELEMENT, SUBSET_DEF]);

(* ------------------------------------------------------------------------- *)
(* Orbits as equivalence classes.                                            *)
(* ------------------------------------------------------------------------- *)

(* Orbit of action: those x in X that can be reached by a in X *)
val orbit_def = Define`
  orbit f g X a = {x | x IN X /\ reach f g a x }
`;

(* Theorem: y IN (orbit f g X x) ==> y IN X /\ reach f g x y *)
val ORBIT_PROPERTY = store_thm(
  "ORBIT_PROPERTY",
  ``!f g X x y. y IN orbit f g X x ==> y IN X /\ reach f g x y``,
  SRW_TAC [][orbit_def]);

(* Theorem: (orbit f g X x) = IMAGE (\a. f a x) g.carrier *)
val ORBIT_DESCRIPTION = store_thm(
  "ORBIT_DESCRIPTION",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==> (orbit f g X x = {f a x | a IN g.carrier})``,
  SRW_TAC [][action_def, orbit_def, reach_def, EXTENSION, EQ_IMP_THM] THEN METIS_TAC []);

(* Theorem: if a in g.carrier, then f x a in (orbit f g X a). *)
val ORBIT_HAS_ACTION_ELEMENT = store_thm(
  "ORBIT_HAS_ACTION_ELEMENT",
  ``!f g X x a. action f g X /\ x IN X /\ a IN g.carrier ==> f a x IN (orbit f g X x)``,
  SRW_TAC [][orbit_def, reach_def] THEN
  METIS_TAC [action_def]);

(* Theorem: action f g X ==> x IN orbit f g X x *)
val ACTION_ORBIT_HAS_ITSELF = store_thm(
  "ACTION_ORBIT_HAS_ITSELF",
  ``!f g X. Group g /\ action f g X /\ x IN X ==> x IN orbit f g X x``,
  SRW_TAC [][orbit_def] THEN
  METIS_TAC [REACH_REFL]);

(* Theorem: orbits are subsets of target set X *)
val ORBIT_SUBSET_TARGET = store_thm(
  "ORBIT_SUBSET_TARGET",
  ``!f g X a. action f g X /\ a IN X ==> orbit f g X a SUBSET X``,
  SRW_TAC [][orbit_def, SUBSET_DEF]);

(* Theorem: Elements of TargetPartition are orbits of its own element.
            !e IN TargetPartition f g X ==> !a IN e. e = orbit f g X a *)
val TARGET_PARTITION_ELEMENT_IS_ORBIT = store_thm(
  "TARGET_PARTITION_ELEMENT_IS_ORBIT",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> !a. a IN e ==> (e = orbit f g X a)``,
  SRW_TAC [][TargetPartition_def, partition_def, orbit_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [REACH_SYM, REACH_TRANS]);

(* Theorem: If for all x IN X, (orbit f g X x) = n, then n divides CARD X. *)
val EQUAL_SIZE_ORBITS_PROPERTY = store_thm(
  "EQUAL_SIZE_ORBITS_PROPERTY",
  ``!f g X n. Group g /\ action f g X /\ FINITE X /\
    (!x. x IN X ==> (CARD (orbit f g X x) = n)) ==> divides n (CARD X)``,
  REPEAT STRIP_TAC THEN
  `!e. e IN TargetPartition f g X ==> !x. x IN e ==> (e = orbit f g X x)` by SRW_TAC [][TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `!e. e IN TargetPartition f g X ==> (CARD e = n)` by SRW_TAC [][] THENL [
    `?y. y IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
    METIS_TAC [TARGET_PARTITION_ELEMENT_SUBSET_TARGET, SUBSET_DEF],
    `CARD X = n * CARD (partition (reach f g) X)` by METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, equal_partition_CARD] THEN
    METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_SYM]
  ]);

(* ------------------------------------------------------------------------- *)
(* Stabilizer subgroups.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Stabilizer of action: for a in X, stabilizer of a = {x in G | f x a = a} *)
val stabilizer_def = Define`
  stabilizer f g a = {x | x IN g.carrier /\ (f x a = a) }
`;

(* Theorem: x IN stabilizer f g a ==> x IN g.carrier and f x a = a. *)
val STABILIZER_PROPERTY = store_thm(
  "STABILIZER_PROPERTY",
  ``!f g X a. action f g X /\ a IN X ==> !x. x IN stabilizer f g a ==> x IN g.carrier /\ (f x a = a)``,
  SRW_TAC [][stabilizer_def]);

(* Define the Stabilizer Group of stabilizer set. *)
val StabilizerGroup_def = Define`
  StabilizerGroup f g a =
    <| carrier := stabilizer f g a;
            id := g.id;
           inv := |/ ;
          mult := g.mult
     |>`;

(* Theorem: If g is a Group, f g X is an action, StabilizerGroup f g a is a Group *)
val StabilizerGroup_group = store_thm(
  "StabilizerGroup_group",
  ``!f g X a. action f g X /\ a IN X /\ Group g ==> Group (StabilizerGroup f g a)``,
  SRW_TAC [][Group_def, StabilizerGroup_def, stabilizer_def, action_def, RES_FORALL_THM] THEN METIS_TAC []);

(* Theorem: The stabilizer is a subset of g.carrier *)
val STABILIZER_SUBSET = store_thm(
  "STABILIZER_SUBSET",
  ``!f g X. action f g X /\ a IN X ==> (stabilizer f g a) SUBSET g.carrier``,
  SRW_TAC [][stabilizer_def, SUBSET_DEF]);

(* Theorem: If g is Group, f g X is an action, then stabilizer is a subgroup of g *)
val StabilizerGroup_subgroup = store_thm(
  "StabilizerGroup_subgroup",
  ``!f g X. action f g X /\ a IN X /\ Group g ==> Subgroup (StabilizerGroup f g a) g``,
  SRW_TAC [][Subgroup_def, StabilizerGroup_def] THEN
  METIS_TAC [StabilizerGroup_def, StabilizerGroup_group, STABILIZER_SUBSET]);

(* ------------------------------------------------------------------------- *)
(* Orbit-Stabilizer Theorem.                                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The map from orbit to coset of stabilizer is well-defined. *)
val ORBIT_STABILIZER_MAP_WD = store_thm(
  "ORBIT_STABILIZER_MAP_WD",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==>
   !x y. x IN g.carrier /\ y IN g.carrier /\ (f x a = f y a) ==>
   (coset g (stabilizer f g a) x = coset g (stabilizer f g a) y)``,
  REPEAT STRIP_TAC THEN
  `StabilizerGroup f g a <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  FULL_SIMP_TAC (srw_ss()) [action_def] THEN
  `f (|/y * x) a = f (|/y) (f x a)` by SRW_TAC [][group_inv_carrier] THEN
  `_ = f (|/y) (f y a)` by ASM_REWRITE_TAC [] THEN
  `_ = f (|/y * y) a` by SRW_TAC [][group_inv_carrier] THEN
  `_ = f #e a` by SRW_TAC [][group_linv] THEN
  `_ = a` by SRW_TAC [][] THEN
  `(|/y * x) IN g.carrier` by SRW_TAC [][group_inv_carrier, group_mult_carrier] THEN
  `(|/y * x) IN (stabilizer f g a)` by SRW_TAC [][stabilizer_def] THEN
  METIS_TAC [SUBGROUP_COSET_EQ]);

(* Theorem: The map from orbit to coset of stabilizer is injective. *)
val ORBIT_STABILIZER_MAP_UQ = store_thm(
  "ORBIT_STABILIZER_MAP_UQ",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==>
   !x y. x IN g.carrier /\ y IN g.carrier /\
   (coset g (stabilizer f g a) x = coset g (stabilizer f g a) y) ==> (f x a = f y a)``,
  REPEAT STRIP_TAC THEN
  `StabilizerGroup f g a <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  `(|/y * x) IN (stabilizer f g a)` by METIS_TAC [SUBGROUP_COSET_EQ] THEN
  `f (|/y * x) a = a` by FULL_SIMP_TAC (srw_ss()) [stabilizer_def] THEN
  `|/y * x IN g.carrier` by SRW_TAC [][group_inv_carrier, group_mult_carrier] THEN
  `f x a = f (#e*x) a` by SRW_TAC [][group_lid] THEN
  `_ = f ((y* |/y)*x) a` by SRW_TAC [][group_rinv] THEN
  `_ = f (y*(|/y*x)) a` by SRW_TAC [][group_assoc, group_inv_carrier] THEN
  `_ = f y (f (|/y * x) a)` by FULL_SIMP_TAC (srw_ss()) [action_def] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)
(* Convert reach definition into a function                                  *)
(* ------------------------------------------------------------------------- *)

(* Existence of act_by:  reach x y ==> ?a. a IN g.carrier /\ f a x = y *)
val lemma = prove(
  ``!f g x y. ?a. reach f g x y ==> a IN g.carrier /\ (f a x = y)``,
  METIS_TAC [reach_def]);

val ACT_BY_DEF = new_specification(
    "ACT_BY_DEF",
    ["ACT_BY"],
    SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* Theorem: Points of (orbit x) and cosets of (stabilizer x) are one-to-one. *)
val ORBIT_STABILIZER_COSETS_BIJ = store_thm(
  "ORBIT_STABILIZER_COSETS_BIJ",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==>
   BIJ (\z.  coset g (stabilizer f g x) (ACT_BY f g x z))
       (orbit f g X x)
       (CosetPartition g (StabilizerGroup f g x))``,
  SRW_TAC [][CosetPartition_def, partition_def, inCoset_def, StabilizerGroup_def, BIJ_DEF, INJ_DEF, SURJ_DEF] THENL [
    `reach f g x z` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.EXISTS_TAC `ACT_BY f g x z` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    `x' IN IMAGE (\z'. ACT_BY f g x z * z') (stabilizer f g x)` by METIS_TAC [coset_def] THEN
    `?z'. z' IN (stabilizer f g x) /\ (x' = ACT_BY f g x z * z')` by METIS_TAC [IN_IMAGE] THEN
    METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier],
    `reach f g x z /\ reach f g x z'` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier /\ ACT_BY f g x z' IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    `f (ACT_BY f g x z) x = f (ACT_BY f g x z') x` by METIS_TAC [ORBIT_STABILIZER_MAP_UQ] THEN
    METIS_TAC [ACT_BY_DEF],
    `reach f g x z` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.EXISTS_TAC `ACT_BY f g x z` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    `x' IN IMAGE (\z'. ACT_BY f g x z * z') (stabilizer f g x)` by METIS_TAC [coset_def] THEN
    `?z'. z' IN (stabilizer f g x) /\ (x' = ACT_BY f g x z * z')` by METIS_TAC [IN_IMAGE] THEN
    METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier],
    Q.EXISTS_TAC `f x'' x` THEN
    SRW_TAC [][] THENL [
      `reach f g x (f x'' x)` by METIS_TAC [reach_def] THEN
      `f x'' x IN X` by METIS_TAC [ACTION_CLOSURE] THEN
      SRW_TAC [][orbit_def],
      `reach f g x (f x'' x)` by METIS_TAC [reach_def] THEN
      `ACT_BY f g x (f x'' x) IN g.carrier /\ (f (ACT_BY f g x (f x'' x)) x = (f x'' x))` by SRW_TAC [][ACT_BY_DEF] THEN
      `coset g (stabilizer f g x) (ACT_BY f g x (f x'' x)) = coset g (stabilizer f g x) x''` by METIS_TAC [ORBIT_STABILIZER_MAP_WD] THEN
      SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
      `x' IN IMAGE (\z. x'' * z) (stabilizer f g x)` by METIS_TAC [coset_def] THEN
      `?z. z IN (stabilizer f g x) /\ (x' = x'' * z)` by METIS_TAC [IN_IMAGE] THEN
      METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier]
    ]
  ]);

(* Theorem: [Orbit-Stabilizer Theorem] CARD G = CARD (orbit x) * CARD (stabilizer x) *)
val ORBIT_STABILIZER_THEOREM = store_thm(
  "ORBIT_STABILIZER_THEOREM",
  ``!f g X x. FiniteGroup g /\ action f g X /\ x IN X /\ FINITE X ==>
   (CARD g.carrier = CARD (orbit f g X x) * CARD (stabilizer f g x))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `StabilizerGroup f g x <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g x).carrier = stabilizer f g x` by SRW_TAC [][StabilizerGroup_def] THEN
  `FINITE (CosetPartition g (StabilizerGroup f g x))` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  `FINITE (orbit f g X x)` by METIS_TAC [ORBIT_DESCRIPTION, IMAGE_DEF, IMAGE_FINITE] THEN
  `CARD (CosetPartition g (StabilizerGroup f g x)) = CARD (orbit f g X x)` by METIS_TAC [ORBIT_STABILIZER_COSETS_BIJ, FINITE_BIJ_CARD_EQ] THEN
  `CARD g.carrier = CARD (stabilizer f g x) * CARD (CosetPartition g (StabilizerGroup f g x))` by METIS_TAC [FiniteGroup_def, LAGRANGE_IDENTITY] THEN
  RW_TAC arith_ss []);

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof via Group action.                                     *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The Group Zn = Addition Modulo n, for n > 0.                              *)
(* ------------------------------------------------------------------------- *)

(* Define Zn = Addition Modulo n Group *)
val Z_def = Define`
  Z n =
    <| carrier := count n;
            id := 0;
           inv := (\i. (n - i) MOD n);  (* so that inv 0 = 0 *)
          mult := (\i j. (i + j) MOD n)
     |>`;

(* Theorem: Zn carrier is finite *)
val FINITE_ZN_carrier = store_thm(
  "FINITE_ZN_carrier",
  ``!n. FINITE (Z n).carrier``,
  SRW_TAC [][Z_def, FINITE_COUNT]);

(* Theorem: CARD (Zn carrier) = n *)
val CARD_ZN_carrier = store_thm(
  "CARD_ZN_carrier",
  ``!n. CARD (Z n).carrier = n``,
  SRW_TAC [][Z_def, CARD_COUNT]);

(* Theorem: Zn is a group if n > 0. *)
val ZN_group = store_thm(
  "ZN_group",
  ``!n. 0 < n ==> Group (Z n)``,
  SRW_TAC [][Z_def, Group_def, RES_FORALL_THM, MOD_ADD_INV, MOD_ADD_ASSOC]);

(* Theorem: Zn is a FiniteGroup if n > 0. *)
val ZN_finite_group = store_thm(
  "ZN_finite_group",
  ``!n. 0 < n ==> FiniteGroup (Z n)``,
  METIS_TAC [ZN_group, FINITE_ZN_carrier, FiniteGroup_def]);

(* Theorem: If l is a Necklace, cycle n l is also a Necklace. *)
val NECKLACE_CYCLE = store_thm(
  "NECKLACE_CYCLE",
  ``!l. l IN Necklace n a ==> !k. (cycle k l) IN Necklace n a``,
  SRW_TAC [][Necklace_def, CYCLE_SAME_LENGTH, CYCLE_SAME_SET]);

(* Theorem: [Closure of cycle for multicoloured necklaces]
            If l is a multicoloured necklace, cycle n l is also multicolored. *)
val multicoloured_CYCLE = store_thm(
  "multicoloured_CYCLE",
  ``!l n a. 0 < n /\ 0 < a /\ l IN multicoloured n a ==> !k. (cycle k l) IN multicoloured n a``,
  REPEAT STRIP_TAC THEN
  `l IN (Necklace n a)` by SRW_TAC [][multicoloured_Necklace] THEN
  `l <> []` by METIS_TAC [NECKLACE_NONNIL] THEN
  FULL_SIMP_TAC (srw_ss()) [multicoloured_def, monocoloured_def, NECKLACE_CYCLE, CYCLE_SAME_SET] THEN
  METIS_TAC [LENGTH_NIL, CYCLE_SAME_LENGTH]);

(* Theorem: CYCLE is an action on multicoloured necklaces *)
val cycle_action_on_multicoloured = store_thm(
  "cycle_action_on_multicoloured",
  ``!n a. 0 < n /\ 0 < a ==> action cycle (Z n) (multicoloured n a)``,
  SRW_TAC [][action_def, Z_def, RES_FORALL_THM] THEN1
  METIS_TAC [multicoloured_CYCLE] THEN1
  METIS_TAC [CYCLE_0] THEN
  METIS_TAC [CYCLE_ADDITION, multicoloured_Necklace, NECKLACE_NONNIL, NECKLACE_PROPERTY]);

(* Theorem: !n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==> cycle 1 l <> l *)
val multicoloured_NOT_CYCLE_1 = store_thm(
  "multicoloured_NOT_CYCLE_1",
  ``!n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==> cycle 1 l <> l``,
  SRW_TAC [][multicoloured_def, monocoloured_def] THEN
  METIS_TAC [CYCLE_1_NONEMPTY_MONO]);

(* Theorem: !n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==>
     NOT SING (orbit cycle (Z n) (multicoloured n a) l) *)
val multicoloured_ORBIT_NOT_SING = store_thm(
  "multicoloured_ORBIT_NOT_SING",
  ``!n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==>
    ~ SING (orbit cycle (Z n) (multicoloured n a) l)``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `action cycle (Z n) (multicoloured n a)` by SRW_TAC [][cycle_action_on_multicoloured] THEN
  `l IN (orbit cycle (Z n) (multicoloured n a) l)` by METIS_TAC [ACTION_ORBIT_HAS_ITSELF, ZN_group] THEN
  `(orbit cycle (Z n) (multicoloured n a) l) = {l}` by METIS_TAC [SING_DEF, IN_SING] THEN
  `Group (Z n)` by SRW_TAC [][ZN_group] THEN
  Cases_on `n=1` THENL [
    `orbit cycle (Z n) (multicoloured n a) l = EMPTY` by METIS_TAC [multicoloured_1_EMPTY, orbit_def, NOT_IN_EMPTY] THEN
    METIS_TAC [NOT_SING_EMPTY],
    `1 < n` by RW_TAC arith_ss [] THEN
    `1 IN (Z n).carrier` by SRW_TAC [][Z_def, IN_COUNT] THEN
    `cycle 1 l IN (orbit cycle (Z n) (multicoloured n a) l)` by SRW_TAC [][ORBIT_HAS_ACTION_ELEMENT] THEN
    `cycle 1 l <> l` by METIS_TAC [multicoloured_NOT_CYCLE_1] THEN
    METIS_TAC [IN_SING]
  ]);

(* Theorem: CARD (orbit cycle (Z n) (multicoloured n a) l) <> 1 *)
val CARD_multicoloured_ORBIT_NOT_1 = store_thm(
  "CARD_multicoloured_ORBIT_NOT_1",
  ``!n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==>
    CARD (orbit cycle (Z n) (multicoloured n a) l) <> 1``,
  REPEAT STRIP_TAC THEN
  `~ SING (orbit cycle (Z n) (multicoloured n a) l)` by SRW_TAC [][multicoloured_ORBIT_NOT_SING] THEN
  `action cycle (Z n) (multicoloured n a)` by SRW_TAC [][cycle_action_on_multicoloured] THEN
  `FINITE (orbit cycle (Z n) (multicoloured n a) l)` by METIS_TAC [FINITE_multicoloured, ORBIT_SUBSET_TARGET, SUBSET_FINITE] THEN
  METIS_TAC [SING_IFF_CARD1]);

(* Theorem: Orbits of cycle action on multicoloured necklaces are of length p, for prime p *)
val CARD_multicoloured_PRIME_ORBIT = store_thm(
  "CARD_multicoloured_PRIME_ORBIT",
  ``!p a. prime p /\ 0 < a /\ l IN (multicoloured p a) ==>
    (CARD (orbit cycle (Z p) (multicoloured p a) l) = p)``,
  REPEAT STRIP_TAC THEN
  `0 < p /\ 1 < p` by METIS_TAC [dividesTheory.PRIME_POS, dividesTheory.ONE_LT_PRIME] THEN
  `FiniteGroup (Z p)` by SRW_TAC [][ZN_finite_group] THEN
  `FINITE (multicoloured p a)` by SRW_TAC [][FINITE_multicoloured] THEN
  `action cycle (Z p) (multicoloured p a)` by METIS_TAC [cycle_action_on_multicoloured] THEN
  `p = CARD (orbit cycle (Z p) (multicoloured p a) l) * CARD (stabilizer cycle (Z p) l)` by METIS_TAC [ORBIT_STABILIZER_THEOREM, CARD_ZN_carrier] THEN
  `CARD (orbit cycle (Z p) (multicoloured p a) l) <> 1` by METIS_TAC [CARD_multicoloured_ORBIT_NOT_1] THEN
  METIS_TAC [FACTORS_OF_PRIME]);

(* Theorem: [Fermat's Little Theorem] !p a. prime p ==> divides p (a**p - a)     *)
(* Proof (J. Petersen in 1872):
   Take p elements from a with repetitions in all ways, that is, in a^p ways.
                   by CARD_Necklace
   The a sets with elements all alike are not changed by a cyclic permutation of the elements,
                   by CARD_monocoloured
   while the remaining (a^p - a) sets are
                   by CARD_multicoloured
   permuted in sets of p.
                   by cycle_action_on_multicoloured, CARD_multicoloured_PRIME_ORBIT
   Hence p divides a^p - a.
                   by EQUAL_SIZE_ORBITS_PROPERTY
*)
val FERMAT_LITTLE_THM = store_thm(
  "FERMAT_LITTLE_THM",
  ``!p a. prime p ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by RW_TAC std_ss [dividesTheory.PRIME_POS] THEN
  Cases_on `a = 0` THENL [
    METIS_TAC [arithmeticTheory.SUB_0, arithmeticTheory.EXP_EQ_0, dividesTheory.ALL_DIVIDES_0],
    `0 < a` by DECIDE_TAC THEN
    `CARD (multicoloured p a) = a**p - a` by RW_TAC std_ss [CARD_multicoloured] THEN
    `Group (Z p)` by RW_TAC std_ss [ZN_group] THEN
    `action cycle (Z p) (multicoloured p a)` by RW_TAC std_ss [cycle_action_on_multicoloured] THEN
    `FINITE (multicoloured p a)` by RW_TAC std_ss [FINITE_multicoloured] THEN
    `!l. l IN (multicoloured p a) ==> (CARD (orbit cycle (Z p) (multicoloured p a) l) = p)`
   by RW_TAC std_ss [CARD_multicoloured_PRIME_ORBIT] THEN
    METIS_TAC [EQUAL_SIZE_ORBITS_PROPERTY]
  ]);
 
(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
