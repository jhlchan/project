(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Combinatorial proof.                            *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by J. Petersen in 1872:

Take p elements from q with repetitions in all ways, that is, in q^p ways.
The q sets with elements all alike are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted in sets of p. Hence p divides q^p - q.

This is a combinatorial using Group action, via Fixed Points Congruence of Zp.
With Group action, there is no need to consider multicoloured necklaces --
they are hidden in the theory.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "AllFLTfixedpoints";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* val _ = load "dividesTheory"; *)

(* Part 1: Basis ----------------------------------------------------------- *)

(* Theorem: If n > 0, ((n - x) MOD n + x) MOD n = 0  for x < n. *)
val MOD_ADD_INV = store_thm(
  "MOD_ADD_INV",
  ``!n. 0 < n /\ x < n ==> (((n - x) MOD n + x) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  `((n - x) MOD n + x) MOD n = ((n-x) MOD n + x MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = ((n-x) + x) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  RW_TAC arith_ss []);

(* Theorem: Addition is associative in MOD: if x, y, z all < n,
            ((x + y) MOD n + z) MOD n = (x + (y + z) MOD n) MOD n. *)
val MOD_ADD_ASSOC = store_thm(
  "MOD_ADD_ASSOC",
  ``!n. 0 < n ==> (!x y z. x < n /\ y < n /\ z < n ==>
                 (((x + y) MOD n + z) MOD n = (x + (y + z) MOD n) MOD n))``,
  REPEAT STRIP_TAC THEN
  `((x + y) MOD n + z) MOD n = ((x + y) MOD n + z MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (((x + y) + z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  `_ = ((x + (y + z)) MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (x MOD n + (y + z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* Factor of prime and prime exponents.                                      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (the missing arithmeticTheory.EQ_MULT_RCANCEL)
   !m n p. (n * m = p * m) <=> (m = 0) \/ (n = p)  *)
val EQ_MULT_RCANCEL = store_thm(
  "EQ_MULT_RCANCEL",
  ``!m n p. (n * m = p * m) <=> (m = 0) \/ (n = p)``,
  METIS_TAC [arithmeticTheory.EQ_MULT_LCANCEL, arithmeticTheory.MULT_SYM]);

(* Theorem: [Factors of prime] For prime p, p = x*y ==> (x = 1 and y = p) or (x = p and y = 1) *)
val FACTORS_OF_PRIME = store_thm(
  "FACTORS_OF_PRIME",
  ``!p x y. prime p /\ (p = x*y) ==> ((x = 1) /\ (y = p)) \/ ((x = p) /\ (y = 1))``,
  REPEAT STRIP_TAC THEN
  `divides x p \/ divides y p` by METIS_TAC [dividesTheory.divides_def] THENL [
    `(x = 1) \/ (x = p)` by METIS_TAC [dividesTheory.prime_def] THEN1
    SRW_TAC [][] THEN
    `0 <> p` by METIS_TAC [dividesTheory.PRIME_POS, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [arithmeticTheory.EQ_MULT_LCANCEL, arithmeticTheory.MULT_RIGHT_1],
    `(y = 1) \/ (y = p)` by METIS_TAC [dividesTheory.prime_def] THEN1
    SRW_TAC [][] THEN
    `0 <> p` by METIS_TAC [dividesTheory.PRIME_POS, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [EQ_MULT_RCANCEL, arithmeticTheory.MULT_LEFT_1]
  ]);

(* Theorem: For a 1-1 map f: s -> s, s and (IMAGE f s) are of the same size. *)
val CARD_IMAGE = store_thm(
  "CARD_IMAGE",
  ``!s f. (!x y. (f x = f y) <=> (x = y)) /\ FINITE s ==> (CARD (IMAGE f s) = CARD s)``,
  Q_TAC SUFF_TAC
    `!f. (!x y. (f x = f y) = (x = y)) ==> !s. FINITE s ==> (CARD (IMAGE f s) = CARD s)`
    THEN1 METIS_TAC [] THEN
  GEN_TAC THEN STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][]);

(* Theorem: If !e IN s, CARD e = k, SIGMA CARD s = CARD s * k. *)
val SIGMA_CARD_CONSTANT = store_thm(
  "SIGMA_CARD_CONSTANT",
  ``!k s. FINITE s ==> (!e. e IN s ==> (CARD e = k)) ==> (SIGMA CARD s = k * (CARD s))``,
  STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][] THEN1 METIS_TAC [SUM_IMAGE_THM] THEN
  `CARD e = k` by SRW_TAC [][] THEN
  `SIGMA CARD (e INSERT s) = CARD e + SIGMA CARD (s DELETE e)` by SRW_TAC [][SUM_IMAGE_THM] THEN
  `s DELETE e = s` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `SUC (CARD s) = 1 + CARD s` by RW_TAC arith_ss [] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  RW_TAC arith_ss []);

(* Theorem: When the partitions are equal size of n, CARD s = n * CARD (partition of s) *)
(* Proof:
   FINITE (partition R s)               by FINITE_partition
   CARD s = SIGMA CARD (partition R s)  by partition_CARD
          = n  * CARD (partition R s)   by SIGMA_CARD_CONSTANT
*)
val equal_partition_CARD = store_thm(
  "equal_partition_CARD",
  ``!R s n. R equiv_on s /\ FINITE s /\
    (!t. t IN partition R s ==> (CARD t = n)) ==>
    (CARD s = n * CARD (partition R s))``,
  METIS_TAC [partition_CARD, FINITE_partition, SIGMA_CARD_CONSTANT]);

(* Note: There is LENGTH_NIL, but no LENGTH_NON_NIL *)
(* Theorem: a non-NIL list has positive LENGTH. *)
val LENGTH_NON_NIL = store_thm(
  "LENGTH_NON_NIL",
  ``!l. l <> [] ==> 0 < LENGTH l``,
  REPEAT STRIP_TAC THEN
  `LENGTH l <> 0` by SRW_TAC [][LENGTH_NIL] THEN
  RW_TAC arith_ss []);

(* DROP and TAKE *)

(* Note: There is TAKE_LENGTH_ID, but no DROP_LENGTH_NIL *)
(* Theorem: DROP the whole length of list is NIL. *)
val DROP_LENGTH_NIL = store_thm(
  "DROP_LENGTH_NIL",
  ``DROP (LENGTH l) l = []``,
  Induct_on `l` THEN SRW_TAC [][]);

(* Theorem: TAKE 1 of non-NIL list is unaffected by appending. *)
val TAKE_1_APPEND = store_thm(
  "TAKE_1_APPEND",
  ``!x y. x <> [] ==> (TAKE 1 (x ++ y) = TAKE 1 x)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 of non-NIL list is mildly affected by appending. *)
val DROP_1_APPEND = store_thm(
  "DROP_1_APPEND",
  ``!x y. x <> [] ==> (DROP 1 (x ++ y) = (DROP 1 x) ++ y)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 more time is once more of previous DROP. *)
val DROP_SUC = store_thm(
  "DROP_SUC",
  ``!n x. DROP (SUC n) x = DROP 1 (DROP n x)``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: TAKE 1 more time is a bit more than previous TAKE. *)
val TAKE_SUC = store_thm(
  "TAKE_SUC",
  ``!n x. TAKE (SUC n) x = (TAKE n x) ++ (TAKE 1 (DROP n x))``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: DROP is non-NIL when the number is less than LENGTH. *)
val DROP_NON_NIL = store_thm(
  "DROP_NON_NIL",
  ``!n l. n < LENGTH l ==> DROP n l <> []``,
  Induct_on `l` THEN SRW_TAC [][] THEN
  `n - 1 < LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Unit-Length Lists *)

(* Theorem: A unit-length list has its set a singleton. *)
val LIST_1_SET_SING = store_thm(
  "LIST_1_SET_SING",
  ``!l. (LENGTH l = 1) ==> SING (set l)``,
  SRW_TAC [][SING_DEF] THEN
  Cases_on `l` THEN1
   (`LENGTH [] = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `1 <> 0` by RW_TAC arith_ss [] THEN METIS_TAC []) THEN
  `SUC (LENGTH t) = 1` by METIS_TAC [LENGTH] THEN
  `LENGTH t = 0` by RW_TAC arith_ss [] THEN
  `t = []` by METIS_TAC [LENGTH_NIL] THEN
  FULL_SIMP_TAC (srw_ss())[] THEN METIS_TAC []);

(* Mono-list Theory: a mono-list is a list l with SING (set l) *)

(* Theorem: A non-mono-list has at least one element in tail that is distinct from its head. *)
val NON_MONO_TAIL_PROPERTY = store_thm(
  "NON_MONO_TAIL_PROPERTY",
  ``!l. ~SING (set (h::t)) ==> ?h'. h' IN set t /\ h' <> h``,
  SRW_TAC [][SING_INSERT] THEN
  `set t <> {}` by METIS_TAC [LIST_TO_SET_EQ_EMPTY] THEN
  `?e. e IN set t` by METIS_TAC [MEMBER_NOT_EMPTY] THEN
  FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
  METIS_TAC []);

(* Theorem: Two mono-lists are equal if their lengths and sets are equal *)
val MONOLIST_EQ = store_thm(
  "MONOLIST_EQ",
  ``!l1 l2. SING (set l1) /\ SING (set l2) ==>
              ((l1 = l2) <=> (LENGTH l1 = LENGTH l2) /\ (set l1 = set l2))``,
  Induct THEN SRW_TAC [][NOT_SING_EMPTY, SING_INSERT] THENL [
    Cases_on `l2` THEN SRW_TAC [][] THEN
    FULL_SIMP_TAC (srw_ss()) [SING_INSERT, EQUAL_SING] THEN
    SRW_TAC [][LENGTH_NIL, NOT_SING_EMPTY, EQUAL_SING] THEN METIS_TAC [],
    Cases_on `l2` THEN SRW_TAC [][] THEN
    FULL_SIMP_TAC (srw_ss()) [SING_INSERT, LENGTH_NIL, NOT_SING_EMPTY, EQUAL_SING] THEN
    METIS_TAC []
  ]);

(* Part 2: General Theory -------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Theory of Necklaces                                                       *)
(* ------------------------------------------------------------------------- *)

(* Define necklaces as lists of length n, i.e. with n beads *)
val Necklace_def = Define `
  Necklace n a = {l | (LENGTH l = n) /\ (set l) SUBSET (count a) }
`;

(* Theorem: Zero-length necklaces of whatever colors is the set of NIL. *)
val Necklace_0 = store_thm(
  "Necklace_0",
  ``!a. Necklace 0 a = {[]}``,
  SRW_TAC [boolSimps.CONJ_ss][Necklace_def, LENGTH_NIL]);

(* Theorem: A necklace of length n <> 0 has an order, i.e. non-NIL. *)
val NECKLACE_NONNIL = store_thm(
  "NECKLACE_NONNIL",
  ``!n a l. (0 < n) /\ (0 < a) /\ (l IN (Necklace n a)) ==> (l <> [])``,
  SRW_TAC [][Necklace_def] THEN
  `LENGTH l <> 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [LENGTH_NIL]);

(* Theorem: If l in (Necklace n a), LENGTH l = n  and colors in count a. *)
val NECKLACE_PROPERTY = store_thm(
  "NECKLACE_PROPERTY",
  ``!n a l. l IN (Necklace n a) ==> (LENGTH l = n) /\ (set l SUBSET count a)``,
  SRW_TAC [][Necklace_def]);

(* Theorem: Relate (Necklace (n+1) a) to (Necklace n a) for induction. *)
val Necklace_SUC = prove(
  ``!n a. Necklace (SUC n) a = IMAGE (\(e,l). e :: l) (count a CROSS Necklace n a)``,
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss]
    [Necklace_def, count_def, SUBSET_DEF, EXTENSION, pairTheory.EXISTS_PROD, LENGTH_CONS] THEN
  METIS_TAC []);

(* Theorem: The set of (Necklace n a) is finite. *)
val FINITE_Necklace = store_thm(
   "FINITE_Necklace",
  ``!n a. FINITE (Necklace n a)``,
  Induct_on `n` THEN SRW_TAC [][Necklace_0, Necklace_SUC]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (Necklace n a) = a^n.                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Size of (Necklace n a) = a^n. *)
val CARD_Necklace = store_thm(
  "CARD_Necklace",
  ``!n a. CARD (Necklace n a) = a ** n``,
  Induct_on `n` THEN
  SRW_TAC [][Necklace_0, FINITE_Necklace,
             Necklace_SUC, CARD_IMAGE, FINITE_COUNT, CARD_CROSS,
             pairTheory.FORALL_PROD, arithmeticTheory.EXP]);

(* ------------------------------------------------------------------------- *)
(* Monocoloured Necklace - necklace with a single color.                     *)
(* ------------------------------------------------------------------------- *)

(* Define mono-colored necklace *)
val monocoloured_def = Define`
  monocoloured n a = {l | (l IN Necklace n a) /\ (l <> [] ==> SING (set l)) }
`;

(* Theorem: A monocoloured necklace is indeed a Necklace. *)
val monocoloured_Necklace = store_thm(
  "monocoloured_Necklace",
  ``!n a l. l IN monocoloured n a ==> l IN Necklace n a``,
  SRW_TAC [][Necklace_def, monocoloured_def]);

(* Theorem: The monocoloured set is FINITE. *)
val FINITE_monocoloured = store_thm(
  "FINITE_monocoloured",
  ``!n a. FINITE (monocoloured n a)``,
  METIS_TAC [SUBSET_DEF, monocoloured_Necklace, SUBSET_FINITE, FINITE_Necklace]);

(* Theorem: Unit-length monocoloured set consists of singletons. *)
val monocoloured_1 = prove(
  ``!a. monocoloured 1 a = {[e] | e IN count a}``,
  SIMP_TAC bool_ss [monocoloured_def, Necklace_def, count_def, arithmeticTheory.ONE, LENGTH_CONS] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION]);

(* Theorem: Unit-length necklaces are monocoloured. *)
val necklace_1_monocoloured = store_thm(
  "necklace_1_monocoloured",
  ``!a. Necklace 1 a = monocoloured 1 a``,
  SRW_TAC [][Necklace_def, monocoloured_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [LIST_1_SET_SING]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (monocoloured n a) = a.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Relate (monocoloured (SUC n) a) to (monocoloured n a) for induction. *)
val monocoloured_SUC = prove(
  ``!n. 0 < n ==> (monocoloured (SUC n) a = IMAGE (\l. HD l :: l) (monocoloured n a))``,
  SRW_TAC [][EXTENSION, EQ_IMP_THM, monocoloured_def] THEN
  FULL_SIMP_TAC (srw_ss()) [Necklace_def, LENGTH_CONS, count_def, SUBSET_DEF, SING_DEF] THENL [
    Cases_on `l'` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
    `x <> [] /\ MEM h x /\ MEM h' x` by SRW_TAC [][] THEN
    `?k. !z . MEM z x <=> (z = k)` by SRW_TAC [][] THEN
    `h = h'` by METIS_TAC [] THEN SRW_TAC [][] THEN
    Q.EXISTS_TAC `x'` THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [] THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN METIS_TAC []
  ]);

(* Theorem: Size of (monocoloured n a) = a *)
val CARD_monocoloured = store_thm(
  "CARD_monocoloured",
  ``!n a. 0 < n ==> (CARD (monocoloured n a) = a)``,
  REPEAT STRIP_TAC THEN
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n` THENL [
    SRW_TAC [][monocoloured_1] THEN
    `{[e] | e < a} = IMAGE (\n. [n]) (count a)` by SRW_TAC [][EXTENSION] THEN
    SRW_TAC [][CARD_IMAGE],
    SRW_TAC [][monocoloured_SUC] THEN
    Q.MATCH_ABBREV_TAC `CARD (IMAGE f s) = a` THEN
    `!x y. (f x = f y) <=> (x = y)` by SRW_TAC [][EQ_IMP_THM, Abbr`f`] THEN
    `FINITE s` by SRW_TAC [][FINITE_monocoloured, Abbr`s`] THEN
    SRW_TAC [][CARD_IMAGE]
  ]);

(* ------------------------------------------------------------------------- *)
(* Multicoloured Necklace                                                    *)
(* ------------------------------------------------------------------------- *)

(* Define multi-colored necklace *)
val multicoloured_def = Define`
  multicoloured n a = (Necklace n a) DIFF (monocoloured n a)
`;

(* Theorem: multicoloured is a Necklace *)
val multicoloured_Necklace = store_thm(
  "multicoloured_Necklace",
  ``!n a l. l IN multicoloured n a ==> l IN Necklace n a``,
  SRW_TAC [][multicoloured_def, Necklace_def]);

(* Theorem: multicoloured set is FINITE *)
val FINITE_multicoloured = store_thm(
  "FINITE_multicoloured",
  ``!n a. FINITE (multicoloured n a)``,
  SRW_TAC [][multicoloured_def, FINITE_Necklace, FINITE_DIFF]);

(* Theorem: mutlicoloured 1 a = EMPTY *)
val multicoloured_1_EMPTY = store_thm(
  "multicoloured_1_EMPTY",
  ``!a. multicoloured 1 a = {}``,
  SRW_TAC [][multicoloured_def, Necklace_def, necklace_1_monocoloured]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (multicoloured n a) = a^n - a.                              *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A multicoloured necklace is not monocoloured. *)
val MULTI_MONO_DISJOINT = store_thm(
  "MULTI_MONO_DISJOINT",
  ``!n a. DISJOINT (multicoloured n a) (monocoloured n a)``,
  SIMP_TAC bool_ss [multicoloured_def, monocoloured_def, Necklace_def,
                    SING_DEF, SUBSET_DEF, DISJOINT_DEF] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION]);

(* Theorem: A necklace is either monocoloured or multicolored. *)
val MULTI_MONO_EXHAUST = store_thm(
  "MULTI_MONO_EXHAUST",
  ``!n a. Necklace n a = (multicoloured n a) UNION (monocoloured n a)``,
  SIMP_TAC bool_ss [multicoloured_def, monocoloured_def, Necklace_def,
                    SING_DEF, SUBSET_DEF, UNION_DEF] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION] THEN
  METIS_TAC []);

(* Theorem: Size of (multicoloured n a) = a^n - a *)
val CARD_multicoloured = store_thm(
  "CARD_multicoloured",
  ``!n a. 0 < n ==> (CARD (multicoloured n a) = a**n - a)``,
  REPEAT STRIP_TAC THEN
  `FINITE (multicoloured n a)` by SRW_TAC [][FINITE_multicoloured] THEN
  `FINITE (monocoloured n a)` by SRW_TAC [][FINITE_monocoloured] THEN
  `CARD (multicoloured n a) + CARD (monocoloured n a) =
      CARD ((multicoloured n a) UNION (monocoloured n a)) +
      CARD ((multicoloured n a) INTER (monocoloured n a))` by SRW_TAC [][CARD_UNION] THEN
  `CARD ((multicoloured n a) INTER (monocoloured n a)) = 0`
    by METIS_TAC [MULTI_MONO_DISJOINT, DISJOINT_DEF, CARD_EMPTY] THEN
  `CARD ((multicoloured n a) UNION (monocoloured n a)) = a**n`
    by METIS_TAC [MULTI_MONO_EXHAUST, CARD_Necklace] THEN
  `CARD (monocoloured n a) = a` by SRW_TAC [][CARD_monocoloured] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* Theory of Cycles                                                          *)
(* ------------------------------------------------------------------------- *)

(* Define cycle in view of action from Z_n to necklace n a *)

(* Cycle as nth iterate of rotate: DROP 1 ++ TAKE 1 *)
val cycle_def = Define `
  cycle n l = FUNPOW (\l. DROP 1 l ++ TAKE 1 l) n l
`;

(* Theorem: cycle 0 l = l *)
val CYCLE_0 = store_thm(
  "CYCLE_0",
  ``!l. cycle 0 l = l``,
  SRW_TAC [][cycle_def]);

(* Theorem: Cycle (n+1) of a list is cycle once more of cycle n list.
            cycle (SUC n) l = cycle 1 (cycle n l) *)
val CYCLE_SUC = prove(
  ``!l n. cycle (SUC n) l = cycle 1 (cycle n l)``,
  METIS_TAC [cycle_def, arithmeticTheory.FUNPOW_SUC, arithmeticTheory.FUNPOW_1]);

(* Theorem: Cycle is additive, (cycle n (cycle m l) = cycle (n+m) l  *)
val CYCLE_ADD = store_thm(
  "CYCLE_ADD",
  ``!l n m. cycle n (cycle m l) = cycle (n+m) l``,
  SRW_TAC [][cycle_def, arithmeticTheory.FUNPOW_ADD]);

(* Theorem: Cycle keeps LENGTH (of necklace), or LENGTH (cycle n l) = LENGTH l *)
val CYCLE_SAME_LENGTH = store_thm(
  "CYCLE_SAME_LENGTH",
  ``!l n. LENGTH (cycle n l) = LENGTH l``,
  `!l. LENGTH (cycle 1 l) = LENGTH l`
      by (SRW_TAC [][cycle_def] THEN Cases_on `l` THEN
          SRW_TAC [ARITH_ss][]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* Theorem: Cycle keep set (of colors), or set (cycle n l) = set l *)
val CYCLE_SAME_SET = store_thm(
  "CYCLE_SAME_SET",
  ``!l n. set (cycle n l) = set l``,
  `!l. set (cycle 1 l) = set l`
      by (Cases_on `l` THEN SRW_TAC [][cycle_def, EXTENSION, DISJ_COMM]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* Theorem: A mono-list has cycle 1 l = l *)
val SING_HAS_CYCLE_1 = store_thm(
  "SING_HAS_CYCLE_1",
  ``!l. SING (set l) ==> (cycle 1 l = l)``,
  SRW_TAC [][cycle_def] THEN
  Cases_on `l` THEN SRW_TAC [][] THEN
  FULL_SIMP_TAC (srw_ss()) [SING_INSERT] THEN
  `SING (set (t ++ [h]))` by SRW_TAC [][SING_UNION] THEN
  SRW_TAC [ARITH_ss][MONOLIST_EQ]);

(* Theorem: A mono-list l has cycle 1 l = l, or LENGTH l = 1 ==> cycle 1 l = l *)
val MONO_HAS_CYCLE_1 = store_thm(
  "MONO_HAS_CYCLE_1",
  ``!l. (LENGTH l = 1) ==> (cycle 1 l = l)``,
  REPEAT STRIP_TAC THEN
  `LENGTH l <> 0` by RW_TAC arith_ss [] THEN
  `?h t. l = h::t` by METIS_TAC [LENGTH_NIL, list_CASES] THEN
  `_ = [h] ++ t` by SRW_TAC [][] THEN
  `LENGTH [h] = 1` by RW_TAC arith_ss [LENGTH] THEN
  `LENGTH t = 0` by FULL_SIMP_TAC (srw_ss()) [LENGTH_APPEND] THEN
  `t = []` by METIS_TAC [LENGTH_NIL] THEN
  SRW_TAC [][cycle_def]);

(* Theorem: cycle 1 l = l makes cycle trivial, or cycle 1 l = l ==> cycle n l = l *)
val CYCLE_1_FIX = store_thm(
  "CYCLE_1_FIX",
  ``!l n. (cycle 1 l = l) ==> (cycle n l = l)``,
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* ------------------------------------------------------------------------- *)
(* To show:  cycle x (cycle y l) = cycle ((x + y) MOD n) l, n = LENGTH l.    *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For n <= LENGTH l, cycle n l = DROP n l ++ TAKE n l *)
val CYCLE_EQ_DROP_TAKE = prove(
  ``!l n. n <= LENGTH l ==> (cycle n l = DROP n l ++ TAKE n l)``,
  REPEAT STRIP_TAC THEN
  Cases_on `l = []` THEN1 (
    `LENGTH l = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `n = 0` by RW_TAC arith_ss [] THEN
    SRW_TAC [][CYCLE_0]) THEN
  Induct_on `n` THEN1 SRW_TAC [][CYCLE_0] THEN
  SRW_TAC [][CYCLE_SUC] THEN
  `n < LENGTH l` by RW_TAC arith_ss [] THEN
  SRW_TAC [ARITH_ss][] THEN
  SRW_TAC [][cycle_def] THEN
  SRW_TAC [][DROP_SUC, TAKE_SUC, DROP_1_APPEND, TAKE_1_APPEND, DROP_NON_NIL]);

(* Theorem: Cycle through length gives original, or cycle (LENGTH l) l = l. *)
val CYCLE_BACK = store_thm(
  "CYCLE_BACK",
  ``!l. cycle (LENGTH l) l = l``,
  SRW_TAC [][CYCLE_EQ_DROP_TAKE, DROP_LENGTH_NIL, TAKE_LENGTH_ID]);

(* Theorem: cycle (n*LENGTH l) l = l. *)
val CYCLE_BACK_MULTIPLE = store_thm(
  "CYCLE_BACK_MULTIPLE",
  ``!l n. cycle (n * LENGTH l) l = l``,
  Induct_on `n` THEN
  SRW_TAC [][CYCLE_0] THEN
  `SUC n * LENGTH l = LENGTH l + n * LENGTH l` by RW_TAC arith_ss [arithmeticTheory.MULT_SUC] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK]);

(* Theorem: if l <> [], cycle n l = cycle (n MOD (LENGTH l)) l *)
val CYCLE_MOD_LENGTH = store_thm(
  "CYCLE_MOD_LENGTH",
  ``!l n. l <> [] ==> (cycle n l = cycle (n MOD (LENGTH l)) l)``,
  REPEAT STRIP_TAC THEN
  `0 < LENGTH l` by SRW_TAC [][LENGTH_NON_NIL] THEN
  `n = n DIV LENGTH l * LENGTH l + n MOD LENGTH l` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = n MOD LENGTH l + n DIV LENGTH l * LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK_MULTIPLE]);

(* Theorem: cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l *)
val CYCLE_ADDITION = store_thm(
  "CYCLE_ADDITION",
  ``!l x y. l <> [] ==> (cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l)``,
  METIS_TAC [CYCLE_ADD, CYCLE_MOD_LENGTH]);

(* Theorem: cycle 1 (h::t) = t ++ [h] *)
val CYCLE_1_EQ = store_thm(
  "CYCLE_1_EQ",
  ``!h t. cycle 1 (h::t) = t ++ [h]``,
  SRW_TAC [][cycle_def]);

(* Theorem: (t ++ [h] <> h::t) if (set t) has some element h' <> h. *)
val CYCLE_1_NEQ = store_thm(
  "CYCLE_1_NEQ",
  ``!h h' t. h' IN set t /\ h' <> h ==> (t ++ [h] <> h::t)``,
  SRW_TAC [][] THEN
  Induct_on `t` THEN SRW_TAC [][] THEN
  METIS_TAC []);

(* Theorem: [inverse of SING_HAS_CYCLE_1]
            !l. (cycle 1 l = l) ==> SING (set l)  *)
val CYCLE_1_NONEMPTY_MONO = store_thm(
  "CYCLE_1_NONEMPTY_MONO",
  ``!l. l <> [] /\ (cycle 1 l = l) ==> SING (set l)``,
  REPEAT STRIP_TAC THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* ------------------------------------------------------------------------- *)
(* Theory of Groups                                                          *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Group Definition.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Set up group type as a record
   A Group has:
   . a carrier set (set = function 'a -> bool, since MEM is a boolean function)
   . an identity element
   . an inverse function (unary operation)
   . a product function called multiplication (binary operation)
*)
val _ = Hol_datatype`
  group = <| carrier: 'a -> bool;
                  id: 'a;
                 inv: 'a -> 'a;
                mult: 'a -> 'a -> 'a
           |>`;

(* set overloading  *)
val _ = overload_on ("*", ``g.mult``);
val _ = overload_on ("|/", ``g.inv``);
val _ = overload_on ("#e", ``g.id``);

(* Group Definition:
   A Group is a set with elements of g of type 'a group, such that
   . g.id is in the carrier set (e in G)
   . (g.mult x y) is in the carrier set (closure: x*y in G)
   . (g.inv x) is in the carrier set (existence of inverse: x^-1 in G)
   . (g.mult (g.inv x) x) = (g.id)  (property of inverse and identity: (x^-1)*(x) = e)
   . (g.mult (g.mult x y) z) = (g.mult x (g.mult y z)) (associativity: (x*y)*z = x*(y*z))
*)
(* Define Group by predicate *)
val Group_def = Define`
  Group (g: 'a group) =
    #e IN g.carrier /\
    (!x y:: (g.carrier). x * y IN g.carrier) /\
    (!x:: (g.carrier). |/x IN g.carrier) /\
    (!x:: (g.carrier). #e * x = x) /\
    (!x:: (g.carrier). |/x * x = #e) /\
    (!x y z:: (g.carrier). (x * y) * z = x * (y * z))
  `;

(* Finite Group: a Group with a finite carrier set. *)
val FiniteGroup_def = Define`
  FiniteGroup (g: 'a group) =
    Group g /\ FINITE g.carrier
  `;

(* ------------------------------------------------------------------------- *)
(* Basic theorem from definition.                                            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Group identity is an element. *)
val group_id_carrier = store_thm(
  "group_id_carrier",
  ``!g. Group g ==> #e IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: Group inverse is an element. *)
val group_inv_carrier = store_thm(
  "group_inv_carrier",
  ``!g. Group g ==> !x :: (g.carrier). |/ x IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group closure] Group product is an element. *)
val group_mult_carrier = store_thm(
  "group_mult_carrier",
  ``!g. Group g ==> !x y :: (g.carrier). x * y IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left Identity] e x = x *)
val group_lid = store_thm(
  "group_lid",
  ``!g. Group g ==> !x :: (g.carrier). #e * x = x``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left inverse] x' x = e *)
val group_linv = store_thm(
  "group_linv",
  ``!g. Group g ==> !x :: (g.carrier). |/x * x = #e``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group associativity] (x y) z = x (y z) *)
val group_assoc = store_thm(
  "group_assoc",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y) * z = x * (y * z)``,
  SRW_TAC [][Group_def]);

(* Export rewrites after using Group_def, don't use Group_def anymore *)
val _ = export_rewrites ["group_id_carrier", "group_inv_carrier", "group_mult_carrier"];

(* ------------------------------------------------------------------------- *)
(* Theorems in basic Group Theory.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Group right inverse] x x' = e *)
val group_rinv = store_thm(
  "group_rinv",
  ``!g. Group g ==> !x :: (g.carrier). x * |/x = #e``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * |/x = #e * (x * |/x)`       by SRW_TAC [][group_lid] THEN
  `_ = (#e * x) * |/x`             by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x * x * |/x`    by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (|/x * x) * |/x`  by SRW_TAC [][GSYM group_assoc] THEN
  `_ = |/(|/x) * #e * |/x`         by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (#e * |/x)`       by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x`              by SRW_TAC [][group_lid] THEN
  `_ = #e`                         by SRW_TAC [][group_linv] THEN
  SRW_TAC [][]);

(* Theorem: [Group right identity] x e = x *)
val group_rid = store_thm(
  "group_rid",
  ``!g. Group g ==> !x :: (g.carrier). x * #e = x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * #e = x * (|/x * x)` by SRW_TAC [][group_linv] THEN
  `_ = (x * |/x) * x`      by SRW_TAC [][group_assoc] THEN
  `_ = #e * x`             by SRW_TAC [][group_rinv] THEN
  SRW_TAC [][group_lid]);

(* Theorem: [Solve left unknown] x y = z  <=> x = z y' *)
val group_lsolve = store_thm(
  "group_lsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (x = z * |/y)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_rid]);

(* Theorem: [Solve right unknown] x y = z  <=> y = x' z *)
val group_rsolve = store_thm(
  "group_rsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (y = |/x * z)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_lid]);

(* Theorem: [Left cancellation] x y = x z <=> y = z *)
val group_lcancel = store_thm(
  "group_lcancel",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y = x * z) = (y = z)``,
  METIS_TAC [group_inv_carrier, group_mult_carrier, group_assoc, group_rsolve, group_linv, group_lid]);

(* Theorem: [Left inverse unique] x y = e <=> x = y' *)
val group_linv_unique = store_thm(
  "group_linv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (x = |/y)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_lsolve, group_linv, group_lid]);

(* Theorem: [Right inverse unique] x y = e <=> y = x' *)
val group_rinv_unique = store_thm(
  "group_rinv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (y = |/x)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_rsolve, group_rinv, group_rid]);

(* Theorem: [Inverse of inverse] x'' = x *)
val group_inv_inv = store_thm(
  "group_inv_inv",
  ``!g. Group g ==> !x :: (g.carrier). |/(|/x) = x``,
  METIS_TAC [group_inv_carrier, group_rinv, group_linv_unique]);

(* Theorem: [Inverse of product] (x y)' = y' x' *)
val group_inv_mult = store_thm(
  "group_inv_mult",
  ``!g. Group g ==> !x y :: (g.carrier). |/(x * y) = |/y * |/x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `(x * y) * (|/y * |/x) = x * (y * |/y) * |/x` by SRW_TAC [][group_assoc] THEN
  `_ = #e`                                      by SRW_TAC [][group_rinv, group_rid] THEN
  POP_ASSUM MP_TAC THEN
  SRW_TAC [][group_rinv_unique]);

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Theory of Subgroups                                                       *)
(* ------------------------------------------------------------------------- *)

(* A Subgroup is a subset of a group that's a group itself, keeping id, inv, mult. *)
val Subgroup_def = Define `
  Subgroup h g =
    Group h /\ Group g /\
    h.carrier SUBSET g.carrier /\
    (h.id = #e) /\ (h.inv = |/) /\ (h.mult = g.mult)
`;

(* set overloading *)
val _ = overload_on ("<=", ``Subgroup``);

(* Theorem: subgroup property *)
val subgroup_property = store_thm(
  "subgroup_property",
  ``!g h. Group g /\ h <= g ==> (Group h) /\ (h.mult = g.mult) /\ (h.id = g.id) /\ (h.inv = g.inv)``,
  SRW_TAC [][Subgroup_def]);

(* ------------------------------------------------------------------------- *)
(* Cosets of a subgroup.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Define coset of subgroup with an element a. *)
val coset_def = Define `
  coset g X a = IMAGE (\z. g.mult a z) X
`;

(* Theorem: For x IN (coset h g.carrier a) ==> ?y IN g.carrier /\ a*y = x *)
val SUBGROUP_COSET_ELEMENT = store_thm(
  "SUBGROUP_COSET_ELEMENT",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN coset g h.carrier a ==> ?y. y IN h.carrier /\ (x = a*y)``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For all x IN h.carrier, a*x IN coset g h.carrier a. *)
val SUBGROUP_COSET_PROPERTY = store_thm(
  "SUBGROUP_COSET_PROPERTY",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN h.carrier ==> a*x IN coset g h.carrier a``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For h <= g, x IN coset g h.carrier x *)
val SUBGROUP_COSET_NONEMPTY = store_thm(
  "SUBGROUP_COSET_NONEMPTY",
  ``!g h x. Group g /\ h <= g /\ x IN g.carrier ==> x IN coset g h.carrier x``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rid, group_id_carrier]);

(* Theorem: For h <= g, y IN coset g h.carrier x ==> ?z IN h.carrier /\ x = y*z *)
val SUBGROUP_COSET_RELATE = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier /\ y IN coset g h.carrier x ==>
   ?z. z IN h.carrier /\ (x = y*z)``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rinv, group_rid, group_inv_carrier, group_assoc]);

(* Theorem: For h <= g, |/y * x in h.carrier ==> coset g h.carrier x = coset g h.carrier y. *)
val SUBGROUP_COSET_EQ1 = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   (|/y * x) IN h.carrier ==> (coset g h.carrier x = coset g h.carrier y)``,
  SRW_TAC [][coset_def, Subgroup_def, SUBSET_DEF, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THENL [
    `|/y * (x * z) = (|/y * x)* z` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    `|/y * (x * z) IN h.carrier` by METIS_TAC [group_mult_carrier] THEN
    `y * (|/y * (x * z)) = x * z` by METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier] THEN
    METIS_TAC [],
    `|/x * (y * z) = (|/x * y)* z` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    `|/(|/y * x) = |/x * y` by METIS_TAC [group_inv_mult, group_inv_inv, group_inv_carrier] THEN
    `|/x * (y * z) IN h.carrier` by METIS_TAC [group_mult_carrier, group_inv_carrier] THEN
    `x * (|/x * (y * z)) = y * z` by METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier] THEN
    METIS_TAC []
  ]);

(* Theorem: For h <= g, coset g h.carrier x = coset g h.carrier y ==> |/y * x in h.carrier. *)
val SUBGROUP_COSET_EQ2 = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   (coset g h.carrier x = coset g h.carrier y) ==> (|/y * x) IN h.carrier``,
  REPEAT STRIP_TAC THEN
  `y IN coset g h.carrier y` by SRW_TAC [][SUBGROUP_COSET_NONEMPTY] THEN
  `y IN coset g h.carrier x` by SRW_TAC [][] THEN
  `?z. z IN h.carrier /\ (x = y*z)` by SRW_TAC [][SUBGROUP_COSET_RELATE] THEN
  METIS_TAC [group_rsolve, Subgroup_def, SUBSET_DEF]);

(* Theorem: For h <= g, coset g h.carrier x = coset g h.carrier y iff |/y * x in h.carrier *)
val SUBGROUP_COSET_EQ = store_thm(
  "SUBGROUP_COSET_EQ",
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   ((coset g h.carrier x = coset g h.carrier y) <=> (|/y * x) IN h.carrier)``,
  METIS_TAC [SUBGROUP_COSET_EQ1, SUBGROUP_COSET_EQ2]);

(* Theorem: There is a bijection between subgroup and its cosets. *)
val subgroup_to_coset_bij = store_thm(
  "subgroup_to_coset_bij",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==>
   BIJ (\x. a*x) h.carrier (coset g h.carrier a)``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY, SUBGROUP_COSET_ELEMENT, group_lcancel, Subgroup_def, SUBSET_DEF]);

(* Theorem: All cosets of subgroup are of the same size as the subgroup *)
val CARD_subgroup_coset = store_thm(
  "CARD_subgroup_coset",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier /\ FINITE h.carrier ==>
   (CARD (coset g h.carrier a) = CARD (h.carrier))``,
  REPEAT STRIP_TAC THEN
  `BIJ (\x. a*x) h.carrier (coset g h.carrier a)` by SRW_TAC [][subgroup_to_coset_bij] THEN
  `FINITE (coset g h.carrier a)` by METIS_TAC [coset_def, IMAGE_FINITE] THEN
  METIS_TAC [FINITE_BIJ_CARD_EQ]);

(* ------------------------------------------------------------------------- *)
(* Langrange's Theorem by Subgroups and Cosets                               *)
(* ------------------------------------------------------------------------- *)

(* Define b ~ a  when  b IN (coset g h.carrier a) *)
val inCoset_def = Define `
  inCoset g h a b = b IN (coset g h.carrier a)
`;

(* Theorem: inCoset is Reflexive. *)
val INCOSET_REFL = store_thm(
  "INCOSET_REFL",
  ``!g h. Group g /\ h <= g ==> !a. a IN g.carrier ==> inCoset g h a a``,
  METIS_TAC [inCoset_def, SUBGROUP_COSET_NONEMPTY]);

(* Theorem: inCoset is Symmetric. *)
val INCOSET_SYM = store_thm(
  "INCOSET_SYM",
  ``!g h. Group g /\ h <= g ==>
   !a b. a IN g.carrier /\ b IN g.carrier
       ==> (inCoset g h a b ==> inCoset g h b a)``,
  SRW_TAC [][inCoset_def] THEN
  `?z. z IN h.carrier /\ (b = a * z)` by METIS_TAC [SUBGROUP_COSET_ELEMENT] THEN
  `|/ z IN h.carrier` by METIS_TAC [Subgroup_def, group_inv_carrier] THEN
  `z IN g.carrier /\ |/ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `b * |/ z = (a * z) * (|/ z)` by SRW_TAC [][] THEN
  `_ = a * (z * |/ z)` by SRW_TAC [][group_assoc] THEN
  `_ = a` by SRW_TAC [][group_rinv, group_rid] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is Transitive. *)
val INCOSET_TRANS = store_thm(
  "INCOSET_TRANS",
  ``!g h. Group g /\ h <= g ==>
   !a b c. a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
       ==> (inCoset g h a b /\ inCoset g h b c ==> inCoset g h a c)``,
  SRW_TAC [][inCoset_def] THEN
  `?y. y IN h.carrier /\ (b = a * y) /\ ?z. z IN h.carrier /\ (c = b * z)` by SRW_TAC [][SUBGROUP_COSET_ELEMENT] THEN
  `y IN g.carrier /\ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `c = (a * y) * z` by SRW_TAC [][] THEN
  `_ = a * (y * z)` by SRW_TAC [][group_assoc] THEN
  `y*z IN h.carrier` by METIS_TAC [subgroup_property, group_mult_carrier] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is an equivalence relation. *)
val INCOSET_EQUIV_ON_CARRIER = store_thm(
  "INCOSET_EQUIV_ON_CARRIER",
  ``!g h. Group g /\ h <= g ==> inCoset g h equiv_on g.carrier``,
  SRW_TAC [][equiv_on_def] THEN METIS_TAC [INCOSET_REFL, INCOSET_SYM, INCOSET_TRANS]);

(* Define coset partitions of g.carrier by inCoset g h. *)
val CosetPartition_def = Define `
  CosetPartition g h = partition (inCoset g h) g.carrier
`;

(* Theorem: For FINITE Group g, h <= g, compute CARD g.carrier by partition. *)
val CARD_CARRIER_BY_COSET_PARTITION = store_thm(
  "CARD_CARRIER_BY_COSET_PARTITION",
  ``!g h. Group g /\ h <= g /\ FINITE g.carrier ==>
    (CARD g.carrier = SIGMA CARD (CosetPartition g h))``,
  METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, partition_CARD]);

(* Theorem: Elements in CosetPartition are cosets of some a In g.carrier *)
val COSET_PARTITION_ELEMENT = store_thm(
  "COSET_PARTITION_ELEMENT",
  ``!g h. Group g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> ?a. a IN g.carrier /\ (e = coset g h.carrier a)``,
  SRW_TAC [][CosetPartition_def, inCoset_def, partition_def] THEN
  Q.EXISTS_TAC `x` THEN
  SRW_TAC [][coset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [Subgroup_def, SUBSET_DEF, group_mult_carrier]);

(* Theorem: For FINITE group, CARD element in CosetPartiton = CARD subgroup. *)
val CARD_COSET_PARTITION_ELEMENT = store_thm(
  "CARD_COSET_PARTITION_ELEMENT",
  ``!g h. FiniteGroup g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> (CARD e = CARD h.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?a. a IN g.carrier /\ (e = coset g h.carrier a)` by SRW_TAC [][COSET_PARTITION_ELEMENT] THEN
  `FINITE h.carrier` by METIS_TAC [Subgroup_def, SUBSET_FINITE] THEN
  METIS_TAC [CARD_subgroup_coset]);

(* Theorem: (Lagrange Identity) For FINITE Group g and subgroup h,
            (size of group) = (size of subgroup) * (size of coset partition). *)
val LAGRANGE_IDENTITY = store_thm(
  "LAGRANGE_IDENTITY",
  ``!g h. FiniteGroup g /\ h <= g ==>
   (CARD g.carrier = CARD h.carrier * CARD (CosetPartition g h))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `FINITE (CosetPartition g h)` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  METIS_TAC [CARD_CARRIER_BY_COSET_PARTITION, SIGMA_CARD_CONSTANT, CARD_COSET_PARTITION_ELEMENT, FiniteGroup_def]);

(* ------------------------------------------------------------------------- *)
(* Theory of Group action                                                    *)
(* ------------------------------------------------------------------------- *)

(* An action from group G to a set X is a map f: GxX -> X such that
   (0)   [is a map] f (x in G)(z in X) in X
   (1)  [id action] f (e in G)(z in X) = z
   (2) [composable] f (x in G)(f (y in G)(z in X)) =
                    f ((mult in G)(x in G)(y in G))(z in X)
*)
val action_def = Define `
   action f g X = !z. z IN X ==>
     (!x :: (g.carrier). f x z IN X) /\
     (f #e z = z) /\
     (!x y :: (g.carrier). f x (f y z) = f (x * y) z)
`;

(* Theorem: For action f g X /\ x IN X, !a IN g.carrier, f a x IN X  *)
val ACTION_CLOSURE = store_thm(
  "ACTION_CLOSURE",
  ``!f g X x a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier ==> f a x IN X``,
  SRW_TAC [][action_def]);

(* Theorem: For action f g X /\ x IN X, !a,b IN g.carrier, f a (f b x) = f (a*b) x  *)
val ACTION_COMPOSE = store_thm(
  "ACTION_COMPOSE",
  ``!f g X x a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier /\ b IN g.carrier ==> (f a (f b x) = f (a*b) x)``,
  SRW_TAC [][action_def]);

(* Theorem: For action f g X /\ x IN X, f #e x = x  *)
val ACTION_ID = store_thm(
  "ACTION_ID",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==> (f #e x = x)``,
  SRW_TAC [][action_def]);
(* This is essentially REACH_REFL *)

(* Theorem: For action f g X /\ x IN X /\ y IN X, !a IN g.carrier, f a x = y ==> f |/a y = x.  *)
val ACTION_REVERSE = store_thm(
  "ACTION_REVERSE",
  ``!f g X x y a. Group g /\ action f g X /\ x IN X /\ y IN X /\ a IN g.carrier ==>
   (f a x = y) ==> (f (|/ a) y = x)``,
  REPEAT STRIP_TAC THEN
  `|/ a IN g.carrier` by SRW_TAC [][group_inv_carrier] THEN
  `f (|/ a) y = f (|/ a) (f a x)` by SRW_TAC [][] THEN
  `_ = f (|/a * a) x` by METIS_TAC [ACTION_COMPOSE, group_mult_carrier] THEN
  METIS_TAC [group_linv, ACTION_ID]);
(* This is essentially REACH_SYM *)

(* Theorem: For action f g X /\ x IN X /\ y IN X, z IN X,
            if f a x = y /\ f b y = z, then f (b*a) x = z.  *)
val ACTION_TRANS = store_thm(
  "ACTION_TRANS",
  ``!f g X x y a. Group g /\ action f g X /\ x IN X /\ y IN X /\ z IN X /\
      a IN g.carrier /\ b IN g.carrier ==>
   (f a x = y) /\ (f b y = z) ==> (f (b * a) x = z)``,
  METIS_TAC [ACTION_COMPOSE, group_mult_carrier]);
(* This is essentially REACH_TRANS *)

(* ------------------------------------------------------------------------- *)
(* Group action induces an equivalence relation.                             *)
(* ------------------------------------------------------------------------- *)

(* Define reach to relate two action points x y in X *)
val reach_def = Define`
  reach f g x y = ?z. z IN g.carrier /\ (f z x = y)
`;

(* Theorem: [Reach is Reflexive] !x in X, reach f g x x.  *)
val REACH_REFL = store_thm(
  "REACH_REFL",
  ``!f g X. Group g /\ action f g X ==> (!x. x IN X ==> reach f g x x)``,
  METIS_TAC [reach_def, ACTION_ID, group_id_carrier]);

(* Theorem: [Reach is Symmetric] !x y in X, reach f g x y ==> reach f g y x. *)
val REACH_SYM = store_thm(
  "REACH_SYM",
  ``!f g X. Group g /\ action f g X ==>
    (!x y. x IN X /\ y IN X ==> reach f g x y ==> reach f g y x)``,
  METIS_TAC [reach_def, ACTION_REVERSE, group_inv_carrier, group_linv]);

(* Theorem: [Reach is Transitive] !x y z in X, reach f g x y /\ reach f g y z ==> reach f g x z. *)
val REACH_TRANS = store_thm(
  "REACH_TRANS",
  ``!f g X. Group g /\ action f g X ==>
    (!x y z. x IN X /\ y IN X /\ z IN X ==> reach f g x y /\ reach f g y z ==> reach f g x z)``,
  SRW_TAC [][reach_def] THEN
  Q.EXISTS_TAC `z''*z'` THEN
  METIS_TAC [ACTION_TRANS, group_mult_carrier]);

(* Theorem: Reach is an equivalence relation on target set X. *)
val REACH_EQUIV_ON_TARGET = store_thm(
  "REACH_EQUIV_ON_TARGET",
  ``!f g X. Group g /\ action f g X ==> reach f g equiv_on X``,
  SRW_TAC [][equiv_on_def] THEN
  METIS_TAC [REACH_REFL, REACH_SYM, REACH_TRANS]);

(* ------------------------------------------------------------------------- *)
(* Partition by Group action.                                                *)
(* ------------------------------------------------------------------------- *)

(* Define partitions of Target set X by reach f g. *)
val TargetPartition_def = Define `
  TargetPartition f g X = partition (reach f g) X
`;

(* Theorem: For e IN (TargetPartition f g X), e <> EMPTY *)
val TARGET_PARTITION_ELEMENT_NONEMPTY = store_thm(
  "TARGET_PARTITION_ELEMENT_NONEMPTY",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> e <> {}``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, EMPTY_NOT_IN_partition]);

(* Theorem: Elements in Element of TargetPartition are in X. *)
val TARGET_PARTITION_ELEMENT_ELEMENT = store_thm(
  "TARGET_PARTITION_ELEMENT_ELEMENT",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> !a. a IN e ==> a IN X``,
  SRW_TAC [][TargetPartition_def, partition_def] THEN
  FULL_SIMP_TAC (srw_ss())[]);

(* Theorem: TargetPartition elements are subset of target *)
val TARGET_PARTITION_ELEMENT_SUBSET_TARGET = store_thm(
  "TARGET_PARTITION_ELEMENT_SUBSET_TARGET",
  ``!f g X. Group g /\ action f g X ==>
   !e. e IN TargetPartition f g X ==> e SUBSET X``,
  METIS_TAC [TARGET_PARTITION_ELEMENT_ELEMENT, SUBSET_DEF]);

(* Theorem: Target partition is FINITE *)
val FINITE_TARGET_PARTITION = store_thm(
  "FINITE_TARGET_PARTITION",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> FINITE (TargetPartition f g X)``,
  SRW_TAC [][TargetPartition_def, REACH_EQUIV_ON_TARGET, FINITE_partition]);

(* Theorem: For e IN (TargetPartition f g X), FINITE X ==> FINITE e *)
val FINITE_TARGET_PARTITION_ELEMENT = store_thm(
  "FINITE_TARGET_PARTITION_ELEMENT",
  ``!f g X. Group g /\ action f g X ==>
     !e. (e IN TargetPartition f g X) ==> FINITE X ==> FINITE e``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, FINITE_partition]);

(* Theorem: For FINITE X, CARD X = SUM of CARD partitions in (TargetPartition f g X) *)
val CARD_TARGET_BY_PARTITION = store_thm(
  "CARD_TARGET_BY_PARTITION",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==>
    (CARD X = SIGMA CARD (TargetPartition f g X))``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, partition_CARD]);

(* ------------------------------------------------------------------------- *)
(* Orbits as equivalence classes.                                            *)
(* ------------------------------------------------------------------------- *)

(* Orbit of action: those x in X that can be reached by a in X *)
val orbit_def = Define`
  orbit f g X a = {x | x IN X /\ reach f g a x }
`;

(* Theorem: y IN (orbit f g X x) ==> y IN X /\ reach f g x y *)
val ORBIT_PROPERTY = store_thm(
  "ORBIT_PROPERTY",
  ``!f g X x y. y IN orbit f g X x ==> y IN X /\ reach f g x y``,
  SRW_TAC [][orbit_def]);

(* Theorem: (orbit f g X x) = IMAGE (\a. f a x) g.carrier *)
val ORBIT_DESCRIPTION = store_thm(
  "ORBIT_DESCRIPTION",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==> (orbit f g X x = {f a x | a IN g.carrier})``,
  SRW_TAC [][action_def, orbit_def, reach_def, EXTENSION, EQ_IMP_THM] THEN METIS_TAC []);

(* Theorem: if a in g.carrier, then f x a in (orbit f g X a). *)
val ORBIT_HAS_ACTION_ELEMENT = store_thm(
  "ORBIT_HAS_ACTION_ELEMENT",
  ``!f g X x a. action f g X /\ x IN X /\ a IN g.carrier ==> f a x IN (orbit f g X x)``,
  SRW_TAC [][orbit_def, reach_def] THEN
  METIS_TAC [action_def]);

(* Theorem: action f g X ==> x IN orbit f g X x *)
val ACTION_ORBIT_HAS_ITSELF = store_thm(
  "ACTION_ORBIT_HAS_ITSELF",
  ``!f g X. Group g /\ action f g X /\ x IN X ==> x IN orbit f g X x``,
  SRW_TAC [][orbit_def] THEN
  METIS_TAC [REACH_REFL]);

(* Theorem: orbits are subsets of target set X *)
val ORBIT_SUBSET_TARGET = store_thm(
  "ORBIT_SUBSET_TARGET",
  ``!f g X a. action f g X /\ a IN X ==> orbit f g X a SUBSET X``,
  SRW_TAC [][orbit_def, SUBSET_DEF]);

(* Theorem: Elements of TargetPartition are orbits of its own element.
            !e IN TargetPartition f g X ==> !a IN e. e = orbit f g X a *)
val TARGET_PARTITION_ELEMENT_IS_ORBIT = store_thm(
  "TARGET_PARTITION_ELEMENT_IS_ORBIT",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> !a. a IN e ==> (e = orbit f g X a)``,
  SRW_TAC [][TargetPartition_def, partition_def, orbit_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [REACH_SYM, REACH_TRANS]);

(* Theorem: If for all x IN X, (orbit f g X x) = n, then n divides CARD X. *)
val EQUAL_SIZE_ORBITS_PROPERTY = store_thm(
  "EQUAL_SIZE_ORBITS_PROPERTY",
  ``!f g X n. Group g /\ action f g X /\ FINITE X /\
    (!x. x IN X ==> (CARD (orbit f g X x) = n)) ==> divides n (CARD X)``,
  REPEAT STRIP_TAC THEN
  `!e. e IN TargetPartition f g X ==> !x. x IN e ==> (e = orbit f g X x)` by SRW_TAC [][TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `!e. e IN TargetPartition f g X ==> (CARD e = n)` by SRW_TAC [][] THENL [
    `?y. y IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
    METIS_TAC [TARGET_PARTITION_ELEMENT_SUBSET_TARGET, SUBSET_DEF],
    `CARD X = n * CARD (partition (reach f g) X)` by METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, equal_partition_CARD] THEN
    METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_SYM]
  ]);

(* ------------------------------------------------------------------------- *)
(* Stabilizer subgroups.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Stabilizer of action: for a in X, stabilizer of a = {x in G | f x a = a} *)
val stabilizer_def = Define`
  stabilizer f g a = {x | x IN g.carrier /\ (f x a = a) }
`;

(* Theorem: x IN stabilizer f g a ==> x IN g.carrier and f x a = a. *)
val STABILIZER_PROPERTY = store_thm(
  "STABILIZER_PROPERTY",
  ``!f g X a. action f g X /\ a IN X ==> !x. x IN stabilizer f g a ==> x IN g.carrier /\ (f x a = a)``,
  SRW_TAC [][stabilizer_def]);

(* Define the Stabilizer Group of stabilizer set. *)
val StabilizerGroup_def = Define`
  StabilizerGroup f g a =
    <| carrier := stabilizer f g a;
            id := g.id;
           inv := |/ ;
          mult := g.mult
     |>`;

(* Theorem: If g is a Group, f g X is an action, StabilizerGroup f g a is a Group *)
val StabilizerGroup_group = store_thm(
  "StabilizerGroup_group",
  ``!f g X a. action f g X /\ a IN X /\ Group g ==> Group (StabilizerGroup f g a)``,
  SRW_TAC [][Group_def, StabilizerGroup_def, stabilizer_def, action_def, RES_FORALL_THM] THEN METIS_TAC []);

(* Theorem: The stabilizer is a subset of g.carrier *)
val STABILIZER_SUBSET = store_thm(
  "STABILIZER_SUBSET",
  ``!f g X. action f g X /\ a IN X ==> (stabilizer f g a) SUBSET g.carrier``,
  SRW_TAC [][stabilizer_def, SUBSET_DEF]);

(* Theorem: If g is Group, f g X is an action, then stabilizer is a subgroup of g *)
val StabilizerGroup_subgroup = store_thm(
  "StabilizerGroup_subgroup",
  ``!f g X. action f g X /\ a IN X /\ Group g ==> Subgroup (StabilizerGroup f g a) g``,
  SRW_TAC [][Subgroup_def, StabilizerGroup_def] THEN
  METIS_TAC [StabilizerGroup_def, StabilizerGroup_group, STABILIZER_SUBSET]);

(* ------------------------------------------------------------------------- *)
(* Orbit-Stabilizer Theorem.                                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The map from orbit to coset of stabilizer is well-defined. *)
val ORBIT_STABILIZER_MAP_WD = store_thm(
  "ORBIT_STABILIZER_MAP_WD",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==>
   !x y. x IN g.carrier /\ y IN g.carrier /\ (f x a = f y a) ==>
   (coset g (stabilizer f g a) x = coset g (stabilizer f g a) y)``,
  REPEAT STRIP_TAC THEN
  `StabilizerGroup f g a <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  FULL_SIMP_TAC (srw_ss()) [action_def] THEN
  `f (|/y * x) a = f (|/y) (f x a)` by SRW_TAC [][group_inv_carrier] THEN
  `_ = f (|/y) (f y a)` by ASM_REWRITE_TAC [] THEN
  `_ = f (|/y * y) a` by SRW_TAC [][group_inv_carrier] THEN
  `_ = f #e a` by SRW_TAC [][group_linv] THEN
  `_ = a` by SRW_TAC [][] THEN
  `(|/y * x) IN g.carrier` by SRW_TAC [][group_inv_carrier, group_mult_carrier] THEN
  `(|/y * x) IN (stabilizer f g a)` by SRW_TAC [][stabilizer_def] THEN
  METIS_TAC [SUBGROUP_COSET_EQ]);

(* Theorem: The map from orbit to coset of stabilizer is injective. *)
val ORBIT_STABILIZER_MAP_UQ = store_thm(
  "ORBIT_STABILIZER_MAP_UQ",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==>
   !x y. x IN g.carrier /\ y IN g.carrier /\
   (coset g (stabilizer f g a) x = coset g (stabilizer f g a) y) ==> (f x a = f y a)``,
  REPEAT STRIP_TAC THEN
  `StabilizerGroup f g a <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  `(|/y * x) IN (stabilizer f g a)` by METIS_TAC [SUBGROUP_COSET_EQ] THEN
  `f (|/y * x) a = a` by FULL_SIMP_TAC (srw_ss()) [stabilizer_def] THEN
  `|/y * x IN g.carrier` by SRW_TAC [][group_inv_carrier, group_mult_carrier] THEN
  `f x a = f (#e*x) a` by SRW_TAC [][group_lid] THEN
  `_ = f ((y* |/y)*x) a` by SRW_TAC [][group_rinv] THEN
  `_ = f (y*(|/y*x)) a` by SRW_TAC [][group_assoc, group_inv_carrier] THEN
  `_ = f y (f (|/y * x) a)` by FULL_SIMP_TAC (srw_ss()) [action_def] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)
(* Convert reach definition into a function                                  *)
(* ------------------------------------------------------------------------- *)

(* Existence of act_by:  reach x y ==> ?a. a IN g.carrier /\ f a x = y *)
val lemma = prove(
  ``!f g x y. ?a. reach f g x y ==> a IN g.carrier /\ (f a x = y)``,
  METIS_TAC [reach_def]);

val ACT_BY_DEF = new_specification(
    "ACT_BY_DEF",
    ["ACT_BY"],
    SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* Theorem: Points of (orbit x) and cosets of (stabilizer x) are one-to-one. *)
val ORBIT_STABILIZER_COSETS_BIJ = store_thm(
  "ORBIT_STABILIZER_COSETS_BIJ",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==>
   BIJ (\z.  coset g (stabilizer f g x) (ACT_BY f g x z))
       (orbit f g X x)
       (CosetPartition g (StabilizerGroup f g x))``,
  SRW_TAC [][CosetPartition_def, partition_def, inCoset_def, StabilizerGroup_def, BIJ_DEF, INJ_DEF, SURJ_DEF] THENL [
    `reach f g x z` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.EXISTS_TAC `ACT_BY f g x z` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    `x' IN IMAGE (\z'. ACT_BY f g x z * z') (stabilizer f g x)` by METIS_TAC [coset_def] THEN
    `?z'. z' IN (stabilizer f g x) /\ (x' = ACT_BY f g x z * z')` by METIS_TAC [IN_IMAGE] THEN
    METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier],
    `reach f g x z /\ reach f g x z'` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier /\ ACT_BY f g x z' IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    `f (ACT_BY f g x z) x = f (ACT_BY f g x z') x` by METIS_TAC [ORBIT_STABILIZER_MAP_UQ] THEN
    METIS_TAC [ACT_BY_DEF],
    `reach f g x z` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.EXISTS_TAC `ACT_BY f g x z` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    `x' IN IMAGE (\z'. ACT_BY f g x z * z') (stabilizer f g x)` by METIS_TAC [coset_def] THEN
    `?z'. z' IN (stabilizer f g x) /\ (x' = ACT_BY f g x z * z')` by METIS_TAC [IN_IMAGE] THEN
    METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier],
    Q.EXISTS_TAC `f x'' x` THEN
    SRW_TAC [][] THENL [
      `reach f g x (f x'' x)` by METIS_TAC [reach_def] THEN
      `f x'' x IN X` by METIS_TAC [ACTION_CLOSURE] THEN
      SRW_TAC [][orbit_def],
      `reach f g x (f x'' x)` by METIS_TAC [reach_def] THEN
      `ACT_BY f g x (f x'' x) IN g.carrier /\ (f (ACT_BY f g x (f x'' x)) x = (f x'' x))` by SRW_TAC [][ACT_BY_DEF] THEN
      `coset g (stabilizer f g x) (ACT_BY f g x (f x'' x)) = coset g (stabilizer f g x) x''` by METIS_TAC [ORBIT_STABILIZER_MAP_WD] THEN
      SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
      `x' IN IMAGE (\z. x'' * z) (stabilizer f g x)` by METIS_TAC [coset_def] THEN
      `?z. z IN (stabilizer f g x) /\ (x' = x'' * z)` by METIS_TAC [IN_IMAGE] THEN
      METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier]
    ]
  ]);

(* Theorem: [Orbit-Stabilizer Theorem] CARD G = CARD (orbit x) * CARD (stabilizer x) *)
val ORBIT_STABILIZER_THEOREM = store_thm(
  "ORBIT_STABILIZER_THEOREM",
  ``!f g X x. FiniteGroup g /\ action f g X /\ x IN X /\ FINITE X ==>
   (CARD g.carrier = CARD (orbit f g X x) * CARD (stabilizer f g x))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `StabilizerGroup f g x <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g x).carrier = stabilizer f g x` by SRW_TAC [][StabilizerGroup_def] THEN
  `FINITE (CosetPartition g (StabilizerGroup f g x))` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  `FINITE (orbit f g X x)` by METIS_TAC [ORBIT_DESCRIPTION, IMAGE_DEF, IMAGE_FINITE] THEN
  `CARD (CosetPartition g (StabilizerGroup f g x)) = CARD (orbit f g X x)` by METIS_TAC [ORBIT_STABILIZER_COSETS_BIJ, FINITE_BIJ_CARD_EQ] THEN
  `CARD g.carrier = CARD (stabilizer f g x) * CARD (CosetPartition g (StabilizerGroup f g x))` by METIS_TAC [FiniteGroup_def, LAGRANGE_IDENTITY] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* Fixed Points of action.                                                   *)
(* ------------------------------------------------------------------------- *)

(* Fixedpoints of action: f g X = {x in X | !z in G. f z x = x} *)
val fixedpoints_def = Define`
  fixedpoints f g X = {x | x IN X /\ (!z. z IN g.carrier ==> (f z x = x)) }
`;

(* Theorem: x IN (fixedpoints f g X) ==> x IN X *)
val FIXEDPOINTS_ELEMENT = store_thm(
  "FIXEDPOINTS_ELEMENT",
  ``!f g X. x IN (fixedpoints f g X) ==> x IN X``,
  SRW_TAC [][fixedpoints_def]);

(* Theorem: a IN fixedpoints f g X ==> (orbit f g X a = {a}) *)
val FIXPOINT_ORBIT_SING = store_thm(
  "FIXPOINT_ORBIT_SING",
  ``!f g X. Group g /\ action f g X ==>
     !a. a IN fixedpoints f g X ==> (orbit f g X a = {a})``,
  SRW_TAC [][fixedpoints_def, orbit_def, reach_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [group_id_carrier]);

(* Theorem: For Action f g X, a IN X, (orbit f g X a = {a}) ==> a IN fixedpoints f g X *)
val ORBIT_SING_FIXPOINT = store_thm(
  "ORBIT_SING_FIXPOINT",
  ``!f g X. action f g X ==>
    !a. a IN X /\ (orbit f g X a = {a}) ==> a IN fixedpoints f g X``,
  SRW_TAC [][action_def, fixedpoints_def, orbit_def, reach_def, EXTENSION] THEN
  METIS_TAC []);

(* Theorem: a IN fixedpoints f g X <=> SING (orbit f g X a}) *)
val FIXPOINT_ORBIT_IS_SING = store_thm(
  "FIXPOINT_ORBIT_IS_SING",
  ``!f g X. Group g /\ action f g X ==> !a. a IN X ==> (a IN fixedpoints f g X <=> SING (orbit f g X a))``,
  METIS_TAC [FIXPOINT_ORBIT_SING, ORBIT_SING_FIXPOINT, ACTION_ORBIT_HAS_ITSELF, SING_DEF, IN_SING]);

val singleorbits_def = Define`
    singleorbits f g X = { e | e IN (TargetPartition f g X) /\ SING e }
`;
val multiorbits_def = Define`
    multiorbits f g X = { e | e IN (TargetPartition f g X) /\ ~ SING e }
`;

(* Theorem: TargetPartition = singleorbits + multiorbits. *)
val TARGET_PARTITION_EQ_SING_MULTI = store_thm(
  "TARGET_PARTITION_EQ_SING_MULTI",
  ``!f g X. singleorbits f g X  = (TargetPartition f g X) DIFF (multiorbits f g X)``,
  SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION, EQ_IMP_THM]);

(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is surjective. *)
val SING_ORBITS_TO_FIXEDPOINTS_SURJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_SURJ",
  ``!f g X. Group g /\ action f g X ==> SURJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  SRW_TAC [][singleorbits_def, SURJ_DEF] THEN1
  METIS_TAC [CHOICE_SING, ORBIT_SING_FIXPOINT, TARGET_PARTITION_ELEMENT_IS_ORBIT, 
             TARGET_PARTITION_ELEMENT_ELEMENT, SING_DEF, IN_SING] THEN
  `orbit f g X x = {x}` by RW_TAC std_ss [FIXPOINT_ORBIT_SING] THEN
  Q.EXISTS_TAC `{x}` THEN
  FULL_SIMP_TAC (srw_ss()) [TargetPartition_def, partition_def, orbit_def] THEN
  METIS_TAC [FIXEDPOINTS_ELEMENT]);

(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is injective. *)
val SING_ORBITS_TO_FIXEDPOINTS_INJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_INJ",
  ``!f g X. Group g /\ action f g X ==> INJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  SRW_TAC [][singleorbits_def, INJ_DEF] THEN
  METIS_TAC [CHOICE_SING, ORBIT_SING_FIXPOINT, TARGET_PARTITION_ELEMENT_IS_ORBIT,
             TARGET_PARTITION_ELEMENT_ELEMENT, SING_DEF, IN_SING] THEN
  METIS_TAC [SING_DEF, CHOICE_SING]);

(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is bijective. *)
val SING_ORBITS_TO_FIXEDPOINTS_BIJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_BIJ",
  ``!f g X. Group g /\ action f g X ==> BIJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  RW_TAC std_ss [BIJ_DEF, SING_ORBITS_TO_FIXEDPOINTS_SURJ, SING_ORBITS_TO_FIXEDPOINTS_INJ]);

(* Theorem: For action f g X, singleorbits is the same size as fixedpoints f g X *)
val CARD_ORBITS_SING = store_thm(
  "CARD_ORBITS_SING",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> (CARD (singleorbits f g X) = CARD (fixedpoints f g X))``,
  REPEAT STRIP_TAC THEN
  `(singleorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][singleorbits_def, SUBSET_DEF] THEN
  `(fixedpoints f g X) SUBSET X` by SRW_TAC [][fixedpoints_def, SUBSET_DEF] THEN
  METIS_TAC [SING_ORBITS_TO_FIXEDPOINTS_BIJ, FINITE_BIJ_CARD_EQ, SUBSET_FINITE, FINITE_TARGET_PARTITION]);

(* Theorem: For action f g X, CARD X = CARD fixedpoints + SIGMA CARD multiorbits *)
val CARD_TARGET_BY_ORBITS = store_thm(
  "CARD_TARGET_BY_ORBITS",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> (CARD X = CARD (fixedpoints f g X) + SIGMA CARD (multiorbits f g X))``,
  REPEAT STRIP_TAC THEN
  `(singleorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][singleorbits_def, SUBSET_DEF] THEN
  `(multiorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][multiorbits_def, SUBSET_DEF] THEN
  `FINITE (singleorbits f g X) /\ FINITE (multiorbits f g X)` by METIS_TAC [FINITE_TARGET_PARTITION, SUBSET_FINITE] THEN
  `(singleorbits f g X) INTER (multiorbits f g X) = {}` by SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION] THEN1
  METIS_TAC [] THEN
  `(singleorbits f g X) UNION (multiorbits f g X) = TargetPartition f g X` 
    by SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION] THEN1
  METIS_TAC [] THEN
  `!e. e IN (singleorbits f g X) ==> (CARD e = 1)` by SRW_TAC [][singleorbits_def, SING_DEF] THEN1
  RW_TAC std_ss [CARD_SING] THEN
  `CARD X = SIGMA CARD ((singleorbits f g X) UNION (multiorbits f g X))` by RW_TAC std_ss [CARD_TARGET_BY_PARTITION] THEN
  `_ = SIGMA CARD (singleorbits f g X) + SIGMA CARD (multiorbits f g X)` by RW_TAC std_ss [SUM_IMAGE_UNION, SUM_IMAGE_THM] THEN
  `_ = 1 * CARD (singleorbits f g X) + SIGMA CARD (multiorbits f g X)` by RW_TAC std_ss [SIGMA_CARD_CONSTANT] THEN
  RW_TAC arith_ss [CARD_ORBITS_SING]);

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof via Group action.                                     *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The Group Zn = Addition Modulo n, for n > 0.                              *)
(* ------------------------------------------------------------------------- *)

(* Define Zn = Addition Modulo n Group *)
val Z_def = Define`
  Z n =
    <| carrier := count n;
            id := 0;
           inv := (\i. (n - i) MOD n);  (* so that inv 0 = 0 *)
          mult := (\i j. (i + j) MOD n)
     |>`;

(* Theorem: Zn carrier is finite *)
val FINITE_ZN_carrier = store_thm(
  "FINITE_ZN_carrier",
  ``!n. FINITE (Z n).carrier``,
  SRW_TAC [][Z_def, FINITE_COUNT]);

(* Theorem: CARD (Zn carrier) = n *)
val CARD_ZN_carrier = store_thm(
  "CARD_ZN_carrier",
  ``!n. CARD (Z n).carrier = n``,
  SRW_TAC [][Z_def, CARD_COUNT]);

(* Theorem: Zn is a group if n > 0. *)
val ZN_group = store_thm(
  "ZN_group",
  ``!n. 0 < n ==> Group (Z n)``,
  RW_TAC std_ss [Z_def, Group_def, count_def, GSPECIFICATION, RES_FORALL_THM, MOD_ADD_INV, MOD_ADD_ASSOC]);

(* Theorem: Zn is a FiniteGroup if n > 0. *)
val ZN_finite_group = store_thm(
  "ZN_finite_group",
  ``!n. 0 < n ==> FiniteGroup (Z n)``,
  METIS_TAC [ZN_group, FINITE_ZN_carrier, FiniteGroup_def]);

(* Theorem: For prime p and action f (Z p) X, e IN (multiorbits f (Z p) X) ==> CARD e = p. *)
val CARD_ZP_NONFIX_ORBIT = store_thm(
  "CARD_ZP_NONFIX_ORBIT",
  ``!p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
   !e. e IN (multiorbits f (Z p) X) ==> (CARD e = p)``,
  SRW_TAC [][multiorbits_def] THEN
  `Group (Z p)` by SRW_TAC [][ZN_group, dividesTheory.PRIME_POS] THEN
  `FINITE (Z p).carrier` by SRW_TAC [][Z_def, FINITE_COUNT] THEN
  `CARD (Z p).carrier = p` by SRW_TAC [][Z_def, CARD_COUNT] THEN
  `FiniteGroup (Z p)` by SRW_TAC [][FiniteGroup_def] THEN
  `FINITE e` by METIS_TAC [FINITE_TARGET_PARTITION_ELEMENT] THEN
  `CARD e <> 1` by METIS_TAC [SING_IFF_CARD1] THEN
  `?a. a IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
  `a IN X` by METIS_TAC [TARGET_PARTITION_ELEMENT_ELEMENT] THEN
  `e = orbit f (Z p) X a` by SRW_TAC [][TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `p = CARD e * CARD (stabilizer f (Z p) a)` by METIS_TAC [ORBIT_STABILIZER_THEOREM] THEN
  `divides (CARD e) p` by METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_COMM] THEN
  METIS_TAC [dividesTheory.prime_def]);

(* Theorem: [Fixed-Point Congruence for Zp action]
            For prime p and action f (Z p) X and FINITE X,
            (CARD X = CARD (fixedpoints f (Z p) X)) (mod p). *)
val ZP_FIXEDPOINTS_CONGRUENCE = store_thm(
  "ZP_FIXEDPOINTS_CONGRUENCE",
  ``!p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
     (CARD X MOD p = CARD (fixedpoints f (Z p) X) MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `Group (Z p)` by SRW_TAC [][ZN_group] THEN
  `!e. e IN (multiorbits f (Z p) X) ==> (CARD e = p)`
      by METIS_TAC [CARD_ZP_NONFIX_ORBIT] THEN
  `(multiorbits f (Z p) X) SUBSET (TargetPartition f (Z p) X)`
      by SRW_TAC [][multiorbits_def, SUBSET_DEF] THEN
  `FINITE (multiorbits f (Z p) X)`
      by METIS_TAC [SUBSET_FINITE, FINITE_TARGET_PARTITION] THEN
  `CARD X = CARD (fixedpoints f (Z p) X) + SIGMA CARD (multiorbits f (Z p) X)`
      by SRW_TAC [][CARD_TARGET_BY_ORBITS] THEN
  `_ = CARD (fixedpoints f (Z p) X) + p * CARD (multiorbits f (Z p) X)`
      by SRW_TAC [][SIGMA_CARD_CONSTANT] THEN
  `_ = CARD (multiorbits f (Z p) X) * p  + CARD (fixedpoints f (Z p) X)`
      by RW_TAC arith_ss [] THEN
  METIS_TAC [arithmeticTheory.MOD_TIMES]);

(* Theorem: Cycle is a group action from Zn to Necklace n a. *)
val CYCLE_ACTION = store_thm(
  "CYCLE_ACTION",
  ``!n a. 0 < n /\ 0 < a ==> action cycle (Z n) (Necklace n a)``,
  SRW_TAC [][action_def, Necklace_def, Z_def, RES_FORALL_THM] THEN
  METIS_TAC [CYCLE_0, CYCLE_SAME_LENGTH, CYCLE_SAME_SET, CYCLE_ADDITION,
             LENGTH_NIL, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Theorem: A monocoloured necklace l has cycle 1 l = l *)
val monocoloured_cycle1 = prove(
  ``!n a l. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> (cycle 1 l = l)``,
  SRW_TAC [][monocoloured_def] THEN
  `l <> []` by METIS_TAC [NECKLACE_NONNIL] THEN
  METIS_TAC [SING_HAS_CYCLE_1]);

(* Theorem: If a necklace l has cycle 1 l = l , it must be monocoloured *)
val cycle1_monocoloured = prove(
  ``!l n a. 0 < a /\ l IN Necklace n a /\ (cycle 1 l = l) ==> l IN monocoloured n a``,
  SRW_TAC [][monocoloured_def, Necklace_def] THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* Theorem: A Necklace l is monocoloured iff cycle 1 l = 1 *)
val monocoloured_iff_cycle1 = store_thm(
  "monocoloured_iff_cycle1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN Necklace n a ==>
     (l IN monocoloured n a <=> (cycle 1 l = l))``,
  METIS_TAC [monocoloured_Necklace, cycle1_monocoloured, monocoloured_cycle1]);

(* ------------------------------------------------------------------------- *)
(* Fixed Points of cycle are monocoloured necklaces.                         *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The fixedpoints of cycle are necklaces of cycle 1 l = l. *)
val CYCLE_FIXPOINTS = store_thm(
  "CYCLE_FIXPOINTS",
  ``!n a. 0 < n /\ 0 < a  ==>
    (fixedpoints cycle (Z n) (Necklace n a) = {l | l IN (Necklace n a) /\ (cycle 1 l = l)} )``,
  SRW_TAC [][fixedpoints_def, Necklace_def, Z_def, EXTENSION, EQ_IMP_THM] THEN1
  (Cases_on `LENGTH x = 1` THEN1 METIS_TAC [MONO_HAS_CYCLE_1] THEN
   `1 < LENGTH x` by RW_TAC arith_ss [] THEN METIS_TAC []) THEN
  METIS_TAC [CYCLE_1_FIX]);

(* Theorem: The fixedpoints of cycle are monocoloured necklaces. *)
val CYCLE_FIXPOINTS_ARE_MONOCOLOURED = store_thm(
  "CYCLE_FIXPOINTS_ARE_MONOCOLOURED",
  ``!n a. 0 < n /\ 0 < a  ==>
    (fixedpoints cycle (Z n) (Necklace n a) = (monocoloured n a) )``,
  REPEAT STRIP_TAC THEN
  `fixedpoints cycle (Z n) (Necklace n a) = {l | l IN (Necklace n a) /\ (cycle 1 l = l)}`
    by SRW_TAC [][CYCLE_FIXPOINTS] THEN
  SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [monocoloured_Necklace, monocoloured_iff_cycle1]);

(* Theorem: (Fermat's Little Theorem by Group action) For prime p, a**p = a (mod p). *)
(* Proof:
   !p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
     (CARD X MOD p = CARD (fixedpoints f (Z p) X) MOD p)      by ZP_FIXEDPOINTS_CONGRUENCE
   !n a. 0 < n /\ 0 < a  ==>
     (fixedpoints cycle (Z n) (Necklace n a) = (monocoloured n a) )
                                                              by CYCLE_FIXPOINTS_ARE_MONOCOLOURED
   !n a. 0 < n /\ 0 < a ==> action cycle (Z n) (Necklace n a) by CYCLE_ACTION
   !n a. FINITE (Necklace n a)                                by FINITE_Necklace
   !n a. CARD (Necklace n a) = a ** n                         by CARD_Necklace
   !n. 0 < n ==> (CARD (monocoloured n a) = a)                by CARD_monocoloured
*)
val FERMAT_LITTLE_THM = store_thm(
  "FERMAT_LITTLE_THM",
  ``!p a. prime p ==> (a**p MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Cases_on `a = 0` THEN1 METIS_TAC [arithmeticTheory.EXP_EQ_0, arithmeticTheory.ZERO_MOD] THEN
  `0 < a` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ACTION, FINITE_Necklace, CARD_Necklace, CARD_monocoloured,
             CYCLE_FIXPOINTS_ARE_MONOCOLOURED, ZP_FIXEDPOINTS_CONGRUENCE]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
