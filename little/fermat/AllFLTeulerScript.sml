(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Group-theoretic Proof.                          *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Group Theory)
======================================
Given a finite group G, consider an element a in G.

Since G is finite, element a has an order: (order a), and a^(order a) = e.

This also means that, the generated subgroup <a> has size (order a).

By Lagrange identity, size of group = k * size of subgroup.

Hence, |G| = k * |<a>|, and a^|<a>| = e.

This implies:   a^|G| = a^(k*|<a>|) = a^(|<a>*k) = (a^|<a>|)^k = e^k = e.

This is the group equivalent of Fermat's Little Theorem.

By putting G = Z*p, a IN Z*p means 0 < a < p,
then a^|G| mod p = 1, or a^(p-1) mod p = 1.

By putting G = Phi*n = {a | a < n /\ gcd(a,n) = 1 },
then a^|G| mod n = 1, or a^phi(n) mod n = 1.

which is Euler's generalization of Fermat's Little Theorem.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "AllFLTeuler";

(* open dependent theories *)
open pred_setTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* Theorem: If n > 0, (m MOD n = 0) <=> divides n m *)
val MOD_0_DIVIDES = store_thm(
  "MOD_0_DIVIDES",
  ``!n m. 0 < n ==> ((m MOD n = 0) <=> divides n m)``,
  SRW_TAC [][dividesTheory.divides_def, EQ_IMP_THM] THEN
  METIS_TAC [arithmeticTheory.DIVISION, arithmeticTheory.ADD_0, arithmeticTheory.MOD_EQ_0]);

(* Theorem: mutliplication is associative in MOD *)
val MOD_MULT_ASSOC = store_thm(
  "MOD_MULT_ASSOC",
  ``!n x y z. 0 < n /\ x < n /\ z < n ==> (((x * y) MOD n * z) MOD n = (x * (y * z) MOD n) MOD n)``,
  REPEAT STRIP_TAC THEN
  `((x * y) MOD n * z) MOD n = ((x * y) MOD n * z MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (((x * y) * z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((x * (y * z)) MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (x MOD n * (y * z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
  RW_TAC arith_ss []);

(* Theorem: If !e IN s, CARD e = k, SIGMA CARD s = CARD s * k. *)
val SIGMA_CARD_CONSTANT = store_thm(
  "SIGMA_CARD_CONSTANT",
  ``!k s. FINITE s ==> (!e. e IN s ==> (CARD e = k)) ==> (SIGMA CARD s = k * (CARD s))``,
  STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][] THEN1 METIS_TAC [SUM_IMAGE_THM] THEN
  `CARD e = k` by SRW_TAC [][] THEN
  `SIGMA CARD (e INSERT s) = CARD e + SIGMA CARD (s DELETE e)` by SRW_TAC [][SUM_IMAGE_THM] THEN
  `s DELETE e = s` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `SUC (CARD s) = 1 + CARD s` by RW_TAC arith_ss [] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  RW_TAC arith_ss []);

(* Part 2: General Theory -------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Theory of Groups.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Set up group type as a record
   A Group has:
   . a carrier set (set = function 'a -> bool, since MEM is a boolean function)
   . an identity element
   . an inverse function (unary operation)
   . a product function called multiplication (binary operation)
*)
val _ = Hol_datatype`
  group = <| carrier: 'a -> bool;
                  id: 'a;
                 inv: 'a -> 'a;
                mult: 'a -> 'a -> 'a
           |>`;

(* set overloading  *)
val _ = overload_on ("*", ``g.mult``);
val _ = overload_on ("|/", ``g.inv``);
val _ = overload_on ("#e", ``g.id``);

(* Group definition:
   A Group is a set with elements of g of type 'a group, such that
   . g.id is in the carrier set (e in G)
   . (g.mult x y) is in the carrier set (closure: x*y in G)
   . (g.inv x) is in the carrier set (existence of inverse: x^-1 in G)
   . (g.mult (g.inv x) x) = (g.id)  (property of inverse and identity: (x^-1)*(x) = e)
   . (g.mult (g.mult x y) z) = (g.mult x (g.mult y z)) (associativity: (x*y)*z = x*(y*z))
*)
(* Define Group by predicate *)
val Group_def = Define`
  Group (g: 'a group) =
    #e IN g.carrier /\
    (!x y:: (g.carrier). x * y IN g.carrier) /\
    (!x:: (g.carrier). |/x IN g.carrier) /\
    (!x:: (g.carrier). #e * x = x) /\
    (!x:: (g.carrier). |/x * x = #e) /\
    (!x y z:: (g.carrier). (x * y) * z = x * (y * z))
  `;

(* Finite Group: a Group with a finite carrier set. *)
val FiniteGroup_def = Define`
  FiniteGroup (g: 'a group) =
    Group g /\ FINITE g.carrier
  `;

(* ------------------------------------------------------------------------- *)
(* Basic theorem from definition.                                            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Group identity is an element. *)
val group_id_carrier = store_thm(
  "group_id_carrier",
  ``!g. Group g ==> #e IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: Group inverse is an element. *)
val group_inv_carrier = store_thm(
  "group_inv_carrier",
  ``!g. Group g ==> !x :: (g.carrier). |/ x IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group closure] Group product is an element. *)
val group_mult_carrier = store_thm(
  "group_mult_carrier",
  ``!g. Group g ==> !x y :: (g.carrier). x * y IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left Identity] e x = x *)
val group_lid = store_thm(
  "group_lid",
  ``!g. Group g ==> !x :: (g.carrier). #e * x = x``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left inverse] x' x = e *)
val group_linv = store_thm(
  "group_linv",
  ``!g. Group g ==> !x :: (g.carrier). |/x * x = #e``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group associativity] (x y) z = x (y z) *)
val group_assoc = store_thm(
  "group_assoc",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y) * z = x * (y * z)``,
  SRW_TAC [][Group_def]);

(* Export rewrites after using Group_def, don't use Group_def anymore *)
val _ = export_rewrites ["group_id_carrier", "group_inv_carrier", "group_mult_carrier"];

(* ------------------------------------------------------------------------- *)
(* Theorems in basic Group Theory.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Group right inverse] x x' = e *)
val group_rinv = store_thm(
  "group_rinv",
  ``!g. Group g ==> !x :: (g.carrier). x * |/x = #e``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * |/x = #e * (x * |/x)`       by SRW_TAC [][group_lid] THEN
  `_ = (#e * x) * |/x`             by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x * x * |/x`    by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (|/x * x) * |/x`  by SRW_TAC [][GSYM group_assoc] THEN
  `_ = |/(|/x) * #e * |/x`         by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (#e * |/x)`       by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x`              by SRW_TAC [][group_lid] THEN
  `_ = #e`                         by SRW_TAC [][group_linv] THEN
  SRW_TAC [][]);

(* Theorem: [Group right identity] x e = x *)
val group_rid = store_thm(
  "group_rid",
  ``!g. Group g ==> !x :: (g.carrier). x * #e = x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * #e = x * (|/x * x)` by SRW_TAC [][group_linv] THEN
  `_ = (x * |/x) * x`      by SRW_TAC [][group_assoc] THEN
  `_ = #e * x`             by SRW_TAC [][group_rinv] THEN
  SRW_TAC [][group_lid]);

(* Theorem: [Solve left unknown] x y = z  <=> x = z y' *)
val group_lsolve = store_thm(
  "group_lsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (x = z * |/y)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_rid]);

(* Theorem: [Solve right unknown] x y = z  <=> y = x' z *)
val group_rsolve = store_thm(
  "group_rsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (y = |/x * z)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_lid]);

(* Theorem: [Left cancellation] x y = x z <=> y = z *)
val group_lcancel = store_thm(
  "group_lcancel",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y = x * z) = (y = z)``,
  METIS_TAC [group_inv_carrier, group_mult_carrier, group_assoc, group_rsolve, group_linv, group_lid]);

(* Theorem: [Right identity unique] x y = x <=> y = e *)
val group_rid_unique = store_thm(
  "group_rid_unique",
  ``!g. Group g ==> !x y :: (g.carrier). (x * y = x) = (y = #e)``,
  METIS_TAC [group_inv_carrier, group_rsolve, group_linv]);

(* Theorem: [Left inverse unique] x y = e <=> x = y' *)
val group_linv_unique = store_thm(
  "group_linv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (x = |/y)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_lsolve, group_linv, group_lid]);

(* Theorem: [Right inverse unique] x y = e <=> y = x' *)
val group_rinv_unique = store_thm(
  "group_rinv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (y = |/x)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_rsolve, group_rinv, group_rid]);

(* Theorem: [Inverse of identity] e' = e *)
val group_inv_id = store_thm(
  "group_inv_id",
  ``!g. Group g ==> (|/ #e = g.id)``,
  METIS_TAC [group_id_carrier, group_lid, group_linv_unique]);

(* Theorem: [Inverse of product] (x y)' = y' x' *)
val group_inv_mult = store_thm(
  "group_inv_mult",
  ``!g. Group g ==> !x y :: (g.carrier). |/(x * y) = |/y * |/x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `(x * y) * (|/y * |/x) = x * (y * |/y) * |/x` by SRW_TAC [][group_assoc] THEN
  `_ = #e`                                      by SRW_TAC [][group_rinv, group_rid] THEN
  POP_ASSUM MP_TAC THEN
  SRW_TAC [][group_rinv_unique]);

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Theory of Group Exponentiation.                                           *)
(* ------------------------------------------------------------------------- *)

(* Define exponents of a group element:
   For x in Group g,   group_exp x 0 = g.id
                       group_exp x (SUC n) = g.mult x (group_exp x n)
*)
val group_exp_def = Define`
  (group_exp g x 0 = g.id) /\
  (group_exp g x (SUC n) = x * (group_exp g x n))
`;

(* set overloading  *)
val _ = overload_on ("**", ``group_exp g``);

(* Theorem: (x ** n) in g.carrier *)
val group_exp_carrier = store_thm(
  "group_exp_carrier",
  ``!g x n. Group g /\ x IN g.carrier ==> (x ** n) IN g.carrier``,
  Induct_on `n` THEN METIS_TAC [group_exp_def, group_id_carrier, group_mult_carrier]);

(* Theorem: x ** 1 = x *)
val group_exp_one = store_thm(
  "group_exp_one",
  ``!g. Group g /\ x IN g.carrier ==> (x ** 1 = x)``,
  REPEAT STRIP_TAC THEN
  `1 = SUC 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_def, group_rid]);

(* Theorem: (g.id ** n) = g.id  *)
val group_id_exp = store_thm(
  "group_id_exp",
  ``!g n. Group g ==> (g.id ** n = #e)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_lid, group_id_carrier]);

(* Theorem: For abelian group g,  (x ** n) * y = y * (x ** n) *)
val group_comm_exp = store_thm(
  "group_comm_exp",
  ``!g x y n. Group g /\ x IN g.carrier /\ y IN g.carrier ==>
    (x * y = y * x) ==> ((x ** n) * y = y * (x ** n))``,
  Induct_on `n` THEN
  SRW_TAC [][group_exp_def, group_lid, group_rid] THEN
  METIS_TAC [group_assoc, group_exp_carrier]);

(* Theorem: (x ** n) * x = x * (x ** n) *)
val group_exp_comm = store_thm(
  "group_exp_comm",
  ``!g x n. Group g /\ x IN g.carrier ==> ((x ** n) * x = x * (x ** n))``,
  METIS_TAC [group_comm_exp]);

(* Theorem: For abelian group, (x * y) ** n = (x ** n) * (y ** n) *)
val group_mult_exp = store_thm(
  "group_mult_exp",
  ``!g x y n. Group g /\ x IN g.carrier /\ y IN g.carrier /\ (x * y = y * x) ==>
    ((x * y) ** n = (x ** n) * (y ** n))``,
  Induct_on `n` THEN1 METIS_TAC [group_exp_def, group_lid, group_id_carrier] THEN
  SRW_TAC [][group_exp_def] THEN
  `x * y * (x * y) ** n = (x*y)*((x**n)*(y**n))` by METIS_TAC [group_mult_carrier] THEN
  `_ = x*(y*((x**n)*(y**n)))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  `_ = x*((y*(x**n))*(y**n))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  `_ = x*(((x**n)*y)*(y**n))` by METIS_TAC [group_mult_carrier, group_exp_carrier, group_comm_exp] THEN
  `_ = x*((x**n)*(y*(y**n)))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier]);

(* Theorem: x ** (m + n) = (x ** m) * (x ** n) *)
val group_exp_add = store_thm(
  "group_exp_add",
  ``!g x m n. Group g /\ x IN g.carrier ==> (x ** (m + n) = (x ** m) * (x ** n))``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_lid, group_id_carrier, group_exp_carrier] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m + n = SUC (m+n)` by RW_TAC arith_ss [] THEN
  `x ** (SUC m + n) = x ** (SUC (m+n))` by SRW_TAC [][] THEN
  `_ = x * (x ** (m + n))` by SRW_TAC [][group_exp_def] THEN
  METIS_TAC [group_assoc, group_exp_carrier]);

(* Theorem: x ** (m * n) = (x ** m) ** n  *)
val group_exp_mult = store_thm(
  "group_exp_mult",
  ``!g x m n. Group g /\ x IN g.carrier ==> (x ** (m * n) = (x ** m) ** n)``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_id_exp] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m * n = m * n + n` by METIS_TAC [arithmeticTheory.MULT] THEN
  `x ** (SUC m * n) = x ** (m*n + n)` by SRW_TAC [][] THEN
  `_ = (x**(m*n))*(x ** n)` by SRW_TAC [][group_exp_add] THEN
  `_ = ((x**m)**n)*(x**n)` by METIS_TAC [] THEN
  `_ = ((x**m)*x)**n` by SRW_TAC [][group_mult_exp, group_exp_comm, group_exp_carrier] THEN
  SRW_TAC [][group_exp_comm]);

(* Theorem: Inverse of exponential: (x**n)' = (x')**n  *)
val group_exp_inv = store_thm(
  "group_exp_inv",
  ``!g x n. Group g /\ x IN g.carrier ==> (|/ (x ** n) = (|/ x) ** n)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_inv_id, group_inv_carrier, group_inv_mult,
             group_exp_carrier, group_exp_comm]);

(* Theorem: For m < n, x**m = x**n ==> x**(n-m) = #e *)
val group_exp_eq = store_thm(
  "group_exp_eq",
  ``!g x m n.Group g /\ x IN g.carrier /\ m < n /\ (x**m = x**n) ==> (x**(n-m) = #e)``,
  REPEAT STRIP_TAC THEN
  `?d. (d = n - m) /\ 0 < d /\ (n = m + d)` by RW_TAC arith_ss [] THEN
  `x**n = x**(m+d)` by SRW_TAC [][] THEN
  `_ = x**m * x**d` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_rid_unique, group_exp_carrier]);

(* ------------------------------------------------------------------------- *)
(* Theory of Subgroup (via Cosets).                                          *)
(* ------------------------------------------------------------------------- *)

(* A Subgroup is a subset of a group that's a group itself, keeping id, inv, mult. *)
val Subgroup_def = Define `
  Subgroup h g =
    Group h /\ Group g /\
    h.carrier SUBSET g.carrier /\
    (h.id = #e) /\ (h.inv = |/) /\ (h.mult = g.mult)
`;

(* set overloading *)
val _ = overload_on ("<=", ``Subgroup``);

(* Theorem: subgroup property *)
val subgroup_property = store_thm(
  "subgroup_property",
  ``!g h. Group g /\ h <= g ==> (Group h) /\ (h.mult = g.mult) /\ (h.id = g.id) /\ (h.inv = g.inv)``,
  SRW_TAC [][Subgroup_def]);

(* Define coset of subgroup with an element a. *)
val coset_def = Define `
  coset g X a = IMAGE (\z. g.mult a z) X
`;

(* Theorem: For x IN (coset h g.carrier a) ==> ?y IN g.carrier /\ a*y = x *)
val SUBGROUP_COSET_ELEMENT = store_thm(
  "SUBGROUP_COSET_ELEMENT",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN coset g h.carrier a ==> ?y. y IN h.carrier /\ (x = a*y)``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For all x IN h.carrier, a*x IN coset g h.carrier a. *)
val SUBGROUP_COSET_PROPERTY = store_thm(
  "SUBGROUP_COSET_PROPERTY",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN h.carrier ==> a*x IN coset g h.carrier a``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For h <= g, x IN coset g h.carrier x *)
val SUBGROUP_COSET_NONEMPTY = store_thm(
  "SUBGROUP_COSET_NONEMPTY",
  ``!g h x. Group g /\ h <= g /\ x IN g.carrier ==> x IN coset g h.carrier x``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rid, group_id_carrier]);

(* Theorem: There is a bijection between subgroup and its cosets. *)
val subgroup_to_coset_bij = store_thm(
  "subgroup_to_coset_bij",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==>
   BIJ (\x. a*x) h.carrier (coset g h.carrier a)``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY, SUBGROUP_COSET_ELEMENT, group_lcancel, Subgroup_def, SUBSET_DEF]);

(* Theorem: All cosets of subgroup are of the same size as the subgroup *)
val CARD_subgroup_coset = store_thm(
  "CARD_subgroup_coset",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier /\ FINITE h.carrier ==>
   (CARD (coset g h.carrier a) = CARD (h.carrier))``,
  REPEAT STRIP_TAC THEN
  `BIJ (\x. a*x) h.carrier (coset g h.carrier a)` by SRW_TAC [][subgroup_to_coset_bij] THEN
  `FINITE (coset g h.carrier a)` by METIS_TAC [coset_def, IMAGE_FINITE] THEN
  METIS_TAC [FINITE_BIJ_CARD_EQ]);

(* ------------------------------------------------------------------------- *)
(* Langrange's Theorem by Subgroups and Cosets                               *)
(* ------------------------------------------------------------------------- *)

(* Define b ~ a  when  b IN (coset g h.carrier a) *)
val inCoset_def = Define `
  inCoset g h a b = b IN (coset g h.carrier a)
`;

(* Theorem: inCoset is Reflexive *)
val INCOSET_REFL = store_thm(
  "INCOSET_REFL",
  ``!g h. Group g /\ h <= g ==> !a. a IN g.carrier ==> inCoset g h a a``,
  METIS_TAC [inCoset_def, SUBGROUP_COSET_NONEMPTY]);

(* Theorem: inCoset is Symmetric *)
val INCOSET_SYM = store_thm(
  "INCOSET_SYM",
  ``!g h. Group g /\ h <= g ==>
   !a b. a IN g.carrier /\ b IN g.carrier
       ==> (inCoset g h a b ==> inCoset g h b a)``,
  SRW_TAC [][inCoset_def] THEN
  `?z. z IN h.carrier /\ (b = a * z)` by METIS_TAC [SUBGROUP_COSET_ELEMENT] THEN
  `|/ z IN h.carrier` by METIS_TAC [Subgroup_def, group_inv_carrier] THEN
  `z IN g.carrier /\ |/ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `b * |/ z = (a * z) * (|/ z)` by SRW_TAC [][] THEN
  `_ = a * (z * |/ z)` by SRW_TAC [][group_assoc] THEN
  `_ = a` by SRW_TAC [][group_rinv, group_rid] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is Transitive *)
val INCOSET_TRANS = store_thm(
  "INCOSET_TRANS",
  ``!g h. Group g /\ h <= g ==>
   !a b c. a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
       ==> (inCoset g h a b /\ inCoset g h b c ==> inCoset g h a c)``,
  SRW_TAC [][inCoset_def] THEN
  `?y. y IN h.carrier /\ (b = a * y) /\ ?z. z IN h.carrier /\ (c = b * z)` by SRW_TAC [][SUBGROUP_COSET_ELEMENT] THEN
  `y IN g.carrier /\ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `c = (a * y) * z` by SRW_TAC [][] THEN
  `_ = a * (y * z)` by SRW_TAC [][group_assoc] THEN
  `y*z IN h.carrier` by METIS_TAC [subgroup_property, group_mult_carrier] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is an equivalence relation *)
val INCOSET_EQUIV_ON_CARRIER = store_thm(
  "INCOSET_EQUIV_ON_CARRIER",
  ``!g h. Group g /\ h <= g ==> inCoset g h equiv_on g.carrier``,
  SRW_TAC [][equiv_on_def] THEN METIS_TAC [INCOSET_REFL, INCOSET_SYM, INCOSET_TRANS]);

(* Define coset partitions of g.carrier by inCoset g h. *)
val CosetPartition_def = Define `
  CosetPartition g h = partition (inCoset g h) g.carrier
`;

(* Theorem: For FINITE Group g, h <= g, compute CARD g.carrier by partition. *)
val CARD_CARRIER_BY_COSET_PARTITION = store_thm(
  "CARD_CARRIER_BY_COSET_PARTITION",
  ``!g h. Group g /\ h <= g /\ FINITE g.carrier ==>
    (CARD g.carrier = SIGMA CARD (CosetPartition g h))``,
  METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, partition_CARD]);

(* Theorem: Elements in CosetPartition are cosets of some a In g.carrier *)
val COSET_PARTITION_ELEMENT = store_thm(
  "COSET_PARTITION_ELEMENT",
  ``!g h. Group g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> ?a. a IN g.carrier /\ (e = coset g h.carrier a)``,
  SRW_TAC [][CosetPartition_def, inCoset_def, partition_def] THEN
  Q.EXISTS_TAC `x` THEN
  SRW_TAC [][coset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [Subgroup_def, SUBSET_DEF, group_mult_carrier]);

(* Theorem: For FINITE group, CARD element in CosetPartiton = CARD subgroup. *)
val CARD_COSET_PARTITION_ELEMENT = store_thm(
  "CARD_COSET_PARTITION_ELEMENT",
  ``!g h. FiniteGroup g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> (CARD e = CARD h.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?a. a IN g.carrier /\ (e = coset g h.carrier a)` by SRW_TAC [][COSET_PARTITION_ELEMENT] THEN
  `FINITE h.carrier` by METIS_TAC [Subgroup_def, SUBSET_FINITE] THEN
  METIS_TAC [CARD_subgroup_coset]);

(* Theorem: (Lagrange Theorem)
            For FINITE Group g, size of subgroup divides size of group. *)
val LAGRANGE_THM = store_thm(
  "LAGRANGE_THM",
  ``!g h. FiniteGroup g /\ h <= g ==> divides (CARD h.carrier) (CARD g.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `FINITE (CosetPartition g h)` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  `CARD g.carrier = SIGMA CARD (CosetPartition g h)` by SRW_TAC [][CARD_CARRIER_BY_COSET_PARTITION] THEN
  `_ = CARD h.carrier * CARD (CosetPartition g h)` by METIS_TAC [SIGMA_CARD_CONSTANT, CARD_COSET_PARTITION_ELEMENT, FiniteGroup_def] THEN
  METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_COMM]);

(* ------------------------------------------------------------------------- *)
(* Theory of Finite group element order.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For Group g and x IN g.carrier, if x**n are all distinct, g.carrier is INFINITE *)
val group_exp_all_distinct = store_thm(
  "group_exp_all_distinct",
  ``!g x. Group g /\ x IN g.carrier /\ (!m n. (x**m = x**n) ==> (m = n)) ==> INFINITE g.carrier``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `?c. c = CARD g.carrier` by SRW_TAC [][] THEN
  `c < SUC c` by RW_TAC arith_ss [] THEN
  `INJ (\n. x**n) (count (SUC c)) g.carrier`
    by SRW_TAC [][pred_setTheory.INJ_DEF, group_exp_carrier] THEN
  METIS_TAC [pred_setTheory.CARD_COUNT, pred_setTheory.PHP]);

(* Theorem: For FINITE Group g and x IN g.carrier, there is a k > 0 such that x**k = #e. *)
val group_exp_period_exists = store_thm(
  "group_exp_period_exists",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> ?k. 0 < k /\ (x**k = #e)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?m n. m <> n /\ (x**m = x**n)` by METIS_TAC [group_exp_all_distinct] THEN
  Cases_on `m < n` THENL [
    `0 < n-m` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [group_exp_eq]);

(* Define order = LEAST period for an element x in Group g *)
val period_def = Define`
  period g x k = 0 < k /\ (x**k = g.id)
`;
val order_def = Define`
  order g x = LEAST k. period g x k
`;

(* Theorem: The finite group order is indeed a period. *)
val group_order_period = store_thm(
  "group_order_period",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> period g x (order g x)``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LEAST_INTRO]);

(* Theorem: If k < order of finite group, then k is not a period. *)
val group_order_minimal = store_thm(
  "group_order_minimal",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> !k. k < (order g x) ==> ~ period g x k``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LESS_LEAST]);

(* Theorem: The finite group order m satisfies: 0 < m and x**m = #e. *)
val group_order_property = store_thm(
  "group_order_property",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> 0 < order g x /\ (x ** order g x = #e)``,
  METIS_TAC [group_order_period, period_def]);

(* Theorem: For finite group, x' = x ** (m-1) where m = order g x *)
val group_order_inv = store_thm(
  "group_order_inv",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> (|/x = x**((order g x)-1))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `x ** 1 = x` by METIS_TAC [group_exp_one] THEN
  `(m-1)+1 = m` by RW_TAC arith_ss [] THEN
  `x ** (m-1) * x = #e` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_linv_unique, group_exp_carrier]);

(* Theorem: For FINITE Group g, x**n = x**(n mod m), where m = order g x *)
val group_exp_mod = store_thm(
  "group_exp_mod",
  ``!g x n. FiniteGroup g /\ x IN g.carrier ==> (x**n = x**(n MOD order g x))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `n = (n DIV m)*m + (n MOD m)` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = m*(n DIV m) + (n MOD m)` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_add, group_exp_mult, group_id_exp, group_lid, group_exp_carrier]);

(* Theorem: For FINITE group g, m, n < (order g x), x**m = x**n ==> m = n *)
val group_order_unique = store_thm(
  "group_order_unique",
  ``!g x m n. FiniteGroup g /\ x IN g.carrier /\ m < (order g x) /\ n < (order g x) ==>
    (x**m = x**n) ==> (m = n)``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  Cases_on `m < n` THENL [
    `0 < n-m /\ n-m < order g x` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n /\ m-n < order g x` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [FiniteGroup_def, group_exp_eq, group_order_minimal, period_def]);

(* ------------------------------------------------------------------------- *)
(* Theory of Generated Subgroup.                                             *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The Subgroup <a> of any element a of Group g.                             *)
(* ------------------------------------------------------------------------- *)

(* Define the generator group, the exponential group of an element a of group g *)
val Generated_def = Define`
  Generated g a =
    <| carrier := {x | ?k. x = a**k };
            id := g.id;
           inv := |/ ;
          mult := g.mult
     |>`;

(* Theorem: For a FINITE group g, the generated group of a in g.carrier is a group *)
val Generated_group = store_thm(
  "Generated_group",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> Group (Generated g a)``,
  SRW_TAC [][FiniteGroup_def] THEN
  SRW_TAC [][Group_def, Generated_def, RES_FORALL_THM] THENL [
    METIS_TAC [group_exp_def],
    METIS_TAC [group_exp_add],
    METIS_TAC [FiniteGroup_def, group_order_inv, group_exp_inv, group_exp_mult, group_inv_carrier],
    METIS_TAC [group_lid, group_exp_carrier],
    METIS_TAC [group_exp_inv, group_linv, group_rinv, group_mult_exp, group_inv_carrier, group_id_exp],
    SRW_TAC [][group_assoc, group_exp_carrier]
  ]);

(* Theorem: The generated group <a> for a IN G is subgroup of G. *)
val Generated_subgroup = store_thm(
  "Generated_subgroup",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> Subgroup (Generated g a) g``,
  SRW_TAC [][FiniteGroup_def, Subgroup_def] THEN1
  SRW_TAC [][FiniteGroup_def, Generated_group] THEN1
  (SRW_TAC [][Generated_def, SUBSET_DEF] THEN METIS_TAC [group_exp_carrier]) THEN
  SRW_TAC [][Generated_def]);

(* Theorem: There is a bijection from (count m) to (Generated g a), where m = order g x *)
val group_order_to_generated_bij = store_thm(
  "group_order_to_generated_bij",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==>
    BIJ (\n. a**n) (count (order g a)) (Generated g a).carrier``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF, Generated_def] THEN1
  METIS_TAC [] THEN1
  METIS_TAC [group_order_unique] THEN1
  METIS_TAC [] THEN1
  METIS_TAC [group_exp_mod, group_order_property, arithmeticTheory.MOD_LESS]);

(* Theorem: The order of the Generated_subgroup is the order of its element *)
val CARD_generated_subgroup = store_thm(
  "CARD_generated_subgroup",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> (CARD (Generated g a).carrier = order g a)``,
  METIS_TAC [Generated_subgroup, Subgroup_def, FiniteGroup_def, SUBSET_DEF, SUBSET_FINITE,
             group_order_to_generated_bij, FINITE_BIJ_CARD_EQ, FINITE_COUNT, CARD_COUNT]);

(* Theorem: [Euler-Fermat Theorem in Group Theory]
            For FiniteGroup g, a IN g.carrier ==> a**(CARD g) = #e *)
val FINITE_GROUP_FERMAT = store_thm(
  "FINITE_GROUP_FERMAT",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> (a**(CARD g.carrier) = #e)``,
  REPEAT STRIP_TAC THEN
  `Subgroup (Generated g a) g` by SRW_TAC [][Generated_subgroup] THEN
  `CARD (Generated g a).carrier = order g a` by SRW_TAC [][CARD_generated_subgroup] THEN
  `divides (order g a) (CARD g.carrier)` by METIS_TAC [LAGRANGE_THM] THEN
  `?k. CARD g.carrier = (order g a) * k` by METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_SYM] THEN
  `a ** (CARD g.carrier) = a ** ((order g a) * k)` by SRW_TAC [][] THEN
  METIS_TAC [FiniteGroup_def, group_exp_mult, group_order_property, group_id_exp]);

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Euler's generalization of Modulo Multiplicative Group for any modulo n.   *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Group-theoretic Proof appplied to E*n.                                    *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Relatively prime, or Coprime.                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If n > 0, k MOD n = 0 ==> !x. (k*x) MOD n = 0 *)
val MOD_MULITPLE_ZERO = store_thm(
  "MOD_MULITPLE_ZERO",
  ``!n k. 0 < n /\ (k MOD n = 0) ==> !x. ((k*x) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  METIS_TAC [arithmeticTheory.MOD_TIMES2, arithmeticTheory.MULT_0,
             arithmeticTheory.MULT_COMM, arithmeticTheory.ZERO_MOD]);

val _ = overload_on ("coprime", ``\x y. gcd x y = 1``);

(* Theorem: If 1 < n, !x. gcd n x = 1  ==> 0 < x /\ 0 < x MOD n *)
val MOD_NOZERO_WHEN_GCD_ONE = store_thm(
  "MOD_NOZERO_WHEN_GCD_ONE",
  ``!n. 1 < n ==> !x. (gcd n x = 1) ==> 0 < x /\ 0 < x MOD n``,
  NTAC 4 STRIP_TAC THEN CONJ_ASM1_TAC THENL [
    `1 <> n` by RW_TAC arith_ss [] THEN
    `x <> 0` by METIS_TAC [gcdTheory.GCD_0R] THEN
    RW_TAC arith_ss [],
    `1 <> n /\ x <> 0` by RW_TAC arith_ss [] THEN
    `?k q. k * x = q * n + 1` by METIS_TAC [gcdTheory.LINEAR_GCD] THEN
    `(k*x) MOD n = (q*n + 1) MOD n` by SRW_TAC [][] THEN
    `_ = 1` by METIS_TAC [arithmeticTheory.MOD_MULT] THEN
    SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
    `x MOD n = 0` by RW_TAC arith_ss [] THEN
    `0 < n` by RW_TAC arith_ss [] THEN
    `(x*k) MOD n = 0` by METIS_TAC [MOD_MULITPLE_ZERO] THEN
    `1 <> 0` by RW_TAC arith_ss [] THEN
    METIS_TAC [arithmeticTheory.MULT_COMM]
  ]);

(* Theorem: For n > 1, (gcd n x = 1) /\ (gcd n y = 1) ==> (gcd n (x*y) = 1) *)
val PRODUCT_WITH_GCD_ONE = store_thm(
  "PRODUCT_WITH_GCD_ONE",
  ``!n x y. 1 < n /\ (gcd n x = 1) /\ (gcd n y = 1) ==> (gcd n (x*y) = 1)``,
  METIS_TAC [gcdTheory.GCD_CANCEL_MULT]);

(* Theorem: For n > 1, (gcd n x = 1) ==> (gcd n (x MOD n) = 1) *)
val MOD_WITH_GCD_ONE = store_thm(
  "MOD_WITH_GCD_ONE",
  ``!n x. 1 < n /\ (gcd n x = 1) ==> (gcd n (x MOD n) = 1)``,
  REPEAT STRIP_TAC THEN
  `0 <> n` by RW_TAC arith_ss [] THEN
  METIS_TAC [gcdTheory.GCD_EFFICIENTLY, gcdTheory.GCD_SYM]);

(* Theorem: If 0 < a, 0 < b, g = gcd a b, then 0 < g and a MOD g = 0 and b MOD g = 0 *)
val GCD_DIVIDES = store_thm(
  "GCD_DIVIDES",
  ``!a b g. 0 < a /\ 0 < b /\ (g = gcd a b) ==> 0 < g /\ (a MOD g = 0) /\ (b MOD g = 0)``,
  REPEAT STRIP_TAC THEN1
  METIS_TAC [gcdTheory.GCD_EQ_0, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN1
 (`0 < g` by METIS_TAC [gcdTheory.GCD_EQ_0, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [gcdTheory.GCD_IS_GCD, gcdTheory.is_gcd_def, MOD_0_DIVIDES]) THEN
  `0 < g` by METIS_TAC [gcdTheory.GCD_EQ_0, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [gcdTheory.GCD_IS_GCD, gcdTheory.is_gcd_def, MOD_0_DIVIDES]);

(* Theorem: If 1 < n, gcd n x = 1 ==> ?k q. (k*x) MOD n = 1 /\ gcd n k = 1 *)
val GCD_ONE_PROPERTY = store_thm(
  "GCD_ONE_PROPERTY",
  ``!n x. 1 < n /\ (gcd n x = 1) ==> ?k. ((k*x) MOD n = 1) /\ (gcd n k = 1)``,
  REPEAT STRIP_TAC THEN
  `n <> 1` by RW_TAC arith_ss [] THEN
  `x <> 0` by METIS_TAC [gcdTheory.GCD_0R] THEN
  `?k q. k * x = q * n + 1` by METIS_TAC [gcdTheory.LINEAR_GCD] THEN
  `(k*x) MOD n = (q*n + 1) MOD n` by SRW_TAC [][] THEN
  `_ = 1` by METIS_TAC [arithmeticTheory.MOD_MULT] THEN
  `?g. g = gcd n k` by SRW_TAC [][] THEN
  `n <> 0 /\ q*n + 1 <> 0` by RW_TAC arith_ss [] THEN
  `k <> 0` by METIS_TAC [arithmeticTheory.MULT_EQ_0] THEN
  `0 < g /\ (n MOD g = 0) /\ (k MOD g = 0)` by METIS_TAC [GCD_DIVIDES, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  `divides g n /\ divides g k` by METIS_TAC [MOD_0_DIVIDES] THEN
  `divides g (n*q) /\ divides g (k*x)` by METIS_TAC [dividesTheory.DIVIDES_MULT] THEN
  `divides g (n*q+1)` by METIS_TAC [arithmeticTheory.MULT_COMM] THEN
  `divides g 1` by METIS_TAC [dividesTheory.DIVIDES_ADD_2] THEN
  METIS_TAC [dividesTheory.DIVIDES_ONE]);

(* ------------------------------------------------------------------------- *)
(* Establish the existence of multiplicative inverse when p is prime.        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For n > 1, (gcd n x = 1) ==> ?y. y*x MOD n = 1  *)
val GCD_MOD_MULT_INV = store_thm(
  "GCD_MOD_MULT_INV",
  ``!n x. 1 < n /\ (gcd n x = 1) /\ 0 < x /\ x < n ==>
   ?y. 0 < y /\ y < n /\ (gcd n y = 1) /\ ((y*x) MOD n = 1)``,
  REPEAT STRIP_TAC THEN
  `?k. ((k*x) MOD n = 1) /\ (gcd n k = 1)` by METIS_TAC [GCD_ONE_PROPERTY] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  `(k MOD n * x MOD n) MOD n = 1` by METIS_TAC [arithmeticTheory.MOD_TIMES2] THEN
  `((k MOD n) * x) MOD n = 1` by METIS_TAC [arithmeticTheory.LESS_MOD] THEN
  `k MOD n < n` by METIS_TAC [arithmeticTheory.MOD_LESS] THEN
  `1 <> 0` by RW_TAC arith_ss [] THEN
  `0 <> k MOD n` by METIS_TAC [MOD_MULITPLE_ZERO] THEN
  `0 < k MOD n` by RW_TAC arith_ss [] THEN
  METIS_TAC [MOD_WITH_GCD_ONE]);

(* Convert this into an existence definition *)
val lemma = prove(
  ``!n x. ?y. 1 < n /\ (gcd n x = 1) /\ 0 < x /\ x < n ==>
              0 < y /\ y < n /\ (gcd n y = 1) /\ ((y*x) MOD n = 1)``,
  METIS_TAC [GCD_MOD_MULT_INV]);

val GEN_MULT_INV_DEF = new_specification(
  "GEN_MULT_INV_DEF",
  ["GCD_MOD_MUL_INV"],
  SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* ------------------------------------------------------------------------- *)
(* Euler's set and totient function                                          *)
(* ------------------------------------------------------------------------- *)

val Euler_def = Define`
  Euler n = { i | 0 < i /\ i < n /\ (gcd n i = 1) }`;

val totient_def = Define`
  totient n = CARD (Euler n)`;

(* Define Multiplicative Modulo n Group *)
val Estar_def = Define`
  Estar n =
   <| carrier := Euler n;
           id := 1;
          inv := GCD_MOD_MUL_INV n;
         mult := (\i j. (i * j) MOD n)
    |>`;

(* Theorem: (Estar n) is a Group *)
val Estar_group = store_thm(
  "Estar_group",
  ``!n. 1 < n ==> Group (Estar n)``,
  SRW_TAC [][Estar_def, Euler_def, Group_def, RES_FORALL_THM, GEN_MULT_INV_DEF] THEN1
  METIS_TAC [PRODUCT_WITH_GCD_ONE, MOD_NOZERO_WHEN_GCD_ONE] THEN1
  RW_TAC arith_ss [arithmeticTheory.MOD_LESS] THEN1
  METIS_TAC [PRODUCT_WITH_GCD_ONE, MOD_WITH_GCD_ONE] THEN
  `0 < n` by RW_TAC arith_ss [] THEN METIS_TAC [MOD_MULT_ASSOC]);

(* Theorem: FINITE (Estar p).carrier *)
val FINITE_Estar_carrier = store_thm(
  "FINITE_Estar_carrier",
  ``!n. FINITE (Estar n).carrier``,
  SRW_TAC [][Estar_def, Euler_def] THEN
  `{i | 0 < i /\ i < n /\ (gcd n i = 1)} SUBSET count n` by SRW_TAC [][pred_setTheory.SUBSET_DEF] THEN
  METIS_TAC [pred_setTheory.FINITE_COUNT, pred_setTheory.SUBSET_FINITE]);

(* Theorem: group_exp (Estar n) a k = a**k MOD n *)
val Estar_exp = store_thm(
  "Estar_exp",
  ``!n a. 1 < n /\ a IN (Estar n).carrier ==> !k. group_exp (Estar n) a k = (a**k) MOD n``,
  REPEAT STRIP_TAC THEN
  `Group (Estar n)` by SRW_TAC [][Estar_group] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  Induct_on `k` THENL [
    `(Estar n).id = 1` by SRW_TAC [][Estar_def] THEN
    `1 MOD n = 1` by RW_TAC arith_ss [] THEN
    METIS_TAC [group_exp_def, arithmeticTheory.EXP],
    SRW_TAC [][group_exp_def, arithmeticTheory.EXP] THEN
    FULL_SIMP_TAC (srw_ss())[Estar_def, Euler_def] THEN
    `a MOD n = a` by RW_TAC arith_ss [] THEN
    METIS_TAC [arithmeticTheory.MOD_TIMES2]
  ]);

(* Theorem: For all a in Estar n, a**(totient n) MOD n = 1 *)
val EULER_FERMAT = store_thm(
  "EULER_FERMAT",
  ``!n a. 1 < n /\ 0 < a /\ a < n /\ (gcd n a = 1) ==> (a**(totient n) MOD n = 1)``,
  REPEAT STRIP_TAC THEN
  `FiniteGroup (Estar n)` by SRW_TAC [][Estar_group, FINITE_Estar_carrier, FiniteGroup_def] THEN
  `CARD (Estar n).carrier = totient n` by SRW_TAC [][Estar_def, totient_def] THEN
  `a IN (Estar n).carrier /\ ((Estar n).id = 1)` by SRW_TAC [][Estar_def, Euler_def] THEN
  `(Estar n).id = 1` by SRW_TAC [][Estar_def] THEN
  METIS_TAC [FINITE_GROUP_FERMAT, Estar_exp]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
