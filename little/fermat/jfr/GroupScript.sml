(* ------------------------------------------------------------------------- *)
(* Group theory for use in Fermat's Little Theorem.                          *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "Group";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
(* open pred_setTheory listTheory; *)
open pred_setTheory;

(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Theory of Groups                                                          *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Group Definition.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Set up group type as a record
   A Group has:
   . a carrier set (set = function 'a -> bool, since MEM is a boolean function)
   . an identity element
   . an inverse function (unary operation)
   . a product function called multiplication (binary operation)
*)
val _ = Hol_datatype`
  group = <| carrier: 'a -> bool;
                  id: 'a;
                 inv: 'a -> 'a;
                mult: 'a -> 'a -> 'a
           |>`;

(* set overloading  *)
val _ = overload_on ("*", ``g.mult``);
val _ = overload_on ("|/", ``g.inv``);
val _ = overload_on ("#e", ``g.id``);

(* Group Definition:
   A Group is a set with elements of g of type 'a group, such that
   . g.id is in the carrier set (e in G)
   . (g.mult x y) is in the carrier set (closure: x*y in G)
   . (g.inv x) is in the carrier set (existence of inverse: x^-1 in G)
   . (g.mult (g.inv x) x) = (g.id)  (property of inverse and identity: (x^-1)*(x) = e)
   . (g.mult (g.mult x y) z) = (g.mult x (g.mult y z)) (associativity: (x*y)*z = x*(y*z))
*)
(* Define Group by predicate *)
val Group_def = Define`
  Group (g: 'a group) =
    #e IN g.carrier /\
    (!x y:: (g.carrier). x * y IN g.carrier) /\
    (!x:: (g.carrier). |/x IN g.carrier) /\
    (!x:: (g.carrier). #e * x = x) /\
    (!x:: (g.carrier). |/x * x = #e) /\
    (!x y z:: (g.carrier). (x * y) * z = x * (y * z))
  `;

(* Finite Group: a Group with a finite carrier set. *)
val FiniteGroup_def = Define`
  FiniteGroup (g: 'a group) =
    Group g /\ FINITE g.carrier
  `;

(* ------------------------------------------------------------------------- *)
(* Basic theorem from definition.                                            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Group identity is an element. *)
val group_id_carrier = store_thm(
  "group_id_carrier",
  ``!g. Group g ==> #e IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: Group inverse is an element. *)
val group_inv_carrier = store_thm(
  "group_inv_carrier",
  ``!g. Group g ==> !x :: (g.carrier). |/ x IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group closure] Group product is an element. *)
val group_mult_carrier = store_thm(
  "group_mult_carrier",
  ``!g. Group g ==> !x y :: (g.carrier). x * y IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left Identity] e x = x *)
val group_lid = store_thm(
  "group_lid",
  ``!g. Group g ==> !x :: (g.carrier). #e * x = x``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left inverse] x' x = e *)
val group_linv = store_thm(
  "group_linv",
  ``!g. Group g ==> !x :: (g.carrier). |/x * x = #e``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group associativity] (x y) z = x (y z) *)
val group_assoc = store_thm(
  "group_assoc",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y) * z = x * (y * z)``,
  SRW_TAC [][Group_def]);

(* Export rewrites after using Group_def, don't use Group_def anymore *)
val _ = export_rewrites ["group_id_carrier", "group_inv_carrier", "group_mult_carrier"];

(* ------------------------------------------------------------------------- *)
(* Theorems in basic Group Theory.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Group right inverse] x x' = e *)
val group_rinv = store_thm(
  "group_rinv",
  ``!g. Group g ==> !x :: (g.carrier). x * |/x = #e``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * |/x = #e * (x * |/x)`       by SRW_TAC [][group_lid] THEN
  `_ = (#e * x) * |/x`             by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x * x * |/x`    by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (|/x * x) * |/x`  by SRW_TAC [][GSYM group_assoc] THEN
  `_ = |/(|/x) * #e * |/x`         by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (#e * |/x)`       by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x`              by SRW_TAC [][group_lid] THEN
  `_ = #e`                         by SRW_TAC [][group_linv] THEN
  SRW_TAC [][]);

(* Theorem: [Group right identity] x e = x *)
val group_rid = store_thm(
  "group_rid",
  ``!g. Group g ==> !x :: (g.carrier). x * #e = x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * #e = x * (|/x * x)` by SRW_TAC [][group_linv] THEN
  `_ = (x * |/x) * x`      by SRW_TAC [][group_assoc] THEN
  `_ = #e * x`             by SRW_TAC [][group_rinv] THEN
  SRW_TAC [][group_lid]);

(* Theorem: [Solve left unknown] x y = z  <=> x = z y' *)
val group_lsolve = store_thm(
  "group_lsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (x = z * |/y)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_rid]);

(* Theorem: [Solve right unknown] x y = z  <=> y = x' z *)
val group_rsolve = store_thm(
  "group_rsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (y = |/x * z)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_lid]);

(* Theorem: [Left cancellation] x y = x z <=> y = z *)
val group_lcancel = store_thm(
  "group_lcancel",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y = x * z) = (y = z)``,
  METIS_TAC [group_inv_carrier, group_mult_carrier, group_assoc, group_rsolve, group_linv, group_lid]);

(* Theorem: [Right identity unique] x y = x <=> y = e *)
val group_rid_unique = store_thm(
  "group_rid_unique",
  ``!g. Group g ==> !x y :: (g.carrier). (x * y = x) = (y = #e)``,
  METIS_TAC [group_inv_carrier, group_rsolve, group_linv]);

(* Theorem: [Left inverse unique] x y = e <=> x = y' *)
val group_linv_unique = store_thm(
  "group_linv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (x = |/y)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_lsolve, group_linv, group_lid]);

(* Theorem: [Right inverse unique] x y = e <=> y = x' *)
val group_rinv_unique = store_thm(
  "group_rinv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (y = |/x)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_rsolve, group_rinv, group_rid]);

(* Theorem: [Inverse of identity] e' = e *)
val group_inv_id = store_thm(
  "group_inv_id",
  ``!g. Group g ==> (|/ #e = g.id)``,
  METIS_TAC [group_id_carrier, group_lid, group_linv_unique]);

(* Theorem: [Inverse of inverse] x'' = x *)
val group_inv_inv = store_thm(
  "group_inv_inv",
  ``!g. Group g ==> !x :: (g.carrier). |/(|/x) = x``,
  METIS_TAC [group_inv_carrier, group_rinv, group_linv_unique]);

(* Theorem: [Inverse of product] (x y)' = y' x' *)
val group_inv_mult = store_thm(
  "group_inv_mult",
  ``!g. Group g ==> !x y :: (g.carrier). |/(x * y) = |/y * |/x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `(x * y) * (|/y * |/x) = x * (y * |/y) * |/x` by SRW_TAC [][group_assoc] THEN
  `_ = #e`                                      by SRW_TAC [][group_rinv, group_rid] THEN
  POP_ASSUM MP_TAC THEN
  SRW_TAC [][group_rinv_unique]);

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Theory of Group Exponentiation.                                           *)
(* ------------------------------------------------------------------------- *)

(* Define exponents of a group element:
   For x in Group g,   group_exp x 0 = g.id
                       group_exp x (SUC n) = g.mult x (group_exp x n)
*)
val group_exp_def = Define`
  (group_exp g x 0 = g.id) /\
  (group_exp g x (SUC n) = x * (group_exp g x n))
`;

(* set overloading  *)
val _ = overload_on ("**", ``group_exp g``);

(* Theorem: (x ** n) in g.carrier *)
val group_exp_carrier = store_thm(
  "group_exp_carrier",
  ``!g x n. Group g /\ x IN g.carrier ==> (x ** n) IN g.carrier``,
  Induct_on `n` THEN METIS_TAC [group_exp_def, group_id_carrier, group_mult_carrier]);

(* Theorem: x ** 1 = x *)
val group_exp_one = store_thm(
  "group_exp_one",
  ``!g. Group g /\ x IN g.carrier ==> (x ** 1 = x)``,
  REPEAT STRIP_TAC THEN
  `1 = SUC 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_def, group_rid]);

(* Theorem: (g.id ** n) = g.id  *)
val group_id_exp = store_thm(
  "group_id_exp",
  ``!g n. Group g ==> (g.id ** n = #e)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_lid, group_id_carrier]);

(* Theorem: For abelian group g,  (x ** n) * y = y * (x ** n) *)
val group_comm_exp = store_thm(
  "group_comm_exp",
  ``!g x y n. Group g /\ x IN g.carrier /\ y IN g.carrier ==>
    (x * y = y * x) ==> ((x ** n) * y = y * (x ** n))``,
  Induct_on `n` THEN
  SRW_TAC [][group_exp_def, group_lid, group_rid] THEN
  METIS_TAC [group_assoc, group_exp_carrier]);

(* Theorem: (x ** n) * x = x * (x ** n) *)
val group_exp_comm = store_thm(
  "group_exp_comm",
  ``!g x n. Group g /\ x IN g.carrier ==> ((x ** n) * x = x * (x ** n))``,
  METIS_TAC [group_comm_exp]);

(* Theorem: For abelian group, (x * y) ** n = (x ** n) * (y ** n) *)
val group_mult_exp = store_thm(
  "group_mult_exp",
  ``!g x y n. Group g /\ x IN g.carrier /\ y IN g.carrier /\ (x * y = y * x) ==>
    ((x * y) ** n = (x ** n) * (y ** n))``,
  Induct_on `n` THEN1 METIS_TAC [group_exp_def, group_lid, group_id_carrier] THEN
  SRW_TAC [][group_exp_def] THEN
  `x * y * (x * y) ** n = (x*y)*((x**n)*(y**n))` by METIS_TAC [group_mult_carrier] THEN
  `_ = x*(y*((x**n)*(y**n)))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  `_ = x*((y*(x**n))*(y**n))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  `_ = x*(((x**n)*y)*(y**n))` by METIS_TAC [group_mult_carrier, group_exp_carrier, group_comm_exp] THEN
  `_ = x*((x**n)*(y*(y**n)))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier]);

(* Theorem: x ** (m + n) = (x ** m) * (x ** n) *)
val group_exp_add = store_thm(
  "group_exp_add",
  ``!g x m n. Group g /\ x IN g.carrier ==> (x ** (m + n) = (x ** m) * (x ** n))``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_lid, group_id_carrier, group_exp_carrier] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m + n = SUC (m+n)` by RW_TAC arith_ss [] THEN
  `x ** (SUC m + n) = x ** (SUC (m+n))` by SRW_TAC [][] THEN
  `_ = x * (x ** (m + n))` by SRW_TAC [][group_exp_def] THEN
  METIS_TAC [group_assoc, group_exp_carrier]);

(* Theorem: x ** (m * n) = (x ** m) ** n  *)
val group_exp_mult = store_thm(
  "group_exp_mult",
  ``!g x m n. Group g /\ x IN g.carrier ==> (x ** (m * n) = (x ** m) ** n)``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_id_exp] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m * n = m * n + n` by METIS_TAC [arithmeticTheory.MULT] THEN
  `x ** (SUC m * n) = x ** (m*n + n)` by SRW_TAC [][] THEN
  `_ = (x**(m*n))*(x ** n)` by SRW_TAC [][group_exp_add] THEN
  `_ = ((x**m)**n)*(x**n)` by METIS_TAC [] THEN
  `_ = ((x**m)*x)**n` by SRW_TAC [][group_mult_exp, group_exp_comm, group_exp_carrier] THEN
  SRW_TAC [][group_exp_comm]);

(* Theorem: Inverse of exponential: (x**n)' = (|/ x)**n  *)
val group_exp_inv = store_thm(
  "group_exp_inv",
  ``!g x n. Group g /\ x IN g.carrier ==> (|/ (x ** n) = (|/ x) ** n)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_inv_id, group_inv_carrier, group_inv_mult,
             group_exp_carrier, group_exp_comm]);

(* Theorem: For m < n, x**m = x**n ==> x**(n-m) = #e *)
val group_exp_eq = store_thm(
  "group_exp_eq",
  ``!g x m n.Group g /\ x IN g.carrier /\ m < n /\ (x**m = x**n) ==> (x**(n-m) = #e)``,
  REPEAT STRIP_TAC THEN
  `?d. (d = n - m) /\ 0 < d /\ (n = m + d)` by RW_TAC arith_ss [] THEN
  `x**n = x**(m+d)` by SRW_TAC [][] THEN
  `_ = x**m * x**d` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_rid_unique, group_exp_carrier]);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
