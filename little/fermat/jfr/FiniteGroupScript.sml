(* ------------------------------------------------------------------------- *)
(* Finite Group theorems for use in Fermat's Little Theorem.                 *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FiniteGroup";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
(* open pred_setTheory listTheory; *)
open pred_setTheory;

(* ------------------------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* val _ = load "GroupTheory"; *)
open GroupTheory;

(* val _ = load "SubgroupTheory"; *)
open SubgroupTheory;

(* ------------------------------------------------------------------------- *)
(* Theory of Finite group element order.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For Group g and x IN g.carrier, if x**n are all distinct, g.carrier is INFINITE *)
val group_exp_all_distinct = store_thm(
  "group_exp_all_distinct",
  ``!g x. Group g /\ x IN g.carrier /\ (!m n. (x**m = x**n) ==> (m = n)) ==> INFINITE g.carrier``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `?c. c = CARD g.carrier` by SRW_TAC [][] THEN
  `c < SUC c` by RW_TAC arith_ss [] THEN
  `INJ (\n. x**n) (count (SUC c)) g.carrier`
    by SRW_TAC [][pred_setTheory.INJ_DEF, group_exp_carrier] THEN
  METIS_TAC [pred_setTheory.CARD_COUNT, pred_setTheory.PHP]);

(* Theorem: For FINITE Group g and x IN g.carrier, there is a k > 0 such that x**k = #e. *)
val group_exp_period_exists = store_thm(
  "group_exp_period_exists",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> ?k. 0 < k /\ (x**k = #e)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?m n. m <> n /\ (x**m = x**n)` by METIS_TAC [group_exp_all_distinct] THEN
  Cases_on `m < n` THENL [
    `0 < n-m` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [group_exp_eq]);

(* Define order = LEAST period for an element x in Group g *)
val period_def = Define`
  period g x k = 0 < k /\ (x**k = g.id)
`;
val order_def = Define`
  order g x = LEAST k. period g x k
`;

(* Theorem: The finite group order is indeed a period. *)
val group_order_period = store_thm(
  "group_order_period",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> period g x (order g x)``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LEAST_INTRO]);

(* Theorem: If k < order of finite group, then k is not a period. *)
val group_order_minimal = store_thm(
  "group_order_minimal",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> !k. k < (order g x) ==> ~ period g x k``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LESS_LEAST]);

(* Theorem: The finite group order m satisfies: 0 < m and x**m = #e. *)
val group_order_property = store_thm(
  "group_order_property",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> 0 < order g x /\ (x ** order g x = #e)``,
  METIS_TAC [group_order_period, period_def]);

(* Theorem: For finite group, x' = x ** (m-1) where m = order g x *)
val group_order_inv = store_thm(
  "group_order_inv",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> (|/x = x**((order g x)-1))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `x ** 1 = x` by METIS_TAC [group_exp_one] THEN
  `(m-1)+1 = m` by RW_TAC arith_ss [] THEN
  `x ** (m-1) * x = #e` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_linv_unique, group_exp_carrier]);

(* Theorem: For FINITE Group g, x**n = x**(n mod m), where m = order g x *)
val group_exp_mod = store_thm(
  "group_exp_mod",
  ``!g x n. FiniteGroup g /\ x IN g.carrier ==> (x**n = x**(n MOD order g x))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `n = (n DIV m)*m + (n MOD m)` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = m*(n DIV m) + (n MOD m)` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_add, group_exp_mult, group_id_exp, group_lid, group_exp_carrier]);

(* Theorem: For FINITE group g, m, n < (order g x), x**m = x**n ==> m = n *)
val group_order_unique = store_thm(
  "group_order_unique",
  ``!g x m n. FiniteGroup g /\ x IN g.carrier /\ m < (order g x) /\ n < (order g x) ==>
    (x**m = x**n) ==> (m = n)``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  Cases_on `m < n` THENL [
    `0 < n-m /\ n-m < order g x` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n /\ m-n < order g x` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [FiniteGroup_def, group_exp_eq, group_order_minimal, period_def]);

(* ------------------------------------------------------------------------- *)
(* Theory of Generated Subgroup.                                             *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The Subgroup <a> of any element a of Group g.                             *)
(* ------------------------------------------------------------------------- *)

(* Define the generator group, the exponential group of an element a of group g *)
val Generated_def = Define`
  Generated g a =
    <| carrier := {x | ?k. x = a**k };
            id := g.id;
           inv := |/ ;
          mult := g.mult
     |>`;

(* Theorem: For a FINITE group g, the generated group of a in g.carrier is a group *)
val Generated_group = store_thm(
  "Generated_group",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> Group (Generated g a)``,
  SRW_TAC [][FiniteGroup_def] THEN
  SRW_TAC [][Group_def, Generated_def, RES_FORALL_THM] THENL [
    METIS_TAC [group_exp_def],
    METIS_TAC [group_exp_add],
    METIS_TAC [FiniteGroup_def, group_order_inv, group_exp_inv, group_exp_mult, group_inv_carrier],
    METIS_TAC [group_lid, group_exp_carrier],
    METIS_TAC [group_exp_inv, group_linv, group_rinv, group_mult_exp, group_inv_carrier, group_id_exp],
    SRW_TAC [][group_assoc, group_exp_carrier]
  ]);

(* Theorem: The generated group <a> for a IN G is subgroup of G. *)
val Generated_subgroup = store_thm(
  "Generated_subgroup",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> Subgroup (Generated g a) g``,
  SRW_TAC [][FiniteGroup_def, Subgroup_def] THEN1
  SRW_TAC [][FiniteGroup_def, Generated_group] THEN1
  (SRW_TAC [][Generated_def, SUBSET_DEF] THEN METIS_TAC [group_exp_carrier]) THEN
  SRW_TAC [][Generated_def]);

(* Theorem: There is a bijection from (count m) to (Generated g a), where m = order g x *)
val group_order_to_generated_bij = store_thm(
  "group_order_to_generated_bij",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==>
    BIJ (\n. a**n) (count (order g a)) (Generated g a).carrier``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF, Generated_def] THEN1
  METIS_TAC [] THEN1
  METIS_TAC [group_order_unique] THEN1
  METIS_TAC [] THEN1
  METIS_TAC [group_exp_mod, group_order_property, arithmeticTheory.MOD_LESS]);

(* Theorem: The order of the Generated_subgroup is the order of its element *)
val CARD_generated_subgroup = store_thm(
  "CARD_generated_subgroup",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> (CARD (Generated g a).carrier = order g a)``,
  METIS_TAC [Generated_subgroup, Subgroup_def, FiniteGroup_def, SUBSET_DEF, SUBSET_FINITE,
             group_order_to_generated_bij, FINITE_BIJ_CARD_EQ, FINITE_COUNT, CARD_COUNT]);

(* Theorem: [Euler-Fermat Theorem in Group Theory]
            For FiniteGroup g, a IN g.carrier ==> a**(CARD g) = #e *)
val FINITE_GROUP_FERMAT = store_thm(
  "FINITE_GROUP_FERMAT",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> (a**(CARD g.carrier) = #e)``,
  REPEAT STRIP_TAC THEN
  `Subgroup (Generated g a) g` by SRW_TAC [][Generated_subgroup] THEN
  `CARD (Generated g a).carrier = order g a` by SRW_TAC [][CARD_generated_subgroup] THEN
  `divides (order g a) (CARD g.carrier)` by METIS_TAC [LAGRANGE_THM] THEN
  `?k. CARD g.carrier = (order g a) * k` by METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_SYM] THEN
  `a ** (CARD g.carrier) = a ** ((order g a) * k)` by SRW_TAC [][] THEN
  METIS_TAC [FiniteGroup_def, group_exp_mult, group_order_property, group_id_exp]);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
