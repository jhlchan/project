(* ------------------------------------------------------------------------- *)
(* Subgroup theorems for use in Fermat's Little Theorem.                     *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "Subgroup";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
(* open pred_setTheory listTheory; *)
open pred_setTheory;

(* ------------------------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* val _ = load "GroupTheory"; *)
open GroupTheory;

(* ------------------------------------------------------------------------- *)
(* Theory of Subgroups                                                       *)
(* ------------------------------------------------------------------------- *)

(* A Subgroup is a subset of a group that's a group itself, keeping id, inv, mult. *)
val Subgroup_def = Define `
  Subgroup h g =
    Group h /\ Group g /\
    h.carrier SUBSET g.carrier /\
    (h.id = #e) /\ (h.inv = |/) /\ (h.mult = g.mult)
`;

(* set overloading *)
val _ = overload_on ("<=", ``Subgroup``);

(* Theorem: subgroup property *)
val subgroup_property = store_thm(
  "subgroup_property",
  ``!g h. Group g /\ h <= g ==> (Group h) /\ (h.mult = g.mult) /\ (h.id = g.id) /\ (h.inv = g.inv)``,
  SRW_TAC [][Subgroup_def]);

(* ------------------------------------------------------------------------- *)
(* Cosets of a subgroup.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Define coset of subgroup with an element a. *)
val coset_def = Define `
  coset g X a = IMAGE (\z. g.mult a z) X
`;

(* Theorem: For x IN (coset h g.carrier a) ==> ?y IN g.carrier /\ a*y = x *)
val SUBGROUP_COSET_ELEMENT = store_thm(
  "SUBGROUP_COSET_ELEMENT",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN coset g h.carrier a ==> ?y. y IN h.carrier /\ (x = a*y)``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For all x IN h.carrier, a*x IN coset g h.carrier a. *)
val SUBGROUP_COSET_PROPERTY = store_thm(
  "SUBGROUP_COSET_PROPERTY",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN h.carrier ==> a*x IN coset g h.carrier a``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For h <= g, x IN coset g h.carrier x *)
val SUBGROUP_COSET_NONEMPTY = store_thm(
  "SUBGROUP_COSET_NONEMPTY",
  ``!g h x. Group g /\ h <= g /\ x IN g.carrier ==> x IN coset g h.carrier x``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rid, group_id_carrier]);

(* Theorem: For h <= g, y IN coset g h.carrier x ==> ?z IN h.carrier /\ x = y*z *)
val SUBGROUP_COSET_RELATE = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier /\ y IN coset g h.carrier x ==>
   ?z. z IN h.carrier /\ (x = y*z)``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rinv, group_rid, group_inv_carrier, group_assoc]);

(* Theorem: For h <= g, |/y * x in h.carrier ==> coset g h.carrier x = coset g h.carrier y. *)
val SUBGROUP_COSET_EQ1 = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   (|/y * x) IN h.carrier ==> (coset g h.carrier x = coset g h.carrier y)``,
  SRW_TAC [][coset_def, Subgroup_def, SUBSET_DEF, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THENL [
    `|/y * (x * z) = (|/y * x)* z` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    `|/y * (x * z) IN h.carrier` by METIS_TAC [group_mult_carrier] THEN
    `y * (|/y * (x * z)) = x * z` by METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier] THEN
    METIS_TAC [],
    `|/x * (y * z) = (|/x * y)* z` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    `|/(|/y * x) = |/x * y` by METIS_TAC [group_inv_mult, group_inv_inv, group_inv_carrier] THEN
    `|/x * (y * z) IN h.carrier` by METIS_TAC [group_mult_carrier, group_inv_carrier] THEN
    `x * (|/x * (y * z)) = y * z` by METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier] THEN
    METIS_TAC []
  ]);

(* Theorem: For h <= g, coset g h.carrier x = coset g h.carrier y ==> |/y * x in h.carrier. *)
val SUBGROUP_COSET_EQ2 = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   (coset g h.carrier x = coset g h.carrier y) ==> (|/y * x) IN h.carrier``,
  REPEAT STRIP_TAC THEN
  `y IN coset g h.carrier y` by SRW_TAC [][SUBGROUP_COSET_NONEMPTY] THEN
  `y IN coset g h.carrier x` by SRW_TAC [][] THEN
  `?z. z IN h.carrier /\ (x = y*z)` by SRW_TAC [][SUBGROUP_COSET_RELATE] THEN
  METIS_TAC [group_rsolve, Subgroup_def, SUBSET_DEF]);

(* Theorem: For h <= g, coset g h.carrier x = coset g h.carrier y iff |/y * x in h.carrier *)
val SUBGROUP_COSET_EQ = store_thm(
  "SUBGROUP_COSET_EQ",
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   ((coset g h.carrier x = coset g h.carrier y) <=> (|/y * x) IN h.carrier)``,
  METIS_TAC [SUBGROUP_COSET_EQ1, SUBGROUP_COSET_EQ2]);

(* Theorem: There is a bijection between subgroup and its cosets. *)
val subgroup_to_coset_bij = store_thm(
  "subgroup_to_coset_bij",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==>
   BIJ (\x. a*x) h.carrier (coset g h.carrier a)``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY, SUBGROUP_COSET_ELEMENT, group_lcancel, Subgroup_def, SUBSET_DEF]);

(* Theorem: All cosets of subgroup are of the same size as the subgroup *)
val CARD_subgroup_coset = store_thm(
  "CARD_subgroup_coset",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier /\ FINITE h.carrier ==>
   (CARD (coset g h.carrier a) = CARD (h.carrier))``,
  REPEAT STRIP_TAC THEN
  `BIJ (\x. a*x) h.carrier (coset g h.carrier a)` by SRW_TAC [][subgroup_to_coset_bij] THEN
  `FINITE (coset g h.carrier a)` by METIS_TAC [coset_def, IMAGE_FINITE] THEN
  METIS_TAC [FINITE_BIJ_CARD_EQ]);

(* ------------------------------------------------------------------------- *)
(* Langrange's Theorem by Subgroups and Cosets                               *)
(* ------------------------------------------------------------------------- *)

(* Define b ~ a  when  b IN (coset g h.carrier a) *)
val inCoset_def = Define `
  inCoset g h a b = b IN (coset g h.carrier a)
`;

(* Theorem: inCoset is Reflexive. *)
val INCOSET_REFL = store_thm(
  "INCOSET_REFL",
  ``!g h. Group g /\ h <= g ==> !a. a IN g.carrier ==> inCoset g h a a``,
  METIS_TAC [inCoset_def, SUBGROUP_COSET_NONEMPTY]);

(* Theorem: inCoset is Symmetric. *)
val INCOSET_SYM = store_thm(
  "INCOSET_SYM",
  ``!g h. Group g /\ h <= g ==>
   !a b. a IN g.carrier /\ b IN g.carrier
       ==> (inCoset g h a b ==> inCoset g h b a)``,
  SRW_TAC [][inCoset_def] THEN
  `?z. z IN h.carrier /\ (b = a * z)` by METIS_TAC [SUBGROUP_COSET_ELEMENT] THEN
  `|/ z IN h.carrier` by METIS_TAC [Subgroup_def, group_inv_carrier] THEN
  `z IN g.carrier /\ |/ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `b * |/ z = (a * z) * (|/ z)` by SRW_TAC [][] THEN
  `_ = a * (z * |/ z)` by SRW_TAC [][group_assoc] THEN
  `_ = a` by SRW_TAC [][group_rinv, group_rid] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is Transitive. *)
val INCOSET_TRANS = store_thm(
  "INCOSET_TRANS",
  ``!g h. Group g /\ h <= g ==>
   !a b c. a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
       ==> (inCoset g h a b /\ inCoset g h b c ==> inCoset g h a c)``,
  SRW_TAC [][inCoset_def] THEN
  `?y. y IN h.carrier /\ (b = a * y) /\ ?z. z IN h.carrier /\ (c = b * z)` by SRW_TAC [][SUBGROUP_COSET_ELEMENT] THEN
  `y IN g.carrier /\ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `c = (a * y) * z` by SRW_TAC [][] THEN
  `_ = a * (y * z)` by SRW_TAC [][group_assoc] THEN
  `y*z IN h.carrier` by METIS_TAC [subgroup_property, group_mult_carrier] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is an equivalence relation. *)
val INCOSET_EQUIV_ON_CARRIER = store_thm(
  "INCOSET_EQUIV_ON_CARRIER",
  ``!g h. Group g /\ h <= g ==> inCoset g h equiv_on g.carrier``,
  SRW_TAC [][equiv_on_def] THEN METIS_TAC [INCOSET_REFL, INCOSET_SYM, INCOSET_TRANS]);

(* Define coset partitions of g.carrier by inCoset g h. *)
val CosetPartition_def = Define `
  CosetPartition g h = partition (inCoset g h) g.carrier
`;

(* Theorem: For FINITE Group g, h <= g, compute CARD g.carrier by partition. *)
val CARD_CARRIER_BY_COSET_PARTITION = store_thm(
  "CARD_CARRIER_BY_COSET_PARTITION",
  ``!g h. Group g /\ h <= g /\ FINITE g.carrier ==>
    (CARD g.carrier = SIGMA CARD (CosetPartition g h))``,
  METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, partition_CARD]);

(* Theorem: Elements in CosetPartition are cosets of some a In g.carrier *)
val COSET_PARTITION_ELEMENT = store_thm(
  "COSET_PARTITION_ELEMENT",
  ``!g h. Group g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> ?a. a IN g.carrier /\ (e = coset g h.carrier a)``,
  SRW_TAC [][CosetPartition_def, inCoset_def, partition_def] THEN
  Q.EXISTS_TAC `x` THEN
  SRW_TAC [][coset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [Subgroup_def, SUBSET_DEF, group_mult_carrier]);

(* Theorem: For FINITE group, CARD element in CosetPartiton = CARD subgroup. *)
val CARD_COSET_PARTITION_ELEMENT = store_thm(
  "CARD_COSET_PARTITION_ELEMENT",
  ``!g h. FiniteGroup g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> (CARD e = CARD h.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?a. a IN g.carrier /\ (e = coset g h.carrier a)` by SRW_TAC [][COSET_PARTITION_ELEMENT] THEN
  `FINITE h.carrier` by METIS_TAC [Subgroup_def, SUBSET_FINITE] THEN
  METIS_TAC [CARD_subgroup_coset]);

(* Theorem: (Lagrange Identity) For FINITE Group g and subgroup h,
            (size of group) = (size of subgroup) * (size of coset partition). *)
val LAGRANGE_IDENTITY = store_thm(
  "LAGRANGE_IDENTITY",
  ``!g h. FiniteGroup g /\ h <= g ==>
   (CARD g.carrier = CARD h.carrier * CARD (CosetPartition g h))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `FINITE (CosetPartition g h)` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  METIS_TAC [CARD_CARRIER_BY_COSET_PARTITION, SIGMA_CARD_CONSTANT, CARD_COSET_PARTITION_ELEMENT, FiniteGroup_def]);

(* Theorem: (Lagrange Theorem)
            For FINITE Group g, size of subgroup divides size of group. *)
val LAGRANGE_THM = store_thm(
  "LAGRANGE_THM",
  ``!g h. FiniteGroup g /\ h <= g ==> divides (CARD h.carrier) (CARD g.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `FINITE (CosetPartition g h)` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  `CARD g.carrier = SIGMA CARD (CosetPartition g h)` by SRW_TAC [][CARD_CARRIER_BY_COSET_PARTITION] THEN
  `_ = CARD h.carrier * CARD (CosetPartition g h)` by METIS_TAC [SIGMA_CARD_CONSTANT, CARD_COSET_PARTITION_ELEMENT, FiniteGroup_def] THEN
  METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_COMM]);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
