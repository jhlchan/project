(* ------------------------------------------------------------------------- *)
(* Simple theorems for use in Fermat's Little Theorem.                       *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "Simple";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Simple arithmetic.                                                        *)
(* ------------------------------------------------------------------------- *)

(* LESS_EQ_SUC_REFL: m <= SUC m *)

(* Theorem: n < SUC n *)
val LESS_SUC = store_thm(
  "LESS_SUC",
  ``!n. n < SUC n``,
  DECIDE_TAC);

(* Theorem: 0 < k ==> ?h. k = SUC h *)
val LESS_EQ_SUC = store_thm(
  "LESS_EQ_SUC",
  ``!k. 0 < k ==> ?h. k = SUC h``,
  METIS_TAC [arithmeticTheory.NOT_ZERO_LT_ZERO, arithmeticTheory.num_CASES]);

(* Theorem: m < n ==> m <> n *)
val LESS_NOT_EQ = store_thm(
  "LESS_NOT_EQ",
  ``!m n. m < n ==> m <> n``,
  DECIDE_TAC);

(* ------------------------------------------------------------------------- *)
(* Modulo arithmetic.                                                        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If n > 0, (m MOD n = 0) <=> divides n m *)
val MOD_0_DIVIDES = store_thm(
  "MOD_0_DIVIDES",
  ``!n m. 0 < n ==> ((m MOD n = 0) <=> divides n m)``,
  SRW_TAC [][dividesTheory.divides_def, EQ_IMP_THM] THEN
  METIS_TAC [arithmeticTheory.DIVISION, arithmeticTheory.ADD_0, arithmeticTheory.MOD_EQ_0]);

(* Theorem: If n > 0, a MOD n = b MOD n ==> (a - b) MOD n = 0 *)
val MOD_EQ_DIFF = store_thm(
  "MOD_EQ_DIFF",
  ``!n a b. 0 < n /\ (a MOD n = b MOD n) ==> ((a - b) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  `a = (a DIV n)*n + a MOD n` by METIS_TAC [arithmeticTheory.DIVISION] THEN
  `b = (b DIV n)*n + b MOD n` by METIS_TAC [arithmeticTheory.DIVISION] THEN
  `a - b = (a DIV n)*n - (b DIV n)*n` by RW_TAC arith_ss [] THEN
  `_ = ((a DIV n) - (b DIV n))*n` by RW_TAC arith_ss [] THEN
  METIS_TAC [arithmeticTheory.MOD_EQ_0]);
(* Note: The reverse is true only when a >= b:
         (a-b) MOD n = 0 cannot imply a MOD n = b MOD n *)

(* Theorem: if n > 0, a >= b, then (a - b) MOD n = 0 <=> a MOD n = b MOD n *)
val MOD_EQ = store_thm(
  "MOD_EQ",
  ``!n a b. 0 < n /\ b <= a ==> (((a - b) MOD n = 0) <=> (a MOD n = b MOD n))``,
  SRW_TAC [][EQ_IMP_THM] THEN1 (
    `?k. (a-b) = k*n` by METIS_TAC [MOD_0_DIVIDES, dividesTheory.divides_def] THEN
    `a = k*n + b` by RW_TAC arith_ss [] THEN
    METIS_TAC [arithmeticTheory.MOD_TIMES]) THEN
  SRW_TAC [][MOD_EQ_DIFF]);

(* Theorem: (x + y + z) MOD n = (x MOD n + y MOD n + z MOD n) MOD n *)
val MOD_PLUS3 = store_thm(
  "MOD_PLUS3",
  ``!n. 0 < n ==> !x y z. (x + y + z) MOD n = (x MOD n + y MOD n + z MOD n) MOD n``,
  METIS_TAC [arithmeticTheory.MOD_PLUS, arithmeticTheory.MOD_MOD]);

(* Theorem: For n > 0, (j MOD n * k) MOD n = (j * k) MOD n *)
val MOD_TIMES_TWO = store_thm(
  "MOD_TIMES_TWO",
  ``!n. 0 < n ==> !j k. (j MOD n * k) MOD n = (j * k) MOD n``,
  REPEAT STRIP_TAC THEN
  `(j MOD n * k) MOD n = (((j MOD n) MOD n) * (k MOD n)) MOD n`
    by SRW_TAC [][arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((j MOD n) * (k MOD n)) MOD n` by SRW_TAC [][arithmeticTheory.MOD_MOD] THEN
  SRW_TAC [][arithmeticTheory.MOD_TIMES2]);

(* Theorem: If n > 0, ((n - x) MOD n + x) MOD n = 0  for x < n. *)
val MOD_ADD_INV = store_thm(
  "MOD_ADD_INV",
  ``!n. 0 < n /\ x < n ==> (((n - x) MOD n + x) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  `((n - x) MOD n + x) MOD n = ((n-x) MOD n + x MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = ((n-x) + x) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  RW_TAC arith_ss []);

(* Theorem: Addition is associative in MOD: if x, y, z all < n,
            ((x + y) MOD n + z) MOD n = (x + (y + z) MOD n) MOD n. *)
val MOD_ADD_ASSOC = store_thm(
  "MOD_ADD_ASSOC",
  ``!n. 0 < n ==> (!x y z. x < n /\ y < n /\ z < n ==>
                 (((x + y) MOD n + z) MOD n = (x + (y + z) MOD n) MOD n))``,
  REPEAT STRIP_TAC THEN
  `((x + y) MOD n + z) MOD n = ((x + y) MOD n + z MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (((x + y) + z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  `_ = ((x + (y + z)) MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (x MOD n + (y + z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_PLUS] THEN
  RW_TAC arith_ss []);

(* Theorem: mutliplication is associative in MOD *)
val MOD_MULT_ASSOC = store_thm(
  "MOD_MULT_ASSOC",
  ``!n x y z. 0 < n /\ x < n /\ z < n ==> (((x * y) MOD n * z) MOD n = (x * (y * z) MOD n) MOD n)``,
  REPEAT STRIP_TAC THEN
  `((x * y) MOD n * z) MOD n = ((x * y) MOD n * z MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (((x * y) * z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((x * (y * z)) MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (x MOD n * (y * z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
  RW_TAC arith_ss []);

(* Theorem: If n > 0, k MOD n = 0 ==> !x. (k*x) MOD n = 0 *)
val MOD_MULITPLE_ZERO = store_thm(
  "MOD_MULITPLE_ZERO",
  ``!n k. 0 < n /\ (k MOD n = 0) ==> !x. ((k*x) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  METIS_TAC [arithmeticTheory.MOD_TIMES2, arithmeticTheory.MULT_0,
             arithmeticTheory.MULT_COMM, arithmeticTheory.ZERO_MOD]);

(* ------------------------------------------------------------------------- *)
(* Factor of prime and prime exponents.                                      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (the missing arithmeticTheory.EQ_MULT_RCANCEL)
   !m n p. (n * m = p * m) <=> (m = 0) \/ (n = p)  *)
val EQ_MULT_RCANCEL = store_thm(
  "EQ_MULT_RCANCEL",
  ``!m n p. (n * m = p * m) <=> (m = 0) \/ (n = p)``,
  METIS_TAC [arithmeticTheory.EQ_MULT_LCANCEL, arithmeticTheory.MULT_SYM]);

(* Theorem: [Factors of prime] For prime p, p = x*y ==> (x = 1 and y = p) or (x = p and y = 1) *)
val FACTORS_OF_PRIME = store_thm(
  "FACTORS_OF_PRIME",
  ``!p x y. prime p /\ (p = x*y) ==> ((x = 1) /\ (y = p)) \/ ((x = p) /\ (y = 1))``,
  REPEAT STRIP_TAC THEN
  `divides x p \/ divides y p` by METIS_TAC [dividesTheory.divides_def] THENL [
    `(x = 1) \/ (x = p)` by METIS_TAC [dividesTheory.prime_def] THEN1
    SRW_TAC [][] THEN
    `0 <> p` by METIS_TAC [dividesTheory.PRIME_POS, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [arithmeticTheory.EQ_MULT_LCANCEL, arithmeticTheory.MULT_RIGHT_1],
    `(y = 1) \/ (y = p)` by METIS_TAC [dividesTheory.prime_def] THEN1
    SRW_TAC [][] THEN
    `0 <> p` by METIS_TAC [dividesTheory.PRIME_POS, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [EQ_MULT_RCANCEL, arithmeticTheory.MULT_LEFT_1]
  ]);

(* ------------------------------------------------------------------------- *)
(* Mapping theorems.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For a 1-1 map f: s -> s, s and (IMAGE f s) are of the same size. *)
val CARD_IMAGE = store_thm(
  "CARD_IMAGE",
  ``!s f. (!x y. (f x = f y) <=> (x = y)) /\ FINITE s ==> (CARD (IMAGE f s) = CARD s)``,
  Q_TAC SUFF_TAC
    `!f. (!x y. (f x = f y) = (x = y)) ==> !s. FINITE s ==> (CARD (IMAGE f s) = CARD s)`
    THEN1 METIS_TAC [] THEN
  GEN_TAC THEN STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][]);

(* Theorem: For all f: s -> t, (IMAGE f s) subset t *)
val MAP_IMAGE_SUBSET = store_thm(
  "MAP_IMAGE_SUBSET",
  ``!f s t. (!x. x IN s ==> f x IN t) ==> (IMAGE f s) SUBSET t``,
  SRW_TAC [][IMAGE_DEF, SUBSET_DEF] THEN
  RW_TAC std_ss []);

(* Theorem: For finite s, self injection means INJ f s (IMAGE f s) *)
val INJ_INJ_IMAGE = store_thm(
  "INJ_INJ_IMAGE",
  ``!f s. INJ f s s ==> INJ f s (IMAGE f s)``,
  SRW_TAC [][INJ_DEF] THEN
  Q.EXISTS_TAC `x` THEN
  RW_TAC std_ss []);

(* Theorem: For finite s, self injection means IMAGE f s = s *)
val FINITE_INJ_IMAGE_EQ = store_thm(
  "FINITE_INJ_IMAGE_EQ",
  ``!s f. FINITE s /\ INJ f s s ==> (IMAGE f s = s)``,
  REPEAT STRIP_TAC THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `FINITE (IMAGE f s)` by SRW_TAC [][IMAGE_FINITE] THEN
  `INJ f s (IMAGE f s)` by RW_TAC std_ss [INJ_INJ_IMAGE] THEN
  `!x. x IN s ==> f x IN s` by METIS_TAC [INJ_DEF] THEN
  `(IMAGE f s) SUBSET s` by SRW_TAC [][MAP_IMAGE_SUBSET] THEN
  `(IMAGE f s) PSUBSET s` by SRW_TAC [][PSUBSET_DEF] THEN
  `CARD (IMAGE f s) < CARD s` by SRW_TAC [][CARD_PSUBSET] THEN
  METIS_TAC [PHP]); (* invoke Pigeon-hole Principle *)

(* ------------------------------------------------------------------------- *)
(* Partition theorems.                                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: interrelate elements are in the same partition *)
val interrelate_elements_in_partition = store_thm(
  "interrelate_elements_in_partition",
  `` !R s. R equiv_on s ==> !t x y. t IN partition R s /\ x IN t /\ y IN s /\ R x y ==> y IN t``,
  SRW_TAC [][equiv_on_def, partition_def, EXTENSION] THEN
  METIS_TAC []);

(* Theorem: If !e IN s, CARD e = k, SIGMA CARD s = CARD s * k. *)
val SIGMA_CARD_CONSTANT = store_thm(
  "SIGMA_CARD_CONSTANT",
  ``!k s. FINITE s ==> (!e. e IN s ==> (CARD e = k)) ==> (SIGMA CARD s = k * (CARD s))``,
  STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][] THEN1 METIS_TAC [SUM_IMAGE_THM] THEN
  `CARD e = k` by SRW_TAC [][] THEN
  `SIGMA CARD (e INSERT s) = CARD e + SIGMA CARD (s DELETE e)` by SRW_TAC [][SUM_IMAGE_THM] THEN
  `s DELETE e = s` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `SUC (CARD s) = 1 + CARD s` by RW_TAC arith_ss [] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  RW_TAC arith_ss []);

(* Theorem: When the partitions are equal size of n, CARD s = n * CARD (partition of s) *)
val equal_partition_CARD = store_thm(
  "equal_partition_CARD",
  ``!R s n. R equiv_on s /\ FINITE s /\
    (!t. t IN partition R s ==> (CARD t = n)) ==>
    (CARD s = n * CARD (partition R s))``,
  METIS_TAC [partition_CARD, FINITE_partition, SIGMA_CARD_CONSTANT]);

(* ------------------------------------------------------------------------- *)
(* List theorems.                                                            *)
(* ------------------------------------------------------------------------- *)

(* Note: There is LENGTH_NIL, but no LENGTH_NON_NIL *)
(* Theorem: a non-NIL list has positive LENGTH. *)
val LENGTH_NON_NIL = store_thm(
  "LENGTH_NON_NIL",
  ``!l. l <> [] ==> 0 < LENGTH l``,
  REPEAT STRIP_TAC THEN
  `LENGTH l <> 0` by SRW_TAC [][LENGTH_NIL] THEN
  RW_TAC arith_ss []);

(* DROP and TAKE *)

(* Note: There is TAKE_LENGTH_ID, but no DROP_LENGTH_NIL *)
(* Theorem: DROP the whole length of list is NIL. *)
val DROP_LENGTH_NIL = store_thm(
  "DROP_LENGTH_NIL",
  ``DROP (LENGTH l) l = []``,
  Induct_on `l` THEN SRW_TAC [][]);

(* Theorem: TAKE 1 of non-NIL list is unaffected by appending. *)
val TAKE_1_APPEND = store_thm(
  "TAKE_1_APPEND",
  ``!x y. x <> [] ==> (TAKE 1 (x ++ y) = TAKE 1 x)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 of non-NIL list is mildly affected by appending. *)
val DROP_1_APPEND = store_thm(
  "DROP_1_APPEND",
  ``!x y. x <> [] ==> (DROP 1 (x ++ y) = (DROP 1 x) ++ y)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 more time is once more of previous DROP. *)
val DROP_SUC = store_thm(
  "DROP_SUC",
  ``!n x. DROP (SUC n) x = DROP 1 (DROP n x)``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: TAKE 1 more time is a bit more than previous TAKE. *)
val TAKE_SUC = store_thm(
  "TAKE_SUC",
  ``!n x. TAKE (SUC n) x = (TAKE n x) ++ (TAKE 1 (DROP n x))``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: DROP is non-NIL when the number is less than LENGTH. *)
val DROP_NON_NIL = store_thm(
  "DROP_NON_NIL",
  ``!n l. n < LENGTH l ==> DROP n l <> []``,
  Induct_on `l` THEN SRW_TAC [][] THEN
  `n - 1 < LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Mono-list Theory: a mono-list is a list l with SING (set l) *)

(* Theorem: Two mono-lists are equal if their lengths and sets are equal *)
val MONOLIST_EQ = store_thm(
  "MONOLIST_EQ",
  ``!l1 l2. SING (set l1) /\ SING (set l2) ==>
              ((l1 = l2) <=> (LENGTH l1 = LENGTH l2) /\ (set l1 = set l2))``,
  Induct THEN SRW_TAC [][NOT_SING_EMPTY, SING_INSERT] THENL [
    Cases_on `l2` THEN SRW_TAC [][] THEN
    FULL_SIMP_TAC (srw_ss()) [SING_INSERT, EQUAL_SING] THEN
    SRW_TAC [][LENGTH_NIL, NOT_SING_EMPTY, EQUAL_SING] THEN METIS_TAC [],
    Cases_on `l2` THEN SRW_TAC [][] THEN
    FULL_SIMP_TAC (srw_ss()) [SING_INSERT, LENGTH_NIL, NOT_SING_EMPTY, EQUAL_SING] THEN
    METIS_TAC []
  ]);

(* Theorem: A non-mono-list has at least one element in tail that is distinct from its head. *)
val NON_MONO_TAIL_PROPERTY = store_thm(
  "NON_MONO_TAIL_PROPERTY",
  ``!l. ~SING (set (h::t)) ==> ?h'. h' IN set t /\ h' <> h``,
  SRW_TAC [][SING_INSERT] THEN
  `set t <> {}` by METIS_TAC [LIST_TO_SET_EQ_EMPTY] THEN
  `?e. e IN set t` by METIS_TAC [MEMBER_NOT_EMPTY] THEN
  FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
  METIS_TAC []);

(* Unit-Length Lists *)

(* Theorem: A unit-length list has its set a singleton. *)
val LIST_1_SET_SING = store_thm(
  "LIST_1_SET_SING",
  ``!l. (LENGTH l = 1) ==> SING (set l)``,
  SRW_TAC [][SING_DEF] THEN
  Cases_on `l` THEN1
   (`LENGTH [] = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `1 <> 0` by RW_TAC arith_ss [] THEN METIS_TAC []) THEN
  `SUC (LENGTH t) = 1` by METIS_TAC [LENGTH] THEN
  `LENGTH t = 0` by RW_TAC arith_ss [] THEN
  `t = []` by METIS_TAC [LENGTH_NIL] THEN
  FULL_SIMP_TAC (srw_ss())[] THEN METIS_TAC []);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
