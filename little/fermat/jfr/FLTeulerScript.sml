(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Group-theoretic Proof.                          *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Group Theory)
======================================
Given a finite group G, consider an element a in G.

Since G is finite, element a has an order: (order a), and a^(order a) = e.

This also means that, the generated subgroup <a> has size (order a).

By Lagrange identity, size of group = k * size of subgroup.

Hence, |G| = k * |<a>|, and a^|<a>| = e.

This implies:   a^|G| = a^(k*|<a>|) = a^(|<a>*k) = (a^|<a>|)^k = e^k = e.

This is the group equivalent of Fermat's Little Theorem.

By putting G = Z*p, a IN Z*p means 0 < a < p,
then a^|G| mod p = 1, or a^(p-1) mod p = 1.

By putting G = Phi*n = {a | a < n /\ gcd(a,n) = 1 },
then a^|G| mod n = 1, or a^phi(n) mod n = 1.

which is Euler's generalization of Fermat's Little Theorem.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTeuler";

(* open dependent theories *)
open pred_setTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* Part 2: General Theory -------------------------------------------------- *)

(* val _ = load "GroupTheory"; *)
open GroupTheory;

(* val _ = load "SubgroupTheory"; *)
open SubgroupTheory;

(* val _ = load "FiniteGroupTheory"; *)
open FiniteGroupTheory;

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Euler's generalization of Modulo Multiplicative Group for any modulo n.   *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Group-theoretic Proof appplied to E*n.                                    *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Relatively prime, or Coprime.                                             *)
(* ------------------------------------------------------------------------- *)

val _ = overload_on ("coprime", ``\x y. gcd x y = 1``);

(* Theorem: If 1 < n, !x. gcd n x = 1  ==> 0 < x /\ 0 < x MOD n *)
val MOD_NOZERO_WHEN_GCD_ONE = store_thm(
  "MOD_NOZERO_WHEN_GCD_ONE",
  ``!n. 1 < n ==> !x. (gcd n x = 1) ==> 0 < x /\ 0 < x MOD n``,
  NTAC 4 STRIP_TAC THEN CONJ_ASM1_TAC THENL [
    `1 <> n` by RW_TAC arith_ss [] THEN
    `x <> 0` by METIS_TAC [gcdTheory.GCD_0R] THEN
    RW_TAC arith_ss [],
    `1 <> n /\ x <> 0` by RW_TAC arith_ss [] THEN
    `?k q. k * x = q * n + 1` by METIS_TAC [gcdTheory.LINEAR_GCD] THEN
    `(k*x) MOD n = (q*n + 1) MOD n` by SRW_TAC [][] THEN
    `_ = 1` by METIS_TAC [arithmeticTheory.MOD_MULT] THEN
    SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
    `x MOD n = 0` by RW_TAC arith_ss [] THEN
    `0 < n` by RW_TAC arith_ss [] THEN
    `(x*k) MOD n = 0` by METIS_TAC [MOD_MULITPLE_ZERO] THEN
    `1 <> 0` by RW_TAC arith_ss [] THEN
    METIS_TAC [arithmeticTheory.MULT_COMM]
  ]);

(* Theorem: For n > 1, (gcd n x = 1) /\ (gcd n y = 1) ==> (gcd n (x*y) = 1) *)
val PRODUCT_WITH_GCD_ONE = store_thm(
  "PRODUCT_WITH_GCD_ONE",
  ``!n x y. 1 < n /\ (gcd n x = 1) /\ (gcd n y = 1) ==> (gcd n (x*y) = 1)``,
  METIS_TAC [gcdTheory.GCD_CANCEL_MULT]);

(* Theorem: For n > 1, (gcd n x = 1) ==> (gcd n (x MOD n) = 1) *)
val MOD_WITH_GCD_ONE = store_thm(
  "MOD_WITH_GCD_ONE",
  ``!n x. 1 < n /\ (gcd n x = 1) ==> (gcd n (x MOD n) = 1)``,
  REPEAT STRIP_TAC THEN
  `0 <> n` by RW_TAC arith_ss [] THEN
  METIS_TAC [gcdTheory.GCD_EFFICIENTLY, gcdTheory.GCD_SYM]);

(* Theorem: If 0 < a, 0 < b, g = gcd a b, then 0 < g and a MOD g = 0 and b MOD g = 0 *)
val GCD_DIVIDES = store_thm(
  "GCD_DIVIDES",
  ``!a b g. 0 < a /\ 0 < b /\ (g = gcd a b) ==> 0 < g /\ (a MOD g = 0) /\ (b MOD g = 0)``,
  REPEAT STRIP_TAC THEN1
  METIS_TAC [gcdTheory.GCD_EQ_0, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN1
 (`0 < g` by METIS_TAC [gcdTheory.GCD_EQ_0, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [gcdTheory.GCD_IS_GCD, gcdTheory.is_gcd_def, MOD_0_DIVIDES]) THEN
  `0 < g` by METIS_TAC [gcdTheory.GCD_EQ_0, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [gcdTheory.GCD_IS_GCD, gcdTheory.is_gcd_def, MOD_0_DIVIDES]);

(* Theorem: If 1 < n, gcd n x = 1 ==> ?k q. (k*x) MOD n = 1 /\ gcd n k = 1 *)
val GCD_ONE_PROPERTY = store_thm(
  "GCD_ONE_PROPERTY",
  ``!n x. 1 < n /\ (gcd n x = 1) ==> ?k. ((k*x) MOD n = 1) /\ (gcd n k = 1)``,
  REPEAT STRIP_TAC THEN
  `n <> 1` by RW_TAC arith_ss [] THEN
  `x <> 0` by METIS_TAC [gcdTheory.GCD_0R] THEN
  `?k q. k * x = q * n + 1` by METIS_TAC [gcdTheory.LINEAR_GCD] THEN
  `(k*x) MOD n = (q*n + 1) MOD n` by SRW_TAC [][] THEN
  `_ = 1` by METIS_TAC [arithmeticTheory.MOD_MULT] THEN
  `?g. g = gcd n k` by SRW_TAC [][] THEN
  `n <> 0 /\ q*n + 1 <> 0` by RW_TAC arith_ss [] THEN
  `k <> 0` by METIS_TAC [arithmeticTheory.MULT_EQ_0] THEN
  `0 < g /\ (n MOD g = 0) /\ (k MOD g = 0)` by METIS_TAC [GCD_DIVIDES, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  `divides g n /\ divides g k` by METIS_TAC [MOD_0_DIVIDES] THEN
  `divides g (n*q) /\ divides g (k*x)` by METIS_TAC [dividesTheory.DIVIDES_MULT] THEN
  `divides g (n*q+1)` by METIS_TAC [arithmeticTheory.MULT_COMM] THEN
  `divides g 1` by METIS_TAC [dividesTheory.DIVIDES_ADD_2] THEN
  METIS_TAC [dividesTheory.DIVIDES_ONE]);

(* ------------------------------------------------------------------------- *)
(* Establish the existence of multiplicative inverse when p is prime.        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For n > 1, (gcd n x = 1) ==> ?y. y*x MOD n = 1  *)
val GCD_MOD_MULT_INV = store_thm(
  "GCD_MOD_MULT_INV",
  ``!n x. 1 < n /\ (gcd n x = 1) /\ 0 < x /\ x < n ==>
   ?y. 0 < y /\ y < n /\ (gcd n y = 1) /\ ((y*x) MOD n = 1)``,
  REPEAT STRIP_TAC THEN
  `?k. ((k*x) MOD n = 1) /\ (gcd n k = 1)` by METIS_TAC [GCD_ONE_PROPERTY] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  `(k MOD n * x MOD n) MOD n = 1` by METIS_TAC [arithmeticTheory.MOD_TIMES2] THEN
  `((k MOD n) * x) MOD n = 1` by METIS_TAC [arithmeticTheory.LESS_MOD] THEN
  `k MOD n < n` by METIS_TAC [arithmeticTheory.MOD_LESS] THEN
  `1 <> 0` by RW_TAC arith_ss [] THEN
  `0 <> k MOD n` by METIS_TAC [MOD_MULITPLE_ZERO] THEN
  `0 < k MOD n` by RW_TAC arith_ss [] THEN
  METIS_TAC [MOD_WITH_GCD_ONE]);

(* Convert this into an existence definition *)
val lemma = prove(
  ``!n x. ?y. 1 < n /\ (gcd n x = 1) /\ 0 < x /\ x < n ==>
              0 < y /\ y < n /\ (gcd n y = 1) /\ ((y*x) MOD n = 1)``,
  METIS_TAC [GCD_MOD_MULT_INV]);

val GEN_MULT_INV_DEF = new_specification(
  "GEN_MULT_INV_DEF",
  ["GCD_MOD_MUL_INV"],
  SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* ------------------------------------------------------------------------- *)
(* Euler's set and totient function                                          *)
(* ------------------------------------------------------------------------- *)

val Euler_def = Define`
  Euler n = { i | 0 < i /\ i < n /\ (gcd n i = 1) }`;

val totient_def = Define`
  totient n = CARD (Euler n)`;

(* Define Multiplicative Modulo n Group *)
val Estar_def = Define`
  Estar n =
   <| carrier := Euler n;
           id := 1;
          inv := GCD_MOD_MUL_INV n;
         mult := (\i j. (i * j) MOD n)
    |>`;

(* Theorem: (Estar n) is a Group *)
val Estar_group = store_thm(
  "Estar_group",
  ``!n. 1 < n ==> Group (Estar n)``,
  SRW_TAC [][Estar_def, Euler_def, Group_def, RES_FORALL_THM, GEN_MULT_INV_DEF] THEN1
  METIS_TAC [PRODUCT_WITH_GCD_ONE, MOD_NOZERO_WHEN_GCD_ONE] THEN1
  RW_TAC arith_ss [arithmeticTheory.MOD_LESS] THEN1
  METIS_TAC [PRODUCT_WITH_GCD_ONE, MOD_WITH_GCD_ONE] THEN
  `0 < n` by RW_TAC arith_ss [] THEN METIS_TAC [MOD_MULT_ASSOC]);

(* Theorem: FINITE (Estar p).carrier *)
val FINITE_Estar_carrier = store_thm(
  "FINITE_Estar_carrier",
  ``!n. FINITE (Estar n).carrier``,
  SRW_TAC [][Estar_def, Euler_def] THEN
  `{i | 0 < i /\ i < n /\ (gcd n i = 1)} SUBSET count n` by SRW_TAC [][pred_setTheory.SUBSET_DEF] THEN
  METIS_TAC [pred_setTheory.FINITE_COUNT, pred_setTheory.SUBSET_FINITE]);

(* Theorem: group_exp (Estar n) a k = a**k MOD n *)
val Estar_exp = store_thm(
  "Estar_exp",
  ``!n a. 1 < n /\ a IN (Estar n).carrier ==> !k. group_exp (Estar n) a k = (a**k) MOD n``,
  REPEAT STRIP_TAC THEN
  `Group (Estar n)` by SRW_TAC [][Estar_group] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  Induct_on `k` THENL [
    `(Estar n).id = 1` by SRW_TAC [][Estar_def] THEN
    `1 MOD n = 1` by RW_TAC arith_ss [] THEN
    METIS_TAC [group_exp_def, arithmeticTheory.EXP],
    SRW_TAC [][group_exp_def, arithmeticTheory.EXP] THEN
    FULL_SIMP_TAC (srw_ss())[Estar_def, Euler_def] THEN
    `a MOD n = a` by RW_TAC arith_ss [] THEN
    METIS_TAC [arithmeticTheory.MOD_TIMES2]
  ]);

(* Theorem: For all a in Estar n, a**(totient n) MOD n = 1 *)
val EULER_FERMAT = store_thm(
  "EULER_FERMAT",
  ``!n a. 1 < n /\ 0 < a /\ a < n /\ (gcd n a = 1) ==> (a**(totient n) MOD n = 1)``,
  REPEAT STRIP_TAC THEN
  `FiniteGroup (Estar n)` by SRW_TAC [][Estar_group, FINITE_Estar_carrier, FiniteGroup_def] THEN
  `CARD (Estar n).carrier = totient n` by SRW_TAC [][Estar_def, totient_def] THEN
  `a IN (Estar n).carrier /\ ((Estar n).id = 1)` by SRW_TAC [][Estar_def, Euler_def] THEN
  `(Estar n).id = 1` by SRW_TAC [][Estar_def] THEN
  METIS_TAC [FINITE_GROUP_FERMAT, Estar_exp]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
