(* ------------------------------------------------------------------------- *)
(* Group Action theorems for use in Fermat's Little Theorem.                 *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "Action";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
(* open pred_setTheory listTheory; *)
open pred_setTheory;

(* ------------------------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* val _ = load "GroupTheory"; *)
open GroupTheory;

(* val _ = load "SubgroupTheory"; *)
open SubgroupTheory;

(* ------------------------------------------------------------------------- *)
(* Theory of Group action                                                    *)
(* ------------------------------------------------------------------------- *)

(* An action from group G to a set X is a map f: GxX -> X such that
   (0)   [is a map] f (x in G)(z in X) in X
   (1)  [id action] f (e in G)(z in X) = z
   (2) [composable] f (x in G)(f (y in G)(z in X)) =
                    f ((mult in G)(x in G)(y in G))(z in X)
*)
val action_def = Define `
   action f g X = !z. z IN X ==>
     (!x :: (g.carrier). f x z IN X) /\
     (f #e z = z) /\
     (!x y :: (g.carrier). f x (f y z) = f (x * y) z)
`;

(* Theorem: For action f g X /\ x IN X, !a IN g.carrier, f a x IN X  *)
val ACTION_CLOSURE = store_thm(
  "ACTION_CLOSURE",
  ``!f g X x a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier ==> f a x IN X``,
  SRW_TAC [][action_def]);

(* Theorem: For action f g X /\ x IN X, !a,b IN g.carrier, f a (f b x) = f (a*b) x  *)
val ACTION_COMPOSE = store_thm(
  "ACTION_COMPOSE",
  ``!f g X x a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier /\ b IN g.carrier ==> (f a (f b x) = f (a*b) x)``,
  SRW_TAC [][action_def]);

(* Theorem: For action f g X /\ x IN X, f #e x = x  *)
val ACTION_ID = store_thm(
  "ACTION_ID",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==> (f #e x = x)``,
  SRW_TAC [][action_def]);
(* This is essentially REACH_REFL *)

(* Theorem: For action f g X /\ x IN X /\ y IN X, !a IN g.carrier, f a x = y ==> f |/a y = x.  *)
val ACTION_REVERSE = store_thm(
  "ACTION_REVERSE",
  ``!f g X x y a. Group g /\ action f g X /\ x IN X /\ y IN X /\ a IN g.carrier ==>
   (f a x = y) ==> (f (|/ a) y = x)``,
  REPEAT STRIP_TAC THEN
  `|/ a IN g.carrier` by SRW_TAC [][group_inv_carrier] THEN
  `f (|/ a) y = f (|/ a) (f a x)` by SRW_TAC [][] THEN
  `_ = f (|/a * a) x` by METIS_TAC [ACTION_COMPOSE, group_mult_carrier] THEN
  METIS_TAC [group_linv, ACTION_ID]);
(* This is essentially REACH_SYM *)

(* Theorem: For action f g X /\ x IN X /\ y IN X, z IN X,
            if f a x = y /\ f b y = z, then f (b*a) x = z.  *)
val ACTION_TRANS = store_thm(
  "ACTION_TRANS",
  ``!f g X x y a. Group g /\ action f g X /\ x IN X /\ y IN X /\ z IN X /\
      a IN g.carrier /\ b IN g.carrier ==>
   (f a x = y) /\ (f b y = z) ==> (f (b * a) x = z)``,
  METIS_TAC [ACTION_COMPOSE, group_mult_carrier]);
(* This is essentially REACH_TRANS *)

(* ------------------------------------------------------------------------- *)
(* Group action induces an equivalence relation.                             *)
(* ------------------------------------------------------------------------- *)

(* Define reach to relate two action points x y in X *)
val reach_def = Define`
  reach f g x y = ?z. z IN g.carrier /\ (f z x = y)
`;

(* Theorem: [Reach is Reflexive] !x in X, reach f g x x.  *)
val REACH_REFL = store_thm(
  "REACH_REFL",
  ``!f g X. Group g /\ action f g X ==> (!x. x IN X ==> reach f g x x)``,
  METIS_TAC [reach_def, ACTION_ID, group_id_carrier]);

(* Theorem: [Reach is Symmetric] !x y in X, reach f g x y ==> reach f g y x. *)
val REACH_SYM = store_thm(
  "REACH_SYM",
  ``!f g X. Group g /\ action f g X ==>
    (!x y. x IN X /\ y IN X ==> reach f g x y ==> reach f g y x)``,
  METIS_TAC [reach_def, ACTION_REVERSE, group_inv_carrier, group_linv]);

(* Theorem: [Reach is Transitive] !x y z in X, reach f g x y /\ reach f g y z ==> reach f g x z. *)
val REACH_TRANS = store_thm(
  "REACH_TRANS",
  ``!f g X. Group g /\ action f g X ==>
    (!x y z. x IN X /\ y IN X /\ z IN X ==> reach f g x y /\ reach f g y z ==> reach f g x z)``,
  SRW_TAC [][reach_def] THEN
  Q.EXISTS_TAC `z''*z'` THEN
  METIS_TAC [ACTION_TRANS, group_mult_carrier]);

(* Theorem: Reach is an equivalence relation on target set X. *)
val REACH_EQUIV_ON_TARGET = store_thm(
  "REACH_EQUIV_ON_TARGET",
  ``!f g X. Group g /\ action f g X ==> reach f g equiv_on X``,
  SRW_TAC [][equiv_on_def] THEN
  METIS_TAC [REACH_REFL, REACH_SYM, REACH_TRANS]);

(* ------------------------------------------------------------------------- *)
(* Partition by Group action.                                                *)
(* ------------------------------------------------------------------------- *)

(* Define partitions of Target set X by reach f g. *)
val TargetPartition_def = Define `
  TargetPartition f g X = partition (reach f g) X
`;

(* Theorem: For e IN (TargetPartition f g X), e <> EMPTY *)
val TARGET_PARTITION_ELEMENT_NONEMPTY = store_thm(
  "TARGET_PARTITION_ELEMENT_NONEMPTY",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> e <> {}``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, EMPTY_NOT_IN_partition]);

(* Theorem: Elements in Element of TargetPartition are in X. *)
val TARGET_PARTITION_ELEMENT_ELEMENT = store_thm(
  "TARGET_PARTITION_ELEMENT_ELEMENT",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> !a. a IN e ==> a IN X``,
  SRW_TAC [][TargetPartition_def, partition_def] THEN
  FULL_SIMP_TAC (srw_ss())[]);

(* Theorem: TargetPartition elements are subset of target *)
val TARGET_PARTITION_ELEMENT_SUBSET_TARGET = store_thm(
  "TARGET_PARTITION_ELEMENT_SUBSET_TARGET",
  ``!f g X. Group g /\ action f g X ==>
   !e. e IN TargetPartition f g X ==> e SUBSET X``,
  METIS_TAC [TARGET_PARTITION_ELEMENT_ELEMENT, SUBSET_DEF]);

(* Theorem: Target partition is FINITE *)
val FINITE_TARGET_PARTITION = store_thm(
  "FINITE_TARGET_PARTITION",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> FINITE (TargetPartition f g X)``,
  SRW_TAC [][TargetPartition_def, REACH_EQUIV_ON_TARGET, FINITE_partition]);

(* Theorem: For e IN (TargetPartition f g X), FINITE X ==> FINITE e *)
val FINITE_TARGET_PARTITION_ELEMENT = store_thm(
  "FINITE_TARGET_PARTITION_ELEMENT",
  ``!f g X. Group g /\ action f g X ==>
     !e. (e IN TargetPartition f g X) ==> FINITE X ==> FINITE e``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, FINITE_partition]);

(* Theorem: For FINITE X, CARD X = SUM of CARD partitions in (TargetPartition f g X) *)
val CARD_TARGET_BY_PARTITION = store_thm(
  "CARD_TARGET_BY_PARTITION",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==>
    (CARD X = SIGMA CARD (TargetPartition f g X))``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, partition_CARD]);

(* ------------------------------------------------------------------------- *)
(* Orbits as equivalence classes.                                            *)
(* ------------------------------------------------------------------------- *)

(* Orbit of action: those x in X that can be reached by a in X *)
val orbit_def = Define`
  orbit f g X a = {x | x IN X /\ reach f g a x }
`;

(* Theorem: y IN (orbit f g X x) ==> y IN X /\ reach f g x y *)
val ORBIT_PROPERTY = store_thm(
  "ORBIT_PROPERTY",
  ``!f g X x y. y IN orbit f g X x ==> y IN X /\ reach f g x y``,
  SRW_TAC [][orbit_def]);

(* Theorem: (orbit f g X x) = IMAGE (\a. f a x) g.carrier *)
val ORBIT_DESCRIPTION = store_thm(
  "ORBIT_DESCRIPTION",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==> (orbit f g X x = {f a x | a IN g.carrier})``,
  SRW_TAC [][action_def, orbit_def, reach_def, EXTENSION, EQ_IMP_THM] THEN METIS_TAC []);

(* Theorem: if a in g.carrier, then f x a in (orbit f g X a). *)
val ORBIT_HAS_ACTION_ELEMENT = store_thm(
  "ORBIT_HAS_ACTION_ELEMENT",
  ``!f g X x a. action f g X /\ x IN X /\ a IN g.carrier ==> f a x IN (orbit f g X x)``,
  SRW_TAC [][orbit_def, reach_def] THEN
  METIS_TAC [action_def]);

(* Theorem: action f g X ==> x IN orbit f g X x *)
val ACTION_ORBIT_HAS_ITSELF = store_thm(
  "ACTION_ORBIT_HAS_ITSELF",
  ``!f g X. Group g /\ action f g X /\ x IN X ==> x IN orbit f g X x``,
  SRW_TAC [][orbit_def] THEN
  METIS_TAC [REACH_REFL]);

(* Theorem: orbits are subsets of target set X *)
val ORBIT_SUBSET_TARGET = store_thm(
  "ORBIT_SUBSET_TARGET",
  ``!f g X a. action f g X /\ a IN X ==> orbit f g X a SUBSET X``,
  SRW_TAC [][orbit_def, SUBSET_DEF]);

(* Theorem: Elements of TargetPartition are orbits of its own element.
            !e IN TargetPartition f g X ==> !a IN e. e = orbit f g X a *)
val TARGET_PARTITION_ELEMENT_IS_ORBIT = store_thm(
  "TARGET_PARTITION_ELEMENT_IS_ORBIT",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> !a. a IN e ==> (e = orbit f g X a)``,
  SRW_TAC [][TargetPartition_def, partition_def, orbit_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [REACH_SYM, REACH_TRANS]);

(* Theorem: If for all x IN X, (orbit f g X x) = n, then n divides CARD X. *)
val EQUAL_SIZE_ORBITS_PROPERTY = store_thm(
  "EQUAL_SIZE_ORBITS_PROPERTY",
  ``!f g X n. Group g /\ action f g X /\ FINITE X /\
    (!x. x IN X ==> (CARD (orbit f g X x) = n)) ==> divides n (CARD X)``,
  REPEAT STRIP_TAC THEN
  `!e. e IN TargetPartition f g X ==> !x. x IN e ==> (e = orbit f g X x)` by SRW_TAC [][TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `!e. e IN TargetPartition f g X ==> (CARD e = n)` by SRW_TAC [][] THENL [
    `?y. y IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
    METIS_TAC [TARGET_PARTITION_ELEMENT_SUBSET_TARGET, SUBSET_DEF],
    `CARD X = n * CARD (partition (reach f g) X)` by METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, equal_partition_CARD] THEN
    METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_SYM]
  ]);

(* ------------------------------------------------------------------------- *)
(* Stabilizer subgroups.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Stabilizer of action: for a in X, stabilizer of a = {x in G | f x a = a} *)
val stabilizer_def = Define`
  stabilizer f g a = {x | x IN g.carrier /\ (f x a = a) }
`;

(* Theorem: x IN stabilizer f g a ==> x IN g.carrier and f x a = a. *)
val STABILIZER_PROPERTY = store_thm(
  "STABILIZER_PROPERTY",
  ``!f g X a. action f g X /\ a IN X ==> !x. x IN stabilizer f g a ==> x IN g.carrier /\ (f x a = a)``,
  SRW_TAC [][stabilizer_def]);

(* Define the Stabilizer Group of stabilizer set. *)
val StabilizerGroup_def = Define`
  StabilizerGroup f g a =
    <| carrier := stabilizer f g a;
            id := g.id;
           inv := |/ ;
          mult := g.mult
     |>`;

(* Theorem: If g is a Group, f g X is an action, StabilizerGroup f g a is a Group *)
val StabilizerGroup_group = store_thm(
  "StabilizerGroup_group",
  ``!f g X a. action f g X /\ a IN X /\ Group g ==> Group (StabilizerGroup f g a)``,
  SRW_TAC [][Group_def, StabilizerGroup_def, stabilizer_def, action_def, RES_FORALL_THM] THEN METIS_TAC []);

(* Theorem: The stabilizer is a subset of g.carrier *)
val STABILIZER_SUBSET = store_thm(
  "STABILIZER_SUBSET",
  ``!f g X. action f g X /\ a IN X ==> (stabilizer f g a) SUBSET g.carrier``,
  SRW_TAC [][stabilizer_def, SUBSET_DEF]);

(* Theorem: If g is Group, f g X is an action, then stabilizer is a subgroup of g *)
val StabilizerGroup_subgroup = store_thm(
  "StabilizerGroup_subgroup",
  ``!f g X. action f g X /\ a IN X /\ Group g ==> Subgroup (StabilizerGroup f g a) g``,
  SRW_TAC [][Subgroup_def, StabilizerGroup_def] THEN
  METIS_TAC [StabilizerGroup_def, StabilizerGroup_group, STABILIZER_SUBSET]);

(* ------------------------------------------------------------------------- *)
(* Orbit-Stabilizer Theorem.                                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The map from orbit to coset of stabilizer is well-defined. *)
val ORBIT_STABILIZER_MAP_WD = store_thm(
  "ORBIT_STABILIZER_MAP_WD",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==>
   !x y. x IN g.carrier /\ y IN g.carrier /\ (f x a = f y a) ==>
   (coset g (stabilizer f g a) x = coset g (stabilizer f g a) y)``,
  REPEAT STRIP_TAC THEN
  `StabilizerGroup f g a <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  FULL_SIMP_TAC (srw_ss()) [action_def] THEN
  `f (|/y * x) a = f (|/y) (f x a)` by SRW_TAC [][group_inv_carrier] THEN
  `_ = f (|/y) (f y a)` by ASM_REWRITE_TAC [] THEN
  `_ = f (|/y * y) a` by SRW_TAC [][group_inv_carrier] THEN
  `_ = f #e a` by SRW_TAC [][group_linv] THEN
  `_ = a` by SRW_TAC [][] THEN
  `(|/y * x) IN g.carrier` by SRW_TAC [][group_inv_carrier, group_mult_carrier] THEN
  `(|/y * x) IN (stabilizer f g a)` by SRW_TAC [][stabilizer_def] THEN
  METIS_TAC [SUBGROUP_COSET_EQ]);

(* Theorem: The map from orbit to coset of stabilizer is injective. *)
val ORBIT_STABILIZER_MAP_UQ = store_thm(
  "ORBIT_STABILIZER_MAP_UQ",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==>
   !x y. x IN g.carrier /\ y IN g.carrier /\
   (coset g (stabilizer f g a) x = coset g (stabilizer f g a) y) ==> (f x a = f y a)``,
  REPEAT STRIP_TAC THEN
  `StabilizerGroup f g a <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  `(|/y * x) IN (stabilizer f g a)` by METIS_TAC [SUBGROUP_COSET_EQ] THEN
  `f (|/y * x) a = a` by FULL_SIMP_TAC (srw_ss()) [stabilizer_def] THEN
  `|/y * x IN g.carrier` by SRW_TAC [][group_inv_carrier, group_mult_carrier] THEN
  `f x a = f (#e*x) a` by SRW_TAC [][group_lid] THEN
  `_ = f ((y* |/y)*x) a` by SRW_TAC [][group_rinv] THEN
  `_ = f (y*(|/y*x)) a` by SRW_TAC [][group_assoc, group_inv_carrier] THEN
  `_ = f y (f (|/y * x) a)` by FULL_SIMP_TAC (srw_ss()) [action_def] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)
(* Convert reach definition into a function                                  *)
(* ------------------------------------------------------------------------- *)

(* Existence of act_by:  reach x y ==> ?a. a IN g.carrier /\ f a x = y *)
val lemma = prove(
  ``!f g x y. ?a. reach f g x y ==> a IN g.carrier /\ (f a x = y)``,
  METIS_TAC [reach_def]);

val ACT_BY_DEF = new_specification(
    "ACT_BY_DEF",
    ["ACT_BY"],
    SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* Theorem: Points of (orbit x) and cosets of (stabilizer x) are one-to-one. *)
val ORBIT_STABILIZER_COSETS_BIJ = store_thm(
  "ORBIT_STABILIZER_COSETS_BIJ",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==>
   BIJ (\z.  coset g (stabilizer f g x) (ACT_BY f g x z))
       (orbit f g X x)
       (CosetPartition g (StabilizerGroup f g x))``,
  SRW_TAC [][CosetPartition_def, partition_def, inCoset_def, StabilizerGroup_def, BIJ_DEF, INJ_DEF, SURJ_DEF] THENL [
    `reach f g x z` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.EXISTS_TAC `ACT_BY f g x z` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    `x' IN IMAGE (\z'. ACT_BY f g x z * z') (stabilizer f g x)` by METIS_TAC [coset_def] THEN
    `?z'. z' IN (stabilizer f g x) /\ (x' = ACT_BY f g x z * z')` by METIS_TAC [IN_IMAGE] THEN
    METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier],
    `reach f g x z /\ reach f g x z'` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier /\ ACT_BY f g x z' IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    `f (ACT_BY f g x z) x = f (ACT_BY f g x z') x` by METIS_TAC [ORBIT_STABILIZER_MAP_UQ] THEN
    METIS_TAC [ACT_BY_DEF],
    `reach f g x z` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.EXISTS_TAC `ACT_BY f g x z` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    `x' IN IMAGE (\z'. ACT_BY f g x z * z') (stabilizer f g x)` by METIS_TAC [coset_def] THEN
    `?z'. z' IN (stabilizer f g x) /\ (x' = ACT_BY f g x z * z')` by METIS_TAC [IN_IMAGE] THEN
    METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier],
    Q.EXISTS_TAC `f x'' x` THEN
    SRW_TAC [][] THENL [
      `reach f g x (f x'' x)` by METIS_TAC [reach_def] THEN
      `f x'' x IN X` by METIS_TAC [ACTION_CLOSURE] THEN
      SRW_TAC [][orbit_def],
      `reach f g x (f x'' x)` by METIS_TAC [reach_def] THEN
      `ACT_BY f g x (f x'' x) IN g.carrier /\ (f (ACT_BY f g x (f x'' x)) x = (f x'' x))` by SRW_TAC [][ACT_BY_DEF] THEN
      `coset g (stabilizer f g x) (ACT_BY f g x (f x'' x)) = coset g (stabilizer f g x) x''` by METIS_TAC [ORBIT_STABILIZER_MAP_WD] THEN
      SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
      `x' IN IMAGE (\z. x'' * z) (stabilizer f g x)` by METIS_TAC [coset_def] THEN
      `?z. z IN (stabilizer f g x) /\ (x' = x'' * z)` by METIS_TAC [IN_IMAGE] THEN
      METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier]
    ]
  ]);

(* Theorem: [Orbit-Stabilizer Theorem] CARD G = CARD (orbit x) * CARD (stabilizer x) *)
val ORBIT_STABILIZER_THEOREM = store_thm(
  "ORBIT_STABILIZER_THEOREM",
  ``!f g X x. FiniteGroup g /\ action f g X /\ x IN X /\ FINITE X ==>
   (CARD g.carrier = CARD (orbit f g X x) * CARD (stabilizer f g x))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `StabilizerGroup f g x <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g x).carrier = stabilizer f g x` by SRW_TAC [][StabilizerGroup_def] THEN
  `FINITE (CosetPartition g (StabilizerGroup f g x))` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  `FINITE (orbit f g X x)` by METIS_TAC [ORBIT_DESCRIPTION, IMAGE_DEF, IMAGE_FINITE] THEN
  `CARD (CosetPartition g (StabilizerGroup f g x)) = CARD (orbit f g X x)` by METIS_TAC [ORBIT_STABILIZER_COSETS_BIJ, FINITE_BIJ_CARD_EQ] THEN
  `CARD g.carrier = CARD (stabilizer f g x) * CARD (CosetPartition g (StabilizerGroup f g x))` by METIS_TAC [FiniteGroup_def, LAGRANGE_IDENTITY] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
