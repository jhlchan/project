(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Binomial Proof.                                 *)
(* ------------------------------------------------------------------------- *)

(*
Fermat's Little Theorem (Binomial proof)
========================================
To prove:  a^p = a  (mod p)   for prime p
Proof:
   By induction on a.

   Base case: 0^p = 0 (mod p)
      true by arithmetic.
   Step case: k^p = k (mod p)  ==> (k+1)^p = (k+1) (mod p)
      (k+1)^p
    = SUM (GENLIST (\j. (binomial p j)* k**(p-j) * 1**j) (SUC p))    by binomial_thm
    = SUM (GENLIST (\j. (binomial p j)* k**(p-j)) (SUC p))           by arithmetic

   By prime_iff_divides_binomials,
      prime p <=> 1 < p /\ (!j. 0 < j /\ j < p ==> divides p (binomial p j))`;
   Therefore in mod p,
      (k+1)^p = k^p + 1^p    (mod p)   just first and last terms
              = k   + 1^p    (mod p)   by induction hypothesis
              = k + 1                  by arithmetic
*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTbinomial";

(* open dependent theories *)
open arithmeticTheory; (* for MOD and EXP *)
open pred_setTheory; (* for PROD_SET_THM *)
open listTheory; (* for SUM and GENLIST *)

(* val _ = load "dividesTheory"; *)
(* val _ = load "gcdTheory"; *)
open dividesTheory gcdTheory;

(* Part 1: Basics ---------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* Part 2: General Theory -------------------------------------------------- *)

(* val _ = load "BinomialTheory"; *)
open BinomialTheory;

(* Theorem: prime p ==> p cannot divide k! for k < p *)
val PRIME_LESS_NOT_DIVIDES_FACT = store_thm(
  "PRIME_LESS_NOT_DIVIDES_FACT",
  ``!p k. prime p /\ k < p ==> ~(divides p (FACT k))``,
  (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
  Induct_on `k` THENL [
    SRW_TAC [][FACT] THEN
    METIS_TAC [ONE_LT_PRIME, LESS_NOT_EQ],
    SRW_TAC [][FACT] THEN
    (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
    `k < p /\ 0 < SUC k` by DECIDE_TAC THEN
    METIS_TAC [P_EUCLIDES, NOT_LT_DIVIDES]
  ]);

(* Theorem: n is prime ==> n divides C(n,k)  for all 0 < k < n *)
val prime_divides_binomials = store_thm(
  "prime_divides_binomials",
  ``!n. prime n ==> 1 < n /\ (!k. 0 < k /\ k < n ==> divides n (binomial n k))``,
  REPEAT STRIP_TAC THEN1
  METIS_TAC [ONE_LT_PRIME] THEN
  `(n = n-k + k) /\ (n-k) < n` by DECIDE_TAC THEN
  `FACT n = (binomial n k) * (FACT (n-k) * FACT k)` by METIS_TAC [binomial_formula] THEN
  `~ divides n (FACT k) /\
    ~ divides n (FACT (n-k))` by METIS_TAC [PRIME_LESS_NOT_DIVIDES_FACT] THEN
  `divides n (FACT n)` by METIS_TAC [DIVIDES_FACT, LESS_TRANS] THEN
  METIS_TAC [P_EUCLIDES]);

(* Theorem: ~ prime n ==> n has a proper prime factor p *)
val PRIME_FACTOR_PROPER = store_thm(
  "PRIME_FACTOR_PROPER",
  ``!n. 1 < n /\ ~prime n ==> ?p. prime p /\ p < n /\ (divides p n)``,
  REPEAT STRIP_TAC THEN
  `0 < n /\ n <> 1` by DECIDE_TAC THEN
  `?p. prime p /\ divides p n` by METIS_TAC [PRIME_FACTOR] THEN
  `~(n < p)` by METIS_TAC [NOT_LT_DIVIDES] THEN
  Cases_on `n = p` THEN1
  FULL_SIMP_TAC std_ss [] THEN
  `p < n` by DECIDE_TAC THEN
  METIS_TAC []);

(* Theorem: n! = PROD_SET (count (n+1))  *)
val FACT_EQ_PROD = store_thm(
  "FACT_EQ_PROD",
  ``!n. FACT n = PROD_SET (IMAGE SUC (count n))``,
  Induct_on `n` THEN1
  SRW_TAC [][PROD_SET_THM, FACT] THEN
  SRW_TAC [][PROD_SET_THM, FACT, COUNT_SUC] THEN
  `(SUC n) NOTIN (IMAGE SUC (count n))` by SRW_TAC [][] THEN
  METIS_TAC [DELETE_NON_ELEMENT]);

(* Theorem: n!/m! = product of (m+1) to n  *)
val FACT_REDUCTION = store_thm(
  "FACT_REDUCTION",
  ``!n m. m < n ==> (FACT n = PROD_SET (IMAGE SUC ((count n) DIFF (count m))) * (FACT m))``,
  Induct_on `n` THEN1
  SRW_TAC [][] THEN
  SRW_TAC [][FACT] THEN
  `m <= n` by DECIDE_TAC THEN
  Cases_on `m = n` THENL [
    SRW_TAC [][] THEN
    `count (SUC m) DIFF count m = {m}` by SRW_TAC [][DIFF_DEF] THENL [
      SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
      DECIDE_TAC,
      `PROD_SET (IMAGE SUC {m}) = SUC m` by SRW_TAC [][PROD_SET_THM] THEN
      METIS_TAC []
    ],
    `m < n` by DECIDE_TAC THEN
    `n NOTIN (count m)` by SRW_TAC [ARITH_ss][] THEN
    `n INSERT ((count n) DIFF (count m)) =
    (n INSERT (count n)) DIFF (count m)` by SRW_TAC [][INSERT_DIFF] THEN
    `_ = count (SUC n) DIFF (count m)` by SRW_TAC [ARITH_ss][EXTENSION] THEN
    `(SUC n) NOTIN (IMAGE SUC ((count n) DIFF (count m)))` by SRW_TAC [][] THEN
    `FINITE (IMAGE SUC ((count n) DIFF (count m)))` by SRW_TAC [][] THEN
    `PROD_SET (IMAGE SUC (count (SUC n) DIFF count m)) =
    (SUC n) * PROD_SET (IMAGE SUC (count n DIFF count m))`
    by METIS_TAC [PROD_SET_IMAGE_REDUCTION] THEN
    METIS_TAC [MULT_ASSOC]
  ]);

(* Theorem: (Generalized Euclid's Lemma)
            If prime p divides a PROD_SET, it divides a member of the PROD_SET *)
val PROD_SET_EUCLID = store_thm(
  "PROD_SET_EUCLID",
  ``!s. FINITE s ==> !p. prime p /\ divides p (PROD_SET s) ==> ?b. b IN s /\ divides p b``,
  HO_MATCH_MP_TAC FINITE_INDUCT THEN
  SRW_TAC [][] THEN1
  METIS_TAC [PROD_SET_EMPTY, DIVIDES_ONE, NOT_PRIME_1] THEN
  `PROD_SET (e INSERT s) = e * PROD_SET s`
    by METIS_TAC [PROD_SET_THM, DELETE_NON_ELEMENT] THEN
  Cases_on `divides p e` THEN1
  METIS_TAC [] THEN
  METIS_TAC [P_EUCLIDES]);

(* Theorem: (multiple gap)
   If n divides m, n cannot divide any x: m - n < x < m, or m < x < m + n *)
val MULTIPLE_INTERVAL = store_thm(
  "MULTIPLE_INTERVAL",
  ``!n m. divides n m ==> !x. m - n < x /\ x < m + n /\ divides n x ==> (x = m)``,
  REPEAT STRIP_TAC THEN
  `(?h. m = h*n) /\ (?k. x = k*n)` by METIS_TAC [divides_def] THEN
  `(h-1)*n < k*n` by METIS_TAC [RIGHT_SUB_DISTRIB, MULT_LEFT_1] THEN
  `k*n < (h+1)*n` by METIS_TAC [RIGHT_ADD_DISTRIB, MULT_LEFT_1] THEN
  `0 < n /\ h-1 < k /\ k < h+1` by METIS_TAC [LT_MULT_RCANCEL] THEN
  `h = k` by DECIDE_TAC THEN
  METIS_TAC []);

(* Theorem: If prime p divides n, p does not divide (n-1)!/(n-p)! *)
val prime_divisor_property = store_thm(
  "prime_divisor_property",
  ``!n p. 1 < n /\ p < n /\ prime p /\ divides p n ==>
    ~(divides p ((FACT (n-1)) DIV (FACT (n-p))))``,
  (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
  `1 < p` by METIS_TAC [ONE_LT_PRIME] THEN
  `n-p < n-1` by DECIDE_TAC THEN
  `(FACT (n-1)) DIV (FACT (n-p)) =
   PROD_SET (IMAGE SUC ((count (n-1)) DIFF (count (n-p))))`
    by METIS_TAC [FACT_REDUCTION, MULT_DIV, FACT_LESS] THEN
  `(count (n-1)) DIFF (count (n-p)) = {x | (n-p) <= x /\ x < (n-1)}`
    by SRW_TAC [ARITH_ss][EXTENSION, EQ_IMP_THM] THEN
  `IMAGE SUC {x | (n-p) <= x /\ x < (n-1)} = {x | (n-p) < x /\ x < n}`
    by SRW_TAC [ARITH_ss][EXTENSION, EQ_IMP_THM] THENL [
    Q.EXISTS_TAC `x-1` THEN
    DECIDE_TAC,
    `FINITE (count (n - 1) DIFF count (n - p))` by SRW_TAC [][] THEN
    `?y. y IN {x| n - p < x /\ x < n} /\ divides p y`
      by METIS_TAC [PROD_SET_EUCLID, IMAGE_FINITE] THEN
    `!m n y. y IN {x | m < x /\ x < n} ==> m < y /\ y < n` by SRW_TAC [][] THEN
    `n-p < y /\ y < n` by METIS_TAC [] THEN
    `y < n + p` by DECIDE_TAC THEN
    `y = n` by METIS_TAC [MULTIPLE_INTERVAL] THEN
    DECIDE_TAC
  ]);

(* Theorem: n divides C(n,k)  for all 0 < k < n ==> n is prime *)
val divides_binomials_imp_prime = store_thm(
  "divides_binomials_imp_prime",
  ``!n. 1 < n /\ (!k. 0 < k /\ k < n ==> divides n (binomial n k)) ==> prime n``,
  (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
  `?p. prime p /\ p < n /\ divides p n` by METIS_TAC [PRIME_FACTOR_PROPER] THEN
  `divides n (binomial n p)` by METIS_TAC [PRIME_POS] THEN
  `0 < p` by METIS_TAC [PRIME_POS] THEN
  `(n = n-p + p) /\ (n-p) < n` by DECIDE_TAC THEN
  `FACT n = (binomial n p) * (FACT (n-p) * FACT p)` by METIS_TAC [binomial_formula] THEN
  `(n = SUC (n-1)) /\ (p = SUC (p-1))` by DECIDE_TAC THEN
  `(FACT n = n * FACT (n-1)) /\ (FACT p = p * FACT (p-1))` by METIS_TAC [FACT] THEN
  `n * FACT (n-1) = (binomial n p) * (FACT (n-p) * (p * FACT (p-1)))` by METIS_TAC [] THEN
  `0 < n` by DECIDE_TAC THEN
  `?q. binomial n p = n * q` by METIS_TAC [divides_def, MULT_COMM] THEN
  `0 <> n` by DECIDE_TAC THEN
  `FACT (n-1) = q * (FACT (n-p) * (p * FACT (p-1)))`
    by METIS_TAC [EQ_MULT_LCANCEL, MULT_ASSOC] THEN
  `_ = q * ((FACT (p-1) * p)* FACT (n-p))` by METIS_TAC [MULT_COMM] THEN
  `_ = q * FACT (p-1) * p * FACT (n-p)` by METIS_TAC [MULT_ASSOC] THEN
  `FACT (n-1) DIV FACT (n-p) = q * FACT (p-1) * p` by METIS_TAC [MULT_DIV, FACT_LESS] THEN
  METIS_TAC [divides_def, prime_divisor_property]);

(* Theorem: n is prime iff n divides C(n,k)  for all 0 < k < n *)
val prime_iff_divides_binomials = store_thm(
  "prime_iff_divides_binomials",
  ``!n. prime n <=> 1 < n /\ (!k. 0 < k /\ k < n ==> divides n (binomial n k))``,
  METIS_TAC [prime_divides_binomials, divides_binomials_imp_prime]);

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Binomial Theorem for prime exponent and modulo.                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: !k. 0 < k /\ k < n ==> (binomial n k MOD n = 0) <=>
            !h. 0 <= h /\ h < PRE n ==> (binomial n (SUC h) MOD n = 0) *)
val binomial_range_shift = store_thm(
  "binomial_range_shift",
  ``!n . 0 < n ==> ((!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
                   (!h. h < PRE n ==> ((binomial n (SUC h)) MOD n = 0)))``,
  RW_TAC std_ss [EQ_IMP_THM] THENL [
    `0 < SUC h /\ SUC h < n` by DECIDE_TAC THEN
    RW_TAC std_ss [],
    `k <> 0` by DECIDE_TAC THEN
    `?h. k = SUC h` by METIS_TAC [num_CASES] THEN
    `h < PRE n` by DECIDE_TAC THEN
    RW_TAC std_ss []
  ]);

(* Theorem: binomial n k MOD n = 0 <=> (binomial n k * x ** (n-k) * y ** k) MOD n = 0 *)
val binomial_mod_zero = store_thm(
  "binomial_mod_zero",
  ``!n. 0 < n ==> !k. (binomial n k MOD n = 0) <=> (!x y. (binomial n k * x ** (n-k) * y ** k) MOD n = 0)``,
  RW_TAC std_ss [EQ_IMP_THM] THEN1
  METIS_TAC [MOD_TIMES2, ZERO_MOD, MULT] THEN
  METIS_TAC [EXP_1, MULT_RIGHT_1]);

(* Theorem: (!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0))) <=>
            (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))) *)
val binomial_range_shift' = store_thm(
  "binomial_range_shift'",
  ``!n . 0 < n ==> ((!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0))) <=>
                   (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))))``,
  RW_TAC std_ss [EQ_IMP_THM] THENL [
    `0 < SUC h /\ SUC h < n` by DECIDE_TAC THEN
    RW_TAC std_ss [],
    `k <> 0` by DECIDE_TAC THEN
    `?h. k = SUC h` by METIS_TAC [num_CASES] THEN
    `h < PRE n` by DECIDE_TAC THEN
    RW_TAC std_ss []
  ]);

(* Theorem: !k. 0 < k /\ k < n ==> (binomial n k) MOD n = 0 <=>
            !x y. SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0 *)
val binomial_mod_zero' = store_thm(
  "binomial_mod_zero'",
  ``!n. 0 < n ==> ((!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
                  !x y. SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0)``,
  REPEAT STRIP_TAC THEN
  `!x y. (\k. (binomial n (SUC k) * x ** (n - SUC k) * y ** (SUC k)) MOD n) =
         (\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC` by RW_TAC std_ss [FUN_EQ_THM] THEN
  `(!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
   (!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0)))`
     by RW_TAC std_ss [binomial_mod_zero] THEN
  `_ = (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0)))`
    by RW_TAC std_ss [binomial_range_shift'] THEN
  `_ = !x y h. h < PRE n ==> (((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))`
    by METIS_TAC [] THEN
  RW_TAC std_ss [EVERY_GENLIST, SUM_EQ_0]);

(* Theorem: [Binomial Expansion for prime exponent]  (x + y)^p = x^p + y^p (mod p) *)
val binomial_thm_prime = store_thm(
  "binomial_thm_prime",
  ``!p. prime p ==> (!x y. (x + y)**p MOD p = (x**p + y**p) MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by RW_TAC std_ss [PRIME_POS] THEN
  `!k. 0 < k /\ k < p ==> ((binomial p k) MOD p  = 0)` by METIS_TAC [prime_iff_divides_binomials, MOD_0_DIVIDES] THEN
  `SUM (GENLIST ((\k. binomial p k * x ** (p - k) * y ** k) o SUC) (PRE p)) MOD p = 0`
    by METIS_TAC [SUM_GENLIST_MOD, binomial_mod_zero', ZERO_MOD] THEN
  `(x + y) ** p MOD p =
   (x ** p + SUM (GENLIST ((\k. binomial p k * x ** (p - k) * y ** k) o SUC) (PRE p)) + y ** p) MOD p`
    by RW_TAC std_ss [binomial_thm, SUM_DECOMPOSE_FIRST_LAST, binomial_n_0, binomial_n_n, EXP] THEN
  METIS_TAC [MOD_PLUS3, ADD_0, MOD_PLUS]);

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem                                                   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (Fermat's Little Theorem by Binomial) For prime p, a**p = a (mod p). *)
(* Proof: by induction on a.
   Base case: 0 ** p MOD p = 0 MOD p
      true by
   Step case: a ** p MOD p = a MOD p ==> SUC a ** p MOD p = SUC a MOD p
     SUC a ** p MOD p
   = (a + 1) ** p MOD p                     by ADD1
   = (a ** p + 1 ** p) MOD p                by binomial_thm_prime
   = (a ** p MOD p + 1 ** p MOD p) MOD p    by MOD_PLUS
   = (a MOD p + 1 ** p MOD p) MOD p         by induction hypothesis
   = (a MOD p + 1 MOD p) MOD p              by EXP_1
   = (a + 1) MOD p                          by MOD_PLUS
   = SUC a MOD p                            by ADD1
*)
val FERMAT_LITTLE_THM = store_thm(
  "FERMAT_LITTLE_THM",
  ``!p a. prime p ==> (a**p MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by RW_TAC std_ss [PRIME_POS] THEN
  `p <> 0` by DECIDE_TAC THEN
  Induct_on `a` THEN1
  RW_TAC std_ss [ZERO_EXP] THEN
  METIS_TAC [binomial_thm_prime, MOD_PLUS, EXP_1, ADD1]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
