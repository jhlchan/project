(* ------------------------------------------------------------------------- *)
(* Group Z_n for use in Fermat's Little Theorem.                             *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "GroupInstances";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
(* open pred_setTheory listTheory; *)
open pred_setTheory;

(* ------------------------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* val _ = load "GroupTheory"; *)
open GroupTheory;

(* ------------------------------------------------------------------------- *)
(* The Group Zn = Addition Modulo n, for n > 0.                              *)
(* ------------------------------------------------------------------------- *)

(* Define Zn = Addition Modulo n Group *)
val Z_def = Define`
  Z n =
    <| carrier := count n;
            id := 0;
           inv := (\i. (n - i) MOD n);  (* so that inv 0 = 0 *)
          mult := (\i j. (i + j) MOD n)
     |>`;

(* Theorem: Zn carrier is finite *)
val FINITE_ZN_carrier = store_thm(
  "FINITE_ZN_carrier",
  ``!n. FINITE (Z n).carrier``,
  SRW_TAC [][Z_def, FINITE_COUNT]);

(* Theorem: CARD (Zn carrier) = n *)
val CARD_ZN_carrier = store_thm(
  "CARD_ZN_carrier",
  ``!n. CARD (Z n).carrier = n``,
  SRW_TAC [][Z_def, CARD_COUNT]);

(* Theorem: Zn is a group if n > 0. *)
val ZN_group = store_thm(
  "ZN_group",
  ``!n. 0 < n ==> Group (Z n)``,
  SRW_TAC [][Z_def, Group_def, RES_FORALL_THM, MOD_ADD_INV, MOD_ADD_ASSOC]);

(* Theorem: Zn is a FiniteGroup if n > 0. *)
val ZN_finite_group = store_thm(
  "ZN_finite_group",
  ``!n. 0 < n ==> FiniteGroup (Z n)``,
  METIS_TAC [ZN_group, FINITE_ZN_carrier, FiniteGroup_def]);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
