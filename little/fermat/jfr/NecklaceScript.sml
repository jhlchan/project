(* ------------------------------------------------------------------------- *)
(* Necklace theorems for use in Fermat's Little Theorem.                     *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "Necklace";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* ------------------------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* ------------------------------------------------------------------------- *)
(* Theory of Necklaces                                                       *)
(* ------------------------------------------------------------------------- *)

(* Define necklaces with n beads and a colors. *)
(* Represent necklace as list, colors taken from COUNT = {1,2,3,....}  *)
val necklace_def = Define `
  necklace n a = {l | (LENGTH l = n) /\ set l SUBSET count a}
`;

(* Theorem: If l in (necklace n a), LENGTH l = n  and colors in count a. *)
val necklace_property = store_thm(
  "necklace_property",
  ``!n a l. l IN necklace n a ==> (LENGTH l = n) /\ set l SUBSET count a``,
  SRW_TAC [][necklace_def]);

(* Theorem: Zero-length necklaces of whatever colors is the set of NIL. *)
val necklace_0 = store_thm(
  "necklace_0",
  ``!a. necklace 0 a = {[]}``,
  SRW_TAC [][necklace_def, EXTENSION, EQ_IMP_THM, LENGTH_NIL]);

(* Theorem: A necklace of length n <> 0 has an order, i.e. non-NIL. *)
val necklace_not_nil = store_thm(
  "necklace_not_nil",
  ``!n a l. 0 < n /\ 0 < a /\ l IN (necklace n a) ==> l <> []``,
  METIS_TAC [necklace_property, LENGTH_NIL, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Theorem: Necklaces with some length but 0 colours are EMPTY. *)
val necklace_EMPTY = store_thm(
  "necklace_EMPTY",
  ``!n. 0 < n ==> (necklace n 0 = {})``,
  SRW_TAC [ARITH_ss][necklace_def, EXTENSION, EQ_IMP_THM]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (necklace n a) = a^n.                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: relate (necklace (n+1) a) to (necklace n a) for induction. *)
val necklace_SUC = store_thm(
  "necklace_SUC",
  ``!n a. necklace (SUC n) a = IMAGE (\(e,l). e :: l) (count a CROSS necklace n a)``,
  SRW_TAC [][necklace_def, EXTENSION, pairTheory.EXISTS_PROD, LENGTH_CONS, EQ_IMP_THM, count_def, SUBSET_DEF] THEN
  METIS_TAC [MEM]);

(* Theorem: The set of (necklace n a) is finite. *)
val FINITE_necklace = store_thm(
  "FINITE_necklace",
  ``!n a. FINITE (necklace n a)``,
  Induct_on `n` THEN SRW_TAC [][necklace_0, necklace_SUC]);

(* Theorem: CARD (necklace n a) = a^n. *)
val CARD_necklace = store_thm(
  "CARD_necklace",
  ``!n a. CARD (necklace n a) = a ** n``,
  Induct_on `n` THEN1
  SRW_TAC [][necklace_0] THEN
  SRW_TAC [][FINITE_necklace, necklace_SUC, CARD_IMAGE, CARD_CROSS,
             pairTheory.FORALL_PROD, arithmeticTheory.EXP]);

(* ------------------------------------------------------------------------- *)
(* Monocoloured Necklace - a necklace with a single color.                   *)
(* ------------------------------------------------------------------------- *)

(* Define monocoloured necklace *)
val monocoloured_def = Define`
  monocoloured n a = {l | (l IN necklace n a) /\ (l <> [] ==> SING (set l)) }
`;

(* Theorem: A monocoloured necklace is indeed a necklace. *)
val monocoloured_necklace = store_thm(
  "monocoloured_necklace",
  ``!n a l. l IN monocoloured n a ==> l IN necklace n a``,
  SRW_TAC [][necklace_def, monocoloured_def]);

(* Theorem: Zero-length monocoloured set is singleton NIL. *)
val monocoloured_0 = store_thm(
  "monocoloured_0",
  ``!a. monocoloured 0 a = {[]}``,
  SRW_TAC [][monocoloured_def, EXTENSION, EQ_IMP_THM, necklace_0]);

(* Theorem: Unit-length monocoloured set consists of singletons. *)
val monocoloured_1 = store_thm(
  "monocoloured_1",
  ``!a. monocoloured 1 a = {[e] | e IN count a}``,
  SIMP_TAC bool_ss [monocoloured_def, necklace_def, count_def, arithmeticTheory.ONE, LENGTH_CONS] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION]);

(* Theorem: Unit-length necklaces are monocoloured. *)
val necklace_1_monocoloured = store_thm(
  "necklace_1_monocoloured",
  ``!a. necklace 1 a = monocoloured 1 a``,
  SRW_TAC [][necklace_def, monocoloured_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [LIST_1_SET_SING]);

(* Theorem: The monocoloured set is FINITE. *)
val FINITE_monocoloured = store_thm(
  "FINITE_monocoloured",
  ``!n a. FINITE (monocoloured n a)``,
  METIS_TAC [SUBSET_DEF, monocoloured_necklace, SUBSET_FINITE, FINITE_necklace]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (monocoloured n a) = a.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Relate (monocoloured (SUC n) a) to (monocoloured n a) for induction. *)
val monocoloured_SUC = prove(
  ``!n a. 0 < n ==> (monocoloured (SUC n) a = IMAGE (\l. HD l :: l) (monocoloured n a))``,
  SRW_TAC [][EXTENSION, EQ_IMP_THM, monocoloured_def] THEN
  FULL_SIMP_TAC (srw_ss()) [necklace_def, LENGTH_CONS, count_def, SUBSET_DEF, SING_DEF] THENL [
    Cases_on `l'` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
    `x <> [] /\ MEM h x /\ MEM h' x` by SRW_TAC [][] THEN
    `?k. !z . MEM z x <=> (z = k)` by SRW_TAC [][] THEN
    `h = h'` by METIS_TAC [] THEN SRW_TAC [][] THEN
    Q.EXISTS_TAC `x'` THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [] THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN METIS_TAC []
  ]);

(* Theorem: Size of (monocoloured n a) = a *)
val CARD_monocoloured = store_thm(
  "CARD_monocoloured",
  ``!n a. 0 < n ==> (CARD (monocoloured n a) = a)``,
  REPEAT STRIP_TAC THEN
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n` THENL [
    SRW_TAC [][monocoloured_1] THEN
    `{[e] | e < a} = IMAGE (\n. [n]) (count a)` by SRW_TAC [][EXTENSION] THEN
    SRW_TAC [][CARD_IMAGE],
    SRW_TAC [][monocoloured_SUC] THEN
    Q.MATCH_ABBREV_TAC `CARD (IMAGE f s) = a` THEN
    `!x y. (f x = f y) <=> (x = y)` by SRW_TAC [][EQ_IMP_THM, Abbr`f`] THEN
    `FINITE s` by SRW_TAC [][FINITE_monocoloured, Abbr`s`] THEN
    SRW_TAC [][CARD_IMAGE]
  ]);

(* ------------------------------------------------------------------------- *)
(* Multicoloured necklaces                                                   *)
(* ------------------------------------------------------------------------- *)

(* Define multicoloured necklace *)
val multicoloured_def = Define`
  multicoloured n a = (necklace n a) DIFF (monocoloured n a)
`;

(* Theorem: multicoloured is a necklace *)
val multicoloured_necklace = store_thm(
  "multicoloured_necklace",
  ``!l. l IN multicoloured n a ==> l IN necklace n a``,
  SRW_TAC [][multicoloured_def, necklace_def]);

(* Theorem: multicoloured set is FINITE *)
val FINITE_multicoloured = store_thm(
  "FINITE_multicoloured",
  ``!n a. FINITE (multicoloured n a)``,
  SRW_TAC [][multicoloured_def, FINITE_necklace, FINITE_DIFF]);

(* Theorem: multicoloured n 0 = EMPTY *)
val multicoloured_EMPTY = store_thm(
  "multicoloured_EMPTY",
  ``!n. 0 < n ==> (multicoloured n 0 = {})``,
  SRW_TAC [][multicoloured_def, multicoloured_necklace, necklace_EMPTY]);

(* Theorem: multicoloured 0 a = EMPTY *)
val multicoloured_0 = store_thm(
  "multicoloured_0",
  ``multicoloured 0 a = {}``,
  SRW_TAC [][multicoloured_def, necklace_0, monocoloured_0]);

(* Theorem: multicoloured l are not monocoloured *)
val multicoloured_not_monocoloured = store_thm(
  "multicoloured_not_monocoloured",
  ``!l n a. l IN multicoloured n a ==> ~(l IN monocoloured n a)``,
  SRW_TAC [][multicoloured_def]);

(* Theorem: mutlicoloured 1 a = EMPTY *)
val multicoloured_1_EMPTY = store_thm(
  "multicoloured_1_EMPTY",
  ``!a. multicoloured 1 a = {}``,
  SRW_TAC [][multicoloured_def, necklace_def, necklace_1_monocoloured]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (multicoloured n a) = a^n - a.  [milestone]                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: CARD (multicoloured n a) = a^n - a *)
val CARD_multicoloured = store_thm(
  "CARD_multicoloured",
  ``!n a. 0 < n ==> (CARD (multicoloured n a) = a**n - a)``,
  REPEAT STRIP_TAC THEN
  `FINITE (necklace n a)` by SRW_TAC [][FINITE_necklace] THEN
  `FINITE (monocoloured n a)` by SRW_TAC [][FINITE_monocoloured] THEN
  `(monocoloured n a) SUBSET (necklace n a)` by SRW_TAC [][monocoloured_necklace, SUBSET_DEF] THEN
  `CARD (multicoloured n a) = CARD (necklace n a DIFF monocoloured n a)` by SRW_TAC [][multicoloured_def] THEN
  `_ = CARD (necklace n a) - CARD (necklace n a INTER monocoloured n a)` by SRW_TAC [][CARD_DIFF] THEN
  `_ = CARD (necklace n a) - CARD (monocoloured n a)` by METIS_TAC [SUBSET_INTER_ABSORPTION, INTER_COMM] THEN
  METIS_TAC [CARD_necklace, CARD_monocoloured]);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
