(* ------------------------------------------------------------------------- *)
(* Cycle theorems for use in Fermat's Little Theorem.                        *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "Cycle";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* ------------------------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* ------------------------------------------------------------------------- *)
(* Theory of Cycles                                                          *)
(* ------------------------------------------------------------------------- *)

(* Define cycle in view of action from Z_n to necklace n a *)

(* Cycle as nth iterate of rotate: DROP 1 ++ TAKE 1 *)
val cycle_def = Define `
  cycle n l = FUNPOW (\l. DROP 1 l ++ TAKE 1 l) n l
`;

(* Theorem: cycle 0 l = l *)
val CYCLE_0 = store_thm(
  "CYCLE_0",
  ``!l. cycle 0 l = l``,
  SRW_TAC [][cycle_def]);

(* Theorem: Cycle (n+1) of a list is cycle once more of cycle n list.
            cycle (SUC n) l = cycle 1 (cycle n l) *)
val CYCLE_SUC = store_thm(
  "CYCLE_SUC",
  ``!l n. cycle (SUC n) l = cycle 1 (cycle n l)``,
  METIS_TAC [cycle_def, arithmeticTheory.FUNPOW_SUC, arithmeticTheory.FUNPOW_1]);

(* Theorem: Cycle is additive, (cycle n (cycle m l) = cycle (n+m) l  *)
val CYCLE_ADD = store_thm(
  "CYCLE_ADD",
  ``!l n m. cycle n (cycle m l) = cycle (n+m) l``,
  SRW_TAC [][cycle_def, arithmeticTheory.FUNPOW_ADD]);

(* Theorem: Only NIL will cycle to NIL. *)
val CYCLE_NIL = store_thm(
  "CYCLE_NIL",
  ``!l n. (cycle n l = []) <=> (l = [])``,
  `!l. (cycle 1 l = []) <=> (l = [])`
      by (SRW_TAC [][cycle_def, EQ_IMP_THM] THEN
          Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) []) THEN
  Induct_on `n` THEN1 SRW_TAC [][CYCLE_0] THEN
  SRW_TAC [][CYCLE_SUC]);

(* Theorem: Cycle keeps LENGTH (of necklace), or LENGTH (cycle n l) = LENGTH l *)
val CYCLE_SAME_LENGTH = store_thm(
  "CYCLE_SAME_LENGTH",
  ``!l n. LENGTH (cycle n l) = LENGTH l``,
  `!l. LENGTH (cycle 1 l) = LENGTH l`
      by (SRW_TAC [][cycle_def] THEN Cases_on `l` THEN
          SRW_TAC [ARITH_ss][]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* Theorem: Cycle keep set (of colors), or set (cycle n l) = set l *)
val CYCLE_SAME_SET = store_thm(
  "CYCLE_SAME_SET",
  ``!l n. set (cycle n l) = set l``,
  `!l. set (cycle 1 l) = set l`
      by (Cases_on `l` THEN SRW_TAC [][cycle_def, EXTENSION, DISJ_COMM]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* ------------------------------------------------------------------------- *)
(* To show:  cycle x (cycle y l) = cycle ((x + y) MOD n) l, n = LENGTH l.    *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For n <= LENGTH l, cycle n l = DROP n l ++ TAKE n l *)
val CYCLE_EQ_DROP_TAKE = prove(
  ``!l n. n <= LENGTH l ==> (cycle n l = DROP n l ++ TAKE n l)``,
  REPEAT STRIP_TAC THEN
  Cases_on `l = []` THEN1 (
    `LENGTH l = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `n = 0` by RW_TAC arith_ss [] THEN
    SRW_TAC [][CYCLE_0]) THEN
  Induct_on `n` THEN1 SRW_TAC [][CYCLE_0] THEN
  SRW_TAC [][CYCLE_SUC] THEN
  `n < LENGTH l` by RW_TAC arith_ss [] THEN
  SRW_TAC [ARITH_ss][] THEN
  SRW_TAC [][cycle_def] THEN
  SRW_TAC [][DROP_SUC, TAKE_SUC, DROP_1_APPEND, TAKE_1_APPEND, DROP_NON_NIL]);

(* Theorem: Cycle through length gives original, or cycle (LENGTH l) l = l. *)
val CYCLE_BACK = store_thm(
  "CYCLE_BACK",
  ``!l. cycle (LENGTH l) l = l``,
  SRW_TAC [][CYCLE_EQ_DROP_TAKE, DROP_LENGTH_NIL, TAKE_LENGTH_ID]);

(* Theorem: cycle (n*LENGTH l) l = l. *)
val CYCLE_BACK_MULTIPLE = store_thm(
  "CYCLE_BACK_MULTIPLE",
  ``!l n. cycle (n * LENGTH l) l = l``,
  Induct_on `n` THEN
  SRW_TAC [][CYCLE_0] THEN
  `SUC n * LENGTH l = LENGTH l + n * LENGTH l` by RW_TAC arith_ss [arithmeticTheory.MULT_SUC] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK]);

(* Theorem: if l <> [], cycle n l = cycle (n MOD (LENGTH l)) l *)
val CYCLE_MOD_LENGTH = store_thm(
  "CYCLE_MOD_LENGTH",
  ``!l n. l <> [] ==> (cycle n l = cycle (n MOD (LENGTH l)) l)``,
  REPEAT STRIP_TAC THEN
  `0 < LENGTH l` by SRW_TAC [][LENGTH_NON_NIL] THEN
  `n = n DIV LENGTH l * LENGTH l + n MOD LENGTH l` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = n MOD LENGTH l + n DIV LENGTH l * LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK_MULTIPLE]);

(* Theorem: cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l *)
val CYCLE_ADDITION = store_thm(
  "CYCLE_ADDITION",
  ``!l x y. l <> [] ==> (cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l)``,
  METIS_TAC [CYCLE_ADD, CYCLE_MOD_LENGTH]);

(* Theorem: cycle (LENGTH l - n) (cycle n l) = l *)
val CYCLE_INV = store_thm(
  "CYCLE_INV",
  ``!n l. n <= LENGTH l ==> (cycle (LENGTH l - n) (cycle n l) = l)``,
  REPEAT STRIP_TAC THEN
  `(LENGTH l - n) + n = LENGTH l` by RW_TAC arith_ss [] THEN
  SRW_TAC [][CYCLE_ADD, CYCLE_BACK]);

(* Theorem: A mono-list has cycle 1 l = l *)
val SING_HAS_CYCLE_1 = store_thm(
  "SING_HAS_CYCLE_1",
  ``!l. SING (set l) ==> (cycle 1 l = l)``,
  SRW_TAC [][cycle_def] THEN
  Cases_on `l` THEN SRW_TAC [][] THEN
  FULL_SIMP_TAC (srw_ss()) [SING_INSERT] THEN
  `SING (set (t ++ [h]))` by SRW_TAC [][SING_UNION] THEN
  SRW_TAC [ARITH_ss][MONOLIST_EQ]);

(* Theorem: cycle 1 (h::t) = t ++ [h] *)
val CYCLE_1_EQ = store_thm(
  "CYCLE_1_EQ",
  ``!h t. cycle 1 (h::t) = t ++ [h]``,
  SRW_TAC [][cycle_def]);

(* Theorem: (t ++ [h] <> h::t) if (set t) has some element h' <> h. *)
val CYCLE_1_NEQ = store_thm(
  "CYCLE_1_NEQ",
  ``!h h' t. h' IN set t /\ h' <> h ==> (t ++ [h] <> h::t)``,
  SRW_TAC [][] THEN
  Induct_on `t` THEN SRW_TAC [][] THEN
  METIS_TAC []);

(* Theorem: [inverse of SING_HAS_CYCLE_1]
            !l. (cycle 1 l = l) ==> SING (set l)  *)
val CYCLE_1_NONEMPTY_MONO = store_thm(
  "CYCLE_1_NONEMPTY_MONO",
  ``!l. l <> [] /\ (cycle 1 l = l) ==> SING (set l)``,
  REPEAT STRIP_TAC THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);


(* Theorem: A mono-list l has cycle 1 l = l, or LENGTH l = 1 ==> cycle 1 l = l *)
val MONO_HAS_CYCLE_1 = store_thm(
  "MONO_HAS_CYCLE_1",
  ``!l. (LENGTH l = 1) ==> (cycle 1 l = l)``,
  REPEAT STRIP_TAC THEN
  `LENGTH l <> 0` by RW_TAC arith_ss [] THEN
  `?h t. l = h::t` by METIS_TAC [LENGTH_NIL, list_CASES] THEN
  `_ = [h] ++ t` by SRW_TAC [][] THEN
  `LENGTH [h] = 1` by RW_TAC arith_ss [LENGTH] THEN
  `LENGTH t = 0` by FULL_SIMP_TAC (srw_ss()) [LENGTH_APPEND] THEN
  `t = []` by METIS_TAC [LENGTH_NIL] THEN
  SRW_TAC [][cycle_def]);

(* Theorem: cycle 1 l = l makes cycle trivial, or cycle 1 l = l ==> cycle n l = l *)
val CYCLE_1_FIX = store_thm(
  "CYCLE_1_FIX",
  ``!l n. (cycle 1 l = l) ==> (cycle n l = l)``,
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* Theorem: If a value n can cycle back, multiples of n can also cycle back. *)
(* Proof: by induction on m. *)
val CYCLE_MULTIPLE_BACK = store_thm(
  "CYCLE_MULTIPLE_BACK",
  ``!n l. (cycle n l = l) ==> !m. cycle (m*n) l = l``,
  REPEAT STRIP_TAC THEN
  Induct_on `m` THEN
  METIS_TAC [arithmeticTheory.MULT_CLAUSES, CYCLE_0, CYCLE_ADD]);


(* Theorem: If two values m, n can cycle back, gcd(m,n) can also cycle back. *)
(* Proof:
     If n = 0, cycle (gcd m 0) l = cycle m l = l by given, GCD_0R.
     If n <> 0, ?p q. p * n = q * m + gcd m n    by LINEAR_GCD,

     cycle (gcd m n) l
   = cycle (gcd m n) (cycle m l)                 by given
   = cycle (gcd m n) (cycle q*m l)               by CYCLE_MULTIPLE_BACK
   = cycle (gcd m n + q*m) l                     by CYCLE_ADD
   = cycle (q*m + gcd m n) l                     by arithmetic
   = cycle (p*n) l                               by substitution
   = cycle n l                                   by CYCLE_MULTIPLE_BACK
   = l                                           by given
*)
val CYCLE_GCD_BACK = store_thm(
  "CYCLE_GCD_BACK",
  ``!m n l. (cycle m l = l) /\ (cycle n l = l) ==> (cycle (gcd m n) l = l)``,
  REPEAT STRIP_TAC THEN
  Cases_on `n=0` THEN1
  METIS_TAC [gcdTheory.GCD_0R] THEN
  `?p q. p * n = q * m + gcd m n` by METIS_TAC [gcdTheory.LINEAR_GCD] THEN
  `cycle (gcd m n) l = cycle (gcd m n) (cycle (q*m) l)` by METIS_TAC [CYCLE_MULTIPLE_BACK] THEN
  METIS_TAC [CYCLE_ADD, arithmeticTheory.ADD_COMM, CYCLE_MULTIPLE_BACK]);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
