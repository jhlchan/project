(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Combinatorial proof.                            *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by J. Petersen in 1872:

Take p elements from q with repetitions in all ways, that is, in q^p ways.
The q sets with elements all alike are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted in sets of p. Hence p divides q^p - q.

This is a combinatorial using Group action, via Fixed Points Congruence of Zp.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTfixedpoints";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* val _ = load "dividesTheory"; *)

(* Part 1: Basis ----------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* Part 2: General Theory -------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Theory of necklaces                                                       *)
(* ------------------------------------------------------------------------- *)

(* val _ = load "NecklaceTheory"; *)
open NecklaceTheory;

(* val _ = load "CycleTheory"; *)
open CycleTheory;

(* val _ = load "GroupTheory"; *)
open GroupTheory;

(* val _ = load "SubgroupTheory"; *)
open SubgroupTheory;

(* val _ = load "ActionTheory"; *)
open ActionTheory;

(* ------------------------------------------------------------------------- *)
(* Fixed Points of action.                                                   *)
(* ------------------------------------------------------------------------- *)

(* Fixedpoints of action: f g X = {x in X | !z in G. f z x = x} *)
val fixedpoints_def = Define`
  fixedpoints f g X = {x | x IN X /\ (!z. z IN g.carrier ==> (f z x = x)) }
`;

(* Theorem: x IN (fixedpoints f g X) ==> x IN X *)
val FIXEDPOINTS_ELEMENT = store_thm(
  "FIXEDPOINTS_ELEMENT",
  ``!f g X. x IN (fixedpoints f g X) ==> x IN X``,
  SRW_TAC [][fixedpoints_def]);

(* Theorem: a IN fixedpoints f g X ==> (orbit f g X a = {a}) *)
val FIXPOINT_ORBIT_SING = store_thm(
  "FIXPOINT_ORBIT_SING",
  ``!f g X. Group g /\ action f g X ==>
     !a. a IN fixedpoints f g X ==> (orbit f g X a = {a})``,
  SRW_TAC [][fixedpoints_def, orbit_def, reach_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [group_id_carrier]);

(* Theorem: For Action f g X, a IN X, (orbit f g X a = {a}) ==> a IN fixedpoints f g X *)
val ORBIT_SING_FIXPOINT = store_thm(
  "ORBIT_SING_FIXPOINT",
  ``!f g X. action f g X ==>
    !a. a IN X /\ (orbit f g X a = {a}) ==> a IN fixedpoints f g X``,
  SRW_TAC [][action_def, fixedpoints_def, orbit_def, reach_def, EXTENSION] THEN
  METIS_TAC []);

(* Theorem: a IN fixedpoints f g X <=> SING (orbit f g X a}) *)
val FIXPOINT_ORBIT_IS_SING = store_thm(
  "FIXPOINT_ORBIT_IS_SING",
  ``!f g X. Group g /\ action f g X ==> !a. a IN X ==> (a IN fixedpoints f g X <=> SING (orbit f g X a))``,
  METIS_TAC [FIXPOINT_ORBIT_SING, ORBIT_SING_FIXPOINT, ACTION_ORBIT_HAS_ITSELF, SING_DEF, IN_SING]);

val singleorbits_def = Define`
    singleorbits f g X = { e | e IN (TargetPartition f g X) /\ SING e }
`;
val multiorbits_def = Define`
    multiorbits f g X = { e | e IN (TargetPartition f g X) /\ ~ SING e }
`;

(* Theorem: TargetPartition = singleorbits + multiorbits. *)
val TARGET_PARTITION_EQ_SING_MULTI = store_thm(
  "TARGET_PARTITION_EQ_SING_MULTI",
  ``!f g X. singleorbits f g X  = (TargetPartition f g X) DIFF (multiorbits f g X)``,
  SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION, EQ_IMP_THM]);

(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is surjective. *)
val SING_ORBITS_TO_FIXEDPOINTS_SURJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_SURJ",
  ``!f g X. Group g /\ action f g X ==> SURJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  SRW_TAC [][singleorbits_def, SURJ_DEF] THEN1
  METIS_TAC [CHOICE_SING, ORBIT_SING_FIXPOINT, TARGET_PARTITION_ELEMENT_IS_ORBIT,
             TARGET_PARTITION_ELEMENT_ELEMENT, SING_DEF, IN_SING] THEN
  `orbit f g X x = {x}` by RW_TAC std_ss [FIXPOINT_ORBIT_SING] THEN
  Q.EXISTS_TAC `{x}` THEN
  FULL_SIMP_TAC (srw_ss()) [TargetPartition_def, partition_def, orbit_def] THEN
  METIS_TAC [FIXEDPOINTS_ELEMENT]);

(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is injective. *)
val SING_ORBITS_TO_FIXEDPOINTS_INJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_INJ",
  ``!f g X. Group g /\ action f g X ==> INJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  SRW_TAC [][singleorbits_def, INJ_DEF] THEN
  METIS_TAC [CHOICE_SING, ORBIT_SING_FIXPOINT, TARGET_PARTITION_ELEMENT_IS_ORBIT,
             TARGET_PARTITION_ELEMENT_ELEMENT, SING_DEF, IN_SING] THEN
  METIS_TAC [SING_DEF, CHOICE_SING]);

(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is bijective. *)
val SING_ORBITS_TO_FIXEDPOINTS_BIJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_BIJ",
  ``!f g X. Group g /\ action f g X ==> BIJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  RW_TAC std_ss [BIJ_DEF, SING_ORBITS_TO_FIXEDPOINTS_SURJ, SING_ORBITS_TO_FIXEDPOINTS_INJ]);

(* Theorem: For action f g X, singleorbits is the same size as fixedpoints f g X *)
val CARD_ORBITS_SING = store_thm(
  "CARD_ORBITS_SING",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> (CARD (singleorbits f g X) = CARD (fixedpoints f g X))``,
  REPEAT STRIP_TAC THEN
  `(singleorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][singleorbits_def, SUBSET_DEF] THEN
  `(fixedpoints f g X) SUBSET X` by SRW_TAC [][fixedpoints_def, SUBSET_DEF] THEN
  METIS_TAC [SING_ORBITS_TO_FIXEDPOINTS_BIJ, FINITE_BIJ_CARD_EQ, SUBSET_FINITE, FINITE_TARGET_PARTITION]);

(* Theorem: For action f g X, CARD X = CARD fixedpoints + SIGMA CARD multiorbits *)
val CARD_TARGET_BY_ORBITS = store_thm(
  "CARD_TARGET_BY_ORBITS",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> (CARD X = CARD (fixedpoints f g X) + SIGMA CARD (multiorbits f g X))``,
  REPEAT STRIP_TAC THEN
  `(singleorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][singleorbits_def, SUBSET_DEF] THEN
  `(multiorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][multiorbits_def, SUBSET_DEF] THEN
  `FINITE (singleorbits f g X) /\ FINITE (multiorbits f g X)` by METIS_TAC [FINITE_TARGET_PARTITION, SUBSET_FINITE] THEN
  `(singleorbits f g X) INTER (multiorbits f g X) = {}` by SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION] THEN1
  METIS_TAC [] THEN
  `(singleorbits f g X) UNION (multiorbits f g X) = TargetPartition f g X`
    by SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION] THEN1
  METIS_TAC [] THEN
  `!e. e IN (singleorbits f g X) ==> (CARD e = 1)` by SRW_TAC [][singleorbits_def, SING_DEF] THEN1
  RW_TAC std_ss [CARD_SING] THEN
  `CARD X = SIGMA CARD ((singleorbits f g X) UNION (multiorbits f g X))` by RW_TAC std_ss [CARD_TARGET_BY_PARTITION] THEN
  `_ = SIGMA CARD (singleorbits f g X) + SIGMA CARD (multiorbits f g X)` by RW_TAC std_ss [SUM_IMAGE_UNION, SUM_IMAGE_THM] THEN
  `_ = 1 * CARD (singleorbits f g X) + SIGMA CARD (multiorbits f g X)` by RW_TAC std_ss [SIGMA_CARD_CONSTANT] THEN
  RW_TAC arith_ss [CARD_ORBITS_SING]);

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof via Group action.                                     *)
(* ------------------------------------------------------------------------- *)

(* val _ = load "GroupInstancesTheory"; *)
open GroupInstancesTheory;

(* Theorem: For prime p and action f (Z p) X, e IN (multiorbits f (Z p) X) ==> CARD e = p. *)
val CARD_ZP_NONFIX_ORBIT = store_thm(
  "CARD_ZP_NONFIX_ORBIT",
  ``!p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
   !e. e IN (multiorbits f (Z p) X) ==> (CARD e = p)``,
  SRW_TAC [][multiorbits_def] THEN
  `Group (Z p)` by SRW_TAC [][ZN_group, dividesTheory.PRIME_POS] THEN
  `FINITE (Z p).carrier` by SRW_TAC [][Z_def, FINITE_COUNT] THEN
  `CARD (Z p).carrier = p` by SRW_TAC [][Z_def, CARD_COUNT] THEN
  `FiniteGroup (Z p)` by SRW_TAC [][FiniteGroup_def] THEN
  `FINITE e` by METIS_TAC [FINITE_TARGET_PARTITION_ELEMENT] THEN
  `CARD e <> 1` by METIS_TAC [SING_IFF_CARD1] THEN
  `?a. a IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
  `a IN X` by METIS_TAC [TARGET_PARTITION_ELEMENT_ELEMENT] THEN
  `e = orbit f (Z p) X a` by SRW_TAC [][TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `p = CARD e * CARD (stabilizer f (Z p) a)` by METIS_TAC [ORBIT_STABILIZER_THEOREM] THEN
  `divides (CARD e) p` by METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_COMM] THEN
  METIS_TAC [dividesTheory.prime_def]);

(* Theorem: [Fixed-Point Congruence for Zp action]
            For prime p and action f (Z p) X and FINITE X,
            (CARD X = CARD (fixedpoints f (Z p) X)) (mod p). *)
val ZP_FIXEDPOINTS_CONGRUENCE = store_thm(
  "ZP_FIXEDPOINTS_CONGRUENCE",
  ``!p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
     (CARD X MOD p = CARD (fixedpoints f (Z p) X) MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `Group (Z p)` by SRW_TAC [][ZN_group] THEN
  `!e. e IN (multiorbits f (Z p) X) ==> (CARD e = p)`
      by METIS_TAC [CARD_ZP_NONFIX_ORBIT] THEN
  `(multiorbits f (Z p) X) SUBSET (TargetPartition f (Z p) X)`
      by SRW_TAC [][multiorbits_def, SUBSET_DEF] THEN
  `FINITE (multiorbits f (Z p) X)`
      by METIS_TAC [SUBSET_FINITE, FINITE_TARGET_PARTITION] THEN
  `CARD X = CARD (fixedpoints f (Z p) X) + SIGMA CARD (multiorbits f (Z p) X)`
      by SRW_TAC [][CARD_TARGET_BY_ORBITS] THEN
  `_ = CARD (fixedpoints f (Z p) X) + p * CARD (multiorbits f (Z p) X)`
      by SRW_TAC [][SIGMA_CARD_CONSTANT] THEN
  `_ = CARD (multiorbits f (Z p) X) * p  + CARD (fixedpoints f (Z p) X)`
      by RW_TAC arith_ss [] THEN
  METIS_TAC [arithmeticTheory.MOD_TIMES]);

(* Theorem: Cycle is a group action from Zn to necklace n a. *)
val CYCLE_ACTION = store_thm(
  "CYCLE_ACTION",
  ``!n a. 0 < n /\ 0 < a ==> action cycle (Z n) (necklace n a)``,
  SRW_TAC [][action_def, necklace_def, Z_def, RES_FORALL_THM] THEN
  METIS_TAC [CYCLE_0, CYCLE_SAME_LENGTH, CYCLE_SAME_SET, CYCLE_ADDITION,
             LENGTH_NIL, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Theorem: A monocoloured necklace l has cycle 1 l = l *)
val monocoloured_cycle1 = prove(
  ``!n a l. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> (cycle 1 l = l)``,
  SRW_TAC [][monocoloured_def] THEN
  `l <> []` by METIS_TAC [necklace_not_nil] THEN
  METIS_TAC [SING_HAS_CYCLE_1]);

(* Theorem: If a necklace l has cycle 1 l = l , it must be monocoloured *)
val cycle1_monocoloured = prove(
  ``!l n a. 0 < a /\ l IN necklace n a /\ (cycle 1 l = l) ==> l IN monocoloured n a``,
  SRW_TAC [][monocoloured_def, necklace_def] THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* Theorem: A necklace l is monocoloured iff cycle 1 l = 1 *)
val monocoloured_iff_cycle1 = store_thm(
  "monocoloured_iff_cycle1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN necklace n a ==>
     (l IN monocoloured n a <=> (cycle 1 l = l))``,
  METIS_TAC [monocoloured_necklace, cycle1_monocoloured, monocoloured_cycle1]);

(* ------------------------------------------------------------------------- *)
(* Fixed Points of cycle are monocoloured necklaces.                         *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The fixedpoints of cycle are necklaces of cycle 1 l = l. *)
val CYCLE_FIXPOINTS = store_thm(
  "CYCLE_FIXPOINTS",
  ``!n a. 0 < n /\ 0 < a  ==>
    (fixedpoints cycle (Z n) (necklace n a) = {l | l IN (necklace n a) /\ (cycle 1 l = l)} )``,
  SRW_TAC [][fixedpoints_def, necklace_def, Z_def, EXTENSION, EQ_IMP_THM] THEN1
  (Cases_on `LENGTH x = 1` THEN1 METIS_TAC [MONO_HAS_CYCLE_1] THEN
   `1 < LENGTH x` by RW_TAC arith_ss [] THEN METIS_TAC []) THEN
  METIS_TAC [CYCLE_1_FIX]);

(* Theorem: The fixedpoints of cycle are monocoloured necklaces. *)
val CYCLE_FIXPOINTS_ARE_MONOCOLOURED = store_thm(
  "CYCLE_FIXPOINTS_ARE_MONOCOLOURED",
  ``!n a. 0 < n /\ 0 < a  ==>
    (fixedpoints cycle (Z n) (necklace n a) = (monocoloured n a) )``,
  REPEAT STRIP_TAC THEN
  `fixedpoints cycle (Z n) (necklace n a) = {l | l IN (necklace n a) /\ (cycle 1 l = l)}`
    by SRW_TAC [][CYCLE_FIXPOINTS] THEN
  SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [monocoloured_necklace, monocoloured_iff_cycle1]);

(* Theorem: (Fermat's Little Theorem by Group action) For prime p, a**p = a (mod p). *)
(* Proof:
   !p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
     (CARD X MOD p = CARD (fixedpoints f (Z p) X) MOD p)      by ZP_FIXEDPOINTS_CONGRUENCE
   !n a. 0 < n /\ 0 < a  ==>
     (fixedpoints cycle (Z n) (necklace n a) = (monocoloured n a) )
                                                              by CYCLE_FIXPOINTS_ARE_MONOCOLOURED
   !n a. 0 < n /\ 0 < a ==> action cycle (Z n) (necklace n a) by CYCLE_ACTION
   !n a. FINITE (necklace n a)                                by FINITE_necklace
   !n a. CARD (necklace n a) = a ** n                         by CARD_necklace
   !n. 0 < n ==> (CARD (monocoloured n a) = a)                by CARD_monocoloured
*)
val FERMAT_LITTLE_THM = store_thm(
  "FERMAT_LITTLE_THM",
  ``!p a. prime p ==> (a**p MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Cases_on `a = 0` THEN1 METIS_TAC [arithmeticTheory.EXP_EQ_0, arithmeticTheory.ZERO_MOD] THEN
  `0 < a` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ACTION, FINITE_necklace, CARD_necklace, CARD_monocoloured,
             CYCLE_FIXPOINTS_ARE_MONOCOLOURED, ZP_FIXEDPOINTS_CONGRUENCE]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
