(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Combinatorial proof.                            *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by J. Petersen in 1872:

Take p elements from q with repetitions in all ways, that is, in q^p ways.
The q sets with elements all alike are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted in sets of p. Hence p divides q^p - q.

This is a combinatorial using Group action, via Orbit-Stabilizer Theorem.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTaction";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* Part 2: General Theory -------------------------------------------------- *)

(* val _ = load "NecklaceTheory"; *)
open NecklaceTheory;

(* val _ = load "CycleTheory"; *)
open CycleTheory;

(* val _ = load "GroupTheory"; *)
open GroupTheory;

(* val _ = load "SubgroupTheory"; *)
open SubgroupTheory;

(* val _ = load "ActionTheory"; *)
open ActionTheory;

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* val _ = load "GroupInstancesTheory"; *)
open GroupInstancesTheory;

(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof via Group action.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If l is a necklace, cycle n l is also a necklace. *)
val NECKLACE_CYCLE = store_thm(
  "NECKLACE_CYCLE",
  ``!l. l IN necklace n a ==> !k. (cycle k l) IN necklace n a``,
  SRW_TAC [][necklace_def, CYCLE_SAME_LENGTH, CYCLE_SAME_SET]);

(* Theorem: [Closure of cycle for multicoloured necklaces]
            If l is a multicoloured necklace, cycle n l is also multicolored. *)
val multicoloured_CYCLE = store_thm(
  "multicoloured_CYCLE",
  ``!l n a. 0 < n /\ 0 < a /\ l IN multicoloured n a ==> !k. (cycle k l) IN multicoloured n a``,
  REPEAT STRIP_TAC THEN
  `l IN (necklace n a)` by SRW_TAC [][multicoloured_necklace] THEN
  `l <> []` by METIS_TAC [necklace_not_nil] THEN
  FULL_SIMP_TAC (srw_ss()) [multicoloured_def, monocoloured_def, NECKLACE_CYCLE, CYCLE_SAME_SET] THEN
  METIS_TAC [LENGTH_NIL, CYCLE_SAME_LENGTH]);

(* Theorem: CYCLE is an action on multicoloured necklaces *)
val cycle_action_on_multicoloured = store_thm(
  "cycle_action_on_multicoloured",
  ``!n a. 0 < n /\ 0 < a ==> action cycle (Z n) (multicoloured n a)``,
  SRW_TAC [][action_def, Z_def, RES_FORALL_THM] THEN1
  METIS_TAC [multicoloured_CYCLE] THEN1
  METIS_TAC [CYCLE_0] THEN
  METIS_TAC [CYCLE_ADDITION, multicoloured_necklace, necklace_not_nil, necklace_property]);

(* Theorem: !n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==> cycle 1 l <> l *)
val multicoloured_NOT_CYCLE_1 = store_thm(
  "multicoloured_NOT_CYCLE_1",
  ``!n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==> cycle 1 l <> l``,
  SRW_TAC [][multicoloured_def, monocoloured_def] THEN
  METIS_TAC [CYCLE_1_NONEMPTY_MONO]);

(* Theorem: !n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==>
     NOT SING (orbit cycle (Z n) (multicoloured n a) l) *)
val multicoloured_ORBIT_NOT_SING = store_thm(
  "multicoloured_ORBIT_NOT_SING",
  ``!n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==>
    ~ SING (orbit cycle (Z n) (multicoloured n a) l)``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `action cycle (Z n) (multicoloured n a)` by SRW_TAC [][cycle_action_on_multicoloured] THEN
  `l IN (orbit cycle (Z n) (multicoloured n a) l)` by METIS_TAC [ACTION_ORBIT_HAS_ITSELF, ZN_group] THEN
  `(orbit cycle (Z n) (multicoloured n a) l) = {l}` by METIS_TAC [SING_DEF, IN_SING] THEN
  `Group (Z n)` by SRW_TAC [][ZN_group] THEN
  Cases_on `n=1` THENL [
    `orbit cycle (Z n) (multicoloured n a) l = EMPTY` by METIS_TAC [multicoloured_1_EMPTY, orbit_def, NOT_IN_EMPTY] THEN
    METIS_TAC [NOT_SING_EMPTY],
    `1 < n` by RW_TAC arith_ss [] THEN
    `1 IN (Z n).carrier` by SRW_TAC [][Z_def, IN_COUNT] THEN
    `cycle 1 l IN (orbit cycle (Z n) (multicoloured n a) l)` by SRW_TAC [][ORBIT_HAS_ACTION_ELEMENT] THEN
    `cycle 1 l <> l` by METIS_TAC [multicoloured_NOT_CYCLE_1] THEN
    METIS_TAC [IN_SING]
  ]);

(* Theorem: CARD (orbit cycle (Z n) (multicoloured n a) l) <> 1 *)
val CARD_multicoloured_ORBIT_NOT_1 = store_thm(
  "CARD_multicoloured_ORBIT_NOT_1",
  ``!n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==>
    CARD (orbit cycle (Z n) (multicoloured n a) l) <> 1``,
  REPEAT STRIP_TAC THEN
  `~ SING (orbit cycle (Z n) (multicoloured n a) l)` by SRW_TAC [][multicoloured_ORBIT_NOT_SING] THEN
  `action cycle (Z n) (multicoloured n a)` by SRW_TAC [][cycle_action_on_multicoloured] THEN
  `FINITE (orbit cycle (Z n) (multicoloured n a) l)` by METIS_TAC [FINITE_multicoloured, ORBIT_SUBSET_TARGET, SUBSET_FINITE] THEN
  METIS_TAC [SING_IFF_CARD1]);

(* Theorem: Orbits of cycle action on multicoloured necklaces are of length p, for prime p *)
val CARD_multicoloured_PRIME_ORBIT = store_thm(
  "CARD_multicoloured_PRIME_ORBIT",
  ``!p a. prime p /\ 0 < a /\ l IN (multicoloured p a) ==>
    (CARD (orbit cycle (Z p) (multicoloured p a) l) = p)``,
  REPEAT STRIP_TAC THEN
  `0 < p /\ 1 < p` by METIS_TAC [dividesTheory.PRIME_POS, dividesTheory.ONE_LT_PRIME] THEN
  `FiniteGroup (Z p)` by SRW_TAC [][ZN_finite_group] THEN
  `FINITE (multicoloured p a)` by SRW_TAC [][FINITE_multicoloured] THEN
  `action cycle (Z p) (multicoloured p a)` by METIS_TAC [cycle_action_on_multicoloured] THEN
  `p = CARD (orbit cycle (Z p) (multicoloured p a) l) * CARD (stabilizer cycle (Z p) l)` by METIS_TAC [ORBIT_STABILIZER_THEOREM, CARD_ZN_carrier] THEN
  `CARD (orbit cycle (Z p) (multicoloured p a) l) <> 1` by METIS_TAC [CARD_multicoloured_ORBIT_NOT_1] THEN
  METIS_TAC [FACTORS_OF_PRIME]);

(* Theorem: [Fermat's Little Theorem] !p a. prime p ==> divides p (a**p - a)     *)
(* Proof (J. Petersen in 1872):
   Take p elements from a with repetitions in all ways, that is, in a^p ways.
                   by CARD_necklace
   The a sets with elements all alike are not changed by a cyclic permutation of the elements,
                   by CARD_monocoloured
   while the remaining (a^p - a) sets are
                   by CARD_multicoloured
   permuted in sets of p.
                   by cycle_action_on_multicoloured, CARD_multicoloured_PRIME_ORBIT
   Hence p divides a^p - a.
                   by EQUAL_SIZE_ORBITS_PROPERTY
*)
val FERMAT_LITTLE_THM = store_thm(
  "FERMAT_LITTLE_THM",
  ``!p a. prime p ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  Cases_on `a = 0` THENL [
    SRW_TAC [][] THEN
    METIS_TAC [arithmeticTheory.EXP_EQ_0, dividesTheory.PRIME_POS, dividesTheory.ALL_DIVIDES_0],
    `0 < p /\ 0 < a` by RW_TAC arith_ss [dividesTheory.PRIME_POS] THEN
    METIS_TAC [CARD_multicoloured, CARD_multicoloured_PRIME_ORBIT, EQUAL_SIZE_ORBITS_PROPERTY, ZN_group, cycle_action_on_multicoloured, FINITE_multicoloured]
  ]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
