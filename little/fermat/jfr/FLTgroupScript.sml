(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Group-theoretic Proof.                          *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Group Theory)
======================================
Given a finite group G, consider an element a in G.

Since G is finite, element a has an order: (order a), and a^(order a) = e.

This also means that, the generated subgroup <a> has size (order a).

By Lagrange identity, size of group = k * size of subgroup.

Hence, |G| = k * |<a>|, and a^|<a>| = e.

This implies:   a^|G| = a^(k*|<a>|) = a^(|<a>*k) = (a^|<a>|)^k = e^k = e.

This is the group equivalent of Fermat's Little Theorem.

By putting G = Z*p, a IN Z*p means 0 < a < p,
then a^|G| mod p = 1, or a^(p-1) mod p = 1.

By putting G = Phi*n = {a | a < n /\ gcd(a,n) = 1 },
then a^|G| mod n = 1, or a^phi(n) mod n = 1.

which is Euler's generalization of Fermat's Little Theorem.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTgroup";

(* open dependent theories *)
open pred_setTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* Part 2: General Theory -------------------------------------------------- *)

(* val _ = load "GroupTheory"; *)
open GroupTheory;

(* val _ = load "SubgroupTheory"; *)
open SubgroupTheory;

(* val _ = load "FiniteGroupTheory"; *)
open FiniteGroupTheory;

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* val _ = load "GroupInstancesTheory"; *)
open GroupInstancesTheory;

(* ------------------------------------------------------------------------- *)
(* Group-theoretic Proof applied to Z*p.                                     *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Residue -- close-relative of COUNT                                        *)
(* ------------------------------------------------------------------------- *)

(* Define the set of residues = nonzero remainders *)
val residue_def = Define `residue n = { i | (0 < i) /\ (i < n) }`;

(* Theorem: count n = 0 INSERT (residue n) *)
val RESIDUE_COUNT = store_thm(
  "RESIDUE_COUNT",
  ``!n. 0 < n ==> (count n = 0 INSERT (residue n))``,
  SRW_TAC [ARITH_ss][residue_def, EXTENSION]);

(* Theorem: FINITE (residue n) *)
val FINITE_RESIDUE = store_thm(
  "FINITE_RESIDUE",
  ``!n. FINITE (residue n)``,
  Cases THEN1 SRW_TAC [][residue_def] THEN
  METIS_TAC [RESIDUE_COUNT, FINITE_INSERT, count_def, FINITE_COUNT,
             DECIDE ``0 < SUC n``]);

(* Theorem: For n > 0, CARD (residue n) = n-1 *)
val CARD_RESIDUE = store_thm(
  "CARD_RESIDUE",
  ``!n. 0 < n ==> (CARD (residue n) = n-1)``,
  REPEAT STRIP_TAC THEN
  `0 NOTIN (residue n)` by SRW_TAC [][residue_def] THEN
  `0 INSERT (residue n) = count n`
    by SRW_TAC [][residue_def, EXTENSION, EQ_IMP_THM] THEN RW_TAC arith_ss [] THEN
  `SUC (CARD (residue n)) = n` by METIS_TAC [FINITE_RESIDUE, CARD_INSERT, CARD_COUNT] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* Establish the existence of multiplicative inverse when p is prime.        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Euclid's Lemma] A prime divides a product iff the prime divides a factor.
            [in MOD notation] For prime p, x*y MOD p = 0 <=> x MOD p = 0 or y MOD p = 0 *)
val EUCLID_LEMMA = store_thm(
  "EUCLID_LEMMA",
  ``!p x y. prime p ==> (((x * y) MOD p = 0) <=> (x MOD p = 0) \/ (y MOD p = 0))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  SRW_TAC [][MOD_0_DIVIDES, EQ_IMP_THM] THEN
  METIS_TAC [gcdTheory.P_EUCLIDES, dividesTheory.DIVIDES_MULT, arithmeticTheory.MULT_COMM]);

(* Theorem: [Existence of Inverse] For prime p, 0 < x < p ==> ?y. y*x MOD p = 1 *)
val MOD_MULT_INV = store_thm(
  "MOD_MULT_INV",
  ``!p x. prime p /\ 0 < x /\ x < p ==> ?y. 0 < y /\ y < p /\ ((y*x) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `gcd p x = 1` by METIS_TAC [gcdTheory.PRIME_GCD, dividesTheory.NOT_LT_DIVIDES] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `?k q. k * x = q * p + 1` by METIS_TAC [gcdTheory.LINEAR_GCD, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  `(k * x) MOD p = 1` by METIS_TAC [arithmeticTheory.MOD_MULT, dividesTheory.ONE_LT_PRIME] THEN
  `((k MOD p) * x) MOD p = 1` by METIS_TAC [arithmeticTheory.MOD_TIMES2, arithmeticTheory.LESS_MOD] THEN
  `1 <> 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [arithmeticTheory.MOD_LESS, EUCLID_LEMMA, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Convert this into an existence definition *)
val lemma = prove(
  ``!p x. ?y. prime p /\ 0 < x /\ x < p ==>
              0 < y /\ y < p /\ ((y*x) MOD p = 1)``,
  METIS_TAC [MOD_MULT_INV]);

val MUL_INV_DEF = new_specification(
  "MUL_INV_DEF",
  ["MOD_MUL_INV"],
  SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* ------------------------------------------------------------------------- *)
(* The Group Z*p = Multiplication Modulo p, for prime p.                     *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The Group of Multiplication modulo p                                      *)
(* ------------------------------------------------------------------------- *)

(* Define Multiplicative Modulo p Group *)
val Zstar_def = Define`
  Zstar p =
   <| carrier := residue p;
           id := 1;
          inv := MOD_MUL_INV p;
         mult := (\i j. (i * j) MOD p)
    |>`;

(* Theorem: FINITE (Zstar p).carrier *)
val FINITE_Zstar_carrier = store_thm(
  "FINITE_Zstar_carrier",
  ``!p. FINITE (Zstar p).carrier``,
  SRW_TAC [][Zstar_def] THEN
  `residue p SUBSET count p` by SRW_TAC [][residue_def, SUBSET_DEF] THEN
  METIS_TAC [FINITE_COUNT, SUBSET_FINITE]);

(* Theorem: p > 0 ==> CARD (Zstar p).carrier = p-1 *)
val CARD_Zstar_carrier = store_thm(
  "CARD_Zstar_carrier",
  ``!p. 0 < p ==> (CARD (Zstar p).carrier = p-1)``,
  SRW_TAC [][Zstar_def, CARD_RESIDUE]);

(* Theorem: (Zstar p) is a Group. *)
val Zstar_group = store_thm(
  "Zstar_group",
  ``!p. prime p ==> Group (Zstar p)``,
  SRW_TAC [][Zstar_def, residue_def, Group_def, RES_FORALL_THM, MUL_INV_DEF]
  THENL [ (* 4 subgoals *)
    SRW_TAC [][dividesTheory.ONE_LT_PRIME],
    `(x*y) MOD p <> 0` by SRW_TAC [][EUCLID_LEMMA] THEN RW_TAC arith_ss [],
    RW_TAC arith_ss [],
    METIS_TAC [dividesTheory.PRIME_POS, MOD_MULT_ASSOC]
  ] );

(* Theorem: group_exp (Zstar p) a n = a**n MOD p *)
val Zstar_exp = store_thm(
  "Zstar_exp",
  ``!p a. prime p /\ a IN (Zstar p).carrier ==> !n. group_exp (Zstar p) a n = (a**n) MOD p``,
  REPEAT STRIP_TAC THEN
  `Group (Zstar p)` by SRW_TAC [][Zstar_group] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Induct_on `n` THEN1 (
    `(Zstar p).id = 1` by SRW_TAC [][Zstar_def] THEN
    `1 < p /\ (1 MOD p = 1)` by RW_TAC arith_ss [dividesTheory.ONE_LT_PRIME] THEN
    METIS_TAC [group_exp_def, arithmeticTheory.EXP]
  ) THEN
  SRW_TAC [][group_exp_def, arithmeticTheory.EXP] THEN
  FULL_SIMP_TAC (srw_ss())[Zstar_def, residue_def] THEN
  `a MOD p = a` by RW_TAC arith_ss [] THEN
  METIS_TAC [arithmeticTheory.MOD_TIMES2]);

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem (by Z*p multiplicative group)                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For prime p, 0 < a < p,  a**(p-1) = 1 (mod p) *)
val FERMAT_LITTLE = store_thm(
  "FERMAT_LITTLE",
  ``!p a. prime p /\ 0 < a /\ a < p ==> (a**(p-1) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `Group (Zstar p)` by SRW_TAC [][Zstar_group] THEN
  `FiniteGroup (Zstar p)` by SRW_TAC [][FiniteGroup_def, FINITE_Zstar_carrier] THEN
  `CARD (Zstar p).carrier = p-1` by METIS_TAC [dividesTheory.PRIME_POS, CARD_Zstar_carrier] THEN
  `a IN (Zstar p).carrier` by SRW_TAC [][Zstar_def, residue_def] THEN
  `(Zstar p).id = 1` by SRW_TAC [][Zstar_def] THEN
  METIS_TAC [FINITE_GROUP_FERMAT, Zstar_exp]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
