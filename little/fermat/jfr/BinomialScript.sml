(* ------------------------------------------------------------------------- *)
(* Binomial coefficients and Binomial Theorem.                               *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "Binomial";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
(* val _ = load "arithmeticTheory"; *)
open arithmeticTheory; (* for MOD and EXP *)
open listTheory; (* for SUM and GENLIST *)

(* val _ = load "dividesTheory"; *)
(* val _ = load "gcdTheory"; *)
open dividesTheory gcdTheory;

(* ------------------------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* ------------------------------------------------------------------------- *)
(* Binomial Coefficients.                                                    *)
(* ------------------------------------------------------------------------- *)

(* Define Binomials:
   C(n,0) = 1
   C(0,k) = 0 if k > 0
   C(n+1,k+1) = C(n,k) + C(n,k+1)
*)
val binomial_def = Define`
    (binomial 0 0  = 1) /\
    (binomial (SUC n) 0  = 1) /\
    (binomial 0 (SUC k) = 0)  /\
    (binomial (SUC n) (SUC k) = binomial n k + binomial n (SUC k))
`;

(* Basic properties *)

(* Theorem: C(n,0) = 1 *)
val binomial_n_0 = store_thm(
  "binomial_n_0",
  ``!n. binomial n 0 = 1``,
  METIS_TAC [binomial_def, num_CASES]);

(* Theorem: C(n,k) = 0 if n < k *)
val binomial_less_0 = store_thm(
  "binomial_less_0",
  ``!n k. n < k ==> (binomial n k = 0)``,
  Induct_on `n` THEN1
  METIS_TAC [binomial_def, num_CASES, NOT_ZERO_LT_ZERO] THEN
  SRW_TAC [][binomial_def] THEN
  `?h. k = SUC h` by METIS_TAC [SUC_NOT, NOT_ZERO_LT_ZERO, LESS_EQ_SUC, LESS_TRANS] THEN
  METIS_TAC [binomial_def, LESS_MONO_EQ, LESS_TRANS, LESS_SUC, ADD_0]);

(* Theorem: C(n,n) = 1 *)
val binomial_n_n = store_thm(
  "binomial_n_n",
  ``!n. binomial n n = 1``,
  Induct_on `n` THEN1
  METIS_TAC [binomial_def] THEN
  METIS_TAC [binomial_def, LESS_SUC, binomial_less_0, ADD_0]);

(* Theorem: C(n+1,k+1) = C(n,k) + C(n,k+1) *)
val binomial_recurrence = store_thm(
  "binomial_recurrence",
  ``!n k. binomial (SUC n) (SUC k) = binomial n k + binomial n (SUC k)``,
  SRW_TAC[][binomial_def]);

(* Theorem: C(n+k,k) = (n+k)!/n!k!  *)
val binomial_formula = store_thm(
  "binomial_formula",
  ``!n k. binomial (n+k) k * (FACT n * FACT k) = FACT (n+k)``,
  Induct_on `k` THEN1
  METIS_TAC [binomial_n_0, FACT, MULT_CLAUSES, ADD_0] THEN
  Induct_on `n` THEN1
  METIS_TAC [binomial_n_n, FACT, MULT_CLAUSES, ADD_CLAUSES] THEN
  `SUC n + SUC k = SUC (SUC (n+k))` by DECIDE_TAC THEN
  `SUC (n + k) = SUC n + k` by DECIDE_TAC THEN
  `binomial (SUC n + SUC k) (SUC k) * (FACT (SUC n) * FACT (SUC k)) =
    (binomial (SUC (n + k)) k +
     binomial (SUC (n + k)) (SUC k)) * (FACT (SUC n) * FACT (SUC k))`
    by METIS_TAC [binomial_recurrence] THEN
  `_ = binomial (SUC (n + k)) k * (FACT (SUC n) * FACT (SUC k)) +
        binomial (SUC (n + k)) (SUC k) * (FACT (SUC n) * FACT (SUC k))`
        by METIS_TAC [RIGHT_ADD_DISTRIB] THEN
  `_ = binomial (SUC n + k) k * (FACT (SUC n) * ((SUC k) * FACT k)) +
        binomial (n + SUC k) (SUC k) * ((SUC n) * FACT n * FACT (SUC k))`
        by METIS_TAC [ADD_COMM, SUC_ADD_SYM, FACT] THEN
  `_ = binomial (SUC n + k) k * FACT (SUC n) * FACT k * (SUC k) +
        binomial (n + SUC k) (SUC k) * FACT n * FACT (SUC k) * (SUC n)`
        by METIS_TAC [MULT_COMM, MULT_ASSOC] THEN
  `_ = FACT (SUC n + k) * SUC k + FACT (n + SUC k) * SUC n`
        by METIS_TAC [MULT_COMM, MULT_ASSOC] THEN
  `_ = FACT (SUC (n+k)) * SUC k + FACT (SUC (n+k)) * SUC n`
        by METIS_TAC [ADD_COMM, SUC_ADD_SYM] THEN
  `_ = FACT (SUC (n+k)) * (SUC k + SUC n)` by METIS_TAC [LEFT_ADD_DISTRIB] THEN
  `_ = (SUC n + SUC k) * FACT (SUC (n+k))` by METIS_TAC [MULT_COMM, ADD_COMM] THEN
  METIS_TAC [FACT]);

(* Theorem: C(n,k) = n!/k!(n-k)!  for 0 <= k <= n *)
val binomial_formula2 = store_thm(
  "binomial_formula2",
  ``!n k. k <= n ==> (FACT n = binomial n k * (FACT (n-k) * FACT k))``,
  METIS_TAC [binomial_formula, SUB_ADD]);

(* ------------------------------------------------------------------------- *)
(* List Summation.                                                           *)
(* ------------------------------------------------------------------------- *)

(* Defined: SUM for summation of list = sequence *)

(* Theorem: SUM [] = 0 *)
val SUM_NIL = save_thm("SUM_NIL", SUM |> CONJUNCT1);

(* Theorem: constant multiplication: k SUM S = SUM (k*S)  *)
val SUM_MULT = store_thm(
  "SUM_MULT",
  ``!s k. k * SUM s = SUM (MAP (\x. k*x) s)``,
  Induct_on `s` THEN1
  METIS_TAC [SUM, MAP, MULT_0] THEN
  METIS_TAC [SUM, MAP, LEFT_ADD_DISTRIB]);

(* Theorem: (m+n) * SUM s = SUM (m*s) + SUM (n*s)  *)
val SUM_RIGHT_ADD_DISTRIB = store_thm(
  "SUM_RIGHT_ADD_DISTRIB",
  ``!s m n. (m + n) * SUM s = SUM (MAP (\x. m*x) s) + SUM (MAP (\x. n*x) s)``,
  METIS_TAC [RIGHT_ADD_DISTRIB, SUM_MULT]);

(* Theorem: SUM (k=0..n) f(k) = f(0) + SUM (k=1..n) f(k)  *)
val SUM_DECOMPOSE_FIRST = store_thm(
  "SUM_DECOMPOSE_FIRST",
  ``!f n. SUM (GENLIST f (SUC n)) = f 0 + SUM (GENLIST (f o SUC) n)``,
  METIS_TAC [GENLIST_CONS, SUM]);

(* Theorem: SUM (k=0..n) f(k) = SUM (k=0..(n-1)) f(k) + f n *)
val SUM_DECOMPOSE_LAST = store_thm(
  "SUM_DECOMPOSE_LAST",
  ``!f n. SUM (GENLIST f (SUC n)) = SUM (GENLIST f n) + f n``,
  REPEAT STRIP_TAC THEN
  `SUM (GENLIST f (SUC n)) = SUM (SNOC (f n) (GENLIST f n))` by METIS_TAC [GENLIST] THEN
  `_ = SUM ((GENLIST f n) ++ [f n])` by METIS_TAC [SNOC_APPEND] THEN
  `_ = SUM (GENLIST f n) + SUM [f n]` by METIS_TAC [SUM_APPEND] THEN
  SRW_TAC [][SUM]);

(* Theorem: 0 < n ==> SUM (GENLIST f (SUC n)) = f 0 + SUM (GENLIST (f o SUC) (PRE n)) + f n *)
val SUM_DECOMPOSE_FIRST_LAST = store_thm(
  "SUM_DECOMPOSE_FIRST_LAST",
  ``!f n. 0 < n ==> (SUM (GENLIST f (SUC n)) = f 0 + SUM (GENLIST (f o SUC) (PRE n)) + f n)``,
  METIS_TAC [SUM_DECOMPOSE_LAST, SUM_DECOMPOSE_FIRST, LESS_EQ_SUC, PRE_SUC_EQ]);

(* Theorem: Binomial Index Shifting, for
     SUM (k=1..n) C(n,k)x^(n+1-k)y^k = SUM (k=0..n-1) C(n,k+1)x^(n-k)y^(k+1) *)
val GENLIST_binomial_index_shift = store_thm(
  "GENLIST_binomial_index_shift",
  ``!n x y. GENLIST ((\k. binomial n k * x ** SUC(n - k) * y ** k) o SUC) n =
            GENLIST (\k. binomial n (SUC k) * x**(n-k) * y**(SUC k)) n``,
  SRW_TAC [][GENLIST_FUN_EQ] THEN
  `SUC (n - SUC k) = n - k` by DECIDE_TAC THEN
  RW_TAC std_ss []);

(* This is closely related to above, with (SUC n) replacing (n),  but does not require k < n. *)
val binomial_index_shift = store_thm(
  "binomial_index_shift",
  ``!n x y. (\k. binomial (SUC n) k * x**((SUC n) - k) * y**k) o SUC =
           (\k. binomial (SUC n) (SUC k) * x**(n-k) * y**(SUC k))``,
  SRW_TAC [][FUN_EQ_THM]);

(* Theorem: SUM (GENLIST a n) + SUM (GENLIST b n) = SUM (GENLIST (\k. a k + b k) n) *)
val SUM_ADD_GENLIST = store_thm(
  "SUM_ADD_GENLIST",
  ``!a b n. SUM (GENLIST a n) + SUM (GENLIST b n) = SUM (GENLIST (\k. a k + b k) n)``,
  Induct_on `n` THEN1
  SRW_TAC [][] THEN
  SRW_TAC [][SUM_DECOMPOSE_LAST] THEN
  `SUM (GENLIST a n) + a n + (SUM (GENLIST b n) + b n) =
    SUM (GENLIST a n) + SUM (GENLIST b n) + (a n + b n)` by DECIDE_TAC THEN
  METIS_TAC []);

(* Theorem: SUM (GENLIST a n ++ GENLIST b n) = SUM (GENLIST (\k. a k + b k) n) *)
val SUM_GENLIST_APPEND = store_thm(
  "SUM_GENLIST_APPEND",
  ``!a b n. SUM (GENLIST a n ++ GENLIST b n) = SUM (GENLIST (\k. a k + b k) n)``,
  METIS_TAC [SUM_APPEND, SUM_ADD_GENLIST]);

(* Theorem: (SUM l) MOD n = (SUM (MAP (\x. x MOD n) l)) MOD n *)
val MOD_SUM = store_thm(
  "MOD_SUM",
  ``!n. 0 < n ==> !l. (SUM l) MOD n = (SUM (MAP (\x. x MOD n) l)) MOD n``,
  REPEAT STRIP_TAC THEN
  Induct_on `l` THEN1
  SRW_TAC [][] THEN
  REPEAT STRIP_TAC THEN
  `SUM (h::l) MOD n = (h MOD n + (SUM l) MOD n) MOD n` by RW_TAC std_ss [SUM, MOD_PLUS] THEN
  `_ = ((h MOD n) MOD n + SUM (MAP (\x. x MOD n) l) MOD n) MOD n` by RW_TAC std_ss [MOD_MOD] THEN
  SRW_TAC [][MOD_PLUS]);

(* Theorem: SUM l = 0 <=> l = EVERY (\x. x = 0) l *)
val SUM_EQ_0 = store_thm(
  "SUM_EQ_0",
  ``!l. (SUM l = 0) <=> EVERY (\x. x = 0) l``,
  Induct THEN
  SRW_TAC [][]);

(* Theorem: SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n =
            SUM (GENLIST ((\k. f k MOD n) o SUC) (PRE n)) MOD n *)
val SUM_GENLIST_MOD = store_thm(
  "SUM_GENLIST_MOD",
  ``!n. 0 < n ==> !f. SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n = SUM (GENLIST ((\k. f k MOD n) o SUC) (PRE n)) MOD n``,
  REPEAT STRIP_TAC THEN
  `SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n =
    SUM (MAP (\x. x MOD n) (GENLIST ((\k. f k) o SUC) (PRE n))) MOD n` by METIS_TAC [MOD_SUM] THEN
  RW_TAC std_ss [MAP_GENLIST, combinTheory.o_ASSOC, combinTheory.o_ABS_L]);

(* ------------------------------------------------------------------------- *)
(* Binomial Theorem                                                          *)
(* ------------------------------------------------------------------------- *)

(* Theorem: multiply x into a binomial term *)
val binomial_term_merge_x = store_thm(
  "binomial_term_merge_x",
  ``!n x y. (\k. x*k) o (\k. binomial n k * x ** (n - k) * y ** k) =
           (\k. binomial n k * x ** (SUC(n - k)) * y ** k)``,
  SRW_TAC [][FUN_EQ_THM] THEN
  `x * (binomial n k * x ** (n - k) * y ** k) =
    binomial n k * (x * x ** (n - k)) * y ** k` by DECIDE_TAC THEN
  METIS_TAC [EXP]);

(* Theorem: multiply y into a binomial term *)
val binomial_term_merge_y = store_thm(
  "binomial_term_merge_y",
  ``!n x y. (\k. y*k) o (\k. binomial n k * x ** (n - k) * y ** k) =
           (\k. binomial n k * x ** (n - k) * y ** (SUC k))``,
  SRW_TAC [][FUN_EQ_THM] THEN
  `y * (binomial n k * x ** (n - k) * y ** k) =
    binomial n k * x ** (n - k) * (y * y ** k)` by DECIDE_TAC THEN
  METIS_TAC [EXP]);

(* Theorem: [Binomial Theorem]  (x + y)^n = SUM (k=0..n) C(n,k)x^(n-k)y^k  *)
val binomial_thm = store_thm(
  "binomial_thm",
  ``!n x y. (x + y)**n = SUM (GENLIST (\k. (binomial n k)* x**(n-k) * y**k) (SUC n))``,
  Induct_on `n` THEN1
  SRW_TAC [][EXP, binomial_n_n] THEN
  SRW_TAC [][EXP] THEN
  `(x + y) * SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** k) (SUC n)) =
    x * SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** k) (SUC n)) +
    y * SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** k) (SUC n))`
    by METIS_TAC [RIGHT_ADD_DISTRIB] THEN
  `_ = SUM (GENLIST ((\k. x*k) o (\k. binomial n k * x ** (n - k) * y ** k)) (SUC n)) +
        SUM (GENLIST ((\k. y*k) o (\k. binomial n k * x ** (n - k) * y ** k)) (SUC n))`
    by METIS_TAC [SUM_MULT, MAP_GENLIST] THEN
  `_ = SUM (GENLIST (\k. binomial n k * x ** SUC(n - k) * y ** k) (SUC n)) +
        SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) (SUC n))`
    by SRW_TAC [][binomial_term_merge_x, binomial_term_merge_y] THEN
  `_ = (\k. binomial n k * x ** SUC (n - k) * y ** k) 0 +
         SUM (GENLIST ((\k. binomial n k * x ** SUC (n - k) * y ** k) o SUC) n) +
        SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) (SUC n))`
    by SRW_TAC [][SUM_DECOMPOSE_FIRST] THEN
  `_ = (\k. binomial n k * x ** SUC (n - k) * y ** k) 0 +
         SUM (GENLIST ((\k. binomial n k * x ** SUC (n - k) * y ** k) o SUC) n) +
        (SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n) +
         (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n )`
    by SRW_TAC [][SUM_DECOMPOSE_LAST] THEN
  `_ = (\k. binomial n k * x ** SUC(n - k) * y ** k) 0 +
         SUM (GENLIST (\k. binomial n (SUC k) * x ** (n - k) * y ** (SUC k)) n) +
        (SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n) +
         (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n )`
    by METIS_TAC [GENLIST_binomial_index_shift] THEN
  `_ = (\k. binomial n k * x ** SUC(n - k) * y ** k) 0 +
        (SUM (GENLIST (\k. binomial n (SUC k) * x ** (n - k) * y ** (SUC k)) n) +
         SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n)) +
         (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n`
    by DECIDE_TAC THEN
  `_ = (\k. binomial n k * x ** SUC (n - k) * y ** k) 0 +
        SUM (GENLIST (\k. (binomial n (SUC k) * x ** (n - k) * y ** (SUC k) +
                           binomial n k * x ** (n - k) * y ** (SUC k))) n) +
        (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n`
    by METIS_TAC [SUM_ADD_GENLIST] THEN
  `_ = (\k. binomial n k * x ** SUC(n - k) * y ** k) 0 +
        SUM (GENLIST (\k. (binomial n (SUC k) + binomial n k) * x ** (n - k) * y ** (SUC k)) n) +
        (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n`
    by SRW_TAC [][RIGHT_ADD_DISTRIB, MULT_ASSOC] THEN
  `_ = (\k. binomial n k * x ** SUC(n - k) * y ** k) 0 +
        SUM (GENLIST (\k. binomial (SUC n) (SUC k) * x ** (n - k) * y ** (SUC k)) n) +
        (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n`
    by SRW_TAC [][binomial_recurrence, ADD_COMM] THEN
  `_ = binomial (SUC n) 0 * x ** (SUC n) * y ** 0 +
        SUM (GENLIST (\k. binomial (SUC n) (SUC k) * x ** (n - k) * y ** (SUC k)) n) +
        binomial (SUC n) (SUC n) * x ** 0 * y ** (SUC n)`
        by SRW_TAC [][binomial_n_0, binomial_n_n] THEN
  `_ = binomial (SUC n) 0 * x ** (SUC n) * y ** 0 +
        SUM (GENLIST ((\k. binomial (SUC n) k * x ** ((SUC n) - k) * y ** k) o SUC) n) +
        binomial (SUC n) (SUC n) * x ** 0 * y ** (SUC n)`
        by SRW_TAC [][binomial_index_shift] THEN
  `_ = SUM (GENLIST (\k. binomial (SUC n) k * x ** (SUC n - k) * y ** k) (SUC n)) +
        (\k. binomial (SUC n) k * x ** (SUC n - k) * y ** k) (SUC n)`
        by SRW_TAC [][SUM_DECOMPOSE_FIRST] THEN
  `_ = SUM (GENLIST (\k. binomial (SUC n) k * x ** (SUC n - k) * y ** k) (SUC (SUC n)))`
        by SRW_TAC [][SUM_DECOMPOSE_LAST] THEN
  DECIDE_TAC);

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
