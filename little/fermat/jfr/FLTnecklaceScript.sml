(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Combinatorial proof.                            *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by J. Petersen in 1872:

Take p elements from q with repetitions in all ways, that is, in q^p ways.
The q sets with elements all alike are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted in sets of p. Hence p divides q^p - q.

This is a combinatorial proof without reference to Group Theory.

The strategy:
* Define necklace and prove basic properties (as necklaceTheory)
* Develop a theory of cycles for necklaces (as cycleTheory)
* Apply cycles with necklaces to derive Fermat's Little Theorem.

The key idea:
* Cycle-Order Theorem: For a finite list, its cycle order divides its length.
* Equal-Paritition Theorem: For a finite partition of a set into equal parts,
     size of part divides size of set.

Therefore it is only necessary to establish the following to arrive at Fermat's Little Theorem:
* CARD (multicoloured p a) = a**p - a                        from necklaceTheory
* (\l. cycle n l) is an equivalence relation in (multicoloured p a)   -- real work
* !l. l IN (multicoloured p a) ==> (order l) = p             by Cycle-Order Theorem
* Hence p divides (a**p - a)                                 by Equal-Partition Theorem

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTnecklace";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* val _ = load "SimpleTheory"; *)
open SimpleTheory;

(* Part 2: General Theory -------------------------------------------------- *)

(* val _ = load "NecklaceTheory"; *)
open NecklaceTheory;

(* val _ = load "CycleTheory"; *)
open CycleTheory;

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof without Group Theory explicitly.                      *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Similarity in Cycles (Theory of Patterns)                                 *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Two lists are similar if they are related through cycle.                  *)
(* ------------------------------------------------------------------------- *)

(* Define similar to relate two lists *)
val similar_def = Define`
  similar l1 l2 = ?n. l2 = cycle n l1
`;

(* set infix and overload for similar *)
val _ = set_fixity "==" (Infixl 480);
val _ = overload_on ("==", ``similar``);

(* Theorem: Only NIL can be similar to NIL. *)
val SIMILAR_NIL = store_thm(
  "SIMILAR_NIL",
  ``!l. l == [] \/ [] == l <=> (l = [])``,
  METIS_TAC [similar_def, CYCLE_NIL]);

(* ------------------------------------------------------------------------- *)
(* Similar is an equivalence relation.                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Reflexive] l == l *)
val SIMILAR_REFL = store_thm(
  "SIMILAR_REFL",
  ``!l. l == l``,
  METIS_TAC [similar_def, CYCLE_0]);

(* Theorem: [Symmetric] l1 == l2 ==> l2 == l1 *)
val SIMILAR_SYM = store_thm(
  "SIMILAR_SYM",
  ``!l1 l2. (l1 == l2) ==> (l2 == l1)``,
  SRW_TAC [][similar_def] THEN
  Cases_on `l1 = []` THEN1
  METIS_TAC [CYCLE_NIL] THEN
  `n MOD LENGTH l1 <= LENGTH l1`
    by METIS_TAC [LENGTH_NON_NIL, arithmeticTheory.MOD_LESS, arithmeticTheory.LESS_IMP_LESS_OR_EQ] THEN
  Q.EXISTS_TAC `LENGTH l1 - (n MOD LENGTH l1)` THEN
  METIS_TAC [CYCLE_MOD_LENGTH, CYCLE_INV]);

(* Theorem: [Transitive] l1 == l2 /\ l2 == l3 ==> l1 == l3 *)
val SIMILAR_TRANS = store_thm(
  "SIMILAR_TRANS",
  ``!l1 l2 l3. (l1 == l2) /\ (l2 == l3) ==> (l1 == l3)``,
  METIS_TAC [similar_def, CYCLE_ADD]);

(* ------------------------------------------------------------------------- *)
(* Similar associates and their cardinality.                                 *)
(* ------------------------------------------------------------------------- *)

(* Define the associate of list: all those that are similar to the list. *)
val associate_def = Define `
  associate x = {y | x == y }
`;

(* Theorem: associate NIL is singleton {NIL}. *)
val ASSOCIATE_NIL = store_thm(
  "ASSOCIATE_NIL",
  ``associate [] = {[]}``,
  SRW_TAC [][associate_def, EXTENSION] THEN METIS_TAC [SIMILAR_NIL]);

(* ------------------------------------------------------------------------- *)
(* To show: associates are FINITE.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The map (\n. cycle n l) from (count LENGTH l) -> (associate l) is surjective. *)
val ASSOCIATE_AS_IMAGE = store_thm(
  "ASSOCIATE_AS_IMAGE",
  ``!l. l <> [] ==> (associate l = IMAGE (\n. cycle n l) (count (LENGTH l)))``,
  SRW_TAC [][associate_def, similar_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [CYCLE_MOD_LENGTH, LENGTH_NIL,
             arithmeticTheory.MOD_LESS, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Theorem: associate l is FINITE *)
val FINITE_ASSOCIATE = store_thm(
  "FINITE_ASSOCIATE",
  ``!l. FINITE (associate l)``,
  STRIP_TAC THEN Cases_on `l` THEN
  METIS_TAC [ASSOCIATE_NIL, FINITE_SING,
             ASSOCIATE_AS_IMAGE, FINITE_COUNT, IMAGE_FINITE]);

(* ------------------------------------------------------------------------- *)
(* Apply to Necklaces: Relate cycle/similar to multicoloured necklaces.      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If l is a necklace, cycle n l is also a necklace. *)
val NECKLACE_CYCLE = store_thm(
  "NECKLACE_CYCLE",
  ``!l. l IN necklace n a ==> !k. (cycle k l) IN necklace n a``,
  SRW_TAC [][necklace_def, CYCLE_SAME_LENGTH, CYCLE_SAME_SET]);

(* Theorem: [Closure of cycle for multicoloured necklaces]
            If l is a multicoloured necklace, cycle n l is also multicolored. *)
val multicoloured_CYCLE = store_thm(
  "multicoloured_CYCLE",
  ``!l n a. l IN multicoloured n a ==> !k. (cycle k l) IN multicoloured n a``,
  REPEAT STRIP_TAC THEN
  `l IN (necklace n a)` by SRW_TAC [][multicoloured_necklace] THEN
  Cases_on `n=0` THEN1 METIS_TAC [multicoloured_0, MEMBER_NOT_EMPTY] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  Cases_on `a=0` THEN1 METIS_TAC [multicoloured_EMPTY, MEMBER_NOT_EMPTY] THEN
  `0 < a` by RW_TAC arith_ss [] THEN
  `l <> []` by METIS_TAC [necklace_not_nil] THEN
  FULL_SIMP_TAC (srw_ss()) [multicoloured_def, monocoloured_def] THEN
  METIS_TAC [NECKLACE_CYCLE, CYCLE_SAME_SET, LENGTH_NIL, CYCLE_SAME_LENGTH]);

(* Theorem: [Closure of similar for multicoloured necklaces]
            Only multicoloured necklaces can be similar to each other. *)
val multicoloured_SIMILAR = store_thm(
  "multicoloured_SIMILAR",
  ``!n a x y. x IN multicoloured n a /\ (x == y) ==> y IN multicoloured n a``,
  METIS_TAC [similar_def, multicoloured_CYCLE]);

(* ------------------------------------------------------------------------- *)
(* Order of Necklaces.                                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A monocoloured necklace l has cycle 1 l = l *)
val monocoloured_cycle1 = store_thm(
  "monocoloured_cycle1",
  ``!n a l. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> (cycle 1 l = l)``,
  SRW_TAC [][monocoloured_def] THEN
  `l <> []` by METIS_TAC [necklace_not_nil] THEN
  METIS_TAC [SING_HAS_CYCLE_1]);

(* Theorem: If a necklace l has cycle 1 l = l , it must be monocoloured *)
val cycle1_monocoloured = store_thm(
  "cycle1_monocoloured",
  ``!l n a. 0 < a /\ l IN necklace n a /\ (cycle 1 l = l) ==> l IN monocoloured n a``,
  SRW_TAC [][monocoloured_def, necklace_def] THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* Theorem: A necklace l is monocoloured iff cycle 1 l = 1 *)
val monocoloured_iff_cycle1 = store_thm(
  "monocoloured_iff_cycle1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN necklace n a ==>
     (l IN monocoloured n a <=> (cycle 1 l = l))``,
  METIS_TAC [monocoloured_necklace, cycle1_monocoloured, monocoloured_cycle1]);

(* ------------------------------------------------------------------------- *)
(* To show: cycle induces is an equivalence relation.      [milestone]       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Similarity is an equivalence relation on multicoloured necklaces. *)
val SIMILAR_EQUIV_ON_multicoloured = store_thm(
  "SIMILAR_EQUIV_ON_multicoloured",
  ``!n a. similar equiv_on multicoloured n a``,
  SRW_TAC [][equiv_on_def] THEN
  METIS_TAC [SIMILAR_REFL, SIMILAR_SYM, SIMILAR_TRANS]);

(* ------------------------------------------------------------------------- *)
(* Size of associate for multicoloured necklaces of length prime.            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For multicoloured l, prime (LENGTH l), the map (count (LENGTH l) to (associate l) is injective *)
(* Proof:
   Essentially this is to show:
   Let p = LENGTH l,
   For all x < p, y < p,  cycle x l = cycle y l ==> x = y.
   Suppose not,
   then there are x, y such that x <> y, and cycle x l = cycle y l.
   Let l' = cycle x l. Assume x < y, then y = d + x, d = difference, and 0 < d < p.
   cycle d l' = cycle d (cycle x l) = cycle (d+x) l = cycle y l = l'
   but cycle p l' = l'   by CYCLE_BACK,
   so cycle (gcd d p) l' = l', by CYCLE_GCD_BACK.
   For prime p, 0 < d < p, gcd d p = 1. This contradicts monocoloured_iff_cycle1.
   Similar for x > y.
*)
val prime_multicoloured_associate_injection = store_thm(
  "prime_multicoloured_associate_injection",
  ``!l p a. prime p /\ l IN multicoloured p a ==> INJ (\n. cycle n l) (count p) (associate l)``,
  SRW_TAC [][associate_def, INJ_DEF] THEN1
  METIS_TAC [similar_def] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [multicoloured_EMPTY, MEMBER_NOT_EMPTY] THEN
  `0 < a` by DECIDE_TAC THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `?l'. cycle n l = l'` by SRW_TAC [][] THEN
  `l' IN multicoloured p a /\ (LENGTH l' = LENGTH l)` by METIS_TAC [multicoloured_CYCLE, CYCLE_SAME_LENGTH] THEN
  `cycle p l' = l'` by METIS_TAC [CYCLE_BACK, multicoloured_necklace, necklace_property] THEN
  Cases_on `n < n'` THENL [
    `?d. n' = d + n` by METIS_TAC [arithmeticTheory.LESS_ADD] THEN
    `0 < d /\ d < p` by RW_TAC arith_ss [] THEN
    `cycle d l' = l'` by METIS_TAC [CYCLE_ADD] THEN
    `cycle (gcd d p) l' = l'` by METIS_TAC [CYCLE_GCD_BACK] THEN
    `gcd d p = 1` by METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD, gcdTheory.GCD_SYM] THEN
    METIS_TAC [monocoloured_iff_cycle1, multicoloured_necklace, multicoloured_not_monocoloured],
    `n' < n` by DECIDE_TAC THEN
    `?d. n = d + n'` by METIS_TAC [arithmeticTheory.LESS_ADD] THEN
    `0 < d /\ d < p` by RW_TAC arith_ss [] THEN
    `cycle d l' = l'` by METIS_TAC [CYCLE_ADD] THEN
    `cycle (gcd d p) l' = l'` by METIS_TAC [CYCLE_GCD_BACK] THEN
    `gcd d p = 1` by METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD, gcdTheory.GCD_SYM] THEN
    METIS_TAC [monocoloured_iff_cycle1, multicoloured_necklace, multicoloured_not_monocoloured]
  ]);

(* Theorem: For multicoloured l, the map (count (LENGTH l) to (associate l) is surjective *)
(* Proof:
   Essentially this is to show:
   l == x ==> ?k. k < (LENGTH l) /\ (cycle k l = x),
   l == x ==> ?h. cycle h l = x.
   Take k = h MOD (LENGTH l), then apply CYCLE_MOD_LENGTH.
*)
val multicoloured_associate_surjection = store_thm(
  "multicoloured_associate_surjection",
  ``!l n a. l IN multicoloured n a ==> SURJ (\k. cycle k l) (count n) (associate l)``,
  SRW_TAC [][associate_def, SURJ_DEF] THEN1
  METIS_TAC [similar_def] THEN
  Cases_on `n = 0` THEN1
  METIS_TAC [multicoloured_0, MEMBER_NOT_EMPTY] THEN
  `0 < n` by DECIDE_TAC THEN
  `LENGTH l = n` by METIS_TAC [multicoloured_necklace, necklace_property] THEN
  `l <> []` by METIS_TAC [LENGTH_NIL, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [similar_def, CYCLE_MOD_LENGTH, arithmeticTheory.MOD_LESS]);

(* Theorem: For multicoloured l, prime (LENGTH l), the map (count (LENGTH l) to (associate l) is bijective *)
(* Proof: from injection and surjection. *)
val prime_multicoloured_associate_bijection = store_thm(
  "prime_multicoloured_associate_bijection",
  ``!l p a. prime p /\ l IN multicoloured p a ==> BIJ (\n. cycle n l) (count p) (associate l)``,
  METIS_TAC [BIJ_DEF, prime_multicoloured_associate_injection, multicoloured_associate_surjection]);

(* Theorem: For multicoloured l, prime (LENGTH l), size of (associate l) = (LENGTH l) *)
(* Proof: by the bijection between (associate l) and (count (LENGTH l)) when (LENGTH l) is prime. *)
val card_prime_multicoloured_associate = store_thm(
  "card_prime_multicoloured_associate",
  ``!l p a. prime p /\ l IN multicoloured p a ==> (CARD (associate l) = p)``,
  METIS_TAC [prime_multicoloured_associate_bijection,
             FINITE_BIJ_CARD_EQ, FINITE_ASSOCIATE, FINITE_COUNT, CARD_COUNT]);

(* Theorem: For 0 < a < p, and prime p, p divids (a^p - a) *)
(* Proof:
   Since p is prime, 0 < p.
   When 0 < p, CARD (multicoloured p a) = a ** p - a   by CARD_multicoloured.
   For all l IN multicoloured p a, LENGTH l = p        by necklace_property
   CARD (associate l) = p                              by card_prime_multicoloured_associate
   But $== equiv_on multicoloured n a                  by SIMILAR_EQUIV_ON_multicoloured
   and (associate l) IN partition $== (multicoloured p a)   by partition_def
   hence CARD (multicoloured p a) = p * CARD (partition $== (multicoloured p a))  by equal_partition_CARD
   Thus   p divides (a**p - a)                         by divides_def;
*)
val FERMAT_LITTLE_GCD = store_thm(
  "FERMAT_LITTLE_GCD",
  ``!p a. prime p ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `a**p - a = CARD (multicoloured p a)` by METIS_TAC [CARD_multicoloured] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [arithmeticTheory.EXP_EQ_0, arithmeticTheory.SUB_0, dividesTheory.ALL_DIVIDES_0] THEN
  `0 < a` by DECIDE_TAC THEN
  `!l. l IN (multicoloured p a) ==> (associate l) IN partition $== (multicoloured p a)`
     by SRW_TAC [][partition_def, associate_def, SIMILAR_EQUIV_ON_multicoloured] THENL [
    Q.EXISTS_TAC `l` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    METIS_TAC [multicoloured_SIMILAR],
    `!l. l IN (multicoloured p a) ==> (CARD (associate l) = p)` by METIS_TAC [card_prime_multicoloured_associate] THEN
    `!t. t IN (partition $== (multicoloured p a)) ==> ? l. l IN (multicoloured p a) /\ (t = associate l)`
     by SRW_TAC [][partition_def, associate_def, SIMILAR_EQUIV_ON_multicoloured] THENL [
      Q.EXISTS_TAC `x` THEN
      SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
      METIS_TAC [multicoloured_SIMILAR],
      `!t. t IN (partition $== (multicoloured p a)) ==> (CARD t = p)` by METIS_TAC [] THEN
      `CARD (multicoloured p a) = p * CARD (partition $== (multicoloured p a))`
   by METIS_TAC [SIMILAR_EQUIV_ON_multicoloured, FINITE_multicoloured, equal_partition_CARD] THEN
      METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_COMM]
    ]
  ]);

(* Part 4: End ------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
