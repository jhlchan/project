(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Number-theoretic Proof.                         *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Number Theory)
=======================================
Equivalent statements of Feramt's Little Theorem:
#0. For prime p, 0 < a < p, a**(p-1) = 1 MOD p.
#1. For prime p, 0 < a < p, a**p = a MOD p.
#2. For prime p, 0 < a < p, (a**p - a) = 0 MOD p.
#3. For prime p, 0 < a < p, p divides (a**p - a).
#4. For prime p, 0 < a < p, 0 < (a**(p-2) MOD p) < p and
                           ((a**(p-2) MOD p) * a) MOD p = 1.

The first one (#0) will be called Fermat's Identity.

There is a number-theoretic proof without reference to Group Theory.

Number-theoretic Proof:

For a prime p, consider the set of residue p = { i | 0 < i < p }.

This set is non-empty since prime p > 1.

For a fixed a in residue p, multiply each element in the above set
by a and take MOD p, i.e. form this coset { (a*i) MOD p | 0 < i < p }.

These two sets are the same because:
(1) 0 < a < p implies 0 < (a*i) MOD p < p.
    The crucial part is to show:
    a MOD p = a <> 0 and i MOD p = i <> 0 implies (a*i) MOD p <> 0,
    but this is the contrapositive of Euclid's Lemma:
      If prime p divides a*b, then (p divides a) or (p divides b).
(2) If (a*i) MOD p = (a*j) MOD p, then i = j.
    This is due to left-cancellation for MOD p when p is prime,
    again given by Euclid Lemma:
        (a*i) MOD p = (a*j) MOD
    ==> (a*i - a*j) MOD p = 0
    ==> (a*(i - j)) MOD p = 0
    ==>     (i - j) MOD p = 0          since a MOD p <> 0
    ==>           i MOD p = j MOD p    assume i > j, otherwise switch i, j.
    ==>                 i = j          as 0 < i < p, 0 < j < p.

Hence for prime p, and any a in (residue p),

  (residue p) = IMAGE (row p a) (residue p)   where row p a x = (a*x) MOD p.

Take the product of elements in each set, MOD p, and they are equal:

  PROD_SET (residue p) = PROD_SET (IMAGE (row p a) (residue p))

Computation by induction gives:

  PROD_SET (residue p) = (FACT (p-1)) MOD p,  and
  PROD_SET (IMAGE (row p a) (residue p)) = (a**(p-1) * FACT (p-1)) MOD p

Hence   (FACT (p-1) * a**(p-1)) MOD p = (FACT (p-1)) MOD p

Since a prime p cannot divide FACT (p-1), cancellation law applies, giving:

         a**(p-1) MOD p = 1, which is Fermat's Identity.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTnumber";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory;

(* load existing theories *)
(* val _ = load "gcdTheory"; *)

(* Get dependent theories in lib *)
open helperNumTheory helperSetTheory;

(* ------------------------------------------------------------------------- *)
(* Number-theoretic Proof without Group Theory.                              *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Mapping the residues to the row of residues.                            *)
(* ------------------------------------------------------------------------- *)

(* Define the row function: producing a row of the Z*n multiplication table *)
val row_def = Define `row n a x = (a*x) MOD n`;

(* Theorem: For prime p, a in (residue p) /\ (n <= p) ==>
                (row p a)(n) not IN IMAGE (row p a) (residue n) *)
val RESIDUE_PRIME_NOTIN_IMAGE = prove(
  ``!p a n. prime p /\ a IN (residue p) /\ n <= p ==>
            (row p a)(n) NOTIN IMAGE (row p a) (residue n)``,
  REPEAT STRIP_TAC THEN
  `!x. x IN (residue n) ==> (a*n) MOD p <> (a*x) MOD p`
    by SRW_TAC [][RESIDUE_PRIME_NEQ] THEN
  FULL_SIMP_TAC (srw_ss())[row_def] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)
(* More PROD_SET properties, especially with ROW.                          *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Inductive step of PROD_SET_IMAGE_ROW
   For prime p, a in (residue p), 0 < n <= p,
     PROD_SET (IMAGE (row p a) (residue (SUC n))) =
     ((a*n) MOD p)* PROD_SET (IMAGE (row p a) (residue n)) *)
(* Proof:
     PROD_SET (IMAGE (row p a) (residue (SUC n)))
   = PROD_SET (IMAGE (row p a) (n INSERT residue n))          by RESIDUE_INSERT, and 0 < n
   = ((row p a)(n)) * PROD_SET (IMAGE (row p a) (residue n))  by PROD_SET_IMAGE_REDUCTION
      assuming          FINITE (IMAGE (row p a) (residue n))  by IMAGE_FINITE and FINITE_RESIDUE
      and ((row p a)(n)) NOTIN (IMAGE (row p a) (residue n))  by RESIDUE_PRIME_NOTIN_IMAGE
   = ((a*n) MOD p) * PROD_SET (IMAGE (row p a) (residue n))   by row_def
*)
val PROD_SET_IMAGE_ROW_REDUCTION = store_thm(
  "PROD_SET_IMAGE_ROW_REDUCTION",
  ``!p n a. prime p /\ a IN (residue p) /\ 0 < n /\ n <= p ==>
   (PROD_SET (IMAGE (row p a) (residue (SUC n))) =
       ((a*n) MOD p)* PROD_SET (IMAGE (row p a) (residue n)))``,
  REPEAT STRIP_TAC THEN
  `(row p a)(n) NOTIN IMAGE (row p a) (residue n)`
    by SRW_TAC [][RESIDUE_PRIME_NOTIN_IMAGE] THEN
  `FINITE (IMAGE (row p a) (residue n))`
    by SRW_TAC [][FINITE_RESIDUE, pred_setTheory.IMAGE_FINITE] THEN
  `PROD_SET (IMAGE (row p a) (residue (SUC n))) =
    PROD_SET (IMAGE (row p a) (n INSERT residue n))`
      by SRW_TAC [][RESIDUE_INSERT] THEN
  `_ = ((row p a)(n)) * PROD_SET (IMAGE (row p a) (residue n))`
     by SRW_TAC [][PROD_SET_IMAGE_REDUCTION] THEN
  METIS_TAC [row_def]);

(* Theorem: For prime p, and a in (residue p),
            PROD_SET IMAGE (row p a) (residue p) = (a**(p-1) * FACT (p-1)) MOD p *)
(* Proof:
   Observe the pattern when p = 5, a = 3,

     PROD_SET IMAGE (row 5 3) (residue 5)
   = PROD_SET IMAGE (row 5 3) {x | 0 < x < 5 }
   = PROD_SET {row 5 3 x | 0 < x < 5 }
   = PROD_SET {(3*x) MOD 5 | 0 < x < 5 }
   = ((3*4) MOD 5) * PROD_SET {(3*x) MOD 5 | 0 < x < 4 }
   = ((3*4) MOD 5) * ((3*3) MOD 5) * PROD_SET {(3*x) MOD 5 | 0 < x < 3 }
   = ((3*4) MOD 5) * ((3*3) MOD 5) * ((3*2) MOD 5) * PROD_SET {(3*x) MOD 5 | 0 < x < 2 }
   = ((3*4) MOD 5) * ((3*3) MOD 5) * ((3*2) MOD 5) * ((3*1) MOD 5) * PROD_SET {(3*x) MOD 5 | 0 < x < 1 }
   = ((3*4) MOD 5) * ((3*3) MOD 5) * ((3*2) MOD 5) * ((3*1) MOD 5) * PROD_SET EMPTY
   = ((3*4) MOD 5) * ((3*3) MOD 5) * ((3*2) MOD 5) * ((3*1) MOD 5) * 1
   = ((3*4) MOD 5) * ((3*3) MOD 5) * ((3*2) MOD 5) * ((3*1) MOD 5) * (1 MOD 5)
   = ((3*4) * (3*3) * (3*2) * (3*1)) MOD 5
   = ((3*3*3*3) * (4*3*2*1)) MOD 5
   = ((3^4) * FACT 4) MOD 5

   Note that MOD p is not changing, but residue n is changing with n <= p.
   So change this theorem to:

   !n. PROD_SET IMAGE (row p a) (residue n) = (a**(n-1) * FACT (n-1)) MOD p

   and apply induciton on n.
*)
val PROD_SET_IMAGE_ROW = store_thm(
  "PROD_SET_IMAGE_ROW",
  ``!p n a. prime p /\ a IN (residue p) /\ 0 < n /\ n <= p ==>
   (PROD_SET (IMAGE (row p a) (residue n)) MOD p = (a**(n-1) * FACT (n-1)) MOD p)``,
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n = 0` THEN1
    RW_TAC arith_ss [RESIDUE_1_EMPTY, pred_setTheory.IMAGE_EMPTY, PROD_SET_EMPTY,
                     arithmeticTheory.FACT, dividesTheory.ONE_LT_PRIME] THEN
  `0 < n /\ n <= p /\ 0 < p` by RW_TAC arith_ss [] THEN
  `(PROD_SET (IMAGE (row p a) (residue (SUC n)))) MOD p =
      (((a*n) MOD p)* PROD_SET (IMAGE (row p a) (residue n))) MOD p`
       by SRW_TAC [][PROD_SET_IMAGE_ROW_REDUCTION] THEN
  `_ = ((a*n) * PROD_SET (IMAGE (row p a) (residue n))) MOD p`
    by SRW_TAC [][MOD_TIMES_TWO] THEN
  `_ = ((a*n) MOD p * (PROD_SET (IMAGE (row p a) (residue n))) MOD p) MOD p`
    by SRW_TAC [][arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((a*n) MOD p * ((a ** (n - 1) * FACT (n - 1))) MOD p) MOD p`
    by SRW_TAC [][] THEN
    (* apply inductive hypothesis *)
  `_ = ((a*n) * (a ** (n - 1) * FACT (n - 1))) MOD p`
    by SRW_TAC [][arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((a* (a ** (n-1))) * (n * FACT (n-1))) MOD p` by RW_TAC arith_ss [] THEN
  `_ = (a**(SUC (n-1)) * (n * FACT (n-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.EXP] THEN
  `_ = ((a**n) * (SUC(n-1) * FACT (n-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.ADD1] THEN
  `_ = (a**n * FACT (SUC(n-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.FACT] THEN
  RW_TAC arith_ss [arithmeticTheory.ADD1]);

(* The above version with n = p *)
val PROD_SET_IMAGE_ROW_ALL = store_thm(
  "PROD_SET_IMAGE_ROW_ALL",
  ``!p a. prime p /\ a IN (residue p) ==>
   (PROD_SET (IMAGE (row p a) (residue p)) MOD p = (a**(p-1) * FACT (p-1)) MOD p)``,
  SRW_TAC [][PROD_SET_IMAGE_ROW, dividesTheory.PRIME_POS]);

(* ------------------------------------------------------------------------- *)
(* Fermat's Identity                                                         *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Let Z* = {n | 0 < n /\ n < p} with prime p, i.e. residue p.
            !a in Z*, the function f: Z* -> Z* = x -> a*x MOD p is an injection,
   or
   For prime p and a in residue p,
   the map  (row p a): (residue p) -> (residue p) is an injection. *)
(* Proof:
   To show an injective map:
   (1) 0 < a < p implies 0 < (a*i) MOD p < p.
       The crucial part is to show: a MOD p = a <> 0 and i MOD p = i <> 0 implies (a*i) MOD p <> 0,
       which is the contrapositive of Euclid's Lemma.
   (2) If (a*x) MOD p = (a*x) MOD p, then x = y.
       This is due to left-cancellation for MOD p when p is prime,
       which also comes from Euclid Lemma.
*)
val RESIDUE_PRIME_ROW_INJ = prove(
  ``!p a. prime p /\ a IN (residue p) ==> INJ (row p a) (residue p) (residue p)``,
  SRW_TAC [][residue_def] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `a MOD p <> 0` by RW_TAC arith_ss [] THEN
  SRW_TAC [][pred_setTheory.INJ_DEF, row_def] THEN1 (
    `x MOD p <> 0` by RW_TAC arith_ss [] THEN
    `(a*x) MOD p <> 0` by METIS_TAC [EUCLID_LEMMA] THEN
    RW_TAC arith_ss []) THEN
  `x MOD p = y MOD p` by METIS_TAC [MOD_MULT_LCANCEL] THEN
  METIS_TAC [arithmeticTheory.LESS_MOD]);

(* Theorem: For prime p, residue p = IMAGE (row p a) (residue p) *)
(* Proof:
   This is due to the map: (row p a): (residue p) -> (residue p)
   is a self-injective map, and (residue p) is FINITE, see FINITE_INJ_IMAGE_EQ.
*)
val RESIDUE_PRIME_ROW = store_thm(
  "RESIDUE_PRIME_ROW",
  ``!p a. prime p /\ (a IN residue p) ==> (residue p = IMAGE (row p a) (residue p))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `FINITE (residue p)` by RW_TAC std_ss [FINITE_RESIDUE] THEN
  `INJ (row p a) (residue p) (residue p)` by RW_TAC std_ss [RESIDUE_PRIME_ROW_INJ] THEN
  METIS_TAC [FINITE_INJ_IMAGE_EQ]);

(* Theorem: Identity for residue modulo prime:
   For prime p, FACT (p-1) MOD p = (a**(p-1) * (FACT (p-1)) MOD p *)
(* Proof:
     FACT (p-1) MOD p
   = PROD_SET (residue p) MOD p                       by PROD_SET_RESIDUE
   = PROD_SET (IMAGE (Coset p a) (residue p)) MOD p   by RESIDUE_PRIME_COSET
   = (a**(p-1) * FACT (p-1)) MOD p                    by PROD_SET_IMAGE_COSET
*)
val RESIDUE_PRIME_IDENTITY = store_thm(
  "RESIDUE_PRIME_IDENTITY",
  ``!p a. prime p /\ a IN (residue p) ==> (FACT (p-1) MOD p = (a**(p-1) * FACT (p-1)) MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `FACT (p-1) MOD p = PROD_SET (residue p) MOD p` by SRW_TAC [][PROD_SET_RESIDUE] THEN
  `_ = PROD_SET (IMAGE (row p a) (residue p)) MOD p` by METIS_TAC [RESIDUE_PRIME_ROW] THEN
  METIS_TAC [PROD_SET_IMAGE_ROW_ALL]);

(* Theorem: Fermat's Identity: For prime p, a**(p-1) MOD p = 1 *)
(* Proof:
     FACT (p-1) MOD p
   = PROD_SET (residue p) MOD p                       by PROD_SET_RESIDUE
   = PROD_SET (IMAGE (row p a) (residue p)) MOD p     by RESIDUE_PRIME_ROW
   = (a**(p-1) * FACT (p-1)) MOD p                    by PROD_SET_IMAGE_ROW_ALL
   = (FACT (p-1) * a**(p-1)) MOD p                    by arithmetic
   ==>   1 MOD p = a**(p-1) MOD p                     by MOD_PRIME_FACT and MOD_MULT_LCANCEL
    or   a**(p-1) MOD p = 1.
*)
val FERMAT_IDENTITY = store_thm(
  "FERMAT_IDENTITY",
  ``!p a. prime p /\ a IN (residue p) ==> (a**(p-1) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `1 < p` by SRW_TAC [][dividesTheory.ONE_LT_PRIME] THEN (* for 1 MOD p = 1 *)
  `FACT (p-1) MOD p <> 0` by SRW_TAC [][MOD_PRIME_FACT] THEN
  `(FACT (p-1)*1) MOD p = FACT (p-1) MOD p` by RW_TAC arith_ss [] THEN
  `_ = PROD_SET (residue p) MOD p` by SRW_TAC [][PROD_SET_RESIDUE] THEN
  `_ = PROD_SET (IMAGE (row p a) (residue p)) MOD p` by METIS_TAC [RESIDUE_PRIME_ROW] THEN
  `_ = (FACT (p-1) * a**(p-1)) MOD p` by RW_TAC arith_ss [PROD_SET_IMAGE_ROW_ALL] THEN
  `1 = 1 MOD p` by RW_TAC arith_ss [] THEN
  `_ = a**(p-1) MOD p` by METIS_TAC [MOD_MULT_LCANCEL] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem (various forms)                                   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Fermat's Little Theorem: For prime p, (a**p) MOD p = a MOD p *)
val FERMAT_LITTLE4 = store_thm(
  "FERMAT_LITTLE4",
  ``!p a. prime p /\ a IN (residue p) ==> ((a**p) MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `1 < p` by SRW_TAC [][dividesTheory.ONE_LT_PRIME] THEN
  `(a**p) MOD p = (a**(SUC(p-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.ADD1] THEN
  `_ = (a * a**(p-1)) MOD p` by RW_TAC arith_ss [arithmeticTheory.EXP] THEN
  `_ = (a MOD p * a**(p-1) MOD p) MOD p` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
  `_ = (a MOD p * 1) MOD p` by SRW_TAC [][FERMAT_IDENTITY] THEN
  RW_TAC arith_ss []);

(* Theorem: Fermat's Little Theorem: For prime p, (a**p - a) MOD p = 0 *)
val FERMAT_LITTLE5 = store_thm(
  "FERMAT_LITTLE5",
  ``!p a. prime p /\ a IN (residue p) ==> ((a**p - a) MOD p = 0)``,
  METIS_TAC [FERMAT_LITTLE4, dividesTheory.PRIME_POS, MOD_EQ_DIFF]);

(* Theorem: Fermat's Little Theorem: For prime p, p divides (a**p - a) *)
val FERMAT_LITTLE6 = store_thm(
  "FERMAT_LITTLE6",
  ``!p a. prime p /\ a IN (residue p) ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  METIS_TAC [FERMAT_LITTLE5, MOD_0_DIVIDES]);

(* Theorem: Fermat's Little Theorem: For prime p, a IN (residue p),
    (1) (a**(p-2) MOD p) IN (residue p
    (2) ((a**(p-2) MOD p) * a) MOD p = 1 *)
(* Proof:
   First to prove: a**(p-2) MOD p > 0.
     Suppose a**(p-2) MOD p = 0, then
       a**(p-1) MOD p
     = (a * a**(p-2)) MOD p               by EXP
     = (a MOD p * a**(p-2) MOD p) MOD p   by MOD_TIMES2
     = 0
   But this contradicts FERMAT_IDENTITY, which says a**(p-1) MOD p = 1.
   Hence a**(p-2) MOD p <> 0, or a**(p-2) MOD p > 0.

   Second to prove: a**(p-2) MOD p < p.
   This is true since any  n MOD p < p, by MOD_LESS.

   Last to prove: (a**(p-2) MOD p) * a) MOD p = 1.
     ((a**(p-2) MOD p) * a) MOD p
   = ((a**(p-2) MOD p) * a MOD p) MOD p   by a = a MOD p for 0 < a < p.
   = (a**(p-2) * a) MOD p                 by MOD_TIMES2
   = (a * a**(p-2)) MOD p                 by arithmetic
   = (a**(p-1)) MOD p                     by EXP
   = 1                                    by FERMAT_IDENTITY.
*)
val FERMAT_LITTLE7 = store_thm(
  "FERMAT_LITTLE7",
  ``!p a. prime p /\ a IN (residue p) ==>
      (a**(p - 2) MOD p) IN (residue p) /\ (((a**(p - 2) MOD p) * a) MOD p = 1)``,
  SRW_TAC [][residue_def] THENL [
    SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
    `a ** (p-2) MOD p = 0` by RW_TAC arith_ss [] THEN
    `a ** (p-1) MOD p = a**(SUC (p-2)) MOD p` by RW_TAC arith_ss [arithmeticTheory.ADD1] THEN
    `_ = (a * a**(p-2)) MOD p` by RW_TAC arith_ss [arithmeticTheory.EXP] THEN
    `_ = (a MOD p * a**(p-2) MOD p) MOD p` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
    `_ = 0` by RW_TAC arith_ss [] THEN
    `a IN (residue p)` by SRW_TAC [][residue_def] THEN (* for METIS_TAC *)
    `1 <> 0` by RW_TAC arith_ss [] THEN (* for METIS_TAC *)
    METIS_TAC [FERMAT_IDENTITY],
    SRW_TAC [][dividesTheory.PRIME_POS, arithmeticTheory.MOD_LESS],
    `(a ** (p - 2) MOD p * a) MOD p = (a MOD p * a**(p-2) MOD p) MOD p` by RW_TAC arith_ss [] THEN
    `_ = (a * a**(p-2)) MOD p` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
    `_ = a ** SUC(p-2) MOD p` by RW_TAC arith_ss [arithmeticTheory.EXP] THEN
    `_ = a ** (p-1) MOD p` by RW_TAC arith_ss [arithmeticTheory.ADD1] THEN
    `a IN (residue p)` by SRW_TAC [][residue_def] THEN (* for METIS_TAC *)
    METIS_TAC [FERMAT_IDENTITY]
  ]);

(* ------------------------------------------------------------------------- *)
(* Power Version of Fermat's Little Theorem                                  *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For prime p, 0 < a /\ a < p ==> a**(p**n) = a**p  mod p *)
(* Proof:
   By induction on n:
   Base case: n = 0, a**(p**0) = a**1 = a,
              a**p = a mod p   by FERMAT_LITTLE4.
   Step case: Assume a**(p**n) = a mod p
                a**(p ** SUC n)
              = a**(p* p**n)   by EXP
              = (a**p)**(p**n) by EXP_EXP_MULT
         Hence  a**(p ** SUC n) mod p
              = (a**p)**(p**n) mod p
              = (a**p mod p)**(p**n) mod p  by EXP_MOD
              = a**(p**n) mod p             by FERMAT_LITTLE4
              = a mod p                     by inductive assumption.
*)
val FERMAT_EXP_IDENTITY = store_thm(
  "FERMAT_EXP_IDENTITY",
  ``!p n. prime p /\ 0 < a /\ a < p ==> (a**(p**n) MOD p = a**p MOD p)``,
  REPEAT STRIP_TAC THEN
  `a IN residue p` by SRW_TAC [][residue_def] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Induct_on `n` THENL [
    SRW_TAC [][arithmeticTheory.EXP, arithmeticTheory.EXP_1] THEN
    METIS_TAC [FERMAT_LITTLE4, arithmeticTheory.LESS_MOD],
    `a ** p ** SUC n MOD p = a ** (p * p ** n) MOD p` by SRW_TAC [][arithmeticTheory.EXP] THEN
    `_ = (a ** p) ** p ** n MOD p` by SRW_TAC [][arithmeticTheory.EXP_EXP_MULT] THEN
    `_ = (a ** p MOD p) ** p ** n MOD p` by METIS_TAC [EXP_MOD] THEN
    METIS_TAC [FERMAT_LITTLE4, arithmeticTheory.LESS_MOD]
  ]);

(* Theorem: (Power version of Fermat's Little Theorem)
            For prime p, 0 < a < p, a**(p**n) = a (mod p) *)
(* Proof:
   By FERMAT_EXP_IDENTITY and FERMAT_LITTLE4.
*)
val FERMAT_LITTLE_EXP1 = store_thm(
  "FERMAT_LITTLE_EXP1",
  ``!p n. prime p /\ 0 < a /\ a < p ==> (a**(p**n) MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `a IN residue p` by SRW_TAC [][residue_def] THEN
  METIS_TAC [FERMAT_EXP_IDENTITY, FERMAT_LITTLE4]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
