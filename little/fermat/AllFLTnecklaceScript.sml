(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Combinatorial proof.                            *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by J. Petersen in 1872:

Take p elements from q with repetitions in all ways, that is, in q^p ways.
The q sets with elements all alike are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted in sets of p. Hence p divides q^p - q.

This is a combinatorial proof without reference to Group Theory.

The strategy:
* Define necklace and prove basic properties (as necklaceTheory)
* Develop a theory of cycles for necklaces (as cycleTheory)
* Apply cycles with necklaces to derive Fermat's Little Theorem.

The key idea:
* Cycle-Order Theorem: For a finite list, its cycle order divides its length.
* Equal-Paritition Theorem: For a finite partition of a set into equal parts,
     size of part divides size of set.

Therefore it is only necessary to establish the following to arrive at Fermat's Little Theorem:
* CARD (multicoloured p a) = a**p - a                        from necklaceTheory
* (\l. cycle n l) is an equivalence relation in (multicoloured p a)   -- real work
* !l. l IN (multicoloured p a) ==> (order l) = p             by Cycle-Order Theorem
* Hence p divides (a**p - a)                                 by Equal-Partition Theorem

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "AllFLTnecklace";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* Theorem: If n > 0, (m MOD n = 0) <=> divides n m *)
val MOD_0_DIVIDES = store_thm(
  "MOD_0_DIVIDES",
  ``!n m. 0 < n ==> ((m MOD n = 0) <=> divides n m)``,
  SRW_TAC [][dividesTheory.divides_def, EQ_IMP_THM] THEN
  METIS_TAC [arithmeticTheory.DIVISION, arithmeticTheory.ADD_0, arithmeticTheory.MOD_EQ_0]);

(* Theorem: For a 1-1 map f: s -> s, s and (IMAGE f s) are of the same size. *)
val CARD_IMAGE = store_thm(
  "CARD_IMAGE",
  ``!s f. (!x y. (f x = f y) <=> (x = y)) /\ FINITE s ==> (CARD (IMAGE f s) = CARD s)``,
  Q_TAC SUFF_TAC
    `!f. (!x y. (f x = f y) = (x = y)) ==> !s. FINITE s ==> (CARD (IMAGE f s) = CARD s)`
    THEN1 METIS_TAC [] THEN
  GEN_TAC THEN STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][]);

(* Theorem: If !e IN s, CARD e = k, SIGMA CARD s = CARD s * k. *)
val SIGMA_CARD_CONSTANT = store_thm(
  "SIGMA_CARD_CONSTANT",
  ``!k s. FINITE s ==> (!e. e IN s ==> (CARD e = k)) ==> (SIGMA CARD s = k * (CARD s))``,
  STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][] THEN1 METIS_TAC [SUM_IMAGE_THM] THEN
  `CARD e = k` by SRW_TAC [][] THEN
  `SIGMA CARD (e INSERT s) = CARD e + SIGMA CARD (s DELETE e)` by SRW_TAC [][SUM_IMAGE_THM] THEN
  `s DELETE e = s` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `SUC (CARD s) = 1 + CARD s` by RW_TAC arith_ss [] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  RW_TAC arith_ss []);

(* Note: There is LENGTH_NIL, but no LENGTH_NON_NIL *)
(* Theorem: a non-NIL list has positive LENGTH. *)
val LENGTH_NON_NIL = store_thm(
  "LENGTH_NON_NIL",
  ``!l. l <> [] ==> 0 < LENGTH l``,
  REPEAT STRIP_TAC THEN
  `LENGTH l <> 0` by SRW_TAC [][LENGTH_NIL] THEN
  RW_TAC arith_ss []);

(* DROP and TAKE *)

(* Note: There is TAKE_LENGTH_ID, but no DROP_LENGTH_NIL *)
(* Theorem: DROP the whole length of list is NIL. *)
val DROP_LENGTH_NIL = store_thm(
  "DROP_LENGTH_NIL",
  ``DROP (LENGTH l) l = []``,
  Induct_on `l` THEN SRW_TAC [][]);

(* Theorem: TAKE 1 of non-NIL list is unaffected by appending. *)
val TAKE_1_APPEND = store_thm(
  "TAKE_1_APPEND",
  ``!x y. x <> [] ==> (TAKE 1 (x ++ y) = TAKE 1 x)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 of non-NIL list is mildly affected by appending. *)
val DROP_1_APPEND = store_thm(
  "DROP_1_APPEND",
  ``!x y. x <> [] ==> (DROP 1 (x ++ y) = (DROP 1 x) ++ y)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 more time is once more of previous DROP. *)
val DROP_SUC = store_thm(
  "DROP_SUC",
  ``!n x. DROP (SUC n) x = DROP 1 (DROP n x)``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: TAKE 1 more time is a bit more than previous TAKE. *)
val TAKE_SUC = store_thm(
  "TAKE_SUC",
  ``!n x. TAKE (SUC n) x = (TAKE n x) ++ (TAKE 1 (DROP n x))``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: DROP is non-NIL when the number is less than LENGTH. *)
val DROP_NON_NIL = store_thm(
  "DROP_NON_NIL",
  ``!n l. n < LENGTH l ==> DROP n l <> []``,
  Induct_on `l` THEN SRW_TAC [][] THEN
  `n - 1 < LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Mono-list Theory: a mono-list is a list l with SING (set l) *)

(* Theorem: Two mono-lists are equal if their lengths and sets are equal *)
val MONOLIST_EQ = store_thm(
  "MONOLIST_EQ",
  ``!l1 l2. SING (set l1) /\ SING (set l2) ==>
              ((l1 = l2) <=> (LENGTH l1 = LENGTH l2) /\ (set l1 = set l2))``,
  Induct THEN SRW_TAC [][NOT_SING_EMPTY, SING_INSERT] THENL [
    Cases_on `l2` THEN SRW_TAC [][] THEN
    FULL_SIMP_TAC (srw_ss()) [SING_INSERT, EQUAL_SING] THEN
    SRW_TAC [][LENGTH_NIL, NOT_SING_EMPTY, EQUAL_SING] THEN METIS_TAC [],
    Cases_on `l2` THEN SRW_TAC [][] THEN
    FULL_SIMP_TAC (srw_ss()) [SING_INSERT, LENGTH_NIL, NOT_SING_EMPTY, EQUAL_SING] THEN
    METIS_TAC []
  ]);

(* Theorem: A non-mono-list has at least one element in tail that is distinct from its head. *)
val NON_MONO_TAIL_PROPERTY = store_thm(
  "NON_MONO_TAIL_PROPERTY",
  ``!l. ~SING (set (h::t)) ==> ?h'. h' IN set t /\ h' <> h``,
  SRW_TAC [][SING_INSERT] THEN
  `set t <> {}` by METIS_TAC [LIST_TO_SET_EQ_EMPTY] THEN
  `?e. e IN set t` by METIS_TAC [MEMBER_NOT_EMPTY] THEN
  FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
  METIS_TAC []);

(* Theorem: interrelate elements are in the same partition *)
val interrelate_elements_in_partition = store_thm(
  "interrelate_elements_in_partition",
  `` !R s. R equiv_on s ==> !t x y. t IN partition R s /\ x IN t /\ y IN s /\ R x y ==> y IN t``,
  SRW_TAC [][equiv_on_def, partition_def, EXTENSION] THEN
  METIS_TAC []);

(* Theorem: When the partitions are equal size of n, CARD s = n * CARD (partition of s) *)
val equal_partition_CARD = store_thm(
  "equal_partition_CARD",
  ``!R s n. R equiv_on s /\ FINITE s /\
    (!t. t IN partition R s ==> (CARD t = n)) ==>
    (CARD s = n * CARD (partition R s))``,
  METIS_TAC [partition_CARD, FINITE_partition, SIGMA_CARD_CONSTANT]);

(* Part 2: General Theory -------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Theory of Necklaces                                                       *)
(* ------------------------------------------------------------------------- *)

(* Define necklaces with n beads and a colors. *)
(* Represent necklace as list, colors taken from COUNT = {1,2,3,....}  *)
val necklace_def = Define `
  necklace n a = {l | (LENGTH l = n) /\ set l SUBSET count a}
`;

(* Theorem: If l in (necklace n a), LENGTH l = n  and colors in count a. *)
val necklace_property = store_thm(
  "necklace_property",
  ``!n a l. l IN necklace n a ==> (LENGTH l = n) /\ set l SUBSET count a``,
  SRW_TAC [][necklace_def]);

(* Theorem: Zero-length necklaces of whatever colors is the set of NIL. *)
val necklace_0 = store_thm(
  "necklace_0",
  ``!a. necklace 0 a = {[]}``,
  SRW_TAC [][necklace_def, EXTENSION, EQ_IMP_THM, LENGTH_NIL]);

(* Theorem: A necklace of length n <> 0 has an order, i.e. non-NIL. *)
val necklace_not_nil = store_thm(
  "necklace_not_nil",
  ``!n a l. 0 < n /\ 0 < a /\ l IN (necklace n a) ==> l <> []``,
  METIS_TAC [necklace_property, LENGTH_NIL, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Theorem: Necklaces with some length but 0 colours are EMPTY. *)
val necklace_EMPTY = store_thm(
  "necklace_EMPTY",
  ``!n. 0 < n ==> (necklace n 0 = {})``,
  SRW_TAC [ARITH_ss][necklace_def, EXTENSION, EQ_IMP_THM]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (necklace n a) = a^n.                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: relate (necklace (n+1) a) to (necklace n a) for induction. *)
val necklace_SUC = store_thm(
  "necklace_SUC",
  ``!n a. necklace (SUC n) a = IMAGE (\(e,l). e :: l) (count a CROSS necklace n a)``,
  SRW_TAC [][necklace_def, EXTENSION, pairTheory.EXISTS_PROD, LENGTH_CONS, EQ_IMP_THM, count_def, SUBSET_DEF] THEN
  METIS_TAC [MEM]);

(* Theorem: The set of (necklace n a) is finite. *)
val FINITE_necklace = store_thm(
  "FINITE_necklace",
  ``!n a. FINITE (necklace n a)``,
  Induct_on `n` THEN SRW_TAC [][necklace_0, necklace_SUC]);

(* Theorem: CARD (necklace n a) = a^n. *)
val CARD_necklace = store_thm(
  "CARD_necklace",
  ``!n a. CARD (necklace n a) = a ** n``,
  Induct_on `n` THEN1
  SRW_TAC [][necklace_0] THEN
  SRW_TAC [][FINITE_necklace, necklace_SUC, CARD_IMAGE, CARD_CROSS,
             pairTheory.FORALL_PROD, arithmeticTheory.EXP]);

(* ------------------------------------------------------------------------- *)
(* Monocoloured Necklace - a necklace with a single color.                   *)
(* ------------------------------------------------------------------------- *)

(* Define monocoloured necklace *)
val monocoloured_def = Define`
  monocoloured n a = {l | (l IN necklace n a) /\ (l <> [] ==> SING (set l)) }
`;

(* Theorem: A monocoloured necklace is indeed a necklace. *)
val monocoloured_necklace = store_thm(
  "monocoloured_necklace",
  ``!n a l. l IN monocoloured n a ==> l IN necklace n a``,
  SRW_TAC [][necklace_def, monocoloured_def]);

(* Theorem: Zero-length monocoloured set is singleton NIL. *)
val monocoloured_0 = store_thm(
  "monocoloured_0",
  ``!a. monocoloured 0 a = {[]}``,
  SRW_TAC [][monocoloured_def, EXTENSION, EQ_IMP_THM, necklace_0]);

(* Theorem: Unit-length monocoloured set consists of singletons. *)
val monocoloured_1 = store_thm(
  "monocoloured_1",
  ``!a. monocoloured 1 a = {[e] | e IN count a}``,
  SIMP_TAC bool_ss [monocoloured_def, necklace_def, count_def, arithmeticTheory.ONE, LENGTH_CONS] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION]);

(* Theorem: The monocoloured set is FINITE. *)
val FINITE_monocoloured = store_thm(
  "FINITE_monocoloured",
  ``!n a. FINITE (monocoloured n a)``,
  METIS_TAC [SUBSET_DEF, monocoloured_necklace, SUBSET_FINITE, FINITE_necklace]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (monocoloured n a) = a.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Relate (monocoloured (SUC n) a) to (monocoloured n a) for induction. *)
val monocoloured_SUC = prove(
  ``!n a. 0 < n ==> (monocoloured (SUC n) a = IMAGE (\l. HD l :: l) (monocoloured n a))``,
  SRW_TAC [][EXTENSION, EQ_IMP_THM, monocoloured_def] THEN
  FULL_SIMP_TAC (srw_ss()) [necklace_def, LENGTH_CONS, count_def, SUBSET_DEF, SING_DEF] THENL [
    Cases_on `l'` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
    `x <> [] /\ MEM h x /\ MEM h' x` by SRW_TAC [][] THEN
    `?k. !z . MEM z x <=> (z = k)` by SRW_TAC [][] THEN
    `h = h'` by METIS_TAC [] THEN SRW_TAC [][] THEN
    Q.EXISTS_TAC `x'` THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [] THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN METIS_TAC []
  ]);

(* Theorem: Size of (monocoloured n a) = a *)
val CARD_monocoloured = store_thm(
  "CARD_monocoloured",
  ``!n a. 0 < n ==> (CARD (monocoloured n a) = a)``,
  REPEAT STRIP_TAC THEN
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n` THENL [
    SRW_TAC [][monocoloured_1] THEN
    `{[e] | e < a} = IMAGE (\n. [n]) (count a)` by SRW_TAC [][EXTENSION] THEN
    SRW_TAC [][CARD_IMAGE],
    SRW_TAC [][monocoloured_SUC] THEN
    Q.MATCH_ABBREV_TAC `CARD (IMAGE f s) = a` THEN
    `!x y. (f x = f y) <=> (x = y)` by SRW_TAC [][EQ_IMP_THM, Abbr`f`] THEN
    `FINITE s` by SRW_TAC [][FINITE_monocoloured, Abbr`s`] THEN
    SRW_TAC [][CARD_IMAGE]
  ]);

(* ------------------------------------------------------------------------- *)
(* Multicoloured necklaces                                                   *)
(* ------------------------------------------------------------------------- *)

(* Define multicoloured necklace *)
val multicoloured_def = Define`
  multicoloured n a = (necklace n a) DIFF (monocoloured n a)
`;

(* Theorem: multicoloured is a necklace *)
val multicoloured_necklace = store_thm(
  "multicoloured_necklace",
  ``!l. l IN multicoloured n a ==> l IN necklace n a``,
  SRW_TAC [][multicoloured_def, necklace_def]);

(* Theorem: multicoloured set is FINITE *)
val FINITE_multicoloured = store_thm(
  "FINITE_multicoloured",
  ``!n a. FINITE (multicoloured n a)``,
  SRW_TAC [][multicoloured_def, FINITE_necklace, FINITE_DIFF]);

(* Theorem: multicoloured n 0 = EMPTY *)
val multicoloured_EMPTY = store_thm(
  "multicoloured_EMPTY",
  ``!n. 0 < n ==> (multicoloured n 0 = {})``,
  SRW_TAC [][multicoloured_def, multicoloured_necklace, necklace_EMPTY]);

(* Theorem: multicoloured 0 a = EMPTY *)
val multicoloured_0 = store_thm(
  "multicoloured_0",
  ``multicoloured 0 a = {}``,
  SRW_TAC [][multicoloured_def, necklace_0, monocoloured_0]);

(* Theorem: multicoloured l are not monocoloured *)
val multicoloured_not_monocoloured = store_thm(
  "multicoloured_not_monocoloured",
  ``!l n a. l IN multicoloured n a ==> ~(l IN monocoloured n a)``,
  SRW_TAC [][multicoloured_def]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (multicoloured n a) = a^n - a.  [milestone]                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: CARD (multicoloured n a) = a^n - a *)
val CARD_multicoloured = store_thm(
  "CARD_multicoloured",
  ``!n a. 0 < n ==> (CARD (multicoloured n a) = a**n - a)``,
  REPEAT STRIP_TAC THEN
  `FINITE (necklace n a)` by SRW_TAC [][FINITE_necklace] THEN
  `FINITE (monocoloured n a)` by SRW_TAC [][FINITE_monocoloured] THEN
  `(monocoloured n a) SUBSET (necklace n a)` by SRW_TAC [][monocoloured_necklace, SUBSET_DEF] THEN
  `CARD (multicoloured n a) = CARD (necklace n a DIFF monocoloured n a)` by SRW_TAC [][multicoloured_def] THEN
  `_ = CARD (necklace n a) - CARD (necklace n a INTER monocoloured n a)` by SRW_TAC [][CARD_DIFF] THEN
  `_ = CARD (necklace n a) - CARD (monocoloured n a)` by METIS_TAC [SUBSET_INTER_ABSORPTION, INTER_COMM] THEN
  METIS_TAC [CARD_necklace, CARD_monocoloured]);

(* ------------------------------------------------------------------------- *)
(* Theory of Cycles                                                          *)
(* ------------------------------------------------------------------------- *)

(* Define cycle in view of action from Z_n to necklace n a *)

(* Cycle as nth iterate of rotate: DROP 1 ++ TAKE 1 *)
val cycle_def = Define `
  cycle n l = FUNPOW (\l. DROP 1 l ++ TAKE 1 l) n l
`;

(* Theorem: cycle 0 l = l *)
val CYCLE_0 = store_thm(
  "CYCLE_0",
  ``!l. cycle 0 l = l``,
  SRW_TAC [][cycle_def]);

(* Theorem: Cycle (n+1) of a list is cycle once more of cycle n list.
            cycle (SUC n) l = cycle 1 (cycle n l) *)
val CYCLE_SUC = store_thm(
  "CYCLE_SUC",
  ``!l n. cycle (SUC n) l = cycle 1 (cycle n l)``,
  METIS_TAC [cycle_def, arithmeticTheory.FUNPOW_SUC, arithmeticTheory.FUNPOW_1]);

(* Theorem: Cycle is additive, (cycle n (cycle m l) = cycle (n+m) l  *)
val CYCLE_ADD = store_thm(
  "CYCLE_ADD",
  ``!l n m. cycle n (cycle m l) = cycle (n+m) l``,
  SRW_TAC [][cycle_def, arithmeticTheory.FUNPOW_ADD]);

(* Theorem: Only NIL will cycle to NIL. *)
val CYCLE_NIL = store_thm(
  "CYCLE_NIL",
  ``!l n. (cycle n l = []) <=> (l = [])``,
  `!l. (cycle 1 l = []) <=> (l = [])`
      by (SRW_TAC [][cycle_def, EQ_IMP_THM] THEN
          Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) []) THEN
  Induct_on `n` THEN1 SRW_TAC [][CYCLE_0] THEN
  SRW_TAC [][CYCLE_SUC]);

(* Theorem: Cycle keeps LENGTH (of necklace), or LENGTH (cycle n l) = LENGTH l *)
val CYCLE_SAME_LENGTH = store_thm(
  "CYCLE_SAME_LENGTH",
  ``!l n. LENGTH (cycle n l) = LENGTH l``,
  `!l. LENGTH (cycle 1 l) = LENGTH l`
      by (SRW_TAC [][cycle_def] THEN Cases_on `l` THEN
          SRW_TAC [ARITH_ss][]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* Theorem: Cycle keep set (of colors), or set (cycle n l) = set l *)
val CYCLE_SAME_SET = store_thm(
  "CYCLE_SAME_SET",
  ``!l n. set (cycle n l) = set l``,
  `!l. set (cycle 1 l) = set l`
      by (Cases_on `l` THEN SRW_TAC [][cycle_def, EXTENSION, DISJ_COMM]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* ------------------------------------------------------------------------- *)
(* To show:  cycle x (cycle y l) = cycle ((x + y) MOD n) l, n = LENGTH l.    *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For n <= LENGTH l, cycle n l = DROP n l ++ TAKE n l *)
val CYCLE_EQ_DROP_TAKE = prove(
  ``!l n. n <= LENGTH l ==> (cycle n l = DROP n l ++ TAKE n l)``,
  REPEAT STRIP_TAC THEN
  Cases_on `l = []` THEN1 (
    `LENGTH l = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `n = 0` by RW_TAC arith_ss [] THEN
    SRW_TAC [][CYCLE_0]) THEN
  Induct_on `n` THEN1 SRW_TAC [][CYCLE_0] THEN
  SRW_TAC [][CYCLE_SUC] THEN
  `n < LENGTH l` by RW_TAC arith_ss [] THEN
  SRW_TAC [ARITH_ss][] THEN
  SRW_TAC [][cycle_def] THEN
  SRW_TAC [][DROP_SUC, TAKE_SUC, DROP_1_APPEND, TAKE_1_APPEND, DROP_NON_NIL]);

(* Theorem: Cycle through length gives original, or cycle (LENGTH l) l = l. *)
val CYCLE_BACK = store_thm(
  "CYCLE_BACK",
  ``!l. cycle (LENGTH l) l = l``,
  SRW_TAC [][CYCLE_EQ_DROP_TAKE, DROP_LENGTH_NIL, TAKE_LENGTH_ID]);

(* Theorem: cycle (n*LENGTH l) l = l. *)
val CYCLE_BACK_MULTIPLE = store_thm(
  "CYCLE_BACK_MULTIPLE",
  ``!l n. cycle (n * LENGTH l) l = l``,
  Induct_on `n` THEN
  SRW_TAC [][CYCLE_0] THEN
  `SUC n * LENGTH l = LENGTH l + n * LENGTH l` by RW_TAC arith_ss [arithmeticTheory.MULT_SUC] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK]);

(* Theorem: if l <> [], cycle n l = cycle (n MOD (LENGTH l)) l *)
val CYCLE_MOD_LENGTH = store_thm(
  "CYCLE_MOD_LENGTH",
  ``!l n. l <> [] ==> (cycle n l = cycle (n MOD (LENGTH l)) l)``,
  REPEAT STRIP_TAC THEN
  `0 < LENGTH l` by SRW_TAC [][LENGTH_NON_NIL] THEN
  `n = n DIV LENGTH l * LENGTH l + n MOD LENGTH l` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = n MOD LENGTH l + n DIV LENGTH l * LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK_MULTIPLE]);

(* Theorem: cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l *)
val CYCLE_ADDITION = store_thm(
  "CYCLE_ADDITION",
  ``!l x y. l <> [] ==> (cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l)``,
  METIS_TAC [CYCLE_ADD, CYCLE_MOD_LENGTH]);

(* Theorem: cycle (LENGTH l - n) (cycle n l) = l *)
val CYCLE_INV = store_thm(
  "CYCLE_INV",
  ``!n l. n <= LENGTH l ==> (cycle (LENGTH l - n) (cycle n l) = l)``,
  REPEAT STRIP_TAC THEN
  `(LENGTH l - n) + n = LENGTH l` by RW_TAC arith_ss [] THEN
  SRW_TAC [][CYCLE_ADD, CYCLE_BACK]);

(* Theorem: A mono-list has cycle 1 l = l *)
val SING_HAS_CYCLE_1 = store_thm(
  "SING_HAS_CYCLE_1",
  ``!l. SING (set l) ==> (cycle 1 l = l)``,
  SRW_TAC [][cycle_def] THEN
  Cases_on `l` THEN SRW_TAC [][] THEN
  FULL_SIMP_TAC (srw_ss()) [SING_INSERT] THEN
  `SING (set (t ++ [h]))` by SRW_TAC [][SING_UNION] THEN
  SRW_TAC [ARITH_ss][MONOLIST_EQ]);

(* Theorem: cycle 1 (h::t) = t ++ [h] *)
val CYCLE_1_EQ = store_thm(
  "CYCLE_1_EQ",
  ``!h t. cycle 1 (h::t) = t ++ [h]``,
  SRW_TAC [][cycle_def]);

(* Theorem: (t ++ [h] <> h::t) if (set t) has some element h' <> h. *)
val CYCLE_1_NEQ = store_thm(
  "CYCLE_1_NEQ",
  ``!h h' t. h' IN set t /\ h' <> h ==> (t ++ [h] <> h::t)``,
  SRW_TAC [][] THEN
  Induct_on `t` THEN SRW_TAC [][] THEN
  METIS_TAC []);

(* Theorem: If a value n can cycle back, multiples of n can also cycle back. *)
(* Proof: by induction on m. *)
val CYCLE_MULTIPLE_BACK = store_thm(
  "CYCLE_MULTIPLE_BACK",
  ``!n l. (cycle n l = l) ==> !m. cycle (m*n) l = l``,
  REPEAT STRIP_TAC THEN
  Induct_on `m` THEN
  METIS_TAC [arithmeticTheory.MULT_CLAUSES, CYCLE_0, CYCLE_ADD]);


(* Theorem: If two values m, n can cycle back, gcd(m,n) can also cycle back. *)
(* Proof:
     If n = 0, cycle (gcd m 0) l = cycle m l = l by given, GCD_0R.
     If n <> 0, ?p q. p * n = q * m + gcd m n    by LINEAR_GCD,
       
     cycle (gcd m n) l
   = cycle (gcd m n) (cycle m l)                 by given
   = cycle (gcd m n) (cycle q*m l)               by CYCLE_MULTIPLE_BACK
   = cycle (gcd m n + q*m) l                     by CYCLE_ADD     
   = cycle (q*m + gcd m n) l                     by arithmetic                
   = cycle (p*n) l                               by substitution
   = cycle n l                                   by CYCLE_MULTIPLE_BACK
   = l                                           by given
*)
val CYCLE_GCD_BACK = store_thm(
  "CYCLE_GCD_BACK",
  ``!m n l. (cycle m l = l) /\ (cycle n l = l) ==> (cycle (gcd m n) l = l)``,
  REPEAT STRIP_TAC THEN
  Cases_on `n=0` THEN1
  METIS_TAC [gcdTheory.GCD_0R] THEN
  `?p q. p * n = q * m + gcd m n` by METIS_TAC [gcdTheory.LINEAR_GCD] THEN
  `cycle (gcd m n) l = cycle (gcd m n) (cycle (q*m) l)` by METIS_TAC [CYCLE_MULTIPLE_BACK] THEN
  METIS_TAC [CYCLE_ADD, arithmeticTheory.ADD_COMM, CYCLE_MULTIPLE_BACK]);

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof without Group Theory explicitly.                      *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Similarity in Cycles (Theory of Patterns)                                 *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Two lists are similar if they are related through cycle.                  *)
(* ------------------------------------------------------------------------- *)

(* Define similar to relate two lists *)
val similar_def = Define`
  similar l1 l2 = ?n. l2 = cycle n l1
`;

(* set infix and overload for similar *)
val _ = set_fixity "==" (Infixl 480);
val _ = overload_on ("==", ``similar``);

(* Theorem: Only NIL can be similar to NIL. *)
val SIMILAR_NIL = store_thm(
  "SIMILAR_NIL",
  ``!l. l == [] \/ [] == l <=> (l = [])``,
  METIS_TAC [similar_def, CYCLE_NIL]);

(* ------------------------------------------------------------------------- *)
(* Similar is an equivalence relation.                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Reflexive] l == l *)
val SIMILAR_REFL = store_thm(
  "SIMILAR_REFL",
  ``!l. l == l``,
  METIS_TAC [similar_def, CYCLE_0]);

(* Theorem: [Symmetric] l1 == l2 ==> l2 == l1 *)
val SIMILAR_SYM = store_thm(
  "SIMILAR_SYM",
  ``!l1 l2. (l1 == l2) ==> (l2 == l1)``,
  SRW_TAC [][similar_def] THEN
  Cases_on `l1 = []` THEN1
  METIS_TAC [CYCLE_NIL] THEN
  `n MOD LENGTH l1 <= LENGTH l1`
    by METIS_TAC [LENGTH_NON_NIL, arithmeticTheory.MOD_LESS, arithmeticTheory.LESS_IMP_LESS_OR_EQ] THEN
  Q.EXISTS_TAC `LENGTH l1 - (n MOD LENGTH l1)` THEN
  METIS_TAC [CYCLE_MOD_LENGTH, CYCLE_INV]);

(* Theorem: [Transitive] l1 == l2 /\ l2 == l3 ==> l1 == l3 *)
val SIMILAR_TRANS = store_thm(
  "SIMILAR_TRANS",
  ``!l1 l2 l3. (l1 == l2) /\ (l2 == l3) ==> (l1 == l3)``,
  METIS_TAC [similar_def, CYCLE_ADD]);

(* ------------------------------------------------------------------------- *)
(* Similar associates and their cardinality.                                 *)
(* ------------------------------------------------------------------------- *)

(* Define the associate of list: all those that are similar to the list. *)
val associate_def = Define `
  associate x = {y | x == y }
`;

(* Theorem: associate NIL is singleton {NIL}. *)
val ASSOCIATE_NIL = store_thm(
  "ASSOCIATE_NIL",
  ``associate [] = {[]}``,
  SRW_TAC [][associate_def, EXTENSION] THEN METIS_TAC [SIMILAR_NIL]);

(* ------------------------------------------------------------------------- *)
(* To show: associates are FINITE.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The map (\n. cycle n l) from (count LENGTH l) -> (associate l) is surjective. *)
val ASSOCIATE_AS_IMAGE = store_thm(
  "ASSOCIATE_AS_IMAGE",
  ``!l. l <> [] ==> (associate l = IMAGE (\n. cycle n l) (count (LENGTH l)))``,
  SRW_TAC [][associate_def, similar_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [CYCLE_MOD_LENGTH, LENGTH_NIL,
             arithmeticTheory.MOD_LESS, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Theorem: associate l is FINITE *)
val FINITE_ASSOCIATE = store_thm(
  "FINITE_ASSOCIATE",
  ``!l. FINITE (associate l)``,
  STRIP_TAC THEN Cases_on `l` THEN
  METIS_TAC [ASSOCIATE_NIL, FINITE_SING,
             ASSOCIATE_AS_IMAGE, FINITE_COUNT, IMAGE_FINITE]);

(* ------------------------------------------------------------------------- *)
(* Apply to Necklaces: Relate cycle/similar to multicoloured necklaces.      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If l is a necklace, cycle n l is also a necklace. *)
val NECKLACE_CYCLE = store_thm(
  "NECKLACE_CYCLE",
  ``!l. l IN necklace n a ==> !k. (cycle k l) IN necklace n a``,
  SRW_TAC [][necklace_def, CYCLE_SAME_LENGTH, CYCLE_SAME_SET]);

(* Theorem: [Closure of cycle for multicoloured necklaces]
            If l is a multicoloured necklace, cycle n l is also multicolored. *)
val multicoloured_CYCLE = store_thm(
  "multicoloured_CYCLE",
  ``!l n a. l IN multicoloured n a ==> !k. (cycle k l) IN multicoloured n a``,
  REPEAT STRIP_TAC THEN
  `l IN (necklace n a)` by SRW_TAC [][multicoloured_necklace] THEN
  Cases_on `n=0` THEN1 METIS_TAC [multicoloured_0, MEMBER_NOT_EMPTY] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  Cases_on `a=0` THEN1 METIS_TAC [multicoloured_EMPTY, MEMBER_NOT_EMPTY] THEN
  `0 < a` by RW_TAC arith_ss [] THEN
  `l <> []` by METIS_TAC [necklace_not_nil] THEN
  FULL_SIMP_TAC (srw_ss()) [multicoloured_def, monocoloured_def] THEN
  METIS_TAC [NECKLACE_CYCLE, CYCLE_SAME_SET, LENGTH_NIL, CYCLE_SAME_LENGTH]);

(* Theorem: [Closure of similar for multicoloured necklaces]
            Only multicoloured necklaces can be similar to each other. *)
val multicoloured_SIMILAR = store_thm(
  "multicoloured_SIMILAR",
  ``!n a x y. x IN multicoloured n a /\ (x == y) ==> y IN multicoloured n a``,
  METIS_TAC [similar_def, multicoloured_CYCLE]);

(* ------------------------------------------------------------------------- *)
(* Order of Necklaces.                                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A monocoloured necklace l has cycle 1 l = l *)
val monocoloured_cycle1 = store_thm(
  "monocoloured_cycle1",
  ``!n a l. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> (cycle 1 l = l)``,
  SRW_TAC [][monocoloured_def] THEN
  `l <> []` by METIS_TAC [necklace_not_nil] THEN
  METIS_TAC [SING_HAS_CYCLE_1]);

(* Theorem: If a necklace l has cycle 1 l = l , it must be monocoloured *)
val cycle1_monocoloured = store_thm(
  "cycle1_monocoloured",
  ``!l n a. 0 < a /\ l IN necklace n a /\ (cycle 1 l = l) ==> l IN monocoloured n a``,
  SRW_TAC [][monocoloured_def, necklace_def] THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* Theorem: A necklace l is monocoloured iff cycle 1 l = 1 *)
val monocoloured_iff_cycle1 = store_thm(
  "monocoloured_iff_cycle1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN necklace n a ==>
     (l IN monocoloured n a <=> (cycle 1 l = l))``,
  METIS_TAC [monocoloured_necklace, cycle1_monocoloured, monocoloured_cycle1]);

(* ------------------------------------------------------------------------- *)
(* To show: cycle induces is an equivalence relation.      [milestone]       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Similarity is an equivalence relation on multicoloured necklaces. *)
val SIMILAR_EQUIV_ON_multicoloured = store_thm(
  "SIMILAR_EQUIV_ON_multicoloured",
  ``!n a. similar equiv_on multicoloured n a``,
  SRW_TAC [][equiv_on_def] THEN
  METIS_TAC [SIMILAR_REFL, SIMILAR_SYM, SIMILAR_TRANS]);

(* ------------------------------------------------------------------------- *)
(* Size of associate for multicoloured necklaces of length prime.            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For multicoloured l, prime (LENGTH l), the map (count (LENGTH l) to (associate l) is injective *)
(* Proof:
   Essentially this is to show:
   Let p = LENGTH l,
   For all x < p, y < p,  cycle x l = cycle y l ==> x = y.
   Suppose not,
   then there are x, y such that x <> y, and cycle x l = cycle y l.
   Let l' = cycle x l. Assume x < y, then y = d + x, d = difference, and 0 < d < p.
   cycle d l' = cycle d (cycle x l) = cycle (d+x) l = cycle y l = l'
   but cycle p l' = l'   by CYCLE_BACK,
   so cycle (gcd d p) l' = l', by CYCLE_GCD_BACK.
   For prime p, 0 < d < p, gcd d p = 1. This contradicts monocoloured_iff_cycle1.
   Similar for x > y.
*)
val prime_multicoloured_associate_injection = store_thm(
  "prime_multicoloured_associate_injection",
  ``!l p a. prime p /\ l IN multicoloured p a ==> INJ (\n. cycle n l) (count p) (associate l)``,
  SRW_TAC [][associate_def, INJ_DEF] THEN1
  METIS_TAC [similar_def] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [multicoloured_EMPTY, MEMBER_NOT_EMPTY] THEN
  `0 < a` by DECIDE_TAC THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `?l'. cycle n l = l'` by SRW_TAC [][] THEN
  `l' IN multicoloured p a /\ (LENGTH l' = LENGTH l)` by METIS_TAC [multicoloured_CYCLE, CYCLE_SAME_LENGTH] THEN
  `cycle p l' = l'` by METIS_TAC [CYCLE_BACK, multicoloured_necklace, necklace_property] THEN
  Cases_on `n < n'` THENL [
    `?d. n' = d + n` by METIS_TAC [arithmeticTheory.LESS_ADD] THEN
    `0 < d /\ d < p` by RW_TAC arith_ss [] THEN
    `cycle d l' = l'` by METIS_TAC [CYCLE_ADD] THEN
    `cycle (gcd d p) l' = l'` by METIS_TAC [CYCLE_GCD_BACK] THEN
    `gcd d p = 1` by METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD, gcdTheory.GCD_SYM] THEN
    METIS_TAC [monocoloured_iff_cycle1, multicoloured_necklace, multicoloured_not_monocoloured],
    `n' < n` by DECIDE_TAC THEN
    `?d. n = d + n'` by METIS_TAC [arithmeticTheory.LESS_ADD] THEN
    `0 < d /\ d < p` by RW_TAC arith_ss [] THEN
    `cycle d l' = l'` by METIS_TAC [CYCLE_ADD] THEN
    `cycle (gcd d p) l' = l'` by METIS_TAC [CYCLE_GCD_BACK] THEN
    `gcd d p = 1` by METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD, gcdTheory.GCD_SYM] THEN
    METIS_TAC [monocoloured_iff_cycle1, multicoloured_necklace, multicoloured_not_monocoloured]
  ]);
  
(* Theorem: For multicoloured l, the map (count (LENGTH l) to (associate l) is surjective *)
(* Proof:
   Essentially this is to show:
   l == x ==> ?k. k < (LENGTH l) /\ (cycle k l = x),
   l == x ==> ?h. cycle h l = x.
   Take k = h MOD (LENGTH l), then apply CYCLE_MOD_LENGTH.
*)
val multicoloured_associate_surjection = store_thm(
  "multicoloured_associate_surjection",
  ``!l n a. l IN multicoloured n a ==> SURJ (\k. cycle k l) (count n) (associate l)``,
  SRW_TAC [][associate_def, SURJ_DEF] THEN1
  METIS_TAC [similar_def] THEN
  Cases_on `n = 0` THEN1
  METIS_TAC [multicoloured_0, MEMBER_NOT_EMPTY] THEN
  `0 < n` by DECIDE_TAC THEN
  `LENGTH l = n` by METIS_TAC [multicoloured_necklace, necklace_property] THEN
  `l <> []` by METIS_TAC [LENGTH_NIL, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [similar_def, CYCLE_MOD_LENGTH, arithmeticTheory.MOD_LESS]);

(* Theorem: For multicoloured l, prime (LENGTH l), the map (count (LENGTH l) to (associate l) is bijective *)
(* Proof: from injection and surjection. *)
val prime_multicoloured_associate_bijection = store_thm(
  "prime_multicoloured_associate_bijection",
  ``!l p a. prime p /\ l IN multicoloured p a ==> BIJ (\n. cycle n l) (count p) (associate l)``,
  METIS_TAC [BIJ_DEF, prime_multicoloured_associate_injection, multicoloured_associate_surjection]);

(* Theorem: For multicoloured l, prime (LENGTH l), size of (associate l) = (LENGTH l) *)
(* Proof: by the bijection between (associate l) and (count (LENGTH l)) when (LENGTH l) is prime. *)
val card_prime_multicoloured_associate = store_thm(
  "card_prime_multicoloured_associate",
  ``!l p a. prime p /\ l IN multicoloured p a ==> (CARD (associate l) = p)``,
  METIS_TAC [prime_multicoloured_associate_bijection,
             FINITE_BIJ_CARD_EQ, FINITE_ASSOCIATE, FINITE_COUNT, CARD_COUNT]);

(* Theorem: For 0 < a < p, and prime p, p divids (a^p - a) *)
(* Proof:
   Since p is prime, 0 < p.
   When 0 < p, CARD (multicoloured p a) = a ** p - a   by CARD_multicoloured.
   For all l IN multicoloured p a, LENGTH l = p        by necklace_property
   CARD (associate l) = p                              by card_prime_multicoloured_associate
   But $== equiv_on multicoloured n a                  by SIMILAR_EQUIV_ON_multicoloured
   and (associate l) IN partition $== (multicoloured p a)   by partition_def
   hence CARD (multicoloured p a) = p * CARD (partition $== (multicoloured p a))  by equal_partition_CARD
   Thus   p divides (a**p - a)                         by divides_def;
*)
val FERMAT_LITTLE_GCD = store_thm(
  "FERMAT_LITTLE_GCD",
  ``!p a. prime p ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `a**p - a = CARD (multicoloured p a)` by METIS_TAC [CARD_multicoloured] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [arithmeticTheory.EXP_EQ_0, arithmeticTheory.SUB_0, dividesTheory.ALL_DIVIDES_0] THEN
  `0 < a` by DECIDE_TAC THEN
  `!l. l IN (multicoloured p a) ==> (associate l) IN partition $== (multicoloured p a)` 
     by SRW_TAC [][partition_def, associate_def, SIMILAR_EQUIV_ON_multicoloured] THENL [
    Q.EXISTS_TAC `l` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    METIS_TAC [multicoloured_SIMILAR],
    `!l. l IN (multicoloured p a) ==> (CARD (associate l) = p)` by METIS_TAC [card_prime_multicoloured_associate] THEN
    `!t. t IN (partition $== (multicoloured p a)) ==> ? l. l IN (multicoloured p a) /\ (t = associate l)` 
     by SRW_TAC [][partition_def, associate_def, SIMILAR_EQUIV_ON_multicoloured] THENL [
      Q.EXISTS_TAC `x` THEN
      SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
      METIS_TAC [multicoloured_SIMILAR],
      `!t. t IN (partition $== (multicoloured p a)) ==> (CARD t = p)` by METIS_TAC [] THEN
      `CARD (multicoloured p a) = p * CARD (partition $== (multicoloured p a))` 
   by METIS_TAC [SIMILAR_EQUIV_ON_multicoloured, FINITE_multicoloured, equal_partition_CARD] THEN
      METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_COMM]
    ]
  ]);

(* Part 4: End ------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
