(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Binomial Proof.                                 *)
(* ------------------------------------------------------------------------- *)

(*
Fermat's Little Theorem (Binomial proof)
========================================
To prove:  a^p = a  (mod p)   for prime p
Proof:
   By induction on a.

   Base case: 0^p = 0 (mod p)
      true by arithmetic.
   Step case: k^p = k (mod p)  ==> (k+1)^p = (k+1) (mod p)
      (k+1)^p
    = SUM (GENLIST (\j. (binomial p j)* k**(p-j) * 1**j) (SUC p))    by binomial_thm
    = SUM (GENLIST (\j. (binomial p j)* k**(p-j)) (SUC p))           by arithmetic

   By prime_iff_divides_binomials,
      prime p <=> 1 < p /\ (!j. 0 < j /\ j < p ==> divides p (binomial p j))`;
   Therefore in mod p,
      (k+1)^p = k^p + 1^p    (mod p)   just first and last terms
              = k   + 1^p    (mod p)   by induction hypothesis
              = k + 1                  by arithmetic
*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "AllFLTbinomial";

(* open dependent theories *)
open arithmeticTheory; (* for MOD and EXP *)
open pred_setTheory; (* for PROD_SET_THM *)
open listTheory; (* for SUM and GENLIST *)

(* val _ = load "dividesTheory"; *)
(* val _ = load "gcdTheory"; *)
open dividesTheory gcdTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* Useful theorems *)

(* LESS_EQ_SUC_REFL: m <= SUC m *)

(* Theorem: n < SUC n *)
val LESS_SUC = store_thm(
  "LESS_SUC",
  ``!n. n < SUC n``,
  DECIDE_TAC);

(* Theorem: 0 < k ==> ?h. k = SUC h *)
val LESS_EQ_SUC = store_thm(
  "LESS_EQ_SUC",
  ``!k. 0 < k ==> ?h. k = SUC h``,
  METIS_TAC [NOT_ZERO_LT_ZERO, num_CASES]);

(* Theorem: m < n ==> m <> n *)
val LESS_NOT_EQ = store_thm(
  "LESS_NOT_EQ",
  ``!m n. m < n ==> m <> n``,
  DECIDE_TAC);

(* Theorem: divides n x <=> x MOD n = 0 *)
val DIVIDES_MOD_0 = store_thm(
  "DIVIDES_MOD_0",
  ``!n x. 0 < n ==> (divides n x <=> (x MOD n = 0))``,
  SRW_TAC [][EQ_IMP_THM] THEN1
  METIS_TAC [divides_def, MOD_EQ_0] THEN
  METIS_TAC [DIVISION, ADD_0, divides_def]);

(* Theorem: If n > 0, a MOD n = b MOD n ==> (a - b) MOD n = 0 *)
val MOD_EQ_DIFF = store_thm(
  "MOD_EQ_DIFF",
  ``!n a b. 0 < n /\ (a MOD n = b MOD n) ==> ((a - b) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  `a = (a DIV n)*n + a MOD n` by METIS_TAC [DIVISION] THEN
  `b = (b DIV n)*n + b MOD n` by METIS_TAC [DIVISION] THEN
  `a - b = (a DIV n)*n - (b DIV n)*n` by RW_TAC arith_ss [] THEN
  `_ = ((a DIV n) - (b DIV n))*n` by RW_TAC arith_ss [] THEN
  METIS_TAC [MOD_EQ_0]);
(* Note: The reverse is true only when a >= b:
         (a-b) MOD n = 0 cannot imply a MOD n = b MOD n *)

(* Theorem: (x + y + z) MOD n = (x MOD n + y MOD n + z MOD n) MOD n *)
val MOD_PLUS3 = store_thm(
  "MOD_PLUS3",
  ``!n. 0 < n ==> !x y z. (x + y + z) MOD n = (x MOD n + y MOD n + z MOD n) MOD n``,
  METIS_TAC [MOD_PLUS, MOD_MOD]);

(* Part 2: General Theory -------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Binomial Coefficients.                                                    *)
(* ------------------------------------------------------------------------- *)

(* Define Binomials:
   C(n,0) = 1
   C(0,k) = 0 if k > 0
   C(n+1,k+1) = C(n,k) + C(n,k+1)
*)
val binomial_def = Define`
    (binomial 0 0  = 1) /\
    (binomial (SUC n) 0  = 1) /\
    (binomial 0 (SUC k) = 0)  /\
    (binomial (SUC n) (SUC k) = binomial n k + binomial n (SUC k))
`;

(* Basic properties *)

(* Theorem: C(n,0) = 1 *)
val binomial_n_0 = store_thm(
  "binomial_n_0",
  ``!n. binomial n 0 = 1``,
  METIS_TAC [binomial_def, num_CASES]);

(* Theorem: C(n,k) = 0 if n < k *)
val binomial_less_0 = store_thm(
  "binomial_less_0",
  ``!n k. n < k ==> (binomial n k = 0)``,
  Induct_on `n` THEN1
  METIS_TAC [binomial_def, num_CASES, NOT_ZERO_LT_ZERO] THEN
  SRW_TAC [][binomial_def] THEN
  `?h. k = SUC h` by METIS_TAC [SUC_NOT, NOT_ZERO_LT_ZERO, LESS_EQ_SUC, LESS_TRANS] THEN
  METIS_TAC [binomial_def, LESS_MONO_EQ, LESS_TRANS, LESS_SUC, ADD_0]);

(* Theorem: C(n,n) = 1 *)
val binomial_n_n = store_thm(
  "binomial_n_n",
  ``!n. binomial n n = 1``,
  Induct_on `n` THEN1
  METIS_TAC [binomial_def] THEN
  METIS_TAC [binomial_def, LESS_SUC, binomial_less_0, ADD_0]);

(* Theorem: C(n+1,k+1) = C(n,k) + C(n,k+1) *)
val binomial_recurrence = store_thm(
  "binomial_recurrence",
  ``!n k. binomial (SUC n) (SUC k) = binomial n k + binomial n (SUC k)``,
  SRW_TAC[][binomial_def]);

(* Theorem: C(n+k,k) = (n+k)!/n!k!  *)
val binomial_formula = store_thm(
  "binomial_formula",
  ``!n k. binomial (n+k) k * (FACT n * FACT k) = FACT (n+k)``,
  Induct_on `k` THEN1
  METIS_TAC [binomial_n_0, FACT, MULT_CLAUSES, ADD_0] THEN
  Induct_on `n` THEN1
  METIS_TAC [binomial_n_n, FACT, MULT_CLAUSES, ADD_CLAUSES] THEN
  `SUC n + SUC k = SUC (SUC (n+k))` by DECIDE_TAC THEN
  `SUC (n + k) = SUC n + k` by DECIDE_TAC THEN
  `binomial (SUC n + SUC k) (SUC k) * (FACT (SUC n) * FACT (SUC k)) =
    (binomial (SUC (n + k)) k +
     binomial (SUC (n + k)) (SUC k)) * (FACT (SUC n) * FACT (SUC k))`
    by METIS_TAC [binomial_recurrence] THEN
  `_ = binomial (SUC (n + k)) k * (FACT (SUC n) * FACT (SUC k)) +
        binomial (SUC (n + k)) (SUC k) * (FACT (SUC n) * FACT (SUC k))`
        by METIS_TAC [RIGHT_ADD_DISTRIB] THEN
  `_ = binomial (SUC n + k) k * (FACT (SUC n) * ((SUC k) * FACT k)) +
        binomial (n + SUC k) (SUC k) * ((SUC n) * FACT n * FACT (SUC k))`
        by METIS_TAC [ADD_COMM, SUC_ADD_SYM, FACT] THEN
  `_ = binomial (SUC n + k) k * FACT (SUC n) * FACT k * (SUC k) +
        binomial (n + SUC k) (SUC k) * FACT n * FACT (SUC k) * (SUC n)`
        by METIS_TAC [MULT_COMM, MULT_ASSOC] THEN
  `_ = FACT (SUC n + k) * SUC k + FACT (n + SUC k) * SUC n`
        by METIS_TAC [MULT_COMM, MULT_ASSOC] THEN
  `_ = FACT (SUC (n+k)) * SUC k + FACT (SUC (n+k)) * SUC n`
        by METIS_TAC [ADD_COMM, SUC_ADD_SYM] THEN
  `_ = FACT (SUC (n+k)) * (SUC k + SUC n)` by METIS_TAC [LEFT_ADD_DISTRIB] THEN
  `_ = (SUC n + SUC k) * FACT (SUC (n+k))` by METIS_TAC [MULT_COMM, ADD_COMM] THEN
  METIS_TAC [FACT]);

(* Theorem: C(n,k) = n!/k!(n-k)!  for 0 <= k <= n *)
val binomial_formula2 = store_thm(
  "binomial_formula2",
  ``!n k. k <= n ==> (FACT n = binomial n k * (FACT (n-k) * FACT k))``,
  METIS_TAC [binomial_formula, SUB_ADD]);

(* Theorem: prime p ==> p cannot divide k! for k < p *)
val PRIME_LESS_NOT_DIVIDES_FACT = store_thm(
  "PRIME_LESS_NOT_DIVIDES_FACT",
  ``!p k. prime p /\ k < p ==> ~(divides p (FACT k))``,
  (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
  Induct_on `k` THENL [
    SRW_TAC [][FACT] THEN
    METIS_TAC [ONE_LT_PRIME, LESS_NOT_EQ],
    SRW_TAC [][FACT] THEN
    (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
    `k < p /\ 0 < SUC k` by DECIDE_TAC THEN
    METIS_TAC [P_EUCLIDES, NOT_LT_DIVIDES]
  ]);

(* Theorem: n is prime ==> n divides C(n,k)  for all 0 < k < n *)
val prime_divides_binomials = store_thm(
  "prime_divides_binomials",
  ``!n. prime n ==> 1 < n /\ (!k. 0 < k /\ k < n ==> divides n (binomial n k))``,
  REPEAT STRIP_TAC THEN1
  METIS_TAC [ONE_LT_PRIME] THEN
  `(n = n-k + k) /\ (n-k) < n` by DECIDE_TAC THEN
  `FACT n = (binomial n k) * (FACT (n-k) * FACT k)` by METIS_TAC [binomial_formula] THEN
  `~ divides n (FACT k) /\
    ~ divides n (FACT (n-k))` by METIS_TAC [PRIME_LESS_NOT_DIVIDES_FACT] THEN
  `divides n (FACT n)` by METIS_TAC [DIVIDES_FACT, LESS_TRANS] THEN
  METIS_TAC [P_EUCLIDES]);

(* Theorem: ~ prime n ==> n has a proper prime factor p *)
val PRIME_FACTOR_PROPER = store_thm(
  "PRIME_FACTOR_PROPER",
  ``!n. 1 < n /\ ~prime n ==> ?p. prime p /\ p < n /\ (divides p n)``,
  REPEAT STRIP_TAC THEN
  `0 < n /\ n <> 1` by DECIDE_TAC THEN
  `?p. prime p /\ divides p n` by METIS_TAC [PRIME_FACTOR] THEN
  `~(n < p)` by METIS_TAC [NOT_LT_DIVIDES] THEN
  Cases_on `n = p` THEN1
  FULL_SIMP_TAC std_ss [] THEN
  `p < n` by DECIDE_TAC THEN
  METIS_TAC []);

(* Theorem: n! = PROD_SET (count (n+1))  *)
val FACT_EQ_PROD = store_thm(
  "FACT_EQ_PROD",
  ``!n. FACT n = PROD_SET (IMAGE SUC (count n))``,
  Induct_on `n` THEN1
  SRW_TAC [][PROD_SET_THM, FACT] THEN
  SRW_TAC [][PROD_SET_THM, FACT, COUNT_SUC] THEN
  `(SUC n) NOTIN (IMAGE SUC (count n))` by SRW_TAC [][] THEN
  METIS_TAC [DELETE_NON_ELEMENT]);

(* Theorem: n!/m! = product of (m+1) to n  *)
val FACT_REDUCTION = store_thm(
  "FACT_REDUCTION",
  ``!n m. m < n ==> (FACT n = PROD_SET (IMAGE SUC ((count n) DIFF (count m))) * (FACT m))``,
  Induct_on `n` THEN1
  SRW_TAC [][] THEN
  SRW_TAC [][FACT] THEN
  `m <= n` by DECIDE_TAC THEN
  Cases_on `m = n` THENL [
    SRW_TAC [][] THEN
    `count (SUC m) DIFF count m = {m}` by SRW_TAC [][DIFF_DEF] THENL [
      SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
      DECIDE_TAC,
      `PROD_SET (IMAGE SUC {m}) = SUC m` by SRW_TAC [][PROD_SET_THM] THEN
      METIS_TAC []
    ],
    `m < n` by DECIDE_TAC THEN
    `n NOTIN (count m)` by SRW_TAC [ARITH_ss][] THEN
    `n INSERT ((count n) DIFF (count m)) =
    (n INSERT (count n)) DIFF (count m)` by SRW_TAC [][INSERT_DIFF] THEN
    `_ = count (SUC n) DIFF (count m)` by SRW_TAC [ARITH_ss][EXTENSION] THEN
    `(SUC n) NOTIN (IMAGE SUC ((count n) DIFF (count m)))` by SRW_TAC [][] THEN
    `FINITE (IMAGE SUC ((count n) DIFF (count m)))` by SRW_TAC [][] THEN
    `PROD_SET (IMAGE SUC (count (SUC n) DIFF count m)) =
    (SUC n) * PROD_SET (IMAGE SUC (count n DIFF count m))`
    by METIS_TAC [PROD_SET_IMAGE_REDUCTION] THEN
    METIS_TAC [MULT_ASSOC]
  ]);

(* Theorem: (Generalized Euclid's Lemma)
            If prime p divides a PROD_SET, it divides a member of the PROD_SET *)
val PROD_SET_EUCLID = store_thm(
  "PROD_SET_EUCLID",
  ``!s. FINITE s ==> !p. prime p /\ divides p (PROD_SET s) ==> ?b. b IN s /\ divides p b``,
  HO_MATCH_MP_TAC FINITE_INDUCT THEN
  SRW_TAC [][] THEN1
  METIS_TAC [PROD_SET_EMPTY, DIVIDES_ONE, NOT_PRIME_1] THEN
  `PROD_SET (e INSERT s) = e * PROD_SET s`
    by METIS_TAC [PROD_SET_THM, DELETE_NON_ELEMENT] THEN
  Cases_on `divides p e` THEN1
  METIS_TAC [] THEN
  METIS_TAC [P_EUCLIDES]);

(* Theorem: (multiple gap)
   If n divides m, n cannot divide any x: m - n < x < m, or m < x < m + n *)
val MULTIPLE_INTERVAL = store_thm(
  "MULTIPLE_INTERVAL",
  ``!n m. divides n m ==> !x. m - n < x /\ x < m + n /\ divides n x ==> (x = m)``,
  REPEAT STRIP_TAC THEN
  `(?h. m = h*n) /\ (?k. x = k*n)` by METIS_TAC [divides_def] THEN
  `(h-1)*n < k*n` by METIS_TAC [RIGHT_SUB_DISTRIB, MULT_LEFT_1] THEN
  `k*n < (h+1)*n` by METIS_TAC [RIGHT_ADD_DISTRIB, MULT_LEFT_1] THEN
  `0 < n /\ h-1 < k /\ k < h+1` by METIS_TAC [LT_MULT_RCANCEL] THEN
  `h = k` by DECIDE_TAC THEN
  METIS_TAC []);

(* Theorem: If prime p divides n, p does not divide (n-1)!/(n-p)! *)
val prime_divisor_property = store_thm(
  "prime_divisor_property",
  ``!n p. 1 < n /\ p < n /\ prime p /\ divides p n ==>
    ~(divides p ((FACT (n-1)) DIV (FACT (n-p))))``,
  (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
  `1 < p` by METIS_TAC [ONE_LT_PRIME] THEN
  `n-p < n-1` by DECIDE_TAC THEN
  `(FACT (n-1)) DIV (FACT (n-p)) =
   PROD_SET (IMAGE SUC ((count (n-1)) DIFF (count (n-p))))`
    by METIS_TAC [FACT_REDUCTION, MULT_DIV, FACT_LESS] THEN
  `(count (n-1)) DIFF (count (n-p)) = {x | (n-p) <= x /\ x < (n-1)}`
    by SRW_TAC [ARITH_ss][EXTENSION, EQ_IMP_THM] THEN
  `IMAGE SUC {x | (n-p) <= x /\ x < (n-1)} = {x | (n-p) < x /\ x < n}`
    by SRW_TAC [ARITH_ss][EXTENSION, EQ_IMP_THM] THENL [
    Q.EXISTS_TAC `x-1` THEN
    DECIDE_TAC,
    `FINITE (count (n - 1) DIFF count (n - p))` by SRW_TAC [][] THEN
    `?y. y IN {x| n - p < x /\ x < n} /\ divides p y`
      by METIS_TAC [PROD_SET_EUCLID, IMAGE_FINITE] THEN
    `!m n y. y IN {x | m < x /\ x < n} ==> m < y /\ y < n` by SRW_TAC [][] THEN
    `n-p < y /\ y < n` by METIS_TAC [] THEN
    `y < n + p` by DECIDE_TAC THEN
    `y = n` by METIS_TAC [MULTIPLE_INTERVAL] THEN
    DECIDE_TAC
  ]);

(* Theorem: n divides C(n,k)  for all 0 < k < n ==> n is prime *)
val divides_binomials_imp_prime = store_thm(
  "divides_binomials_imp_prime",
  ``!n. 1 < n /\ (!k. 0 < k /\ k < n ==> divides n (binomial n k)) ==> prime n``,
  (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
  `?p. prime p /\ p < n /\ divides p n` by METIS_TAC [PRIME_FACTOR_PROPER] THEN
  `divides n (binomial n p)` by METIS_TAC [PRIME_POS] THEN
  `0 < p` by METIS_TAC [PRIME_POS] THEN
  `(n = n-p + p) /\ (n-p) < n` by DECIDE_TAC THEN
  `FACT n = (binomial n p) * (FACT (n-p) * FACT p)` by METIS_TAC [binomial_formula] THEN
  `(n = SUC (n-1)) /\ (p = SUC (p-1))` by DECIDE_TAC THEN
  `(FACT n = n * FACT (n-1)) /\ (FACT p = p * FACT (p-1))` by METIS_TAC [FACT] THEN
  `n * FACT (n-1) = (binomial n p) * (FACT (n-p) * (p * FACT (p-1)))` by METIS_TAC [] THEN
  `0 < n` by DECIDE_TAC THEN
  `?q. binomial n p = n * q` by METIS_TAC [divides_def, MULT_COMM] THEN
  `0 <> n` by DECIDE_TAC THEN
  `FACT (n-1) = q * (FACT (n-p) * (p * FACT (p-1)))`
    by METIS_TAC [EQ_MULT_LCANCEL, MULT_ASSOC] THEN
  `_ = q * ((FACT (p-1) * p)* FACT (n-p))` by METIS_TAC [MULT_COMM] THEN
  `_ = q * FACT (p-1) * p * FACT (n-p)` by METIS_TAC [MULT_ASSOC] THEN
  `FACT (n-1) DIV FACT (n-p) = q * FACT (p-1) * p` by METIS_TAC [MULT_DIV, FACT_LESS] THEN
  METIS_TAC [divides_def, prime_divisor_property]);

(* Theorem: n is prime iff n divides C(n,k)  for all 0 < k < n *)
val prime_iff_divides_binomials = store_thm(
  "prime_iff_divides_binomials",
  ``!n. prime n <=> 1 < n /\ (!k. 0 < k /\ k < n ==> divides n (binomial n k))``,
  METIS_TAC [prime_divides_binomials, divides_binomials_imp_prime]);

(* ------------------------------------------------------------------------- *)
(* List Summation.                                                           *)
(* ------------------------------------------------------------------------- *)

(* Defined: SUM for summation of list = sequence *)

(* Theorem: SUM [] = 0 *)
val SUM_NIL = save_thm("SUM_NIL", SUM |> CONJUNCT1);

(* Theorem: constant multiplication: k SUM S = SUM (k*S)  *)
val SUM_MULT = store_thm(
  "SUM_MULT",
  ``!s k. k * SUM s = SUM (MAP (\x. k*x) s)``,
  Induct_on `s` THEN1
  METIS_TAC [SUM, MAP, MULT_0] THEN
  METIS_TAC [SUM, MAP, LEFT_ADD_DISTRIB]);

(* Theorem: (m+n) * SUM s = SUM (m*s) + SUM (n*s)  *)
val SUM_RIGHT_ADD_DISTRIB = store_thm(
  "SUM_RIGHT_ADD_DISTRIB",
  ``!s m n. (m + n) * SUM s = SUM (MAP (\x. m*x) s) + SUM (MAP (\x. n*x) s)``,
  METIS_TAC [RIGHT_ADD_DISTRIB, SUM_MULT]);

(* Theorem: SUM (k=0..n) f(k) = f(0) + SUM (k=1..n) f(k)  *)
val SUM_DECOMPOSE_FIRST = store_thm(
  "SUM_DECOMPOSE_FIRST",
  ``!f n. SUM (GENLIST f (SUC n)) = f 0 + SUM (GENLIST (f o SUC) n)``,
  METIS_TAC [GENLIST_CONS, SUM]);

(* Theorem: SUM (k=0..n) f(k) = SUM (k=0..(n-1)) f(k) + f n *)
val SUM_DECOMPOSE_LAST = store_thm(
  "SUM_DECOMPOSE_LAST",
  ``!f n. SUM (GENLIST f (SUC n)) = SUM (GENLIST f n) + f n``,
  REPEAT STRIP_TAC THEN
  `SUM (GENLIST f (SUC n)) = SUM (SNOC (f n) (GENLIST f n))` by METIS_TAC [GENLIST] THEN
  `_ = SUM ((GENLIST f n) ++ [f n])` by METIS_TAC [SNOC_APPEND] THEN
  `_ = SUM (GENLIST f n) + SUM [f n]` by METIS_TAC [SUM_APPEND] THEN
  SRW_TAC [][SUM]);

(* Theorem: 0 < n ==> SUM (GENLIST f (SUC n)) = f 0 + SUM (GENLIST (f o SUC) (PRE n)) + f n *)
val SUM_DECOMPOSE_FIRST_LAST = store_thm(
  "SUM_DECOMPOSE_FIRST_LAST",
  ``!f n. 0 < n ==> (SUM (GENLIST f (SUC n)) = f 0 + SUM (GENLIST (f o SUC) (PRE n)) + f n)``,
  METIS_TAC [SUM_DECOMPOSE_LAST, SUM_DECOMPOSE_FIRST, LESS_EQ_SUC, PRE_SUC_EQ]);

(* Theorem: Binomial Index Shifting, for
     SUM (k=1..n) C(n,k)x^(n+1-k)y^k = SUM (k=0..n-1) C(n,k+1)x^(n-k)y^(k+1) *)
val GENLIST_binomial_index_shift = store_thm(
  "GENLIST_binomial_index_shift",
  ``!n x y. GENLIST ((\k. binomial n k * x ** SUC(n - k) * y ** k) o SUC) n =
            GENLIST (\k. binomial n (SUC k) * x**(n-k) * y**(SUC k)) n``,
  SRW_TAC [][GENLIST_FUN_EQ] THEN
  `SUC (n - SUC k) = n - k` by DECIDE_TAC THEN
  RW_TAC std_ss []);

(* This is closely related to above, with (SUC n) replacing (n),  but does not require k < n. *)
val binomial_index_shift = store_thm(
  "binomial_index_shift",
  ``!n x y. (\k. binomial (SUC n) k * x**((SUC n) - k) * y**k) o SUC =
           (\k. binomial (SUC n) (SUC k) * x**(n-k) * y**(SUC k))``,
  SRW_TAC [][FUN_EQ_THM]);

(* Theorem: SUM (GENLIST a n) + SUM (GENLIST b n) = SUM (GENLIST (\k. a k + b k) n) *)
val SUM_ADD_GENLIST = store_thm(
  "SUM_ADD_GENLIST",
  ``!a b n. SUM (GENLIST a n) + SUM (GENLIST b n) = SUM (GENLIST (\k. a k + b k) n)``,
  Induct_on `n` THEN1
  SRW_TAC [][] THEN
  SRW_TAC [][SUM_DECOMPOSE_LAST] THEN
  `SUM (GENLIST a n) + a n + (SUM (GENLIST b n) + b n) =
    SUM (GENLIST a n) + SUM (GENLIST b n) + (a n + b n)` by DECIDE_TAC THEN
  METIS_TAC []);

(* Theorem: SUM (GENLIST a n ++ GENLIST b n) = SUM (GENLIST (\k. a k + b k) n) *)
val SUM_GENLIST_APPEND = store_thm(
  "SUM_GENLIST_APPEND",
  ``!a b n. SUM (GENLIST a n ++ GENLIST b n) = SUM (GENLIST (\k. a k + b k) n)``,
  METIS_TAC [SUM_APPEND, SUM_ADD_GENLIST]);

(* Theorem: (SUM l) MOD n = (SUM (MAP (\x. x MOD n) l)) MOD n *)
val MOD_SUM = store_thm(
  "MOD_SUM",
  ``!n. 0 < n ==> !l. (SUM l) MOD n = (SUM (MAP (\x. x MOD n) l)) MOD n``,
  REPEAT STRIP_TAC THEN
  Induct_on `l` THEN1
  SRW_TAC [][] THEN
  REPEAT STRIP_TAC THEN
  `SUM (h::l) MOD n = (h MOD n + (SUM l) MOD n) MOD n` by RW_TAC std_ss [SUM, MOD_PLUS] THEN
  `_ = ((h MOD n) MOD n + SUM (MAP (\x. x MOD n) l) MOD n) MOD n` by RW_TAC std_ss [MOD_MOD] THEN
  SRW_TAC [][MOD_PLUS]);

(* Theorem: SUM l = 0 <=> l = EVERY (\x. x = 0) l *)
val SUM_EQ_0 = store_thm(
  "SUM_EQ_0",
  ``!l. (SUM l = 0) <=> EVERY (\x. x = 0) l``,
  Induct THEN
  SRW_TAC [][]);

(* Theorem: SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n =
            SUM (GENLIST ((\k. f k MOD n) o SUC) (PRE n)) MOD n *)
val SUM_GENLIST_MOD = store_thm(
  "SUM_GENLIST_MOD",
  ``!n. 0 < n ==> !f. SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n = SUM (GENLIST ((\k. f k MOD n) o SUC) (PRE n)) MOD n``,
  REPEAT STRIP_TAC THEN
  `SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n =
    SUM (MAP (\x. x MOD n) (GENLIST ((\k. f k) o SUC) (PRE n))) MOD n` by METIS_TAC [MOD_SUM] THEN
  RW_TAC std_ss [MAP_GENLIST, combinTheory.o_ASSOC, combinTheory.o_ABS_L]);

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Binomial Theorem                                                          *)
(* ------------------------------------------------------------------------- *)

(* Theorem: multiply x into a binomial term *)
val binomial_term_merge_x = store_thm(
  "binomial_term_merge_x",
  ``!n x y. (\k. x*k) o (\k. binomial n k * x ** (n - k) * y ** k) =
           (\k. binomial n k * x ** (SUC(n - k)) * y ** k)``,
  SRW_TAC [][FUN_EQ_THM] THEN
  `x * (binomial n k * x ** (n - k) * y ** k) =
    binomial n k * (x * x ** (n - k)) * y ** k` by DECIDE_TAC THEN
  METIS_TAC [EXP]);

(* Theorem: multiply y into a binomial term *)
val binomial_term_merge_y = store_thm(
  "binomial_term_merge_y",
  ``!n x y. (\k. y*k) o (\k. binomial n k * x ** (n - k) * y ** k) =
           (\k. binomial n k * x ** (n - k) * y ** (SUC k))``,
  SRW_TAC [][FUN_EQ_THM] THEN
  `y * (binomial n k * x ** (n - k) * y ** k) =
    binomial n k * x ** (n - k) * (y * y ** k)` by DECIDE_TAC THEN
  METIS_TAC [EXP]);

(* Theorem: [Binomial Theorem]  (x + y)^n = SUM (k=0..n) C(n,k)x^(n-k)y^k  *)
val binomial_thm = store_thm(
  "binomial_thm",
  ``!n x y. (x + y)**n = SUM (GENLIST (\k. (binomial n k)* x**(n-k) * y**k) (SUC n))``,
  Induct_on `n` THEN1
  SRW_TAC [][EXP, binomial_n_n] THEN
  SRW_TAC [][EXP] THEN
  `(x + y) * SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** k) (SUC n)) =
    x * SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** k) (SUC n)) +
    y * SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** k) (SUC n))`
    by METIS_TAC [RIGHT_ADD_DISTRIB] THEN
  `_ = SUM (GENLIST ((\k. x*k) o (\k. binomial n k * x ** (n - k) * y ** k)) (SUC n)) +
        SUM (GENLIST ((\k. y*k) o (\k. binomial n k * x ** (n - k) * y ** k)) (SUC n))`
    by METIS_TAC [SUM_MULT, MAP_GENLIST] THEN
  `_ = SUM (GENLIST (\k. binomial n k * x ** SUC(n - k) * y ** k) (SUC n)) +
        SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) (SUC n))`
    by SRW_TAC [][binomial_term_merge_x, binomial_term_merge_y] THEN
  `_ = (\k. binomial n k * x ** SUC (n - k) * y ** k) 0 +
         SUM (GENLIST ((\k. binomial n k * x ** SUC (n - k) * y ** k) o SUC) n) +
        SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) (SUC n))`
    by SRW_TAC [][SUM_DECOMPOSE_FIRST] THEN
  `_ = (\k. binomial n k * x ** SUC (n - k) * y ** k) 0 +
         SUM (GENLIST ((\k. binomial n k * x ** SUC (n - k) * y ** k) o SUC) n) +
        (SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n) +
         (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n )`
    by SRW_TAC [][SUM_DECOMPOSE_LAST] THEN
  `_ = (\k. binomial n k * x ** SUC(n - k) * y ** k) 0 +
         SUM (GENLIST (\k. binomial n (SUC k) * x ** (n - k) * y ** (SUC k)) n) +
        (SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n) +
         (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n )`
    by METIS_TAC [GENLIST_binomial_index_shift] THEN
  `_ = (\k. binomial n k * x ** SUC(n - k) * y ** k) 0 +
        (SUM (GENLIST (\k. binomial n (SUC k) * x ** (n - k) * y ** (SUC k)) n) +
         SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n)) +
         (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n`
    by DECIDE_TAC THEN
  `_ = (\k. binomial n k * x ** SUC (n - k) * y ** k) 0 +
        SUM (GENLIST (\k. (binomial n (SUC k) * x ** (n - k) * y ** (SUC k) +
                           binomial n k * x ** (n - k) * y ** (SUC k))) n) +
        (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n`
    by METIS_TAC [SUM_ADD_GENLIST] THEN
  `_ = (\k. binomial n k * x ** SUC(n - k) * y ** k) 0 +
        SUM (GENLIST (\k. (binomial n (SUC k) + binomial n k) * x ** (n - k) * y ** (SUC k)) n) +
        (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n`
    by SRW_TAC [][RIGHT_ADD_DISTRIB, MULT_ASSOC] THEN
  `_ = (\k. binomial n k * x ** SUC(n - k) * y ** k) 0 +
        SUM (GENLIST (\k. binomial (SUC n) (SUC k) * x ** (n - k) * y ** (SUC k)) n) +
        (\k. binomial n k * x ** (n - k) * y ** (SUC k)) n`
    by SRW_TAC [][binomial_recurrence, ADD_COMM] THEN
  `_ = binomial (SUC n) 0 * x ** (SUC n) * y ** 0 +
        SUM (GENLIST (\k. binomial (SUC n) (SUC k) * x ** (n - k) * y ** (SUC k)) n) +
        binomial (SUC n) (SUC n) * x ** 0 * y ** (SUC n)`
        by SRW_TAC [][binomial_n_0, binomial_n_n] THEN
  `_ = binomial (SUC n) 0 * x ** (SUC n) * y ** 0 +
        SUM (GENLIST ((\k. binomial (SUC n) k * x ** ((SUC n) - k) * y ** k) o SUC) n) +
        binomial (SUC n) (SUC n) * x ** 0 * y ** (SUC n)`
        by SRW_TAC [][binomial_index_shift] THEN
  `_ = SUM (GENLIST (\k. binomial (SUC n) k * x ** (SUC n - k) * y ** k) (SUC n)) +
        (\k. binomial (SUC n) k * x ** (SUC n - k) * y ** k) (SUC n)`
        by SRW_TAC [][SUM_DECOMPOSE_FIRST] THEN
  `_ = SUM (GENLIST (\k. binomial (SUC n) k * x ** (SUC n - k) * y ** k) (SUC (SUC n)))`
        by SRW_TAC [][SUM_DECOMPOSE_LAST] THEN
  DECIDE_TAC);

(* ------------------------------------------------------------------------- *)
(* Binomial Theorem for prime exponent and modulo.                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: !k. 0 < k /\ k < n ==> (binomial n k MOD n = 0) <=>
            !h. 0 <= h /\ h < PRE n ==> (binomial n (SUC h) MOD n = 0) *)
val binomial_range_shift = store_thm(
  "binomial_range_shift",
  ``!n . 0 < n ==> ((!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
                   (!h. h < PRE n ==> ((binomial n (SUC h)) MOD n = 0)))``,
  RW_TAC std_ss [EQ_IMP_THM] THENL [
    `0 < SUC h /\ SUC h < n` by DECIDE_TAC THEN
    RW_TAC std_ss [],
    `k <> 0` by DECIDE_TAC THEN
    `?h. k = SUC h` by METIS_TAC [num_CASES] THEN
    `h < PRE n` by DECIDE_TAC THEN
    RW_TAC std_ss []
  ]);

(* Theorem: binomial n k MOD n = 0 <=> (binomial n k * x ** (n-k) * y ** k) MOD n = 0 *)
val binomial_mod_zero = store_thm(
  "binomial_mod_zero",
  ``!n. 0 < n ==> !k. (binomial n k MOD n = 0) <=> (!x y. (binomial n k * x ** (n-k) * y ** k) MOD n = 0)``,
  RW_TAC std_ss [EQ_IMP_THM] THEN1
  METIS_TAC [MOD_TIMES2, ZERO_MOD, MULT] THEN
  METIS_TAC [EXP_1, MULT_RIGHT_1]);

(* Theorem: (!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0))) <=>
            (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))) *)
val binomial_range_shift' = store_thm(
  "binomial_range_shift'",
  ``!n . 0 < n ==> ((!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0))) <=>
                   (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))))``,
  RW_TAC std_ss [EQ_IMP_THM] THENL [
    `0 < SUC h /\ SUC h < n` by DECIDE_TAC THEN
    RW_TAC std_ss [],
    `k <> 0` by DECIDE_TAC THEN
    `?h. k = SUC h` by METIS_TAC [num_CASES] THEN
    `h < PRE n` by DECIDE_TAC THEN
    RW_TAC std_ss []
  ]);

(* Theorem: !k. 0 < k /\ k < n ==> (binomial n k) MOD n = 0 <=>
            !x y. SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0 *)
val binomial_mod_zero' = store_thm(
  "binomial_mod_zero'",
  ``!n. 0 < n ==> ((!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
                  !x y. SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0)``,
  REPEAT STRIP_TAC THEN
  `!x y. (\k. (binomial n (SUC k) * x ** (n - SUC k) * y ** (SUC k)) MOD n) =
         (\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC` by RW_TAC std_ss [FUN_EQ_THM] THEN
  `(!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
   (!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0)))`
     by RW_TAC std_ss [binomial_mod_zero] THEN
  `_ = (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0)))`
    by RW_TAC std_ss [binomial_range_shift'] THEN
  `_ = !x y h. h < PRE n ==> (((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))`
    by METIS_TAC [] THEN
  RW_TAC std_ss [EVERY_GENLIST, SUM_EQ_0]);

(* Theorem: [Binomial Expansion for prime exponent]  (x + y)^p = x^p + y^p (mod p) *)
val binomial_thm_prime = store_thm(
  "binomial_thm_prime",
  ``!p. prime p ==> (!x y. (x + y)**p MOD p = (x**p + y**p) MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by RW_TAC std_ss [PRIME_POS] THEN
  `!k. 0 < k /\ k < p ==> ((binomial p k) MOD p  = 0)` by METIS_TAC [prime_iff_divides_binomials, DIVIDES_MOD_0] THEN
  `SUM (GENLIST ((\k. binomial p k * x ** (p - k) * y ** k) o SUC) (PRE p)) MOD p = 0`
    by METIS_TAC [SUM_GENLIST_MOD, binomial_mod_zero', ZERO_MOD] THEN
  `(x + y) ** p MOD p =
   (x ** p + SUM (GENLIST ((\k. binomial p k * x ** (p - k) * y ** k) o SUC) (PRE p)) + y ** p) MOD p`
    by RW_TAC std_ss [binomial_thm, SUM_DECOMPOSE_FIRST_LAST, binomial_n_0, binomial_n_n, EXP] THEN
  METIS_TAC [MOD_PLUS3, ADD_0, MOD_PLUS]);

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem                                                   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (Fermat's Little Theorem by Binomial) For prime p, a**p = a (mod p). *)
(* Proof: by induction on a.
   Base case: 0 ** p MOD p = 0 MOD p
      true by
   Step case: a ** p MOD p = a MOD p ==> SUC a ** p MOD p = SUC a MOD p
     SUC a ** p MOD p
   = (a + 1) ** p MOD p                     by ADD1
   = (a ** p + 1 ** p) MOD p                by binomial_thm_prime
   = (a ** p MOD p + 1 ** p MOD p) MOD p    by MOD_PLUS
   = (a MOD p + 1 ** p MOD p) MOD p         by induction hypothesis
   = (a MOD p + 1 MOD p) MOD p              by EXP_1
   = (a + 1) MOD p                          by MOD_PLUS
   = SUC a MOD p                            by ADD1
*)
val FERMAT_LITTLE_THM = store_thm(
  "FERMAT_LITTLE_THM",
  ``!p a. prime p ==> (a**p MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by RW_TAC std_ss [PRIME_POS] THEN
  `p <> 0` by DECIDE_TAC THEN
  Induct_on `a` THEN1
  RW_TAC std_ss [ZERO_EXP] THEN
  METIS_TAC [binomial_thm_prime, MOD_PLUS, EXP_1, ADD1]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
