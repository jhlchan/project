(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Number-theoretic Proof.                         *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Number Theory)
=======================================
Equivalent statements of Feramt's Little Theorem:
#0. For prime p, 0 < a < p, a**(p-1) = 1 MOD p.
#1. For prime p, 0 < a < p, a**p = a MOD p.
#2. For prime p, 0 < a < p, (a**p - a) = 0 MOD p.
#3. For prime p, 0 < a < p, p divides (a**p - a).
#4. For prime p, 0 < a < p, 0 < (a**(p-2) MOD p) < p and
                           ((a**(p-2) MOD p) * a) MOD p = 1.

The first one (#0) will be called Fermat's Identity.

There is a number-theoretic proof using Group Z*p, via Lagrange Theorem.

Number-theoretic Proof:

For a prime p, consider the set of residue p = { i | 0 < i < p }.

This set is non-empty since prime p > 1.

This is a proof via Z*p being a group:

   For prime p, there is a multiplication modulo group Z*p.

   Being a finite group, any element 0 < a < p in Z*p has an order m
   such that 0 < m and a**m MOD p = 1.

   This order also equals the size of the generated subgroup <a>.

   By Lagrange's Theorem, the subgroup size m divides the size of group Z*p.

   Since CARD (Z*p).carrier = (p-1), and (p-1) = m*k, we have:
     a**(p-1) MOD p
   = a**(m*k) MOD p
   = (a**m)**k MOD p
   = (1)**k MOD p
   = 1

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTmultiplicative";

(* ------------------------------------------------------------------------- *)

(* load existing theories *)
(* val _ = load "gcdTheory"; *)

(* Get dependent theories in lib *)
open helperSetTheory;
(* Get dependent theories in group *)
open groupTheory subgroupTheory groupInstancesTheory;

(* ------------------------------------------------------------------------- *)
(* Group-theoretic Proof appplied to Z*p and E*n.                            *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem (by Zp multiplicative group)                      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For prime p, 0 < a < p,  a**(p-1) = 1 (mod p) *)
(* Proof:
   Since 0 < a < p, a IN (Zstar p).carrier.
   Since (Zstar p) is FINITE, there exists m = order (Zstar p) a.
   We have   a**m = #e = 1    by group_order_property.
   By Lagrange's Theorem, m divides CARD (Zstar p).carrier = (p-1).
   Or  (p-1) = m*k  for some k, by divides_def.
   Hence  a ** (p-1)
        = a**(m*k)
        = (a**m)**k    by group_exp_mult
        = (#e)**k
        = #e           by group_id_exp
        = 1            in (Zstar p)
   or a ** (p-1) = 1  (mod p).

   i.e. apply FINITE_GROUP_FERMAT to Z*p.
*)
val FERMAT_LITTLE8A = store_thm(
  "FERMAT_LITTLE8A",
  ``!p a. prime p /\ 0 < a /\ a < p ==> (a**(p-1) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `Group (Zstar p)` by SRW_TAC [][Zstar_group] THEN
  `FiniteGroup (Zstar p)` by SRW_TAC [][FiniteGroup_def, FINITE_Zstar_carrier] THEN
  `CARD (Zstar p).carrier = p-1` by METIS_TAC [dividesTheory.PRIME_POS, CARD_Zstar_carrier] THEN
  `a IN (Zstar p).carrier` by SRW_TAC [][Zstar_def, residue_def] THEN
  `(Zstar p).id = 1` by SRW_TAC [][Zstar_def] THEN
  METIS_TAC [FINITE_GROUP_FERMAT, Zstar_exp]);

(* Theorem: For all a in Estar n, a**(totient n) MOD n = 1 *)
val EULER_FERMAT = store_thm(
  "EULER_FERMAT",
  ``!n a. 1 < n /\ 0 < a /\ a < n /\ (gcd n a = 1) ==> (a**(totient n) MOD n = 1)``,
  REPEAT STRIP_TAC THEN
  `FiniteGroup (Estar n)` by SRW_TAC [][Estar_group, FINITE_Estar_carrier, FiniteGroup_def] THEN
  `CARD (Estar n).carrier = totient n` by SRW_TAC [][Estar_def, totient_def] THEN
  `a IN (Estar n).carrier /\ ((Estar n).id = 1)` by SRW_TAC [][Estar_def, Euler_def] THEN
  `(Estar n).id = 1` by SRW_TAC [][Estar_def] THEN
  METIS_TAC [FINITE_GROUP_FERMAT, Estar_exp]);

(* REPEAT from FLTeulerScript *)

(* Theorem: For prime p, (Euler p = residue p) *)
(* Proof:
   Essentially this is to prove:
   For prime p, gcd p x = 1   for 0 < x < p.
   Since x < p, x does not divide p, result follows by PRIME_GCD.
*)
val PRIME_EULER = store_thm(
  "PRIME_EULER",
  ``!p. prime p ==> (Euler p = residue p)``,
  SRW_TAC [][Euler_def, residue_def, pred_setTheory.EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD]);

(* Theorem: For prime p, totient p = p-1 *)
(* Proof:
   totient p = CARD (Euler p)    by definition
             = CARD (residue p)  by PRIME_EULER
             = p-1               by CARD_RESIDUE, and prime p > 0.
*)
val PRIME_TOTIENT = store_thm(
  "PRIME_TOTIENT",
  ``!p. prime p ==> (totient p = p - 1)``,
  METIS_TAC [totient_def, PRIME_EULER, CARD_RESIDUE, dividesTheory.PRIME_POS]);

(* Theorem: For prime p, 0 < a < p ==> a ** (p-1) MOD p = 1 *)
(* Proof:
   For prime p,  gcd p a = 1           by PRIME_GCD, NOT_LT_DIVIDES
   Hence  (a**(totient p) MOD p = 1)   by EULER_FERMAT, prime p > 1.
   or      a**(p-1) MOD p = 1          by PRIME_TOTIENT
*)
val FERMAT_LITTLE8B = store_thm(
  "FERMAT_LITTLE8B",
  ``!p a. prime p /\ 0 < a /\ a < p ==> (a**(p-1) MOD p = 1)``,
  METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD,
             dividesTheory.ONE_LT_PRIME, EULER_FERMAT, PRIME_TOTIENT]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
