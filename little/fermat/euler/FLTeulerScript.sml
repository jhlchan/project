(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Number-theoretic Proof.                         *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem from Euler's Generalization
===================================================
By defining           Estar n = all numbers x from 1 to (n-1) with gcd (n x) = 1.
It can be shown that  Estar n is a finite Abelian group under multiplication MOD n.
Thus for any a IN Estar n,  a ** (CARD Estar n) MOD n = 1  by FINITE_ABELIAN_FERMAT.
Euler defines         totient n = CARD Estar n.
When n = prime p,     totient p = (p-1)   since all a < p has gcd p a = 1.
so giving Fermat's Little Theorem:  a ** (p-1) MOD p = 1.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTeuler";

(* ------------------------------------------------------------------------- *)

(* load existing theories *)
(* val _ = load "gcdTheory"; *)
(* val _ = load "dividesTheory"; *)
(* val _ = load "helperNumTheory"; *)
(* val _ = load "helperSetTheory"; *)
(* val _ = load "groupTheory"; *)

(* Get dependent theories in lib *)
open helperNumTheory helperSetTheory;
(* Get dependent theories in group *)
open groupTheory subgroupTheory groupInstancesTheory;

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Number-theoretic Proof with Group Theory.                                 *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem (by Zp finite abelian group)                      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For prime p, 0 < a < p,  a**(p-1) = 1 (mod p) *)
(* Proof:
   Since 0 < a < p, a IN (Zstar p).carrier,
   and (Zstar p) is a FiniteAbelian Group, by Zstar_finite_abelian_group
   and CARD (Zstar p).carrier = (p-1), by CARD_Zstar_carrier.
   this follows by FINITE_ABELIAN_FERMAT and Zstar_exp, which relates group_exp to arithmeticTheory.EXP.
*)
val FERMAT_LITTLE9 = store_thm(
  "FERMAT_LITTLE9",
  ``!p a. prime p /\ 0 < a /\ a < p ==> (a**(p-1) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `FiniteAbelianGroup (Zstar p)` by SRW_TAC [][Zstar_finite_abelian_group] THEN
  `a IN (Zstar p).carrier /\ ((Zstar p).id = 1)` by SRW_TAC [][Zstar_def, residue_def] THEN
  `CARD (Zstar p).carrier = p-1` by METIS_TAC [dividesTheory.PRIME_POS, CARD_Zstar_carrier] THEN
  METIS_TAC [FINITE_ABELIAN_FERMAT, Zstar_exp]);

(* ------------------------------------------------------------------------- *)
(* Euler's generalization of Fermat's Theorem                                *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For all a in Estar n, a**(totient n) MOD n = 1 *)
val EULER_FERMAT = store_thm(
  "EULER_FERMAT",
  ``!n a. 1 < n /\ 0 < a /\ a < n /\ (gcd n a = 1) ==> (a**(totient n) MOD n = 1)``,
  REPEAT STRIP_TAC THEN
  `FiniteAbelianGroup (Estar n)` by SRW_TAC [][Estar_finite_abelian_group] THEN
  `a IN (Estar n).carrier /\ ((Estar n).id = 1)` by SRW_TAC [][Estar_def, Euler_def] THEN
  `CARD (Estar n).carrier = totient n` by SRW_TAC [][Estar_def, totient_def] THEN
  METIS_TAC [FINITE_ABELIAN_FERMAT, Estar_exp]);

(* Theorem: For prime p, (Euler p = residue p) *)
(* Proof:
   Essentially this is to prove:
   For prime p, gcd p x = 1   for 0 < x < p.
   Since x < p, x does not divide p, result follows by PRIME_GCD.
*)
val PRIME_EULER = store_thm(
  "PRIME_EULER",
  ``!p. prime p ==> (Euler p = residue p)``,
  SRW_TAC [][Euler_def, residue_def, pred_setTheory.EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD]);

(* Theorem: For prime p, totient p = p-1 *)
(* Proof:
   totient p = CARD (Euler p)    by definition
             = CARD (residue p)  by PRIME_EULER
             = p-1               by CARD_RESIDUE, and prime p > 0.
*)
val PRIME_TOTIENT = store_thm(
  "PRIME_TOTIENT",
  ``!p. prime p ==> (totient p = p - 1)``,
  METIS_TAC [totient_def, PRIME_EULER, CARD_RESIDUE, dividesTheory.PRIME_POS]);

(* Theorem: For prime p, 0 < a < p ==> a ** (p-1) MOD p = 1 *)
(* Proof (version 10):
   For prime p,  gcd p a = 1           by PRIME_GCD, NOT_LT_DIVIDES
   Hence  (a**(totient p) MOD p = 1)   by EULER_FERMAT, prime p > 1.
   or      a**(p-1) MOD p = 1          by PRIME_TOTIENT
*)
val FERMAT_LITTLE10 = store_thm(
  "FERMAT_LITTLE10",
  ``!p a. prime p /\ 0 < a /\ a < p ==> (a**(p-1) MOD p = 1)``,
  METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD,
             dividesTheory.ONE_LT_PRIME, EULER_FERMAT, PRIME_TOTIENT]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
