(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Group-theoretic Proof.                          *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Group Theory)
======================================
Given a finite group G, consider an element a in G.

Since G is finite, element a has an order: (order a), and a^(order a) = e.

This also means that, the generated subgroup <a> has size (order a).

By Lagrange identity, size of group = k * size of subgroup.

Hence, |G| = k * |<a>|, and a^|<a>| = e.

This implies:   a^|G| = a^(k*|<a>|) = a^(|<a>*k) = (a^|<a>|)^k = e^k = e.

This is the group equivalent of Fermat's Little Theorem.

By putting G = Z*p, a IN Z*p means 0 < a < p,
then a^|G| mod p = 1, or a^(p-1) mod p = 1.

By putting G = Phi*n = {a | a < n /\ gcd(a,n) = 1 },
then a^|G| mod n = 1, or a^phi(n) mod n = 1.

which is Euler's generalization of Fermat's Little Theorem.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "AllFLTgroup";

(* open dependent theories *)
open pred_setTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* Theorem: If n > 0, (m MOD n = 0) <=> divides n m *)
val MOD_0_DIVIDES = store_thm(
  "MOD_0_DIVIDES",
  ``!n m. 0 < n ==> ((m MOD n = 0) <=> divides n m)``,
  SRW_TAC [][dividesTheory.divides_def, EQ_IMP_THM] THEN
  METIS_TAC [arithmeticTheory.DIVISION, arithmeticTheory.ADD_0, arithmeticTheory.MOD_EQ_0]);

(* Theorem: mutliplication is associative in MOD *)
val MOD_MULT_ASSOC = store_thm(
  "MOD_MULT_ASSOC",
  ``!n x y z. 0 < n /\ x < n /\ z < n ==> (((x * y) MOD n * z) MOD n = (x * (y * z) MOD n) MOD n)``,
  REPEAT STRIP_TAC THEN
  `((x * y) MOD n * z) MOD n = ((x * y) MOD n * z MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (((x * y) * z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((x * (y * z)) MOD n) MOD n` by RW_TAC arith_ss [] THEN
  `_ = (x MOD n * (y * z) MOD n) MOD n` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
  RW_TAC arith_ss []);

(* Theorem: If !e IN s, CARD e = k, SIGMA CARD s = CARD s * k. *)
val SIGMA_CARD_CONSTANT = store_thm(
  "SIGMA_CARD_CONSTANT",
  ``!k s. FINITE s ==> (!e. e IN s ==> (CARD e = k)) ==> (SIGMA CARD s = k * (CARD s))``,
  STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][] THEN1 METIS_TAC [SUM_IMAGE_THM] THEN
  `CARD e = k` by SRW_TAC [][] THEN
  `SIGMA CARD (e INSERT s) = CARD e + SIGMA CARD (s DELETE e)` by SRW_TAC [][SUM_IMAGE_THM] THEN
  `s DELETE e = s` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `SUC (CARD s) = 1 + CARD s` by RW_TAC arith_ss [] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  RW_TAC arith_ss []);

(* Part 2: General Theory -------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Theory of Groups.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Set up group type as a record
   A Group has:
   . a carrier set (set = function 'a -> bool, since MEM is a boolean function)
   . an identity element
   . an inverse function (unary operation)
   . a product function called multiplication (binary operation)
*)
val _ = Hol_datatype`
  group = <| carrier: 'a -> bool;
                  id: 'a;
                 inv: 'a -> 'a;
                mult: 'a -> 'a -> 'a
           |>`;

(* set overloading  *)
val _ = overload_on ("*", ``g.mult``);
val _ = overload_on ("|/", ``g.inv``);
val _ = overload_on ("#e", ``g.id``);

(* Group definition:
   A Group is a set with elements of g of type 'a group, such that
   . g.id is in the carrier set (e in G)
   . (g.mult x y) is in the carrier set (closure: x*y in G)
   . (g.inv x) is in the carrier set (existence of inverse: x^-1 in G)
   . (g.mult (g.inv x) x) = (g.id)  (property of inverse and identity: (x^-1)*(x) = e)
   . (g.mult (g.mult x y) z) = (g.mult x (g.mult y z)) (associativity: (x*y)*z = x*(y*z))
*)
(* Define Group by predicate *)
val Group_def = Define`
  Group (g: 'a group) =
    #e IN g.carrier /\
    (!x y:: (g.carrier). x * y IN g.carrier) /\
    (!x:: (g.carrier). |/x IN g.carrier) /\
    (!x:: (g.carrier). #e * x = x) /\
    (!x:: (g.carrier). |/x * x = #e) /\
    (!x y z:: (g.carrier). (x * y) * z = x * (y * z))
  `;

(* Finite Group: a Group with a finite carrier set. *)
val FiniteGroup_def = Define`
  FiniteGroup (g: 'a group) =
    Group g /\ FINITE g.carrier
  `;

(* ------------------------------------------------------------------------- *)
(* Basic theorem from definition.                                            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Group identity is an element. *)
val group_id_carrier = store_thm(
  "group_id_carrier",
  ``!g. Group g ==> #e IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: Group inverse is an element. *)
val group_inv_carrier = store_thm(
  "group_inv_carrier",
  ``!g. Group g ==> !x :: (g.carrier). |/ x IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group closure] Group product is an element. *)
val group_mult_carrier = store_thm(
  "group_mult_carrier",
  ``!g. Group g ==> !x y :: (g.carrier). x * y IN g.carrier``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left Identity] e x = x *)
val group_lid = store_thm(
  "group_lid",
  ``!g. Group g ==> !x :: (g.carrier). #e * x = x``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group left inverse] x' x = e *)
val group_linv = store_thm(
  "group_linv",
  ``!g. Group g ==> !x :: (g.carrier). |/x * x = #e``,
  SRW_TAC [][Group_def]);

(* Theorem: [Group associativity] (x y) z = x (y z) *)
val group_assoc = store_thm(
  "group_assoc",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y) * z = x * (y * z)``,
  SRW_TAC [][Group_def]);

(* Export rewrites after using Group_def, don't use Group_def anymore *)
val _ = export_rewrites ["group_id_carrier", "group_inv_carrier", "group_mult_carrier"];

(* ------------------------------------------------------------------------- *)
(* Theorems in basic Group Theory.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Group right inverse] x x' = e *)
val group_rinv = store_thm(
  "group_rinv",
  ``!g. Group g ==> !x :: (g.carrier). x * |/x = #e``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * |/x = #e * (x * |/x)`       by SRW_TAC [][group_lid] THEN
  `_ = (#e * x) * |/x`             by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x * x * |/x`    by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (|/x * x) * |/x`  by SRW_TAC [][GSYM group_assoc] THEN
  `_ = |/(|/x) * #e * |/x`         by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (#e * |/x)`       by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x`              by SRW_TAC [][group_lid] THEN
  `_ = #e`                         by SRW_TAC [][group_linv] THEN
  SRW_TAC [][]);

(* Theorem: [Group right identity] x e = x *)
val group_rid = store_thm(
  "group_rid",
  ``!g. Group g ==> !x :: (g.carrier). x * #e = x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * #e = x * (|/x * x)` by SRW_TAC [][group_linv] THEN
  `_ = (x * |/x) * x`      by SRW_TAC [][group_assoc] THEN
  `_ = #e * x`             by SRW_TAC [][group_rinv] THEN
  SRW_TAC [][group_lid]);

(* Theorem: [Solve left unknown] x y = z  <=> x = z y' *)
val group_lsolve = store_thm(
  "group_lsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (x = z * |/y)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_rid]);

(* Theorem: [Solve right unknown] x y = z  <=> y = x' z *)
val group_rsolve = store_thm(
  "group_rsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (y = |/x * z)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_lid]);

(* Theorem: [Left cancellation] x y = x z <=> y = z *)
val group_lcancel = store_thm(
  "group_lcancel",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y = x * z) = (y = z)``,
  METIS_TAC [group_inv_carrier, group_mult_carrier, group_assoc, group_rsolve, group_linv, group_lid]);

(* Theorem: [Right identity unique] x y = x <=> y = e *)
val group_rid_unique = store_thm(
  "group_rid_unique",
  ``!g. Group g ==> !x y :: (g.carrier). (x * y = x) = (y = #e)``,
  METIS_TAC [group_inv_carrier, group_rsolve, group_linv]);

(* Theorem: [Left inverse unique] x y = e <=> x = y' *)
val group_linv_unique = store_thm(
  "group_linv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (x = |/y)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_lsolve, group_linv, group_lid]);

(* Theorem: [Right inverse unique] x y = e <=> y = x' *)
val group_rinv_unique = store_thm(
  "group_rinv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (y = |/x)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_rsolve, group_rinv, group_rid]);

(* Theorem: [Inverse of identity] e' = e *)
val group_inv_id = store_thm(
  "group_inv_id",
  ``!g. Group g ==> (|/ #e = g.id)``,
  METIS_TAC [group_id_carrier, group_lid, group_linv_unique]);

(* Theorem: [Inverse of product] (x y)' = y' x' *)
val group_inv_mult = store_thm(
  "group_inv_mult",
  ``!g. Group g ==> !x y :: (g.carrier). |/(x * y) = |/y * |/x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `(x * y) * (|/y * |/x) = x * (y * |/y) * |/x` by SRW_TAC [][group_assoc] THEN
  `_ = #e`                                      by SRW_TAC [][group_rinv, group_rid] THEN
  POP_ASSUM MP_TAC THEN
  SRW_TAC [][group_rinv_unique]);

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Theory of Group Exponentiation.                                           *)
(* ------------------------------------------------------------------------- *)

(* Define exponents of a group element:
   For x in Group g,   group_exp x 0 = g.id
                       group_exp x (SUC n) = g.mult x (group_exp x n)
*)
val group_exp_def = Define`
  (group_exp g x 0 = g.id) /\
  (group_exp g x (SUC n) = x * (group_exp g x n))
`;

(* set overloading  *)
val _ = overload_on ("**", ``group_exp g``);

(* Theorem: (x ** n) in g.carrier *)
val group_exp_carrier = store_thm(
  "group_exp_carrier",
  ``!g x n. Group g /\ x IN g.carrier ==> (x ** n) IN g.carrier``,
  Induct_on `n` THEN METIS_TAC [group_exp_def, group_id_carrier, group_mult_carrier]);

(* Theorem: x ** 1 = x *)
val group_exp_one = store_thm(
  "group_exp_one",
  ``!g. Group g /\ x IN g.carrier ==> (x ** 1 = x)``,
  REPEAT STRIP_TAC THEN
  `1 = SUC 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_def, group_rid]);

(* Theorem: (g.id ** n) = g.id  *)
val group_id_exp = store_thm(
  "group_id_exp",
  ``!g n. Group g ==> (g.id ** n = #e)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_lid, group_id_carrier]);

(* Theorem: For abelian group g,  (x ** n) * y = y * (x ** n) *)
val group_comm_exp = store_thm(
  "group_comm_exp",
  ``!g x y n. Group g /\ x IN g.carrier /\ y IN g.carrier ==>
    (x * y = y * x) ==> ((x ** n) * y = y * (x ** n))``,
  Induct_on `n` THEN
  SRW_TAC [][group_exp_def, group_lid, group_rid] THEN
  METIS_TAC [group_assoc, group_exp_carrier]);

(* Theorem: (x ** n) * x = x * (x ** n) *)
val group_exp_comm = store_thm(
  "group_exp_comm",
  ``!g x n. Group g /\ x IN g.carrier ==> ((x ** n) * x = x * (x ** n))``,
  METIS_TAC [group_comm_exp]);

(* Theorem: For abelian group, (x * y) ** n = (x ** n) * (y ** n) *)
val group_mult_exp = store_thm(
  "group_mult_exp",
  ``!g x y n. Group g /\ x IN g.carrier /\ y IN g.carrier /\ (x * y = y * x) ==>
    ((x * y) ** n = (x ** n) * (y ** n))``,
  Induct_on `n` THEN1 METIS_TAC [group_exp_def, group_lid, group_id_carrier] THEN
  SRW_TAC [][group_exp_def] THEN
  `x * y * (x * y) ** n = (x*y)*((x**n)*(y**n))` by METIS_TAC [group_mult_carrier] THEN
  `_ = x*(y*((x**n)*(y**n)))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  `_ = x*((y*(x**n))*(y**n))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  `_ = x*(((x**n)*y)*(y**n))` by METIS_TAC [group_mult_carrier, group_exp_carrier, group_comm_exp] THEN
  `_ = x*((x**n)*(y*(y**n)))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier]);

(* Theorem: x ** (m + n) = (x ** m) * (x ** n) *)
val group_exp_add = store_thm(
  "group_exp_add",
  ``!g x m n. Group g /\ x IN g.carrier ==> (x ** (m + n) = (x ** m) * (x ** n))``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_lid, group_id_carrier, group_exp_carrier] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m + n = SUC (m+n)` by RW_TAC arith_ss [] THEN
  `x ** (SUC m + n) = x ** (SUC (m+n))` by SRW_TAC [][] THEN
  `_ = x * (x ** (m + n))` by SRW_TAC [][group_exp_def] THEN
  METIS_TAC [group_assoc, group_exp_carrier]);

(* Theorem: x ** (m * n) = (x ** m) ** n  *)
val group_exp_mult = store_thm(
  "group_exp_mult",
  ``!g x m n. Group g /\ x IN g.carrier ==> (x ** (m * n) = (x ** m) ** n)``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_id_exp] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m * n = m * n + n` by METIS_TAC [arithmeticTheory.MULT] THEN
  `x ** (SUC m * n) = x ** (m*n + n)` by SRW_TAC [][] THEN
  `_ = (x**(m*n))*(x ** n)` by SRW_TAC [][group_exp_add] THEN
  `_ = ((x**m)**n)*(x**n)` by METIS_TAC [] THEN
  `_ = ((x**m)*x)**n` by SRW_TAC [][group_mult_exp, group_exp_comm, group_exp_carrier] THEN
  SRW_TAC [][group_exp_comm]);

(* Theorem: Inverse of exponential: (x**n)' = (x')**n  *)
val group_exp_inv = store_thm(
  "group_exp_inv",
  ``!g x n. Group g /\ x IN g.carrier ==> (|/ (x ** n) = (|/ x) ** n)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_inv_id, group_inv_carrier, group_inv_mult,
             group_exp_carrier, group_exp_comm]);

(* Theorem: For m < n, x**m = x**n ==> x**(n-m) = #e *)
val group_exp_eq = store_thm(
  "group_exp_eq",
  ``!g x m n.Group g /\ x IN g.carrier /\ m < n /\ (x**m = x**n) ==> (x**(n-m) = #e)``,
  REPEAT STRIP_TAC THEN
  `?d. (d = n - m) /\ 0 < d /\ (n = m + d)` by RW_TAC arith_ss [] THEN
  `x**n = x**(m+d)` by SRW_TAC [][] THEN
  `_ = x**m * x**d` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_rid_unique, group_exp_carrier]);

(* ------------------------------------------------------------------------- *)
(* Theory of Subgroup (via Cosets).                                          *)
(* ------------------------------------------------------------------------- *)

(* A Subgroup is a subset of a group that's a group itself, keeping id, inv, mult. *)
val Subgroup_def = Define `
  Subgroup h g =
    Group h /\ Group g /\
    h.carrier SUBSET g.carrier /\
    (h.id = #e) /\ (h.inv = |/) /\ (h.mult = g.mult)
`;

(* set overloading *)
val _ = overload_on ("<=", ``Subgroup``);

(* Theorem: subgroup property *)
val subgroup_property = store_thm(
  "subgroup_property",
  ``!g h. Group g /\ h <= g ==> (Group h) /\ (h.mult = g.mult) /\ (h.id = g.id) /\ (h.inv = g.inv)``,
  SRW_TAC [][Subgroup_def]);

(* Define coset of subgroup with an element a. *)
val coset_def = Define `
  coset g X a = IMAGE (\z. g.mult a z) X
`;

(* Theorem: For x IN (coset h g.carrier a) ==> ?y IN g.carrier /\ a*y = x *)
val SUBGROUP_COSET_ELEMENT = store_thm(
  "SUBGROUP_COSET_ELEMENT",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN coset g h.carrier a ==> ?y. y IN h.carrier /\ (x = a*y)``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For all x IN h.carrier, a*x IN coset g h.carrier a. *)
val SUBGROUP_COSET_PROPERTY = store_thm(
  "SUBGROUP_COSET_PROPERTY",
  ``!g h a. Group g /\ h <= g  /\ a IN g.carrier ==>
   !x. x IN h.carrier ==> a*x IN coset g h.carrier a``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN
  METIS_TAC []);

(* Theorem: For h <= g, x IN coset g h.carrier x *)
val SUBGROUP_COSET_NONEMPTY = store_thm(
  "SUBGROUP_COSET_NONEMPTY",
  ``!g h x. Group g /\ h <= g /\ x IN g.carrier ==> x IN coset g h.carrier x``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rid, group_id_carrier]);

(* Theorem: There is a bijection between subgroup and its cosets. *)
val subgroup_to_coset_bij = store_thm(
  "subgroup_to_coset_bij",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==>
   BIJ (\x. a*x) h.carrier (coset g h.carrier a)``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY, SUBGROUP_COSET_ELEMENT, group_lcancel, Subgroup_def, SUBSET_DEF]);

(* Theorem: All cosets of subgroup are of the same size as the subgroup *)
val CARD_subgroup_coset = store_thm(
  "CARD_subgroup_coset",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier /\ FINITE h.carrier ==>
   (CARD (coset g h.carrier a) = CARD (h.carrier))``,
  REPEAT STRIP_TAC THEN
  `BIJ (\x. a*x) h.carrier (coset g h.carrier a)` by SRW_TAC [][subgroup_to_coset_bij] THEN
  `FINITE (coset g h.carrier a)` by METIS_TAC [coset_def, IMAGE_FINITE] THEN
  METIS_TAC [FINITE_BIJ_CARD_EQ]);

(* ------------------------------------------------------------------------- *)
(* Langrange's Theorem by Subgroups and Cosets                               *)
(* ------------------------------------------------------------------------- *)

(* Define b ~ a  when  b IN (coset g h.carrier a) *)
val inCoset_def = Define `
  inCoset g h a b = b IN (coset g h.carrier a)
`;

(* Theorem: inCoset is Reflexive. *)
val INCOSET_REFL = store_thm(
  "INCOSET_REFL",
  ``!g h. Group g /\ h <= g ==> !a. a IN g.carrier ==> inCoset g h a a``,
  METIS_TAC [inCoset_def, SUBGROUP_COSET_NONEMPTY]);

(* Theorem: inCoset is Symmetric. *)
val INCOSET_SYM = store_thm(
  "INCOSET_SYM",
  ``!g h. Group g /\ h <= g ==>
   !a b. a IN g.carrier /\ b IN g.carrier
       ==> (inCoset g h a b ==> inCoset g h b a)``,
  SRW_TAC [][inCoset_def] THEN
  `?z. z IN h.carrier /\ (b = a * z)` by METIS_TAC [SUBGROUP_COSET_ELEMENT] THEN
  `|/ z IN h.carrier` by METIS_TAC [Subgroup_def, group_inv_carrier] THEN
  `z IN g.carrier /\ |/ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `b * |/ z = (a * z) * (|/ z)` by SRW_TAC [][] THEN
  `_ = a * (z * |/ z)` by SRW_TAC [][group_assoc] THEN
  `_ = a` by SRW_TAC [][group_rinv, group_rid] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is Transitive. *)
val INCOSET_TRANS = store_thm(
  "INCOSET_TRANS",
  ``!g h. Group g /\ h <= g ==>
   !a b c. a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
       ==> (inCoset g h a b /\ inCoset g h b c ==> inCoset g h a c)``,
  SRW_TAC [][inCoset_def] THEN
  `?y. y IN h.carrier /\ (b = a * y) /\ ?z. z IN h.carrier /\ (c = b * z)` by SRW_TAC [][SUBGROUP_COSET_ELEMENT] THEN
  `y IN g.carrier /\ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `c = (a * y) * z` by SRW_TAC [][] THEN
  `_ = a * (y * z)` by SRW_TAC [][group_assoc] THEN
  `y*z IN h.carrier` by METIS_TAC [subgroup_property, group_mult_carrier] THEN
  METIS_TAC [SUBGROUP_COSET_PROPERTY]);

(* Theorem: inCoset is an equivalence relation *)
val INCOSET_EQUIV_ON_CARRIER = store_thm(
  "INCOSET_EQUIV_ON_CARRIER",
  ``!g h. Group g /\ h <= g ==> inCoset g h equiv_on g.carrier``,
  SRW_TAC [][equiv_on_def] THEN METIS_TAC [INCOSET_REFL, INCOSET_SYM, INCOSET_TRANS]);

(* Define coset partitions of g.carrier by inCoset g h. *)
val CosetPartition_def = Define `
  CosetPartition g h = partition (inCoset g h) g.carrier
`;

(* Theorem: For FINITE Group g, h <= g, compute CARD g.carrier by partition. *)
val CARD_CARRIER_BY_COSET_PARTITION = store_thm(
  "CARD_CARRIER_BY_COSET_PARTITION",
  ``!g h. Group g /\ h <= g /\ FINITE g.carrier ==>
    (CARD g.carrier = SIGMA CARD (CosetPartition g h))``,
  METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, partition_CARD]);

(* Theorem: Elements in CosetPartition are cosets of some a In g.carrier *)
val COSET_PARTITION_ELEMENT = store_thm(
  "COSET_PARTITION_ELEMENT",
  ``!g h. Group g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> ?a. a IN g.carrier /\ (e = coset g h.carrier a)``,
  SRW_TAC [][CosetPartition_def, inCoset_def, partition_def] THEN
  Q.EXISTS_TAC `x` THEN
  SRW_TAC [][coset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [Subgroup_def, SUBSET_DEF, group_mult_carrier]);

(* Theorem: For FINITE group, CARD element in CosetPartiton = CARD subgroup. *)
val CARD_COSET_PARTITION_ELEMENT = store_thm(
  "CARD_COSET_PARTITION_ELEMENT",
  ``!g h. FiniteGroup g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> (CARD e = CARD h.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?a. a IN g.carrier /\ (e = coset g h.carrier a)` by SRW_TAC [][COSET_PARTITION_ELEMENT] THEN
  `FINITE h.carrier` by METIS_TAC [Subgroup_def, SUBSET_FINITE] THEN
  METIS_TAC [CARD_subgroup_coset]);

(* Theorem: (Lagrange Theorem)
            For FINITE Group g, size of subgroup divides size of group. *)
val LAGRANGE_THM = store_thm(
  "LAGRANGE_THM",
  ``!g h. FiniteGroup g /\ h <= g ==> divides (CARD h.carrier) (CARD g.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `FINITE (CosetPartition g h)` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  `CARD g.carrier = SIGMA CARD (CosetPartition g h)` by SRW_TAC [][CARD_CARRIER_BY_COSET_PARTITION] THEN
  `_ = CARD h.carrier * CARD (CosetPartition g h)` by METIS_TAC [SIGMA_CARD_CONSTANT, CARD_COSET_PARTITION_ELEMENT, FiniteGroup_def] THEN
  METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_COMM]);

(* ------------------------------------------------------------------------- *)
(* Theory of Finite group element order.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For Group g and x IN g.carrier, if x**n are all distinct, g.carrier is INFINITE *)
val group_exp_all_distinct = store_thm(
  "group_exp_all_distinct",
  ``!g x. Group g /\ x IN g.carrier /\ (!m n. (x**m = x**n) ==> (m = n)) ==> INFINITE g.carrier``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `?c. c = CARD g.carrier` by SRW_TAC [][] THEN
  `c < SUC c` by RW_TAC arith_ss [] THEN
  `INJ (\n. x**n) (count (SUC c)) g.carrier`
    by SRW_TAC [][pred_setTheory.INJ_DEF, group_exp_carrier] THEN
  METIS_TAC [pred_setTheory.CARD_COUNT, pred_setTheory.PHP]);

(* Theorem: For FINITE Group g and x IN g.carrier, there is a k > 0 such that x**k = #e. *)
val group_exp_period_exists = store_thm(
  "group_exp_period_exists",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> ?k. 0 < k /\ (x**k = #e)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?m n. m <> n /\ (x**m = x**n)` by METIS_TAC [group_exp_all_distinct] THEN
  Cases_on `m < n` THENL [
    `0 < n-m` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [group_exp_eq]);

(* Define order = LEAST period for an element x in Group g *)
val period_def = Define`
  period g x k = 0 < k /\ (x**k = g.id)
`;
val order_def = Define`
  order g x = LEAST k. period g x k
`;

(* Theorem: The finite group order is indeed a period. *)
val group_order_period = store_thm(
  "group_order_period",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> period g x (order g x)``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LEAST_INTRO]);

(* Theorem: If k < order of finite group, then k is not a period. *)
val group_order_minimal = store_thm(
  "group_order_minimal",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> !k. k < (order g x) ==> ~ period g x k``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LESS_LEAST]);

(* Theorem: The finite group order m satisfies: 0 < m and x**m = #e. *)
val group_order_property = store_thm(
  "group_order_property",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> 0 < order g x /\ (x ** order g x = #e)``,
  METIS_TAC [group_order_period, period_def]);

(* Theorem: For finite group, x' = x ** (m-1) where m = order g x *)
val group_order_inv = store_thm(
  "group_order_inv",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> (|/x = x**((order g x)-1))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `x ** 1 = x` by METIS_TAC [group_exp_one] THEN
  `(m-1)+1 = m` by RW_TAC arith_ss [] THEN
  `x ** (m-1) * x = #e` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_linv_unique, group_exp_carrier]);

(* Theorem: For FINITE Group g, x**n = x**(n mod m), where m = order g x *)
val group_exp_mod = store_thm(
  "group_exp_mod",
  ``!g x n. FiniteGroup g /\ x IN g.carrier ==> (x**n = x**(n MOD order g x))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `n = (n DIV m)*m + (n MOD m)` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = m*(n DIV m) + (n MOD m)` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_add, group_exp_mult, group_id_exp, group_lid, group_exp_carrier]);

(* Theorem: For FINITE group g, m, n < (order g x), x**m = x**n ==> m = n *)
val group_order_unique = store_thm(
  "group_order_unique",
  ``!g x m n. FiniteGroup g /\ x IN g.carrier /\ m < (order g x) /\ n < (order g x) ==>
    (x**m = x**n) ==> (m = n)``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  Cases_on `m < n` THENL [
    `0 < n-m /\ n-m < order g x` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n /\ m-n < order g x` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [FiniteGroup_def, group_exp_eq, group_order_minimal, period_def]);

(* ------------------------------------------------------------------------- *)
(* Theory of Generated Subgroup.                                             *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The Subgroup <a> of any element a of Group g.                             *)
(* ------------------------------------------------------------------------- *)

(* Define the generator group, the exponential group of an element a of group g *)
val Generated_def = Define`
  Generated g a =
    <| carrier := {x | ?k. x = a**k };
            id := g.id;
           inv := |/ ;
          mult := g.mult
     |>`;

(* Theorem: For a FINITE group g, the generated group of a in g.carrier is a group *)
val Generated_group = store_thm(
  "Generated_group",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> Group (Generated g a)``,
  SRW_TAC [][FiniteGroup_def] THEN
  SRW_TAC [][Group_def, Generated_def, RES_FORALL_THM] THENL [
    METIS_TAC [group_exp_def],
    METIS_TAC [group_exp_add],
    METIS_TAC [FiniteGroup_def, group_order_inv, group_exp_inv, group_exp_mult, group_inv_carrier],
    METIS_TAC [group_lid, group_exp_carrier],
    METIS_TAC [group_exp_inv, group_linv, group_rinv, group_mult_exp, group_inv_carrier, group_id_exp],
    SRW_TAC [][group_assoc, group_exp_carrier]
  ]);

(* Theorem: The generated group <a> for a IN G is subgroup of G. *)
val Generated_subgroup = store_thm(
  "Generated_subgroup",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> Subgroup (Generated g a) g``,
  SRW_TAC [][FiniteGroup_def, Subgroup_def] THEN1
  SRW_TAC [][FiniteGroup_def, Generated_group] THEN1
  (SRW_TAC [][Generated_def, SUBSET_DEF] THEN METIS_TAC [group_exp_carrier]) THEN
  SRW_TAC [][Generated_def]);

(* Theorem: There is a bijection from (count m) to (Generated g a), where m = order g x *)
val group_order_to_generated_bij = store_thm(
  "group_order_to_generated_bij",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==>
    BIJ (\n. a**n) (count (order g a)) (Generated g a).carrier``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF, Generated_def] THEN1
  METIS_TAC [] THEN1
  METIS_TAC [group_order_unique] THEN1
  METIS_TAC [] THEN1
  METIS_TAC [group_exp_mod, group_order_property, arithmeticTheory.MOD_LESS]);

(* Theorem: The order of the Generated_subgroup is the order of its element *)
val CARD_generated_subgroup = store_thm(
  "CARD_generated_subgroup",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> (CARD (Generated g a).carrier = order g a)``,
  METIS_TAC [Generated_subgroup, Subgroup_def, FiniteGroup_def, SUBSET_DEF, SUBSET_FINITE,
             group_order_to_generated_bij, FINITE_BIJ_CARD_EQ, FINITE_COUNT, CARD_COUNT]);

(* Theorem: [Euler-Fermat Theorem in Group Theory]
            For FiniteGroup g, a IN g.carrier ==> a**(CARD g) = #e *)
val FINITE_GROUP_FERMAT = store_thm(
  "FINITE_GROUP_FERMAT",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> (a**(CARD g.carrier) = #e)``,
  REPEAT STRIP_TAC THEN
  `Subgroup (Generated g a) g` by SRW_TAC [][Generated_subgroup] THEN
  `CARD (Generated g a).carrier = order g a` by SRW_TAC [][CARD_generated_subgroup] THEN
  `divides (order g a) (CARD g.carrier)` by METIS_TAC [LAGRANGE_THM] THEN
  `?k. CARD g.carrier = (order g a) * k` by METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_SYM] THEN
  `a ** (CARD g.carrier) = a ** ((order g a) * k)` by SRW_TAC [][] THEN
  METIS_TAC [FiniteGroup_def, group_exp_mult, group_order_property, group_id_exp]);

(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Group-theoretic Proof applied to Z*p.                                     *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Residue -- close-relative of COUNT                                        *)
(* ------------------------------------------------------------------------- *)

(* Define the set of residues = nonzero remainders *)
val residue_def = Define `residue n = { i | (0 < i) /\ (i < n) }`;

(* Theorem: count n = 0 INSERT (residue n) *)
val RESIDUE_COUNT = store_thm(
  "RESIDUE_COUNT",
  ``!n. 0 < n ==> (count n = 0 INSERT (residue n))``,
  SRW_TAC [ARITH_ss][residue_def, EXTENSION]);

(* Theorem: FINITE (residue n) *)
val FINITE_RESIDUE = store_thm(
  "FINITE_RESIDUE",
  ``!n. FINITE (residue n)``,
  Cases THEN1 SRW_TAC [][residue_def] THEN
  METIS_TAC [RESIDUE_COUNT, FINITE_INSERT, count_def, FINITE_COUNT,
             DECIDE ``0 < SUC n``]);

(* Theorem: For n > 0, CARD (residue n) = n-1 *)
val CARD_RESIDUE = store_thm(
  "CARD_RESIDUE",
  ``!n. 0 < n ==> (CARD (residue n) = n-1)``,
  REPEAT STRIP_TAC THEN
  `0 NOTIN (residue n)` by SRW_TAC [][residue_def] THEN
  `0 INSERT (residue n) = count n`
    by SRW_TAC [][residue_def, EXTENSION, EQ_IMP_THM] THEN RW_TAC arith_ss [] THEN
  `SUC (CARD (residue n)) = n` by METIS_TAC [FINITE_RESIDUE, CARD_INSERT, CARD_COUNT] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* The Group Z*p = Multiplication Modulo p, for prime p.                     *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Establish the existence of multiplicative inverse when p is prime.        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Euclid's Lemma] A prime divides a product iff the prime divides a factor.
            [in MOD notation] For prime p, x*y MOD p = 0 <=> x MOD p = 0 or y MOD p = 0 *)
val EUCLID_LEMMA = store_thm(
  "EUCLID_LEMMA",
  ``!p x y. prime p ==> (((x * y) MOD p = 0) <=> (x MOD p = 0) \/ (y MOD p = 0))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  SRW_TAC [][MOD_0_DIVIDES, EQ_IMP_THM] THEN
  METIS_TAC [gcdTheory.P_EUCLIDES, dividesTheory.DIVIDES_MULT, arithmeticTheory.MULT_COMM]);

(* Theorem: [Existence of Inverse] For prime p, 0 < x < p ==> ?y. y*x MOD p = 1 *)
val MOD_MULT_INV = store_thm(
  "MOD_MULT_INV",
  ``!p x. prime p /\ 0 < x /\ x < p ==> ?y. 0 < y /\ y < p /\ ((y*x) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `gcd p x = 1` by METIS_TAC [gcdTheory.PRIME_GCD, dividesTheory.NOT_LT_DIVIDES] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `?k q. k * x = q * p + 1` by METIS_TAC [gcdTheory.LINEAR_GCD, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  `(k * x) MOD p = 1` by METIS_TAC [arithmeticTheory.MOD_MULT, dividesTheory.ONE_LT_PRIME] THEN
  `((k MOD p) * x) MOD p = 1` by METIS_TAC [arithmeticTheory.MOD_TIMES2, arithmeticTheory.LESS_MOD] THEN
  `1 <> 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [arithmeticTheory.MOD_LESS, EUCLID_LEMMA, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Convert this into an existence definition *)
val lemma = prove(
  ``!p x. ?y. prime p /\ 0 < x /\ x < p ==>
              0 < y /\ y < p /\ ((y*x) MOD p = 1)``,
  METIS_TAC [MOD_MULT_INV]);

val MUL_INV_DEF = new_specification(
  "MUL_INV_DEF",
  ["MOD_MUL_INV"],
  SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* ------------------------------------------------------------------------- *)
(* The Group of Multiplication modulo p                                      *)
(* ------------------------------------------------------------------------- *)

(* Define Multiplicative Modulo p Group *)
val Zstar_def = Define`
  Zstar p =
   <| carrier := residue p;
           id := 1;
          inv := MOD_MUL_INV p;
         mult := (\i j. (i * j) MOD p)
    |>`;

(* Theorem: (Zstar p) is a Group. *)
val Zstar_group = store_thm(
  "Zstar_group",
  ``!p. prime p ==> Group (Zstar p)``,
  SRW_TAC [][Zstar_def, residue_def, Group_def, RES_FORALL_THM, MUL_INV_DEF]
  THENL [ (* 4 subgoals *)
    SRW_TAC [][dividesTheory.ONE_LT_PRIME],
    `(x*y) MOD p <> 0` by SRW_TAC [][EUCLID_LEMMA] THEN RW_TAC arith_ss [],
    RW_TAC arith_ss [],
    METIS_TAC [dividesTheory.PRIME_POS, MOD_MULT_ASSOC]
  ] );

(* Theorem: FINITE (Zstar p).carrier *)
val FINITE_Zstar_carrier = store_thm(
  "FINITE_Zstar_carrier",
  ``!p. FINITE (Zstar p).carrier``,
  SRW_TAC [][Zstar_def] THEN
  `residue p SUBSET count p` by SRW_TAC [][residue_def, pred_setTheory.SUBSET_DEF] THEN
  METIS_TAC [pred_setTheory.FINITE_COUNT, pred_setTheory.SUBSET_FINITE]);

(* Theorem: p > 0 ==> CARD (Zstar p).carrier = p-1 *)
val CARD_Zstar_carrier = store_thm(
  "CARD_Zstar_carrier",
  ``!p. 0 < p ==> (CARD (Zstar p).carrier = p-1)``,
  SRW_TAC [][Zstar_def, CARD_RESIDUE]);

(* Theorem: group_exp (Zstar p) a n = a**n MOD p *)
val Zstar_exp = store_thm(
  "Zstar_exp",
  ``!p a. prime p /\ a IN (Zstar p).carrier ==> !n. group_exp (Zstar p) a n = (a**n) MOD p``,
  REPEAT STRIP_TAC THEN
  `Group (Zstar p)` by SRW_TAC [][Zstar_group] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Induct_on `n` THEN1 (
    `(Zstar p).id = 1` by SRW_TAC [][Zstar_def] THEN
    `1 < p /\ (1 MOD p = 1)` by RW_TAC arith_ss [dividesTheory.ONE_LT_PRIME] THEN
    METIS_TAC [group_exp_def, arithmeticTheory.EXP]
  ) THEN
  SRW_TAC [][group_exp_def, arithmeticTheory.EXP] THEN
  FULL_SIMP_TAC (srw_ss())[Zstar_def, residue_def] THEN
  `a MOD p = a` by RW_TAC arith_ss [] THEN
  METIS_TAC [arithmeticTheory.MOD_TIMES2]);

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem (by Z*p multiplicative group)                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For prime p, 0 < a < p,  a**(p-1) = 1 (mod p) *)
val FERMAT_LITTLE = store_thm(
  "FERMAT_LITTLE",
  ``!p a. prime p /\ 0 < a /\ a < p ==> (a**(p-1) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `Group (Zstar p)` by SRW_TAC [][Zstar_group] THEN
  `FiniteGroup (Zstar p)` by SRW_TAC [][FiniteGroup_def, FINITE_Zstar_carrier] THEN
  `CARD (Zstar p).carrier = p-1` by METIS_TAC [dividesTheory.PRIME_POS, CARD_Zstar_carrier] THEN
  `a IN (Zstar p).carrier` by SRW_TAC [][Zstar_def, residue_def] THEN
  `(Zstar p).id = 1` by SRW_TAC [][Zstar_def] THEN
  METIS_TAC [FINITE_GROUP_FERMAT, Zstar_exp]);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
