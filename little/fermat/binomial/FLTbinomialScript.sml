(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Binomial proof.                                 *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Binomial proof)
========================================
To prove:  a^p = a  (mod p)   for prime p
Proof:
   By induction on a.

   Base case: 0^p = 0 (mod p)
      true by arithmetic.
   Step case: k^p = k (mod p)  ==> (k+1)^p = (k+1) (mod p)
      (k+1)^p
    = SUM (GENLIST (\j. (binomial p j)* k**(p-j) * 1**j) (SUC p))    by binomial_thm
    = SUM (GENLIST (\j. (binomial p j)* k**(p-j)) (SUC p))           by arithmetic

   By prime_iff_divides_binomials,
      prime p <=> 1 < p /\ (!j. 0 < j /\ j < p ==> divides p (binomial p j))`;
   Therefore in mod p,
      (k+1)^p = k^p + 1^p    (mod p)   just first and last terms
              = k   + 1^p    (mod p)   by induction hypothesis
              = k + 1                  by arithmetic
*)

(* Binomial proof of Fermat's Little Theorem in HOL:
C:\jc\www\ml\hol\info\Hol\examples\miller\RSA\fermatScript.sml
*)


(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTbinomial";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
open pred_setTheory listTheory arithmeticTheory;
(* val _ = load "dividesTheory"; *)
open dividesTheory;

(* Get dependent theories in lib *)
(*
(* val _ = load "helperNumTheory"; *)
(* val _ = load "helperListTheory"; *)
(* val _ = load "helperSetTheory"; *)
open helperNumTheory helperListTheory helperSetTheory;
*)

(* Get dependent theories in local *)
(* val _ = load "helperBinomialTheory"; *)
open binomialTheory helperBinomialTheory;


(* ------------------------------------------------------------------------- *)
(* Binomial Proof via Induction.                                             *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (Fermat's Little Theorem by Binomial) For prime p, a**p = a (mod p). *)
(* Proof: by induction on a.
   Base case: 0 ** p MOD p = 0 MOD p
      true by
   Step case: a ** p MOD p = a MOD p ==> SUC a ** p MOD p = SUC a MOD p
     SUC a ** p MOD p
   = (a + 1) ** p MOD p                     by ADD1
   = (a ** p + 1 ** p) MOD p                by binomial_thm_prime
   = (a ** p MOD p + 1 ** p MOD p) MOD p    by MOD_PLUS
   = (a MOD p + 1 ** p MOD p) MOD p         by induction hypothesis
   = (a MOD p + 1 MOD p) MOD p              by EXP_1
   = (a + 1) MOD p                          by MOD_PLUS
   = SUC a MOD p                            by ADD1
*)
val FERMAT_LITTLE4 = store_thm(
  "FERMAT_LITTLE4",
  ``!p a. prime p ==> (a**p MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by RW_TAC std_ss [PRIME_POS] THEN
  `p <> 0` by DECIDE_TAC THEN
  Induct_on `a` THEN1
  RW_TAC std_ss [ZERO_EXP] THEN
  METIS_TAC [binomial_thm_prime, MOD_PLUS, EXP_1, ADD1]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
