(* ------------------------------------------------------------------------- *)
(* Necklace Theory - monocoloured and multicoloured.                         *)
(* ------------------------------------------------------------------------- *)

(*

Necklace Theory
===============

Consider the set N of necklaces of length n (i.e. with number of beads = n)
with a colors (i.e. the number of bead colors = a). A linear picture of such
a necklace is:

+--+--+--+--+--+--+--+
|2 |4 |0 |3 |1 |2 |3 |  p = 7, with (lots of) beads of a = 5 colors: 01234.
+--+--+--+--+--+--+--+

Since a bead can have any of the a colors, and there are n beads in total,

Number of such necklaces = CARD N = a*a*...*a = a^n.

There is only 1 necklace of pure color A, 1 necklace with pure color B, etc.

Number of monocoloured necklaces = a = CARD S, where S = monocoloured necklaces.

So, N = S UNION M, where M = multicoloured necklaces (i.e. more than one color).

Since S and M are disjoint, CARD M = CARD N - CARD S = a^n - a.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "necklace";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* Get dependent theories *)
(* val _ = load "helperNumTheory"; *)
(* val _ = load "helperSetTheory"; *)
(* val _ = load "helperListTheory"; *)
open helperNumTheory helperSetTheory helperListTheory;

(* ------------------------------------------------------------------------- *)
(* Necklace                                                                  *)
(* ------------------------------------------------------------------------- *)

(* Define necklaces as lists of length n, i.e. with n beads *)
val Necklace_def = Define `
  Necklace n a = {l | (LENGTH l = n) /\ (set l) SUBSET (count a) }
`;

(* ------------------------------------------------------------------------- *)
(* Know the Necklaces.                                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Zero-length necklaces of whatever colors is the set of NIL. *)
val Necklace_0 = store_thm(
  "Necklace_0",
  ``!a. Necklace 0 a = {[]}``,
  SRW_TAC [boolSimps.CONJ_ss][Necklace_def, LENGTH_NIL]);

(* val _ = export_rewrites ["Necklace_0"]; *)

(* Theorem: Necklaces with some length but 0 colors is EMPTY. *)
val Necklace_EMPTY = store_thm(
  "Necklace_EMPTY",
  ``!n. 0 < n ==> (Necklace n 0 = {})``,
  SRW_TAC [boolSimps.CONJ_ss][Necklace_def, LENGTH_NIL, EXTENSION] THEN
  RW_TAC arith_ss []);

(* Theorem: A necklace of length n <> 0 has an order, i.e. non-NIL. *)
val NECKLACE_NONNIL = store_thm(
  "NECKLACE_NONNIL",
  ``!n a l. (0 < n) /\ (0 < a) /\ (l IN (Necklace n a)) ==> (l <> [])``,
  SRW_TAC [][Necklace_def] THEN
  `LENGTH l <> 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [LENGTH_NIL]);

(* Theorem: If l in (Necklace n a), LENGTH l = n  and colors in count a. *)
val NECKLACE_PROPERTY = store_thm(
  "NECKLACE_PROPERTY",
  ``!n a l. l IN (Necklace n a) ==> (LENGTH l = n) /\ (set l SUBSET count a)``,
  SRW_TAC [][Necklace_def]);

(* ------------------------------------------------------------------------- *)
(* To show: (Necklace n a) is FINITE.                                        *)
(* ------------------------------------------------------------------------- *)

(* A lemma for induction *)
(* Theorem: Relate (Necklace (n+1) a) to (Necklace n a) for induction. *)
val Necklace_SUC = prove(
  ``!n a. Necklace (SUC n) a = IMAGE (\(e,l). e :: l) (count a CROSS Necklace n a)``,
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss]
    [Necklace_def, count_def, SUBSET_DEF, EXTENSION, pairTheory.EXISTS_PROD, LENGTH_CONS] THEN
  METIS_TAC []);

(* Theorem: The set of (Necklace n a) is finite. *)
val FINITE_Necklace = store_thm(
   "FINITE_Necklace",
  ``!n a. FINITE (Necklace n a)``,
  Induct_on `n` THEN SRW_TAC [][Necklace_0, Necklace_SUC]);

(* val _ = export_rewrites ["FINITE_Necklace"]; *)

(* ------------------------------------------------------------------------- *)
(* To show: CARD (Necklace n a) = a^n.                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Size of (Necklace n a) = a^n. *)
val CARD_Necklace = store_thm(
  "CARD_Necklace",
  ``!n a. CARD (Necklace n a) = a ** n``,
  Induct_on `n` THEN
  SRW_TAC [][Necklace_0, FINITE_Necklace,
             Necklace_SUC, CARD_IMAGE, FINITE_COUNT, CARD_CROSS,
             pairTheory.FORALL_PROD, arithmeticTheory.EXP]);

(* ------------------------------------------------------------------------- *)
(* Mono-colored Necklace - necklace with a single color.                     *)
(* ------------------------------------------------------------------------- *)

(* Define mono-colored necklace *)
val monocoloured_def = Define`
  monocoloured n a = {l | (l IN Necklace n a) /\ (l <> [] ==> SING (set l)) }
`;

(* ------------------------------------------------------------------------- *)
(* Know the Mono-coloured Necklaces.                                         *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A monocoloured necklace is indeed a Necklace. *)
val monocoloured_Necklace = store_thm(
  "monocoloured_Necklace",
  ``!n a l. l IN monocoloured n a ==> l IN Necklace n a``,
  SRW_TAC [][Necklace_def, monocoloured_def]);

(* Theorem: The monocoloured set is FINITE. *)
val FINITE_monocoloured = store_thm(
  "FINITE_monocoloured",
  ``!n a. FINITE (monocoloured n a)``,
  METIS_TAC [SUBSET_DEF, monocoloured_Necklace, SUBSET_FINITE, FINITE_Necklace]);

(* val _ = export_rewrites ["FINITE_monocoloured"]; *)

(* Theorem: Zero-length monocoloured set is singleton NIL. *)
val monocoloured_0 = prove(
  ``!a. monocoloured 0 a = {[]}``,
  SRW_TAC [][monocoloured_def, Necklace_0, EXTENSION, EQ_IMP_THM]);

(* Theorem: Unit-length monocoloured set consists of singletons. *)
val monocoloured_1 = prove(
  ``!a. monocoloured 1 a = {[e] | e IN count a}``,
  SIMP_TAC bool_ss [monocoloured_def, Necklace_def, count_def, arithmeticTheory.ONE, LENGTH_CONS] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION]);

(* Theorem: Unit-length necklaces are monocoloured. *)
val necklace_1_monocoloured = store_thm(
  "necklace_1_monocoloured",
  ``!a. Necklace 1 a = monocoloured 1 a``,
  SRW_TAC [][Necklace_def, monocoloured_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [LIST_1_SET_SING]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (monocoloured n a) = a.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Relate (monocoloured (SUC n) a) to (monocoloured n a) for induction. *)
val monocoloured_SUC = prove(
  ``!n a. 0 < n ==> (monocoloured (SUC n) a = IMAGE (\l. HD l :: l) (monocoloured n a))``,
  SRW_TAC [][EXTENSION, EQ_IMP_THM, monocoloured_def] THEN
  FULL_SIMP_TAC (srw_ss()) [Necklace_def, LENGTH_CONS, count_def, SUBSET_DEF, SING_DEF] THENL [
    Cases_on `l'` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
    `x <> [] /\ MEM h x /\ MEM h' x` by SRW_TAC [][] THEN
    `?k. !z . MEM z x <=> (z = k)` by SRW_TAC [][] THEN
    `h = h'` by METIS_TAC [] THEN SRW_TAC [][] THEN
    Q.EXISTS_TAC `x'` THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [] THEN METIS_TAC [],
    Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN METIS_TAC []
  ]);

(* Theorem: Size of (monocoloured n a) = a *)
val CARD_monocoloured = store_thm(
  "CARD_monocoloured",
  ``!n a. 0 < n ==> (CARD (monocoloured n a) = a)``,
  REPEAT STRIP_TAC THEN
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n` THENL [
    SRW_TAC [][monocoloured_1] THEN
    `{[e] | e < a} = IMAGE (\n. [n]) (count a)` by SRW_TAC [][EXTENSION] THEN
    SRW_TAC [][CARD_IMAGE],
    SRW_TAC [][monocoloured_SUC] THEN
    Q.MATCH_ABBREV_TAC `CARD (IMAGE f s) = a` THEN
    `!x y. (f x = f y) <=> (x = y)` by SRW_TAC [][EQ_IMP_THM, Abbr`f`] THEN
    `FINITE s` by SRW_TAC [][FINITE_monocoloured, Abbr`s`] THEN
    SRW_TAC [][CARD_IMAGE]
  ]);

(* ------------------------------------------------------------------------- *)
(* Multi-colored Necklace                                                    *)
(* ------------------------------------------------------------------------- *)

(* Define multi-colored necklace *)
val multicoloured_def = Define`
  multicoloured n a = (Necklace n a) DIFF (monocoloured n a)
`;

(* ------------------------------------------------------------------------- *)
(* Know the Multi-coloured Necklaces.                                        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: multicoloured is a Necklace *)
val multicoloured_Necklace = store_thm(
  "multicoloured_Necklace",
  ``!n a l. l IN multicoloured n a ==> l IN Necklace n a``,
  SRW_TAC [][multicoloured_def, Necklace_def]);

(* Theorem: multicoloured set is FINITE *)
val FINITE_multicoloured = store_thm(
  "FINITE_multicoloured",
  ``!n a. FINITE (multicoloured n a)``,
  SRW_TAC [][multicoloured_def, FINITE_Necklace, FINITE_DIFF]);

(* Theorem: multicoloured n 0 = EMPTY *)
val multicoloured_EMPTY = store_thm(
  "multicoloured_EMPTY",
  ``!n. 0 < n ==> (multicoloured n 0 = {})``,
  SRW_TAC [][multicoloured_def, multicoloured_Necklace, Necklace_EMPTY]);

(* Theorem: multicoloured 0 a = EMPTY *)
(* Proof:
     multicoloured 0 a
   = (Necklace 0 a) DIFF (monocoloured 0 a)
   = {[]} - {[]}
   = {}
*)
val multicoloured_0 = store_thm(
  "multicoloured_0",
  ``!a. multicoloured 0 a = {}``,
  SRW_TAC [][multicoloured_def, Necklace_0, monocoloured_0]);

(* Theorem: mutlicoloured 1 a = EMPTY *)
(* Proof:
   When n = 1, LENGTH l = 1 ==> SING (set l), hence monocoloured.
*)
val multicoloured_1_EMPTY = store_thm(
  "multicoloured_1_EMPTY",
  ``!a. multicoloured 1 a = {}``,
  SRW_TAC [][multicoloured_def, Necklace_def, necklace_1_monocoloured]);

(* ------------------------------------------------------------------------- *)
(* To show: CARD (multicoloured n a) = a^n - a.                              *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A multicoloured necklace is not monocoloured. *)
val MULTI_MONO_DISJOINT = store_thm(
  "MULTI_MONO_DISJOINT",
  ``!n a. DISJOINT (multicoloured n a) (monocoloured n a)``,
  SIMP_TAC bool_ss [multicoloured_def, monocoloured_def, Necklace_def,
                    SING_DEF, SUBSET_DEF, DISJOINT_DEF] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION]);

(* Theorem: A necklace is either monocoloured or multicolored. *)
val MULTI_MONO_EXHAUST = store_thm(
  "MULTI_MONO_EXHAUST",
  ``!n a. Necklace n a = (multicoloured n a) UNION (monocoloured n a)``,
  SIMP_TAC bool_ss [multicoloured_def, monocoloured_def, Necklace_def,
                    SING_DEF, SUBSET_DEF, UNION_DEF] THEN
  SRW_TAC [boolSimps.CONJ_ss, boolSimps.DNF_ss][LENGTH_NIL, EXTENSION] THEN
  METIS_TAC []);

(* Theorem: Size of (multicoloured n a) = a^n - a *)
(* Proof:
   With all sets finite,

   CARD (multicoloured n a) + CARD (monocoloured n a) =
      CARD ((multicoloured n a) UNION (monocoloured n a)) +
      CARD ((multicoloured n a) INTER (monocoloured n a))

   But CARD ((multicoloured n a) INTER (monocoloured n a))
     = 0                         by MULTI_MONO_DISJOINT
   and CARD ((multicoloured n a) UNION (monocoloured n a))
     = CARD (Necklace n a)       by MULTI_MONO_EXHAUST
     = a^n

   With  CARD (monocoloured n a) = a   by CARD_monocoloured, the result follows.
*)
val CARD_multicoloured = store_thm(
  "CARD_multicoloured",
  ``!n a. 0 < n ==> (CARD (multicoloured n a) = a**n - a)``,
  REPEAT STRIP_TAC THEN
  `FINITE (multicoloured n a)` by SRW_TAC [][FINITE_multicoloured] THEN
  `FINITE (monocoloured n a)` by SRW_TAC [][FINITE_monocoloured] THEN
  `CARD (multicoloured n a) + CARD (monocoloured n a) =
      CARD ((multicoloured n a) UNION (monocoloured n a)) +
      CARD ((multicoloured n a) INTER (monocoloured n a))` by SRW_TAC [][CARD_UNION] THEN
  `CARD ((multicoloured n a) INTER (monocoloured n a)) = 0`
    by METIS_TAC [MULTI_MONO_DISJOINT, DISJOINT_DEF, CARD_EMPTY] THEN
  `CARD ((multicoloured n a) UNION (monocoloured n a)) = a**n`
    by METIS_TAC [MULTI_MONO_EXHAUST, CARD_Necklace] THEN
  `CARD (monocoloured n a) = a` by SRW_TAC [][CARD_monocoloured] THEN
  RW_TAC arith_ss []);

(* Theorem: (multicoloured n a) NOT empty when 1 < n /\ 1 < a *)
val multicoloured_NONEMPTY = store_thm(
  "multicoloured_NONEMPTY",
  ``!n a. 1 < n /\ 1 < a ==> (multicoloured n a) <> EMPTY``,
  REPEAT STRIP_TAC THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  `CARD (multicoloured n a) = a**n - a` by SRW_TAC [][CARD_multicoloured] THEN
  `a < a**n` by SRW_TAC [][EXP_LESS] THEN
  `0 <> CARD (multicoloured n a)` by RW_TAC arith_ss [] THEN
  METIS_TAC [FINITE_multicoloured, CARD_EMPTY]);

(* ------------------------------------------------------------------------- *)

(* For revised necklace proof using GCD. *)

(* Theorem: multicoloured l are not monocoloured *)
val multicoloured_not_monocoloured = store_thm(
  "multicoloured_not_monocoloured",
  ``!l n a. l IN multicoloured n a ==> ~(l IN monocoloured n a)``,
  SRW_TAC [][multicoloured_def]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
