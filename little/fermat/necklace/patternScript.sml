(* ------------------------------------------------------------------------- *)
(* Pattern Theory.                                                           *)
(* List patterns to be investigated by similar lists.                        *)
(* List sub-patterns to be investigated with period and order.               *)
(* ------------------------------------------------------------------------- *)

(*

This is applying the new cycle theory to the original Necklace Proof,
i.e. using the idea of action, but not explicitly mentioning it.

Cycle Theory
============

Necklaces are represented by linear lists of length n.

Since they are necklaces, it is natural to join them end to end. For example,
the following "cycling" necklaces are considered equivalent:

+--+--+--+--+--+--+--+
|2 |4 |0 |3 |1 |2 |3 |
+--+--+--+--+--+--+--+
|4 |0 |3 |1 |2 |3 |2 |
+--+--+--+--+--+--+--+
|0 |3 |1 |2 |3 |2 |4 |
+--+--+--+--+--+--+--+
|3 |1 |2 |3 |2 |4 |0 |
+--+--+--+--+--+--+--+
|1 |2 |3 |2 |4 |0 |3 |
+--+--+--+--+--+--+--+
|2 |3 |2 |4 |0 |3 |1 |
+--+--+--+--+--+--+--+
|3 |2 |4 |0 |3 |1 |2 | (next cycle identical to first)
+--+--+--+--+--+--+--+

We shall define and investigate the "cycle" operation on a list.

We shall consider a "similar" relation between lists via cycling.

We shall prove that "similar" is reflexive, symmetric and transitive.

Hence "similar" is an equivalence relation, giving partitions via
equivalent classes on necklaces of length n.

Cycle Order Theory
==================

The basic cycle theory considers necklaces of length n, ignoring its colors.

Ignoring colors, there are only two ways that guarantee cycling to the original:
(1) with 0 steps:  cycle 0 l = l
(2) with n steps:  cycle n l = l   where n = length of the list.

With colors, necklaces have patterns, which may consist of sub-patterns.

This give rise to the concept of order: the least number of steps to cycle
the necklace to its original. The trivial case of 0 steps is excluded to
make the least number interesting.

The main result of this investigation are:
(a) the order k must divide the length n.
(b) the order k of a list is the cardinality of the list's associate,
    the equivalent class of "similar" relation with the given list.

Example of order/associate:

Take LENGTH l = 4, e.g. l = [2;3;2;3]

   cycle 0 l = [2;3;2;3]
   cycle 1 l = [3;2;3;2]
   cycle 2 l = [2;3;2;3]
   cycle 3 l = [3;2;3;2]
   cycle 4 l = [2;3;2;3]

   So, similar in this set is true:
   similar l l
   similar l1 l2 ==> similar l2 l1
   similar l1 l2 /\ similar l2 l3 ==> similar l1 l3

   However, the size of this set is 2, not LENGTH l = 4.

   Hence for n = 4 (length of necklace), a = 2 (colors)
   The set of multicoloured necklaces = M, with CARD M = a^n - a = 2^4 - 2 = 16 - 2 = 14.

   Let the colors be {0,1}. The 14 multicoloured necklaces are:

   #01 [0;0;0;1] cycle with: #02, #04, #08, size of set = 4.
   #02 [0;0;1;0] (see #01)
   #03 [0;0;1;1] cycle with: #06, #12, #09, size of set = 4.
   #04 [0;1;0;0] (see #01)
   #05 [0;1;0;1] cycle with: #10,           size of set = 2.
   #06 [0;1;1;0] (see #03)
   #07 [0;1;1;1] cycle with: #14, #13, #11, size of set = 4.
   #08 [1;0;0;0] (see #01)
   #09 [1;0;0;1] (see #03)
   #10 [1;0;1;0] (see #05)
   #11 [1;0;1;1] (see #07)
   #12 [1;1;0;0] (see #03)
   #13 [1;1;0;1] (see #07)
   #14 [1;1;1;0] (see #07)

   That is, similar partitions the set, but the partitions are of different sizes.
   Note that: 4 + 4 + 2 + 4 = 14 = a^n - a.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "pattern";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* Get dependent theories from lib *)
open helperSetTheory helperListTheory cycleTheory;

(* ------------------------------------------------------------------------- *)
(* Two lists are similar if they are related through cycle.                  *)
(* ------------------------------------------------------------------------- *)

(* Define similar to relate two lists *)
val similar_def = Define`
  similar l1 l2 = ?n. l2 = cycle n l1
`;

(* set infix and overload for similar *)
val _ = set_fixity "==" (Infixl 480);
val _ = overload_on ("==", ``similar``);

(* ------------------------------------------------------------------------- *)
(* Know about Similar between Cycles.                                        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Only NIL can be similar to NIL. *)
val SIMILAR_NIL = store_thm(
  "SIMILAR_NIL",
  ``!l. l == [] \/ [] == l <=> (l = [])``,
  METIS_TAC [similar_def, CYCLE_NIL]);

(* Theorem: Only same length lists can be similar to each other. *)
val SIMILAR_LENGTH = store_thm(
  "SIMILAR_LENGTH",
  ``!l1 l2. l1 == l2 ==> (LENGTH l1 = LENGTH l2)``,
  METIS_TAC [similar_def, CYCLE_SAME_LENGTH]);

(* ------------------------------------------------------------------------- *)
(* Show that Similar is an equivalence relation.                             *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Reflexive] l == l *)
(* Proof:
   By CYCLE_0, cycle 0 l = l, and apply similar definition.
*)
val SIMILAR_REFL = store_thm(
  "SIMILAR_REFL",
  ``!l. l == l``,
  METIS_TAC [similar_def, CYCLE_0]);

(* Theorem: [Symmetric] l1 == l2 ==> l2 == l1 *)
(* Proof:
       l1 == l2
   ==> l2 = cycle n l1   for some n.
   ==> cycle m l2 = cycle m (cycle n l1)   where m+n=LENGTH l1
                  = l1                     by CYCLE_INV
   Since l1 = cycle m l2, l2 == l1.
*)
val SIMILAR_SYM = store_thm(
  "SIMILAR_SYM",
  ``!l1 l2. (l1 == l2) ==> (l2 == l1)``,
  SRW_TAC [][similar_def] THEN
  Cases_on `l1 = []` THEN1
  METIS_TAC [CYCLE_NIL] THEN
  `n MOD LENGTH l1 <= LENGTH l1`
    by METIS_TAC [LENGTH_NON_NIL, arithmeticTheory.MOD_LESS, arithmeticTheory.LESS_IMP_LESS_OR_EQ] THEN
  Q.EXISTS_TAC `LENGTH l1 - (n MOD LENGTH l1)` THEN
  METIS_TAC [CYCLE_MOD_LENGTH, CYCLE_INV]);

(* Theorem: [Transitive] l1 == l2 /\ l2 == l3 ==> l1 == l3 *)
(* Proof:
   l1 == l2  implies  l2 = cycle n1 l1   for some n1.
   l2 == l3  implies  l3 = cycle n2 l2   for some n2.

   Therefore l3 = cycle n2 l2
                = cycle n2 (cycle n1 l1)
                = cycle (n2+n1) l1  by CYCLE_ADD.
   Or l1 == l3.
*)
val SIMILAR_TRANS = store_thm(
  "SIMILAR_TRANS",
  ``!l1 l2 l3. (l1 == l2) /\ (l2 == l3) ==> (l1 == l3)``,
  METIS_TAC [similar_def, CYCLE_ADD]);

(* ------------------------------------------------------------------------- *)
(* Similar associates and their cardinality.                                 *)
(* ------------------------------------------------------------------------- *)

(* Define the associate of list: all those that are similar to the list. *)
val associate_def = Define `
  associate x = {y | x == y }
`;

(* ------------------------------------------------------------------------- *)
(* Know the associates.                                                      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: associate NIL is singleton {NIL}. *)
val ASSOCIATE_NIL = store_thm(
  "ASSOCIATE_NIL",
  ``associate [] = {[]}``,
  SRW_TAC [][associate_def, EXTENSION] THEN METIS_TAC [SIMILAR_NIL]);

(* Theorem: associate x is non-EMPTY. *)
val ASSOCIATE_NONEMPTY = store_thm(
  "ASSOCIATE_NONEMPTY",
  ``!x. associate x <> {}``,
  SRW_TAC [][associate_def, EXTENSION, SIMILAR_REFL] THEN
  METIS_TAC [SIMILAR_REFL]);

(* ------------------------------------------------------------------------- *)
(* To show: associates are FINITE.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The map (\n. cycle n l) from (count LENGTH l) -> (associate l)
            is surjective. *)
(* Proof:
   Essentially this is to prove:
   (1) l <> [] ==> ?n'. (cycle n l = cycle n' l) /\ n' < LENGTH l
   (2) l <> [] /\ n < LENGTH l ==> ?n'. cycle n l = cycle n' l
   Both are true by taking n' = n LENGTH l, by CYCLE_MOD_LENGTH.
*)
val ASSOCIATE_AS_IMAGE = store_thm(
  "ASSOCIATE_AS_IMAGE",
  ``!l. l <> [] ==> (associate l = IMAGE (\n. cycle n l) (count (LENGTH l)))``,
  SRW_TAC [][associate_def, similar_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [CYCLE_MOD_LENGTH, LENGTH_NIL,
             arithmeticTheory.MOD_LESS, arithmeticTheory.NOT_ZERO_LT_ZERO]);

(* Theorem: associate l is FINITE *)
(* Proof:
   When l = [], associate [] = {[]), hence FINITE.
   When l <> [], apply ASSOCIATE_AS_IMAGE to deduce it is FINITE.
*)
val FINITE_ASSOCIATE = store_thm(
  "FINITE_ASSOCIATE",
  ``!l. FINITE (associate l)``,
  STRIP_TAC THEN Cases_on `l` THEN
  METIS_TAC [ASSOCIATE_NIL, FINITE_SING,
             ASSOCIATE_AS_IMAGE, FINITE_COUNT, IMAGE_FINITE]);

(* ------------------------------------------------------------------------- *)
(* To give an estimate of CARD (associate l).                                *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Size of (associate l) <= LENGTH l *)
val CARD_ASSOCIATE_LE_LENGTH = store_thm(
  "CARD_ASSOCIATE_LE_LENGTH",
  ``!l. l <> [] ==> (CARD (associate l) <= LENGTH l)``,
  METIS_TAC [ASSOCIATE_AS_IMAGE, FINITE_COUNT, CARD_IMAGE_LE_CARD, CARD_COUNT]);

(* ------------------------------------------------------------------------- *)
(* Cycle sub-patterns                                                        *)
(* ------------------------------------------------------------------------- *)

(* We know: CYCLE_0     cycle 0 l = l.
       and: CYCLE_BACK  cycle (LENGTH l) l = l.
   Is there some  0 < k < LENGTH l such that   cycle k l = l ?
   If there is such a k, it is very special,
   by the DIVISION algorithm and nature of cycle.
*)

(* ------------------------------------------------------------------------- *)
(* Period of a list.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Define period k for a list l, as a predicate. *)
val period_def = Define`
  period l k = 0 < k /\ (cycle k l = l)
`;

(* Note:
   . 0 is excluded as a period.
   . Since LENGTH [] = 0, [] cannot have a period.
*)

(* Theorem: A non-NIL list has a period: its length. *)
(* Proof: by CYCLE_BACK. *)
val CYCLE_PERIOD_EXISTS = store_thm(
  "CYCLE_PERIOD_EXISTS",
  ``!l. l <> [] ==> period l (LENGTH l)``,
  SRW_TAC [][period_def, LENGTH_NON_NIL, CYCLE_BACK]);

(* Theorem: If list l has period k,
            then multiples of k are also periods. *)
(* Proof: by  induction and CYCLE_ADD. *)
val CYCLE_PERIOD_MULTIPLE = store_thm(
  "CYCLE_PERIOD_MULTIPLE",
  ``!l k n. 0 < n /\ period l k ==> period l (n*k)``,
  SRW_TAC [][period_def] THEN1
  RW_TAC arith_ss [arithmeticTheory.LESS_MULT2] THEN
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n = 0` THEN SRW_TAC [][] THEN
  `0 < n /\ ((SUC n)*k = n*k + k)` by RW_TAC arith_ss [arithmeticTheory.MULT_SUC] THEN
  METIS_TAC [CYCLE_ADD]);

(* Theorem: If list l has period k, and x == y, then k is also a period of y,
            i.e. period x k, and x == y ==> period y k. *)
(* Proof:
   x == y means ?n. y = cycle n x.
     cycle k (cycle n x)
   = cycle (k+n) x                by CYCLE_ADD
   = cycle (n+k) x
   = cycle n (cycle k x)          by CYCLE_ADD
   = cycle n x                    by k being a period
*)
val CYCLE_SIMILAR_PERIOD = store_thm(
  "CYCLE_SIMILAR_PERIOD",
  ``!k x y. (period x k) /\ (x == y) ==> (period y k)``,
  SRW_TAC [][period_def, similar_def] THEN
  METIS_TAC [CYCLE_ADD, arithmeticTheory.ADD_COMM]);

(* ------------------------------------------------------------------------- *)
(* Order of a list - or minimal period of a list.                            *)
(* ------------------------------------------------------------------------- *)

(* Define order as minimal period of a list. *)
val order_def = Define`
  order l = LEAST k. period l k
`;

(* Some examples
EVAL ``order [2;3;2;3]``;
val it = [] |- order [2; 3; 2; 3] = 2 : thm

EVAL ``order [2;3;2;3;2]``;
val it = [] |- order [2; 3; 2; 3; 2] = 5 : thm
*)

(* Theorem: The list order is indeed a period. *)
val CYCLE_ORDER_PERIOD = store_thm(
  "CYCLE_ORDER_PERIOD",
  ``!l. l <> [] ==> period l (order l)``,
  SRW_TAC [][order_def] THEN
  METIS_TAC [CYCLE_PERIOD_EXISTS, whileTheory.LEAST_INTRO]);

(* Theorem: If k < list order, k is not a period. *)
val CYCLE_ORDER_MINIMAL = store_thm(
  "CYCLE_ORDER_MINIMAL",
  ``!l k. l <> [] /\ (k < order l) ==> ~(period l k)``,
  SRW_TAC [][order_def] THEN
  METIS_TAC [CYCLE_PERIOD_EXISTS, whileTheory.LESS_LEAST]);

(* Theorem: The list order is positive and a period. *)
val CYCLE_ORDER_PROPERTY = store_thm(
  "CYCLE_ORDER_PROPERTY",
  ``!l. l <> [] ==> 0 < order l /\ (cycle (order l) l = l)``,
  METIS_TAC [CYCLE_ORDER_PERIOD, period_def]);

(* Theorem: cycle (n*(order l)) l = l. *)
val CYCLE_ORDER_MULTIPLE = store_thm(
  "CYCLE_ORDER_MULTIPLE",
  ``!l n. l <> [] ==> (cycle (n*(order l)) l = l)``,
  REPEAT STRIP_TAC THEN Cases_on `n = 0` THEN1 SRW_TAC [][CYCLE_0] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ORDER_PERIOD, CYCLE_PERIOD_MULTIPLE, period_def]);

(* Theorem: The list order divides its length: (LENGTH l) MOD (order l) = 0. *)
(* Proof:
   Let LENGTH 1 = n, and order l = k.
   Then n = q*k + r by DIVISION, with r < k and q*k <= n. Then
     l
   = cycle n l                 by CYCLE_BACK
   = cycle (q*k+r) l           by DIVISION
   = cycle (r+q*k) l           by arithmetic
   = cycle r (cycle (q*k) l)   by CYCLE_ADD
   = cycle r l                 by CYCLE_ORDER_PERIOD (cycle k l = l) and CYCLE_PERIOD_MULTIPLE
   If r <> 0,
   0 < r < k but cycle r l = l. This contradicts the minimal nature of k.
   Hence r = 0, or k divides n = LENGTH l.
*)
val CYCLE_ORDER_DIVIDES_LENGTH = store_thm(
  "CYCLE_ORDER_DIVIDES_LENGTH",
  ``!l. l <> [] ==> (LENGTH l MOD (order l) = 0)``,
  REPEAT STRIP_TAC THEN
  `?k. k = order l` by SRW_TAC [][] THEN
  `0 < k /\ (cycle k l = l)` by SRW_TAC [][CYCLE_ORDER_PROPERTY] THEN
  `?q r. r < k /\ (LENGTH l = q*k + r)` by METIS_TAC [arithmeticTheory.DA] THEN
  `_ = r + q*k` by RW_TAC arith_ss [] THEN
  `LENGTH l MOD order l = r` by METIS_TAC [arithmeticTheory.MOD_UNIQUE, arithmeticTheory.ADD_COMM] THEN
  Cases_on `r = 0` THEN1 RW_TAC arith_ss [] THEN
  `0 < r /\ r <= LENGTH l` by RW_TAC arith_ss [] THEN
  `l = cycle (LENGTH l) l` by SRW_TAC [][CYCLE_BACK] THEN
  `_ = cycle (r + q*k) l` by METIS_TAC [] THEN
  `_ = cycle r (cycle (q*k) l)` by METIS_TAC [CYCLE_ADD] THEN
  `_ = cycle r l` by METIS_TAC [CYCLE_ORDER_MULTIPLE] THEN
  METIS_TAC [CYCLE_ORDER_MINIMAL, period_def]);

(* ------------------------------------------------------------------------- *)
(* Similar lists have the same order.                                        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If list l has order k, and x == y, then k is also a order of y,
            i.e. order x k, and x == y ==> order y k. *)
(* Proof:
   By CYCLE_ORDER_PERIOD, order k is a period of y.
   Let order of y be k'.
   If k' < k, k' will be a period of x, which contradicts the minimal nature of k.
   Hence k' = k, or k is the order of y.
*)
val CYCLE_SIMILAR_ORDER = store_thm(
  "CYCLE_SIMILAR_ORDER",
  ``!x y. x <> [] /\ y <> [] /\ (x == y) ==> (order x = order y)``,
  REPEAT STRIP_TAC THEN
  `period x (order y) /\ period y (order x)`
    by METIS_TAC [CYCLE_ORDER_PERIOD, CYCLE_SIMILAR_PERIOD, SIMILAR_SYM] THEN
  Cases_on `order x < order y` THEN1 METIS_TAC [CYCLE_ORDER_MINIMAL] THEN
  Cases_on `order x = order y` THEN1 SRW_TAC [][] THEN
  `order y < order x` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ORDER_MINIMAL]);

(* Theorem: For k = order l, IMAGE (\n. cycle n l) (count k) = (associate l). *)
(* Proof:
   Show that the two sets are subset of each other.

   For an element  cycle j x  in IMAGE, with j < k.
   Since  j < k  and k <= LENGTH x, j <= LENGTH x.
   Hence (cycle j x) in (associate x).
   This shows IMAGE (\j. cycle j x) (count k) SUBSET (associate x).

   For an element y in associate x, x == y.
   That is, ?n. (n <= LENGTH x) /\ (y = cycle n x)
   Divide this n by k:   n = q*k + r with (n MOD k) < k. (arithmeticTheory.DA)
     cycle n x
   = cycle (r + q*k) x        by n = q*k + r
   = cycle r (cycle (q*k) x)  by CYCLE_ADD
   = cycle r x                by CYCLE_ORDER_MULTIPLE and cycle k x = x
   but (cycle r x) in IMAGE (\j. cycle j x) (count k),
   hence (cycle n x) in IMAGE (\j. cycle j x) (count k).
   This means (associate x) SUBSET IMAGE (\j. cycle j x) (count k).

   Essentially this is to prove:
   (1) l <> [] /\ n < order l ==> ?n'. cycle n l = cycle n' l, true by taking n' = n.
   (2) l <> [] ==> ?n'. (cycle n l = cycle n' l) /\ n' < order l
       Using cycle n l = cycle (n MOD LENGTH l) l,
       divide (n MOD LENGTH l) by (order l) and let n' = remainder.
*)
val ASSOCIATE_ORDER_MAP_IMAGE = store_thm(
  "ASSOCIATE_ORDER_MAP_IMAGE",
  ``!l. l <> [] ==> (IMAGE (\n. cycle n l) (count (order l)) = associate l)``,
  SRW_TAC [][associate_def, similar_def, EXTENSION, EQ_IMP_THM] THEN1 METIS_TAC [] THEN
  `0 < order l` by SRW_TAC [][CYCLE_ORDER_PROPERTY] THEN
  `?q r. (r < order l) /\ (n = q*(order l) + r)` by METIS_TAC [arithmeticTheory.DA] THEN
  `_ = r + q*(order l)` by RW_TAC arith_ss [] THEN
  `0 < LENGTH l` by METIS_TAC [LENGTH_NON_NIL] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_ORDER_MULTIPLE]);

(* Theorem: For order k, (\n. cycle n l) (count (order l)) (associate l) is surjective. *)
val ASSOCIATE_ORDER_MAP_SURJ = prove(
  ``!l. l <> [] ==> SURJ (\n. cycle n l) (count (order l)) (associate l)``,
  SRW_TAC [][IMAGE_SURJ, ASSOCIATE_ORDER_MAP_IMAGE]);

(* Theorem: For order k, x < k and y < k and x <= y and cycle x l = cycle y l ==> x = y *)
(* Proof:
   For two elements  cycle p x = cycle q x    with p, q < k,
   If p <> q, let p = q + d, then
     cycle p x
   = cycle (d + q) x      by p = q + d
   = cycle d (cycle q x)  by CYCLE_ADD
   = cycle d (cycle p x)  by given
   So there is y = cycle p x such that cycle d y = y, with d < p < k.
   By CYCLE_SIMILAR_PERIOD, this is also cycle d x = x, as x == y (by y = cycle p x).
   Hence there is d < k with cycle d x = x, contradicting the minimal nature of k unless d = 0.
*)
val ASSOCIATE_ORDER_MAP_INJ_IMP = store_thm(
  "ASSOCIATE_ORDER_MAP_INJ_IMP",
  ``!l x y. l <> [] /\ (x < (order l)) /\ (y < (order l)) /\
           (x <= y) /\ (cycle x l = cycle y l) ==> (x = y)``,
  REPEAT STRIP_TAC THEN
  `?k. k = order l` by SRW_TAC [][] THEN
  `0 < k` by SRW_TAC [][CYCLE_ORDER_PROPERTY] THEN
  `?d. y = x + d` by METIS_TAC [arithmeticTheory.LESS_EQ_EXISTS] THEN
  `_ = d + x` by RW_TAC arith_ss [] THEN (* reverse for CYCLE_ADD *)
  Cases_on `d = 0` THEN1 SRW_TAC [][] THEN(* solves case d = 0 *)
  `0 < d /\ d < k` by RW_TAC arith_ss [] THEN
  `cycle y l = cycle (d+x) l` by SRW_TAC [][] THEN
  `_ = cycle d (cycle x l)` by METIS_TAC [CYCLE_ADD] THEN
  `_ = cycle d (cycle y l)` by METIS_TAC [] THEN
  `l == (cycle y l)` by METIS_TAC [similar_def] THEN (* not (cycle y l) == l, non-ending *)
  `(cycle y l) == l` by SRW_TAC [][SIMILAR_SYM] THEN (* need below *)
  `period (cycle y l) d` by SRW_TAC [][period_def] THEN
  `period l d ` by METIS_TAC [CYCLE_SIMILAR_PERIOD] THEN
  METIS_TAC [CYCLE_ORDER_MINIMAL]); (* solves by contradiction *)

(* Theorem: For k = order l, (\n. cycle n l) (count k) (associate l) is injective. *)
(* Proof:
   This is to prove: for x,y in count k, cycle x l = cycle y l ==> x = y.
   The previous proof shows the case for x <= y.
   The case y <= x is symmetrical, by the same logic.

   Essentially this is to prove:
   (1) l <> [] /\ n < order l ==> ?n'. cycle n l = cycle n' l, true by taking n' = n.
   (2) l <> [] /\ n < order l /\ n' < order l /\  cycle n l = cycle n' l ==> n = n',
       true by ASSOCIATE_ORDER_MAP_INJ_IMP for two cases: n <= n' or n > n'.
*)
val ASSOCIATE_ORDER_MAP_INJ = prove(
  ``!l. l <> [] ==> INJ (\n. cycle n l) (count (order l)) (associate l)``,
  SRW_TAC [][associate_def, similar_def, INJ_DEF] THEN1 METIS_TAC [] THEN
  Cases_on `n <= n'` THENL [ALL_TAC, `n' <= n` by RW_TAC arith_ss []] THEN
  METIS_TAC [ASSOCIATE_ORDER_MAP_INJ_IMP]);

(* Theorem: For k = order l, (\n. cycle n l) (count k) (associate l) is bijective. *)
val ASSOCIATE_ORDER_MAP_BIJ = store_thm(
  "ASSOCIATE_ORDER_MAP_BIJ",
  ``!l. l <> [] ==> BIJ (\n. cycle n l) (count (order l)) (associate l)``,
  METIS_TAC [BIJ_DEF, ASSOCIATE_ORDER_MAP_INJ, ASSOCIATE_ORDER_MAP_SURJ]);

(* Theorem: CARD (associate l) = order l *)
(* Proof:
   Since the map (\n. cycle n l): (count (order l)) -> (associate l) is bijective,
   CARD (associate l) = CARD (count (order l))
                  = order l
*)
val CARD_ASSOCIATE_ORDER = store_thm(
  "CARD_ASSOCIATE_ORDER",
  ``!l. l <> [] ==> (CARD (associate l) = order l)``,
  METIS_TAC [ASSOCIATE_ORDER_MAP_BIJ, FINITE_BIJ_CARD_EQ,
             FINITE_COUNT, FINITE_ASSOCIATE, CARD_COUNT]);

(* Theorem: CARD (associate l) divides LENGTH l *)
val CARD_ASSOCIATE_DIVIDES_LENGTH = store_thm(
  "CARD_ASSOCIATE_DIVIDES_LENGTH",
  ``!l. l <> [] ==> ((LENGTH l) MOD (CARD (associate l)) = 0)``,
  METIS_TAC [CARD_ASSOCIATE_ORDER, CYCLE_ORDER_DIVIDES_LENGTH]);

(* ------------------------------------------------------------------------- *)
(* Cycle 1 and Order 1.                                                      *)
(* ------------------------------------------------------------------------- *)

(* Order of Cycle 1 *)

(* Theorem: A list with cycle 1 l = l iff order 1. *)
val CYCLE_1_EQ_ORDER_1 = store_thm(
  "CYCLE_1_EQ_ORDER_1",
  ``!l. l <> [] ==> ((cycle 1 l = l) <=> (order l = 1))``,
  SRW_TAC [][EQ_IMP_THM] THENL [
    SRW_TAC [][order_def] THEN
    `0 < LENGTH l`
      by METIS_TAC [LENGTH_NIL, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
    `1 <= LENGTH l` by RW_TAC arith_ss [] THEN
    `period l 1` by SRW_TAC [][period_def] THEN
    numLib.LEAST_ELIM_TAC THEN SRW_TAC [][] THEN1 METIS_TAC [] THEN
    Cases_on `n = 1` THEN SRW_TAC [][] THEN
    Cases_on `n = 0` THEN1 METIS_TAC [period_def] THEN
    `1 < n` by RW_TAC arith_ss [] THEN METIS_TAC [],
    METIS_TAC [CYCLE_ORDER_PERIOD, period_def]
  ]);

(* ------------------------------------------------------------------------- *)
(* Finally, primes enter into the picture.                                   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If length of a list is prime p, its order is either 1 or p. *)
(* Proof:
   By CYCLE_ORDER_DIVIDES_LENGTH, the order divides a prime, which is 1 or p.
*)
val CYCLE_ORDER_PRIME_LENGTH = store_thm(
  "CYCLE_ORDER_PRIME_LENGTH",
  ``!l. l <> [] /\ prime (LENGTH l) ==> (order l = 1) \/ (order l = LENGTH l)``,
  REPEAT STRIP_TAC THEN
  `?k. k = order l` by SRW_TAC [][] THEN
  `(0 < k) /\ ((LENGTH l) MOD k = 0)`
    by SRW_TAC [][CYCLE_ORDER_PROPERTY, CYCLE_ORDER_DIVIDES_LENGTH] THEN
  `LENGTH l = (LENGTH l DIV k)* k + (LENGTH l MOD k)`
    by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = (LENGTH l DIV k)* k` by SRW_TAC [][] THEN
  `divides k (LENGTH l)` by METIS_TAC [dividesTheory.divides_def] THEN
  METIS_TAC [dividesTheory.prime_def]);

(* Theorem: If length of a list is prime p, its associate size is 1 or p. *)
val CARD_ASSOCIATE_PRIME_LENGTH = store_thm(
  "CARD_ASSOCIATE_PRIME_LENGTH",
  ``!l. l <> [] /\ prime (LENGTH l) ==>
    (CARD (associate l) = 1) \/ (CARD (associate l) = LENGTH l)``,
  METIS_TAC [CYCLE_ORDER_PRIME_LENGTH, CARD_ASSOCIATE_ORDER]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
