(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Combinatorial proof.                            *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by J. Petersen in 1872:

Take p elements from q with repetitions in all ways, that is, in q^p ways.
The q sets with elements all alike are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted in sets of p. Hence p divides q^p - q.

This is a combinatorial proof without reference to Group Theory.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTnecklace";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
(* val _ = load "dividesTheory"; *)
open pred_setTheory listTheory;

(* Get dependent theories from lib *)
open helperNumTheory helperSetTheory helperListTheory cycleTheory;
(* Get dependent theories local *)
open necklaceTheory patternTheory;


(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof without Group Theory explicitly.                      *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Apply Cycle and Similarity theories to Necklaces.                         *)
(* ------------------------------------------------------------------------- *)

(* Relate necklaces to cycle *)

(* Theorem: If l is a Necklace, cycle n l is also a Necklace. *)
val NECKLACE_CYCLE = store_thm(
  "NECKLACE_CYCLE",
  ``!l. l IN Necklace n a ==> !k. (cycle k l) IN Necklace n a``,
  SRW_TAC [][Necklace_def, CYCLE_SAME_LENGTH, CYCLE_SAME_SET]);

(* Relate necklaces to similar *)

(* Theorem: If x is a Necklace and x == y, then y is also a Necklace. *)
val NECKLACE_SIMILAR = store_thm(
  "NECKLACE_SIMILAR",
  ``!x. x IN Necklace n a ==> !y. (x == y) ==> y IN Necklace n a``,
  METIS_TAC [similar_def, NECKLACE_CYCLE]);

(* ------------------------------------------------------------------------- *)
(* Similar as an Equivalence Relation and look at its Partition Sets         *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Similarity is an equivalence relation on Necklaces. *)
val SIMILAR_EQUIV_ON_NECKLACE = store_thm(
  "SIMILAR_EQUIV_ON_NECKLACE",
  ``!n a. similar equiv_on Necklace n a``,
  SRW_TAC [][equiv_on_def] THEN
  METIS_TAC [SIMILAR_REFL, SIMILAR_SYM, SIMILAR_TRANS]);

(* Define partitions of Necklaces by Similarity *)
val NecklacePartition_def = Define `
  NecklacePartition n a = partition similar (Necklace n a)
`;

(* Theorem: Elements in (NecklacePartition n a) are non-empty. *)
val NECKLACE_PARTITION_ELEMENT_NONEMPTY = store_thm(
  "NECKLACE_PARTITION_ELEMENT_NONEMPTY",
  ``!e k. e IN NecklacePartition n a ==> e <> {}``,
  METIS_TAC [NecklacePartition_def, SIMILAR_EQUIV_ON_NECKLACE, EMPTY_NOT_IN_partition]);

(* Theorem: Elements in (NecklacePartition n a) are finite. *)
val FINITE_NECKLACE_PARTITION_ELEMENT = store_thm(
  "FINITE_NECKLACE_PARTITION_ELEMENT",
  ``!e. e IN NecklacePartition n a ==> FINITE e``,
  METIS_TAC [NecklacePartition_def, SIMILAR_EQUIV_ON_NECKLACE, FINITE_partition, FINITE_Necklace]);

(* Relate partition to associate *)

(* Theorem: Elements in (NecklacePartition n a) are associates of its member. *)
val NECKLACE_ASSOCIATE = store_thm(
  "NECKLACE_ASSOCIATE",
  ``!e l. e IN NecklacePartition n a /\ l IN e ==> (e = associate l)``,
  SRW_TAC [][NecklacePartition_def, associate_def, partition_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [NECKLACE_SIMILAR, SIMILAR_TRANS, SIMILAR_SYM]);

(* Theorem: Members of elements in (NecklacePartition n a) are Necklaces. *)
val NECKLACE_IN_ASSOCIATE = store_thm(
  "NECKLACE_IN_ASSOCIATE",
  ``!e l. e IN NecklacePartition n a /\ l IN e ==> l IN Necklace n a``,
  SRW_TAC [][NecklacePartition_def, partition_def] THEN
  FULL_SIMP_TAC (srw_ss() ++ SET_SPEC_ss)[]);

(* Theorem: For e in (NecklacePartition n a), (CARD e) > 0. *)
(* Proof:
   Since e <> EMPTY  by NECKLACE_PARTITION_ELEMENT_NONEMPTY
   Hence 0 < CARD e  by CARD_EQ_0 and FINITE_NECKLACE_PARTITION_ELEMENT
*)
val CARD_NECKLACE_ASSOCIATE_NONZERO = store_thm(
  "CARD_NECKLACE_ASSOCIATE_NONZERO",
  ``!e. e IN NecklacePartition n a ==> 0 < CARD e``,
  REPEAT STRIP_TAC THEN
  `e <> EMPTY` by METIS_TAC [NECKLACE_PARTITION_ELEMENT_NONEMPTY] THEN
  `CARD e <> 0` by METIS_TAC [FINITE_NECKLACE_PARTITION_ELEMENT, CARD_EQ_0] THEN
  RW_TAC arith_ss []);

(* Theorem: For e in (NecklacePartition n a), (CARD e) divides n. *)
(* Proof:
   Since e <> EMPTY by CARD_NECKLACE_ASSOCIATE_NONZERO,
   Let l in e,
   This  means   l in Necklace n a by NECKLACE_IN_ASSOCIATE
   Since              n = LENGTH l by NECKLACE_PROPERTY,
   Hence       (CARD e) divides n  by CARD_ASSOCIATE_DIVIDES_LENGTH (in orderTheory)
   The proof assumes 0 < n. The case n = 0 is trivial when e <> EMPTY.
*)
val CARD_NECKLACE_ASSOCIATE_DIVIDES_LENGTH = store_thm(
  "CARD_NECKLACE_ASSOCIATE_DIVIDES_LENGTH",
  ``!e. e IN NecklacePartition n a ==> (n MOD (CARD e) = 0)``,
  REPEAT STRIP_TAC THEN
  `0 < CARD e` by METIS_TAC [CARD_NECKLACE_ASSOCIATE_NONZERO] THEN
  Cases_on `n = 0` THEN1 SRW_TAC [][arithmeticTheory.ZERO_MOD] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  `?l. l IN e` by METIS_TAC [NECKLACE_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
  `n = LENGTH l` by METIS_TAC [NECKLACE_IN_ASSOCIATE, NECKLACE_PROPERTY] THEN
  METIS_TAC [NECKLACE_ASSOCIATE, LENGTH_NIL, CARD_ASSOCIATE_DIVIDES_LENGTH]);

(* Relate cycle/similar to multicoloured necklaces *)

(* Theorem: [Closure of cycle for multicoloured necklaces]
            If l is a multicoloured necklace, cycle n l is also multicolored. *)
val multicoloured_CYCLE = store_thm(
  "multicoloured_CYCLE",
  ``!l n a. 0 < n /\ 0 < a /\ l IN multicoloured n a ==> !k. (cycle k l) IN multicoloured n a``,
  REPEAT STRIP_TAC THEN
  `l IN (Necklace n a)` by SRW_TAC [][multicoloured_Necklace] THEN
  `l <> []` by METIS_TAC [NECKLACE_NONNIL] THEN
  FULL_SIMP_TAC (srw_ss()) [multicoloured_def, monocoloured_def, NECKLACE_CYCLE, CYCLE_SAME_SET] THEN
  METIS_TAC [LENGTH_NIL, CYCLE_SAME_LENGTH]);

(* Theorem: [Closure of similar for multicoloured necklaces]
            Only multicoloured necklaces can be similar to each other. *)
val multicoloured_SIMILAR = store_thm(
  "multicoloured_SIMILAR",
  ``!x y n a. 0 < n /\ 0 < a /\ x IN multicoloured n a /\ (x == y) ==> y IN multicoloured n a``,
  METIS_TAC [similar_def, multicoloured_CYCLE]);

(* Theorem: Similarity is an equivalence relation on multicoloured necklaces. *)
(* Proof:
   Similarity is reflexive, symmetric and transitive on multicoloured necklaces.
*)
val SIMILAR_EQUIV_ON_multicoloured = store_thm(
  "SIMILAR_EQUIV_ON_multicoloured",
  ``!n a. similar equiv_on multicoloured n a``,
  SRW_TAC [][equiv_on_def] THEN
  METIS_TAC [SIMILAR_REFL, SIMILAR_SYM, SIMILAR_TRANS]);

(* ------------------------------------------------------------------------- *)
(* Investigate the monocoloured Similarity equivalence partitions.           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A monocoloured necklace l has cycle 1 l = l *)
val monocoloured_cycle1 = prove(
  ``!n a l. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> (cycle 1 l = l)``,
  SRW_TAC [][monocoloured_def] THEN
  `l <> []` by METIS_TAC [NECKLACE_NONNIL] THEN
  METIS_TAC [SING_HAS_CYCLE_1]);

(* Theorem: If a necklace l has cycle 1 l = l , it must be monocoloured *)
val cycle1_monocoloured = prove(
  ``!l n a. 0 < a /\ l IN Necklace n a /\ (cycle 1 l = l) ==> l IN monocoloured n a``,
  SRW_TAC [][monocoloured_def, Necklace_def] THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* Theorem: A Necklace l is monocoloured iff cycle 1 l = 1 *)
val monocoloured_iff_cycle1 = store_thm(
  "monocoloured_iff_cycle1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN Necklace n a ==>
     (l IN monocoloured n a <=> (cycle 1 l = l))``,
  METIS_TAC [monocoloured_Necklace, cycle1_monocoloured, monocoloured_cycle1]);

(* Order of monocoloured necklaces *)

(* Theorem: The order of a monocoloured necklace is 1 *)
val monocoloured_order1 = prove(
  ``!l n a. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> (order l = 1)``,
  REPEAT STRIP_TAC THEN
  `l <> []` by METIS_TAC [monocoloured_Necklace, NECKLACE_NONNIL] THEN
  `cycle 1 l = l` by METIS_TAC[monocoloured_Necklace, monocoloured_iff_cycle1] THEN
  METIS_TAC [CYCLE_1_EQ_ORDER_1]);

(* Theorem: A necklace l with order l = 1 must be monocoloured. *)
val order1_monocoloured = prove(
  ``!l n a. 0 < n /\ 0 < a /\ l IN Necklace n a /\ (order l = 1) ==> l IN monocoloured n a``,
  SRW_TAC [][monocoloured_iff_cycle1] THEN
  `l <> []` by METIS_TAC [NECKLACE_NONNIL] THEN
  METIS_TAC [CYCLE_1_EQ_ORDER_1]);


(* Theorem: A necklace with order l = 1 iff it is monocoloured *)
val monocoloured_iff_order1 = store_thm(
  "monocoloured_iff_order1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN Necklace n a ==>
    (l IN monocoloured n a <=> (order l = 1))``,
  METIS_TAC [monocoloured_order1, order1_monocoloured]);

(* Theorem: For a multicoloured necklace l, cycle 1 l <> l *)
val multicoloured_order_not1 = store_thm(
  "multicoloured_order_not1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN multicoloured n a ==> order l <> 1``,
  METIS_TAC [multicoloured_def, monocoloured_iff_order1, IN_DIFF]);

(* ------------------------------------------------------------------------- *)
(* Investigate the multicoloured Similarity equivalence partitions.            *)
(* ------------------------------------------------------------------------- *)

(* Define partitions of multicoloured Necklaces by Similarity *)
val MulticolouredPartition_def = Define `
  MulticolouredPartition n a = partition similar (multicoloured n a)
`;

(* Theorem: Multicoloured Partition n a is FINITE *)
val FINITE_multicoloured_PARTITION = store_thm(
  "FINITE_multicoloured_PARTITION",
  ``!n a. FINITE (MulticolouredPartition n a)``,
  METIS_TAC [MulticolouredPartition_def, SIMILAR_EQUIV_ON_multicoloured,
             FINITE_partition, FINITE_multicoloured]);

(* Theorem: For e IN (MulticolouredPartition n a), e <> EMPTY *)
val multicoloured_PARTITION_ELEMENT_NONEMPTY = store_thm(
  "multicoloured_PARTITION_ELEMENT_NONEMPTY",
  ``!e. e IN MulticolouredPartition n a ==> e <> {}``,
  METIS_TAC [MulticolouredPartition_def, SIMILAR_EQUIV_ON_multicoloured,
             EMPTY_NOT_IN_partition]);

(* Theorem: For e IN (MulticolouredPartition n a), FINIET e *)
val FINITE_multicoloured_PARTITION_ELEMENT = store_thm(
  "FINITE_multicoloured_PARTITION_ELEMENT",
  ``!e. e IN MulticolouredPartition n a ==> FINITE e``,
  METIS_TAC [MulticolouredPartition_def, SIMILAR_EQUIV_ON_multicoloured,
             FINITE_partition, FINITE_multicoloured]);

(* Theorem: (e IN MulticolouredPartition n a) /\ (l IN e) ==> (e = associate l) *)
val multicoloured_ASSOCIATE = store_thm(
  "multicoloured_ASSOCIATE",
  ``!e l. e IN MulticolouredPartition n a /\ l IN e ==> (e = associate l)``,
  SRW_TAC [][MulticolouredPartition_def] THEN
  `?x. x IN multicoloured n a /\ (e = {y | y IN multicoloured n a /\ x == y})`
     by (FULL_SIMP_TAC (srw_ss()) [partition_def] THEN METIS_TAC []) THEN
  SRW_TAC [][associate_def] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  SRW_TAC [][EQ_IMP_THM, EXTENSION] THENL [
    METIS_TAC [SIMILAR_TRANS, SIMILAR_SYM],
    ALL_TAC,
    METIS_TAC [SIMILAR_TRANS, SIMILAR_SYM]
  ] THEN
  Cases_on `n = 0` THEN1 FULL_SIMP_TAC (srw_ss()) [multicoloured_0] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  Cases_on `a = 0` THEN1 FULL_SIMP_TAC (srw_ss()) [multicoloured_EMPTY] THEN
  `0 < a` by RW_TAC arith_ss [] THEN
  METIS_TAC [multicoloured_SIMILAR]);

(* Theorem: (e IN MulticolouredPartition n a) /\ (l IN e) ==> l IN (multicoloured n a) *)
val multicoloured_IN_ASSOCIATE = store_thm(
  "multicoloured_IN_ASSOCIATE",
  ``!e l. e IN MulticolouredPartition n a /\ l IN e ==> l IN multicoloured n a``,
  SRW_TAC [][MulticolouredPartition_def, partition_def] THEN
  FULL_SIMP_TAC (srw_ss() ++ SET_SPEC_ss)[]);

(* Theorem: The order of a prime-length multicoloured necklace is p. *)
(* Proof:
   By CYCLE_ORDER_PRIME_LENGTH, the possible order for a prime p is 1 or p.
   Since order <> 1 for multicoloured necklaces, by multicoloured_order_not1,
   the result follows.
*)
val multicoloured_prime_order = store_thm(
  "multicoloured_prime_order",
  ``!l p a. 1 < a /\ prime p /\ l IN multicoloured p a ==> (order l = p)``,
  REPEAT STRIP_TAC THEN
  `1 < p` by SRW_TAC [][dividesTheory.ONE_LT_PRIME] THEN
  `0 < p /\ 0 < a` by RW_TAC arith_ss [] THEN
  `l <> []` by METIS_TAC [multicoloured_Necklace, NECKLACE_NONNIL] THEN
  `p = LENGTH l` by METIS_TAC [multicoloured_Necklace, NECKLACE_PROPERTY] THEN
  `order l <> 1` by METIS_TAC [multicoloured_order_not1] THEN
  METIS_TAC [CYCLE_ORDER_PRIME_LENGTH]);

(* Theorem: For length prime p, e IN (MulticolouredPartition p a), (CARD e = p) *)
(* Proof:
   Since  CARD e = CARD (associate l) = order l = p, where l IN e.
*)
val CARD_prime_multicoloured_ASSOCIATE = store_thm(
  "CARD_prime_multicoloured_ASSOCIATE",
  ``!e p a. 1 < a /\ prime p /\ e IN MulticolouredPartition p a ==> (CARD e = p)``,
  REPEAT STRIP_TAC THEN
  `e <> {}` by METIS_TAC [multicoloured_PARTITION_ELEMENT_NONEMPTY] THEN
  `?l. l IN  e` by SRW_TAC [][MEMBER_NOT_EMPTY] THEN
  `e = associate l` by METIS_TAC [multicoloured_ASSOCIATE] THEN
  `l IN (multicoloured p a)` by METIS_TAC [multicoloured_IN_ASSOCIATE] THEN
  `1 < p` by SRW_TAC [][dividesTheory.ONE_LT_PRIME] THEN
  `0 < p /\ 0 < a` by RW_TAC arith_ss [] THEN
  `l <> []` by METIS_TAC [multicoloured_Necklace, NECKLACE_NONNIL] THEN
  `CARD e = order l` by SRW_TAC [][CARD_ASSOCIATE_ORDER] THEN
  METIS_TAC [multicoloured_prime_order]);

(* Theorem: For the (multicoloured p a) with prime length,
            CARD (multicoloured p a) = p* CARD (MulticolouredPartition p a) *)
(* Proof:
   By partition_CARD,

     CARD (multicoloured p a)
   = SIGMA CARD (partition similar (multicoloured n a)))
   = SIGMA CARD (associate l)  where l IN (multicoloured n a)
   = SIGMA (order l)       by CARD_ASSOCIATE_ORDER
   = SIGMA (p)             by order of a multicoloured necklace <> 1
                           and for prime length, order = 1 or p.
   = p * CARD (MulticolouredPartition p a)  by SIGMA_CARD_CONSTANT
*)
val CARD_prime_multicoloured = store_thm(
  "CARD_prime_multicoloured",
  ``!p a. 1 < a /\ prime p ==>
    (CARD (multicoloured p a) = p * CARD (MulticolouredPartition p a))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `CARD (multicoloured p a) = SIGMA CARD (MulticolouredPartition p a)`
     by METIS_TAC [MulticolouredPartition_def, partition_CARD,
                   SIMILAR_EQUIV_ON_multicoloured, FINITE_multicoloured] THEN
  METIS_TAC [SIGMA_CARD_CONSTANT, CARD_prime_multicoloured_ASSOCIATE, FINITE_multicoloured_PARTITION]);

(* Theorem: [Fermat's Little Theorem]
            For 0 < a < p, and prime p, (a^p - a) MOD p = 0 *)
(* Proof:
   The case a = 1 is trivial.
   For 1 < a, apply CARD_prime_multicoloured:

      CARD (multicoloured p a) = p* CARD (MulticolouredPartition p a)
   or                  a^p - a = p* k  for some k
   Hence  (a^p - a) MOD p = 0.
*)
val FERMAT_LITTLE1 = store_thm(
  "FERMAT_LITTLE1",
  ``!p a. prime p ==> ((a**p - a) MOD p = 0)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Cases_on `(a = 1) \/ (a = 0)` THEN SRW_TAC [][] THEN
  SRW_TAC [ARITH_ss][arithmeticTheory.EXP_1, arithmeticTheory.ZERO_EXP] THEN
  `1 < a` by RW_TAC arith_ss [] THEN
  `a**p - a = CARD (multicoloured p a)` by METIS_TAC [CARD_multicoloured] THEN
  `_ = p* CARD (MulticolouredPartition p a)` by SRW_TAC [][CARD_prime_multicoloured] THEN
  METIS_TAC [arithmeticTheory.MULT_EQ_DIV]);

(* Theorem: For 0 < a < p, and prime p, p divids (a^p - a) *)
(* Proof:
   Since (a^p - a) MOD p = 0, by Fermat_Little_Theorem, p divides (a^p - a).
*)
val FERMAT_LITTLE2 = store_thm(
  "FERMAT_LITTLE2",
  ``!p a. prime p ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  METIS_TAC [FERMAT_LITTLE1, DIVIDES_MOD_0]);

(* ------------------------------------------------------------------------- *)

(* For revised necklace proof using GCD. *)

(* Theorem: For multicoloured l, prime (LENGTH l), the map (count (LENGTH l) to (associate l) is injective *)
(* Proof:
   Essentially this is to show:
   Let p = LENGTH l,
   For all x < p, y < p,  cycle x l = cycle y l ==> x = y.
   Suppose not,
   then there are x, y such that x <> y, and cycle x l = cycle y l.
   Let l' = cycle x l. Assume x < y, then y = d + x, d = difference, and 0 < d < p.
   cycle d l' = cycle d (cycle x l) = cycle (d+x) l = cycle y l = l'
   but cycle p l' = l'   by CYCLE_BACK,
   so cycle (gcd d p) l' = l', by CYCLE_GCD_BACK.
   For prime p, 0 < d < p, gcd d p = 1. This contradicts monocoloured_iff_cycle1.
   Similar for x > y.
*)
val prime_multicoloured_associate_injection = store_thm(
  "prime_multicoloured_associate_injection",
  ``!l p a. prime p /\ l IN multicoloured p a ==> INJ (\n. cycle n l) (count p) (associate l)``,
  SRW_TAC [][associate_def, INJ_DEF] THEN1
  METIS_TAC [similar_def] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [multicoloured_EMPTY, MEMBER_NOT_EMPTY] THEN
  `0 < a` by DECIDE_TAC THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `?l'. cycle n l = l'` by SRW_TAC [][] THEN
  `l' IN multicoloured p a /\ (LENGTH l' = LENGTH l)` by METIS_TAC [multicoloured_CYCLE, CYCLE_SAME_LENGTH] THEN
  `cycle p l' = l'` by METIS_TAC [CYCLE_BACK, multicoloured_Necklace, NECKLACE_PROPERTY] THEN
  Cases_on `n < n'` THENL [
    `?d. n' = d + n` by METIS_TAC [arithmeticTheory.LESS_ADD] THEN
    `0 < d /\ d < p` by RW_TAC arith_ss [] THEN
    `cycle d l' = l'` by METIS_TAC [CYCLE_ADD] THEN
    `cycle (gcd d p) l' = l'` by METIS_TAC [CYCLE_GCD_BACK] THEN
    `gcd d p = 1` by METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD, gcdTheory.GCD_SYM] THEN
    METIS_TAC [monocoloured_iff_cycle1, multicoloured_Necklace, multicoloured_not_monocoloured],
    `n' < n` by DECIDE_TAC THEN
    `?d. n = d + n'` by METIS_TAC [arithmeticTheory.LESS_ADD] THEN
    `0 < d /\ d < p` by RW_TAC arith_ss [] THEN
    `cycle d l' = l'` by METIS_TAC [CYCLE_ADD] THEN
    `cycle (gcd d p) l' = l'` by METIS_TAC [CYCLE_GCD_BACK] THEN
    `gcd d p = 1` by METIS_TAC [dividesTheory.NOT_LT_DIVIDES, gcdTheory.PRIME_GCD, gcdTheory.GCD_SYM] THEN
    METIS_TAC [monocoloured_iff_cycle1, multicoloured_Necklace, multicoloured_not_monocoloured]
  ]);

(* Theorem: For multicoloured l, the map (count (LENGTH l) to (associate l) is surjective *)
(* Proof:
   Essentially this is to show:
   l == x ==> ?k. k < (LENGTH l) /\ (cycle k l = x),
   l == x ==> ?h. cycle h l = x.
   Take k = h MOD (LENGTH l), then apply CYCLE_MOD_LENGTH.
*)
val multicoloured_associate_surjection = store_thm(
  "multicoloured_associate_surjection",
  ``!l n a. l IN multicoloured n a ==> SURJ (\k. cycle k l) (count n) (associate l)``,
  SRW_TAC [][associate_def, SURJ_DEF] THEN1
  METIS_TAC [similar_def] THEN
  Cases_on `n = 0` THEN1
  METIS_TAC [multicoloured_0, MEMBER_NOT_EMPTY] THEN
  `0 < n` by DECIDE_TAC THEN
  `LENGTH l = n` by METIS_TAC [multicoloured_Necklace, NECKLACE_PROPERTY] THEN
  `l <> []` by METIS_TAC [LENGTH_NIL, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [similar_def, CYCLE_MOD_LENGTH, arithmeticTheory.MOD_LESS]);

(* Theorem: For multicoloured l, prime (LENGTH l), the map (count (LENGTH l) to (associate l) is bijective *)
(* Proof: from injection and surjection. *)
val prime_multicoloured_associate_bijection = store_thm(
  "prime_multicoloured_associate_bijection",
  ``!l p a. prime p /\ l IN multicoloured p a ==> BIJ (\n. cycle n l) (count p) (associate l)``,
  METIS_TAC [BIJ_DEF, prime_multicoloured_associate_injection, multicoloured_associate_surjection]);

(* Theorem: For multicoloured l, prime (LENGTH l), size of (associate l) = (LENGTH l) *)
(* Proof: by the bijection between (associate l) and (count (LENGTH l)) when (LENGTH l) is prime. *)
val card_prime_multicoloured_associate = store_thm(
  "card_prime_multicoloured_associate",
  ``!l p a. prime p /\ l IN multicoloured p a ==> (CARD (associate l) = p)``,
  METIS_TAC [prime_multicoloured_associate_bijection,
             FINITE_BIJ_CARD_EQ, FINITE_ASSOCIATE, FINITE_COUNT, CARD_COUNT]);

(* Theorem: For 0 < a < p, and prime p, p divids (a^p - a) *)
(* Proof:
   Since p is prime, 0 < p.
   When 0 < p, CARD (multicoloured p a) = a ** p - a   by CARD_multicoloured.
   For all l IN multicoloured p a, LENGTH l = p        by NECKLACE_PROPERTY
   CARD (associate l) = p                              by card_prime_multicoloured_associate
   But $== equiv_on multicoloured n a                  by SIMILAR_EQUIV_ON_multicoloured
   and (associate l) IN partition $== (multicoloured p a)   by partition_def
   hence CARD (multicoloured p a) = p * CARD (partition $== (multicoloured p a))  by equal_partition_CARD
   Thus   p divides (a**p - a)                         by divides_def;
*)
val FERMAT_LITTLE_GCD = store_thm(
  "FERMAT_LITTLE_GCD",
  ``!p a. prime p ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `a**p - a = CARD (multicoloured p a)` by METIS_TAC [CARD_multicoloured] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [arithmeticTheory.EXP_EQ_0, arithmeticTheory.SUB_0, dividesTheory.ALL_DIVIDES_0] THEN
  `0 < a` by DECIDE_TAC THEN
  `!l. l IN (multicoloured p a) ==> (associate l) IN partition $== (multicoloured p a)`
     by SRW_TAC [][partition_def, associate_def, SIMILAR_EQUIV_ON_multicoloured] THENL [
    Q.EXISTS_TAC `l` THEN
    SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
    METIS_TAC [multicoloured_SIMILAR],
    `!l. l IN (multicoloured p a) ==> (CARD (associate l) = p)`
        by METIS_TAC [card_prime_multicoloured_associate] THEN
    `!t. t IN (partition $== (multicoloured p a)) ==> ? l. l IN (multicoloured p a) /\ (t = associate l)`
     by (ASM_SIMP_TAC (srw_ss())[partition_def, associate_def,
                                 SIMILAR_EQUIV_ON_multicoloured] THEN
         GEN_TAC THEN DISCH_THEN (Q.X_CHOOSE_THEN `x` STRIP_ASSUME_TAC) THEN
         Q.EXISTS_TAC `x` THEN
         SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
         METIS_TAC [multicoloured_SIMILAR]) THEN
    `!t. t IN (partition $== (multicoloured p a)) ==> (CARD t = p)`
        by METIS_TAC [] THEN
   `CARD (multicoloured p a) = p * CARD (partition $== (multicoloured p a))`
      by METIS_TAC [SIMILAR_EQUIV_ON_multicoloured, FINITE_multicoloured,
                    equal_partition_CARD] THEN
   METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_COMM]
  ]);

(* Another proof of monocoloured_iff_cycle1 according to the paper. *)

val HD_APPEND = store_thm(
  "HD_APPEND",
  ``!h t l. (HD (h::t ++ l) = h)``,
  SRW_TAC [][]);

val EL_LAST_TAIL = store_thm(
  "EL_LAST_TAIL",
  ``!h t n. 0 <> n ==> (EL (n-1) t = EL n (h::t))``,
  REPEAT STRIP_TAC THEN
  `n = SUC (n-1)` by DECIDE_TAC THEN
  METIS_TAC [EL, TL]);

(* Theorem: head of (cycle n l) = element n l *)
val CYCLE_HEAD_TO_ELEMENT = store_thm(
  "CYCLE_HEAD_TO_ELEMENT",
  ``!n. n < LENGTH l ==> (HD (cycle n l) = EL n l)``,
  Induct_on `l` THENL [
    `!n. ~(n < 0)` by RW_TAC arith_ss [] THEN
    METIS_TAC [LENGTH_NIL],
    SRW_TAC [][CYCLE_EQ_DROP_TAKE, arithmeticTheory.LESS_IMP_LESS_OR_EQ] THEN
    `n - 1 < LENGTH l` by DECIDE_TAC THEN
    `HD (DROP (n-1) l ++ TAKE (n-1) l) = EL (n-1) l` by METIS_TAC [CYCLE_EQ_DROP_TAKE, arithmeticTheory.LESS_IMP_LESS_OR_EQ] THEN
    `LENGTH (DROP (n-1) l) = LENGTH l - (n - 1)` by METIS_TAC [LENGTH_DROP] THEN
    `LENGTH (DROP (n-1) l) <> 0` by DECIDE_TAC THEN
    `DROP (n-1) l <> []` by METIS_TAC [LENGTH_NIL] THEN
    `HD (DROP (n - 1) l ++ h::TAKE (n - 1) l) = EL (n-1) l` by METIS_TAC [HD_APPEND, list_CASES] THEN
    SRW_TAC [][] THEN
    METIS_TAC [EL_LAST_TAIL]
  ]);

(* Theorem: If all elements are the same, the set is SING. *)
(* Proof: (by contradiction)
   Since there are elements, set is not EMPTY.
   Suppose the set is not SING, then there are at least two distinct elements x, y in set.
   x IN set l ==> ?h. x = EL h l
   y IN set l ==> ?k. y = EL k l
   But all elements are the same, hence x = y, contradicting (distinct) x <> y.
*)
val SAME_ELEMENT_LIST_SING_SET = store_thm(
  "SAME_ELEMENT_LIST_SING_SET",
  ``!h t. (!n. n < LENGTH (h::t) ==> (EL n (h::t) = h)) ==> SING (set (h::t))``,
  REPEAT STRIP_TAC THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `h IN set (h::t)` by SRW_TAC [][] THEN
  `set (h::t) <> EMPTY` by METIS_TAC [MEMBER_NOT_EMPTY] THEN
  `?c. c = CHOICE (set (h::t))` by SRW_TAC [][] THEN
  Cases_on `h = c` THENL [
    `REST (set (h::t)) <> EMPTY` by METIS_TAC [SING_IFF_EMPTY_REST] THEN
    `?k. k IN REST (set (h::t)) /\ k <> h` by METIS_TAC [MEMBER_NOT_EMPTY, CHOICE_NOT_IN_REST] THEN
    METIS_TAC [REST_SUBSET, SUBSET_DEF, IN_LIST_TO_SET, MEM_EL],
    METIS_TAC [CHOICE_DEF, IN_LIST_TO_SET, MEM_EL]
  ]);

(* Theorem: a list of cycle 1 has SING set. *)
val CYCLE_1_HAS_SING = store_thm(
  "CYCLE_1_HAS_SING",
  ``!l. l <> [] /\ (cycle 1 l = l) ==> SING (set l)``,
  REPEAT STRIP_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN
  `!n. cycle n l = l` by METIS_TAC [CYCLE_MULTIPLE_BACK, arithmeticTheory.MULT_RIGHT_1] THEN
  `!n. n < LENGTH l ==> (HD (cycle n l) = EL n l)` by METIS_TAC [CYCLE_HEAD_TO_ELEMENT] THEN
  `!n. n < LENGTH l ==> (EL n l = h)` by METIS_TAC [HD] THEN
  METIS_TAC [SAME_ELEMENT_LIST_SING_SET]);

(* Theorem: A list has cycle 1 iff SING set *)
val CYCLE_1_IFF_SET_SING = store_thm(
  "CYCLE_1_IFF_SET_SING",
  ``!l. l <> [] /\ (cycle 1 l = l) <=> SING (set l)``,
  SRW_TAC [][EQ_IMP_THM] THEN1
  METIS_TAC [CYCLE_1_HAS_SING] THEN1
  METIS_TAC [NOT_EMPTY_SING, SING_DEF, LIST_TO_SET_THM] THEN
  METIS_TAC [SING_HAS_CYCLE_1]);

(* Theorem: A necklace l is monocoloured iff cycle 1 l = 1 *)
val monocoloured_iff_cycle1' = store_thm(
  "monocoloured_iff_cycle1'",
  ``!l n a. 0 < n /\ 0 < a /\ l IN Necklace n a ==> (l IN monocoloured n a <=> (cycle 1 l = l))``,
  SRW_TAC [][monocoloured_def, EQ_IMP_THM] THEN1
  METIS_TAC [NECKLACE_NONNIL, CYCLE_1_IFF_SET_SING] THEN
  METIS_TAC [CYCLE_1_IFF_SET_SING]);

(* end of alternative proof of monocoloured_iff_cycle1 *)

(* ------------------------------------------------------------------------- *)

val _ = export_theory();

(*===========================================================================*)
