(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Number-theoretic Proof.                         *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Number Theory)
=======================================

For a prime p, consider the set of residue p = { i | 0 < i < p }.

This set is non-empty since prime p > 1.

For a fixed a in residue p, multiply each element in the above set
by a and take MOD p, i.e. form this coset { (a*i) MOD p | 0 < i < p }.

These two sets are the same because:
(1) 0 < a < p implies 0 < (a*i) MOD p < p.
    The crucial part is to show:
    a MOD p = a <> 0 and i MOD p = i <> 0 implies (a*i) MOD p <> 0,
    but this is the contrapositive of Euclid's Lemma:
      If prime p divides a*b, then (p divides a) or (p divides b).
(2) If (a*i) MOD p = (a*j) MOD p, then i = j.
    This is due to left-cancellation for MOD p when p is prime,
    again given by Euclid Lemma:
        (a*i) MOD p = (a*j) MOD
    ==> (a*i - a*j) MOD p = 0
    ==> (a*(i - j)) MOD p = 0
    ==>     (i - j) MOD p = 0          since a MOD p <> 0
    ==>           i MOD p = j MOD p    assume i > j, otherwise switch i, j.
    ==>                 i = j          as 0 < i < p, 0 < j < p.

Hence for prime p, and any a in (residue p),

  (residue p) = IMAGE (row p a) (residue p)   where row p a x = (a*x) MOD p.

Take the product of elements in each set, MOD p, and they are equal:

  PROD_SET (residue p) = PROD_SET (IMAGE (row p a) (residue p))

Computation by induction gives:

  PROD_SET (residue p) = (FACT (p-1)) MOD p,  and
  PROD_SET (IMAGE (row p a) (residue p)) = (a**(p-1) * FACT (p-1)) MOD p

Hence   (FACT (p-1) * a**(p-1)) MOD p = (FACT (p-1)) MOD p

Since a prime p cannot divide FACT (p-1), cancellation law applies, giving:

         a**(p-1) MOD p = 1, which is Fermat's Identity.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "AllFLTnumber";

(* open dependent theories *)
open pred_setTheory;

(* Part 1: Basis ----------------------------------------------------------- *)

(* Theorem: If n > 0, (m MOD n = 0) <=> divides n m *)
val MOD_0_DIVIDES = store_thm(
  "MOD_0_DIVIDES",
  ``!n m. 0 < n ==> ((m MOD n = 0) <=> divides n m)``,
  SRW_TAC [][dividesTheory.divides_def, EQ_IMP_THM] THEN
  METIS_TAC [arithmeticTheory.DIVISION, arithmeticTheory.ADD_0, arithmeticTheory.MOD_EQ_0]);

(* Theorem: If n > 0, a MOD n = b MOD n ==> (a - b) MOD n = 0 *)
val MOD_EQ_DIFF = store_thm(
  "MOD_EQ_DIFF",
  ``!n a b. 0 < n /\ (a MOD n = b MOD n) ==> ((a - b) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  `a = (a DIV n)*n + a MOD n` by METIS_TAC [arithmeticTheory.DIVISION] THEN
  `b = (b DIV n)*n + b MOD n` by METIS_TAC [arithmeticTheory.DIVISION] THEN
  `a - b = (a DIV n)*n - (b DIV n)*n` by RW_TAC arith_ss [] THEN
  `_ = ((a DIV n) - (b DIV n))*n` by RW_TAC arith_ss [] THEN
  METIS_TAC [arithmeticTheory.MOD_EQ_0]);
(* Note: The reverse is true only when a >= b:
         (a-b) MOD n = 0 cannot imply a MOD n = b MOD n *)

(* Theorem: if n > 0, a >= b, then (a - b) MOD n = 0 <=> a MOD n = b MOD n *)
val MOD_EQ = store_thm(
  "MOD_EQ",
  ``!n a b. 0 < n /\ b <= a ==> (((a - b) MOD n = 0) <=> (a MOD n = b MOD n))``,
  SRW_TAC [][EQ_IMP_THM] THEN1 (
    `?k. (a-b) = k*n` by METIS_TAC [MOD_0_DIVIDES, dividesTheory.divides_def] THEN
    `a = k*n + b` by RW_TAC arith_ss [] THEN
    METIS_TAC [arithmeticTheory.MOD_TIMES]) THEN
  SRW_TAC [][MOD_EQ_DIFF]);

(* Theorem: For n > 0, (j MOD n * k) MOD n = (j * k) MOD n *)
val MOD_TIMES_TWO = store_thm(
  "MOD_TIMES_TWO",
  ``!n. 0 < n ==> !j k. (j MOD n * k) MOD n = (j * k) MOD n``,
  REPEAT STRIP_TAC THEN
  `(j MOD n * k) MOD n = (((j MOD n) MOD n) * (k MOD n)) MOD n`
    by SRW_TAC [][arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((j MOD n) * (k MOD n)) MOD n` by SRW_TAC [][arithmeticTheory.MOD_MOD] THEN
  SRW_TAC [][arithmeticTheory.MOD_TIMES2]);

(* Theorem: For all f: s -> t, (IMAGE f s) subset t *)
val MAP_IMAGE_SUBSET = store_thm(
  "MAP_IMAGE_SUBSET",
  ``!f s t. (!x. x IN s ==> f x IN t) ==> (IMAGE f s) SUBSET t``,
  SRW_TAC [][IMAGE_DEF, SUBSET_DEF] THEN
  RW_TAC std_ss []);

(* Theorem: For finite s, self injection means INJ f s (IMAGE f s) *)
val INJ_INJ_IMAGE = store_thm(
  "INJ_INJ_IMAGE",
  ``!f s. INJ f s s ==> INJ f s (IMAGE f s)``,
  SRW_TAC [][INJ_DEF] THEN
  Q.EXISTS_TAC `x` THEN
  RW_TAC std_ss []);

(* Theorem: For finite s, self injection means IMAGE f s = s *)
val FINITE_INJ_IMAGE_EQ = store_thm(
  "FINITE_INJ_IMAGE_EQ",
  ``!s f. FINITE s /\ INJ f s s ==> (IMAGE f s = s)``,
  REPEAT STRIP_TAC THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `FINITE (IMAGE f s)` by SRW_TAC [][IMAGE_FINITE] THEN
  `INJ f s (IMAGE f s)` by RW_TAC std_ss [INJ_INJ_IMAGE] THEN
  `!x. x IN s ==> f x IN s` by METIS_TAC [INJ_DEF] THEN
  `(IMAGE f s) SUBSET s` by SRW_TAC [][MAP_IMAGE_SUBSET] THEN
  `(IMAGE f s) PSUBSET s` by SRW_TAC [][PSUBSET_DEF] THEN
  `CARD (IMAGE f s) < CARD s` by SRW_TAC [][CARD_PSUBSET] THEN
  METIS_TAC [PHP]); (* invoke Pigeon-hole Principle *)

(* ------------------------------------------------------------------------- *)
(* Product of set elements.                                                  *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* PROD IMAGE and PROD_SET -- similar to SUM_IMAGE and SUM_SET               *)
(* ------------------------------------------------------------------------- *)

(* ----------------------------------------------------------------------
    PROD_IMAGE

    This construct is the same as standard mathematics \Pi operator:

     \Pi_{x\in P}{f(x)} = PROD_IMAGE f P

    Where f's range is the natural numbers and P is finite.
   ---------------------------------------------------------------------- *)

(* Define PROD_IMAGE similar to SUM_IMAGE *)
val PROD_IMAGE_DEF = Define `PROD_IMAGE f s = ITSET (\e acc. f e * acc) s 1`;

(* Theorem: property of PROD_IMAGE *)
val PROD_IMAGE_THM = store_thm(
  "PROD_IMAGE_THM",
  ``!f. (PROD_IMAGE f {} = 1) /\
        (!e s. FINITE s ==>
          (PROD_IMAGE f (e INSERT s) = f e * PROD_IMAGE f (s DELETE e)))``,
  REPEAT STRIP_TAC THEN1
    SIMP_TAC (srw_ss()) [ITSET_THM, PROD_IMAGE_DEF] THEN
  SIMP_TAC (srw_ss()) [PROD_IMAGE_DEF] THEN
  Q.ABBREV_TAC `g = \e acc. f e * acc` THEN
  Q_TAC SUFF_TAC `ITSET g (e INSERT s) 1 =
                  g e (ITSET g (s DELETE e) 1)` THEN1 SRW_TAC [][Abbr`g`] THEN
  MATCH_MP_TAC COMMUTING_ITSET_RECURSES THEN
  SRW_TAC [ARITH_ss][Abbr`g`]);

(*---------------------------------------------------------------------------*)
(* PROD_SET multiplies the elements of a set of natural numbers              *)
(*---------------------------------------------------------------------------*)

(* Define PROD_SET similar to SUM_SET *)
(* val PROD_SET_DEF = new_definition("PROD_SET_DEF", ``PROD_SET = PROD_IMAGE I``); *)
val PROD_SET_DEF = Define `PROD_SET = PROD_IMAGE I`;

(* Theorem: Product Set property *)
val PROD_SET_THM = store_thm(
  "PROD_SET_THM",
  ``(PROD_SET {} = 1) /\
    (!x s. FINITE s ==> (PROD_SET (x INSERT s) = x * PROD_SET (s DELETE x)))``,
  SRW_TAC [][PROD_SET_DEF, PROD_IMAGE_THM]);

val PROD_SET_EMPTY = save_thm("PROD_SET_EMPTY", CONJUNCT1 PROD_SET_THM);

(* Theorem: PROD_SET (IMAGE f (x INSERT s)) = (f x) * PROD_SET (IMAGE f s) *)
val PROD_SET_IMAGE_REDUCTION = store_thm(
  "PROD_SET_IMAGE_REDUCTION",
  ``!f s x. FINITE (IMAGE f s) /\ f x NOTIN IMAGE f s ==>
     (PROD_SET (IMAGE f (x INSERT s)) = (f x) * PROD_SET (IMAGE f s))``,
  METIS_TAC [DELETE_NON_ELEMENT, IMAGE_INSERT, PROD_SET_THM]);

(* Part 2: General Theory -------------------------------------------------- *)
(* Part 3: Actual Proof ---------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Number-theoretic Proof without Group Theory.                              *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Modulo Arithmetic results.                                                *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Euclid's Lemma] A prime divides a product iff the prime divides a factor.
            [in MOD notation] For prime p, x*y MOD p = 0 <=> x MOD p = 0 or y MOD p = 0 *)
val EUCLID_LEMMA = store_thm(
  "EUCLID_LEMMA",
  ``!p x y. prime p ==> (((x * y) MOD p = 0) <=> (x MOD p = 0) \/ (y MOD p = 0))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  SRW_TAC [][MOD_0_DIVIDES, EQ_IMP_THM] THEN
  METIS_TAC [gcdTheory.P_EUCLIDES, dividesTheory.DIVIDES_MULT, arithmeticTheory.MULT_COMM]);

(* Theorem: [Cancellation Law for MOD p]
   For prime p, if x MOD p <> 0, (x*y) MOD p = (x*z) MOD p ==> y MOD p = z MOD p *)
val MOD_MULT_LCANCEL1 = prove(
  ``!p x y z. (prime p) /\ (z <= y) ==>
     (((x * y) MOD p = (x * z) MOD p) /\ x MOD p <> 0 ==> (y MOD p = z MOD p))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `((x*y) - (x*z)) MOD p = 0` by SRW_TAC [][MOD_EQ_DIFF] THEN
  `(x*(y - z)) MOD p = 0` by SRW_TAC [][arithmeticTheory.LEFT_SUB_DISTRIB] THEN
  METIS_TAC [EUCLID_LEMMA, MOD_EQ]);
val MOD_MULT_LCANCEL = store_thm(
  "MOD_MULT_LCANCEL",
  ``!p x y z. prime p ==>
      (((x * y) MOD p = (x * z) MOD p) /\ x MOD p <> 0 ==> (y MOD p = z MOD p))``,
  REPEAT STRIP_TAC THEN
  Cases_on `z <= y` THENL [ALL_TAC, `y <= z` by RW_TAC arith_ss []] THEN
  METIS_TAC [MOD_MULT_LCANCEL1]);

(* Theorem: For prime p, FACT (p-1) MOD p <> 0 *)
val MOD_PRIME_FACT = store_thm(
  "MOD_PRIME_FACT",
  ``!p n. prime p /\ 0 < n /\ n <= p ==> FACT (n-1) MOD p <> 0``,
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n = 0` THEN1 RW_TAC arith_ss [dividesTheory.ONE_LT_PRIME, arithmeticTheory.FACT] THEN
  `0 < n /\ n <= p` by RW_TAC arith_ss [] THEN
  `(FACT n) MOD p = (FACT (SUC (n-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.ADD1] THEN
  `_ = (SUC (n-1) * FACT (n-1)) MOD p` by RW_TAC arith_ss [arithmeticTheory.FACT] THEN
  `_ = (n * FACT (n-1)) MOD p` by RW_TAC arith_ss [arithmeticTheory.ADD1] THEN
  `n MOD p <> 0` by RW_TAC arith_ss [] THEN
  `FACT (n - 1) MOD p <> 0` by SRW_TAC [][] THEN
  METIS_TAC [EUCLID_LEMMA]);

(* ------------------------------------------------------------------------- *)
(* Residue -- close-relative of COUNT                                        *)
(* ------------------------------------------------------------------------- *)

(* Define the set of residues = nonzero remainders *)
val residue_def = Define `residue n = { i | (0 < i) /\ (i < n) }`;

(* Theorem: residue 1 = EMPTY *)
val RESIDUE_1_EMPTY = store_thm(
  "RESIDUE_1_EMPTY",
  ``residue 1 = {}``,
  SRW_TAC [ARITH_ss][residue_def, EXTENSION]);

(* Theorem: For n > 0, residue (SUC n) = n INSERT residue n *)
val RESIDUE_INSERT = store_thm(
  "RESIDUE_INSERT",
  ``!n. 0 < n ==> (residue (SUC n) = n INSERT residue n)``,
  SRW_TAC [ARITH_ss][residue_def, EXTENSION]);

(* Theorem: (residue n) DELETE n = residue n *)
(* Proof: Because n is not in (residue n). *)
val RESIDUE_DELETE_N = store_thm(
  "RESIDUE_DELETE_N",
  ``!n. 0 < n ==> ((residue n) DELETE n = residue n)``,
  REPEAT STRIP_TAC THEN
  `n NOTIN (residue n)` by SRW_TAC [][residue_def] THEN
  METIS_TAC [DELETE_NON_ELEMENT]);

(* Theorem: count n = 0 INSERT (residue n) *)
val RESIDUE_COUNT = store_thm(
  "RESIDUE_COUNT",
  ``!n. 0 < n ==> (count n = 0 INSERT (residue n))``,
  SRW_TAC [ARITH_ss][residue_def, EXTENSION]);

(* Theorem: FINITE (residue n) *)
val FINITE_RESIDUE = store_thm(
  "FINITE_RESIDUE",
  ``!n. FINITE (residue n)``,
  Cases THEN1 SRW_TAC [][residue_def] THEN
  METIS_TAC [RESIDUE_COUNT, FINITE_INSERT, count_def, FINITE_COUNT,
             DECIDE ``0 < SUC n``]);

(* Theorem: For prime m, a in residue m, n <= m, a*n MOD m <> a*x MOD m  for all x in residue n *)
val RESIDUE_PRIME_NEQ = store_thm(
  "RESIDUE_PRIME_NEQ",
  ``!p a n. prime p /\ a IN (residue p) /\ n <= p ==>
    !x. x IN (residue n) ==> (a*n) MOD p <> (a*x) MOD p``,
  SRW_TAC [][residue_def] THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `(a MOD p <> 0) /\ (x MOD p <> 0)` by RW_TAC arith_ss [] THEN
  `n MOD p = x MOD p` by METIS_TAC [MOD_MULT_LCANCEL] THEN
  Cases_on `n = p` THEN1 METIS_TAC [arithmeticTheory.DIVMOD_ID] THEN
  `n < p` by RW_TAC arith_ss [] THEN
  `(n MOD p = n) /\ (x MOD p = x)` by RW_TAC arith_ss [] THEN
  RW_TAC arith_ss []);

(* Theorem: PROD_SET (residue n) = FACT (n-1) *)
(* true for all n because of the (ugly) way that 0-1 = 0 *)
val PROD_SET_RESIDUE = store_thm(
  "PROD_SET_RESIDUE",
  ``!n. PROD_SET (residue n) = FACT (n-1)``,
  GEN_TAC THEN
  `(n = 0) \/ 0 < n`  by DECIDE_TAC THEN1
    SRW_TAC [][residue_def, arithmeticTheory.FACT, PROD_SET_EMPTY] THEN
  Induct_on `n` THEN RW_TAC arith_ss [] THEN
  Cases_on `0 < n` THENL [
    `FINITE (residue n)` by SRW_TAC [][FINITE_RESIDUE] THEN
    `residue (SUC n) = n INSERT residue n` by SRW_TAC [][RESIDUE_INSERT] THEN
    `PROD_SET (n INSERT (residue n)) = n * PROD_SET ((residue n) DELETE n)`
       by SRW_TAC [][PROD_SET_THM] THEN
    `(residue n) DELETE n = residue n` by SRW_TAC [][RESIDUE_DELETE_N] THEN
    `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
    METIS_TAC [arithmeticTheory.FACT],
    `n = 0` by DECIDE_TAC THEN
    SRW_TAC [][RESIDUE_1_EMPTY, arithmeticTheory.FACT, PROD_SET_THM]
  ]);

(* ------------------------------------------------------------------------- *)
(* Mapping the residues to the row of residues.                              *)
(* ------------------------------------------------------------------------- *)

(* Define the row function: producing a row of the Z*n multiplication table *)
val row_def = Define `row n a x = (a*x) MOD n`;

(* Theorem: For prime p, a in (residue p) /\ (n <= p) ==>
                (row p a)(n) not IN IMAGE (row p a) (residue n) *)
val RESIDUE_PRIME_NOTIN_IMAGE = prove(
  ``!p a n. prime p /\ a IN (residue p) /\ n <= p ==>
            (row p a)(n) NOTIN IMAGE (row p a) (residue n)``,
  REPEAT STRIP_TAC THEN
  `!x. x IN (residue n) ==> (a*n) MOD p <> (a*x) MOD p`
    by SRW_TAC [][RESIDUE_PRIME_NEQ] THEN
  FULL_SIMP_TAC (srw_ss())[row_def] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)
(* More PROD_SET properties, especially with ROW.                            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Inductive step of PROD_SET_IMAGE_ROW
   For prime p, a in (residue p), 0 < n <= p,
     PROD_SET (IMAGE (row p a) (residue (SUC n))) =
     ((a*n) MOD p)* PROD_SET (IMAGE (row p a) (residue n)) *)
val PROD_SET_IMAGE_ROW_REDUCTION = store_thm(
  "PROD_SET_IMAGE_ROW_REDUCTION",
  ``!p n a. prime p /\ a IN (residue p) /\ 0 < n /\ n <= p ==>
   (PROD_SET (IMAGE (row p a) (residue (SUC n))) =
       ((a*n) MOD p)* PROD_SET (IMAGE (row p a) (residue n)))``,
  REPEAT STRIP_TAC THEN
  `(row p a)(n) NOTIN IMAGE (row p a) (residue n)`
    by SRW_TAC [][RESIDUE_PRIME_NOTIN_IMAGE] THEN
  `FINITE (IMAGE (row p a) (residue n))`
    by SRW_TAC [][FINITE_RESIDUE, pred_setTheory.IMAGE_FINITE] THEN
  `PROD_SET (IMAGE (row p a) (residue (SUC n))) =
    PROD_SET (IMAGE (row p a) (n INSERT residue n))`
      by SRW_TAC [][RESIDUE_INSERT] THEN
  `_ = ((row p a)(n)) * PROD_SET (IMAGE (row p a) (residue n))`
     by SRW_TAC [][PROD_SET_IMAGE_REDUCTION] THEN
  METIS_TAC [row_def]);

(* Theorem: For prime p, and a in (residue p),
            PROD_SET IMAGE (row p a) (residue p) = (a**(p-1) * FACT (p-1)) MOD p *)
val PROD_SET_IMAGE_ROW = store_thm(
  "PROD_SET_IMAGE_ROW",
  ``!p n a. prime p /\ a IN (residue p) /\ 0 < n /\ n <= p ==>
   (PROD_SET (IMAGE (row p a) (residue n)) MOD p = (a**(n-1) * FACT (n-1)) MOD p)``,
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n = 0` THEN1
    RW_TAC arith_ss [RESIDUE_1_EMPTY, pred_setTheory.IMAGE_EMPTY, PROD_SET_EMPTY,
                     arithmeticTheory.FACT, dividesTheory.ONE_LT_PRIME] THEN
  `0 < n /\ n <= p /\ 0 < p` by RW_TAC arith_ss [] THEN
  `(PROD_SET (IMAGE (row p a) (residue (SUC n)))) MOD p =
      (((a*n) MOD p)* PROD_SET (IMAGE (row p a) (residue n))) MOD p`
       by SRW_TAC [][PROD_SET_IMAGE_ROW_REDUCTION] THEN
  `_ = ((a*n) * PROD_SET (IMAGE (row p a) (residue n))) MOD p`
    by SRW_TAC [][MOD_TIMES_TWO] THEN
  `_ = ((a*n) MOD p * (PROD_SET (IMAGE (row p a) (residue n))) MOD p) MOD p`
    by SRW_TAC [][arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((a*n) MOD p * ((a ** (n - 1) * FACT (n - 1))) MOD p) MOD p`
    by SRW_TAC [][] THEN
    (* apply inductive hypothesis *)
  `_ = ((a*n) * (a ** (n - 1) * FACT (n - 1))) MOD p`
    by SRW_TAC [][arithmeticTheory.MOD_TIMES2] THEN
  `_ = ((a* (a ** (n-1))) * (n * FACT (n-1))) MOD p` by RW_TAC arith_ss [] THEN
  `_ = (a**(SUC (n-1)) * (n * FACT (n-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.EXP] THEN
  `_ = ((a**n) * (SUC(n-1) * FACT (n-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.ADD1] THEN
  `_ = (a**n * FACT (SUC(n-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.FACT] THEN
  RW_TAC arith_ss [arithmeticTheory.ADD1]);

(* The above version with n = p *)
val PROD_SET_IMAGE_ROW_ALL = store_thm(
  "PROD_SET_IMAGE_ROW_ALL",
  ``!p a. prime p /\ a IN (residue p) ==>
   (PROD_SET (IMAGE (row p a) (residue p)) MOD p = (a**(p-1) * FACT (p-1)) MOD p)``,
  SRW_TAC [][PROD_SET_IMAGE_ROW, dividesTheory.PRIME_POS]);

(* ------------------------------------------------------------------------- *)
(* Fermat's Identity                                                         *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Let Z* = {n | 0 < n /\ n < p} with prime p, i.e. residue p.
            !a in Z*, the function f: Z* -> Z* = x -> a*x MOD p is an injection,
   or
   For prime p and a in residue p,
   the map  (row p a): (residue p) -> (residue p) is an injection. *)
val RESIDUE_PRIME_ROW_INJ = prove(
  ``!p a. prime p /\ a IN (residue p) ==> INJ (row p a) (residue p) (residue p)``,
  SRW_TAC [][residue_def] THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `a MOD p <> 0` by RW_TAC arith_ss [] THEN
  SRW_TAC [][pred_setTheory.INJ_DEF, row_def] THEN1 (
    `x MOD p <> 0` by RW_TAC arith_ss [] THEN
    `(a*x) MOD p <> 0` by METIS_TAC [EUCLID_LEMMA] THEN
    RW_TAC arith_ss []) THEN
  `x MOD p = y MOD p` by METIS_TAC [MOD_MULT_LCANCEL] THEN
  METIS_TAC [arithmeticTheory.LESS_MOD]);

(* Theorem: For prime p, residue p = IMAGE (row p a) (residue p) *)
val RESIDUE_PRIME_ROW = store_thm(
  "RESIDUE_PRIME_ROW",
  ``!p a. prime p /\ (a IN residue p) ==> (residue p = IMAGE (row p a) (residue p))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `FINITE (residue p)` by RW_TAC std_ss [FINITE_RESIDUE] THEN
  `INJ (row p a) (residue p) (residue p)` by RW_TAC std_ss [RESIDUE_PRIME_ROW_INJ] THEN
  METIS_TAC [FINITE_INJ_IMAGE_EQ]);

(* Theorem: Fermat's Identity: For prime p, a**(p-1) MOD p = 1 *)
val FERMAT_IDENTITY = store_thm(
  "FERMAT_IDENTITY",
  ``!p a. prime p /\ a IN (residue p) ==> (a**(p-1) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `1 < p` by SRW_TAC [][dividesTheory.ONE_LT_PRIME] THEN (* for 1 MOD p = 1 *)
  `FACT (p-1) MOD p <> 0` by SRW_TAC [][MOD_PRIME_FACT] THEN
  `(FACT (p-1)*1) MOD p = FACT (p-1) MOD p` by RW_TAC arith_ss [] THEN
  `_ = PROD_SET (residue p) MOD p` by SRW_TAC [][PROD_SET_RESIDUE] THEN
  `_ = PROD_SET (IMAGE (row p a) (residue p)) MOD p` by METIS_TAC [RESIDUE_PRIME_ROW] THEN
  `_ = (FACT (p-1) * a**(p-1)) MOD p` by RW_TAC arith_ss [PROD_SET_IMAGE_ROW_ALL] THEN
  `1 = 1 MOD p` by RW_TAC arith_ss [] THEN
  `_ = a**(p-1) MOD p` by METIS_TAC [MOD_MULT_LCANCEL] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem                                                   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Fermat's Little Theorem: For prime p, (a**p) MOD p = a MOD p *)
val FERMAT_LITTLE = store_thm(
  "FERMAT_LITTLE",
  ``!p a. prime p /\ a IN (residue p) ==> ((a**p) MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `1 < p` by SRW_TAC [][dividesTheory.ONE_LT_PRIME] THEN
  `(a**p) MOD p = (a**(SUC(p-1))) MOD p` by RW_TAC arith_ss [arithmeticTheory.ADD1] THEN
  `_ = (a * a**(p-1)) MOD p` by RW_TAC arith_ss [arithmeticTheory.EXP] THEN
  `_ = (a MOD p * a**(p-1) MOD p) MOD p` by RW_TAC arith_ss [arithmeticTheory.MOD_TIMES2] THEN
  `_ = (a MOD p * 1) MOD p` by SRW_TAC [][FERMAT_IDENTITY] THEN
  RW_TAC arith_ss []);

(* Part 4: End ------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
