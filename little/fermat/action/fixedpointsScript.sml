(* ------------------------------------------------------------------------- *)
(* Extending Group Theory: Group action.                                     *)
(* ------------------------------------------------------------------------- *)

(*

Group action
============
. action f is a map from Group g to Target set X, satisfying some conditions.
. The action induces an equivalence relation "reach" on Target set X.
. The equivalent classes of "reach" on X are called orbits.
. Due to this partition, CARD X = SIGMA CARD orbits.
. As equivalent classes are non-empty, minimum CARD orbit = 1.
. These singleton orbits have a 1-1 correspondence with a special set on X:
  the fixedpoints. The main result is:
  CARD X = CARD fixedpoints + SIGMA CARD non-singleton orbits.

  Somewhere Zn enters into the picture. Where?
*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "fixedpoints";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
open pred_setTheory;

(* Get dependent theories *)
(* val _ = load "helperListTheory"; *)
(* val _ = load "helperNumTheory"; *)
(* val _ = load "helperSetTheory"; *)
(* val _ = load "groupTheory"; *)
(* val _ = load "subgroupTheory"; *)
open helperListTheory helperNumTheory helperSetTheory groupTheory subgroupTheory;

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* val _ = load "actionTheory"; *)
open actionTheory;


(* ------------------------------------------------------------------------- *)
(* Group action                                                              *)
(* ------------------------------------------------------------------------- *)


(* A trivial example of Group action *)

(* Theorem: The group operation is an action on itself. *)
(* Proof: by definition. *)
val GROUP_OP_ACTION = store_thm(
  "GROUP_OP_ACTION",
  ``!g. Group g ==> action g.mult g g.carrier``,
  RW_TAC std_ss [action_def, Group_def]);

(* ------------------------------------------------------------------------- *)
(* Group action induces an equivalence relation.                             *)
(* ------------------------------------------------------------------------- *)


(* ------------------------------------------------------------------------- *)
(* Fixed Points of action.                                                   *)
(* ------------------------------------------------------------------------- *)

(*
Fixed Points have singleton orbits -- although it is not defined in this way,
this property is the theorem FIXPOINT_ORBIT_SING.

This important property of fixed points gives this simple trick:
to count how many singleton orbits, just count the set (fixedpoints f g X).

Since orbits are equivalent classes, they cannot be empty, hence singleton
orbits are the simplest type. For equivalent classes:

CARD Target = SUM CARD (TargetPartitions)
            = SUM CARD (orbits)
            = SUM CARD (singleton orbits) + SUM CARD (non-singleton orbits)
            = CARD (fixedpoints) + SUM CARD (non-singleton orbits)
*)

(* Fixedpoints of action: f g X = {x in X | !z in G. f z x = x} *)
val fixedpoints_def = Define`
  fixedpoints f g X = {x | x IN X /\ (!z. z IN g.carrier ==> (f z x = x)) }
`;

(* Theorem: x IN (fixedpoints f g X) ==> x IN X *)
(* Proof: by definition. *)
val FIXEDPOINTS_ELEMENT = store_thm(
  "FIXEDPOINTS_ELEMENT",
  ``!f g X. x IN (fixedpoints f g X) ==> x IN X``,
  SRW_TAC [][fixedpoints_def]);

(* Theorem: fixedpoints are subsets of target set X *)
(* Proof: by definition. *)
val FIXEDPOINTS_SUBSET_TARGET = store_thm(
  "FIXEDPOINTS_SUBSET_TARGET",
  ``!f g X. action f g X  ==> fixedpoints f g X SUBSET X``,
  SRW_TAC [][fixedpoints_def, SUBSET_DEF]);

(* ------------------------------------------------------------------------- *)
(* Fixed Points have singleton orbits.
   Or those points with stabilizer = whole group
   With ZP_ORBIT_DISTINCT,
   This gives the key result:
        fixpoints f (Z p) X <==bijective==> (Z p).carrier
   This will make the computation of CARD (fixpoints f (Z p) X) easy.
*)
(* ------------------------------------------------------------------------- *)

(* Theorem: a IN fixedpoints f g X ==> (orbit f g X a = {a}) *)
(* Proof:
   This is to prove:
   (1) a IN X /\ x' IN g.carrier /\ (!z. z IN g.carrier ==> (f z a = a)) ==> f x' a = a
       This is true by the included implication.
   (2) a IN X /\ (!z. z IN g.carrier ==> (f z a = a)) ==> ?x. (a = f x a) /\ x IN g.carrier
       This is true by taking x = g.id, which is in g.carrier by group_id_carrier,
       and f g.id a = a by action.
*)
val FIXPOINT_ORBIT_SING = store_thm(
  "FIXPOINT_ORBIT_SING",
  ``!f g X. Group g /\ action f g X ==>
     !a. a IN fixedpoints f g X ==> (orbit f g X a = {a})``,
  SRW_TAC [][fixedpoints_def, orbit_def, reach_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [group_id_carrier]);

(* Theorem: For Action f g X, a IN X, (orbit f g X a = {a}) ==> a IN fixedpoints f g X *)
(* Proof:
   This is to prove:
   a IN X /\ z IN g.carrier /\ !x. (?z. z IN g.carrier /\ (x = f z a)) <=> (x = a) ==> f z a = a
   Since g.id IN g.carrier, f g.id a = a by action, this gives f z a = a.
*)
val ORBIT_SING_FIXPOINT = store_thm(
  "ORBIT_SING_FIXPOINT",
  ``!f g X. action f g X ==>
    !a. a IN X /\ (orbit f g X a = {a}) ==> a IN fixedpoints f g X``,
  SRW_TAC [][action_def, fixedpoints_def, orbit_def, reach_def, EXTENSION] THEN
  METIS_TAC []);

(* Combine the two and use SING. *)

(* Theorem: a IN fixedpoints f g X <=> SING (orbit f g X a}) *)
(* Proof:
   If part: a IN fixedpoints f g X ==> SING (orbit f g X a})
       a IN fixedpoints f g X
   ==> !z. z IN G ==> (f z a = a)        by fixedpoints_def
   Now, a IN (orbit f g X a)             by ACTION_ORBIT_HAS_ITSELF
   For any x IN (orbit f g X a),
   we have reach f g a x                 by orbit_def
   which is  ?z. z IN G /\ (f z a = x)   by reach_def
   By fixedpoints_def, a = x,
   hence SING (orbit f g X a}            by SING_DEF, IN_SING

   Only-if part: SING (orbit f g X a}) ==> a IN fixedpoints f g X
   Since a IN (orbit f g X a)            by ACTION_ORBIT_HAS_ITSELF
   hence !x. x IN (orbit f g X a) ==> x = a    by IN_SING
   Since orbit f g X a = IMAGE (\z. f z a) G)  by ORBIT_AS_IMAGE
   This implies !z. z IN G ==> f z a = a,
   so a IN fixedpoints f g X             by fixedpoints_def
*)
val FIXPOINT_ORBIT_IS_SING = store_thm(
  "FIXPOINT_ORBIT_IS_SING",
  ``!f g X. Group g /\ action f g X ==> !a. a IN X ==> (a IN fixedpoints f g X <=> SING (orbit f g X a))``,
  METIS_TAC [FIXPOINT_ORBIT_SING, ORBIT_SING_FIXPOINT, ACTION_ORBIT_HAS_ITSELF, SING_DEF, IN_SING]);

(* Theorem: a IN (X DIFF fixedpoints f g X) <=> a IN X /\ ~ SING (orbit f g X a))  *)
(* Proof:
       a IN (X DIFF fixedpoints f g X)
   <=> a IN X /\ a NOTIN (fixedpoints f g X)    by IN_DIFF
   <=> a IN X /\ ~ SING (orbit f g X a))        by FIXPOINT_ORBIT_IS_SING
*)
val NONFIX_ORBIT_NOT_SING = store_thm(
  "NONFIX_ORBIT_NOT_SING",
  ``!f g X. Group g /\ action f g X ==> !a. a IN (X DIFF fixedpoints f g X) <=> a IN X /\ ~ SING (orbit f g X a)``,
  METIS_TAC [IN_DIFF, FIXPOINT_ORBIT_IS_SING]);

(* Theorem: CARD (X DIFF fixedpoints f g X) = CARD X - CARD (fixedpoints f g X) *)
(* Proof:
   Since (fixedpoints f g X) is a subset of X   by fixedpoints_def
   X INTER (fixedpoints f g X) = (fixedpoints f g X)  by SUBSET_INTER_ABSORPTION
   Then  CARD (X DIFF fixedpoints f g X)
       = CARD X - CARD (X INTER (fixedpoints f g X)))  by CARD_DIFF
       = CARD X - CARD (fixedpoints f g X)             by SUBSET_INTER_ABSORPTION
*)
val CARD_TARGET_DIFF_FIX = store_thm(
  "CARD_TARGET_DIFF_FIX",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> (CARD (X DIFF fixedpoints f g X) = CARD X - CARD (fixedpoints f g X))``,
  REPEAT STRIP_TAC THEN
  `(fixedpoints f g X) SUBSET X` by SRW_TAC [][fixedpoints_def, SUBSET_DEF] THEN
  METIS_TAC [CARD_DIFF, SUBSET_INTER_ABSORPTION, SUBSET_FINITE, INTER_COMM]);

(* ------------------------------------------------------------------------- *)
(* Partition of TargetPartition into singleorbits and multiorbits.           *)
(* ------------------------------------------------------------------------- *)

(* Define singleton and non-singleton orbits *)
val singleorbits_def = Define`
    singleorbits f g X = { e | e IN (TargetPartition f g X) /\ SING e }
`;
val multiorbits_def = Define`
    multiorbits f g X = { e | e IN (TargetPartition f g X) /\ ~ SING e }
`;

(* Theorem: TargetPartition = singleorbits + multiorbits. *)
(* Proof: by definition, and IN_DIFF. *)
val TARGET_PARTITION_EQ_SING_MULTI = store_thm(
  "TARGET_PARTITION_EQ_SING_MULTI",
  ``!f g X. singleorbits f g X  = (TargetPartition f g X) DIFF (multiorbits f g X)``,
  SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION, EQ_IMP_THM]);

(* Theorem: For action f g X, singleorbits is the same size as fixedpoints f g X *)
(* Proof:
   Construction a bijection between the two sets.
   For each a IN (fixedpoints f g X),
     a IN X ==> (orbit f g a = {a})  by FIXPOINT_ORBIT_SING.
   For each e IN TargetPartition f g X,
     !a. a IN e ==> (e = orbit f g a)  by TARGET_PARTITION_ELEMENT_IS_ORBIT.

   For each e IN (TargetPartition f g X) DIFF (multiorbits f g X),
       e IN TargetPartition f g X /\ SING e   by multiorbits_def.
   ==> (!a. a IN e ==> (e = orbit f g a)) /\ SING e  by TARGET_PARTITION_ELEMENT_IS_ORBIT.
   ==> e = {a} /\ (e = orbit f g a))        by SING_DEF: e /\ !a. a IN e.
   ==> orbit f g a = {a}
   ==> a IN (fixedpoints f g X)             by ORBIT_SING_FIXPOINT
   So the mapping is: e IN DIFF --> a IN (fixedpoints f g X)  where e = {a}. (CHOICE_SING?)
*)


(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is surjective. *)
(* Proof:
   This is to prove:
   (1) e IN TargetPartition f g X /\ SING e ==> CHOICE e IN fixedpoints f g X
       This is true by TARGET_PARTITION_ELEMENT_SING_CHOICE.
   (2) x IN fixedpoints f g X ==> ?e. (e IN TargetPartition f g X /\ SING e) /\ (CHOICE e = x)
       orbit f g X x = {x}  by FIXPOINT_ORBIT_SING.
       Since x IN X by FIXEDPOINTS_ELEMENT,
       so e = {x} will satisfy the requirement.
*)
val SING_ORBITS_TO_FIXEDPOINTS_SURJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_SURJ",
  ``!f g X. Group g /\ action f g X ==> SURJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  SRW_TAC [][singleorbits_def, SURJ_DEF] THEN1
  METIS_TAC [CHOICE_SING, ORBIT_SING_FIXPOINT, TARGET_PARTITION_ELEMENT_IS_ORBIT,
             TARGET_PARTITION_ELEMENT_ELEMENT, SING_DEF, IN_SING] THEN
  `orbit f g X x = {x}` by RW_TAC std_ss [FIXPOINT_ORBIT_SING] THEN
  Q.EXISTS_TAC `{x}` THEN
  FULL_SIMP_TAC (srw_ss()) [TargetPartition_def, partition_def, orbit_def] THEN
  METIS_TAC [FIXEDPOINTS_ELEMENT]);

(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is injective. *)
(* Proof:
   This is to prove:
   (1) e IN TargetPartition f g X /\ SING e ==> CHOICE e IN fixedpoints f g X
       This is true by TARGET_PARTITION_ELEMENT_SING_CHOICE.
   (2) SING e /\ SING e' /\ CHOICE e = CHOICE e' ==>  e = e'
       This is true by SING_DEF, CHOICE_SING.
*)
val SING_ORBITS_TO_FIXEDPOINTS_INJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_INJ",
  ``!f g X. Group g /\ action f g X ==> INJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  SRW_TAC [][singleorbits_def, INJ_DEF] THEN
  METIS_TAC [CHOICE_SING, ORBIT_SING_FIXPOINT, TARGET_PARTITION_ELEMENT_IS_ORBIT,
             TARGET_PARTITION_ELEMENT_ELEMENT, SING_DEF, IN_SING] THEN
  METIS_TAC [SING_DEF, CHOICE_SING]);

(* Theorem: The map: e IN (singleorbits f g X) --> a IN (fixedpoints f g X)  where e = {a} is bijective. *)
(* Proof:
   True since the map is shown to be both surjective and injective.
*)
val SING_ORBITS_TO_FIXEDPOINTS_BIJ = store_thm(
  "SING_ORBITS_TO_FIXEDPOINTS_BIJ",
  ``!f g X. Group g /\ action f g X ==> BIJ (\e. CHOICE e) (singleorbits f g X) (fixedpoints f g X)``,
  RW_TAC std_ss [BIJ_DEF, SING_ORBITS_TO_FIXEDPOINTS_SURJ, SING_ORBITS_TO_FIXEDPOINTS_INJ]);

(* Theorem: For action f g X, singleorbits is the same size as fixedpoints f g X *)
(* Proof:
   Since (singleorbits f g X) SUBSET (TargetPartition f g X) by singleorbits_def
   and   (fixedpoints f g X) SUBSET X                        by fixedpoints_def
   both are FINITE: first one by FINITE_TARGET_PARTITION, second one by SUBSET_FINITE.
   With SING_ORBITS_TO_FIXEDPOINTS_BIJ, this show they have the same CARD by FINITE_BIJ_CARD_EQ.
*)
val CARD_ORBITS_SING = store_thm(
  "CARD_ORBITS_SING",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> (CARD (singleorbits f g X) = CARD (fixedpoints f g X))``,
  REPEAT STRIP_TAC THEN
  `(singleorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][singleorbits_def, SUBSET_DEF] THEN
  `(fixedpoints f g X) SUBSET X` by SRW_TAC [][fixedpoints_def, SUBSET_DEF] THEN
  METIS_TAC [SING_ORBITS_TO_FIXEDPOINTS_BIJ, FINITE_BIJ_CARD_EQ, SUBSET_FINITE, FINITE_TARGET_PARTITION]);

(* Theorem: For action f g X,
            CARD X = CARD fixedpoints + SIGMA CARD multiorbits *)
(* Proof:
     CARD X
   = SIGMA CARD (TargetPartition f g X)   by CARD_TARGET_BY_PARTITION
   = SIGMA CARD (singleorbits f g X) UNION (multiorbits f g X))  by UNION_DIFF
   = SIGMA CARD (singleorbits f g X) + SIGMA CARD (multiorbits f g X)  by SUM_IMAGE_UNION and DIFF_DISJOINT
   = 1*CARD (singleorbits f g X) + SIGMA CARD (multiorbits f g X) by SIGMA_CARD_CONSTANT
   = CARD (fixedpoints f g X) + SIGMA CARD (multiorbits f g X) by CARD_ORBITS_SING
*)
val CARD_TARGET_BY_ORBITS = store_thm(
  "CARD_TARGET_BY_ORBITS",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> (CARD X = CARD (fixedpoints f g X) + SIGMA CARD (multiorbits f g X))``,
  REPEAT STRIP_TAC THEN
  `(singleorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][singleorbits_def, SUBSET_DEF] THEN
  `(multiorbits f g X) SUBSET (TargetPartition f g X)` by SRW_TAC [][multiorbits_def, SUBSET_DEF] THEN
  `FINITE (singleorbits f g X) /\ FINITE (multiorbits f g X)` by METIS_TAC [FINITE_TARGET_PARTITION, SUBSET_FINITE] THEN
  `(singleorbits f g X) INTER (multiorbits f g X) = {}` by SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION] THEN1
  METIS_TAC [] THEN
  `(singleorbits f g X) UNION (multiorbits f g X) = TargetPartition f g X`
    by SRW_TAC [][singleorbits_def, multiorbits_def, EXTENSION] THEN1
  METIS_TAC [] THEN
  `!e. e IN (singleorbits f g X) ==> (CARD e = 1)` by SRW_TAC [][singleorbits_def, SING_DEF] THEN1
  RW_TAC std_ss [CARD_SING] THEN
  `CARD X = SIGMA CARD ((singleorbits f g X) UNION (multiorbits f g X))` by RW_TAC std_ss [CARD_TARGET_BY_PARTITION] THEN
  `_ = SIGMA CARD (singleorbits f g X) + SIGMA CARD (multiorbits f g X)` by RW_TAC std_ss [SUM_IMAGE_UNION, SUM_IMAGE_THM] THEN
  `_ = 1 * CARD (singleorbits f g X) + SIGMA CARD (multiorbits f g X)` by RW_TAC std_ss [SIGMA_CARD_CONSTANT] THEN
  RW_TAC arith_ss [CARD_ORBITS_SING]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
