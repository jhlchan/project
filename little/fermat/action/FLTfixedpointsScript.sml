(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Combinatorial proof.                            *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by J. Petersen in 1872:

Take p elements from q with repetitions in all ways, that is, in q^p ways.
The q sets with elements all alike are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted in sets of p. Hence p divides q^p - q.

This is a combinatorial using Group action, via Fixed Points Congruence of Zp.
With Group action, there is no need to consider multicoloured necklaces --
they are hidden in the theory.

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTfixedpoints";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
open pred_setTheory listTheory arithmeticTheory;
(* val _ = load "dividesTheory"; *)
open dividesTheory;

(* Get dependent theories in lib *)
(* val _ = load "helperNumTheory"; *)
(* val _ = load "helperListTheory"; *)
(* val _ = load "helperSetTheory"; *)
(* val _ = load "cycleTheory"; *)
open helperNumTheory helperListTheory helperSetTheory cycleTheory;
(* Get dependent theories in group *)
(* val _ = load "groupTheory"; *)
(* val _ = load "groupInstancesTheory"; *)
open groupTheory groupInstancesTheory;
(* Get dependent theories in necklace *)
(* val _ = load "necklaceTheory"; *)
open necklaceTheory;

(* Get dependent theories in action *)
(* val _ = load "actionTheory"; *)
open actionTheory;
(* Get dependent theories local *)
(* val _ = load "fixedpointsTheory"; *)
open fixedpointsTheory;


(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof via Group action.                                     *)
(* ------------------------------------------------------------------------- *)


(* ------------------------------------------------------------------------- *)
(* Group action of Zp, where p is prime.                                     *)
(* Note: This is the addition modulo Zp, not the multiplication modulo Z*p.  *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For prime p and action f (Z p) X,
            e IN (multiorbits f (Z p) X) ==> CARD e = p. *)
(* Proof:
   By e IN (multiorbits f (Z p) X), ~SING e, hence CARD e <> 1.
   By ORBIT_STABILIZER_THEOREM, CARD e = p, as p is prime.
*)
val CARD_ZP_NONFIX_ORBIT = store_thm(
  "CARD_ZP_NONFIX_ORBIT",
  ``!p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
   !e. e IN (multiorbits f (Z p) X) ==> (CARD e = p)``,
  SRW_TAC [][multiorbits_def] THEN
  `Group (Z p)` by SRW_TAC [][ZN_group, PRIME_POS] THEN
  `FINITE (Z p).carrier` by SRW_TAC [][Z_def, FINITE_COUNT] THEN
  `CARD (Z p).carrier = p` by SRW_TAC [][Z_def, CARD_COUNT] THEN
  `FiniteGroup (Z p)` by SRW_TAC [][FiniteGroup_def] THEN
  `FINITE e` by METIS_TAC [FINITE_TARGET_PARTITION_ELEMENT] THEN
  `CARD e <> 1` by METIS_TAC [SING_IFF_CARD1] THEN
  `?a. a IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
  `a IN X` by METIS_TAC [TARGET_PARTITION_ELEMENT_ELEMENT] THEN
  `e = orbit f (Z p) X a` by SRW_TAC [][TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `p = CARD e * CARD (stabilizer f (Z p) a)` by METIS_TAC [ORBIT_STABILIZER_THEOREM] THEN
  `divides (CARD e) p` by METIS_TAC [divides_def, MULT_COMM] THEN
  METIS_TAC [prime_def]);

(* Theorem: [Fixed-Point Congruence for Zp action]
            For prime p and action f (Z p) X and FINITE X,
            (CARD X = CARD (fixedpoints f (Z p) X)) (mod p). -- COPIED *)
(* Proof:

     CARD X
   = CARD (fixedpoints f (Z p) X) + SIGMA CARD (multiorbits f (Z p) X)   by CARD_TARGET_BY_ORBITS
   = CARD (fixedpoints f (Z p) X) + p * CARD (multiorbits f (Z p) X)     by SIGMA_CARD_CONSTANT
        since e IN (multiorbits f (Z p) X) ==> (CARD e = p) by CARD_ZP_NONFIX_ORBIT
   Hence the result follows by taking MOD p on both sides.
*)
val ZP_FIXEDPOINTS_CONGRUENCE = store_thm(
  "ZP_FIXEDPOINTS_CONGRUENCE",
  ``!p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
     (CARD X MOD p = CARD (fixedpoints f (Z p) X) MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][PRIME_POS] THEN
  `Group (Z p)` by SRW_TAC [][ZN_group] THEN
  `!e. e IN (multiorbits f (Z p) X) ==> (CARD e = p)`
      by METIS_TAC [CARD_ZP_NONFIX_ORBIT] THEN
  `(multiorbits f (Z p) X) SUBSET (TargetPartition f (Z p) X)`
      by SRW_TAC [][multiorbits_def, SUBSET_DEF] THEN
  `FINITE (multiorbits f (Z p) X)`
      by METIS_TAC [SUBSET_FINITE, FINITE_TARGET_PARTITION] THEN
  `CARD X = CARD (fixedpoints f (Z p) X) + SIGMA CARD (multiorbits f (Z p) X)`
      by SRW_TAC [][CARD_TARGET_BY_ORBITS] THEN
  `_ = CARD (fixedpoints f (Z p) X) + p * CARD (multiorbits f (Z p) X)`
      by SRW_TAC [][SIGMA_CARD_CONSTANT] THEN
  `_ = CARD (multiorbits f (Z p) X) * p  + CARD (fixedpoints f (Z p) X)`
      by RW_TAC arith_ss [] THEN
  METIS_TAC [MOD_TIMES]);

(* ------------------------------------------------------------------------- *)
(* Apply Group action to Necklaces.                                          *)
(* G = Zp, X = Necklace n a. action = cycle.                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Cycle is a group action from Zn to Necklace n a. *)
(* Proof:
   Essentially this is to prove:
   (1) LENGTH (cycle x z) = LENGTH z,                        true by CYCLE_SAME_LENGTH.
   (2) set (cycle x z) SUBSET count a,                       true by CYCLE_SAME_SET.
   (3) cycle 0 z = z,                                        true by CYCLE_0.
   (4) cycle x (cycle y z) = cycle ((x + y) MOD LENGTH z) z, true by CYCLE_ADDITION.
*)
val CYCLE_ACTION = store_thm(
  "CYCLE_ACTION",
  ``!n a. 0 < n /\ 0 < a ==> action cycle (Z n) (Necklace n a)``,
  SRW_TAC [][action_def, Necklace_def, Z_def, RES_FORALL_THM] THEN
  METIS_TAC [CYCLE_0, CYCLE_SAME_LENGTH, CYCLE_SAME_SET, CYCLE_ADDITION,
             LENGTH_NIL, NOT_ZERO_LT_ZERO]);

(* REPEAT: Order of monocoloured necklaces -- repeat from Part 1: FLTnecklaceScript *)

(* ------------------------------------------------------------------------- *)
(* Investigate the monocoloured Similarity equivalence partitions.           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A monocoloured necklace l has cycle 1 l = l *)
val monocoloured_cycle1 = prove(
  ``!n a l. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> (cycle 1 l = l)``,
  SRW_TAC [][monocoloured_def] THEN
  `l <> []` by METIS_TAC [NECKLACE_NONNIL] THEN
  METIS_TAC [SING_HAS_CYCLE_1]);

(* Theorem: If a necklace l has cycle 1 l = l , it must be monocoloured *)
val cycle1_monocoloured = prove(
  ``!l n a. 0 < a /\ l IN Necklace n a /\ (cycle 1 l = l) ==> l IN monocoloured n a``,
  SRW_TAC [][monocoloured_def, Necklace_def] THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* Theorem: A Necklace l is monocoloured iff cycle 1 l = 1 *)
val monocoloured_iff_cycle1 = store_thm(
  "monocoloured_iff_cycle1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN Necklace n a ==>
     (l IN monocoloured n a <=> (cycle 1 l = l))``,
  METIS_TAC [monocoloured_Necklace, cycle1_monocoloured, monocoloured_cycle1]);

(* End of repeat from Part 1: FLTnecklaceScript *)

(* ------------------------------------------------------------------------- *)
(* Fixed Points of cycle are monocoloured necklaces.                         *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The fixedpoints of cycle are necklaces of cycle 1 l = l. *)
(* Proof:
   Essentially this is to prove:
   (1) (!z. z < LENGTH x ==> (cycle z x = x)) ==> cycle 1 x = x
       If 1 < LENGTH x, this is true by taking z = 1.
       If 1 = LENGTH x, this is true by MONO_HAS_CYCLE_1.
   (2) cycle 1 x = x /\ z < LENGTH x ==> cycle z x = x
       This is true by CYCLE_1_FIX.
*)
val CYCLE_FIXPOINTS = store_thm(
  "CYCLE_FIXPOINTS",
  ``!n a. 0 < n /\ 0 < a  ==>
    (fixedpoints cycle (Z n) (Necklace n a) = {l | l IN (Necklace n a) /\ (cycle 1 l = l)} )``,
  SRW_TAC [][fixedpoints_def, Necklace_def, Z_def, EXTENSION, EQ_IMP_THM] THEN1
  (Cases_on `LENGTH x = 1` THEN1 METIS_TAC [MONO_HAS_CYCLE_1] THEN
   `1 < LENGTH x` by RW_TAC arith_ss [] THEN METIS_TAC []) THEN
  METIS_TAC [CYCLE_1_FIX]);

(* Theorem: The fixedpoints of cycle are monocoloured necklaces. *)
(* Proof:
   By CYCLE_FIXPOINTS, they have cycle 1 l = 1.
   By cycle1_monocoloured, they are monocoloured.
*)
val CYCLE_FIXPOINTS_ARE_MONOCOLOURED = store_thm(
  "CYCLE_FIXPOINTS_ARE_MONOCOLOURED",
  ``!n a. 0 < n /\ 0 < a  ==>
    (fixedpoints cycle (Z n) (Necklace n a) = (monocoloured n a) )``,
  REPEAT STRIP_TAC THEN
  `fixedpoints cycle (Z n) (Necklace n a) = {l | l IN (Necklace n a) /\ (cycle 1 l = l)}`
    by SRW_TAC [][CYCLE_FIXPOINTS] THEN
  SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [monocoloured_Necklace, monocoloured_iff_cycle1]);

(* Theorem: (Fermat's Little Theorem by Group action) For prime p, a**p = a (mod p). *)
(* Proof:
   !p f X. prime p /\ action f (Z p) X /\ FINITE X ==>
     (CARD X MOD p = CARD (fixedpoints f (Z p) X) MOD p)      by ZP_FIXEDPOINTS_CONGRUENCE
   !n a. 0 < n /\ 0 < a  ==>
     (fixedpoints cycle (Z n) (Necklace n a) = (monocoloured n a) )
                                                              by CYCLE_FIXPOINTS_ARE_MONOCOLOURED
   !n a. 0 < n /\ 0 < a ==> action cycle (Z n) (Necklace n a) by CYCLE_ACTION
   !n a. FINITE (Necklace n a)                                by FINITE_Necklace
   !n a. CARD (Necklace n a) = a ** n                         by CARD_Necklace
   !n. 0 < n ==> (CARD (monocoloured n a) = a)                by CARD_monocoloured
*)
val FERMAT_LITTLE3 = store_thm(
  "FERMAT_LITTLE3",
  ``!p a. prime p ==> (a**p MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][PRIME_POS] THEN
  Cases_on `a = 0` THEN1 METIS_TAC [EXP_EQ_0, ZERO_MOD] THEN
  `0 < a` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ACTION, FINITE_Necklace, CARD_Necklace, CARD_monocoloured,
             CYCLE_FIXPOINTS_ARE_MONOCOLOURED, ZP_FIXEDPOINTS_CONGRUENCE]);

(* ------------------------------------------------------------------------- *)
(* Power Version of Fermat's Little Theorem                                  *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For prime p and action f (Z p**n) X and FINITE X,
            e IN (multiorbits f (Z p**n) X) ==> p divides CARD e. *)
(* Proof:
   Let m = p**n
       e IN (multiorbits f (Z m) X)
   ==> e IN TargetPartition f g X and ~ SING e         by multiorbits_def
   ==> !a. e = (orbit f (Z m) X a ) and ~ SING e       by TARGET_PARTITION_ELEMENT_IS_ORBIT
   ==> CARD e <> 1                                     by SING_IFF_CARD1, FINITE_TARGET_PARTITION_ELEMENT, FINITE X
   ==> p divides CARD e, since (CARD e)*k = m = p**n   by ORBIT_STABILIZER_THEOREM and elementary number theory.
*)
val CARD_ZPN_MULTIORBITS = store_thm(
  "CARD_ZPN_MULTIORBITS",
  ``!p n f X. prime p /\ action f (Z (p**n)) X /\ FINITE X ==>
   !e. e IN (multiorbits f (Z (p**n)) X) ==> divides p (CARD e)``,
  SRW_TAC [][multiorbits_def, TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  Q.ABBREV_TAC `m = p**n` THEN
  `0 < m` by METIS_TAC [ZERO_LT_EXP, PRIME_POS] THEN
  `Group (Z m)` by SRW_TAC [][ZN_group] THEN
  `?a. a IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
  `e = orbit f (Z m) X a` by METIS_TAC [TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `CARD e <> 1` by METIS_TAC [SING_IFF_CARD1, FINITE_TARGET_PARTITION_ELEMENT] THEN
  `FiniteGroup (Z m)` by METIS_TAC [ZN_finite_abelian_group, FiniteAbelianGroup_def] THEN
  `a IN X` by FULL_SIMP_TAC (srw_ss())[orbit_def] THEN
  `CARD (Z m).carrier = m` by SRW_TAC [][Z_def, CARD_COUNT] THEN
  `m = (CARD e)*  CARD (stabilizer f (Z m) a)` by METIS_TAC [ORBIT_STABILIZER_THEOREM] THEN
  `divides (CARD e) m` by METIS_TAC [divides_def, MULT_COMM] THEN
  METIS_TAC [PRIME_EXP_FACTOR]);

(* Theorem: [Fixed-Point Congrunce for Zp**n action]
            For prime p and action f (Z p**n) X and FINITE X,
            (CARD X = CARD (fixedpoints f (Z p**n) X)) (mod p). *)
(* Proof:
   Let m = p**n.
     CARD X
   = CARD (fixedpoints f (Z m) X) + SIGMA CARD (multiorbits f (Z m) X)   by CARD_TARGET_BY_ORBITS
   p divides CARD element in (multiorbits f (Z m) X) ==> p divides SIGMA CARD (multiorbits f (Z m) X).
   Hence the result follows by taking MOD p on both sides.
*)
val ZPN_FIXEDPOINTS_CONGRUENCE = store_thm(
  "ZPN_FIXEDPOINTS_CONGRUENCE",
  ``!p n f X. prime p /\ action f (Z (p**n)) X /\ FINITE X ==>
     (CARD X MOD p = CARD (fixedpoints f (Z (p**n)) X) MOD p)``,
  REPEAT STRIP_TAC THEN
  Q.ABBREV_TAC `m = p**n` THEN
  `0 < m` by METIS_TAC [ZERO_LT_EXP, PRIME_POS] THEN
  `Group (Z m)` by SRW_TAC [][ZN_group] THEN
  `!e. e IN (multiorbits f (Z m) X) ==> divides p (CARD e)` by METIS_TAC [CARD_ZPN_MULTIORBITS, Abbr`m`] THEN
  `CARD X = CARD (fixedpoints f (Z m) X) + SIGMA CARD (multiorbits f (Z m) X)` by SRW_TAC [][CARD_TARGET_BY_ORBITS] THEN
  `(multiorbits f (Z m) X) SUBSET (TargetPartition f (Z m) X)` by SRW_TAC [][multiorbits_def, SUBSET_DEF] THEN
  `FINITE (multiorbits f (Z m) X)` by METIS_TAC [SUBSET_FINITE, FINITE_TARGET_PARTITION] THEN
  `divides p (SIGMA CARD (multiorbits f (Z m) X))` by METIS_TAC [DIVIDES_SIGMA_CARD, Abbr`m`] THEN
  METIS_TAC [PRIME_POS, divides_def,
              MOD_EQ_0, MOD_PLUS,
              ZERO_MOD, ADD_0, MOD_MOD]);

(* Theorem: [General Fermat's Little Theorem by Group Action]
            For prime p, for all n, a**(p**n) = a (mod p). *)
(* Proof:
   Let m = p**n. The addition group Z_m acts on (Necklace m a) via cycle action.
   By ORBIT_STABILIZER_THEOREM, CARD Z_m = CARD (orbit x) * CARD (stabilizer x)  for x in (Necklace m a).
   When m = p**n, the possible factors are 1, p, p^2, p^3, ......, hence p divides all CARD (multorbits) .
   Hence CARD (Necklace m a) = CARD (fixedpoints m a) (mod p), and there are still only 'a' fixedpoints.
   or a**m = a**(p**n) = a (mod p).

   !p n f X. prime p /\ action f (Z p**n) X /\ FINITE X ==>
     (CARD X MOD p = CARD (fixedpoints f (Z p**n) X) MOD p)         by ZPN_FIXEDPOINTS_CONGRUENCE
   !n a. 0 < n /\ 0 < a  ==>
     (fixedpoints cycle (Z n) (Necklace n a) = (monocoloured n a))  by CYCLE_FIXPOINTS_ARE_MONOCOLOURED
   !n a. 0 < n /\ 0 < a ==> action cycle (Z n) (Necklace n a)       by CYCLE_ACTION
   !n a. FINITE (Necklace n a)                                      by FINITE_Necklace
   !n a. CARD (Necklace n a) = a ** n                               by CARD_Necklace
   !n. 0 < n ==> (CARD (monocoloured n a) = a)                      by CARD_monocoloured
*)
val FERMAT_LITTLE_EXP2 = store_thm(
  "FERMAT_LITTLE_EXP2",
  ``!p n a. prime p ==> (a**(p**n) MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  Q.ABBREV_TAC `m = p**n` THEN
  `0 < m` by METIS_TAC [PRIME_POS, ZERO_LT_EXP, Abbr`m`] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [EXP_EQ_0, ZERO_MOD] THEN
  `0 < a` by RW_TAC arith_ss [] THEN
  `(fixedpoints cycle (Z m) (Necklace m a) = (monocoloured m a))` by METIS_TAC [CYCLE_FIXPOINTS_ARE_MONOCOLOURED] THEN
  METIS_TAC [CYCLE_ACTION, FINITE_Necklace, CARD_Necklace, CARD_monocoloured, ZPN_FIXEDPOINTS_CONGRUENCE]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
