(* ------------------------------------------------------------------------- *)
(* Extending Group Theory: Group action.                                     *)
(* ------------------------------------------------------------------------- *)

(*

Group action
============
. action f is a map from Group g to Target set X, satisfying some conditions.
. The action induces an equivalence relation "reach" on Target set X.
. The equivalent classes of "reach" on X are called orbits.
. Due to this partition, CARD X = SIGMA CARD orbits.
. As equivalent classes are non-empty, minimum CARD orbit = 1.
. These singleton orbits have a 1-1 correspondence with a special set on X:
  the fixedpoints. The main result is:
  CARD X = CARD fixedpoints + SIGMA CARD non-singleton orbits.

  Somewhere Zn enters into the picture. Where?
*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "action";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory;

(* Get dependent theories *)
(* val _ = load "helperListTheory"; *)
(* val _ = load "helperNumTheory"; *)
(* val _ = load "helperSetTheory"; *)
(* val _ = load "subgroupTheory"; *)
open helperListTheory helperNumTheory helperSetTheory groupTheory subgroupTheory;

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Group action                                                              *)
(* ------------------------------------------------------------------------- *)

(* Avoid using S, which is a combinator *)
(* An action from group G to a set X is a map f: GxX -> X such that
   (0)   [is a map] f (x in G)(z in X) in X
   (1)  [id action] f (e in G)(z in X) = z
   (2) [composable] f (x in G)(f (y in G)(z in X)) =
                    f ((mult in G)(x in G)(y in G))(z in X)
*)
val action_def = Define `
   action f g X = !z. z IN X ==>
     (!x :: (g.carrier). f x z IN X) /\
     (f #e z = z) /\
     (!x y :: (g.carrier). f x (f y z) = f (x * y) z)
`;

(* ------------------------------------------------------------------------- *)
(* Some properties of action                                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For action f g X /\ x IN X, !a IN g.carrier, f a x IN X  *)
(* Proof: by action_def. *)
val ACTION_CLOSURE = store_thm(
  "ACTION_CLOSURE",
  ``!f g X x a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier ==> f a x IN X``,
  SRW_TAC [][action_def]);

(* Theorem: For action f g X /\ x IN X, !a,b IN g.carrier, f a (f b x) = f (a*b) x  *)
(* Proof: by action_def. *)
val ACTION_COMPOSE = store_thm(
  "ACTION_COMPOSE",
  ``!f g X x a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier /\ b IN g.carrier ==> (f a (f b x) = f (a*b) x)``,
  SRW_TAC [][action_def]);

(* Theorem: For action f g X /\ x IN X, f #e x = x  *)
(* Proof: by action_def. *)
val ACTION_ID = store_thm(
  "ACTION_ID",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==> (f #e x = x)``,
  SRW_TAC [][action_def]);
(* This is essentially REACH_REFL *)

(* Theorem: For action f g X /\ x IN X /\ y IN X, !a IN g.carrier, f a x = y ==> f |/a y = x.  *)
(* Proof:
   Since a IN g.carrier, |/ a IN g.carrier    by group_inv_carrier
     f (|/ a) y
   = f (|/ a) (f a x)    by y = f a x
   = f (|/ a * a) x      by action_def
   = f #e x              by group_rinv
   = x                   by action_def
*)
val ACTION_REVERSE = store_thm(
  "ACTION_REVERSE",
  ``!f g X x y a. Group g /\ action f g X /\ x IN X /\ y IN X /\ a IN g.carrier ==>
   (f a x = y) ==> (f (|/ a) y = x)``,
  REPEAT STRIP_TAC THEN
  `|/ a IN g.carrier` by SRW_TAC [][group_inv_carrier] THEN
  `f (|/ a) y = f (|/ a) (f a x)` by SRW_TAC [][] THEN
  `_ = f (|/a * a) x` by METIS_TAC [ACTION_COMPOSE, group_mult_carrier] THEN
  METIS_TAC [group_linv, ACTION_ID]);
(* This is essentially REACH_SYM *)

(* Theorem: For action f g X /\ x IN X /\ y IN X, z IN X,
            if f a x = y /\ f b y = z, then f (b*a) x = z.  *)
(* Proof:
   By action_def,
   f (b*a) x = f b (f a x) = f b y = z.
*)
val ACTION_TRANS = store_thm(
  "ACTION_TRANS",
  ``!f g X x y a. Group g /\ action f g X /\ x IN X /\ y IN X /\ z IN X /\
      a IN g.carrier /\ b IN g.carrier ==>
   (f a x = y) /\ (f b y = z) ==> (f (b * a) x = z)``,
  METIS_TAC [ACTION_COMPOSE, group_mult_carrier]);
(* This is essentially REACH_TRANS *)

(* ------------------------------------------------------------------------- *)
(* Group action induces an equivalence relation.                             *)
(* ------------------------------------------------------------------------- *)

(* Define reach to relate two action points x y in X *)
val reach_def = Define`
  reach f g x y = ?z. z IN g.carrier /\ (f z x = y)
`;

(* eliminate "group" from default simpset *)
(* val groupSS = diminish_srw_ss ["group"]; -- essential? *)

(* Theorem: [Reach is Reflexive] !x in X, reach f g x x.  *)
(* Proof:
   By action_def, f #e x = x.
   Since #e in g.carrier by group_id_carrier, this satisfies reach_def.
*)
val REACH_REFL = store_thm(
  "REACH_REFL",
  ``!f g X. Group g /\ action f g X ==> (!x. x IN X ==> reach f g x x)``,
  METIS_TAC [reach_def, ACTION_ID, group_id_carrier]);

(* Theorem: [Reach is Symmetric] !x y in X, reach f g x y ==> reach f g y x. *)
(* Proof:
   By reach_def, ?z IN g.carrier. f z x = y.
   By action_def, ?|/z IN g.carrier.
     f |/z y
   = f |/z (f z x)   by y = f z x
   = f (|/z * z) x   by action_def
   = f #e x          by Group_def
   = x               by action_def
*)
val REACH_SYM = store_thm(
  "REACH_SYM",
  ``!f g X. Group g /\ action f g X ==>
    (!x y. x IN X /\ y IN X ==> reach f g x y ==> reach f g y x)``,
  METIS_TAC [reach_def, ACTION_REVERSE, group_inv_carrier, group_linv]);

(* Theorem: [Reach is Transitive] !x y z in X, reach f g x y /\ reach f g y z ==> reach f g x z. *)
(* Proof:
   By reach_def, ?z1 IN g.carrier. f z1 x = y.
                 ?z2 IN g.carrier. f z2 y = z.
   By action_def,
     f (z2*z1) x
   = f z2 (f z1 x)  by action_def
   = f z2 y         by f z1 x = y
   = z              by f z2 y = z
*)
val REACH_TRANS = store_thm(
  "REACH_TRANS",
  ``!f g X. Group g /\ action f g X ==>
    (!x y z. x IN X /\ y IN X /\ z IN X ==> reach f g x y /\ reach f g y z ==> reach f g x z)``,
  SRW_TAC [][reach_def] THEN
  Q.EXISTS_TAC `z''*z'` THEN
  METIS_TAC [ACTION_TRANS, group_mult_carrier]);

(* Theorem: Reach is an equivalence relation on target set X. *)
(* Proof:
   By Reach being Reflexive, Symmetric and Transitive.
*)
val REACH_EQUIV_ON_TARGET = store_thm(
  "REACH_EQUIV_ON_TARGET",
  ``!f g X. Group g /\ action f g X ==> reach f g equiv_on X``,
  SRW_TAC [][equiv_on_def] THEN
  METIS_TAC [REACH_REFL, REACH_SYM, REACH_TRANS]);

(* ------------------------------------------------------------------------- *)
(* Partition by Group action.                                                *)
(* ------------------------------------------------------------------------- *)

(* Define partitions of Target set X by reach f g. *)
val TargetPartition_def = Define `
  TargetPartition f g X = partition (reach f g) X
`;

(* Theorem: Target partition is FINITE *)
val FINITE_TARGET_PARTITION = store_thm(
  "FINITE_TARGET_PARTITION",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==> FINITE (TargetPartition f g X)``,
  SRW_TAC [][TargetPartition_def, REACH_EQUIV_ON_TARGET, FINITE_partition]);

(* Theorem: For e IN (TargetPartition f g X), FINITE X ==> FINITE e *)
val FINITE_TARGET_PARTITION_ELEMENT = store_thm(
  "FINITE_TARGET_PARTITION_ELEMENT",
  ``!f g X. Group g /\ action f g X ==>
     !e. (e IN TargetPartition f g X) ==> FINITE X ==> FINITE e``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, FINITE_partition]);

(* Theorem: For e IN (TargetPartition f g X), e <> EMPTY *)
val TARGET_PARTITION_ELEMENT_NONEMPTY = store_thm(
  "TARGET_PARTITION_ELEMENT_NONEMPTY",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> e <> {}``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, EMPTY_NOT_IN_partition]);

(* Theorem: Elements in Element of TargetPartition are in X. *)
val TARGET_PARTITION_ELEMENT_ELEMENT = store_thm(
  "TARGET_PARTITION_ELEMENT_ELEMENT",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> !a. a IN e ==> a IN X``,
  SRW_TAC [][TargetPartition_def, partition_def] THEN
  FULL_SIMP_TAC (srw_ss())[]);

(* Theorem: TargetPartition elements are subset of target *)
val TARGET_PARTITION_ELEMENT_SUBSET_TARGET = store_thm(
  "TARGET_PARTITION_ELEMENT_SUBSET_TARGET",
  ``!f g X. Group g /\ action f g X ==>
   !e. e IN TargetPartition f g X ==> e SUBSET X``,
  METIS_TAC [TARGET_PARTITION_ELEMENT_ELEMENT, SUBSET_DEF]);

(* ------------------------------------------------------------------------- *)
(* Orbits as equivalence classes.                                            *)
(* ------------------------------------------------------------------------- *)

(* Orbit of action: those x in X that can be reached by a in X *)
val orbit_def = Define`
  orbit f g X a = {x | x IN X /\ reach f g a x }
`;

(* Theorem: y IN (orbit f g X x) ==> y IN X /\ reach f g x y *)
(* Proof: by definitions *)
val ORBIT_PROPERTY = store_thm(
  "ORBIT_PROPERTY",
  ``!f g X x y. y IN orbit f g X x ==> y IN X /\ reach f g x y``,
  SRW_TAC [][orbit_def]);

(* Theorem: (orbit f g X x) = IMAGE (\a. f a x) g.carrier *)
(* Proof: by definitions *)
val ORBIT_DESCRIPTION = store_thm(
  "ORBIT_DESCRIPTION",
  ``!(f:'a -> 'b -> 'b) g X x.
          Group g /\ action f g X /\ x IN X ==>
          (orbit f g X x = {f a x | a IN g.carrier})``,
  SRW_TAC [][action_def, orbit_def, reach_def, EXTENSION, EQ_IMP_THM] THEN METIS_TAC []);

(* same as: ORBIT_IS_ACTION_IMAGE *)
val ORBIT_AS_IMAGE = store_thm(
  "ORBIT_AS_IMAGE",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==> (orbit f g X x = IMAGE (\a. f a x) g.carrier)``,
  SRW_TAC [][action_def, orbit_def, reach_def, EXTENSION, EQ_IMP_THM] THEN METIS_TAC []);

(* Theorem: if a in g.carrier, then f x a in (orbit f g X a). *)
(* <<ORBIT_HAS_ACTION_ELEMENT>> *)
val ORBIT_HAS_ACTION_ELEMENT = store_thm(
  "ORBIT_HAS_ACTION_ELEMENT",
  ``!f g X x a. action f g X /\ x IN X /\ a IN g.carrier ==> f a x IN (orbit f g X x)``,
  SRW_TAC [][orbit_def, reach_def] THEN
  METIS_TAC [action_def]);

(* Theorem: action f g X ==> x IN orbit f g X x *)
(* Proof: by definition, and f e x = x *)
val ACTION_ORBIT_HAS_ITSELF = store_thm(
  "ACTION_ORBIT_HAS_ITSELF",
  ``!f g X. Group g /\ action f g X /\ x IN X ==> x IN orbit f g X x``,
  SRW_TAC [][orbit_def] THEN
  METIS_TAC [REACH_REFL]);

(* Theorem: orbits are subsets of target set X *)
val ORBIT_SUBSET_TARGET = store_thm(
  "ORBIT_SUBSET_TARGET",
  ``!f g X a. action f g X /\ a IN X ==> orbit f g X a SUBSET X``,
  SRW_TAC [][orbit_def, SUBSET_DEF]);

(* Theorem: Elements of TargetPartition are orbits of its own element.
            !e IN TargetPartition f g X ==> !a IN e. e = orbit f g X a *)
(* Proof:
   Essentially this is to prove:
   (1) x' IN e ==> x' IN X
       This is true since e is a partition of X.
   (2) reach f g x a /\ reach f g x x' ==> reach f g a x'
           reach f g x a /\ reach f g x x'
       ==> reach f g a x /\ reach f g x x'  by REACH_SYM
       ==> reach f g a x'                   by REACH_TRANS
   (3) a IN e /\ reach f g a x' ==> x' IN e
       Since a IN e ==> reach f g x a       by orbit_def
       So  reach f g x a /\ reach f g a x'
       ==> reach f g x x'                   by REACH_TRANS
       Therefore x' IN e                    by orbit_def.
*)
val TARGET_PARTITION_ELEMENT_IS_ORBIT = store_thm(
  "TARGET_PARTITION_ELEMENT_IS_ORBIT",
  ``!f g X. Group g /\ action f g X ==>
    !e. e IN TargetPartition f g X ==> !a. a IN e ==> (e = orbit f g X a)``,
  SRW_TAC [][TargetPartition_def, partition_def, orbit_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [REACH_SYM, REACH_TRANS]);

(* Theorem: For action f g X, all a in X are reachable, belongs to some orbit,
            i.e. (orbit f g X a) = IMAGE (\x. f x a) g.carrier.
   This should follow from the fact that reach induces a partition, and
   the partition elements are orbits (TARGET_PARTITION_ELEMENT_IS_ORBIT).
   Is such a proof more complicated, or simpler, than the one below?        *)
(* Proof:
   Essentially this is to prove:
   (1) a IN x /\ x IN g.carrier ==> reach f g a (f x a)
       This is because (f x a) IN X   by action_def
       Hence reach f g a (f x a)      by reach_def
   (2) a IN x /\ x IN g.carrier /\ reach f g a x ==> ?x'. x' IN g.carrier /\ (f x' a = x)
       reach f g a x ==> ?z. z IN g.carrier /\ (f z a = x)  by reach_def.
*)
val ACTION_TO_ORBIT_SURJ = store_thm(
  "ACTION_TO_ORBIT_SURJ",
  ``!f g X a. action f g X /\ a IN X ==> SURJ (\x. f x a) g.carrier (orbit f g X a)``,
  SRW_TAC [][action_def, orbit_def, SURJ_DEF] THEN METIS_TAC [reach_def]);

(* Theorem: For Action f g X, orbit f g X a = IMAGE (\x. f x a) g.carrier *)
(* Proof:
   By ACTION_TO_ORBIT_SURJ.
*)
val ORBIT_IS_ACTION_IMAGE = store_thm(
  "ORBIT_IS_ACTION_IMAGE",
  ``!f g X a. action f g X /\ a IN X ==> (orbit f g X a = IMAGE (\x. f x a) g.carrier)``,
  SRW_TAC [][ACTION_TO_ORBIT_SURJ, GSYM IMAGE_SURJ]);

(* Theorem: if (\x. f x a) is INJ into orbit for action, then orbit is same size as the group *)
(* Proof:
   Since  SURJ (\x. f x a) g.carrier (orbit f g a) by ACTION_TO_ORBIT_SURJ,
   With INJ given, we have BIJ (\x. f x a) g.carrier (orbit f g X a).
   Now orbit f g a SUBSET X  by ORBIT_SUBSET_TARGET.
   With FINITE X, we have FINITE (orbit f g X a) by SUBSET_FINITE.
   Hence FINITE g.carrier by FINITE_INJ,
   and the conclusion follows by FINITE_BIJ_CARD_EQ.
*)
val CARD_ORBIT_FINITE_INJ = store_thm(
  "CARD_ORBIT_FINITE_INJ",
  ``!f g X a. action f g X /\ a IN X /\ FINITE X ==>
    INJ (\x. f x a) g.carrier (orbit f g X a) ==> (CARD (orbit f g X a) = CARD g.carrier)``,
  METIS_TAC [ACTION_TO_ORBIT_SURJ, BIJ_DEF,
             ORBIT_SUBSET_TARGET, SUBSET_FINITE, FINITE_INJ, FINITE_BIJ_CARD_EQ]);

(* Theorem: For FINITE X, CARD X = SUM of CARD partitions in (TargetPartition f g X) *)
(* Proof:
   Apply partition_CARD
    |- !R s. R equiv_on s /\ FINITE s ==> (CARD s = SIGMA CARD (partition R s))
*)
val CARD_TARGET_BY_PARTITION = store_thm(
  "CARD_TARGET_BY_PARTITION",
  ``!f g X. Group g /\ action f g X /\ FINITE X ==>
    (CARD X = SIGMA CARD (TargetPartition f g X))``,
  METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, partition_CARD]);

(* Theorem: If for all x IN X, (orbit f g X x) = n, then n divides CARD X. *)
val EQUAL_SIZE_ORBITS_PROPERTY = store_thm(
  "EQUAL_SIZE_ORBITS_PROPERTY",
  ``!(f:'a -> 'b -> 'b) g X n. Group g /\ action f g X /\ FINITE X /\
    (!x. x IN X ==> (CARD (orbit f g X x) = n)) ==> divides n (CARD X)``,
  REPEAT STRIP_TAC THEN
  `!e. e IN TargetPartition f g X ==> !x. x IN e ==> (e = orbit f g X x)` by SRW_TAC [][TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `!e. e IN TargetPartition f g X ==> (CARD e = n)` by SRW_TAC [][] THENL [
    `?y. y IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
    METIS_TAC [TARGET_PARTITION_ELEMENT_SUBSET_TARGET, SUBSET_DEF],
    `CARD X = n * CARD (partition (reach f g) X)` by METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, equal_partition_CARD] THEN
    METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_SYM]
  ]);

(* Theorem: If for all x IN X, n divides (orbit f g X x), then n divides CARD X. *)
val FACTOR_ORBITS_PROPERTY = store_thm(
  "FACTOR_ORBITS_PROPERTY",
  ``!f g X n. Group g /\ action f g X /\ FINITE X /\
    (!x. x IN X ==> divides n (CARD (orbit f g X x))) ==> divides n (CARD X)``,
  REPEAT STRIP_TAC THEN
  `!e. e IN TargetPartition f g X ==> !x. x IN e ==> (e = orbit f g X x)` by SRW_TAC [][TARGET_PARTITION_ELEMENT_IS_ORBIT] THEN
  `!e. e IN TargetPartition f g X ==> divides n (CARD e)` by SRW_TAC [][] THENL [
    `?y. y IN e` by METIS_TAC [TARGET_PARTITION_ELEMENT_NONEMPTY, MEMBER_NOT_EMPTY] THEN
    METIS_TAC [TARGET_PARTITION_ELEMENT_SUBSET_TARGET, SUBSET_DEF],
    METIS_TAC [TargetPartition_def, REACH_EQUIV_ON_TARGET, factor_partition_CARD]
  ]);

(* ------------------------------------------------------------------------- *)
(* Stabilizer and properties.                                                *)
(* ------------------------------------------------------------------------- *)

(* Stabilizer of action: for a in X, stabilizer of a = {x in G | f x a = a} *)
val stabilizer_def = Define`
  stabilizer f g a = {x | x IN g.carrier /\ (f x a = a) }
`;

(* Theorem: x IN stabilizer f g a ==> x IN g.carrier and f x a = a. *)
val STABILIZER_PROPERTY = store_thm(
  "STABILIZER_PROPERTY",
  ``!f g X a. action f g X /\ a IN X ==> !x. x IN stabilizer f g a ==> x IN g.carrier /\ (f x a = a)``,
  SRW_TAC [][stabilizer_def]);

(* Theorem: stabilizer f g a has g.id *)
val STABILIZER_HAS_ID = store_thm(
  "STABILIZER_HAS_ID",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==> g.id IN stabilizer f g a``,
  SRW_TAC [][action_def, stabilizer_def, group_id_carrier]);
(* This means (stabilizer f g a) is non-empty *)

(* ------------------------------------------------------------------------- *)
(* Application:                                                              *)
(* Stabilizer subgroup.                                                      *)
(* ------------------------------------------------------------------------- *)

(* Define the generator group, the exponential group of an element a of group g *)
val StabilizerGroup_def = Define`
  StabilizerGroup f g a =
    <| carrier := stabilizer f g a;
            id := g.id;
           inv := |/ ;
          mult := g.mult
     |>`;

(* Theorem: If g is a Group, f g X is an action, StabilizerGroup f g a is a Group *)
(* Proof:
   Essentially this is to prove:
   (1) f x a = a /\ f y a = a ==> f (x*y) a = a
         f (x*y) a
       = f x (f y a)      by action_def
       = f x a
       = a

   (2) f (|/x) a
     = f (|/x) (f x a)     by f x a = a
     = f (|/x * x) a       by action_def
     = f (#e) a            by group_linv
     = a                   by action_def
*)
val StabilizerGroup_group = store_thm(
  "StabilizerGroup_group",
  ``!f g X a. action f g X /\ a IN X /\ Group g ==> Group (StabilizerGroup f g a)``,
  SRW_TAC [][Group_def, StabilizerGroup_def, stabilizer_def, action_def, RES_FORALL_THM] THEN METIS_TAC []);

(* Theorem: The stabilizer is a subset of g.carrier *)
val STABILIZER_SUBSET = store_thm(
  "STABILIZER_SUBSET",
  ``!f g X. action f g X /\ a IN X ==> (stabilizer f g a) SUBSET g.carrier``,
  SRW_TAC [][stabilizer_def, SUBSET_DEF]);

(* Theorem: If g is Group, f g X is an action, then stabilizer is a subgroup of g *)
(* Proof:
   Essentially this is to prove:
   (1) Group (StabilizerGroup f g a),     true by StabilizerGroup_group
   (2) stabilizer f g a SUBSET g.carrier, true by STABILIZER_SUBSET
*)
val StabilizerGroup_subgroup = store_thm(
  "StabilizerGroup_subgroup",
  ``!f g X. action f g X /\ a IN X /\ Group g ==> Subgroup (StabilizerGroup f g a) g``,
  SRW_TAC [][Subgroup_def, StabilizerGroup_def] THEN
  METIS_TAC [StabilizerGroup_def, StabilizerGroup_group, STABILIZER_SUBSET]);

(* Theorem: If g is FINITE Group, StabilizerGroup f g a is a FINITE Group *)
(* Proof:
   This is to prove:
   (1) Group (StabilizerGroup f g a)    true by StabilizerGroup_group
   (2) FINITE g.carrier ==> FINITE (StabilizerGroup f g a).carrier, true by STABILIZER_SUBSET and SUBSET_FINITE
*)
val FINITE_StabilizerGroup = store_thm(
  "FINITE_StabilizerGroup",
  ``!f g X a. action f g X /\ a IN X /\ FiniteGroup g ==> FiniteGroup (StabilizerGroup f g a)``,
  SRW_TAC [][FiniteGroup_def] THEN1 METIS_TAC [StabilizerGroup_group] THEN
  SRW_TAC [][StabilizerGroup_def] THEN1 METIS_TAC [STABILIZER_SUBSET, SUBSET_FINITE]);

(* Theorem: If g is FINITE Group, CARD (stabilizer f g a) divides CARD (g.carrier) *)
(* Proof:
   By Lagrange's Theorem, and (StabilizerGroup f g a) is a subgroup of g.
*)
val CARD_STABILIZER_DIVIDES_GROUP = store_thm(
  "CARD_STABILIZER_DIVIDES_GROUP",
  ``!f g X a. action f g X /\ a IN X /\ FiniteGroup g ==>
   divides (CARD (stabilizer f g a)) (CARD g.carrier)``,
  REPEAT STRIP_TAC THEN
  `Subgroup (StabilizerGroup f g a) g` by METIS_TAC [FiniteGroup_def, StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  METIS_TAC [LAGRANGE_THM]);

(* ------------------------------------------------------------------------- *)
(* Another proof of Orbit-Stabilizer Theorem.                                *)
(* ------------------------------------------------------------------------- *)

(* Theorem: The map from orbit to coset of stabilizer is well-defined. *)
(* Proof:
     f (|/y * x) a
   = f (|/y) (f x a)  by action_def
   = f (|/y) (f y a)  by given
   = f (|/y * y) a    by action_def
   = f #e a           by group_linv
   = a                by action_id
   (|/y * x) IN (stabilizer f g a)
   hence cosets are equal by SUBGROUP_COSET_EQ.
*)
val ORBIT_STABILIZER_MAP_WD = store_thm(
  "ORBIT_STABILIZER_MAP_WD",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==>
   !x y. x IN g.carrier /\ y IN g.carrier /\ (f x a = f y a) ==>
   (coset g (stabilizer f g a) x = coset g (stabilizer f g a) y)``,
  REPEAT STRIP_TAC THEN
  `StabilizerGroup f g a <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  FULL_SIMP_TAC (srw_ss()) [action_def] THEN
  `f (|/y * x) a = f (|/y) (f x a)` by SRW_TAC [][group_inv_carrier] THEN
  `_ = f (|/y) (f y a)` by ASM_REWRITE_TAC [] THEN
  `_ = f (|/y * y) a` by SRW_TAC [][group_inv_carrier] THEN
  `_ = f #e a` by SRW_TAC [][group_linv] THEN
  `_ = a` by SRW_TAC [][] THEN
  `(|/y * x) IN g.carrier` by SRW_TAC [][group_inv_carrier, group_mult_carrier] THEN
  `(|/y * x) IN (stabilizer f g a)` by SRW_TAC [][stabilizer_def] THEN
  METIS_TAC [SUBGROUP_COSET_EQ]);

(* Theorem: The map from orbit to coset of stabilizer is injective. *)
(* Proof:
       coset g (stabilizer f g a) x = coset g (stabilizer f g a) y
   ==> (|/y * x) IN (stabilizer f g a)   by SUBGROUP_COSET_EQ
   ==> f (|/y * x) a = a                 by stabilizer_def
     f x a
   = f (#e*x) a           by group_lid
   = f ((y*|/y)*x) a      by group_rinv
   = f (y*(|/y*x)) a      by group_assoc
   = f y (f (|/y * x) a)  by action_def
   = f y a                by a = f (|/y * x) a
*)
val ORBIT_STABILIZER_MAP_UQ = store_thm(
  "ORBIT_STABILIZER_MAP_UQ",
  ``!f g X a. Group g /\ action f g X /\ a IN X ==>
   !x y. x IN g.carrier /\ y IN g.carrier /\
   (coset g (stabilizer f g a) x = coset g (stabilizer f g a) y) ==> (f x a = f y a)``,
  REPEAT STRIP_TAC THEN
  `StabilizerGroup f g a <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g a).carrier = stabilizer f g a` by SRW_TAC [][StabilizerGroup_def] THEN
  `(|/y * x) IN (stabilizer f g a)` by METIS_TAC [SUBGROUP_COSET_EQ] THEN
  `f (|/y * x) a = a` by FULL_SIMP_TAC (srw_ss()) [stabilizer_def] THEN
  `|/y * x IN g.carrier` by SRW_TAC [][group_inv_carrier, group_mult_carrier] THEN
  `f x a = f (#e*x) a` by SRW_TAC [][group_lid] THEN
  `_ = f ((y* |/y)*x) a` by SRW_TAC [][group_rinv] THEN
  `_ = f (y*(|/y*x)) a` by SRW_TAC [][group_assoc, group_inv_carrier] THEN
  `_ = f y (f (|/y * x) a)` by FULL_SIMP_TAC (srw_ss()) [action_def] THEN
  METIS_TAC []);

(* Theorem: For action f g X /\ x IN X,
   !a, b in g.carrier, f a x = f b x <=> |/ a * b IN (stabilizer f g x)  *)
(* Proof:
     f a x = f b x
   <=> f (1/a) (f b a) = f (1/a) (f a x)
   <=> f (1/a*b) x = f (1/a*a) x
   <=> f (1/a*b) x = f #e x
   <=> f (1/a*b) x = x
   <=>    1/a*b IN (stabilizer f g x)
   Essentially this is to prove:
   (1) f a x = f b x ==> f (1/ a * b) x = x
         f (1/a * b) x
       = f (1/a) (f b x)  by action_def
       = f (1/a) (f a x)  by given
       = f (1/a * a) x    by action_def
       = f #e x
       = x
   (2) f (1/ a * b) x = x ==> f a x = f b x
         f a x
       = f a (f (1/a * b) x)    by x = f (1/a * b) x
       = f (a*(1/a*b) x         by action_def
       = f ((a*1/a)*b) x        by group_assoc
       = f (#e*b) x             by group_rinv
       = f b x                  by group_lid
*)
val ACTION_MATCH_CONDITION = store_thm(
  "ACTION_MATCH_CONDITION",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==>
   !a b. a IN g.carrier /\ b IN g.carrier ==> ((f a x = f b x) <=> (|/ a * b) IN (stabilizer f g x))``,
  SRW_TAC [][EQ_IMP_THM] THENL [
    `|/a IN g.carrier /\ |/a * b IN g.carrier` by METIS_TAC [group_inv_carrier, group_mult_carrier] THEN
    `f (|/ a * b) x = f (|/ a) (f b x)` by METIS_TAC [ACTION_CLOSURE, ACTION_COMPOSE] THEN
    `_ = x` by METIS_TAC [ACTION_CLOSURE, ACTION_REVERSE] THEN
    SRW_TAC [][stabilizer_def],
    `(|/ a * b) IN g.carrier /\ (f (|/a * b) x = x)` by METIS_TAC [STABILIZER_PROPERTY] THEN
    `f a x = f a (f (|/a * b) x)` by SRW_TAC [][] THEN
    `_ = f (a * (|/ a * b)) x` by METIS_TAC [ACTION_COMPOSE] THEN
    `_ = f ((a * |/ a) * b) x` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    SRW_TAC [][group_rinv, group_lid]
  ]);

(* Theorem: stabilizers of points in same orbit:
   stabilizer f g (z*a) = z * (stabilizer f g a) * 1/z
   Proof: in Section 1.12 of Volume I of [Jacobson] N.Jacobson, Basic Algebra, 1980.
   [Artin] E. Artin, Galois Theory 1942.
   Proof:
   Essentially this is to prove:
   (1) x' IN g.carrier /\ f x' (f a x) = f a x ==> ?z. (x' = a * z * |/ a) /\ z IN g.carrier /\ (f z x = x)
   (2) z IN g.carrier /\ f z x = x ==> a * z * |/ a IN g.carrier
   (3) z IN g.carrier /\ f z x = x ==> f (a * z * |/ a) (f a x) = f a x
*)
val REACHABLE_STABILIZER = store_thm(
  "REACHABLE_STABILIZER",
  ``!f g X a. Group g /\ action f g X /\ x IN X /\ a IN g.carrier ==>
   (stabilizer f g (f a x) = conjugate g a (stabilizer f g x))``,
  SRW_TAC [][conjugate_def, stabilizer_def, EXTENSION, EQ_IMP_THM] THENL [
    `|/a IN g.carrier` by METIS_TAC [group_inv_carrier] THEN
    Q.EXISTS_TAC `|/a * x' * a` THEN
    SRW_TAC [][] THENL [
      `a * (|/ a * x' * a) = a * (|/ a * (x' * a))` by SRW_TAC [][group_assoc, group_mult_carrier] THEN
      `_ = (a * |/ a) * (x' * a)` by SRW_TAC [][group_assoc, group_mult_carrier] THEN
      `_ = x' * a` by SRW_TAC [][group_rinv, group_lid, group_mult_carrier] THEN
      `a * (|/ a * x' * a) * |/ a = x' * a * |/ a` by SRW_TAC [][] THEN
      `_ = x' * (a * |/ a)` by SRW_TAC [][group_assoc, group_mult_carrier] THEN
      SRW_TAC [][group_rinv, group_rid],
      SRW_TAC [][group_inv_carrier, group_mult_carrier],
      `|/ a * x' IN g.carrier /\ x' * a IN g.carrier` by METIS_TAC [group_mult_carrier] THEN
      `f (|/ a * x' * a) x = f (|/ a * (x' * a)) x` by SRW_TAC [][group_assoc] THEN
      `_ = f (|/ a) (f (x' * a) x)` by METIS_TAC [ACTION_COMPOSE] THEN
      `_ = f (|/ a) (f x' (f a x))` by METIS_TAC [ACTION_COMPOSE] THEN
      `_ = f (|/ a) (f a x)` by SRW_TAC [][] THEN
      `_ = f (|/ a * a) x` by METIS_TAC [ACTION_COMPOSE] THEN
      METIS_TAC [group_linv, ACTION_ID]
    ],
    METIS_TAC [group_inv_carrier, group_mult_carrier],
    `|/a IN g.carrier` by METIS_TAC [group_inv_carrier] THEN
    `a * z * |/ a IN g.carrier` by METIS_TAC [group_mult_carrier] THEN
    `f (a * z * |/ a) (f a x) = f (a * z * |/ a * a) x` by METIS_TAC [ACTION_COMPOSE] THEN
    `_ = f ((a * z) * (|/ a * a)) x` by SRW_TAC [][group_assoc, group_mult_carrier] THEN
    `_ = f (a * z) x` by METIS_TAC [group_linv, group_rid, group_mult_carrier] THEN
    METIS_TAC [ACTION_COMPOSE]
  ]);

(* - reach_def;
> val it = |- !f g x y. reach f g x y <=> ?z. z IN g.carrier /\ (f z x = y) :  thm
*)

(* Existence of act_by:  reach x y ==> ?a. a IN g.carrier /\ f a x = y *)
val lemma = prove(
  ``!f g x y. ?a. reach f g x y ==> a IN g.carrier /\ (f a x = y)``,
  METIS_TAC [reach_def]);
(*
- SKOLEM_THM;
> val it = |- !P. (!x. ?y. P x y) <=> ?f. !x. P x (f x) : thm
*)
val ACT_BY_DEF = new_specification(
    "ACT_BY_DEF",
    ["ACT_BY"],
    SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);
(* > val ACT_BY_DEF =
    |- !f' g x y.
         reach f' g x y ==>
         ACT_BY f' g x y IN g.carrier /\ (f' (ACT_BY f' g x y) x = y) : thm
*)

(* Theorem: The reachable set from x to y is the coset ACT_BY y of (stabilizer x) *)
val ACTION_REACHABLE_COSET = store_thm(
  "ACTION_REACHABLE_COSET",
  ``!f g X x. Group g /\ action f g X /\ x IN X /\ y IN orbit f g X x ==>
   ({a | a IN g.carrier /\ (f a x = y)} = coset g (stabilizer f g x) (ACT_BY f g x y))``,
  SRW_TAC [][orbit_def, coset_def, EXTENSION, EQ_IMP_THM] THENL [
    `ACT_BY f g x (f x' x) IN g.carrier /\ (f (ACT_BY f g x (f x' x)) x = (f x' x))` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.ABBREV_TAC `a = ACT_BY f g x (f x' x)` THEN
    `|/ a * x' IN (stabilizer f g x)` by METIS_TAC [ACTION_MATCH_CONDITION] THEN
    Q.EXISTS_TAC `|/ a * x'` THEN
    `a * (|/ a * x') = (a * |/ a)* x'` by SRW_TAC [][group_assoc, group_inv_carrier] THEN
    METIS_TAC [group_rinv, group_lid],
    METIS_TAC [ACT_BY_DEF, group_mult_carrier, STABILIZER_PROPERTY],
    METIS_TAC [ACT_BY_DEF, ACTION_COMPOSE, STABILIZER_PROPERTY]
  ]);

(* Theorem: The reachable set from x to y is the coset ACT_BY y of (stabilizer x), another formulation. *)
(* <<ACTION_REACHABLE_COSET2>> *)
(*
g `!f g X x. Group g /\ action f g X /\ x IN X /\ y IN orbit f g X x ==>
   !b. b IN g.carrier /\ (f b x = y) ==> ({a | a IN g.carrier /\ (f a x = y)} = coset g (stabilizer f g x) b)`;
e (SRW_TAC [][orbit_def, coset_def, EXTENSION, EQ_IMP_THM]); >> 3
-- case: ?z. (x' = b * z) /\ z IN stabilizer f g x
e (Q.EXISTS_TAC `|/b * x'`);
e (RW_TAC std_ss []); >> 
(* case: x' = b * (|/ b * x') *)
e (`b * (|/ b * x') = (b * |/ b) * x'` by SRW_TAC [][group_assoc, group_inv_carrier]);
e (METIS_TAC [group_rinv, group_lid]); <<
-- case: |/ b * x' IN stabilizer f g x
e (SRW_TAC [][group_inv_carrier, group_mult_carrier, stabilizer_def]);
e (METIS_TAC [group_inv_carrier, ACTION_COMPOSE, ACTION_REVERSE]); <<
-- case: b * z IN G
e (METIS_TAC [group_inv_carrier, group_mult_carrier, STABILIZER_PROPERTY]); <<
-- case: f (b * z) x = f b x
e (METIS_TAC [ACTION_COMPOSE, STABILIZER_PROPERTY]); <<
drop();
*)
val ACTION_REACHABLE_COSET2 = store_thm(
  "ACTION_REACHABLE_COSET2",
  ``!f g X x. Group g /\ action f g X /\ x IN X /\ y IN orbit f g X x ==>
   !b. b IN g.carrier /\ (f b x = y) ==> ({a | a IN g.carrier /\ (f a x = y)} = coset g (stabilizer f g x) b)``,
  SRW_TAC [][orbit_def, coset_def, EXTENSION, EQ_IMP_THM] THENL [
    Q.EXISTS_TAC `|/b * x'` THEN
    RW_TAC std_ss [] THENL [
      `b * (|/ b * x') = (b * |/ b) * x'` by SRW_TAC [][group_assoc, group_inv_carrier] THEN
      METIS_TAC [group_rinv, group_lid],
      SRW_TAC [][group_inv_carrier, group_mult_carrier, stabilizer_def] THEN
      METIS_TAC [group_inv_carrier, ACTION_COMPOSE, ACTION_REVERSE]
    ],
    METIS_TAC [group_inv_carrier, group_mult_carrier, STABILIZER_PROPERTY],
    METIS_TAC [ACTION_COMPOSE, STABILIZER_PROPERTY]
  ]);
   
(* Theorem: Points of (orbit x) and cosets of (stabilizer x) are one-to-one. *)
(* previously called: ORBIT_STABILIZER *)
(* Proof:
   Essentially this is to prove:
   (1) z IN orbit f g X x ==>
       ?a. (coset g (stabilizer f g x) (ACT_BY f g x z) = coset g (stabilizer f g x) a) /\ a IN g.carrier
   (2) z IN orbit f g X x /\ z' IN orbit f g X x /\
       coset g (stabilizer f g x) (ACT_BY f g x z) = coset g (stabilizer f g x) (ACT_BY f g x z') ==> z = z'
   (3) same as (1)
   (4) a IN g.carrier ==>
       ?z. z IN orbit f g X x /\ (coset g (stabilizer f g x) (ACT_BY f g x z) = coset g (stabilizer f g x) a)
*)
val ORBIT_STABILIZER_COSETS_BIJ3 = store_thm(
  "ORBIT_STABILIZER_COSETS_BIJ3",
  ``!f g X a. Group g /\ action f g X /\ x IN X ==>
   BIJ (\z.  coset g (stabilizer f g x) (ACT_BY f g x z))
       (orbit f g X x)
       {coset g (stabilizer f g x) a | a | a IN g.carrier}``,
  SRW_TAC [][BIJ_DEF, INJ_DEF, SURJ_DEF, EQ_IMP_THM] THEN1
  METIS_TAC [ORBIT_PROPERTY, ACT_BY_DEF] THEN1
 (`reach f g x z /\ reach f g x z'` by METIS_TAC [ORBIT_PROPERTY] THEN
  `ACT_BY f g x z IN g.carrier /\ ACT_BY f g x z' IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
  `f (ACT_BY f g x z) x = f (ACT_BY f g x z') x` by METIS_TAC [ORBIT_STABILIZER_MAP_UQ] THEN
  METIS_TAC [ACT_BY_DEF]) THEN1
  METIS_TAC [ORBIT_PROPERTY, ACT_BY_DEF] THEN
  Q.EXISTS_TAC `f a x` THEN
  SRW_TAC [][] THENL [
    `reach f g x (f a x)` by METIS_TAC [reach_def] THEN
    `f a x IN X` by METIS_TAC [ACTION_CLOSURE] THEN
    SRW_TAC [][orbit_def],
    `reach f g x (f a x)` by METIS_TAC [reach_def] THEN
    METIS_TAC [ORBIT_STABILIZER_MAP_WD, ACT_BY_DEF]
  ]);
(* This version is not using CosetPartition *)

(* Theorem: Points of (orbit x) and cosets of (stabilizer x) are one-to-one. *)
(* Proof:
   Essentially this is to prove:
   (1) z IN orbit f g X x ==>
       ?a. (coset g (stabilizer f g x) (ACT_BY f g x z) = coset g (stabilizer f g x) a) /\ a IN g.carrier
   (2) z IN orbit f g X x /\ z' IN orbit f g X x /\
       coset g (stabilizer f g x) (ACT_BY f g x z) = coset g (stabilizer f g x) (ACT_BY f g x z') ==> z = z'
   (3) same as (1)
   (4) a IN g.carrier ==>
       ?z. z IN orbit f g X x /\ (coset g (stabilizer f g x) (ACT_BY f g x z) = coset g (stabilizer f g x) a)
*)
val ORBIT_STABILIZER_COSETS_BIJ4 = store_thm(
  "ORBIT_STABILIZER_COSETS_BIJ4",
  ``!f g X x. Group g /\ action f g X /\ x IN X ==>
   BIJ (\z.  coset g (stabilizer f g x) (ACT_BY f g x z))
       (orbit f g X x)
       (CosetPartition g (StabilizerGroup f g x))``,
  SIMP_TAC (srw_ss()) [CosetPartition_def, partition_def, inCoset_def,
                       StabilizerGroup_def, BIJ_DEF, INJ_DEF, SURJ_DEF] THEN
  REPEAT GEN_TAC THEN STRIP_TAC THEN REPEAT CONJ_TAC THENL [
    Q.X_GEN_TAC `z` THEN STRIP_TAC THEN
    `reach f g x z` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.EXISTS_TAC `ACT_BY f g x z` THEN
    ASM_SIMP_TAC (srw_ss()) [EXTENSION, EQ_IMP_THM] THEN Q.X_GEN_TAC `x'` THEN
    STRIP_TAC THEN
    `x' IN IMAGE (\z'. ACT_BY f g x z * z') (stabilizer f g x)`
      by METIS_TAC [coset_def] THEN
    `?z'. z' IN (stabilizer f g x) /\ (x' = ACT_BY f g x z * z')`
       by METIS_TAC [IN_IMAGE] THEN
    METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier],
    MAP_EVERY Q.X_GEN_TAC [`z`, `z'`] THEN REPEAT STRIP_TAC THEN
    `reach f g x z /\ reach f g x z'` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier /\ ACT_BY f g x z' IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    `f (ACT_BY f g x z) x = f (ACT_BY f g x z') x` by METIS_TAC [ORBIT_STABILIZER_MAP_UQ] THEN
    METIS_TAC [ACT_BY_DEF],
    Q.X_GEN_TAC `z` THEN REPEAT STRIP_TAC THEN
    `reach f g x z` by METIS_TAC [ORBIT_PROPERTY] THEN
    `ACT_BY f g x z IN g.carrier` by SRW_TAC [][ACT_BY_DEF] THEN
    Q.EXISTS_TAC `ACT_BY f g x z` THEN
    ASM_SIMP_TAC (srw_ss())[EXTENSION, EQ_IMP_THM] THEN Q.X_GEN_TAC `x'` THEN
    STRIP_TAC THEN
    `x' IN IMAGE (\z'. ACT_BY f g x z * z') (stabilizer f g x)` by METIS_TAC [coset_def] THEN
    `?z'. z' IN (stabilizer f g x) /\ (x' = ACT_BY f g x z * z')` by METIS_TAC [IN_IMAGE] THEN
    METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier],
    Q.X_GEN_TAC `x'` THEN
    DISCH_THEN (Q.X_CHOOSE_THEN `x''` STRIP_ASSUME_TAC) THEN
    Q.EXISTS_TAC `f x'' x` THEN
    SRW_TAC [][] THENL [
      `reach f g x (f x'' x)` by METIS_TAC [reach_def] THEN
      `f x'' x IN X` by METIS_TAC [ACTION_CLOSURE] THEN
      SRW_TAC [][orbit_def],
      `reach f g x (f x'' x)` by METIS_TAC [reach_def] THEN
      `ACT_BY f g x (f x'' x) IN g.carrier /\ (f (ACT_BY f g x (f x'' x)) x = (f x'' x))` by SRW_TAC [][ACT_BY_DEF] THEN
      `coset g (stabilizer f g x) (ACT_BY f g x (f x'' x)) = coset g (stabilizer f g x) x''` by METIS_TAC [ORBIT_STABILIZER_MAP_WD] THEN
      ASM_SIMP_TAC (srw_ss()) [EXTENSION, EQ_IMP_THM] THEN
      Q.X_GEN_TAC `x'` THEN STRIP_TAC THEN
      `x' IN IMAGE (\z. x'' * z) (stabilizer f g x)` by METIS_TAC [coset_def] THEN
      `?z. z IN (stabilizer f g x) /\ (x' = x'' * z)` by METIS_TAC [IN_IMAGE] THEN
      METIS_TAC [STABILIZER_PROPERTY, group_mult_carrier]
    ]
  ]);

(* Theorem: [Orbit-Stabilizer Theorem]
            CARD G = CARD (orbit x) * CARD (stabilizer x) *)
(* Proof:
   By LAGRANGE_IDENTITY:
       !g h. FiniteGroup g /\ h <= g ==>
       (CARD g.carrier = CARD h.carrier * CARD (CosetPartition g h))

   Since (StabilizerGroup f g x) <= g,

     CARD g.carrier
   = CARD (StabilizerGroup f g x).carrier * CARD (CosetPartition g (StabilizerGroup f g x))
   = CARD (stabilizer f g x) * CARD (orbit f g X x)  by ORBIT_STABILIZER_COSETS_BIJ
*)
val ORBIT_STABILIZER_THEOREM = store_thm(
  "ORBIT_STABILIZER_THEOREM",
  ``!f g X x. FiniteGroup g /\ action f g X /\ x IN X /\ FINITE X ==>
   (CARD g.carrier = CARD (orbit f g X x) * CARD (stabilizer f g x))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `StabilizerGroup f g x <= g` by METIS_TAC [StabilizerGroup_subgroup] THEN
  `(StabilizerGroup f g x).carrier = stabilizer f g x` by SRW_TAC [][StabilizerGroup_def] THEN
  `FINITE (CosetPartition g (StabilizerGroup f g x))` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  `FINITE (orbit f g X x)` by METIS_TAC [ORBIT_DESCRIPTION, IMAGE_DEF, IMAGE_FINITE] THEN
  `CARD (CosetPartition g (StabilizerGroup f g x)) = CARD (orbit f g X x)` by METIS_TAC [ORBIT_STABILIZER_COSETS_BIJ4, FINITE_BIJ_CARD_EQ] THEN
  `CARD g.carrier = CARD (stabilizer f g x) * CARD (CosetPartition g (StabilizerGroup f g x))` by METIS_TAC [FiniteGroup_def, LAGRANGE_IDENTITY] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
