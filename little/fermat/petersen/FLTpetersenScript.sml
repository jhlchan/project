(* !wait  1*second *)
(* !pause "Press CTRL-Z when HOL is ready." *)
(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem - Necklace proof of Julius Petersen.              *)
(* ------------------------------------------------------------------------- *)

(*

Fermat's Little Theorem (Combinatorial proof)
=============================================
Solomon W. Golomb (1956)
http://www.cimat.mx/~mmoreno/teaching/spring08/Fermats_Little_Thm.pdf

Original proof by Julius Petersen in 1872:

Take p elements from q with repetitions in all ways,
that is, in q^p ways. The q sets with elements all alike
are not changed by a cyclic permutation of the elements,
while the remaining q<sup>p</sup>-q sets are permuted
in sets of p. Hence p divides q^p - q.

This proof uses Group action of Zp on multicoloured necklaces.
The Necklace Theorem by Orbit-Stabilizer Theorem, then
Fermat's Little Theorem follows from Necklace Theorem.

*)
(* !pause *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "FLTpetersen";

(* ------------------------------------------------------------------------- *)


(* First, we load the built-in theories of HOL4 *)
(* val _ = load "dividesTheory"; *)
open pred_setTheory listTheory arithmeticTheory dividesTheory;

(* Next, we get the required thoeries developed for this proof *)
(* val _ = load "helperNumTheory"; *)
(* val _ = load "helperListTheory"; *)
(* val _ = load "helperSetTheory"; *)
(* val _ = load "cycleTheory"; *)
(* val _ = load "groupTheory"; *)
(* val _ = load "groupInstancesTheory"; *)
(* val _ = load "necklaceTheory"; *)
(* val _ = load "actionTheory"; *)
open helperNumTheory helperListTheory helperSetTheory cycleTheory;
open groupTheory groupInstancesTheory;
open necklaceTheory;
open actionTheory;

(* !pause *)

(* ------------------------------------------------------------------------- *)
(* Necklace Proof via Group action.                                          *)
(* ------------------------------------------------------------------------- *)


(* Theorem: [Necklace Theorem]
   If l is a Necklace of length prime p,
   then l is monocoloured iff CARD (orbit cycle l) = 1,
   and  l is multicoloured iff CARD (orbit cycle l) = p.
*)

(* ------------------------------------------------------------------------- *)
(* Combinatorial Proof via Group action.                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If l is a Necklace, cycle n l is also a Necklace. *)
(* Proof: by definition and cycle preserves length and set. *)
val NECKLACE_CYCLE = store_thm(
  "NECKLACE_CYCLE",
  ``!l. l IN Necklace n a ==> !k. (cycle k l) IN Necklace n a``,
  SRW_TAC [][Necklace_def, CYCLE_SAME_LENGTH, CYCLE_SAME_SET]);
(* !pause *)

(* ------------------------------------------------------------------------- *)
(* Apply Group action to Necklaces.                                          *)
(* G = Zp, X = Necklace n a. action = cycle.                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Cycle is a group action from Zn to Necklace n a. *)
(* Proof:
   Essentially this is to prove:
   (1) cycle x z IN Necklace n a         true by NECKLACE_CYCLE
   (2) cycle 0 z = z                     true by CYCLE_0
   (3) cycle x (cycle y z) = cycle ((x + y) MOD LENGTH z) z
                                         true by CYCLE_ADDITION.
*)
val CYCLE_ACTION = store_thm(
  "CYCLE_ACTION",
  ``!n a. 0 < n /\ 0 < a ==> action cycle (Z n) (Necklace n a)``,
  SRW_TAC [][action_def, Z_def, RES_FORALL_THM] THEN1
  METIS_TAC [NECKLACE_CYCLE] THEN1
  METIS_TAC [CYCLE_0] THEN
  METIS_TAC [CYCLE_ADDITION, NECKLACE_NONNIL, NECKLACE_PROPERTY]);
(* !pause *)

(* Theorem: For necklace n a, action orbit of necklace l = factor of n. *)
(* Proof: by ORBIT_STABILIZER_THEOREM. *)
val CARD_orbit_cycle_necklace = store_thm(
  "CARD_orbit_cycle_necklace",
  ``!n a. 0 < n /\ 0 < a ==> !l. l IN (Necklace n a) ==> divides (CARD (orbit cycle (Z n) (Necklace n a) l)) n``,
  REPEAT STRIP_TAC THEN
  `FiniteGroup (Z n)` by RW_TAC std_ss [ZN_finite_group] THEN
  `action cycle (Z n) (Necklace n a)` by RW_TAC std_ss [CYCLE_ACTION] THEN
  `FINITE (Necklace n a)` by RW_TAC std_ss [FINITE_Necklace] THEN
  `CARD (Z n).carrier = n` by RW_TAC std_ss [CARD_ZN_carrier] THEN
  `n = CARD (orbit cycle (Z n) (Necklace n a) l) *
       CARD (stabilizer cycle (Z n) l)` by RW_TAC std_ss [GSYM ORBIT_STABILIZER_THEOREM] THEN
  METIS_TAC [divides_def, MULT_COMM]);

(* Theorem: For necklace p a with prime p, action orbit of necklace l = 1 or p. *)
(* Proof: by ORBIT_STABILIZER_THEOREM. *)
val CARD_orbit_cycle_necklace_prime = store_thm(
  "CARD_orbit_cycle_necklace_prime",
  ``!p a. prime p /\ 0 < a /\ l IN (Necklace p a) ==>
   (CARD (orbit cycle (Z p) (Necklace p a) l) = 1) \/
   (CARD (orbit cycle (Z p) (Necklace p a) l) = p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by METIS_TAC [PRIME_POS] THEN
  `FiniteGroup (Z p)` by METIS_TAC [ZN_finite_group] THEN
  `action cycle (Z p) (Necklace p a)` by METIS_TAC [CYCLE_ACTION] THEN
  `FINITE (Necklace p a)` by METIS_TAC [FINITE_Necklace] THEN
  `CARD (Z p).carrier = p` by METIS_TAC [CARD_ZN_carrier] THEN
  `p = CARD (orbit cycle (Z p) (Necklace p a) l) *
        CARD (stabilizer cycle (Z p) l)` by METIS_TAC [ORBIT_STABILIZER_THEOREM] THEN
  METIS_TAC [FACTORS_OF_PRIME]);
(* !pause *)

(* Repeat these for Multicoloured Necklaces *)

(* Theorem: [Closure of cycle for multicoloured necklaces]
            If l is a multicoloured necklace, cycle n l is also multicolored. *)
(* Proof: by CYCLE_SAME_LENGTH and CYCLE_SAME_SET. *)
val multicoloured_CYCLE = store_thm(
  "multicoloured_CYCLE",
  ``!n a l. 0 < n /\ 0 < a /\ l IN multicoloured n a ==> !k. (cycle k l) IN multicoloured n a``,
  SRW_TAC [][Necklace_def, multicoloured_def, monocoloured_def, CYCLE_SAME_LENGTH, CYCLE_SAME_SET] THEN1
  METIS_TAC [CYCLE_NIL] THEN
  RW_TAC std_ss []);
(* !pause *)

(* Theorem: CYCLE is an action on multicoloured necklaces *)
(* Proof:
   Essentially this is to prove, for z IN multicoloured n a:
   (1) x < n ==> cycle x z IN multicoloured n a,    true by multicoloured_CYCLE
   (2) cycle 0 z = z,                               true by CYCLE_0
   (3) x < n /\ y < n ==> cycle x (cycle y z) = cycle ((x + y) MOD n) z
                                                    true by CYCLE_ADDITION
*)
val cycle_action_on_multicoloured = store_thm(
  "cycle_action_on_multicoloured",
  ``!n a. 0 < n /\ 0 < a ==> action cycle (Z n) (multicoloured n a)``,
  SRW_TAC [][action_def, Z_def, RES_FORALL_THM] THEN1
  METIS_TAC [multicoloured_CYCLE] THEN1
  METIS_TAC [CYCLE_0] THEN
  METIS_TAC [CYCLE_ADDITION, multicoloured_Necklace, NECKLACE_NONNIL, NECKLACE_PROPERTY]);
(* !pause *)

(* Theorem: !n a. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==> cycle 1 l <> l *)
(* Proof: by CYCLE_1_NONEMPTY_MONO. *)
val multicoloured_NOT_CYCLE_1 = store_thm(
  "multicoloured_NOT_CYCLE_1",
  ``!n a l. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==> cycle 1 l <> l``,
  SRW_TAC [][multicoloured_def, monocoloured_def] THEN
  METIS_TAC [CYCLE_1_NONEMPTY_MONO]);
(* !pause *)

(* Theorem: !n a l. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==>
     NOT SING (orbit cycle (Z n) (multicoloured n a) l) *)
(* Proof
   Case n = 1:
      orbit cycle (Z n) (multicoloured n a) l = EMPTY  by multicoloured_1_EMPTY
      and NOT SING EMPTY                               by NOT_SING_EMPTY
   Case n > 1:
      1 in (Z n).carrier                               by IN_COUNT
      l in (orbit cycle (Z n) (multicoloured n a) l)   by ACTION_ORBIT_HAS_ITSELF
      cycle 1 l in (orbit cycle (Z n) (multicoloured n a) l) by ORBIT_HAS_ACTION_ELEMENT
      but cycle 1 l <> l by multicoloured_NOT_CYCLE_1, hence NOT SING by IN_SING.
*)
val multicoloured_ORBIT_NOT_SING = store_thm(
  "multicoloured_ORBIT_NOT_SING",
  ``!n a l. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==> ~ SING (orbit cycle (Z n) (multicoloured n a) l)``,
  REPEAT STRIP_TAC THEN
  `Group (Z n)` by METIS_TAC [ZN_group] THEN
  `action cycle (Z n) (multicoloured n a)` by METIS_TAC [cycle_action_on_multicoloured] THEN
  `l IN (orbit cycle (Z n) (multicoloured n a) l)` by METIS_TAC [ACTION_ORBIT_HAS_ITSELF] THEN
  `(orbit cycle (Z n) (multicoloured n a) l) = {l}` by METIS_TAC [SING_DEF, IN_SING] THEN
  Cases_on `n = 1` THEN1
  METIS_TAC [multicoloured_1_EMPTY, NOT_IN_EMPTY, NOT_SING_EMPTY] THEN
  `1 IN (Z n).carrier` by RW_TAC arith_ss [Z_def, IN_COUNT] THEN
  METIS_TAC [ORBIT_HAS_ACTION_ELEMENT, multicoloured_NOT_CYCLE_1, IN_SING]);
(* !pause *)

(* Theorem: CARD (orbit cycle (Z n) (multicoloured n a) l) <> 1 *)
(* Proof: by multicoloured_ORBIT_NOT_SING *)
val CARD_multicoloured_ORBIT_NOT_1 = store_thm(
  "CARD_multicoloured_ORBIT_NOT_1",
  ``!n a l. 0 < n /\ 0 < a /\ l IN (multicoloured n a) ==> CARD (orbit cycle (Z n) (multicoloured n a) l) <> 1``,
  METIS_TAC [cycle_action_on_multicoloured, multicoloured_ORBIT_NOT_SING, SING_IFF_CARD1,
             FINITE_multicoloured, ORBIT_SUBSET_TARGET, SUBSET_FINITE]);
(* !pause *)

(* Theorem: Orbits of cycle action on multicoloured necklaces are of length p, for prime p *)
(* Proof:
   action cycle (Z p) (multicoloured p a)     by cycle_action_on_multicoloured
   p = CARD (orbit) CARD (stabilizer)         by ORBIT_STABILIZER_THM
   CARD (orbit) <> 1                          by CARD_multicoloured_ORBIT_NOT_1
   hence CARD (orbit) = p                     by FACTORS_OF_PRIME
*)
val CARD_multicoloured_PRIME_ORBIT = store_thm(
  "CARD_multicoloured_PRIME_ORBIT",
  ``!p a l. prime p /\ 0 < a /\ l IN (multicoloured p a) ==> (CARD (orbit cycle (Z p) (multicoloured p a) l) = p)``,
  METIS_TAC [PRIME_POS, ZN_finite_group, FINITE_multicoloured,
   cycle_action_on_multicoloured, ORBIT_STABILIZER_THEOREM, CARD_ZN_carrier,
   CARD_multicoloured_ORBIT_NOT_1, FACTORS_OF_PRIME]);
(* !pause *)

(* Theorem: [Fermat's Little Theorem]
            !p a. prime p ==> divides p (a**p - a)     *)
(* Proof (J. Petersen in 1872):
   Take p elements from a with repetitions in all ways, that is, in a^p ways.
                   by CARD_Necklace
   The a sets with elements all alike are not changed by a cyclic permutation of the elements,
                   by CARD_monocoloured
   while the remaining (a^p - a) sets are
                   by CARD_multicoloured
   permuted in sets of p.
                   by cycle_action_on_multicoloured, CARD_multicoloured_PRIME_ORBIT
   Hence p divides a^p - a.
                   by EQUAL_SIZE_ORBITS_PROPERTY
*)
val FERMAT_LITTLE_Petersen = store_thm(
  "FERMAT_LITTLE_Petersen",
  ``!p a. prime p ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by METIS_TAC [PRIME_POS] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [SUB_0, EXP_EQ_0, ALL_DIVIDES_0] THEN
  METIS_TAC [CARD_multicoloured, cycle_action_on_multicoloured, NOT_ZERO_LT_ZERO,
   CARD_multicoloured_PRIME_ORBIT, EQUAL_SIZE_ORBITS_PROPERTY, ZN_group, FINITE_multicoloured]);
(* !pause *)

(* Theorem: [Fermat's Little Theorem] -- line by line
            !p a. prime p ==> divides p (a**p - a)     *)
(* Proof (J. Petersen in 1872):
   Take p elements from a with repetitions in all ways, that is, in a^p ways.
                   by CARD_Necklace
   The a sets with elements all alike are not changed by a cyclic permutation of the elements,
                   by CARD_monocoloured
   while the remaining (a^p - a) sets are
                   by CARD_multicoloured
   permuted in sets of p.
                   by cycle_action_on_multicoloured, CARD_multicoloured_PRIME_ORBIT
   Hence p divides a^p - a.
                   by EQUAL_SIZE_ORBITS_PROPERTY
*)
val FERMAT_LITTLE_THM = store_thm(
  "FERMAT_LITTLE_THM",
  ``!p a. prime p ==> divides p (a**p - a)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by RW_TAC std_ss [PRIME_POS] THEN
  Cases_on `a = 0` THENL [
    METIS_TAC [SUB_0, EXP_EQ_0, ALL_DIVIDES_0],
    `0 < a` by DECIDE_TAC THEN
    `CARD (multicoloured p a) = a**p - a` by RW_TAC std_ss [CARD_multicoloured] THEN
    `Group (Z p)` by RW_TAC std_ss [ZN_group] THEN
    `action cycle (Z p) (multicoloured p a)` by RW_TAC std_ss [cycle_action_on_multicoloured] THEN
    `FINITE (multicoloured p a)` by RW_TAC std_ss [FINITE_multicoloured] THEN
    `!l. l IN (multicoloured p a) ==> (CARD (orbit cycle (Z p) (multicoloured p a) l) = p)`
   by RW_TAC std_ss [CARD_multicoloured_PRIME_ORBIT] THEN
    METIS_TAC [EQUAL_SIZE_ORBITS_PROPERTY]
  ]);

(* Theorem: Orbits of cycle action on multicoloured necklaces of length p**n have sizes divisible by p, for prime p *)
(* Note: 0 < n because, if n = 0, p**n = 1, multicoloured_1_EMPTY,
         but orbit cannot be EMPTY -- think Orbit-Stabilizer Theorem. *)
(* Proof: by ORBIT_STABILIZER_THEOREM and PRIME_EXP_FACTOR. *)
val CARD_multicoloured_PRIME_EXP_ORBIT = store_thm(
  "CARD_multicoloured_PRIME_EXP_ORBIT",
  ``!p n a. prime p /\ 0 < n /\ 0 < a /\ l IN (multicoloured (p**n) a) ==>
    divides p (CARD (orbit cycle (Z (p**n)) (multicoloured (p**n) a) l))``,
  REPEAT STRIP_TAC THEN
  Q.ABBREV_TAC `m = p**n` THEN
  `0 < m` by METIS_TAC [ZERO_LT_EXP, PRIME_POS] THEN
  `1 < m` by METIS_TAC [EXP_EXP_LT_MONO, EXP_1, ONE_LT_PRIME] THEN
  `FiniteGroup (Z m)` by SRW_TAC [][ZN_finite_group] THEN
  `FINITE (multicoloured m a)` by SRW_TAC [][FINITE_multicoloured] THEN
  `action cycle (Z m) (multicoloured m a)` by METIS_TAC [cycle_action_on_multicoloured] THEN
  `m = CARD (orbit cycle (Z m) (multicoloured m a) l) * CARD (stabilizer cycle (Z m) l)` by METIS_TAC [ORBIT_STABILIZER_THEOREM, CARD_ZN_carrier] THEN
  `CARD (orbit cycle (Z m) (multicoloured m a) l) <> 1` by METIS_TAC [CARD_multicoloured_ORBIT_NOT_1] THEN
  METIS_TAC [PRIME_EXP_FACTOR, divides_def, MULT_SYM]);
(* !pause *)

(* Theorem: Power version of Fermat's Little Theorem
            !p n a. prime p ==> (a**(p**n) MOD p = a MOD p)   *)
(* Proof:
   When base a = 0, all exponents 0^n = 0 by EXP_EQ_0, this is trivial: 0 = 0.
   When base a > 0,
   When exponent n = 0, p^0 = 1, a^1 = a, this is trivial: a = a.
   When exponent n > 0, we still have (let m = p**n):

   CARD (multicoloured m a) = a**m - a      by CARD_multicoloured
   action cycle (Z m) (multicoloured m a)   by cycle_action_on_multicoloured
   !l. l IN (multicoloured m a) ==>
            divides p (CARD (orbit cycle (Z m) (multicoloured m a) l))
                                            by CARD_multicoloured_PRIME_EXP_ORBIT
   hence: divides p (a**m - a)              by FACTOR_ORBITS_PROPERTY
   and the result follows.
*)
val FERMAT_LITTLE_EXP_Petersen = store_thm(
  "FERMAT_LITTLE_EXP_Petersen",
  ``!p n a. prime p ==> (a**(p**n) MOD p = a MOD p)``,
  REPEAT STRIP_TAC THEN
  Q.ABBREV_TAC `m = p**n` THEN
  `0 < m` by METIS_TAC [PRIME_POS, ZERO_LT_EXP] THEN
  Cases_on `a = 0` THEN1
  METIS_TAC [EXP_EQ_0, ZERO_MOD] THEN
  `0 < a` by RW_TAC arith_ss [] THEN
  Cases_on `n = 0` THEN1
  SRW_TAC [][EXP, Abbr`m`] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  `CARD (multicoloured m a) = a**m - a` by METIS_TAC [CARD_multicoloured] THEN
  `action cycle (Z m) (multicoloured m a)` by METIS_TAC [cycle_action_on_multicoloured] THEN
  `!l. l IN (multicoloured m a) ==> divides p (CARD (orbit cycle (Z m) (multicoloured m a) l))`
    by METIS_TAC [CARD_multicoloured_PRIME_EXP_ORBIT] THEN
  `divides p (a**m - a)` by METIS_TAC [FACTOR_ORBITS_PROPERTY, ZN_group, FINITE_multicoloured] THEN
  `0 < p` by METIS_TAC [PRIME_POS] THEN
  `(a**m - a) MOD p = 0` by METIS_TAC [divides_def, MOD_EQ_0] THEN
  `1 < m` by METIS_TAC [ONE_LT_EXP, ONE_LT_PRIME] THEN
  METIS_TAC [MOD_EQ, EXP_LESS_EQ]);
(* !pause *)

(* ------------------------------------------------------------------------- *)
(* Classification of Monocoloured necklace orbits.                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: A monocoloured necklace l has cycle 1 l = l *)
(* Proof: by SING_HAS_CYCLE_1. *)
val monocoloured_cycle1 = store_thm(
  "monocoloured_cycle1",
  ``!n a l. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> (cycle 1 l = l)``,
  SRW_TAC [][monocoloured_def] THEN
  METIS_TAC [NECKLACE_NONNIL, SING_HAS_CYCLE_1]);
(* !pause *)

(* Theorem: s DIFF (s DIFF t) = t *)
val DIFF_DIFF_SUBSET = store_thm(
  "DIFF_DIFF_SUBSET",
  ``!s t. t SUBSET s ==> (s DIFF (s DIFF t) = t)``,
  REPEAT STRIP_TAC THEN
  `!x. x IN t ==> x IN s DIFF (s DIFF t)` by METIS_TAC [SUBSET_DEF, IN_DIFF] THEN
  `!x. x IN s DIFF (s DIFF t) ==> x IN t` by METIS_TAC [SUBSET_DEF, IN_DIFF] THEN
  METIS_TAC [SUBSET_DEF, SET_EQ_SUBSET]);

(* Theorem: If a necklace l has cycle 1 l = l , it must be monocoloured *)
(* Proof: by multicoloured_NOT_CYCLE_1. *)
val cycle1_monocoloured = store_thm(
  "cycle1_monocoloured",
  ``!n a l. 0 < n /\ 0 < a /\ l IN Necklace n a /\ (cycle 1 l = l) ==> l IN monocoloured n a``,
  REPEAT STRIP_TAC THEN
  `~ (l IN (multicoloured n a))` by METIS_TAC [multicoloured_NOT_CYCLE_1] THEN
  `l IN (Necklace n a DIFF multicoloured n a)` by METIS_TAC [IN_DIFF] THEN
  `(monocoloured n a) SUBSET (Necklace n a)` by METIS_TAC [monocoloured_Necklace, SUBSET_DEF] THEN
  METIS_TAC [multicoloured_def, DIFF_DIFF_SUBSET]);

(* original *)
(* Theorem: If a necklace l has cycle 1 l = l , it must be monocoloured *)
(* Proof: by CYCLE_1_EQ, CYCLE_1_NEQ. *)
val cycle1_monocoloured = prove(
  ``!n a l. 0 < a /\ l IN Necklace n a /\ (cycle 1 l = l) ==> l IN monocoloured n a``,
  SRW_TAC [][monocoloured_def, Necklace_def] THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);
(* !pause *)

(* Theorem: A Necklace l is monocoloured iff cycle 1 l = 1 *)
(* Proof: by cycle1_monocoloured and monocoloured_cycle1 *)
val monocoloured_iff_cycle1 = store_thm(
  "monocoloured_iff_cycle1",
  ``!n a l. 0 < n /\ 0 < a /\ l IN Necklace n a ==> (l IN monocoloured n a <=> (cycle 1 l = l))``,
  METIS_TAC [cycle1_monocoloured, monocoloured_cycle1]);
(* !pause *)

(* Theorem: [Closure of cycle for monocoloured necklaces]
            If l is a monocoloured necklace, cycle n l is also monocoloured. *)
(* Proof: by NECKLACE_CYCLE, CYCLE_SAME_SET. *)
val monocoloured_CYCLE = store_thm(
  "monocoloured_CYCLE",
  ``!l n a. 0 < n /\ 0 < a /\ l IN monocoloured n a ==> !k. (cycle k l) IN monocoloured n a``,
  REPEAT STRIP_TAC THEN
  `l IN (Necklace n a)` by SRW_TAC [][monocoloured_Necklace] THEN
  `l <> []` by METIS_TAC [NECKLACE_NONNIL] THEN
  FULL_SIMP_TAC (srw_ss()) [monocoloured_def, NECKLACE_CYCLE, CYCLE_SAME_SET]);
(* !pause *)

(* Theorem: CYCLE is an action on monocoloured necklaces *)
(* Proof:
   Essentially this is to prove, for z IN monocoloured n a:
   (1) x < n ==> cycle x z IN multicoloured n a,    true by monocoloured_CYCLE
   (2) cycle 0 z = z,                               true by CYCLE_0
   (3) x < n /\ y < n ==> cycle x (cycle y z) = cycle ((x + y) MOD n) z
                                                    try by CYCLE_ADDITION
*)
val cycle_action_on_monocoloured = store_thm(
  "cycle_action_on_monocoloured",
  ``!n a. 0 < n /\ 0 < a ==> action cycle (Z n) (monocoloured n a)``,
  SRW_TAC [][action_def, Z_def, RES_FORALL_THM] THEN1
  METIS_TAC [monocoloured_CYCLE] THEN1
  METIS_TAC [CYCLE_0] THEN
  METIS_TAC [CYCLE_ADDITION, monocoloured_Necklace, NECKLACE_NONNIL, NECKLACE_PROPERTY]);
(* !pause *)

(* Theorem: Monocoloured necklace orbits are singletons. *)
(* Proof:
   By monocoloured_cycle1, cycle 1 l = l   for l IN (monocoloured n a).
   By CYCLE_ADD, cycle k l = l for all k.
   Hence the monocoloured necklace orbit collapses to a singleton.
*)
val monocoloured_ORBIT_SING = store_thm(
  "monocoloured_ORBIT_SING",
  ``!l n a. 0 < n /\ 0 < a /\ l IN (monocoloured n a) ==> SING (orbit cycle (Z n) (monocoloured n a) l)``,
  REPEAT STRIP_TAC THEN
  `action cycle (Z n) (monocoloured n a)` by METIS_TAC [cycle_action_on_monocoloured] THEN
  `l IN (orbit cycle (Z n) (monocoloured n a) l)` by METIS_TAC [ACTION_ORBIT_HAS_ITSELF, ZN_group] THEN
  `!k. (cycle k l) = l` by Induct_on `k` THEN1
  METIS_TAC [CYCLE_0] THEN1
  METIS_TAC [monocoloured_cycle1, SUC_ONE_ADD, CYCLE_ADD] THEN
  `!h. h IN (orbit cycle (Z n) (monocoloured n a) l) ==> (h = l)` by SRW_TAC [][orbit_def, reach_def] THEN
  METIS_TAC [ONE_ELEMENT_SING, MEMBER_NOT_EMPTY, SING_DEF]);
(* !pause *)

(* Theorem: CARD (monocoloured necklace orbit) = 1. *)
(* Proof: by monocoloured_ORBIT_SING and CARD_SING. *)
val CARD_monocoloured_ORBIT_1 = store_thm(
  "CARD_monocoloured_ORBIT_1",
  ``!l n a. 0 < n /\ 0 < a /\ l IN (monocoloured n a) ==>
    (CARD (orbit cycle (Z n) (monocoloured n a) l) = 1)``,
  METIS_TAC [monocoloured_ORBIT_SING, SING_DEF, CARD_SING]);
(* !pause *)

(* Theorem: For necklace p a with prime p, action orbit of necklace l = 1 or p. *)
(* Proof: by ORBIT_STABILIZER_THEOREM. *)
val CARD_orbit_cycle_necklace_prime = store_thm(
  "CARD_orbit_cycle_necklace_prime",
  ``!p a. prime p /\ 0 < a /\ l IN (Necklace p a) ==>
   (CARD (orbit cycle (Z p) (Necklace p a) l) = 1) \/
   (CARD (orbit cycle (Z p) (Necklace p a) l) = p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by METIS_TAC [PRIME_POS] THEN
  `FiniteGroup (Z p)` by METIS_TAC [ZN_finite_group] THEN
  `action cycle (Z p) (Necklace p a)` by METIS_TAC [CYCLE_ACTION] THEN
  `FINITE (Necklace p a)` by METIS_TAC [FINITE_Necklace] THEN
  `CARD (Z p).carrier = p` by METIS_TAC [CARD_ZN_carrier] THEN
  `p = CARD (orbit cycle (Z p) (Necklace p a) l) *
        CARD (stabilizer cycle (Z p) l)` by METIS_TAC [ORBIT_STABILIZER_THEOREM] THEN
  METIS_TAC [FACTORS_OF_PRIME]);

(* Theorem: l IN (monocoloured n a) <=> CARD (orbit cycle (Z n) (Necklace n a) l) = 1 *)
(* Proof:
   If part: l IN (monocoloured n a) ==> CARD (orbit cycle (Z n) (Necklace n a) l) = 1
   By monocoloured_cycle1, cycle 1 l = l   for l IN (monocoloured n a).
   By CYCLE_ADD, cycle k l = l for all k.
   Hence the monocoloured necklace orbit collapses to a singleton.

   Only-if part: CARD (orbit cycle (Z n) (Necklace n a) l) = 1 ==> l IN (monocoloured n a)
   By contradiction.
   Suppose ~(l IN (monocoloured n a)), then
   l IN (multicoloured n a)    by monocoloured_def, IN_DIFF.
   then n <> 1                 by necklace_1_monocoloured
   hence 1 < n, and 1 IN (Z n).carrier  by ZN_PROPERTY.
   Thus cycle 1 l IN IMAGE (\k. cycle k l) (Z n).carrier    by IN_IMAGE
   and          l IN (orbit cycle (Z n) (Necklace n a) l)   by ACTION_ORBIT_HAS_ITSELF
   But (orbit cycle (Z n) (Necklace n a) l)
     = IMAGE (\k. cycle k l) (Z n).carrier                  by ORBIT_AS_IMAGE
   and CARD (orbit cycle (Z n) (Necklace n a) l) = 1)
   gives cycle 1 l = l, contradicting monocoloured_iff_cycle1.
*)
val monocoloured_necklace_theorem = store_thm(
  "monocoloured_necklace_theorem",
  ``!l n a. 0 < n /\ 0 < a /\ l IN (Necklace n a) ==>
     (l IN (monocoloured n a) <=> (CARD (orbit cycle (Z n) (Necklace n a) l) = 1))``,
  REPEAT STRIP_TAC THEN
  `Group (Z n) /\ FINITE (Z n).carrier` by METIS_TAC [ZN_finite_group, FiniteGroup_def] THEN
  `action cycle (Z n) (Necklace n a)` by RW_TAC std_ss [CYCLE_ACTION] THEN
  `l IN (Necklace n a)` by RW_TAC std_ss [monocoloured_Necklace] THEN
  `l IN (orbit cycle (Z n) (Necklace n a) l)` by RW_TAC std_ss [ACTION_ORBIT_HAS_ITSELF] THEN
  RW_TAC std_ss [EQ_IMP_THM] THENL [
    `!k. (cycle k l) = l` by Induct_on `k` THEN1
    RW_TAC std_ss [CYCLE_0] THEN1
    METIS_TAC [monocoloured_cycle1, ADD1, CYCLE_ADD] THEN
    `!h. h IN (orbit cycle (Z n) (Necklace n a) l) ==> (h = l)` by RW_TAC std_ss [IN_IMAGE, ORBIT_AS_IMAGE] THEN
    METIS_TAC [ONE_ELEMENT_SING, MEMBER_NOT_EMPTY, CARD_SING],
    (SPOSE_NOT_THEN STRIP_ASSUME_TAC) THEN
    `n <> 1` by METIS_TAC [necklace_1_monocoloured] THEN
    `1 < n` by DECIDE_TAC THEN
    `1 IN (Z n).carrier` by RW_TAC std_ss [ZN_PROPERTY] THEN
    `l IN (orbit cycle (Z n) (Necklace n a) l)` by RW_TAC std_ss [ACTION_ORBIT_HAS_ITSELF] THEN
    `cycle 1 l IN IMAGE (\k. cycle k l) (Z n).carrier` by RW_TAC std_ss [IN_IMAGE] THEN1
    METIS_TAC [] THEN
    `IMAGE (\k. cycle k l) (Z n).carrier = (orbit cycle (Z n) (Necklace n a) l)` by RW_TAC std_ss [ORBIT_AS_IMAGE] THEN
    `SING (IMAGE (\k. cycle k l) (Z n).carrier)` by METIS_TAC [SING_IFF_CARD1, ORBIT_AS_IMAGE, IMAGE_FINITE] THEN
    `cycle 1 l = l` by METIS_TAC [SING_DEF, IN_SING] THEN
    METIS_TAC [monocoloured_iff_cycle1]
  ]);

(* Theorem: [Necklace Theorem]
   If l is a Necklace of length prime p,
   then l is monocoloured iff CARD (orbit cycle l) = 1,
   and  l is multicoloured iff CARD (orbit cycle l) = p.
*)
(* Proof: by CARD_orbit_cycle_necklace_prime, monocoloured_necklace_theorem. *)
val necklace_theorem = store_thm(
  "necklace_theorem",
  ``!p a. prime p /\ 0 < a /\ l IN (Necklace p a) ==>
   (l IN (monocoloured p a) <=> (CARD (orbit cycle (Z p) (Necklace p a) l) = 1)) /\
   (l IN (multicoloured p a) <=> (CARD (orbit cycle (Z p) (Necklace p a) l) = p))``,
  REPEAT STRIP_TAC THEN1
  RW_TAC std_ss [monocoloured_necklace_theorem, PRIME_POS] THEN
  `0 < p /\ 1 < p` by RW_TAC std_ss [PRIME_POS, ONE_LT_PRIME] THEN
  RW_TAC std_ss [EQ_IMP_THM] THEN1
  METIS_TAC [CARD_orbit_cycle_necklace_prime, monocoloured_necklace_theorem, multicoloured_def, IN_DIFF] THEN
  `CARD (orbit cycle (Z p) (Necklace p a) l) <> 1` by DECIDE_TAC THEN
  RW_TAC std_ss [monocoloured_necklace_theorem, multicoloured_def, IN_DIFF]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
