(* ------------------------------------------------------------------------- *)
(* Applying Group Theory: Group Instances                                    *)
(* ------------------------------------------------------------------------- *)

(*

Group Instances
===============
The most important ones:

 Zn -- Addition Modulo n, n > 0.
Z*p -- Multiplication Modulo p, p a prime.
E*n -- Multiplication Modulo n, of order phi(n).

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "groupInstances";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
(* val _ = load "dividesTheory"; *)
(* val _ = load "gcdTheory"; *)
open pred_setTheory arithmeticTheory dividesTheory gcdTheory;

(* Get dependent theories in lib *)
open helperListTheory helperNumTheory helperSetTheory
(* Get dependent theories local *)
open groupTheory subgroupTheory;

(* ------------------------------------------------------------------------- *)
(* The Group Zn = Addition Modulo n, for n > 0.                              *)
(* ------------------------------------------------------------------------- *)

(* Define Zn = Addition Modulo n Group *)
val Z_def = Define`
  Z n =
    <| carrier := count n;
            id := 0;
           inv := (\i. (n - i) MOD n);  (* so that inv 0 = 0 *)
          mult := (\i j. (i + j) MOD n)
     |>`;

(* Theorem: Zn carrier is finite *)
val FINITE_ZN_carrier = store_thm(
  "FINITE_ZN_carrier",
  ``!n. FINITE (Z n).carrier``,
  SRW_TAC [][Z_def, FINITE_COUNT]);

(* Theorem: CARD (Zn carrier) = n *)
val CARD_ZN_carrier = store_thm(
  "CARD_ZN_carrier",
  ``!n. CARD (Z n).carrier = n``,
  SRW_TAC [][Z_def, CARD_COUNT]);

(* Theorem: Zn is a group if n > 0. *)
val ZN_group = store_thm(
  "ZN_group",
  ``!n. 0 < n ==> Group (Z n)``,
  SRW_TAC [][Z_def, Group_def, RES_FORALL_THM, MOD_ADD_INV, MOD_ADD_ASSOC]);

(* Theorem: Zn is a FiniteGroup if n > 0. *)
val ZN_finite_group = store_thm(
  "ZN_finite_group",
  ``!n. 0 < n ==> FiniteGroup (Z n)``,
  METIS_TAC [ZN_group, FINITE_ZN_carrier, FiniteGroup_def]);

(* Theorem: Zn is a finite Abelian group if n > 0. *)
val ZN_finite_abelian_group = store_thm(
  "ZN_finite_abelian_group",
  ``!n. 0 < n ==> FiniteAbelianGroup (Z n)``,
  SRW_TAC [ARITH_ss][ZN_group, FiniteAbelianGroup_def, FiniteGroup_def, AbelianGroup_def,
                     Z_def, RES_FORALL_THM]);

(* Theorem: (Z n).id = 0 *)
val ZN_ID = store_thm(
  "ZN_ID",
  ``!n. (Z n).id = 0``,
  SRW_TAC [][Z_def]);

(* Theorem: m IN Zn <=> m < n *)
val ZN_PROPERTY = store_thm(
  "ZN_PROPERTY",
  ``!m n. m IN (Z n).carrier <=> m < n``,
  SRW_TAC [][Z_def]);

(* ------------------------------------------------------------------------- *)
(* The Group Z*p = Multiplication Modulo p, for prime p.                     *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Establish the existence of multiplicative inverse when p is prime.        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For prime p, 0 < x < p ==> ?y. 0 < y /\ y < p /\ y*x MOD p = 1 *)
(* Proof:
       0 < x < p
   ==> ~ divides p x                    by NOT_LT_DIVIDES
   ==> gcd p x = 1                      by PRIME_GCD
   ==> ?k q. k * x = q * p + 1          by LINEAR_GCD
   ==> k*x MOD p = (q*p + 1) MOD p      by arithmetic
   ==> k*x MOD p = 1                    by MOD_MULT, 1 < p.
   ==> (k MOD p)*(x MOD p) MOD p = 1    by MOD_TIMES2
   ==> ((k MOD p) * x) MOD p = 1        by LESS_MOD, x < p.
   Now   k MOD p < p                    by MOD_LESS
   and   0 < k MOD p since (k*x) MOD p <> 0  (by 1 <> 0)
                       and x MOD p <> 0      (by ~ divides p x)
                                        by EUCLID_LEMMA
   Hence take y = k MOD p, then 0 < y < p.
*)
val MOD_MULT_INV = store_thm(
  "MOD_MULT_INV",
  ``!p x. prime p /\ 0 < x /\ x < p ==> ?y. 0 < y /\ y < p /\ ((y*x) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][PRIME_POS] THEN
  `~ divides p x` by SRW_TAC [][NOT_LT_DIVIDES] THEN
  `gcd p x = 1` by METIS_TAC [PRIME_GCD] THEN
  `?k q. k * x = q * p + 1` by METIS_TAC [LINEAR_GCD, NOT_ZERO_LT_ZERO] THEN
  `(k*x) MOD p = (q*p + 1) MOD p` by SRW_TAC [][] THEN
  `_ = 1` by METIS_TAC [MOD_MULT, ONE_LT_PRIME] THEN
  `1 <> 0` by RW_TAC arith_ss [] THEN
  `0 <> x MOD p` by METIS_TAC [MOD_0_DIVIDES] THEN (* by ~ divides p x and MOD_0_DIVIDES *)
  `0 < k MOD p` by METIS_TAC [EUCLID_LEMMA, NOT_ZERO_LT_ZERO] THEN
  `(x = x MOD p) /\ k MOD p < p` by SRW_TAC [][] THEN
  METIS_TAC [MOD_TIMES2]);

val lemma = prove(
  ``!p x. ?y. prime p /\ 0 < x /\ x < p ==>
              0 < y /\ y < p /\ ((y*x) MOD p = 1)``,
  METIS_TAC [MOD_MULT_INV]);

(*
- SKOLEM_THM;
> val it = |- !P. (!x. ?y. P x y) <=> ?f. !x. P x (f x) : thm
*)
(* val lemma' = lemma |> SIMP_RULE (srw_ss()) [SKOLEM_THM] *)
(* val lemma' = SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma; *)
(*
> val lemma' =
    |- ?f.
         !p x.
           prime p /\ 0 < x /\ x < p ==>
           0 < f p x /\ f p x < p /\ ((f p x * x) MOD p = 1) : thm
*)

(* val MUL_INV_DEF = new_specification("MUL_INV_DEF", ["MOD_MUL_INV"], lemma') *)
(*
> val MUL_INV_DEF =
    |- !p x.
         prime p /\ 0 < x /\ x < p ==>
         0 < MOD_MUL_INV p x /\ MOD_MUL_INV p x < p /\
         ((MOD_MUL_INV p x * x) MOD p = 1) : thm
*)
val MUL_INV_DEF = new_specification(
  "MUL_INV_DEF",
  ["MOD_MUL_INV"],
  SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* ------------------------------------------------------------------------- *)
(* The Group of Multiplication modulo p                                      *)
(* ------------------------------------------------------------------------- *)

(* Define Multiplicative Modulo p Group *)
val Zstar_def = Define`
  Zstar p =
   <| carrier := residue p;
           id := 1;
          inv := MOD_MUL_INV p;
         mult := (\i j. (i * j) MOD p)
    |>`;

val Zstar_group = store_thm(
  "Zstar_group",
  ``!p. prime p ==> Group (Zstar p)``,
  SRW_TAC [][Zstar_def, residue_def, Group_def, RES_FORALL_THM,
             MUL_INV_DEF]
  THENL [ (* 4 subgoals *)
    SRW_TAC [][ONE_LT_PRIME],
    `(x*y) MOD p <> 0` by SRW_TAC [][EUCLID_LEMMA] THEN RW_TAC arith_ss [],
    RW_TAC arith_ss [],
    METIS_TAC [PRIME_POS, MOD_MULT_ASSOC]
  ] );

(* Theorem: FINITE (Zstar p).carrier *)
val FINITE_Zstar_carrier = store_thm(
  "FINITE_Zstar_carrier",
  ``!p. FINITE (Zstar p).carrier``,
  SRW_TAC [][Zstar_def] THEN
  `residue p SUBSET count p` by SRW_TAC [][residue_def, pred_setTheory.SUBSET_DEF] THEN
  METIS_TAC [pred_setTheory.FINITE_COUNT, pred_setTheory.SUBSET_FINITE]);

(* Theorem: p > 0 ==> CARD (Zstar p).carrier = p-1 *)
val CARD_Zstar_carrier = store_thm(
  "CARD_Zstar_carrier",
  ``!p. 0 < p ==> (CARD (Zstar p).carrier = p-1)``,
  SRW_TAC [][Zstar_def, CARD_RESIDUE]);

(* Theorem: If p is prime, Z*p is a finite Abelian group. *)
(* Proof:
   Verify all finite Abelian group axioms for Z*p.
*)
val Zstar_finite_abelian_group = store_thm(
  "Zstar_finite_abelian_group",
  ``!p. prime p ==> FiniteAbelianGroup (Zstar p)``,
  SRW_TAC [][Zstar_group, FiniteAbelianGroup_def,
             FiniteGroup_def, AbelianGroup_def, RES_FORALL_THM] THEN1
  METIS_TAC [FINITE_Zstar_carrier] THEN
  RW_TAC arith_ss [Zstar_def]);

(* Theorem: group_exp (Zstar p) a n = a**n MOD p *)
(* Proof:
   By induction on n.
   Base case: group_exp (Zstar p) a 0 = (Zstar p).id = 1, and a**0 MOD p = 1 MOD p = 1.
   Step case: group_exp (Zstar p) a (SUC n)
            = a * (group_exp (Zstar p) a n)  by group_exp_def
            = a * ((a ** n) MOD p)           by inductive hypothesis
            = (a MOD p) * ((a**n) MOD p)     by a < p, MOD_LESS
            = (a*(a**n)) MOD p               by MOD_TIMES2
            = (a**(SUC n) MOD p              by EXP
*)
val Zstar_exp = store_thm(
  "Zstar_exp",
  ``!p a. prime p /\ a IN (Zstar p).carrier ==> !n. group_exp (Zstar p) a n = (a**n) MOD p``,
  REPEAT STRIP_TAC THEN
  `Group (Zstar p)` by SRW_TAC [][Zstar_group] THEN
  `0 < p` by SRW_TAC [][PRIME_POS] THEN
  Induct_on `n` THEN1 (
    `(Zstar p).id = 1` by SRW_TAC [][Zstar_def] THEN
    `1 < p /\ (1 MOD p = 1)` by RW_TAC arith_ss [ONE_LT_PRIME] THEN
    METIS_TAC [group_exp_def, EXP]
  ) THEN
  SRW_TAC [][group_exp_def, EXP] THEN
  FULL_SIMP_TAC (srw_ss())[Zstar_def, residue_def] THEN
  `a MOD p = a` by RW_TAC arith_ss [] THEN
  METIS_TAC [MOD_TIMES2]);

(* ------------------------------------------------------------------------- *)
(* Euler's generalization of Modulo Multiplicative Group for any modulo n.   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For n > 1, (gcd n x = 1) /\ 0 < x < n ==>
            ?y. 0 < y /\ y < n /\ gcd n y = 1 /\ y*x MOD n = 1 *)
(* Proof:
       gcd n x = 1
   ==> ?k. (k*x) MOD n = 1 /\ gcd n k = 1     by GCD_ONE_PROPERTY
       (k*x) MOD n = 1
   ==> (k MOD n * x MOD n) MOD n = 1    by MOD_TIMES2
   ==> ((k MOD n) * x) MOD n = 1        by LESS_MOD, x < n.

   Now   k MOD n < n                    by MOD_LESS
   and   0 < k MOD n                    by MOD_MULITPLE_ZERO and 1 <> 0.

   Hence take y = k MOD n, then 0 < y < n.
   and gcd n k = 1 ==> gcd n (k MOD n) = 1    by MOD_WITH_GCD_ONE.
*)
val GCD_MOD_MULT_INV = store_thm(
  "GCD_MOD_MULT_INV",
  ``!n x. 1 < n /\ (gcd n x = 1) /\ 0 < x /\ x < n ==>
   ?y. 0 < y /\ y < n /\ (gcd n y = 1) /\ ((y*x) MOD n = 1)``,
  REPEAT STRIP_TAC THEN
  `?k. ((k*x) MOD n = 1) /\ (gcd n k = 1)` by METIS_TAC [GCD_ONE_PROPERTY] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  `(k MOD n * x MOD n) MOD n = 1` by METIS_TAC [MOD_TIMES2] THEN
  `((k MOD n) * x) MOD n = 1` by METIS_TAC [LESS_MOD] THEN
  `k MOD n < n` by METIS_TAC [MOD_LESS] THEN
  `1 <> 0` by RW_TAC arith_ss [] THEN
  `0 <> k MOD n` by METIS_TAC [MOD_MULITPLE_ZERO] THEN
  `0 < k MOD n` by RW_TAC arith_ss [] THEN
  METIS_TAC [MOD_WITH_GCD_ONE]);


(* Convert this into an existence definition *)
val lemma = prove(
  ``!n x. ?y. 1 < n /\ (gcd n x = 1) /\ 0 < x /\ x < n ==>
              0 < y /\ y < n /\ (gcd n y = 1) /\ ((y*x) MOD n = 1)``,
  METIS_TAC [GCD_MOD_MULT_INV]);

val GEN_MULT_INV_DEF = new_specification(
  "GEN_MULT_INV_DEF",
  ["GCD_MOD_MUL_INV"],
  SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(* ------------------------------------------------------------------------- *)
(* Euler's totient function                                                  *)
(* ------------------------------------------------------------------------- *)
val Euler_def = Define`
  Euler n = { i | 0 < i /\ i < n /\ (gcd n i = 1) }`;
(* that is, Euler n = { i | i in (residue n) /\ (gcd n i = 1) }`; *)

val totient_def = Define`
  totient n = CARD (Euler n)`;

(* Define Multiplicative Modulo n Group *)
val Estar_def = Define`
  Estar n =
   <| carrier := Euler n;
           id := 1;
          inv := GCD_MOD_MUL_INV n;
         mult := (\i j. (i * j) MOD n)
    |>`;


(* Theorem: Estar is a Group *)
val Estar_group = store_thm(
  "Estar_group",
  ``!n. 1 < n ==> Group (Estar n)``,
  SRW_TAC [][Estar_def, Euler_def, Group_def, RES_FORALL_THM, GEN_MULT_INV_DEF] THEN1
  METIS_TAC [PRODUCT_WITH_GCD_ONE, MOD_NOZERO_WHEN_GCD_ONE] THEN1
  RW_TAC arith_ss [MOD_LESS] THEN1
  METIS_TAC [PRODUCT_WITH_GCD_ONE, MOD_WITH_GCD_ONE] THEN
  `0 < n` by RW_TAC arith_ss [] THEN METIS_TAC [MOD_MULT_ASSOC]);


(* Theorem: FINITE (Estar p).carrier *)
val FINITE_Estar_carrier = store_thm(
  "FINITE_Estar_carrier",
  ``!n. FINITE (Estar n).carrier``,
  SRW_TAC [][Estar_def, Euler_def] THEN
  `{i | 0 < i /\ i < n /\ (gcd n i = 1)} SUBSET count n` by SRW_TAC [][pred_setTheory.SUBSET_DEF] THEN
  METIS_TAC [pred_setTheory.FINITE_COUNT, pred_setTheory.SUBSET_FINITE]);


(* Theorem: Estar is a Finite Abelian Group *)
val Estar_finite_abelian_group = store_thm(
  "Estar_finite_abelian_group",
  ``!n. 1 < n ==> FiniteAbelianGroup (Estar n)``,
  SRW_TAC [][Estar_group, FiniteAbelianGroup_def, FiniteGroup_def, AbelianGroup_def, RES_FORALL_THM] THEN1
  METIS_TAC [FINITE_Estar_carrier] THEN
  RW_TAC arith_ss [Estar_def]);


(* Theorem: group_exp (Estar n) a k = a**k MOD n *)
(* Proof:
   By induction on k.
   Base case: group_exp (Estar n) a 0 = (Estar n).id = 1, and a**0 MOD n = 1 MOD n = 1.
   Step case: group_exp (Estar n) a (SUC k)
            = a * (group_exp (Estar n) a k)  by group_exp_def
            = a * ((a ** k) MOD n)           by inductive hypothesis
            = (a MOD n) * ((a**k) MOD n)     by a < n, MOD_LESS
            = (a*(a**k)) MOD n               by MOD_TIMES2
            = (a**(SUC k) MOD n              by EXP
*)
val Estar_exp = store_thm(
  "Estar_exp",
  ``!n a. 1 < n /\ a IN (Estar n).carrier ==> !k. group_exp (Estar n) a k = (a**k) MOD n``,
  REPEAT STRIP_TAC THEN
  `Group (Estar n)` by SRW_TAC [][Estar_group] THEN
  `0 < n` by RW_TAC arith_ss [] THEN
  Induct_on `k` THENL [
    `(Estar n).id = 1` by SRW_TAC [][Estar_def] THEN
    `1 MOD n = 1` by RW_TAC arith_ss [] THEN
    METIS_TAC [group_exp_def, EXP],
    SRW_TAC [][group_exp_def, EXP] THEN
    FULL_SIMP_TAC (srw_ss())[Estar_def, Euler_def] THEN
    `a MOD n = a` by RW_TAC arith_ss [] THEN
    METIS_TAC [MOD_TIMES2]
  ]);

(* ------------------------------------------------------------------------- *)
(* The following is a rework from Hol\examples\elliptic\groupScript.sml      *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The trivial group.                                                        *)
(* ------------------------------------------------------------------------- *)

(* The trivial group: {#e} *)
val trivial_group_def = Define
  `trivial_group e : 'a group =
   <| carrier := {e};
           id := e;
          inv := (\x. e);
         mult := (\x y. e)
    |>`;

(* Theorem: {#e} is indeed a group *)
(* Proof: check by definition. *)
val trivial_group = store_thm(
  "trivial_group",
  ``!e. FiniteAbelianGroup (trivial_group e)``,
  SRW_TAC [][trivial_group_def, FiniteAbelianGroup_def, FiniteGroup_def, AbelianGroup_def, Group_def]);

(* ------------------------------------------------------------------------- *)
(* The cyclic group.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Cyclic group = all FUNPOW f by a generator e = {e, f e, f f e, f f f e, ... } *)
val cyclic_group_def = Define
  `cyclic_group e f : 'a group =
   <| carrier := { x | ?n. FUNPOW f n e = x };
           id := e;
          inv := (\x. @y. ?yi. (FUNPOW f yi e = y) /\
                   (!xi. (FUNPOW f xi e = x) ==> (FUNPOW f (xi + yi) e = e)));
         mult := (\x y. @z. !xi yi.
                   (FUNPOW f xi e = x) /\ (FUNPOW f yi e = y) ==>
                   (FUNPOW f (xi + yi) e = z))
    |>`;

(* Original:

val cyclic_group_def = Define
  `cyclic_group e f : 'a group =
   <| carrier := { x | ?n. FUNPOW f n e = x };
      id := e;
      inv := (\x. @y. ?yi. !xi.
                (FUNPOW f yi e = y) /\
                ((FUNPOW f xi e = x) ==> (FUNPOW f (xi + yi) e = e)));
      mult := (\x y. @z. !xi yi.
                (FUNPOW f xi e = x) /\ (FUNPOW f yi e = y) ==>
                (FUNPOW f (xi + yi) e = z)) |>`;

*)

(* Theorem: alternative characterization of cyclic group:
   If there exists a period k: k <> 0 /\ FUNPOW f k e = e
   Let order n = LEAST such k, then:
   (1) (cyclic_group e f).carrier = { FUNPOW f k e | k < n }
   (2) (cyclic_group e f).id = e)
   (3) !i. (cyclic_group e f).inv (FUNPOW f i e) = FUNPOW f ((n - i MOD n) MOD n) e
   (4) !i j. (cyclic_group e f).mult (FUNPOW f i e) (FUNPOW f j e) = FUNPOW f ((i + j) MOD n) e
*)
val cyclic_group_alt = store_thm(
  "cyclic_group_alt",
  ``!e f n.
     (?k. k <> 0 /\ (FUNPOW f k e = e)) /\
     (n = LEAST k. k <> 0 /\ (FUNPOW f k e = e)) ==>
     ((cyclic_group e f).carrier = { FUNPOW f k e | k < n }) /\
     ((cyclic_group e f).id = e) /\
     (!i. (cyclic_group e f).inv (FUNPOW f i e) =
          FUNPOW f ((n - i MOD n) MOD n) e) /\
     (!i j. (cyclic_group e f).mult (FUNPOW f i e) (FUNPOW f j e) =
          FUNPOW f ((i + j) MOD n) e)``,
  REPEAT GEN_TAC THEN
  SIMP_TAC std_ss [whileTheory.LEAST_EXISTS] THEN
  Q.SPEC_TAC (`LEAST k. k <> 0 /\ (FUNPOW f k e = e)`,`h`) THEN
  GEN_TAC THEN
  STRIP_TAC THEN
  `0 < h` by DECIDE_TAC THEN
  SRW_TAC [][cyclic_group_def, EXTENSION, EQ_IMP_THM] THEN1
  METIS_TAC [FUNPOW_MOD, MOD_LESS] THEN1
  METIS_TAC [] THEN1
 (normalForms.SELECT_TAC THEN
  MATCH_MP_TAC (PROVE [] ``a /\ (b ==> c) ==> ((a ==> b) ==> c)``) THEN
  CONJ_TAC THENL [
    Q.EXISTS_TAC `FUNPOW f (h - i MOD h) e` THEN
    Q.EXISTS_TAC `h - i MOD h` THEN
    SRW_TAC [][] THEN
    `FUNPOW f (xi + (h - i MOD h)) e = FUNPOW f (h - i MOD h) (FUNPOW f i e)` by METIS_TAC [FUNPOW_ADD, ADD_COMM] THEN
    `_ = FUNPOW f (h - i MOD h) (FUNPOW f (i MOD h) e)` by METIS_TAC [FUNPOW_MOD] THEN
    `_ = FUNPOW f (h - i MOD h + i MOD h) e` by METIS_TAC [FUNPOW_ADD] THEN
    METIS_TAC [SUB_ADD, MOD_LESS, LESS_IMP_LESS_OR_EQ],
    SRW_TAC [][] THEN
    Q_TAC SUFF_TAC `FUNPOW f (yi MOD h) e = FUNPOW f (h - i MOD h) e` THEN1
    METIS_TAC [FUNPOW_MOD] THEN
    Cases_on `i MOD h = 0` THENL [
      `FUNPOW f (h - i MOD h) e = e` by METIS_TAC [SUB_0] THEN
      `yi MOD h < h` by METIS_TAC [MOD_LESS] THEN
      `(yi MOD h = 0) \/ FUNPOW f (yi MOD h) e <> e` by METIS_TAC [] THEN1
      METIS_TAC [FUNPOW_0] THEN
      METIS_TAC [FUNPOW_0, FUNPOW_MOD, ADD_CLAUSES],
      `0 < i MOD h` by DECIDE_TAC THEN
      `i MOD h < h` by METIS_TAC [MOD_LESS] THEN
      Q.ABBREV_TAC `y = yi MOD h` THEN
      Q.ABBREV_TAC `x = h - i MOD h` THEN
      `x < h /\ y < h` by RW_TAC arith_ss [MOD_LESS, Abbr`x`, Abbr`y`] THEN
      `FUNPOW f (i MOD h) e = FUNPOW f i e` by METIS_TAC [FUNPOW_MOD] THEN
      `e = FUNPOW f (i MOD h + yi) e` by METIS_TAC [] THEN
      `_ = FUNPOW f (i MOD h + y) e` by METIS_TAC [FUNPOW_ADD, FUNPOW_MOD] THEN
      Cases_on `x = y` THEN1
      METIS_TAC [] THEN
      Cases_on `x > y` THENL [
        `?d. d = x - y` by RW_TAC arith_ss [] THEN
        `(x = d + y) /\ 0 <> d /\ d < h` by DECIDE_TAC THEN
        `e = FUNPOW f (x + i MOD h) e` by METIS_TAC [SUB_ADD, LESS_IMP_LESS_OR_EQ] THEN
        `_ = FUNPOW f (d + y + i MOD h) e` by METIS_TAC [] THEN
        `_ = FUNPOW f d (FUNPOW f (i MOD h + y) e)` by METIS_TAC [ADD_ASSOC, ADD_COMM, FUNPOW_ADD] THEN
        `_ = FUNPOW f d e` by METIS_TAC [] THEN
        METIS_TAC [],
        `y > x` by DECIDE_TAC THEN
        `?d. d = y - x` by RW_TAC arith_ss [] THEN
        `(y = d + x) /\ 0 <> d /\ d < h` by DECIDE_TAC THEN
        `e = FUNPOW f (y + i MOD h) e` by METIS_TAC [ADD_COMM] THEN
        `_ = FUNPOW f (d + x + i MOD h) e` by METIS_TAC [] THEN
        `_ = FUNPOW f d (FUNPOW f (x + i MOD h) e)` by METIS_TAC [FUNPOW_ADD, ADD_ASSOC] THEN
        `_ = FUNPOW f d (FUNPOW f h e)` by METIS_TAC [SUB_ADD, LESS_IMP_LESS_OR_EQ] THEN
        `_ = FUNPOW f d e` by METIS_TAC [] THEN
        METIS_TAC []
      ]
    ]
  ]) THEN
  normalForms.SELECT_TAC THEN
  MATCH_MP_TAC (PROVE [] ``a /\ (b ==> c) ==> ((a ==> b) ==> c)``) THEN
  CONJ_TAC THENL [
    Q.EXISTS_TAC `FUNPOW f (i + j) e` THEN
    SRW_TAC [][] THEN
    METIS_TAC [FUNPOW_ADD, ADD_COMM],
    SRW_TAC [][] THEN
    METIS_TAC [FUNPOW_MOD]
  ]);

(* Theorem: Group (cyclic_group e f) *)
val cyclic_group_group = store_thm(
  "cyclic_group_group",
  ``!e f. (?n. n <> 0 /\ (FUNPOW f n e = e)) ==> Group (cyclic_group e f)``,
  REPEAT GEN_TAC THEN
  (DISCH_THEN ASSUME_TAC) THEN
  MP_TAC (Q.SPECL [`e`,`f`,`LEAST n. ~(n = 0) /\ (FUNPOW f n e = e)`] cyclic_group_alt) THEN
  MATCH_MP_TAC (PROVE [] ``a /\ (b ==> c) ==> ((a ==> b) ==> c)``) THEN
  CONJ_TAC THEN1
  SRW_TAC [][] THEN
  POP_ASSUM MP_TAC THEN
  SIMP_TAC std_ss [whileTheory.LEAST_EXISTS] THEN
  Q.SPEC_TAC (`LEAST n. ~(n = 0) /\ (FUNPOW f n e = e)`,`h`) THEN
  REPEAT GEN_TAC THEN
  STRIP_TAC THEN
  `0 < h` by DECIDE_TAC THEN
  STRIP_TAC THEN
  SRW_TAC [][Group_def] THENL [
    Q.EXISTS_TAC `0` THEN
    METIS_TAC [FUNPOW_0],
    SRW_TAC [][RES_FORALL_THM] THEN
    FULL_SIMP_TAC std_ss [] THEN
    Q.EXISTS_TAC `(k + k') MOD h` THEN
    METIS_TAC [MOD_LESS],
    SRW_TAC [][RES_FORALL_THM] THEN
    FULL_SIMP_TAC std_ss [] THEN
    Q.EXISTS_TAC `(h - k) MOD h` THEN
    METIS_TAC [MOD_LESS],
    SRW_TAC [][RES_FORALL_THM] THEN
    METIS_TAC [FUNPOW_0, ADD_CLAUSES, LESS_MOD],
    SRW_TAC [][RES_FORALL_THM] THEN
    FULL_SIMP_TAC std_ss [] THEN
    METIS_TAC [LESS_MOD, MOD_PLUS, SUB_ADD, LESS_IMP_LESS_OR_EQ, MOD_EQ_0, MULT_LEFT_1, FUNPOW_0],
    SRW_TAC [][RES_FORALL_THM] THEN
    FULL_SIMP_TAC std_ss [] THEN
    METIS_TAC [LESS_MOD, MOD_PLUS, ADD_ASSOC]
  ]);

(* Theorem: FiniteAbelianGroup (cyclic_group e f) *)
(* Proof:
   Use cyclic_group_alt due to assumption: (?n. n <> 0 /\ (FUNPOW f n e = e))
*)
val cyclic_group_finite_abelian = store_thm(
  "cyclic_group_finite_abelian",
  ``!e f. (?n. n <> 0 /\ (FUNPOW f n e = e)) ==> FiniteAbelianGroup (cyclic_group e f)``,
  REPEAT GEN_TAC THEN
  (DISCH_THEN ASSUME_TAC) THEN
  MP_TAC (Q.SPECL [`e`,`f`,`LEAST n. ~(n = 0) /\ (FUNPOW f n e = e)`] cyclic_group_alt) THEN
  MATCH_MP_TAC (PROVE [] ``a /\ (b ==> c) ==> ((a ==> b) ==> c)``) THEN
  CONJ_TAC THEN1
  SRW_TAC [][] THEN
  POP_ASSUM MP_TAC THEN
  SIMP_TAC std_ss [whileTheory.LEAST_EXISTS] THEN
  Q.SPEC_TAC (`LEAST n. ~(n = 0) /\ (FUNPOW f n e = e)`,`h`) THEN
  REPEAT GEN_TAC THEN
  STRIP_TAC THEN
  `0 < h` by DECIDE_TAC THEN
  STRIP_TAC THEN
  SRW_TAC [][FiniteAbelianGroup_def, FiniteGroup_def, AbelianGroup_def] THEN1
  METIS_TAC [cyclic_group_group] THEN1
  METIS_TAC [FINITE_COUNT_IMAGE] THEN1
  METIS_TAC [cyclic_group_group] THEN
  SRW_TAC [][RES_FORALL_THM] THEN
  FULL_SIMP_TAC std_ss [] THEN
  METIS_TAC [ADD_COMM]);
(* Originally called: cyclic_group *)

(* ------------------------------------------------------------------------- *)
(* The group of addition modulo n.                                           *)
(* ------------------------------------------------------------------------- *)

(* Define Nonzero as predicate. *)
val Nonzero_def = Define `Nonzero n = n <> 0`;

(* Theorem: Nonzero n > 0 *)
(* Proof: by
- NOT_ZERO_LT_ZERO;
> val it = |- !n. n <> 0 <=> 0 < n : thm
*)
val Nonzero_positive = store_thm(
  "Nonzero_positive",
  ``!n. Nonzero n <=> 0 < n``,
  SRW_TAC [][Nonzero_def, NOT_ZERO_LT_ZERO]);

(* Additive Modulo Group *)
val add_mod_def = Define
  `add_mod n =
   <| carrier := { i | i < n };
           id := 0;
          inv := (\i. (n - i) MOD n);
         mult := (\i j. (i + j) MOD n)
    |>`;

(* Theorem: Additive Modulo Group is a group. *)
(* Proof: check group definitions. *)
val group_add_mod = store_thm(
  "group_add_mod",
  ``!n. Nonzero n ==> Group (add_mod n)``,
  REPEAT STRIP_TAC THEN
  `0 < n` by METIS_TAC [Nonzero_positive] THEN
  RW_TAC std_ss [add_mod_def, Group_def] THEN1
  SRW_TAC [][] THEN1
  SRW_TAC [][] THEN1
  SRW_TAC [][] THEN1
  SRW_TAC [][] THEN1
 (SRW_TAC [][RES_FORALL_THM] THEN
  METIS_TAC [LESS_MOD, MOD_PLUS, SUB_ADD, LESS_IMP_LESS_OR_EQ, MOD_EQ_0, MULT_LEFT_1]) THEN
  SRW_TAC [][RES_FORALL_THM] THEN
  METIS_TAC [LESS_MOD, MOD_PLUS, ADD_ASSOC]);

(* Theorem: Additive Modulo Group is a finite abelian group. *)
(* Proof: check definitions. *)
val add_mod = store_thm(
  "add_mod",
  ``!n. Nonzero n ==> FiniteAbelianGroup (add_mod n)``,
  REPEAT STRIP_TAC THEN
  `0 < n` by METIS_TAC [Nonzero_positive] THEN
  SRW_TAC [][FiniteAbelianGroup_def, FiniteGroup_def, AbelianGroup_def] THEN1
  METIS_TAC [group_add_mod] THEN1
 (`(add_mod n).carrier = count n` by SRW_TAC [][add_mod_def, count_def] THEN
  METIS_TAC [FINITE_COUNT]) THEN1
  METIS_TAC [group_add_mod] THEN
  SRW_TAC [][add_mod_def, RES_FORALL_THM] THEN
  METIS_TAC [ADD_COMM]);

(* ------------------------------------------------------------------------- *)
(* The group of multiplication modulo p.                                     *)
(* ------------------------------------------------------------------------- *)

(* Define Prime as predicate -- redundant. *)
(* val Prime_def = Define `Prime n = prime n`; *)

(* Multiplicative Modulo Group *)
(* This version relies on fermat_little from pure Number Theory! *)
(*
val mult_mod_def = Define
  `mult_mod p =
   <| carrier := { i | i <> 0 /\ i < p };
           id := 1;
          inv := (\i. i ** (p - 2) MOD p);
         mult := (\i j. (i * j) MOD p)
    |>`;
*)

(* This version relies on MOD_MULT_INV, using LINEAR_GCD. *)
val mult_mod_def = Define
  `mult_mod p =
   <| carrier := { i | i <> 0 /\ i < p };
           id := 1;
          inv := (\i. MOD_MULT_INV p i);
         mult := (\i j. (i * j) MOD p)
    |>`;

(* Theorem: prime p is Nonzero. *)
(* Proof: by
- PRIME_POS;
> val it = |- !p. prime p ==> 0 < p : thm
*)
val Prime_Nonzero = store_thm(
  "Prime_Nonzero",
  ``!p. prime p ==> Nonzero p``,
  METIS_TAC [PRIME_POS, Nonzero_positive]);

(* Theorem: Multiplicative Modulo Group is a group for prime p. *)
(* Proof: check by group definitions. *)
val group_mult_mod = store_thm(
  "group_mult_mod",
  ``!p. prime p ==> Group (mult_mod p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by METIS_TAC [PRIME_POS] THEN
  RW_TAC std_ss [mult_mod_def, Group_def] THENL [
    SRW_TAC [][] THEN
    METIS_TAC [ONE_LT_PRIME],
    SRW_TAC [][RES_FORALL_THM] THEN
    METIS_TAC [EUCLID_LEMMA, LESS_MOD],
    SRW_TAC [][RES_FORALL_THM] THEN1
    METIS_TAC [MOD_MULT_INV_DEF, NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [MOD_MULT_INV_DEF, NOT_ZERO_LT_ZERO],
    SRW_TAC [][],
    SRW_TAC [][RES_FORALL_THM] THEN
    METIS_TAC [MOD_MULT_INV_DEF, NOT_ZERO_LT_ZERO],
    SRW_TAC [][RES_FORALL_THM] THEN
    METIS_TAC [MOD_MULT_ASSOC]
  ]);

(* Theorem: Multiplicative Modulo Group is a finite abelian group for prime p. *)
(* Proof: check definitions *)
val mult_mod = store_thm(
  "mult_mod",
  ``!p. prime p ==> FiniteAbelianGroup (mult_mod p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by METIS_TAC [PRIME_POS] THEN
  SRW_TAC [][FiniteAbelianGroup_def, FiniteGroup_def, AbelianGroup_def] THEN1
  METIS_TAC [group_mult_mod] THEN1
 (`(mult_mod p).carrier SUBSET count p` by SRW_TAC [][mult_mod_def, SUBSET_DEF] THEN
  METIS_TAC [FINITE_COUNT, SUBSET_FINITE]) THEN1
  METIS_TAC [group_mult_mod] THEN
  SRW_TAC [][mult_mod_def, RES_FORALL_THM] THEN
  METIS_TAC [MULT_COMM]);

(* ========================================================================= *)
(* Cryptography based on groups                                              *)
(* ========================================================================= *)


(* ------------------------------------------------------------------------- *)
(* ElGamal encryption and decryption -- purely group-theoretic.              *)
(* ------------------------------------------------------------------------- *)

val elgamal_encrypt_def = Define
  `elgamal_encrypt g y h m k = (y ** k, g.mult (h ** k) m)`;

val elgamal_decrypt_def = Define
  `elgamal_decrypt g x (a,b) = g.mult (|/ (a ** x)) b`;

(* Theorem: ElGamal decypt can undo ElGamal encrypt. *)
(* Proof:
   This is to show
   elgamal_decrypt g x (elgamal_encrypt g y h m k)  = m
   or:     |/ ((y ** k) ** x) * ((y ** x) ** k * m) = m

   |/ ((y ** k) ** x) * ((y ** x) ** k * m)
 = |/ (y ** (k*x)) * (y ** (x*k) * m)      by group_exp_mult
 = (|/ y)**(k*x) * (y**(x*k) * m)          by group_exp_inv
 = (|/ y)**(k*x) * (y**(k*x) * m)          by MULT_COMM (the x*k is not g.mult, in exp)
 = ((|/ y)**(k*x) * y**(k*x)) * m          by group_assoc
 = (|/y * y)**(k*x) * m                    by group_mult_exp
 = #e**(k*x) * m                           by group_linv, group_rinv
 = #e * m                                  by group_id_exp
 = m                                       by group_lid
*)
val elgamal_correctness = store_thm(
  "elgamal_correctness",
  ``!g. Group g ==> !y h m :: (g.carrier). !k x.
       (h = y ** x) ==> (elgamal_decrypt g x (elgamal_encrypt g y h m k) = m)``,
  SRW_TAC [][elgamal_encrypt_def, elgamal_decrypt_def, RES_FORALL_THM] THEN
  `|/ ((y ** k) ** x) * ((y ** x) ** k * m) = |/ (y ** (k*x)) * (y ** (x*k) * m)` by METIS_TAC [group_exp_mult] THEN
  `_ = (|/ y)**(k*x) * (y**(x*k) * m)` by METIS_TAC [group_exp_inv] THEN
  `_ = (|/ y)**(k*x) * (y**(k*x) * m)` by METIS_TAC [MULT_COMM] THEN
  `_ = ((|/ y)**(k*x) * y**(k*x)) * m` by SRW_TAC [][group_assoc, group_inv_carrier, group_exp_carrier] THEN
  `_ = (|/y * y)**(k*x) * m` by METIS_TAC [group_linv, group_rinv, group_mult_exp, group_inv_carrier] THEN
  METIS_TAC [group_linv, group_id_exp, group_lid]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
