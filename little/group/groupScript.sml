(* !wait  1*second *)
(* !pause "Press CTRL-Z when HOL is ready." *)
(* ------------------------------------------------------------------------- *)
(* Group Theory -- axioms to exponentiation.                                 *)
(* ------------------------------------------------------------------------- *)

(*

Group Theory
============
based on: examples/elliptic/groupScript.sml

The idea behind this script is discussed in (Secton 2.1.1. Groups):

Formalizing Elliptic Curve Cryptography in Higher Order Logic (Joe Hurd)
http://www.gilith.com/research/papers/elliptic.pdf

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "group";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory;

(* ------------------------------------------------------------------------- *)
(* Group Definition.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Set up group type as a record
   A Group has:
   . a carrier set (set = function 'a -> bool, since MEM is a boolean function)
   . an identity element
   . an inverse function (unary operation)
   . a product function called multiplication (binary operation)
*)
val _ = Hol_datatype`
  group = <| carrier: 'a -> bool;
                  id: 'a;
                 inv: 'a -> 'a;
                mult: 'a -> 'a -> 'a
           |>`;

(* overloading for convenience *)
val _ = overload_on ("*", ``g.mult``);
val _ = overload_on ("|/", ``g.inv``);
val _ = overload_on ("#e", ``g.id``);
val _ = overload_on ("G", ``g.carrier``);

(* Group Definition:
   A Group is a set with elements of g of type 'a group, such that
   . g.id is in the carrier set (#e in G)
   . (g.mult x y) is in the carrier set (closure: x*y in G)
   . (g.inv x) is in the carrier set (existence of inverse: x^-1 in G)
   . (g.mult (g.inv x) x) = (g.id)  (property of inverse and identity: (|/ x)*(x) = e)
   . (g.mult (g.mult x y) z) = (g.mult x (g.mult y z)) (associativity: (x*y)*z = x*(y*z))
*)
(* Define Group by predicate *)
val Group_def = Define`
  Group (g: 'a group) =
    #e IN G /\
    (!x y::(G). x * y IN G) /\
    (!x::(G). |/x IN G) /\
    (!x::(G). #e * x = x) /\
    (!x::(G). |/x * x = #e) /\
    (!x y z::(G). (x * y) * z = x * (y * z))
  `;
(* !pause *)

(* ------------------------------------------------------------------------- *)
(* More Group Defintions.                                                    *)
(* ------------------------------------------------------------------------- *)
(* Abelian Group: a Group with a commutative product: x * y = y * x. *)
val AbelianGroup_def = Define`
  AbelianGroup (g: 'a group) =
    Group g /\
    !x y::(G). x * y = y * x
  `;

(* Finite Group: a Group with a finite carrier set. *)
val FiniteGroup_def = Define`
  FiniteGroup (g: 'a group) =
    Group g /\ FINITE g.carrier
  `;

(* Finite Abelian Group: a Group that is both Finite and Abelian. *)
val FiniteAbelianGroup_def = Define`
  FiniteAbelianGroup (g: 'a group) =
    FiniteGroup g /\ AbelianGroup g
  `;
(* !pause *)

(* Summary of theorems

group_id_carrier    #e in G
group_inv_carrier   |/ x in G
group_mult_carrier  x * y in G                   [closure]
group_lid           #e * x = x                   [left-identity]
group_linv          |/ x * x = #e                [left-inverse]
group_assoc         (x * y) * z = x * (y * z)    [associativity]

group_rinv          x * |/ x = #e
group_rid           x * #e = x                   [uses group_rinv]
group_lsolve        x * y = z <=> x = z * |/ y   [uses group_rinv/linv, group_rid]
group_rsolve        x * y = z <=> y = |/ x * z   [uses group_rinv/linv, group_lid]
group_lcancel       x * y = x * z <=> y = z      [uses group_rsolve, group_linv, group_lid]
group_rcancel       y * x = z * x <=> y = z      [uses group_lsolve, group_rinv, group_rid]
group_lid_unique    y * x = x <=> y = #e         [uses group_lsolve, group_rinv]
group_rid_unique    x * y = x <=> y = #e         [uses group_rsovle, group_linv]
group_linv_unique   x * y = #e <=> x = |/ y      [uses group_lsovle, group_linv, group_lid]
group_rinv_unique   x * y = #e <=> y = |/ x      [uses group_rsolve, group_rinv, group_rid]
group_inv_inv       |/ (|/ x) = x                [uses group_linv_unique, group_rinv]
group_inv_cancel    |/ x = |/ y <=> x = y        [uses group_inv_inv]
group_inv_eq_swap   |/ x = y <=> x = |/ y        [uses group_inv_inv]
group_inv_id        |/ #e = #e                   [uses group_linv_unique, group_lid]
group_inv_mult      |/ (x * y) = |/ y * |/ x     [uses group_rinv_unique, group_rinv, group_rid]
group_id_fix        x * x = x ==> x = #e         [uses group_lid, group_rcancel]

group_exp_carrier   x**n in G
group_exp_zero      x**0 = #e
group_exp_one       x**1 = x                          [uses group_rid]
group_id_exp        #e**n = #e                        [uses group_lid]
group_comm_exp      x*y = y*x ==> x**n *y = y* x**n   [uses group_lid, group_rid, group_assoc]
group_exp_comm      x**n *x = x* x**n
group_mult_exp      x*y = y*x ==> (x*y)**n = (x**n)*(y**n)  [uses group_lid, group_assoc, group_comm_exp]
group_exp_add       x**(m + n) = (x**m)*(x**n)        [uses group_lid, group_assoc]
group_exp_mult      x**(m * n) = (x** m)** n          [uses group_exp_add, group_mult_exp, group_exp_comm]
group_exp_inv       |/(x**n) = (|/x)**n               [uses group_inv_id, group_exp_comm]
group_exp_eq        m < n, x**m = x**n ==> x**(n-m) = #e  [uses group_rid_unique]

*)
(* !pause *)

(* ------------------------------------------------------------------------- *)
(* Basic theorem from definition.                                            *)
(* ------------------------------------------------------------------------- *)

(* Group clauses from definition *)
val group_clauses = save_thm("group_clauses",
    Group_def |> SPEC_ALL |> #1 o EQ_IMP_RULE);

(* Theorem: Group identity is an element. *)
val group_id_carrier = save_thm("group_id_carrier",
    group_clauses |> UNDISCH_ALL |> CONJUNCT1 |> DISCH_ALL |> GEN_ALL);

(* Theorem: Group inverse is an element. *)
val group_inv_carrier = save_thm("group_inv_carrier",
    group_clauses |> UNDISCH_ALL |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT1 |> DISCH_ALL |> GEN_ALL);

(* Theorem: [Group closure] Group product is an element. *)
val group_mult_carrier = save_thm("group_mult_carrier",
    group_clauses |> UNDISCH_ALL |> CONJUNCT2 |> CONJUNCT1 |> DISCH_ALL |> GEN_ALL);

(* Theorem: [Group left Identity] #e * x = x *)
val group_lid = save_thm("group_lid",
    group_clauses |> UNDISCH_ALL |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT1 |> DISCH_ALL |> GEN_ALL);

(* Theorem: [Group left inverse] |/ x * x = e *)
val group_linv = save_thm("group_linv",
    group_clauses |> UNDISCH_ALL |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT1 |> DISCH_ALL |> GEN_ALL);

(* Theorem: [Group associativity] (x * y) * z = x * (y * z) *)
val group_assoc = save_thm("group_assoc",
    group_clauses |> UNDISCH_ALL |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT2 |> CONJUNCT2 |> DISCH_ALL |> GEN_ALL);

(* Export rewrites after using Group_def, don't use Group_def anymore *)
val _ = export_rewrites ["group_id_carrier", "group_inv_carrier", "group_mult_carrier"];
(* !pause *)

(* ------------------------------------------------------------------------- *)
(* Theorems in basic Group Theory.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Group right inverse] x * |/ x = #e *)
(* Proof:
   x * |/ x
 = #e  * (x * |/ x)                 by left identity: X = #e * X
 = (#e * x) * |/ x                  by associativity
 = (|/ (|/ x) * |/ x) * x) * |/ x   by left inverse: #e = |/ Y * Y
 = (|/ (|/ x) * (|/ x * x)) * |/ x  by associativity
 = (|/ (|/ x) * #e) * |/ x          by left inverse: |/ Y * Y = #e
 = |/ (|/ x) * (#e * |/ x)          by associativity
 = |/ (|/ x) * (|/ x)               by left idenity: #e * X = X
 = #e                               by left inverse: |/ Y * Y = #e
*)
val group_rinv = store_thm(
  "group_rinv",
  ``!g. Group g ==> !x :: (g.carrier). x * |/x = #e``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * |/x = #e * (x * |/x)`       by SRW_TAC [][group_lid] THEN
  `_ = (#e * x) * |/x`             by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x * x * |/x`    by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (|/x * x) * |/x`  by SRW_TAC [][GSYM group_assoc] THEN
  `_ = |/(|/x) * #e * |/x`         by SRW_TAC [][group_linv] THEN
  `_ = |/(|/x) * (#e * |/x)`       by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x`              by SRW_TAC [][group_lid] THEN
  `_ = #e`                         by SRW_TAC [][group_linv] THEN
  SRW_TAC [][]);
(* !pause *)

(* Theorem: [Group right identity] x * #e = x *)
(* Proof:
   x * #e
 = x * (|/ x * x)     by left inverse: |/ Y * Y = #e
 = (x * |/ x) * x     by associativity
 = #e * x             by right inverse: Y * |/ Y = #e
 = x                  by left identity: #e * X = X
*)
val group_rid = store_thm(
  "group_rid",
  ``!g. Group g ==> !x :: (g.carrier). x * #e = x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `x * #e = x * (|/x * x)` by SRW_TAC [][group_linv] THEN
  `_ = (x * |/x) * x`      by SRW_TAC [][group_assoc] THEN
  `_ = #e * x`             by SRW_TAC [][group_rinv] THEN
  SRW_TAC [][group_lid]);
(* !pause *)

(* Theorem: [Solve left unknown] x * y = z  <=> x = z * y' *)
(* Proof:
   For the if part:
      z y'
    = (x y) y'   by substituting z
    = x (y y')   by associativity
    = x e        by right inverse
    = x          by right identity
   For the only-if part:
      x y
    = (z y') y   by substituting x
    = z (y' y)   by associativity
    = z e        by left inverse
    = z          by right identity
*)
val group_lsolve = store_thm(
  "group_lsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (x = z * |/y)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_rid]);
(* !pause *)

(* Theorem: [Solve right unknown] x y = z  <=> y = x' z *)
(* Proof:
   For the if part:
      x' z
    = x' (x y)   by substituting z
    = (x' x) y   by associativity
    = e y        by left inverse
    = y          by left identity
   For the only-if part:
      x y
    = x (x' z)   by substituting y
    = (x x') z   by associativity
    = e z        by right inverse
    = z          by left identity
*)
val group_rsolve = store_thm(
  "group_rsolve",
  ``!g. Group g ==> !x y z:: (g.carrier). (x * y = z) = (y = |/x * z)``,
  METIS_TAC [group_inv_carrier, group_assoc, group_rinv, group_linv, group_lid]);
(* !pause *)

(* Theorem: [Left cancellation] x y = x z <=> y = z *)
(* Proof:
        x y = x z
   <=>  y = x' (x z)    by group_rsolve
          = (x' x) z    by associativity
          = e z         by left inverse
          = z           by left identity
*)
val group_lcancel = store_thm(
  "group_lcancel",
  ``!g. Group g ==> !x y z :: (g.carrier). (x * y = x * z) = (y = z)``,
  METIS_TAC [group_inv_carrier, group_mult_carrier, group_assoc, group_rsolve, group_linv, group_lid]);
(* !pause *)

(* Theorem: [Right cancellation] y x = z x <=> y = z *)
(* Proof:
       y x = z x
   <=> y = (z x) x'   by group_lsolve
         = z (x x')   by associativity
         = z e        by right inverse
         = z          by right identity
*)
val group_rcancel = store_thm(
  "group_rcancel",
  ``!g. Group g ==> !x y z :: (g.carrier). (y * x = z * x) = (y = z)``,
  METIS_TAC [group_inv_carrier, group_mult_carrier, group_assoc, group_lsolve, group_rinv, group_rid]);
(* !pause *)

(* Theorem: [Left identity unique] y x = x <=> y = e *)
(* Proof:
       y x = x
   <=> y = x x'   by group_lsolve
         = e      by right inverse
*)
val group_lid_unique = store_thm(
  "group_lid_unique",
  ``!g. Group g ==> !x y :: (g.carrier). (y * x = x) = (y = #e)``,
  METIS_TAC [group_inv_carrier, group_lsolve, group_rinv]);
(* !pause *)

(* Theorem: [Right identity unique] x y = x <=> y = e *)
(* Proof:
       x y = x
   <=> y = x' x    by group_rsolve
         = e       by left inverse
*)
val group_rid_unique = store_thm(
  "group_rid_unique",
  ``!g. Group g ==> !x y :: (g.carrier). (x * y = x) = (y = #e)``,
  METIS_TAC [group_inv_carrier, group_rsolve, group_linv]);
(* !pause *)

(* Theorem: [Left inverse unique] x y = e <=> x = y' *)
(* Proof:
       x y = e
   <=> x = e y'    by group_lsolve
         = y'      by left identity
*)
val group_linv_unique = store_thm(
  "group_linv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (x = |/y)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_lsolve, group_linv, group_lid]);
(* !pause *)

(* Theorem: [Right inverse unique] x y = e <=> y = x' *)
(* Proof:
       x y = e
   <=> y = x' e    by group_rsolve
         = x'      by right identity
*)
val group_rinv_unique = store_thm(
  "group_rinv_unique",
  ``!g. Group g ==> !x y:: (g.carrier). (x * y = #e) = (y = |/x)``,
  METIS_TAC [group_mult_carrier, group_inv_carrier, group_rsolve, group_rinv, group_rid]);
(* !pause *)

(* Theorem: [Inverse of inverse] x'' = x *)
(* Proof:
       x x' = e     by right inverse
   <=>    x = (x')' by group_linv_unique
*)
val group_inv_inv = store_thm(
  "group_inv_inv",
  ``!g. Group g ==> !x :: (g.carrier). |/(|/x) = x``,
  METIS_TAC [group_inv_carrier, group_rinv, group_linv_unique]);
(* !pause *)

(* Theorem: [Inverse cancellation] x' = y' <=> x = y *)
(* Proof:
   Only-if part is trivial. For the if part:
       x' = y'
   ==> (x')' = (y')'
   ==>     x = y       by group_inv_inv
*)
val group_inv_cancel = store_thm(
  "group_inv_cancel",
  ``!g. Group g ==> !x y :: (g.carrier). (|/x = |/y) = (x = y)``,
  METIS_TAC [group_inv_carrier, group_inv_inv]);
(* !pause *)

(* Theorem: [Inverse equality swap]: x' = y <=> x = y' *)
(* Proof:
       x' = y
   <=> x'' = y'    by group_inv_cancel
   <=>   x = y'    by group_inv_inv
*)
val group_inv_eq_swap = store_thm(
  "group_inv_eq_swap",
  ``!g. Group g ==> !x y :: (g.carrier). (|/x = y) = (x = |/y)``,
  METIS_TAC [group_inv_cancel, group_inv_inv]);
(* !pause *)

(* Theorem: [Inverse of identity] e' = e *)
(* Proof:
       e e = e    by left identity: e X = X
   <=>   e = e'   by group_linv_unique
*)
val group_inv_id = store_thm(
  "group_inv_id",
  ``!g. Group g ==> (|/ #e = g.id)``,
  METIS_TAC [group_id_carrier, group_lid, group_linv_unique]);
val group_inv_id = store_thm(
  "group_inv_id",
  ``!g. Group g ==> (|/g.id = #e)``,
  METIS_TAC [group_id_carrier, group_lid, group_linv_unique]);
(* Note: cannot use |/#e = #e, for then !g will become !g' by alpha-conversion *)
(* !pause *)

(* Theorem: [Inverse of product] (x y)' = y' x' *)
(* Proof:
   First show this product:
     (x y) (y' x')
   = ((x y) y') x'     by associativity
   = (x (y y')) x'     by associativity
   = (x e) x'          by right inverse
   = x x'              by right identity
   = e                 by rigtt inverse
   Hence (x y)' = y' x'  by group_rinv_unique.
*)
val group_inv_mult = store_thm(
  "group_inv_mult",
  ``!g. Group g ==> !x y :: (g.carrier). |/(x * y) = |/y * |/x``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `(x * y) * (|/y * |/x) = x * (y * |/y) * |/x` by SRW_TAC [][group_assoc] THEN
  `_ = #e`                                      by SRW_TAC [][group_rinv, group_rid] THEN
  POP_ASSUM MP_TAC THEN
  SRW_TAC [][group_rinv_unique]);
(* !pause *)

(* Theorem: The identity is a fixed point: x*x = x ==> x = #e. *)
(* Proof:
   For the if part:
       x * x = x
   ==> x * x = #e * x     by group_lid
   ==> x = #e             by group_rcancel
   For the only-if part:
       #e * #e = #e       by group_lid
*)
val group_id_fix = store_thm(
  "group_id_fix",
  ``!g x. Group g /\ x IN g.carrier ==> ((x * x = x) <=> (x = #e))``,
  METIS_TAC [group_lid, group_rcancel, group_id_carrier]);
(* !pause *)

(* ------------------------------------------------------------------------- *)
(* Application of basic Group Theory:                                        *)
(* Exponentiation - the FUNPOW version of Group operation.                   *)
(* ------------------------------------------------------------------------- *)

(* Define exponents of a group element:
   For x in Group g,   group_exp x 0 = g.id
                       group_exp x (SUC n) = g.mult x (group_exp x n)
*)
val group_exp_def = Define`
  (group_exp g x 0 = g.id) /\
  (group_exp g x (SUC n) = x * (group_exp g x n))
`;

(* set overloading  *)
val _ = overload_on ("**", ``group_exp g``);

(* Theorem: (x ** n) in g.carrier *)
(* Proof:
   By induction on n.
   Base case: x ** 0 = g.id IN g.carrier by group_id_carrier.
   Step case: True because !x y :: g.carrier. x*y IN g.carrier by group_mult_carrier.
*)
val group_exp_carrier = store_thm(
  "group_exp_carrier",
  ``!g x n. Group g /\ x IN g.carrier ==> (x ** n) IN g.carrier``,
  Induct_on `n` THEN METIS_TAC [group_exp_def, group_id_carrier, group_mult_carrier]);

(* Theorem: x ** 0 = #e *)
(* Proof:
   x ** 0 = #e  by definition.
*)
val group_exp_zero = store_thm(
  "group_exp_zero",
  ``!g. Group g /\ x IN g.carrier ==> (x ** 0 = #e)``,
  SRW_TAC [][group_exp_def]);

(* Theorem: x ** 1 = x *)
(* Proof:
   x ** 1 = x ** SUC 0 = x * x**0 = x * #e = x   by group_rid.
*)
val group_exp_one = store_thm(
  "group_exp_one",
  ``!g. Group g /\ x IN g.carrier ==> (x ** 1 = x)``,
  REPEAT STRIP_TAC THEN
  `1 = SUC 0` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_def, group_rid]);

(* Theorem: (g.id ** n) = g.id  *)
(* Proof:
   By induction on n.
   Base case: #e ** 0 = #e    by group_exp_def.
   Step case: True because !y :: g.carrier. #e*y = y  by group_lid.
*)
val group_id_exp = store_thm(
  "group_id_exp",
  ``!g n. Group g ==> (g.id ** n = #e)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_lid, group_id_carrier]);

(* Theorem: For abelian group g,  (x ** n) * y = y * (x ** n) *)
(* Proof:
   By induction on n.
   Base case: (x ** 0) * y
             = #e * y           by group_exp_def
             = y * #e           by group_rid
             = y * (x ** 0)     by group_exp_def
   Step case:  (x * x** n) * y
             = x * ((x ** n)*y) by group_assoc
             = x * (y*(x ** n)) by abelian group
             = (x*y) * (x ** n) by group_assoc
             = (y*x) * (x ** n) by abelian group
             = y *(x* (x ** n)) by group_assoc
*)
val group_comm_exp = store_thm(
  "group_comm_exp",
  ``!g x y n. Group g /\ x IN g.carrier /\ y IN g.carrier ==>
    (x * y = y * x) ==> ((x ** n) * y = y * (x ** n))``,
  Induct_on `n` THEN
  SRW_TAC [][group_exp_def, group_lid, group_rid] THEN
  METIS_TAC [group_assoc, group_exp_carrier]);

(* Theorem: (x ** n) * x = x * (x ** n) *)
(* Proof:
   Since x * x = x * x, this is true by group_comm_exp.
*)
val group_exp_comm = store_thm(
  "group_exp_comm",
  ``!g x n. Group g /\ x IN g.carrier ==> ((x ** n) * x = x * (x ** n))``,
  METIS_TAC [group_comm_exp]);

(* Theorem: For abelian group, (x * y) ** n = (x ** n) * (y ** n) *)
(* Proof:
   By induction on n.
   Base case: (x*y) ** 0 = #e = #e * #e = (x ** 0) * (y ** 0).
   Step case: (x*y)*((x*y) ** n)
            = (x*y)*((x**n)*(y**n)) by inductive hypothesis
            = x*(y*((x**n)*(y**n))) by group_assoc
            = x*((y*(x**n))*(y**n)) by group_assoc
            = x*(((x**n)*y)*(y**n)) by group_comm_exp
            = x*((x**n)*(y*(y**n))) by group_assoc
            = (x*(x**n))*(y*(y**n)) by group_assoc
*)
val group_mult_exp = store_thm(
  "group_mult_exp",
  ``!g x y n. Group g /\ x IN g.carrier /\ y IN g.carrier /\ (x * y = y * x) ==>
    ((x * y) ** n = (x ** n) * (y ** n))``,
  Induct_on `n` THEN1 METIS_TAC [group_exp_def, group_lid, group_id_carrier] THEN
  SRW_TAC [][group_exp_def] THEN
  `x * y * (x * y) ** n = (x*y)*((x**n)*(y**n))` by METIS_TAC [group_mult_carrier] THEN
  `_ = x*(y*((x**n)*(y**n)))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  `_ = x*((y*(x**n))*(y**n))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  `_ = x*(((x**n)*y)*(y**n))` by METIS_TAC [group_mult_carrier, group_exp_carrier, group_comm_exp] THEN
  `_ = x*((x**n)*(y*(y**n)))` by SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier] THEN
  SRW_TAC [][group_assoc, group_mult_carrier, group_exp_carrier]);

(* Theorem: x ** (m + n) = (x ** m) * (x ** n) *)
(* Proof:
   By induction on m.
   Base case: x ** (0 + n) = x ** n = #e * (x ** n) = (x ** 0) * (x ** n).
   Step case: x ** (SUC m + n)
            = x ** (SUC (m + n))   by arithmetic
            = x * (x ** (m + n))   by group_exp_def
            = x * ((x**m)*(x**n))  by inductive hypothesis
            = (x * (x**m))*(x**n)  by group_assoc
*)
val group_exp_add = store_thm(
  "group_exp_add",
  ``!g x m n. Group g /\ x IN g.carrier ==> (x ** (m + n) = (x ** m) * (x ** n))``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_lid, group_id_carrier, group_exp_carrier] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m + n = SUC (m+n)` by RW_TAC arith_ss [] THEN
  `x ** (SUC m + n) = x ** (SUC (m+n))` by SRW_TAC [][] THEN
  `_ = x * (x ** (m + n))` by SRW_TAC [][group_exp_def] THEN
  METIS_TAC [group_assoc, group_exp_carrier]);

(* Theorem: x ** (m * n) = (x ** m) ** n  *)
(* Proof:
   By induction on m.
   Base case: x ** (0 * n) = x ** 0 = #e = (#e)**n = (x ** 0) ** n, by group_id_exp.
   Step case: x ** (SUC m * n)
            = x ** (m*n + n)       by arithmetic
            = (x**(m*n))*(x ** n)  by group_exp_add
            = ((x**m)**n)*(x**n)   by inductive hypothesis
            = ((x**m)*x)**n        by group_mult_exp and group_exp_comm
            = (x*(x**m))**n        by group_exp_comm
*)
val group_exp_mult = store_thm(
  "group_exp_mult",
  ``!g x m n. Group g /\ x IN g.carrier ==> (x ** (m * n) = (x ** m) ** n)``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_id_exp] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m * n = m * n + n` by METIS_TAC [arithmeticTheory.MULT] THEN
  `x ** (SUC m * n) = x ** (m*n + n)` by SRW_TAC [][] THEN
  `_ = (x**(m*n))*(x ** n)` by SRW_TAC [][group_exp_add] THEN
  `_ = ((x**m)**n)*(x**n)` by METIS_TAC [] THEN
  `_ = ((x**m)*x)**n` by SRW_TAC [][group_mult_exp, group_exp_comm, group_exp_carrier] THEN
  SRW_TAC [][group_exp_comm]);

(* Theorem: Inverse of exponential:  |/(x**n) = (|/x) ** n *)
(* Proof:
   By induction on n.
   Base case: |/ x**0 = |/#e = #e = #e ** 0 = (|/#e)**0  since |/#e = #e by group_inv_id
   Step case: |/ x**(SUC n)
            = |/ x * (x ** n)      by group_exp_def
            = (|/ (x ** n))*(|/x)  by group_inv_mult
            = (|/x)**n * (|/x)     by inductive hypothesis
            = (|/x)* (|/x) ** n    by group_exp_comm
            = (|/x)*(SUC n)
*)
val group_exp_inv = store_thm(
  "group_exp_inv",
  ``!g x n. Group g /\ x IN g.carrier ==> (|/ (x ** n) = (|/ x) ** n)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_inv_id, group_inv_carrier, group_inv_mult,
             group_exp_carrier, group_exp_comm]);

(* Theorem: For m < n, x**m = x**n ==> x**(n-m) = #e *)
(* Proof:
   If m < n, then n = m + d,
     x**n
   = x**(m+d)             by n = m+d
   = (x**m)*(x**d)        by group_exp_add
   ==> x**d = #e          by group_rid_unique
*)
val group_exp_eq = store_thm(
  "group_exp_eq",
  ``!g x m n.Group g /\ x IN g.carrier /\ m < n /\ (x**m = x**n) ==> (x**(n-m) = #e)``,
  REPEAT STRIP_TAC THEN
  `?d. (d = n - m) /\ 0 < d /\ (n = m + d)` by RW_TAC arith_ss [] THEN
  `x**n = x**(m+d)` by SRW_TAC [][] THEN
  `_ = x**m * x**d` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_rid_unique, group_exp_carrier]);

(* ------------------------------------------------------------------------- *)
(* Application of Group Exponentiaton:                                       *)
(* The order of a group element in a Finite Group.                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For Group g and x IN g.carrier, if x**n are all distinct, g.carrier is INFINITE *)
(* Proof:
   By contradiction.
   Let c = CARD g.carrier.
   The map (count (SUC c)) -> G such that n -> x**n is:
   (1) a map since each x**n IN g.carrier
   (2) injective since all x**n are distinct
   But c < SUC c = CARD (count (SUC c)), and this contradicts the Pigeon-hole Principle.
*)
val group_exp_all_distinct = store_thm(
  "group_exp_all_distinct",
  ``!g x. Group g /\ x IN g.carrier /\ (!m n. (x**m = x**n) ==> (m = n)) ==> INFINITE g.carrier``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `?c. c = CARD g.carrier` by SRW_TAC [][] THEN
  `c < SUC c` by RW_TAC arith_ss [] THEN
  `INJ (\n. x**n) (count (SUC c)) g.carrier`
    by SRW_TAC [][pred_setTheory.INJ_DEF, group_exp_carrier] THEN
  METIS_TAC [pred_setTheory.CARD_COUNT, pred_setTheory.PHP]);

(* Theorem: For FINITE Group g and x IN g.carrier, there is a k > 0 such that x**k = #e. *)
(* Proof:
   Since g.carrier is FINITE,
   ?m n. m <> n and x**m = x**n      by group_exp_all_distinct
   Assume m < n, then x**(n-m) = #e  by group_exp_eq
   The case m > n is symmetric.

   Note: Probably can be improved to bound k <= CARD g.carrier.
*)
val group_exp_period_exists = store_thm(
  "group_exp_period_exists",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> ?k. 0 < k /\ (x**k = #e)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?m n. m <> n /\ (x**m = x**n)` by METIS_TAC [group_exp_all_distinct] THEN
  Cases_on `m < n` THENL [
    `0 < n-m` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [group_exp_eq]);

(* Define order = LEAST period for an element x in Group g *)
val period_def = Define`
  period g x k = 0 < k /\ (x**k = g.id)
`;
val order_def = Define`
  order g x = LEAST k. period g x k
`;

(* Theorem: The finite group order is indeed a period. *)
val group_order_period = store_thm(
  "group_order_period",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> period g x (order g x)``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LEAST_INTRO]);

(* Theorem: If k < order of finite group, then k is not a period. *)
val group_order_minimal = store_thm(
  "group_order_minimal",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> !k. k < (order g x) ==> ~ period g x k``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LESS_LEAST]);

(* Theorem: The finite group order m satisfies: 0 < m and x**m = #e. *)
(* Proof: by order being a period, and apply period_def. *)
val group_order_property = store_thm(
  "group_order_property",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> 0 < order g x /\ (x ** order g x = #e)``,
  METIS_TAC [group_order_period, period_def]);

(* Theorem: For finite group, |/ x = x ** (m-1) where m = order g x *)
(* Proof:
   Let m = order g x.
   Since x**(m-1)*x = x**m = #e    by group_exp_add
   |/x = x**(m-1)                  by group_linv_unique
*)
val group_order_inv = store_thm(
  "group_order_inv",
  ``!g x. FiniteGroup g /\ x IN g.carrier ==> (|/x = x**((order g x)-1))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `x ** 1 = x` by METIS_TAC [group_exp_one] THEN
  `(m-1)+1 = m` by RW_TAC arith_ss [] THEN
  `x ** (m-1) * x = #e` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_linv_unique, group_exp_carrier]);

(* Theorem: For FINITE Group g, x**n = x**(n mod m), where m = order g x *)
(* Proof:
     x**n
   = x**(m*q + r)        by arithmetic and n = q*m + r
   = x**(m*q)*(x**r)     by group_exp_add
   = ((x**m)**q)*(x**r)  by group_exp_mult
   = (#e**q)*(x**r)      by group_order_property
   = #e*(x**r)           by group_id_exp
   = x**r                by group_lid
*)
val group_exp_mod = store_thm(
  "group_exp_mod",
  ``!g x n. FiniteGroup g /\ x IN g.carrier ==> (x**n = x**(n MOD order g x))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `n = (n DIV m)*m + (n MOD m)` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = m*(n DIV m) + (n MOD m)` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_add, group_exp_mult, group_id_exp, group_lid, group_exp_carrier]);

(* Theorem: For FINITE group g, m, n < (order g x), x**m = x**n ==> m = n *)
(* Proof:
   Otherwise x**(m-n) = #e by group_exp_eq,
   contradicting minimal nature of m.
*)
val group_order_unique = store_thm(
  "group_order_unique",
  ``!g x m n. FiniteGroup g /\ x IN g.carrier /\ m < (order g x) /\ n < (order g x) ==>
    (x**m = x**n) ==> (m = n)``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  Cases_on `m < n` THENL [
    `0 < n-m /\ n-m < order g x` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n /\ m-n < order g x` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [FiniteGroup_def, group_exp_eq, group_order_minimal, period_def]);

(* ------------------------------------------------------------------------- *)
(* Homomorphisms, isomorphisms, endomorphisms, automorphisms and subgroups.  *)
(* ------------------------------------------------------------------------- *)

(* A function f from g to h is a homomorphism if group properties are preserved. *)
val GroupHom_def = Define`
  GroupHom f g h =
    (!x :: (g.carrier). f x IN h.carrier) /\
    (f #e = h.id) /\
    (!x :: (g.carrier). f (|/ x) = h.inv (f x)) /\
    (!x y :: (g.carrier). f (x * y) = h.mult (f x) (f y))
`;

(* A function f from g to h is an isomorphism if f is a surjective homomorphism. *)
val GroupIso_def = Define`
  GroupIso f g h =
    GroupHom f g h /\
    (!y :: (h.carrier). ?!x :: (g.carrier). f x = y)
`;

(* A group homomorphism from g to g is an endomorphism. *)
val GroupEndo_def = Define `GroupEndo f g = GroupHom f g g`;

(* A group isomorphism from g to g is an automorphism. *)
val GroupAuto_def = Define `GroupAuto f g = GroupIso f g g`;

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
