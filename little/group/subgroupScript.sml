(* ------------------------------------------------------------------------- *)
(* Group Theory -- Subgroups.                                                *)
(* ------------------------------------------------------------------------- *)

(*

Subgroups
=========
Cosets
Lagrange's Theorem

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "subgroup";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory;

(* Get dependent theories in lib *)
open helperSetTheory;

(* Get dependent theories local *)
(* val _ = load "groupTheory"; *)
open groupTheory;

(* eliminate "group" from default simpset *)
val groupSS = diminish_srw_ss ["group"];

(* ------------------------------------------------------------------------- *)
(* Subgroup of a Group.                                                      *)
(* ------------------------------------------------------------------------- *)

(* A Subgroup is a subset of a group that's a group itself, keeping id, inv, mult. *)
val Subgroup_def = Define `
  Subgroup h g =
    Group h /\ Group g /\
    h.carrier SUBSET g.carrier /\
    (h.id = #e) /\
    (!x:: (h.carrier). h.inv x = |/ x) /\
    (!x y:: (h.carrier). h.mult x y = x * y)
`;

(* set overloading *)
val _ = overload_on ("<=", ``Subgroup``);

(* Theorem: subgroup property *)
(* assume h <= g, then derive all consequences of definition except carriers'
   subset relationship *)
val subgroup_property = save_thm(
  "subgroup_property",
  Subgroup_def
      |> SPEC_ALL
      |> REWRITE_RULE [ASSUME ``h:'a group <= g``]
      |> CONJUNCTS
      |> (fn thl => List.take(thl, 2) @ List.drop(thl, 3))
      |> LIST_CONJ
      |> DISCH_ALL
      |> Q.GEN `h` |> Q.GEN `g`)

(* Theorem: elements in subgroup are also in group. *)
(* Proof: Due to subgroup carrier is a subset of group carrier. *)
val subgroup_element = store_thm(
  "subgroup_element",
  ``!h g. h <= g ==> !z. z IN h.carrier ==> z IN g.carrier``,
  METIS_TAC [Subgroup_def, SUBSET_DEF]);

(* A subgroup h of g if identity is a homomorphism from g to h *)
(* val subgroup_def = Define `subgroup g h = GroupHom I g h`; -- original *)

(* Theorem: A subgroup h of g iff identity is a homomorphism from h to g. *)
val subgroup_homomorphism = store_thm(
  "subgroup_homomorphism",
  ``!g h. h <= g  = Group h /\ Group g /\ GroupHom I h g``,
  SRW_TAC [][Subgroup_def, GroupHom_def, SUBSET_DEF, EQ_IMP_THM, RES_FORALL_THM]);

(* ------------------------------------------------------------------------- *)
(* Cosets, especially cosets of a subgroup.                                  *)
(* ------------------------------------------------------------------------- *)

(* Define coset of subgroup with an element a. *)
val coset_def = Define `
  coset g X a = IMAGE (\z. g.mult a z) X
`;

(* Theorem: (coset g X a) SUBSET g.carrier *)
val COSET_PROPERTY = store_thm(
  "COSET_PROPERTY",
  ``!g a. Group g /\ a IN g.carrier ==> !X. X SUBSET g.carrier ==> (coset g X a) SUBSET g.carrier``,
  SRW_TAC [][coset_def, IMAGE_DEF, SUBSET_DEF] THEN
  METIS_TAC [group_mult_carrier]);

(* Theorem: coset g {} a = {} *)
val COSET_EMPTY = store_thm(
  "COSET_EMPTY",
  ``!g a. Group g /\ a IN g.carrier ==> (coset g {} a = {})``,
  SRW_TAC [][coset_def, IMAGE_DEF]);

(* Theorem: Group g, a IN g.carrier ==> coset g g.carrier a = g.carrier *)
(* Proof:
   By closure property of g.mult.
   This is to prove:
   (1) a * z IN g.carrier, True by group_mult_carrier.
   (2) ?z. (x = a * z) /\ z IN g.carrier, True by z = |/a*x, from group_rsolve.
*)
val GROUP_COSET_EQ_ITSELF = store_thm(
  "GROUP_COSET_EQ_ITSELF",
  ``!g a. Group g /\ a IN g.carrier ==> (coset g g.carrier a = g.carrier)``,
  SRW_TAC [][coset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier]);

(* Theorem: For x IN (coset h g.carrier a) ==> ?y IN g.carrier /\ a*y = x *)
(* Proof:
   Solving, y = |/a * x, which is IN g.carrier.
   Essentially this is to prove:
   (1) z IN h.carrier ==> ?y. y IN h.carrier /\ (a * z = a * y)
   Take y = z.
*)
val COSET_ELEMENT = store_thm(
  "COSET_ELEMENT",
  ``!g s a. a IN g.carrier ==>
           !x. x IN coset g s a ==> ?y. y IN s /\ (x = a*y)``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN METIS_TAC []);

(* Theorem: Group g, h <= g, a IN g.carrier /\ x IN coset g h.carrier a ==> x IN g.carrier *)
(* Proof:
   Coset contains all  x = a*z  where a IN g.carrier and z IN h.carrier, so x IN g.carrier by group_mult_carrier.
*)
val SUBGROUP_COSET_SUBSET = store_thm(
  "SUBGROUP_COSET_SUBSET",
  ``!g h a x.
      h <= g /\ a IN g.carrier /\ x IN coset g h.carrier a ==> x IN g.carrier``,
  SRW_TAC [][coset_def, Subgroup_def] THEN
  METIS_TAC [SUBSET_DEF, group_mult_carrier]);

(* Theorem: For all x IN h.carrier, a*x IN coset g h.carrier a. *)
(* Proof: by coset definition
   or to prove: ?z. (a * x = a * z) /\ z IN h.carrier. Take z = x.
*)
val ELEMENT_COSET_PROPERTY = store_thm(
  "ELEMENT_COSET_PROPERTY",
  ``!g s a. a IN g.carrier ==>
           !x. x IN s ==> a*x IN coset g s a``,
  SRW_TAC [][coset_def, IMAGE_DEF] THEN METIS_TAC []);

(* Theorem: For h <= g, x IN coset g h.carrier x *)
(* Proof:
   Since #e IN h.carrier, and x * #e = x.
   Essentially this is to prove:
   (1) ?z. (x = x * z) /\ z IN h.carrier, true by z = #e.
*)
val SUBGROUP_COSET_NONEMPTY = store_thm(
  "SUBGROUP_COSET_NONEMPTY",
  ``!g h x. Group g /\ h <= g /\ x IN g.carrier ==> x IN coset g h.carrier x``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rid, group_id_carrier]);

(* Theorem: For h <= g, y IN coset g h.carrier x ==> ?z IN h.carrier /\ x = y*z *)
(* Proof:
   This is to prove:
   x * z IN g.carrier /\ z IN h.carrier ==> ?z'. z' IN h.carrier /\ (x = x * z * z')
   Just take z' = |/z.
*)
val SUBGROUP_COSET_RELATE = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier /\ y IN coset g h.carrier x ==>
   ?z. z IN h.carrier /\ (x = y*z)``,
  SRW_TAC [][coset_def, IMAGE_DEF, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [group_rinv, group_rid, group_inv_carrier, group_assoc]);

(* Theorem: For h <= g, |/y * x in h.carrier ==> coset g h.carrier x = coset g h.carrier y. *)
(* Proof:
   Essentially this is to prove:
   (1) |/ y * x IN h.carrier /\ z IN h.carrier ==> ?z'. (x * z = y * z') /\ z' IN h.carrier
       Solving, z' = |/y * (x * z) = (|/y * x) * z, in h.carrier by group_mult_carrier.
   (2) |/ y * x IN h.carrier /\ z IN h.carrier ==> ?z'. (y * z = x * z') /\ z' IN h.carrier
       Solving, z' = |/x * (y * z) = (|/x * y) * z, and |/(|/y * x) = |/x * y IN h.carrier.
*)
val SUBGROUP_COSET_EQ1 = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   (|/y * x) IN h.carrier ==> (coset g h.carrier x = coset g h.carrier y)``,
  SRW_TAC [][coset_def, Subgroup_def, SUBSET_DEF, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THENL [
    `|/y * (x * z) = (|/y * x)* z` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    `|/y * (x * z) IN h.carrier` by METIS_TAC [group_mult_carrier] THEN
    `y * (|/y * (x * z)) = x * z` by METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier] THEN
    METIS_TAC [],
    `|/x * (y * z) = (|/x * y)* z` by SRW_TAC [][group_inv_carrier, group_assoc] THEN
    `|/(|/y * x) = |/x * y` by METIS_TAC [group_inv_mult, group_inv_inv, group_inv_carrier] THEN
    `|/x * (y * z) IN h.carrier` by METIS_TAC [group_mult_carrier, group_inv_carrier] THEN
    `x * (|/x * (y * z)) = y * z` by METIS_TAC [group_rsolve, group_inv_carrier, group_mult_carrier] THEN
    METIS_TAC []
  ]);

(* Theorem: For h <= g, coset g h.carrier x = coset g h.carrier y ==> |/y * x in h.carrier. *)
(* Proof:
   Since y IN coset g h.carrier y, always, by SUBGROUP_COSET_NONEMPTY.
   we have y IN coset g h.carrier x, since the cosets are equal.
   hence ?z IN h.carrier /\  x = y*z  by SUBGROUP_COSET_RELATE.
   Solving, z = |/y * x, and z IN h.carrier.
*)
val SUBGROUP_COSET_EQ2 = prove(
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   (coset g h.carrier x = coset g h.carrier y) ==> (|/y * x) IN h.carrier``,
  REPEAT STRIP_TAC THEN
  `y IN coset g h.carrier y` by SRW_TAC [][SUBGROUP_COSET_NONEMPTY] THEN
  `y IN coset g h.carrier x` by SRW_TAC [][] THEN
  `?z. z IN h.carrier /\ (x = y*z)` by SRW_TAC [][SUBGROUP_COSET_RELATE] THEN
  METIS_TAC [group_rsolve, Subgroup_def, SUBSET_DEF]);

(* Theorem: For h <= g, coset g h.carrier x = coset g h.carrier y iff |/y * x in h.carrier *)
(* Proof:
   By SUBGROUP_COSET_EQ1 and SUBGROUP_COSET_EQ2.
*)
val SUBGROUP_COSET_EQ = store_thm(
  "SUBGROUP_COSET_EQ",
  ``!g h x y. Group g /\ h <= g /\ x IN g.carrier /\ y IN g.carrier ==>
   ((coset g h.carrier x = coset g h.carrier y) <=> (|/y * x) IN h.carrier)``,
  METIS_TAC [SUBGROUP_COSET_EQ1, SUBGROUP_COSET_EQ2]);

(* Theorem: There is a bijection between subgroup and its cosets. *)
(* Proof:
   Essentially this is to prove:
   (1) x IN h.carrier ==> a * x IN coset g h.carrier a
       True by ELEMENT_COSET_PROPERTY.
   (2) x IN h.carrier /\ x' IN h.carrier /\ a * x = a * x' ==>  x = x'
       True by group_lcancel.
   (3) same as (1)
   (4) x IN coset g h.carrier a ==> ?x'. x' IN h.carrier /\ (a * x' = x)
       True by COSET_ELEMENT.
*)
val subgroup_to_coset_bij = store_thm(
  "subgroup_to_coset_bij",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==>
   BIJ (\x. a*x) h.carrier (coset g h.carrier a)``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF] THEN
  METIS_TAC [ELEMENT_COSET_PROPERTY, COSET_ELEMENT, group_lcancel, Subgroup_def, SUBSET_DEF]);

(* Theorem: All cosets of subgroup are of the same size as the subgroup *)
(* Proof:
   Due to BIJ (\x. a*x) h.carrier (coset g h.carrier a), and sets are FINITE.
*)
(* Note: An infinite group can have a finite subgroup, e.g. the units of complex multiplication. *)
val CARD_subgroup_coset = store_thm(
  "CARD_subgroup_coset",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier /\ FINITE h.carrier ==>
   (CARD (coset g h.carrier a) = CARD (h.carrier))``,
  REPEAT STRIP_TAC THEN
  `BIJ (\x. a*x) h.carrier (coset g h.carrier a)` by SRW_TAC [][subgroup_to_coset_bij] THEN
  `FINITE (coset g h.carrier a)` by METIS_TAC [coset_def, IMAGE_FINITE] THEN
  METIS_TAC [FINITE_BIJ_CARD_EQ]);

(* ------------------------------------------------------------------------- *)
(* Langrange's Theorem by Subgroups and Cosets                               *)
(* ------------------------------------------------------------------------- *)

(* Theorem: All cosets of subgroup are of the same size as the subgroup *)
(* Proof:
   Due to BIJ (\x. a*x) h.carrier (coset g h.carrier a), and sets are FINITE.
*)
(* Note: An infinite group can have a finite subgroup, e.g. the units of complex multiplication. *)
(* From CARD_subgroup_coset (or CARD_subgroup_lcoset):
   `!g h a. Group g /\ h <= g /\ a IN g.carrier /\ FINITE h.carrier ==>
   (CARD (coset g h.carrier a) = CARD (h.carrier))`

   This can be used directly to prove Lagrange's Theorem for subgroup.
*)

(* Theorem: (Lagrange Theorem) For FINITE Group g, size of subgroup divides size of group. *)
(* Proof:
   For the action g.mult h g.carrier

     CARD g.carrier
   = SIGMA CARD (TargetPartition g.mult h g.carrier)  by CARD_TARGET_BY_PARTITION
   = (CARD h.carrier) * CARD (TargetPartition g.mult h g.carrier)
           by SIGMA_CARD_CONSTANT, and (CARD e = CARD h.carrier) from CARD_subgroup_partition

   Hence (CARD h.carrier) divides (CARD g.carrier).
*)

(* Define b ~ a  when  b IN (coset g h.carrier a) *)
val inCoset_def = Define `
  inCoset g h a b = b IN (coset g h.carrier a)
`;

(* Theorem: inCoset is Reflexive:
            Group g /\ h <= g /\ a IN g.carrier ==> inCoset g h a a
   Proof:
   Follows from SUBGROUP_COSET_NONEMPTY.
*)
val INCOSET_REFL = store_thm(
  "INCOSET_REFL",
  ``!g h. Group g /\ h <= g ==> !a. a IN g.carrier ==> inCoset g h a a``,
  METIS_TAC [inCoset_def, SUBGROUP_COSET_NONEMPTY]);

(* Theorem: inCoset is Symmetric:
            Group g /\ h <= g /\ a IN g.carrier /\ b IN g.carrier
            ==> (inCoset g h a b ==> inCoset g h b a)
   Proof:
       inCoset g h a b
   ==> b IN (coset g h.carrier a)    by definition
   ==> ?z in h.carrier. b = a * z    by COSET_ELEMENT
   ==> |/z in h.carrier              by h <= g, group_inv_carrier
   ==> b * (|/z) =  (a * z) * (|/z) = a    by group_assoc, group_rinv, group_rid
   The result follows by ELEMENT_COSET_PROPERTY:
   !x. x IN h.carrier ==> b*x IN coset g h.carrier b  -- take x = |/z.
*)
val INCOSET_SYM = store_thm(
  "INCOSET_SYM",
  ``!g h. Group g /\ h <= g ==>
   !a b. a IN g.carrier /\ b IN g.carrier
       ==> (inCoset g h a b ==> inCoset g h b a)``,
  SRW_TAC [][inCoset_def] THEN
  `?z. z IN h.carrier /\ (b = a * z)` by METIS_TAC [COSET_ELEMENT] THEN
  `|/ z IN h.carrier` by METIS_TAC [Subgroup_def, group_inv_carrier] THEN
  `z IN g.carrier /\ |/ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `b * |/ z = (a * z) * (|/ z)` by SRW_TAC [][] THEN
  `_ = a * (z * |/ z)` by SRW_TAC [][group_assoc] THEN
  `_ = a` by SRW_TAC [][group_rinv, group_rid] THEN
  METIS_TAC [ELEMENT_COSET_PROPERTY]);

(* Theorem: inCoset is Transitive:
            Group g /\ h <= g /\ a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
            ==> (inCoset g h a b /\ inCoset g h b c ==> inCoset g h a c)
   Proof:
       inCoset g h a b
   ==> b IN (coset g h.carrier a)    by definition
   ==> ?y in h.carrier. b = a * y    by COSET_ELEMENT

       inCoset g h b c
   ==> c IN (coset g h.carrier b)    by definition
   ==> ?z in h.carrier. c = b * z    by COSET_ELEMENT

   Hence  c = b * z
            = (a * y)* z
            = a * (y * z)            by group_assoc
   Since y*z in h.carrier            by group_mult_carrier
   Hence  c IN (coset g h.carrier a), the result follows from ELEMENT_COSET_PROPERTY.
*)
val INCOSET_TRANS = store_thm(
  "INCOSET_TRANS",
  ``!g h. Group g /\ h <= g ==>
   !a b c. a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
       ==> (inCoset g h a b /\ inCoset g h b c ==> inCoset g h a c)``,
  SRW_TAC [][inCoset_def] THEN
  `?y. y IN h.carrier /\ (b = a * y) /\ ?z. z IN h.carrier /\ (c = b * z)` by SRW_TAC [][COSET_ELEMENT] THEN
  `y IN g.carrier /\ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `c = (a * y) * z` by SRW_TAC [][] THEN
  `_ = a * (y * z)` by SRW_TAC [][group_assoc] THEN
  `y*z IN h.carrier` by METIS_TAC [subgroup_property, group_mult_carrier] THEN
  METIS_TAC [ELEMENT_COSET_PROPERTY]);

(* Theorem: inCoset is an equivalence relation.
            Group g /\ h <= g ==> (inCoset g h) is an equivalent relation on g.carrier.
   Proof:
   By INCOSET_REFL, INCOSET_SYM, and INCOSET_TRANS.
*)
val INCOSET_EQUIV_ON_CARRIER = store_thm(
  "INCOSET_EQUIV_ON_CARRIER",
  ``!g h. Group g /\ h <= g ==> inCoset g h equiv_on g.carrier``,
  SRW_TAC [][equiv_on_def] THEN METIS_TAC [INCOSET_REFL, INCOSET_SYM, INCOSET_TRANS]);

(* Define coset partitions of g.carrier by inCoset g h. *)
val CosetPartition_def = Define `
  CosetPartition g h = partition (inCoset g h) g.carrier
`;

(* Theorem: For FINITE Group g, h <= g ==>
            CARD g.carrier = SUM of CARD partitions in (CosetPartition g h) *)
(* Proof:
   Apply partition_CARD
    |- !R s. R equiv_on s /\ FINITE s ==> (CARD s = SIGMA CARD (partition R s))
*)
val CARD_CARRIER_BY_COSET_PARTITION = store_thm(
  "CARD_CARRIER_BY_COSET_PARTITION",
  ``!g h. Group g /\ h <= g /\ FINITE g.carrier ==>
    (CARD g.carrier = SIGMA CARD (CosetPartition g h))``,
  METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, partition_CARD]);

(* Theorem: Elements in CosetPartition are cosets of some a In g.carrier *)
val COSET_PARTITION_ELEMENT = store_thm(
  "COSET_PARTITION_ELEMENT",
  ``!g h. Group g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> ?a. a IN g.carrier /\ (e = coset g h.carrier a)``,
  SIMP_TAC (srw_ss()) [CosetPartition_def, inCoset_def, partition_def] THEN
  REPEAT GEN_TAC THEN STRIP_TAC THEN GEN_TAC THEN
  DISCH_THEN (Q.X_CHOOSE_THEN `x` STRIP_ASSUME_TAC) THEN
  Q.EXISTS_TAC `x` THEN
  SRW_TAC [][coset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [Subgroup_def, SUBSET_DEF, group_mult_carrier]);

(* Theorem: For FINITE group, CARD element in CosetPartiton = CARD subgroup.
   Proof:
   By COSET_PARTITION_ELEMENT and CARD_subgroup_coset
*)
val CARD_COSET_PARTITION_ELEMENT = store_thm(
  "CARD_COSET_PARTITION_ELEMENT",
  ``!g h. FiniteGroup g /\ h <= g ==>
   !e. e IN CosetPartition g h ==> (CARD e = CARD h.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?a. a IN g.carrier /\ (e = coset g h.carrier a)` by SRW_TAC [][COSET_PARTITION_ELEMENT] THEN
  `FINITE h.carrier` by METIS_TAC [Subgroup_def, SUBSET_FINITE] THEN
  METIS_TAC [CARD_subgroup_coset]);

(* Theorem: (Lagrange Identity)
            For FINITE Group g and subgroup h,
            (size of group) = (size of subgroup) * (size of coset partition). *)
(* Proof:
   Since
   !e. e IN CosetPartition g h ==> (CARD e = CARD h.carrier)  by CARD_COSET_PARTITION_ELEMENT

   CARD g.carrier
   = SIGMA CARD (CosetPartition g h)   by CARD_CARRIER_BY_COSET_PARTITION
   = CARD h.carrier * CARD (CosetPartition g h)  by SIGMA_CARD_CONSTANT
*)
val LAGRANGE_IDENTITY = store_thm(
  "LAGRANGE_IDENTITY",
  ``!g h. FiniteGroup g /\ h <= g ==>
   (CARD g.carrier = CARD h.carrier * CARD (CosetPartition g h))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `FINITE (CosetPartition g h)` by METIS_TAC [CosetPartition_def, INCOSET_EQUIV_ON_CARRIER, FINITE_partition] THEN
  METIS_TAC [CARD_CARRIER_BY_COSET_PARTITION, SIGMA_CARD_CONSTANT, CARD_COSET_PARTITION_ELEMENT, FiniteGroup_def]);

(* Theorem: (Coset Partition size)
            For FINITE Group g, size of coset partition = (size of group) div (size of subgroup). *)
(* Proof:
   By LAGRANGE_IDENTITY and MULT_DIV.
*)
val CARD_COSET_PARTITION = store_thm(
  "CARD_COSET_PARTITION",
  ``!g h. FiniteGroup g /\ h <= g ==>
   (CARD (CosetPartition g h) = CARD g.carrier DIV CARD h.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `Group h /\ FINITE h.carrier` by METIS_TAC [Subgroup_def, SUBSET_FINITE] THEN
  `0 < CARD h.carrier` by METIS_TAC [group_id_carrier, MEMBER_NOT_EMPTY, CARD_EQ_0, arithmeticTheory.NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [FiniteGroup_def, LAGRANGE_IDENTITY, arithmeticTheory.MULT_DIV, arithmeticTheory.MULT_SYM]);

(* Theorem: (Lagrange Theorem)
            For FINITE Group g, size of subgroup divides size of group. *)
(* Proof:
   By LAGRANGE_IDENTITY and divides_def.
*)
val LAGRANGE_THM = store_thm(
  "LAGRANGE_THM",
  ``!g h. FiniteGroup g /\ h <= g ==> divides (CARD h.carrier) (CARD g.carrier)``,
  METIS_TAC [LAGRANGE_IDENTITY, arithmeticTheory.MULT_SYM, dividesTheory.divides_def]);

(* ------------------------------------------------------------------------- *)
(* Alternate proof without using inCoset.                                    *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Subgroup Coset membership is Symmetric:
            Group g /\ h <= g /\ a IN g.carrier /\ b IN g.carrier
            ==> b IN coset g h.carrier a ==> a IN coset g h.carrier b
   Proof:
       b IN (coset g h.carrier a)
   ==> ?z in h.carrier. b = a * z    by COSET_ELEMENT
   ==> |/z in h.carrier              by h <= g, group_inv_carrier
   ==> b * (|/z) =  (a * z) * (|/z) = a    by group_assoc, group_rinv, group_rid
   The result follows by ELEMENT_COSET_PROPERTY:
   !x. x IN h.carrier ==> b*x IN coset g h.carrier b  -- take x = |/z.
*)
val SUBGROUP_COSET_SYM = store_thm(
  "SUBGROUP_COSET_SYM",
  ``!g h. Group g /\ h <= g ==>
   !a b. a IN g.carrier /\ b IN g.carrier
       ==> b IN (coset g h.carrier a) ==> a IN (coset g h.carrier b)``,
  REPEAT STRIP_TAC THEN
  `?z. z IN h.carrier /\ (b = a * z)` by METIS_TAC [COSET_ELEMENT] THEN
  `|/ z IN h.carrier` by METIS_TAC [Subgroup_def, group_inv_carrier] THEN
  `z IN g.carrier /\ |/ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `b * |/ z = (a * z) * (|/ z)` by SRW_TAC [][] THEN
  `_ = a * (z * |/ z)` by SRW_TAC [][group_assoc] THEN
  `_ = a` by SRW_TAC [][group_rinv, group_rid] THEN
  METIS_TAC [ELEMENT_COSET_PROPERTY]);

(* Theorem: Subgroup Coset membership is Transitive:
            Group g /\ h <= g /\ a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
            ==> b IN (coset g h.carrier a) /\ c IN (coset g h.carrier b) ==> c IN (coset g h.carrier a)
   Proof:
       b IN (coset g h.carrier a)    by definition
   ==> ?y in h.carrier. b = a * y    by COSET_ELEMENT
       c IN (coset g h.carrier b)    by definition
   ==> ?z in h.carrier. c = b * z    by COSET_ELEMENT

   Hence  c = b * z
            = (a * y)* z
            = a * (y * z)            by group_assoc
   Since y*z in h.carrier            by group_mult_carrier
   Hence  c IN (coset g h.carrier a), the result follows from ELEMENT_COSET_PROPERTY.
*)
val SUBGROUP_COSET_TRANS = store_thm(
  "SUBGROUP_COSET_TRANS",
  ``!g h. Group g /\ h <= g ==>
   !a b c. a IN g.carrier /\ b IN g.carrier /\ c IN g.carrier
       ==> b IN (coset g h.carrier a) /\ c IN (coset g h.carrier b) ==> c IN (coset g h.carrier a)``,
  REPEAT STRIP_TAC THEN
  `?y. y IN h.carrier /\ (b = a * y) /\
   ?z. z IN h.carrier /\ (c = b * z)` by SRW_TAC [][COSET_ELEMENT] THEN
  `y IN g.carrier /\ z IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `c = (a * y) * z` by SRW_TAC [][] THEN
  `_ = a * (y * z)` by SRW_TAC [][group_assoc] THEN
  `y*z IN h.carrier` by METIS_TAC [subgroup_property, group_mult_carrier] THEN
  METIS_TAC [ELEMENT_COSET_PROPERTY]);

(* Theorem: inCoset is an equivalence relation.
            Group g /\ h <= g ==> (inCoset g h) is an equivalent relation on g.carrier.
   Proof:
   By SUBGROUP_COSET_NONEMPTY, SUBGROUP_COSET_SYM, and SUBGROUP_COSET_TRANS.
*)
val SUBGROUP_INCOSET_EQUIV = store_thm(
  "SUBGROUP_INCOSET_EQUIV",
  ``!g h. Group g /\ h <= g ==> (coset g h.carrier) equiv_on g.carrier``,
  SRW_TAC [][equiv_on_def] THEN1
  METIS_TAC [SUBGROUP_COSET_NONEMPTY, SPECIFICATION] THEN1
  METIS_TAC [SUBGROUP_COSET_SYM, SPECIFICATION] THEN
  METIS_TAC [SUBGROUP_COSET_TRANS, SPECIFICATION]);

(* Theorem: For FINITE Group g, h <= g ==>
            CARD g.carrier = SUM of CARD partitions by (coset g h.carrier) *)
(* Proof:
   Apply partition_CARD
    |- !R s. R equiv_on s /\ FINITE s ==> (CARD s = SIGMA CARD (partition R s))
*)
val CARD_CARRIER_BY_SUBGROUP_COSET_PARTITION = store_thm(
  "CARD_CARRIER_BY_SUBGROUP_COSET_PARTITION",
  ``!g h. Group g /\ h <= g /\ FINITE g.carrier ==>
    (CARD g.carrier = SIGMA CARD (partition (coset g h.carrier) g.carrier))``,
  METIS_TAC [SUBGROUP_INCOSET_EQUIV, partition_CARD]);

(* Theorem: Elements in coset partition are cosets of some a In g.carrier *)
val SUBGROUP_COSET_PARTITION_ELEMENT = store_thm(
  "SUBGROUP_COSET_PARTITION_ELEMENT",
  ``!g h. Group g /\ h <= g ==>
   !e. e IN (partition (coset g h.carrier) g.carrier) ==> ?a. a IN g.carrier /\ (e = coset g h.carrier a)``,
  SIMP_TAC (srw_ss())[partition_def] THEN REPEAT GEN_TAC THEN STRIP_TAC THEN
  GEN_TAC THEN
  DISCH_THEN (Q.X_CHOOSE_THEN `x` STRIP_ASSUME_TAC) THEN
  Q.EXISTS_TAC `x` THEN
  ASM_SIMP_TAC (srw_ss())[EXTENSION, EQ_IMP_THM] THEN
  Q.X_GEN_TAC `x'` THEN SRW_TAC[][SPECIFICATION] THEN
  `x' IN coset g h.carrier x` by METIS_TAC [SPECIFICATION] THEN
  `x' IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF, COSET_PROPERTY] THEN
  METIS_TAC [SPECIFICATION]);

(* Theorem: For FINITE group, CARD element in subgroup coset partiton = CARD subgroup.
   Proof:
   By SUBGROUP_COSET_PARTITION_ELEMENT and CARD_subgroup_coset
*)
val CARD_SUBGROUP_COSET_PARTITION_ELEMENT = store_thm(
  "CARD_SUBGROUP_COSET_PARTITION_ELEMENT",
  ``!g h. FiniteGroup g /\ h <= g ==>
   !e. e IN (partition (coset g h.carrier) g.carrier) ==> (CARD e = CARD h.carrier)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?a. a IN g.carrier /\ (e = coset g h.carrier a)` by SRW_TAC [][SUBGROUP_COSET_PARTITION_ELEMENT] THEN
  `FINITE h.carrier` by METIS_TAC [Subgroup_def, SUBSET_FINITE] THEN
  METIS_TAC [CARD_subgroup_coset]);

(* Theorem: (Lagrange Identity)
            For FINITE Group g and subgroup h,
            (size of group) = (size of subgroup) * (size of coset partition). *)
(* Proof:
   Since
   !e. e IN coset partition g h ==> (CARD e = CARD h.carrier)  by CARD_SUBGROUP_COSET_PARTITION_ELEMENT

   CARD g.carrier
   = SIGMA CARD (CosetPartition g h)   by CARD_CARRIER_BY_SUBGROUP_COSET_PARTITION
   = CARD h.carrier * CARD (CosetPartition g h)  by SIGMA_CARD_CONSTANT
*)
val LAGRANGE_IDENTITY2 = store_thm(
  "LAGRANGE_IDENTITY2",
  ``!g h. FiniteGroup g /\ h <= g ==>
   (CARD g.carrier = CARD h.carrier * CARD (partition (coset g h.carrier) g.carrier))``,
  SRW_TAC [][FiniteGroup_def] THEN
  `FINITE (partition (coset g h.carrier) g.carrier)` by METIS_TAC [partition_FINITE] THEN
  METIS_TAC [CARD_CARRIER_BY_SUBGROUP_COSET_PARTITION,
             CARD_SUBGROUP_COSET_PARTITION_ELEMENT,
             SIGMA_CARD_CONSTANT, FiniteGroup_def]);

(* ------------------------------------------------------------------------- *)
(* Application of Finite Group element order:                                *)
(* The generated subgroup of by a group element.                             *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The Subgroup <a> of any element a of Group g.                             *)
(* ------------------------------------------------------------------------- *)

(* Define the generator group, the exponential group of an element a of group g *)
val Generated_def = Define`
  Generated g a =
    <| carrier := {x | ?k. x = a**k };
            id := g.id;
           inv := |/ ;
          mult := g.mult
     |>`;

(* Theorem: For a FINITE group g, the generated group of a in g.carrier is a group *)
(* For now, FINITE is required to have the order m, giving the inverse *)
(* In general, |/a = a**-1, but that would require exponents to be :int, not :num *)
(* Proof:
   There is m = order g a with the property a**m = #e.
   This is to prove:
   (1) ?k. g.id = a ** k                   by group_exp_def, a**0 = #e.
   (2) ?k''. a ** k * a ** k' = a ** k''   by group_exp_add
   (3) ?k'. |/ (a ** k) = a ** k'          by |/a = a**(m-1)
       and |/a**k = (|/a)**k = (a**(m-1))**k = a**((m-1)*k).
                                           by group_order_inv, group_exp_inv, group_exp_mult
   (4) g.id * a ** k = a ** k              by group_lid
   (5) |/ (a ** k) * a ** k = g.id         by |/(a**k) = (|/a)**k  by group_exp_inv,
                                           then group_mult_exp since a*|/a = |/a*a
   (6) a ** k * a ** k' * a ** k'' = a ** k * (a ** k' * a ** k'')  by group_assoc
*)
val Generated_group = store_thm(
  "Generated_group",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> Group (Generated g a)``,
  SRW_TAC [][FiniteGroup_def] THEN
  SRW_TAC [][Group_def, Generated_def, RES_FORALL_THM] THENL [
    METIS_TAC [group_exp_def],
    METIS_TAC [group_exp_add],
    METIS_TAC [FiniteGroup_def, group_order_inv, group_exp_inv, group_exp_mult, group_inv_carrier],
    METIS_TAC [group_lid, group_exp_carrier],
    METIS_TAC [group_exp_inv, group_linv, group_rinv, group_mult_exp, group_inv_carrier, group_id_exp],
    SRW_TAC [][group_assoc, group_exp_carrier]
  ]);

(* Theorem: The generated group <a> for a IN G is subgroup of G. *)
(* Proof:
   Essentially this is to prove:
   (1) Group (Generated g a)                      true by Generated_group.
   (2) (Generated g a).carrier SUBSET g.carrier   true by definitions and SUBSET_DEF.
   (3) (Generated g a).id = g.id                  true by definition
   (4) (Generated g a).inv = g.inv                true by definition
   (5) (Generated g a).mult = g.mult              true by definition
*)
val Generated_subgroup = store_thm(
  "Generated_subgroup",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> Subgroup (Generated g a) g``,
  SRW_TAC [][FiniteGroup_def, Subgroup_def] THEN1
  SRW_TAC [][FiniteGroup_def, Generated_group] THEN1
  (SRW_TAC [][Generated_def, SUBSET_DEF] THEN METIS_TAC [group_exp_carrier]) THEN
  SRW_TAC [][Generated_def]);

(* Lemma: There is a bijection from (count m) to (Generated g a), where m = order g x *)
(* Proof:
   The map (\n. a**n) from (count m) to (Generated g a) is bijective:
   (1) surjective because x in (Generated g a) means ?k. x = a**k = a**(k mod m), so take n=k mod m.
       This is group_exp_mod.
   (2) injective because a**m = a**n ==> m = n, otherwise a**(m-n) = #e, contradicting minimal nature of m.
       This is group_order_unique.

   Essentially this is to prove:
   (1) a IN g.carrier /\ n < order g a ==> ?k. a ** n = a ** k,         just take k = n.
   (2) n < order g a /\ n' < order g a /\ a ** n = a ** n' ==> n = n',  true by group_order_unique
   (3) same as (1)
   (4) a IN g.carrier ==> ?n. n < order g a /\ (a ** n = a ** k),       true by group_exp_mod, n = k mod order.
*)
val group_order_to_generated_bij = prove(
  ``!g a. FiniteGroup g /\ a IN g.carrier ==>
    BIJ (\n. a**n) (count (order g a)) (Generated g a).carrier``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF, Generated_def] THEN1
  METIS_TAC [] THEN1
  METIS_TAC [group_order_unique] THEN1
  METIS_TAC [] THEN1
  METIS_TAC [group_exp_mod, group_order_property, arithmeticTheory.MOD_LESS]);

(* Theorem: The order of the Generated_subgroup is the order of its element *)
(* Proof:
   By BIJ (\n. a**n) (count (order g a)) (Generated g a).carrier,
   It follows that CARD (Generated g a).carrier = order g a.
*)
val CARD_generated_subgroup = store_thm(
  "CARD_generated_subgroup",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> (CARD (Generated g a).carrier = order g a)``,
  METIS_TAC [Generated_subgroup, Subgroup_def, FiniteGroup_def, SUBSET_DEF, SUBSET_FINITE,
             group_order_to_generated_bij, FINITE_BIJ_CARD_EQ, FINITE_COUNT, CARD_COUNT]);

(* Theorem: For FiniteGroup g, a IN g.carrier ==> a**(CARD g) = #e *)
(* Proof:
   Subgroup (Generated g a) g                  by Generated_subgroup
   CARD (Generated g a).carrier = order g a    by CARD_generated_subgroup
   a ** (order g a) = #e                       by group_order_property

   divides (order g a) (CARD g.carrier)   by LAGRANGE_THM
   ?k. CARD g.carrier = (order g a) * k   by arithmetic

     a ** (CARD g.carrier)
   = a ** ((order g a) * k)
   = (a ** (order g a))** k               by group_exp_mult
   = (#e)**k                              by group_order_property
   = #e                                   by group_id_exp
*)
val FINITE_GROUP_FERMAT = store_thm(
  "FINITE_GROUP_FERMAT",
  ``!g a. FiniteGroup g /\ a IN g.carrier ==> (a**(CARD g.carrier) = #e)``,
  REPEAT STRIP_TAC THEN
  `Subgroup (Generated g a) g` by SRW_TAC [][Generated_subgroup] THEN
  `CARD (Generated g a).carrier = order g a` by SRW_TAC [][CARD_generated_subgroup] THEN
  `divides (order g a) (CARD g.carrier)` by METIS_TAC [LAGRANGE_THM] THEN
  `?k. CARD g.carrier = (order g a) * k` by METIS_TAC [dividesTheory.divides_def, arithmeticTheory.MULT_SYM] THEN
  `a ** (CARD g.carrier) = a ** ((order g a) * k)` by SRW_TAC [][] THEN
  METIS_TAC [FiniteGroup_def, group_exp_mult, group_order_property, group_id_exp]);

(* ------------------------------------------------------------------------- *)
(* The GROUP ITSET theorems, fold of g.mult, especially for Abelian Groups.  *)
(* ------------------------------------------------------------------------- *)

(* Define ITSET for Group *)
val _ = overload_on("GITSET", ``\g s b. ITSET g.mult s b``)

fun gINST th = th |> SPEC_ALL |> INST_TYPE [beta |-> alpha]
                  |> Q.INST [`f` |-> `g.mult`]
val GITSET_THM = save_thm("GITSET_THM", gINST ITSET_THM)


(* Theorem: GITSET {} b = b *)
val GITSET_EMPTY = save_thm("GITSET_EMPTY", gINST ITSET_EMPTY)

(* Theorem: GITSET g (x INSERT s) b = GITSET g (REST (x INSERT s)) ((CHOICE (x INSERT s)) * b) *)
(* Proof:
   By GITSET_THM, since x INSERT s is non-empty.
*)
val GITSET_INSERT = save_thm(
  "GITSET_INSERT",
  gINST (ITSET_INSERT |> SPEC_ALL |> UNDISCH) |> DISCH_ALL);

(* Theorem: [simplified GITSET_INSERT]
            s <> {} ==> GITSET g s b = GITSET g (REST s) ((CHOICE s) * b) *)
(* Proof:
   By GITSET_INSERT, since CHOICE s IN s, so (CHOICE s) INSERT s = s.
*)
val GITSET_PROPERTY = store_thm(
  "GITSET_PROPERTY",
  ``!g s. FINITE s /\ s <> {} ==>
          !b. GITSET g s b = GITSET g (REST s) ((CHOICE s) * b)``,
  REPEAT STRIP_TAC THEN
  `(CHOICE s) INSERT s = s` by METIS_TAC [CHOICE_DEF, ABSORPTION] THEN
  METIS_TAC [GITSET_INSERT]);

(* Theorem: AbelianGroup g ==>
            GITSET g (x INSERT s) b = GITSET g (s DELETE x) (x * b) *)
(* Proof:
   By complete induction of CARD s.
   Applying GITSET_INSERT to LHS, this is to prove:
   GITSET g t (y * b) = GITSET g (s DELETE x) (b * x)
            |  |
            |  y = CHOICE (x INSERT s)
            +- t = REST (x INSERT s)
   Case x IN s:
       then x INSERT s = s.
       y = CHOICE s, t = REST s
       Since s = CHOICE s INSERT REST s = y INSERT t,
       Hence t = s DELETE y.
       Since CARD t < CARD s, apply induction:
       GITSET g (y INSERT t) b = GITSET g (t DELETE y) (b * y).
       Compare to LHS of goal, we want to prove:
       GITSET g (t DELETE y) (b * y) = GITSET g (s DELETE x) (b * x)
       If x = y, this is trivial.
       If x <> y,
       Let t = REST s = x INSERT u.
       Now s = x INSERT (y INSERT u) = x INSERT z, where z = y INSERT u, and x NOTIN z.
       Therefore (s DELETE x) = z.
       Since CARD z < CARD s, apply induction:
       GITSET g (x INSERT z) b = GITSET g (z DELETE x) (b * x)
       LHS = GITSET g s b
           = GITSET g (x INSERT z) b             by s = x INSERT z
           = GITSET g (z DELETE x) (b * x)       by induction
           = GITSET g z (b * x)                  by x NOTIN z
           = GITSET g (s DELETE x) (b * x)       by z = s DELETE x
           = RHS

   Case x NOTIN s:
       then s DELETE x = x.
       To prove: GITSET g (x INSERT s) b = GITSET g s (b * x)
       The CHOICE/REST of (x INSERT s) cannot be simplified, but can be replaced by:
         y INSERT t
       = CHOICE (x INSERT s) INSERT (REST (x INSERT s)) by CHOICE_INSERT_REST
       = x INSERT s     since (x INSERT s) <> {} by NOT_EMPTY_INSERT.
       LHS = GITSET g (x INSERT s) b
           = GITSET g (REST (x INSERT s)) (CHOICE (x INSERT s) * b)  by GITSET_PROPERTY
           = GITSET g t (y * b)        by t = REST (x INSERT s), y = CHOICE (x INSERT s)

       If the choice y = x, then t = s.  t DELETE y = s DELETE x = s, since x NOTIN s.
       The goal reduces to: GITSET g t (y * b) = GITSET g s (b * x),
       which is trivial by commutative g.mult.

       If the choice y <> x,
       Then x IN t and y IN s, since x INSERT s = y INSERT t and x <> y.
       Let s = y INSERT u, or u = s DELETE y, and y NOTIN u.
       Also t = REST (x INSERT s) = x INSERT u.

       Now CARD u < CARD s, so apply induction:
       GITSET g (x INSERT u) b' = GITSET g (u DELETE x) (b' * x)

       LHS = GITSET g (x INSERT s) b
           = GITSET g t (y * b)               by t = REST (x INSERT s), y = CHOICE (x INSERT s)
           = GITSET g (x INSERT u) (y * b)    by t = x INSERT u
           = GITSET g (u DELETE x) (x * (y * b))   by induction
           = GITSET g u (x * (y * b))              by x NOTIN u
       RHS = GITSET g s (b * x)
           = GITSET g (y INSERT u) (b * x)         by s = y INSERT u
           = GITSET g (u DELETE y) (y * (b * x))   by induction hypothesis
           = GITSET g u (y * (b * x))              by y NOTIN u
       Applying the commuting property of g.mult, and associativity, LHS = RHS.
*)
val COMMUTING_GITSET_INSERT = store_thm(
  "COMMUTING_GITSET_INSERT",
  ``!g s. AbelianGroup g /\ FINITE s /\ s SUBSET g.carrier ==>
          !b x::(g.carrier).
               GITSET g (x INSERT s) b = GITSET g (s DELETE x) (x * b)``,
  completeInduct_on `CARD s` THEN
  RULE_ASSUM_TAC
      (SIMP_RULE bool_ss [GSYM RIGHT_FORALL_IMP_THM, AND_IMP_INTRO]) THEN
  SRW_TAC [][GITSET_INSERT, RES_FORALL_THM] THEN
  FULL_SIMP_TAC (srw_ss()) [AbelianGroup_def] THEN
  Q.ABBREV_TAC `t = REST (x INSERT s)` THEN
  Q.ABBREV_TAC `y = CHOICE (x INSERT s)` THEN
  `y NOTIN t` by METIS_TAC [CHOICE_NOT_IN_REST] THEN
  `!x s. x IN s ==> (x INSERT s = s)` by SRW_TAC [][ABSORPTION] THEN
  `!x s. x NOTIN s ==> (s DELETE x = s)` by SRW_TAC [][DELETE_NON_ELEMENT] THEN
  Cases_on `x IN s` THENL [
    FULL_SIMP_TAC (srw_ss()) [] THEN
    `s = y INSERT t` by METIS_TAC [NOT_IN_EMPTY, CHOICE_INSERT_REST] THEN
    `t = s DELETE y` by METIS_TAC [DELETE_INSERT] THEN
    `FINITE s /\ FINITE t` by METIS_TAC [REST_SUBSET, SUBSET_FINITE] THEN
    `CARD s = SUC(CARD t)` by METIS_TAC [CARD_INSERT] THEN
    `CARD t < CARD s` by RW_TAC arith_ss [] THEN
    `t SUBSET g.carrier` by METIS_TAC [DELETE_SUBSET, SUBSET_TRANS] THEN
    `s <> {}` by METIS_TAC [NOT_INSERT_EMPTY] THEN
    `y IN g.carrier` by METIS_TAC [CHOICE_DEF, SUBSET_DEF] THEN
    `GITSET g (y INSERT t) b = GITSET g (t DELETE y) (b * y)` by SRW_TAC [][] THEN
    Cases_on `x = y` THEN1
    METIS_TAC [] THEN
    `x IN t` by METIS_TAC [IN_INSERT] THEN
    Q.ABBREV_TAC `u = t DELETE x` THEN
    `t = x INSERT u` by SRW_TAC [][INSERT_DELETE, Abbr`u`] THEN
    `x NOTIN u` by METIS_TAC [IN_DELETE] THEN
    Q.ABBREV_TAC `z = y INSERT u` THEN
    `s = x INSERT z` by SRW_TAC [][INSERT_COMM, Abbr `z`] THEN
    `x NOTIN z` by SRW_TAC [][Abbr `z`] THEN
    `FINITE z` by METIS_TAC [FINITE_INSERT] THEN
    `CARD s = SUC(CARD z)` by METIS_TAC [CARD_INSERT] THEN
    `CARD z < CARD s` by RW_TAC arith_ss [] THEN
    `z SUBSET g.carrier` by METIS_TAC [INSERT_SUBSET, SUBSET_TRANS] THEN
    `GITSET g (x INSERT z) b = GITSET g (z DELETE x) (b * x)` by SRW_TAC [][] THEN
    `s DELETE x = z` by SRW_TAC [][DELETE_INSERT, Abbr `z`] THEN
    `z = s DELETE x` by SRW_TAC [][] THEN
    METIS_TAC [],
    SRW_TAC [][] THEN
    `y INSERT t = x INSERT s` by METIS_TAC [NOT_EMPTY_INSERT, CHOICE_INSERT_REST] THEN
    `x INSERT s <> {}` by SRW_TAC [][MEMBER_NOT_EMPTY] THEN
    `GITSET g (x INSERT s) b = GITSET g t (y * b)` by SRW_TAC [][GITSET_PROPERTY, FiniteGroup_def] THEN
    Cases_on `x = y` THENL [
      `t = s` by METIS_TAC [DELETE_INSERT] THEN
      METIS_TAC [],
      `x IN t /\ y IN s` by METIS_TAC [IN_INSERT] THEN
      Q.ABBREV_TAC `u = s DELETE y` THEN
      `s = y INSERT u` by SRW_TAC [][INSERT_DELETE, Abbr`u`] THEN
      `y NOTIN u` by METIS_TAC [IN_DELETE] THEN
      `t = x INSERT u` by FULL_SIMP_TAC (srw_ss()) [EXTENSION, IN_INSERT] THEN1 METIS_TAC [] THEN
      `x NOTIN u` by METIS_TAC [IN_INSERT] THEN
      `FINITE u` by METIS_TAC [FINITE_DELETE, SUBSET_FINITE] THEN
      `CARD u < CARD s` by SRW_TAC [][] THEN
      `u SUBSET g.carrier` by METIS_TAC [DELETE_SUBSET, SUBSET_TRANS] THEN
      `y IN g.carrier` by METIS_TAC [CHOICE_DEF, SUBSET_DEF] THEN
      `y*b IN g.carrier /\ b*x IN g.carrier` by SRW_TAC [][group_mult_carrier] THEN
      `GITSET g t (y * b) = GITSET g (x INSERT u) (y * b)` by SRW_TAC [][] THEN
      `_ = GITSET g (u DELETE x) (x * (y * b))` by SRW_TAC [][] THEN
      `_ = GITSET g u (x * (y * b))` by SRW_TAC [][] THEN
      `GITSET g s (b * x) = GITSET g (y INSERT u) (b * x)` by SRW_TAC [][] THEN
      `_ = GITSET g (u DELETE y) (y * (b * x))` by SRW_TAC [][] THEN
      `_ = GITSET g u (y * (b * x))` by SRW_TAC [][] THEN
      METIS_TAC [group_assoc]
    ]
  ]);

(* Theorem: FiniteAbelianGroup g ==> GITSET g s (x * b) = x * (GITSET g s b) *)
(* Proof:
   This is to prove: GITSET g s (x * b) = x * GITSET g s b
   The case s = {} is trivial by GITSET_EMPTY.
   When s <> {}, let s = y INSERT t, where y = CHOICE s, t = REST s.
   By COMMUTING_GITSET_INSERT;
   GITSET g (x INSERT s) b = GITSET g (s DELETE x) (x * b)
     LHS = GITSET g s (x * b)
         = GITSET g (y INSERT t) (x * b)        by s = y INSERT t
         = GITSET g (t DELETE y) (y * (x * b))  by COMMUTING_GITSET_INSERT
         = GITSET g t (y * (x * b))             by y NOTIN t and DELETE_NON_ELEMENT
         = y * (GITSET g t (x * b))             by inductive assumption, CARD t < CARD s
         = y * (x * (GITSET g t b))             by inductive assumption again, CARD t < CARD s
     RHS = x * (GITSET g s b)
         = x * (GITSET g (y INSERT t) b)        by s = y INSERT t
         = x * (GITSET g (t DELETE y) (y * b))  by COMMUTING_GITSET_INSERT
         = x * (GITSET g t (y * b))             by y NOTIN t and DELETE_NON_ELEMENT
         = x * (y * (GITSET g t b))             by inductive assumption, CARD t < CARD s
         = LHS                                  by commuting property of g.mult and group_assoc.
*)
val COMMUTING_GITSET_REDUCTION = store_thm(
  "COMMUTING_GITSET_REDUCTION",
  ``!g s. AbelianGroup g /\ FINITE s /\ s SUBSET g.carrier ==>
   !b x::(g.carrier). GITSET g s (x * b) = x * (GITSET g s b)``,
  completeInduct_on `CARD s` THEN
  RULE_ASSUM_TAC
      (SIMP_RULE bool_ss [GSYM RIGHT_FORALL_IMP_THM, AND_IMP_INTRO]) THEN
  SRW_TAC [][RES_FORALL_THM] THEN
  `Group g` by METIS_TAC [AbelianGroup_def] THEN
  Cases_on `s = {}` THEN1
    METIS_TAC [GITSET_EMPTY, group_mult_carrier] THEN
  `?y t. (y = CHOICE s) /\ (t = REST s) /\ (s = y INSERT t)` by SRW_TAC [][CHOICE_DEF, REST_DEF] THEN
  `y NOTIN t` by METIS_TAC [CHOICE_NOT_IN_REST] THEN
  `y IN g.carrier` by METIS_TAC [CHOICE_DEF, SUBSET_DEF] THEN
  `x * b IN g.carrier /\ y * b IN g.carrier` by SRW_TAC [][group_mult_carrier] THEN
  `t SUBSET g.carrier` by METIS_TAC [REST_SUBSET, SUBSET_TRANS] THEN
  `FINITE t` by METIS_TAC [REST_SUBSET, SUBSET_FINITE] THEN
  `CARD s = SUC(CARD t)` by METIS_TAC [CARD_INSERT] THEN
  `CARD t < CARD s` by RW_TAC arith_ss [] THEN
  `GITSET g s (x * b) = GITSET g (y INSERT t) (x * b)` by SRW_TAC [][] THEN
  `_ = GITSET g (t DELETE y) (y * (x * b))` by SRW_TAC [][COMMUTING_GITSET_INSERT] THEN
  `_ = GITSET g t (y * (x * b))` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `_ = y * (GITSET g t (x * b))` by SRW_TAC [][] THEN
  `_ = y * (x * (GITSET g t b))` by SRW_TAC [][] THEN
  `x * (GITSET g s b) = x * (GITSET g (y INSERT t) b)` by SRW_TAC [][] THEN
  `_ = x * (GITSET g (t DELETE y) (y * b))` by SRW_TAC [][COMMUTING_GITSET_INSERT] THEN
  `_ = x * (GITSET g t (y * b))` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `_ = x * (y * (GITSET g t b))` by SRW_TAC [][] THEN
  METIS_TAC [group_assoc, AbelianGroup_def]);

(* Theorem: FiniteAbelianGroup g ==>
            GITSET g (x INSERT s) b = x * (GITSET g (s DELETE x) b) *)
(* Proof:
   LHS = GITSET g (x INSERT s) b
       = GITSET g (s DELETE x) (x * b)   by COMMUTING_GITSET_INSERT
       = x * (GITSET g (s DELETE x) b)   by COMMUTING_GITSET_REDUCTION
       = RHS
*)
val COMMUTING_GITSET_RECURSES = store_thm(
  "COMMUTING_GITSET_RECURSES",
  ``!g s. AbelianGroup g /\ FINITE s /\ s SUBSET g.carrier ==>
   !b x::(g.carrier). GITSET g (x INSERT s) b = x * (GITSET g (s DELETE x) b)``,
  SRW_TAC [][RES_FORALL_THM] THEN
  `Group g` by METIS_TAC [AbelianGroup_def] THEN
  `x*b IN g.carrier` by SRW_TAC [][group_mult_carrier] THEN
  `(s DELETE x) SUBSET g.carrier` by METIS_TAC [DELETE_SUBSET, SUBSET_TRANS] THEN
  METIS_TAC [COMMUTING_GITSET_INSERT, COMMUTING_GITSET_REDUCTION, FINITE_DELETE]);

(* ------------------------------------------------------------------------- *)
(* The GROUP PROD_SET, especially for Abelian Groups.                        *)
(* ------------------------------------------------------------------------- *)

(* Define GPROD_SET via GITSET *)
val GPROD_SET_def = Define `GPROD_SET g s = GITSET g s #e`;

(* Theorem: property of GPROD_SET *)
(* Proof:
   This is to prove:
   (1) GITSET g {} g.id = g.id
       True by GITSET_EMPTY, and group_id_carrier.
   (2) GITSET g (x INSERT s) g.id = x * GITSET g (s DELETE x) g.id
       True by COMMUTING_GITSET_RECURSES, and group_id_carrier.
*)
val GPROD_SET_THM = store_thm(
  "GPROD_SET_THM",
  ``!g s. FiniteAbelianGroup g /\ s SUBSET g.carrier ==>
   (GPROD_SET g {} = #e) /\
   (!x::(g.carrier). GPROD_SET g (x INSERT s) = x * GPROD_SET g (s DELETE x))``,
  SRW_TAC [][GPROD_SET_def, RES_FORALL_THM, GITSET_EMPTY,
             FiniteAbelianGroup_def, FiniteGroup_def] THEN
  `FINITE s` by METIS_TAC [SUBSET_FINITE] THEN
  METIS_TAC [COMMUTING_GITSET_RECURSES, group_id_carrier]);

(* Theorem: GPROD_SET g {} = #e *)
(* Proof: by GPROD_SET_THM. *)
val GPROD_SET_EMPTY = store_thm(
  "GPROD_SET_EMPTY",
  ``!g s. FiniteAbelianGroup g /\ s SUBSET g.carrier ==> (GPROD_SET g {} = #e)``,
  METIS_TAC [GPROD_SET_THM]);

(* Theorem: For Group g, a IN g.carrier ==>
            GPROD_SET g coset g g.carrier a = GPROD_SET g g.carrier *)
(* Proof:
   This is trivial by GROUP_COSET_EQ_ITSELF.
*)
val GPROD_SET_IMAGE = store_thm(
  "GPROD_SET_IMAGE",
  ``!g a. Group g /\ a IN g.carrier ==>
   (GPROD_SET g (coset g g.carrier a) = GPROD_SET g g.carrier)``,
  METIS_TAC [GROUP_COSET_EQ_ITSELF]);

(* Theorem: GPROD_SET g s IN g.carrier *)
(* Proof:
   By complete induction on CARD s.
   Case s = {},
       Then GPROD_SET g {} = #e    by GPROD_SET_THM
       and #e IN g.carrier         by group_id_carrier
   Case s <> {},
       Let x = CHOICE s, t = REST s, s = x INSERT t, x NOTIN t.
         GPROD_SET g s
       = GPROD_SET g (x INSERT t)       by s = x INSERT t
       = x * GPROD_SET g (t DELETE x)   by GPROD_SET_THM
       = x * GPROD_SET g t              by DELETE_NON_ELEMENT, x NOTIN t
       IN g.carrier                     by induction, and group_mult_carrier.
*)
val GPROD_SET_PROPERTY = store_thm(
  "GPROD_SET_PROPERTY",
  ``!g s. FiniteAbelianGroup g /\ s SUBSET g.carrier ==> GPROD_SET g s IN g.carrier``,
  completeInduct_on `CARD s` THEN
  POP_ASSUM (ASSUME_TAC o SIMP_RULE bool_ss [GSYM RIGHT_FORALL_IMP_THM, AND_IMP_INTRO]) THEN
  SRW_TAC [][RES_FORALL_THM] THEN
  `AbelianGroup g /\ Group g /\ FINITE g.carrier` by METIS_TAC [FiniteAbelianGroup_def, FiniteGroup_def] THEN
  Cases_on `s = {}` THEN1
  METIS_TAC [GPROD_SET_THM, group_id_carrier] THEN
  `?x t. (x = CHOICE s) /\ (t = REST s) /\ (s = x INSERT t)` by SRW_TAC [][CHOICE_INSERT_REST] THEN
  `x IN g.carrier` by METIS_TAC [CHOICE_DEF, SUBSET_DEF] THEN
  `t SUBSET g.carrier /\ FINITE t` by METIS_TAC [REST_SUBSET, SUBSET_TRANS, SUBSET_FINITE] THEN
  `x NOTIN t` by METIS_TAC [CHOICE_NOT_IN_REST] THEN
  `CARD t < CARD s` by SRW_TAC [][] THEN
  METIS_TAC [GPROD_SET_THM, DELETE_NON_ELEMENT, group_mult_carrier]);

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem of Abelian Groups.                                *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* An Invariant Property when reducing GPROD_SET g (IMAGE (\z. a*z) g.carrier):
     GPROD_SET g (IMAGE (\z. a*z) g.carrier)
   = (a*z) * GPROD_SET g ((IMAGE (\z. a*z) g.carrier) DELETE (a*z))
   = a * (GPROD_SET g (z INSERT {})) * GPROD_SET g (IMAGE (\z. a*z) (g.carrier DELETE z))
   = a * <building up a GPROD_SET> * <reducing down a GPROD_SET>
   = a*a * <building one more> * <reducing one more>
   = a*a*a * <building one more> * <reducing one more>
   = a**(CARD g.carrier) * GPROD_SET g g.carrier * GPROD_SET g {}
   = a**(CARD g.carrier) * GPROD_SET g g.carrier * #e
   = a**(CARD g.carrier) * GPROD_SET g g.carrier
*)
(* ------------------------------------------------------------------------- *)

(* Theorem: [INSERT for GPROD_SET_REDUCTION]
            (a*x)* GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)))
            = GPROD_SET g (coset g (g.carrier DIFF t)) *)
(* Proof:
   Essentially this is to prove:
   a * x * GPROD_SET g {a * z | z | z IN g.carrier /\ z <> x /\ z NOTIN s} =
           GPROD_SET g {a * z | z | z IN g.carrier /\ z NOTIN s}
   Let q = {a * z | z | z IN g.carrier /\ z <> x /\ z NOTIN s}
       p = {a * z | z | z IN g.carrier /\ z NOTIN s}
   Since p = (a*x) INSERT q   by EXTENSION,
     and (a*x) NOTIN q        by group_lcancel, a NOTIN s.
     and (a*x) IN g.carrier   by group_mult_carrier
   RHS = GPROD_SET g p
       = GPROD_SET g ((a*x) INSERT q)            by p = (a*x) INSERT q
       = (a*x) * GPROD_SET g (q DELETE (a*x))    by GPROD_SET_THM
       = (a*x) * GPROD_SET g q                   by DELETE_NON_ELEMENT, (a*x) NOTIN q.
       = LHS
*)
val GPROD_SET_REDUCTION_INSERT = store_thm(
  "GPROD_SET_REDUCTION_INSERT",
  ``!g s. FiniteAbelianGroup g /\ s SUBSET g.carrier ==>
   !a x::(g.carrier). x NOTIN s ==>
   (a*x* GPROD_SET g (coset g (g.carrier DIFF (x INSERT s)) a) = GPROD_SET g (coset g (g.carrier DIFF s) a))``,
  SRW_TAC [][coset_def, IMAGE_DEF, EXTENSION, RES_FORALL_THM] THEN
  Q.ABBREV_TAC `p = {a * z | z | z IN g.carrier /\ z NOTIN s}` THEN
  Q.ABBREV_TAC `q = {a * z | z | z IN g.carrier /\ z <> x /\ z NOTIN s}` THEN
  `p = (a*x) INSERT q` by (SRW_TAC [][EXTENSION, EQ_IMP_THM, Abbr `p`, Abbr `q`] THEN METIS_TAC []) THEN
  `FiniteGroup g /\ AbelianGroup g /\ Group g` by METIS_TAC [FiniteAbelianGroup_def, FiniteGroup_def] THEN
  `!z. z IN g.carrier /\ (a * z = a * x) <=> (z = x)` by METIS_TAC [RES_FORALL_THM, group_lcancel] THEN
  `(a*x) NOTIN q` by (SRW_TAC [][Abbr `q`] THEN METIS_TAC []) THEN
  `q SUBSET g.carrier` by (SRW_TAC [][EXTENSION, SUBSET_DEF, Abbr `q`] THEN METIS_TAC [group_mult_carrier]) THEN
  `a*x IN g.carrier` by SRW_TAC [][group_mult_carrier] THEN
  METIS_TAC [GPROD_SET_THM, DELETE_NON_ELEMENT]);

(* Theorem: (a**n) * <building n-steps GPROD_SET> * <reducing n-steps GPROD_SET> = GPROD_SET g g.carrier *)
(* Proof:
   By complete induction on CARD s.
   Case s = {},
     LHS = a**(CARD s) * (GPROD_SET g s) * GPROD_SET g (coset g (g.carrier DIFF s) a)
         = a**0 * (GPROD_SET g {}) * GPROD_SET g (coset g (g.carrier DIFF {}) a)  by CARD_EMPTY
         = #e * #e * GPROD_SET g (coset g g.carrier a)  by group_exp_zero, DIFF_EMPTY, GPROD_SET_EMPTY.
         = GPROD_SET g (coset g g.carrier a)   by group_lid
         = GPROD_SET g g.carrier                by GPROD_SET_IMAGE
         = RHS
   Case s <> {},
     Let x = CHOICE s, t = REST s, so s = x INSERT t, x NOTIN t.
     LHS = a**(CARD s) * (GPROD_SET g s) * GPROD_SET g (coset g (g.carrier DIFF s) a)
         = a**SUC(CARD t)*
           (GPROD_SET g (x INSERT t)) *
           GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)   by CARD s = SUC(CARD t), s = x INSERT t.
         = a**SUC(CARD t)*
           (x * GPROD_SET g (t DELETE x)) *
           GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)   by GPROD_SET_THM
         = a**SUC(CARD t)*
           (x * GPROD_SET g t) *
           GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)   by DELETE_NON_ELEMENT, x NOTIN t.
         = a*a**(CARD t) *
           x * GPROD_SET g t *
           GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)   by group_exp_def
         = a**(CARD t) *
           GPROD_SET g t *
           (a*x)*GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)  by Abelian group commuting
         = a ** (CARD t) *
           GPROD_SET g t *
           GPROD_SET g (coset g (g.carrier DIFF t) a)   by GPROD_SET_REDUCTION_INSERT
         = RHS                          by induction
*)
val GPROD_SET_REDUCTION = store_thm(
  "GPROD_SET_REDUCTION",
  ``!g s. FiniteAbelianGroup g /\ s SUBSET g.carrier ==>
   !a::(g.carrier).
   a**(CARD s) * (GPROD_SET g s) * GPROD_SET g (coset g (g.carrier DIFF s) a) = GPROD_SET g g.carrier``,
  completeInduct_on `CARD s` THEN
  POP_ASSUM (ASSUME_TAC o SIMP_RULE bool_ss [GSYM RIGHT_FORALL_IMP_THM, AND_IMP_INTRO]) THEN
  SRW_TAC [][RES_FORALL_THM] THEN
  `AbelianGroup g /\ Group g /\ FINITE g.carrier` by METIS_TAC [FiniteAbelianGroup_def, FiniteGroup_def] THEN
  Cases_on `s = {}` THENL [
    SRW_TAC [][] THEN
    `GPROD_SET g g.carrier IN g.carrier` by SRW_TAC [][GPROD_SET_PROPERTY] THEN
    METIS_TAC [group_exp_zero, GPROD_SET_EMPTY, GPROD_SET_IMAGE, group_lid, group_id_carrier],
    `?x t. (x = CHOICE s) /\ (t = REST s) /\ (s = x INSERT t)` by SRW_TAC [][CHOICE_INSERT_REST] THEN
    `x IN g.carrier` by METIS_TAC [CHOICE_DEF, SUBSET_DEF] THEN
    `t SUBSET g.carrier /\ FINITE t` by METIS_TAC [REST_SUBSET, SUBSET_TRANS, SUBSET_FINITE] THEN
    `x NOTIN t` by METIS_TAC [CHOICE_NOT_IN_REST] THEN
    `CARD t < CARD s` by SRW_TAC [][] THEN
    `CARD s = SUC(CARD t)` by SRW_TAC [][CARD_INSERT] THEN
    `a ** CARD s * GPROD_SET g s * GPROD_SET g (coset g (g.carrier DIFF s) a) =
    a ** SUC(CARD t) * GPROD_SET g (x INSERT t) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by SRW_TAC [][] THEN
    `_ = a ** SUC(CARD t) * (x * GPROD_SET g (t DELETE x)) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by SRW_TAC [][GPROD_SET_THM] THEN
    `_ = a ** SUC(CARD t) * (x * GPROD_SET g t) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by METIS_TAC [DELETE_NON_ELEMENT] THEN
    `_ = (a * a ** (CARD t)) * (x * GPROD_SET g t) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by SRW_TAC [][group_exp_def] THEN
    `_ = (a ** (CARD t) * a) * (x * GPROD_SET g t) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by METIS_TAC [AbelianGroup_def, group_exp_carrier] THEN
    `_ = a ** (CARD t) * (a * (x * GPROD_SET g t)) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by SRW_TAC [][group_assoc, group_exp_carrier, group_mult_carrier, GPROD_SET_PROPERTY] THEN
    `_ = a ** (CARD t) * ((a * x) * GPROD_SET g t) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by SRW_TAC [][group_assoc, GPROD_SET_PROPERTY] THEN
    `_ = a ** (CARD t) * (GPROD_SET g t * (a * x)) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by METIS_TAC [AbelianGroup_def, group_mult_carrier, GPROD_SET_PROPERTY] THEN
    `_ = (a ** (CARD t) * GPROD_SET g t) * (a * x) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a)`
    by SRW_TAC [][group_assoc, group_exp_carrier, group_mult_carrier, GPROD_SET_PROPERTY] THEN
    `_ = a ** (CARD t) * GPROD_SET g t * ((a * x) * GPROD_SET g (coset g (g.carrier DIFF (x INSERT t)) a))`
    by SRW_TAC [][group_assoc, group_exp_carrier, group_mult_carrier, GPROD_SET_PROPERTY, COSET_PROPERTY, DIFF_SUBSET] THEN
    `_ = a ** (CARD t) * GPROD_SET g t * GPROD_SET g (coset g (g.carrier DIFF t) a)`
    by METIS_TAC [GPROD_SET_REDUCTION_INSERT] THEN
    METIS_TAC []
  ]);

(* More theorems for paper *)

(* Theorem: [Cosets of a group are permutations]
            (coset g g.carrier a) = g.carrier *)
(* Proof:
   Essentially this is to prove:
   (1) a IN g.carrier /\ x IN g.carrier ==> a*x IN g.carrier, true by group_mult_carrier
   (2) a IN g.carrier /\ x IN g.carrier ==> ?z. (x = a * z) /\ z IN g.carrier, true by group_rsolve
*)
val GROUP_COSET_IS_PERMUTATION = store_thm(
  "GROUP_COSET_IS_PERMUTATION",
  ``!g a. Group g /\ a IN g.carrier ==> (coset g g.carrier a = g.carrier)``,
  SRW_TAC [][coset_def, pred_setTheory.IMAGE_DEF, pred_setTheory.EXTENSION, EQ_IMP_THM] THEN1
  METIS_TAC [group_mult_carrier] THEN
  `|/ a * x IN g.carrier` by METIS_TAC [group_inv_carrier, group_mult_carrier] THEN
  METIS_TAC [group_rsolve]);

(* Define Group Factorial *)
val GFACT_def = Define `
  GFACT g = GPROD_SET g g.carrier
`;

(* Theorem: GFACT g is an element in Group g. *)
(* Proof:
   By GPROD_SET_PROPERTY:
   !g s. FiniteAbelianGroup g /\ s SUBSET g.carrier ==> GPROD_SET g s IN g.carrier : thm
*)
val GFACT_IN_GROUP = store_thm(
  "GFACT_IN_GROUP",
  ``!g. FiniteAbelianGroup g ==> GFACT g IN g.carrier``,
  METIS_TAC [GFACT_def, GPROD_SET_PROPERTY, pred_setTheory.SUBSET_REFL]);

(* Theorem: For FiniteAbelian Group g, a IN g.carrier ==> GFACT g = a**(CARD g) * GFACT g *)
(* Proof:
   Since g.carrier SUBSET g.carrier,
   and g.carrier DIFF g.carrier = {},
   Put s = g.carrier in GPROD_SET_REDUCTION:
   a ** (CARD g.carrier) * GPROD_SET g g.carrier * GPROD_SET g (coset g (g.carrier DIFF g.carrier) a) =
   GPROD_SET g g.carrier
   ==> a ** (CARD g.carrier) * GPROD_SET g g.carrier * GPROD_SET g (coset g {} a) = GPROD_SET g g.carrier
   ==> a ** (CARD g.carrier) * GPROD_SET g g.carrier * GPROD_SET g {} = GPROD_SET g g.carrier, by COSET_EMPTY.
   ==> a ** (CARD g.carrier) * GPROD_SET g g.carrier * #e = GPROD_SET g g.carrier, by GPROD_SET_EMPTY.
   ==> a ** (CARD g.carrier) * GPROD_SET g g.carrier = GPROD_SET g g.carrier, by group_assoc and group_rid
*)
val GFACT_IDENTITY = store_thm(
  "GFACT_IDENTITY",
  ``!g a. FiniteAbelianGroup g /\ a IN g.carrier ==> (GFACT g = a**(CARD g.carrier) * GFACT g)``,
  REPEAT STRIP_TAC THEN
  `g.carrier SUBSET g.carrier` by SRW_TAC [][] THEN
  `g.carrier DIFF g.carrier = {}` by SRW_TAC [][] THEN
  `GPROD_SET g g.carrier IN g.carrier` by SRW_TAC [][GPROD_SET_PROPERTY] THEN
  `AbelianGroup g /\ Group g /\ FINITE g.carrier` by METIS_TAC [FiniteAbelianGroup_def, FiniteGroup_def] THEN
  `GPROD_SET g g.carrier =
    a ** (CARD g.carrier) * GPROD_SET g g.carrier * GPROD_SET g (coset g (g.carrier DIFF g.carrier) a)`
    by SRW_TAC [][GPROD_SET_REDUCTION] THEN
  `_ = a ** (CARD g.carrier) * GPROD_SET g g.carrier * GPROD_SET g (coset g {} a)` by SRW_TAC [][] THEN
  `_ = a ** (CARD g.carrier) * GPROD_SET g g.carrier * GPROD_SET g {}` by SRW_TAC [][COSET_EMPTY] THEN
  `_ = a ** (CARD g.carrier) * GPROD_SET g g.carrier * #e` by METIS_TAC [GPROD_SET_EMPTY] THEN
  `_ = a ** (CARD g.carrier) * GPROD_SET g g.carrier` by SRW_TAC [][group_rid, group_exp_carrier, group_mult_carrier] THEN
  METIS_TAC [GFACT_def]);

(* Theorem: For FiniteAbelian Group g, a IN g.carrier ==> a**(CARD g) = #e *)
(* Proof:
   By GFACT_IDENTITY: a**(CARD g.carrier) * GFACT g = GFACT g
   By group_lid_unique, a**(CARD g.carrier) = #e
*)
val FINITE_ABELIAN_FERMAT = store_thm(
  "FINITE_ABELIAN_FERMAT",
  ``!g a. FiniteAbelianGroup g /\ a IN g.carrier ==> (a**(CARD g.carrier) = #e)``,
  REPEAT STRIP_TAC THEN
  `Group g` by METIS_TAC [FiniteAbelianGroup_def, FiniteGroup_def] THEN
  `GFACT g IN g.carrier` by SRW_TAC [][GFACT_IN_GROUP] THEN
  `a**(CARD g.carrier) * GFACT g = GFACT g` by SRW_TAC [][GFACT_IDENTITY] THEN
  METIS_TAC [group_exp_carrier, group_lid_unique]);

(* ------------------------------------------------------------------------- *)
(* Conjugate of sets and subgroups                                           *)
(* ------------------------------------------------------------------------- *)

(* Conjugate of a set s by a group element a in G is the set {a*z*1/a | z in s}. *)
val conjugate_def = Define `
  conjugate g a s = { a * z * g.inv a | z IN s}
`;

(* Conjugate of subgroup h <= g by a in G is the set {a*z*1/a | z in H}. *)
val conjugate_subgroup_def = Define `
  conjugate_subgroup h g a =
      <| carrier := conjugate g a h.carrier;
              id := #e;
             inv := |/ ;
            mult := g.mult
       |>`;

(* Theorem: Group g, h <= g, a in G ==> Group (conjugate_subgroup h g a) *)
(* Proof:
   Closure: (a*z*|/a)*(a*z'*|/ a) = a*z*(|/ a * a)*z'*|/ a = a*(z*z')*|/ a, and z*z' IN h.carrier.
   Associativity: inherits from group associativity?
   Identity: #e in (conjugate_subgroup h g a) since #e IN h.carrier and a*#e*|/ a = #e.
   Inverse: |/ (a*z*|/a) = |/(|/a) * (|/ z) * |/a = a*(|/z)*|/a, and |/z IN h.carrier.
*)
val CONJUGATE_SUBGROUP_GROUP = store_thm(
  "CONJUGATE_SUBGROUP_GROUP",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==> Group (conjugate_subgroup h g a)``,
  REPEAT STRIP_TAC THEN
  `Group h /\ h.carrier SUBSET g.carrier /\ #e IN h.carrier` by METIS_TAC [Subgroup_def, group_id_carrier] THEN
  `#e IN g.carrier /\ |/ a IN g.carrier` by SRW_TAC [][group_id_carrier, group_inv_carrier] THEN
  SRW_TAC [][conjugate_subgroup_def, conjugate_def, Group_def, RES_FORALL_THM] THEN1
  METIS_TAC [group_rid, group_rinv] THEN1
 (`z IN g.carrier /\ z' IN g.carrier ` by METIS_TAC [SUBSET_DEF, group_mult_carrier] THEN
  `a * z' * |/ a = a * (z' * |/ a)` by SRW_TAC [][group_assoc, group_mult_carrier, group_inv_carrier] THEN
  `a * z * |/ a * (a * (z' * |/ a)) = a * z * (|/ a * a) * (z' * |/ a)` by SRW_TAC [][group_assoc, group_mult_carrier, group_inv_carrier] THEN
  `_ = a * z * (z' * |/ a)` by SRW_TAC [][group_mult_carrier, group_linv, group_rid] THEN
  `_ = a *(z * z')* |/ a` by SRW_TAC [][group_assoc, group_mult_carrier, group_inv_carrier] THEN
  METIS_TAC [Subgroup_def, group_mult_carrier]) THEN1
 (`|/ z IN h.carrier` by METIS_TAC [Subgroup_def, group_inv_carrier] THEN
  `z IN g.carrier /\ |/ z IN g.carrier` by METIS_TAC [SUBSET_DEF] THEN
  `|/ (a * z * |/ a) = |/ (|/ a) * (|/ z * |/ a)` by SRW_TAC [][group_inv_mult, group_mult_carrier] THEN
  `_ = a * |/ z * |/ a` by SRW_TAC [][group_inv_inv, group_assoc, group_mult_carrier] THEN
  METIS_TAC []) THEN1
 (`z IN g.carrier` by METIS_TAC [SUBSET_DEF] THEN
  SRW_TAC [][group_mult_carrier, group_lid]) THEN1
 (`z IN g.carrier` by METIS_TAC [SUBSET_DEF] THEN
  SRW_TAC [][group_mult_carrier, group_linv]) THEN
  `z IN g.carrier /\ z' IN g.carrier /\ z''' IN g.carrier` by METIS_TAC [SUBSET_DEF] THEN
  `a * z * |/ a IN g.carrier /\
    a * z' * |/ a IN g.carrier /\
    a * z''' * |/ a IN g.carrier` by SRW_TAC [][group_mult_carrier] THEN
  SRW_TAC [][group_mult_carrier, group_assoc]);

(* Theorem: Group g, h <= g, a in G ==> (conjugate_subgroup h g a) <= g *)
(* Proof:
   By CONJUGATE_SUBGROUP_GROUP, and (conjugate_subgroup h g a).carrier SUBSET g.carrier.
*)
val CONJUGATE_SUBGROUP_SUBGROUP = store_thm(
  "CONJUGATE_SUBGROUP_SUBGROUP",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==> (conjugate_subgroup h g a) <= g``,
  REPEAT STRIP_TAC THEN
  `Group (conjugate_subgroup h g a)` by SRW_TAC [][CONJUGATE_SUBGROUP_GROUP] THEN
  SRW_TAC [][conjugate_subgroup_def, conjugate_def, Subgroup_def, SUBSET_DEF] THEN
  METIS_TAC [Subgroup_def, SUBSET_DEF, group_inv_carrier, group_mult_carrier]);

(* Theorem: [Bijection between subgroup and its conjugate]
            Group g, h <= g, a in G ==>
            BIJ (\z. a * z * |/ a) h.carrier (conjugate_subgroup h g a).carrier *)
(* Proof:
   Essentially this is to prove:
   (1) z IN h.carrier ==> ?z'. (a * z * |/ a = a * z' * |/ a) /\ z' IN h.carrier
       True by taking z' = z.
   (2) z IN h.carrier /\ z' IN h.carrier /\ a * z * |/ a = a * z' * |/ a ==> z = z'
       True by group left/right cancellations.
   (3) z IN h.carrier ==> ?z'. (a * z * |/ a = a * z' * |/ a) /\ z' IN h.carrier
       True by taking z' = z.
   (4) z IN h.carrier ==> ?z'. z' IN h.carrier /\ (a * z' * |/ a = a * z * |/ a)
       True by taking z' = z.
*)
val SUBGROUP_CONJUGATE_SUBGROUP_BIJ = store_thm(
  "SUBGROUP_CONJUGATE_SUBGROUP_BIJ",
  ``!g h a. Group g /\ h <= g /\ a IN g.carrier ==>
    BIJ (\z. a * z * |/ a) h.carrier (conjugate_subgroup h g a).carrier``,
  SRW_TAC [][conjugate_subgroup_def, conjugate_def, BIJ_DEF, INJ_DEF, SURJ_DEF] THEN1
  METIS_TAC [] THEN1
 (`z IN g.carrier /\ z' IN g.carrier` by METIS_TAC [Subgroup_def, SUBSET_DEF] THEN
  `|/ a IN g.carrier` by SRW_TAC [][group_inv_carrier] THEN
  Q.ABBREV_TAC `b = a*z` THEN
  Q.ABBREV_TAC `c = a*z'` THEN
  `b IN g.carrier /\ c IN g.carrier` by METIS_TAC [group_mult_carrier] THEN
  `b = c` by METIS_TAC [group_rcancel] THEN
  METIS_TAC [group_lcancel]) THEN1
  METIS_TAC [] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
