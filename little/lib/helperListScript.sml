(* ------------------------------------------------------------------------- *)
(* Helper Theorems - a collection of useful results -- for Lists.            *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "helperList";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory dividesTheory;
open helperNumTheory helperSetTheory;

(* ------------------------------------------------------------------------- *)
(* More for listTheory                                                 *)
(* ------------------------------------------------------------------------- *)

(* Note: There is LENGTH_NIL, but no LENGTH_NON_NIL *)

(* Theorem: a non-NIL list has positive LENGTH. *)
val LENGTH_NON_NIL = store_thm(
  "LENGTH_NON_NIL",
  ``!l. l <> [] ==> 0 < LENGTH l``,
  REPEAT STRIP_TAC THEN
  `LENGTH l <> 0` by SRW_TAC [][LENGTH_NIL] THEN
  RW_TAC arith_ss []);

(* DROP and TAKE *)

(* Note: There is TAKE_LENGTH_ID, but no DROP_LENGTH_NIL *)

(* Theorem: DROP the whole length of list is NIL. *)
val DROP_LENGTH_NIL = store_thm(
  "DROP_LENGTH_NIL",
  ``DROP (LENGTH l) l = []``,
  Induct_on `l` THEN SRW_TAC [][]);

(* Theorem: TAKE 1 of non-NIL list is unaffected by appending. *)
val TAKE_1_APPEND = store_thm(
  "TAKE_1_APPEND",
  ``!x y. x <> [] ==> (TAKE 1 (x ++ y) = TAKE 1 x)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 of non-NIL list is mildly affected by appending. *)
val DROP_1_APPEND = store_thm(
  "DROP_1_APPEND",
  ``!x y. x <> [] ==> (DROP 1 (x ++ y) = (DROP 1 x) ++ y)``,
  Cases_on `x` THEN SRW_TAC [][]);

(* Theorem: DROP 1 more time is once more of previous DROP. *)
val DROP_SUC = store_thm(
  "DROP_SUC",
  ``!n x. DROP (SUC n) x = DROP 1 (DROP n x)``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: TAKE 1 more time is a bit more than previous TAKE. *)
val TAKE_SUC = store_thm(
  "TAKE_SUC",
  ``!n x. TAKE (SUC n) x = (TAKE n x) ++ (TAKE 1 (DROP n x))``,
  Induct_on `x` THEN SRW_TAC [][] THEN
  `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Theorem: DROP is non-NIL when the number is less than LENGTH. *)
val DROP_NON_NIL = store_thm(
  "DROP_NON_NIL",
  ``!n l. n < LENGTH l ==> DROP n l <> []``,
  Induct_on `l` THEN SRW_TAC [][] THEN
  `n - 1 < LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC []);

(* Unit-Length Lists *)

(* Theorem: A unit-length list has its set a singleton. *)
val LIST_1_SET_SING = store_thm(
  "LIST_1_SET_SING",
  ``!l. (LENGTH l = 1) ==> SING (set l)``,
  SRW_TAC [][SING_DEF] THEN
  Cases_on `l` THEN1
   (`LENGTH [] = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `1 <> 0` by RW_TAC arith_ss [] THEN METIS_TAC []) THEN
  `SUC (LENGTH t) = 1` by METIS_TAC [LENGTH] THEN
  `LENGTH t = 0` by RW_TAC arith_ss [] THEN
  `t = []` by METIS_TAC [LENGTH_NIL] THEN
  FULL_SIMP_TAC (srw_ss())[] THEN METIS_TAC []);

(* Mono-list Theory: a mono-list is a list l with SING (set l) *)

(* Theorem: Two mono-lists are equal if their lengths and sets are equal *)
val MONOLIST_EQ = store_thm(
  "MONOLIST_EQ",
  ``!l1 l2. SING (set l1) /\ SING (set l2) ==>
              ((l1 = l2) <=> (LENGTH l1 = LENGTH l2) /\ (set l1 = set l2))``,
  Induct THEN SRW_TAC [][NOT_SING_EMPTY, SING_INSERT] THENL [
    Cases_on `l2` THEN SRW_TAC [][] THEN
    FULL_SIMP_TAC (srw_ss()) [SING_INSERT, EQUAL_SING] THEN
    SRW_TAC [][LENGTH_NIL, NOT_SING_EMPTY, EQUAL_SING] THEN METIS_TAC [],
    Cases_on `l2` THEN SRW_TAC [][] THEN
    FULL_SIMP_TAC (srw_ss()) [SING_INSERT, LENGTH_NIL, NOT_SING_EMPTY, EQUAL_SING] THEN
    METIS_TAC []
  ]);

(* Theorem: A non-mono-list has at least one element in tail that is distinct from its head. *)
val NON_MONO_TAIL_PROPERTY = store_thm(
  "NON_MONO_TAIL_PROPERTY",
  ``!l. ~SING (set (h::t)) ==> ?h'. h' IN set t /\ h' <> h``,
  SRW_TAC [][SING_INSERT] THEN
  `set t <> {}` by METIS_TAC [LIST_TO_SET_EQ_EMPTY] THEN
  `?e. e IN set t` by METIS_TAC [MEMBER_NOT_EMPTY] THEN
  FULL_SIMP_TAC (srw_ss()) [EXTENSION] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
