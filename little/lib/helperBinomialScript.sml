(* ------------------------------------------------------------------------- *)
(* Binomial coefficients and expansion                                       *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "helperBinomial";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
open pred_setTheory listTheory arithmeticTheory;
(* val _ = load "dividesTheory"; *)
(* val _ = load "gcdTheory"; *)
open dividesTheory gcdTheory;

(* Get dependent theories in lib *)
(* val _ = load "helperNumTheory"; *)
(* val _ = load "helperSetTheory"; *)
open helperNumTheory helperSetTheory;

(* val _ = load "binomialTheory"; *)
open binomialTheory;


(* ------------------------------------------------------------------------- *)
(* Helper theorems on SUM                                                    *)
(* ------------------------------------------------------------------------- *)

(* Theorem: 0 < n ==> SUM (GENLIST f (SUC n)) = f 0 + SUM (GENLIST (f o SUC) (PRE n)) + f n *)
(* Proof:
     SUM (GENLIST f (SUC n))
   = SUM (GENLIST f n) + f n                       by SUM_DECOMPOSE_LAST
   = SUM (GENLIST f (SUC m)) + f n                 by n = SUC m, 0 < n
   = f 0 + SUM (GENLIST (f o SUC) m) + f n         by SUM_DECOMPOSE_FIRST
   = f 0 + SUM (GENLIST (f o SUC) (PRE n)) + f n   by PRE_SUC_EQ
*)
val SUM_DECOMPOSE_FIRST_LAST = store_thm(
  "SUM_DECOMPOSE_FIRST_LAST",
  ``!f n. 0 < n ==> (SUM (GENLIST f (SUC n)) = f 0 + SUM (GENLIST (f o SUC) (PRE n)) + f n)``,
  METIS_TAC [SUM_DECOMPOSE_LAST, SUM_DECOMPOSE_FIRST, LESS_EQ_SUC, PRE_SUC_EQ]);

(* Theorem: (x + y + z) MOD n = (x MOD n + y MOD n + z MOD n) MOD n *)
(* Proof:
     (x + y + z) MOD n
   = ((x + y) MOD n + z MOD n) MOD n               by MOD_PLUS
   = ((x MOD n + y MOD n) MOD n + z MOD n) MOD n   by MOD_PLUS
   = (x MOD n + y MOD n + z MOD n) MOD n           by MOD_MOD
*)
val MOD_PLUS3 = store_thm(
  "MOD_PLUS3",
  ``!n. 0 < n ==> !x y z. (x + y + z) MOD n = (x MOD n + y MOD n + z MOD n) MOD n``,
  METIS_TAC [MOD_PLUS, MOD_MOD]);

(* Theorem: (SUM l) MOD n = (SUM (MAP (\x. x MOD n) l)) MOD n *)
(* Proof: by list induction.
   Base case: SUM [] MOD n = SUM (MAP (\x. x MOD n) []) MOD n
      true by SUM [] = 0, MAP f [] = 0, and 0 MOD n = 0.
   Step case: SUM l MOD n = SUM (MAP (\x. x MOD n) l) MOD n ==>
              !h. SUM (h::l) MOD n = SUM (MAP (\x. x MOD n) (h::l)) MOD n
      SUM (h::l) MOD n
    = (h + SUM l) MOD n                                           by SUM
    = (h MOD n + (SUM l) MOD n) MOD n                             by MOD_PLUS
    = (h MOD n + SUM (MAP (\x. x MOD n) l) MOD n) MOD n           by induction hypothesis
    = ((h MOD n) MOD n + SUM (MAP (\x. x MOD n) l) MOD n) MOD n   by MOD_MOD
    = ((h MOD n + SUM (MAP (\x. x MOD n) l)) MOD n) MOD n         by MOD_PLUS
    = (h MOD n + SUM (MAP (\x. x MOD n) l)) MOD n                 by MOD_MOD
    = (SUM (h MOD n ::(MAP (\x. x MOD n) l))) MOD n               by SUM
    = (SUM (MAP (\x. x MOD n) (h::l))) MOD n                      by MAP
*)
val MOD_SUM = store_thm(
  "MOD_SUM",
  ``!n. 0 < n ==> !l. (SUM l) MOD n = (SUM (MAP (\x. x MOD n) l)) MOD n``,
  REPEAT STRIP_TAC THEN
  Induct_on `l` THEN1
  SRW_TAC [][] THEN
  REPEAT STRIP_TAC THEN
  `SUM (h::l) MOD n = (h MOD n + (SUM l) MOD n) MOD n` by RW_TAC std_ss [SUM, MOD_PLUS] THEN
  `_ = ((h MOD n) MOD n + SUM (MAP (\x. x MOD n) l) MOD n) MOD n` by RW_TAC std_ss [MOD_MOD] THEN
  SRW_TAC [][MOD_PLUS]);

(* Theorem: SUM l = 0 <=> l = EVERY (\x. x = 0) l *)
(* Proof: by induction on l.
   Base case: (SUM [] = 0) <=> EVERY (\x. x = 0) []
      true by SUM [] = 0 and GENLIST f 0 = [].
   Step case: (SUM l = 0) <=> EVERY (\x. x = 0) l ==>
              !h. (SUM (h::l) = 0) <=> EVERY (\x. x = 0) (h::l)
       SUM (h::l) = 0
   <=> h + SUM l = 0                  by SUM
   <=> h = 0 /\ SUM l = 0             by ADD_EQ_0
   <=> h = 0 /\ EVERY (\x. x = 0) l   by induction hypothesis
   <=> EVERY (\x. x = 0) (h::l)       by EVERY_DEF
*)
val SUM_EQ_0 = store_thm(
  "SUM_EQ_0",
  ``!l. (SUM l = 0) <=> EVERY (\x. x = 0) l``,
  Induct THEN
  SRW_TAC [][]);

(* Note:

- EVERY_MAP;
> val it = |- !P f l. EVERY P (MAP f l) <=> EVERY (\x. P (f x)) l : thm
- EVERY_GENLIST;
> val it = |- !n. EVERY P (GENLIST f n) <=> !i. i < n ==> P (f i) : thm
- MAP_GENLIST;
> val it = |- !f g n. MAP f (GENLIST g n) = GENLIST (f o g) n : thm
*)


(* Theorem: !k. 0 < k /\ k < n ==> (binomial n k MOD n = 0) <=>
            !h. 0 <= h /\ h < PRE n ==> (binomial n (SUC h) MOD n = 0) *)
(* Proof: by h = PRE k, or k = SUC h. *)
val binomial_range_shift = store_thm(
  "binomial_range_shift",
  ``!n . 0 < n ==> ((!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
                   (!h. h < PRE n ==> ((binomial n (SUC h)) MOD n = 0)))``,
  RW_TAC std_ss [EQ_IMP_THM] THENL [
    `0 < SUC h /\ SUC h < n` by DECIDE_TAC THEN
    RW_TAC std_ss [],
    `k <> 0` by DECIDE_TAC THEN
    `?h. k = SUC h` by METIS_TAC [num_CASES] THEN
    `h < PRE n` by DECIDE_TAC THEN
    RW_TAC std_ss []
  ]);

(* Theorem: binomial n k MOD n = 0 <=> (binomial n k * x ** (n-k) * y ** k) MOD n = 0 *)
(* Proof:
       (binomial n k * x ** (n-k) * y ** k) MOD n = 0
   <=> (binomial n k * (x ** (n-k) * y ** k)) MOD n = 0    by MULT_ASSOC
   <=> (((binomial n k) MOD n) * ((x ** (n - k) * y ** k) MOD n)) MOD n = 0  by MOD_TIMES2
   If part, apply 0 * z = 0  by MULT.
   Only-if part, pick x = 1, y = 1, apply EXP_1.
*)
val binomial_mod_zero = store_thm(
  "binomial_mod_zero",
  ``!n. 0 < n ==> !k. (binomial n k MOD n = 0) <=> (!x y. (binomial n k * x ** (n-k) * y ** k) MOD n = 0)``,
  RW_TAC std_ss [EQ_IMP_THM] THEN1
  METIS_TAC [MOD_TIMES2, ZERO_MOD, MULT] THEN
  METIS_TAC [EXP_1, MULT_RIGHT_1]);




(* Theorem: !k. 0 < k /\ k < n ==> (binomial n k) MOD n = 0 <=>
            EVERY (\x. x = 0) (GENLIST (\k. (binomial n (SUC k)) MOD n) (PRE n)) *)
(* Proof:
       !k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)
   <=> !h. h < PRE n ==> ((binomial n (SUC h)) MOD n = 0)                    by binomial_range_shift
   <=> EVERY (\x. x = 0) (GENLIST (\k. (binomial n (SUC k)) MOD n) (PRE n))  by GENLIST_CONSTANT
*)

(* Theorem: (!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0))) <=>
            (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))) *)
(* Proof: by h = PRE k, or k = SUC h. *)
val binomial_range_shift' = store_thm(
  "binomial_range_shift'",
  ``!n . 0 < n ==> ((!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0))) <=>
                   (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))))``,
  RW_TAC std_ss [EQ_IMP_THM] THENL [
    `0 < SUC h /\ SUC h < n` by DECIDE_TAC THEN
    RW_TAC std_ss [],
    `k <> 0` by DECIDE_TAC THEN
    `?h. k = SUC h` by METIS_TAC [num_CASES] THEN
    `h < PRE n` by DECIDE_TAC THEN
    RW_TAC std_ss []
  ]);

(* Theorem: !k. 0 < k /\ k < n ==> (binomial n k) MOD n = 0 <=>
            !x y. SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0 *)
(* Proof:
       !k. 0 < k /\ k < n ==> (binomial n k) MOD n = 0
   <=> !k. 0 < k /\ k < n ==> !x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0)   by binomial_mod_zero
   <=> !h. h < PRE n ==> !x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0)  by binomial_range_shift'
   <=> !x y. EVERY (\z. z = 0) (GENLIST (\k. (binomial n (SUC k) * x ** (n - (SUC k)) * y ** (SUC k)) MOD n) (PRE n)) by EVERY_GENLIST
   <=> !x y. EVERY (\x. x = 0) (GENLIST ((\k. binomial n k * x ** (n - k) * y ** k) o SUC) (PRE n)  by FUN_EQ_THM
   <=> !x y. SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0   by SUM_EQ_0
*)
val binomial_mod_zero' = store_thm(
  "binomial_mod_zero'",
  ``!n. 0 < n ==> ((!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
                  !x y. SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0)``,
  REPEAT STRIP_TAC THEN
  `!x y. (\k. (binomial n (SUC k) * x ** (n - SUC k) * y ** (SUC k)) MOD n) = (\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC` by RW_TAC std_ss [FUN_EQ_THM] THEN
  `(!k. 0 < k /\ k < n ==> ((binomial n k) MOD n = 0)) <=>
    (!k. 0 < k /\ k < n ==> (!x y. ((binomial n k * x ** (n - k) * y ** k) MOD n = 0)))` by RW_TAC std_ss [binomial_mod_zero] THEN
  `_ = (!h. h < PRE n ==> (!x y. ((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0)))` by RW_TAC std_ss [binomial_range_shift'] THEN
  `_ = !x y h. h < PRE n ==> (((binomial n (SUC h) * x ** (n - (SUC h)) * y ** (SUC h)) MOD n = 0))` by METIS_TAC [] THEN
  RW_TAC std_ss [EVERY_GENLIST, SUM_EQ_0]);

(* Theorem: SUM (GENLIST ((\k. binomial n k * x ** (n - k) * y ** k) o SUC) (PRE n)) MOD n =
            SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) MOD n *)
(* Proof:
     SUM (GENLIST ((\k. binomial n k * x ** (n - k) * y ** k) o SUC) (PRE n)) MOD n
   = SUM (MAP (\x. x MOD n) (GENLIST ((\k. binomial n k * x ** (n - k) * y ** k) o SUC) (PRE n))) MOD n  by MOD_SUM
   = SUM (GENLIST ((\x. x MOD n) o ((\k. binomial n k * x ** (n - k) * y ** k) o SUC)) (PRE n)) MOD n    by MAP_GENLIST
   = SUM (GENLIST ((\x. x MOD n) o (\k. binomial n k * x ** (n - k) * y ** k) o SUC) (PRE n)) MOD n      by composition associative
   = SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) MOD n              by composition
*)

(* Theorem: SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n =
            SUM (GENLIST ((\k. f k MOD n) o SUC) (PRE n)) MOD n *)
(* Proof:
     SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n
   = SUM (MAP (\x. x MOD n) (GENLIST ((\k. f k) o SUC) (PRE n))) MOD n  by MOD_SUM
   = SUM (GENLIST ((\x. x MOD n) o ((\k. f k) o SUC)) (PRE n)) MOD n    by MAP_GENLIST
   = SUM (GENLIST ((\x. x MOD n) o (\k. f k) o SUC) (PRE n)) MOD n      by composition associative
   = SUM (GENLIST ((\k. f k MOD n) o SUC) (PRE n)) MOD n                by composition
*)
val SUM_GENLIST_MOD = store_thm(
  "SUM_GENLIST_MOD",
  ``!n. 0 < n ==> !f. SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n = SUM (GENLIST ((\k. f k MOD n) o SUC) (PRE n)) MOD n``,
  REPEAT STRIP_TAC THEN
  `SUM (GENLIST ((\k. f k) o SUC) (PRE n)) MOD n =
    SUM (MAP (\x. x MOD n) (GENLIST ((\k. f k) o SUC) (PRE n))) MOD n` by METIS_TAC [MOD_SUM] THEN
  RW_TAC std_ss [MAP_GENLIST, combinTheory.o_ASSOC, combinTheory.o_ABS_L]);

(* Theorem: [Binomial Expansion for prime exponent]  (x + y)^p = x^p + y^p (mod p) *)
(* Proof:
     (x+y)^p  (mod p)
   = SUM (k=0..p) C(p,k)x^(p-k)y^k  (mod p)                                     by binomial theorem
   = (C(p,0)x^py^0 + SUM (k=1..(p-1)) C(p,k)x^(p-k)y^k + C(p,p)x^0y^p) (mod p)  by breaking sum
   = (x^p + SUM (k=1..(p-1)) C(p,k)x^(p-k)y^k + y^k) (mod p)                    by binomial_n_0, binomial_n_n
   = ((x^p mod p) + (SUM (k=1..(p-1)) C(p,k)x^(p-k)y^k) (mod p) + (y^p mod p)) mod p   by MOD_PLUS
   = ((x^p mod p) + (SUM (k=1..(p-1)) (C(p,k)x^(p-k)y^k) (mod p)) + (y^p mod p)) mod p
   = (x^p mod p  + 0 + y^p mod p) mod p                                         by prime_iff_divides_binomials
   = (x^p + y^p) (mod p)                                                        by MOD_PLUS

     (x + y) ** n
   = SUM (GENLIST (\k. binomial n k * x ** (n - k) * y ** k) (SUC n))    by binomial_thm
   = x ** n + SUM (GENLIST ((\k. binomial n k * x ** (n - k) * y ** k) o SUC) (PRE n)) + y**n   by SUM_DECOMPOSE_FIRST_LAST

   Since 1 < n,
     prime n <=> !k. 0 < k /\ k < n ==> (binomial n k) MOD n  = 0     by prime_iff_divides_binomials, DIVIDES_MOD_0
             <=> !x y. SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0
                                                                      by binomial_mod_zero'
   Hence
       (x + y) ** n  MOD n
     = ((x ** n MOD n) +
        (SUM (GENLIST ((\k. binomial n k * x ** (n - k) * y ** k) o SUC) (PRE n)) MOD n) +
        (y ** n MOD n)) MOD n                                         by MOD_PLUS3
     = (x ** n MOD n + y ** n MOD n) MOD n                            iff prime n
     = (x ** n + y ** n) MOD n                                        by MOD_PLUS
*)
(*
e (`SUM (GENLIST ((\k. (binomial n k * x ** (n - k) * y ** k) MOD n) o SUC) (PRE n)) = 0` by ???
We need this to proceed, but this involves a concept in polynomials:
A polynomial MOD n = 0 <=> all (coefficients of polynomial MOD n) = 0
e.g.   (a x + b) MOD n = 0,   how to show a MOD n = 0 and b MOD n = 0 ?
Put x = 0, then b MOD n = 0.
Put x = 1, then (a + b) MOD n = 0, or a MOD n = 0.
e.g.   (a x^2 + b x + c) MOD n  = 0.
Put x = 0, c MOD n = 0
Put x = 1, (a + b) MOD n = 0
Put x = -1 MOD n, (a - b) MOD n = 0
So  2a MOD n = 0, a MOD n = 0  if gcd (2,n) = 1. If not choose other values.
The fact is: putting each value of x gives an equation, there are MANY equations to determine just 3 constants.
This reduces to the Principle of Equating Coefficients, which amounts to:

If P(x) = 0, then all coefficients of P(x) = 0    (for MOD infinity)
If P(x) MOD n = 0, then all (coefficients of P(x) MOD n = 0).

In turn, this depends on the fact that: number of roots of P(x) cannot exceed its degree.
This seems to hold only for a Field, but Z_n is generally not a Field, so what is really required>
*)

(* Theorem: [Binomial Expansion for prime exponent]  (x + y)^p = x^p + y^p (mod p) *)
(* Proof:
     (x+y)^p  (mod p)
   = SUM (k=0..p) C(p,k)x^(p-k)y^k  (mod p)                                     by binomial theorem
   = (C(p,0)x^py^0 + SUM (k=1..(p-1)) C(p,k)x^(p-k)y^k + C(p,p)x^0y^p) (mod p)  by breaking sum
   = (x^p + SUM (k=1..(p-1)) C(p,k)x^(p-k)y^k + y^k) (mod p)                    by binomial_n_0, binomial_n_n
   = ((x^p mod p) + (SUM (k=1..(p-1)) C(p,k)x^(p-k)y^k) (mod p) + (y^p mod p)) mod p   by MOD_PLUS
   = ((x^p mod p) + (SUM (k=1..(p-1)) (C(p,k)x^(p-k)y^k) (mod p)) + (y^p mod p)) mod p
   = (x^p mod p  + 0 + y^p mod p) mod p                                         by prime_iff_divides_binomials
   = (x^p + y^p) (mod p)                                                        by MOD_PLUS
*)
val binomial_thm_prime = store_thm(
  "binomial_thm_prime",
  ``!p. prime p ==> (!x y. (x + y)**p MOD p = (x**p + y**p) MOD p)``,
  REPEAT STRIP_TAC THEN
  `0 < p` by RW_TAC std_ss [PRIME_POS] THEN
  `!k. 0 < k /\ k < p ==> ((binomial p k) MOD p  = 0)` by METIS_TAC [prime_iff_divides_binomials, DIVIDES_MOD_0] THEN
  `SUM (GENLIST ((\k. binomial p k * x ** (p - k) * y ** k) o SUC) (PRE p)) MOD p = 0` by METIS_TAC [SUM_GENLIST_MOD, binomial_mod_zero', ZERO_MOD] THEN
  `(x + y) ** p MOD p = (x ** p + SUM (GENLIST ((\k. binomial p k * x ** (p - k) * y ** k) o SUC) (PRE p)) + y ** p) MOD p` by RW_TAC std_ss [binomial_thm, SUM_DECOMPOSE_FIRST_LAST, binomial_n_0, binomial_n_n, EXP] THEN
  METIS_TAC [MOD_PLUS3, ADD_0, MOD_PLUS]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
