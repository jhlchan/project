(* ------------------------------------------------------------------------- *)
(* Cycle Theory -- simplifed to be valid for any n.                          *)
(* ------------------------------------------------------------------------- *)

(*

Cycle Theory
============

Necklaces are represented by linear lists of length n.

Since they are necklaces, it is natural to join them end to end. For example,
the following "cycling" necklaces are considered equivalent:

+--+--+--+--+--+--+--+
|2 |4 |0 |3 |1 |2 |3 |
+--+--+--+--+--+--+--+
|4 |0 |3 |1 |2 |3 |2 |
+--+--+--+--+--+--+--+
|0 |3 |1 |2 |3 |2 |4 |
+--+--+--+--+--+--+--+
|3 |1 |2 |3 |2 |4 |0 |
+--+--+--+--+--+--+--+
|1 |2 |3 |2 |4 |0 |3 |
+--+--+--+--+--+--+--+
|2 |3 |2 |4 |0 |3 |1 |
+--+--+--+--+--+--+--+
|3 |2 |4 |0 |3 |1 |2 | (next cycle identical to first)
+--+--+--+--+--+--+--+

We shall define and investigate the "cycle" operation on a list.
*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "cycle";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory;

(* Get dependent theories local *)
(* val _ = load "helperListTheory"; *)
open helperListTheory;

(* ------------------------------------------------------------------------- *)
(* Cycle Theory -- using FUNPOW of rotate.                                  *)
(* ------------------------------------------------------------------------- *)

(* Define cycle as action from Z_n to Necklace n a *)

(* Cycle as nth iterate of rotate *)
val cycle_def = Define `
  cycle n l = FUNPOW (\l. DROP 1 l ++ TAKE 1 l) n l
`;

(* Notes: This is defined for all n, even n > LENGTH l.

Examples:

EVAL ``cycle 0 [2;3;4;5]``;
> val it = |- cycle 0 [2; 3; 4; 5] = [2; 3; 4; 5] : thm
EVAL ``cycle 1 [2;3;4;5]``;
> val it = |- cycle 1 [2; 3; 4; 5] = [3; 4; 5; 2] : thm
EVAL ``cycle 2 [2;3;4;5]``;
> val it = |- cycle 2 [2; 3; 4; 5] = [4; 5; 2; 3] : thm
EVAL ``cycle 3 [2;3;4;5]``;
> val it = |- cycle 3 [2; 3; 4; 5] = [5; 2; 3; 4] : thm
EVAL ``cycle 4 [2;3;4;5]``;
> val it = |- cycle 4 [2; 3; 4; 5] = [2; 3; 4; 5] : thm
EVAL ``cycle 7 [2;3;4;5]``;
> val it = |- cycle 7 [2; 3; 4; 5] = [5; 2; 3; 4] : thm
*)

(* ------------------------------------------------------------------------- *)
(* Properties of Cycles: additive.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: cycle 0 l = l *)
(* Proof:
     cycle 0 l
   = FUNPOW rotate 0 l
   = l                   by FUNPOW_0: FUNPOW 0 x = x.
*)
val CYCLE_0 = store_thm(
  "CYCLE_0",
  ``!l. cycle 0 l = l``,
  SRW_TAC [][cycle_def]);

(* Theorem: Cycle (n+1) of a list is cycle once more of cycle n list.
            cycle (SUC n) l = cycle 1 (cycle n l) *)
(* Proof:
     cycle (SUC n) l
   = FUNPOW rotate (SUC n) l       by cycle_def
   = rotate (FUNPOW rotate) n l)   by FUNPOW_SUC
   = rotate (cycle n l)            by cycle_def
   = FUNPOW rotate 1 (cycle n l)   by FUNPOW_1
   = cycle 1 (cycle n l)           by cycle_def
*)
val CYCLE_SUC = prove(
  ``!l n. cycle (SUC n) l = cycle 1 (cycle n l)``,
  METIS_TAC [cycle_def, arithmeticTheory.FUNPOW_SUC, arithmeticTheory.FUNPOW_1]);

(* Theorem: Cycle is additive, (cycle n (cycle m l) = cycle (n+m) l  *)
(* Proof:
   By induction on n.
   When n = 0,
     cycle 0 (cycle m l) = cycle m l = cycle (0+m) l.
   When n = SUC n,
     cycle (SUC n) (cycle m l)
   = cycle 1 (cycle n (cycle m l))  by CYCLE_SUC
   = cycle 1 (cycle (n+m) l)        by induction hypothesis.
   = cycle (SUC (n+m)) l            by CYCLE_SUC
   = cycle (SUC n + m) l            by arithmetic
*)
val CYCLE_ADD = store_thm(
  "CYCLE_ADD",
  ``!l n m. cycle n (cycle m l) = cycle (n+m) l``,
  SRW_TAC [][cycle_def, arithmeticTheory.FUNPOW_ADD]);

(* ------------------------------------------------------------------------- *)
(* How cycle affects a list.                                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Only NIL will cycle to NIL.
            cycle n l = [] <=> l = [] *)
(* Proof:
   By induction on n:
   Base case is true by CYCLE_0.
   For the if part: cycle (SUC n) l = [] ==> l = []
   [] = cycle (SUC n) l
      = cycle (n + 1) l       by arithmetic
      = cycle n (cycle 1 l))  by CYCLE_ADD
   ==> cycle 1 l = []         by inductive hypothesis
   ==> rotate 1 = []          by cycle_def
   ==> l = []                 by ROTATE_NIL
   For the only-if part: l = [] ==> cycle (SUC n) l = []
     cycle (SUC n) []
   = cycle 1 (cycle n [])     by CYCLE_SUC
   = cycle 1 []               by inductive hypothesis
   = rotate []                by cycle_def
   = []                       by PREMUTE_NIL
*)
val CYCLE_NIL = store_thm(
  "CYCLE_NIL",
  ``!l n. (cycle n l = []) <=> (l = [])``,
  `!l. (cycle 1 l = []) <=> (l = [])`
      by (SRW_TAC [][cycle_def, EQ_IMP_THM] THEN
          Cases_on `l` THEN FULL_SIMP_TAC (srw_ss()) []) THEN
  Induct_on `n` THEN1 SRW_TAC [][CYCLE_0] THEN
  SRW_TAC [][CYCLE_SUC]);

(* Theorem: Cycle keeps LENGTH (of necklace), or LENGTH (cycle n l) = LENGTH l *)
(* Proof: by induction on n.
     LENGTH (cycle (SUC n) l)
   = LENGTH (cycle 1 (cycle n l))      by CYCLE_SUC
   = LENGTH (rotate (cycle n l))       by CYCLE_1_EQ_ROTATE
   = LENGTH (cycle n l)                by ROTATE_SAME_LENGTH
   = LENGTH l                          by inductive hypothesis
*)
val CYCLE_SAME_LENGTH = store_thm(
  "CYCLE_SAME_LENGTH",
  ``!l n. LENGTH (cycle n l) = LENGTH l``,
  `!l. LENGTH (cycle 1 l) = LENGTH l`
      by (SRW_TAC [][cycle_def] THEN Cases_on `l` THEN
          SRW_TAC [ARITH_ss][]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* Theorem: Cycle keep set (of colors), or set (cycle n l) = set l *)
(* Proof: by induction on n.
     set (cycle (SUC n) l))
   = set (cycle 1 (cycle n l)      by CYCLE_SUC
   = set (rotate (cycle n l))      by CYCLE_1_EQ_ROTATE
   = set (cycle n l)               by ROTATE_SAME_SET
   = set l                         by inductive hypothesis
*)
val CYCLE_SAME_SET = store_thm(
  "CYCLE_SAME_SET",
  ``!l n. set (cycle n l) = set l``,
  `!l. set (cycle 1 l) = set l`
      by (Cases_on `l` THEN SRW_TAC [][cycle_def, EXTENSION, DISJ_COMM]) THEN
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* ------------------------------------------------------------------------- *)
(* To show:  cycle x (cycle y l) = cycle ((x + y) MOD n) l, n = LENGTH l.    *)
(* This is required for Group Action.                                        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For n <= LENGTH l, cycle n l = DROP n l ++ TAKE n l *)
(* Proof: by induction on n.
     cycle (SUC n) l
   = cycle 1 (cycle n l)              by CYCLE_SUC
   = cycle 1 (DROP n l ++ TAKE n l)   by inductive hypothesis
   = rotate (DROP n l ++ TAKE n l)    by CYCLE_1_EQ_ROTATE
   = DROP 1 (DROP n l ++ TAKE n l) ++
     TAKE 1 (DROP n l ++ TAKE n l)    by rotate_def
   = (DROP 1 (DROP n l) ++ TAKE n l) ++
      TAKE 1 (DROP n l)               by DROP_1_APPEND, TAKE_1_APPEND, assume l <> []
   = DROP (SUC n) l ++ TAKE (SUC n) l by DROP_SUC, TAKE_SUC
*)
val CYCLE_EQ_DROP_TAKE = store_thm(
  "CYCLE_EQ_DROP_TAKE",
  ``!l n. n <= LENGTH l ==> (cycle n l = DROP n l ++ TAKE n l)``,
  REPEAT STRIP_TAC THEN
  Cases_on `l = []` THEN1 (
    `LENGTH l = 0` by SRW_TAC [][LENGTH_NIL] THEN
    `n = 0` by RW_TAC arith_ss [] THEN
    SRW_TAC [][CYCLE_0]) THEN
  Induct_on `n` THEN1 SRW_TAC [][CYCLE_0] THEN
  SRW_TAC [][CYCLE_SUC] THEN
  `n < LENGTH l` by RW_TAC arith_ss [] THEN
  SRW_TAC [ARITH_ss][] THEN
  SRW_TAC [][cycle_def] THEN
  SRW_TAC [][DROP_SUC, TAKE_SUC, DROP_1_APPEND, TAKE_1_APPEND, DROP_NON_NIL]);

(* Theorem: Cycle through length gives original, or cycle (LENGTH l) l = l. *)
(* Proof:
     cycle (LENGTH l) l
   = DROP (LENGTH l) l ++ TAKE (LENGTH l) l   by CYCLE_EQ_DROP_TAKE
   = [] ++ l                                  by DROP_LENGTH_NIL, TAKE_LENGTH_ID
   = l
*)
val CYCLE_BACK = store_thm(
  "CYCLE_BACK",
  ``!l. cycle (LENGTH l) l = l``,
  SRW_TAC [][CYCLE_EQ_DROP_TAKE, DROP_LENGTH_NIL, TAKE_LENGTH_ID]);

(* Theorem: cycle (n*LENGTH l) l = l. *)
(* Proof: by induction on n.
     cycle (SUC n * LENGTH l) l
   = cycle (LENGTH l + n * LENGTH l) l          by MULT_SUC
   = cycle (LENGHT l) (cycle (n * LENGHT l) l)  by CYCLE_ADD
   = cycle (LENGHT l) l                         by inductive hypothesis
   = l                                          by CYCLE_BACK
*)
val CYCLE_BACK_MULTIPLE = store_thm(
  "CYCLE_BACK_MULTIPLE",
  ``!l n. cycle (n * LENGTH l) l = l``,
  Induct_on `n` THEN
  SRW_TAC [][CYCLE_0] THEN
  `SUC n * LENGTH l = LENGTH l + n * LENGTH l` by RW_TAC arith_ss [arithmeticTheory.MULT_SUC] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK]);

(* Theorem: if l <> [], cycle n l = cycle (n MOD (LENGTH l)) l *)
(* Proof:
     cycle n l
   = cycle (n DIV (LENGTH l) * (LENGTH l) + n MOD (LENGTH l)) l         by DIVISION
   = cycle (n MOD (LENGTH l) + n DIV (LENGTH l) * (LENGTH l)) l         by arithmetic
   = cycle (n MOD (LENGTH l)) (cycle (n DIV (LENGTH l) * (LENGTH l)) l) by CYCLE_ADD
   = cycle (n MOD (LENGTH l)) l                                         by CYCLE_BACK_MULTIPLE
*)
val CYCLE_MOD_LENGTH = store_thm(
  "CYCLE_MOD_LENGTH",
  ``!l n. l <> [] ==> (cycle n l = cycle (n MOD (LENGTH l)) l)``,
  REPEAT STRIP_TAC THEN
  `0 < LENGTH l` by SRW_TAC [][LENGTH_NON_NIL] THEN
  `n = n DIV LENGTH l * LENGTH l + n MOD LENGTH l` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = n MOD LENGTH l + n DIV LENGTH l * LENGTH l` by RW_TAC arith_ss [] THEN
  METIS_TAC [CYCLE_ADD, CYCLE_BACK_MULTIPLE]);

(* Theorem: cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l *)
(* Proof:
     cycle x (cycle y l)
   = cycle (x+y) l                  by CYCLE_ADD
   = cycle ((x+y) MOD LENGTH l) l   by CYCLE_MOD_LENGTH
*)
val CYCLE_ADDITION = store_thm(
  "CYCLE_ADDITION",
  ``!l x y. l <> [] ==> (cycle x (cycle y l) = cycle ((x + y) MOD LENGTH l) l)``,
  METIS_TAC [CYCLE_ADD, CYCLE_MOD_LENGTH]);

(* Theorem: A cycle can be undone by another cycle:
            cycle m (cycle n l) = l  when  m+n = LENGTH l. *)
(* Proof:
     cycle (LENGTH l - n) (cycle n l)
   = cycle (LENGTH l - n + n) l        by CYCLE_ADD
   = cycle (LENGTH l) l                by arithmetic
   = l                                 by CYCLE_BACK
*)
val CYCLE_INV = store_thm(
  "CYCLE_INV",
  ``!l n. n <= LENGTH l ==> (cycle (LENGTH l - n) (cycle n l) = l)``,
  REPEAT STRIP_TAC THEN
  `(LENGTH l - n) + n = LENGTH l` by RW_TAC arith_ss [] THEN
  SRW_TAC [][CYCLE_ADD, CYCLE_BACK]);

(* ------------------------------------------------------------------------- *)
(* Some results related to: cycle 1 l = l.                                   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: cycle 1 l = l makes cycle trivial, or
            cycle 1 l = l ==> cycle n l = l       *)
(* Proof:
   By induction on n:
   Base case is to show cycle 0 l = l, true by CYCLE_0.
   Step case is:
     cycle (SUC n) l
   = cycle 1 (cycle n l)    by CYCLE_SUC
   = cycle 1 l              by inductive hypothesis
   = l                      by given
*)
val CYCLE_1_FIX = store_thm(
  "CYCLE_1_FIX",
  ``!l n. (cycle 1 l = l) ==> (cycle n l = l)``,
  Induct_on `n` THEN SRW_TAC [][CYCLE_0, CYCLE_SUC]);

(* Theorem: A mono-list l has cycle 1 l = l, or
            LENGTH l = 1 ==> cycle 1 l = l      *)
(* Proof:
   Since LENGTH l <> 0, l <> []    by LENGTH_NIL
   hence  l = h::t                 by list_CASES
   LENGTH l = 1 ==> LENGTH t = 0   by LENGTH_APPEND, and h::t = [h] ++ t.
   hence  t = []                   by LENGTH_NIL
   Therefore by definitions:
     cycle 1 l
   = cycle 1 [h]
   = rotate [h]
   = [h]
   = l
*)
val MONO_HAS_CYCLE_1 = store_thm(
  "MONO_HAS_CYCLE_1",
  ``!l. (LENGTH l = 1) ==> (cycle 1 l = l)``,
  REPEAT STRIP_TAC THEN
  `LENGTH l <> 0` by RW_TAC arith_ss [] THEN
  `?h t. l = h::t` by METIS_TAC [LENGTH_NIL, list_CASES] THEN
  `_ = [h] ++ t` by SRW_TAC [][] THEN
  `LENGTH [h] = 1` by RW_TAC arith_ss [LENGTH] THEN
  `LENGTH t = 0` by FULL_SIMP_TAC (srw_ss()) [LENGTH_APPEND] THEN
  `t = []` by METIS_TAC [LENGTH_NIL] THEN
  SRW_TAC [][cycle_def]);

(* Theorem: A mono-list has cycle 1 l = l *)
val SING_HAS_CYCLE_1 = store_thm(
  "SING_HAS_CYCLE_1",
  ``!l. SING (set l) ==> (cycle 1 l = l)``,
  SRW_TAC [][cycle_def] THEN
  Cases_on `l` THEN SRW_TAC [][] THEN
  FULL_SIMP_TAC (srw_ss()) [SING_INSERT] THEN
  `SING (set (t ++ [h]))` by SRW_TAC [][SING_UNION] THEN
  SRW_TAC [ARITH_ss][MONOLIST_EQ]);

(* Theorem: cycle 1 (h::t) = t ++ [h] *)
val CYCLE_1_EQ = store_thm(
  "CYCLE_1_EQ",
  ``!h t. cycle 1 (h::t) = t ++ [h]``,
  SRW_TAC [][cycle_def]);

(* Theorem: (t ++ [h] <> h::t) if (set t) has some element h' <> h. *)
val CYCLE_1_NEQ = store_thm(
  "CYCLE_1_NEQ",
  ``!h h' t. h' IN set t /\ h' <> h ==> (t ++ [h] <> h::t)``,
  SRW_TAC [][] THEN
  Induct_on `t` THEN SRW_TAC [][] THEN
  METIS_TAC []);

(* Theorem: [inverse of SING_HAS_CYCLE_1]
            !l. (cycle 1 l = l) ==> SING (set l)  *)
val CYCLE_1_NONEMPTY_MONO = store_thm(
  "CYCLE_1_NONEMPTY_MONO",
  ``!l. l <> [] /\ (cycle 1 l = l) ==> SING (set l)``,
  REPEAT STRIP_TAC THEN
  SPOSE_NOT_THEN ASSUME_TAC THEN
  `?h t. l = h::t` by METIS_TAC [list_CASES] THEN SRW_TAC [][] THEN
  `?h'. h' IN set t /\ h' <> h` by SRW_TAC [][NON_MONO_TAIL_PROPERTY] THEN
  METIS_TAC [CYCLE_1_EQ, CYCLE_1_NEQ]);

(* ------------------------------------------------------------------------- *)

(* For revised necklace proof using GCD. *)

(* Theorem: If a value n can cycle back, multiples of n can also cycle back. *)
(* Proof: by induction on m. *)
val CYCLE_MULTIPLE_BACK = store_thm(
  "CYCLE_MULTIPLE_BACK",
  ``!n l. (cycle n l = l) ==> !m. cycle (m*n) l = l``,
  REPEAT STRIP_TAC THEN
  Induct_on `m` THEN
  METIS_TAC [arithmeticTheory.MULT_CLAUSES, CYCLE_0, CYCLE_ADD]);


(* Theorem: If two values m, n can cycle back, gcd(m,n) can also cycle back. *)
(* Proof:
     If n = 0, cycle (gcd m 0) l = cycle m l = l by given, GCD_0R.
     If n <> 0, ?p q. p * n = q * m + gcd m n    by LINEAR_GCD,

     cycle (gcd m n) l
   = cycle (gcd m n) (cycle m l)                 by given
   = cycle (gcd m n) (cycle q*m l)               by CYCLE_MULTIPLE_BACK
   = cycle (gcd m n + q*m) l                     by CYCLE_ADD
   = cycle (q*m + gcd m n) l                     by arithmetic
   = cycle (p*n) l                               by substitution
   = cycle n l                                   by CYCLE_MULTIPLE_BACK
   = l                                           by given
*)
val CYCLE_GCD_BACK = store_thm(
  "CYCLE_GCD_BACK",
  ``!m n l. (cycle m l = l) /\ (cycle n l = l) ==> (cycle (gcd m n) l = l)``,
  REPEAT STRIP_TAC THEN
  Cases_on `n=0` THEN1
  METIS_TAC [gcdTheory.GCD_0R] THEN
  `?p q. p * n = q * m + gcd m n` by METIS_TAC [gcdTheory.LINEAR_GCD] THEN
  `cycle (gcd m n) l = cycle (gcd m n) (cycle (q*m) l)` by METIS_TAC [CYCLE_MULTIPLE_BACK] THEN
  METIS_TAC [CYCLE_ADD, arithmeticTheory.ADD_COMM, CYCLE_MULTIPLE_BACK]);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
