(* ------------------------------------------------------------------------- *)
(* Helper Theorems - a collection of useful results -- for Sets.             *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "helperSet";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* val _ = load "dividesTheory"; *)
open pred_setTheory dividesTheory;

(* val _ = load "helperNumTheory"; *)
open helperNumTheory;


(* ------------------------------------------------------------------------- *)
(* More for pred_setTheory                                                   *)
(* ------------------------------------------------------------------------- *)

(* Singletons  *)

(* Theorem: A non-empty set with all elements equal to a is the singleton {a} *)
val ONE_ELEMENT_SING = store_thm(
  "ONE_ELEMENT_SING",
  ``!s a. s <> {} /\ (!k. k IN s ==> (k = a)) ==> (s = {a})``,
  SRW_TAC [][EXTENSION, EQ_IMP_THM] THEN METIS_TAC []);

(* Union and Intersection *)

(* Theorem: (s DIFF t) and t are DISJOINT. *)
val DIFF_DISJOINT = store_thm(
  "DIFF_DISJOINT",
  ``!s t. DISJOINT (s DIFF t) t``,
  SRW_TAC [][DISJOINT_DEF, DIFF_DEF, EXTENSION] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)
(* Mapping Images                                                            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For a 1-1 map f: s -> s, s and (IMAGE f s) are of the same size. *)
val CARD_IMAGE = store_thm(
  "CARD_IMAGE",
  ``!s f. (!x y. (f x = f y) <=> (x = y)) /\ FINITE s ==> (CARD (IMAGE f s) = CARD s)``,
  Q_TAC SUFF_TAC
    `!f. (!x y. (f x = f y) = (x = y)) ==> !s. FINITE s ==> (CARD (IMAGE f s) = CARD s)`
    THEN1 METIS_TAC [] THEN
  GEN_TAC THEN STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][]);

(* Theorem: For any map f: s -> s, CARD (IMAGE f s) <= CARD s. *)
val CARD_IMAGE_LE_CARD = store_thm(
  "CARD_IMAGE_LE_CARD",
  ``!s. FINITE s ==> !f. CARD (IMAGE f s) <= CARD s``,
  HO_MATCH_MP_TAC FINITE_INDUCT THEN
  SRW_TAC [][] THEN
  SRW_TAC [][] THEN
  `CARD (IMAGE f s) <= (CARD s)` by SRW_TAC [][] THEN
  `(CARD s) <= SUC (CARD s)`
    by RW_TAC arith_ss [arithmeticTheory.LESS_EQ_LESS_TRANS] THEN
  METIS_TAC [arithmeticTheory.LESS_EQ_TRANS]);

(* Theorem: If !e IN s, CARD e = k, SIGMA CARD s = CARD s * k. *)
val SIGMA_CARD_CONSTANT = store_thm(
  "SIGMA_CARD_CONSTANT",
  ``!k s. FINITE s ==> (!e. e IN s ==> (CARD e = k)) ==> (SIGMA CARD s = k * (CARD s))``,
  STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN SRW_TAC [][] THEN1 METIS_TAC [SUM_IMAGE_THM] THEN
  `CARD e = k` by SRW_TAC [][] THEN
  `SIGMA CARD (e INSERT s) = CARD e + SIGMA CARD (s DELETE e)` by SRW_TAC [][SUM_IMAGE_THM] THEN
  `s DELETE e = s` by METIS_TAC [DELETE_NON_ELEMENT] THEN
  `SUC (CARD s) = 1 + CARD s` by RW_TAC arith_ss [] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  RW_TAC arith_ss []);

(* Theorem: If n divides CARD e for all e in s, then n divides SIGMA CARD s. *)
(* Proof:
   Use finite induction and SUM_IMAGE_THM.
   Base case: divides n (SIGMA CARD {})
        !f. SIGMA f {} = 0  by SUM_IMAGE_THM
        Hence true by ALL_DIVIDES_0: divides a 0.
   Step case: e NOTIN s ==> divides n (SIGMA CARD (e INSERT s))
        SIGMA CARD (e INSERT s) = CARD e + SIGMA CARD (s DELETE e)   by SUM_IMAGE_THM
                                = CARD e + SIGMA CARD s              by DELETE_NON_ELEMENT
        Hence true by DIVIDES_ADD_1: (divides a b) /\ (divides a c) ==> divides a (b + c).
*)
val DIVIDES_SIGMA_CARD = store_thm(
  "DIVIDES_SIGMA_CARD",
  ``!n s. FINITE s ==> (!e. e IN s ==> divides n (CARD e)) ==> divides n (SIGMA CARD s)``,
  STRIP_TAC THEN
  HO_MATCH_MP_TAC FINITE_INDUCT THEN
  SRW_TAC [][] THEN1
  METIS_TAC [SUM_IMAGE_THM, dividesTheory.ALL_DIVIDES_0] THEN
  METIS_TAC [SUM_IMAGE_THM, DELETE_NON_ELEMENT, dividesTheory.DIVIDES_ADD_1]);

(* Theorem: For all f: s -> t, (IMAGE f s) subset t *)
val MAP_IMAGE_SUBSET = store_thm(
  "MAP_IMAGE_SUBSET",
  ``!f s t. (!x. x IN s ==> f x IN t) ==> (IMAGE f s) SUBSET t``,
  SRW_TAC [][IMAGE_DEF, SUBSET_DEF] THEN
  RW_TAC std_ss []);

(* Theorem: For finite s, self injection means INJ f s (IMAGE f s) *)
val INJ_INJ_IMAGE = store_thm(
  "INJ_INJ_IMAGE",
  ``!f s. INJ f s s ==> INJ f s (IMAGE f s)``,
  SRW_TAC [][INJ_DEF] THEN
  Q.EXISTS_TAC `x` THEN
  RW_TAC std_ss []);

(* Theorem: For finite s, self injection means IMAGE f s = s *)
(* Proof:
   For any map, (IMAGE f s) SUBSET s.
   So suppose   (IMAGE f s) <> s, then (IMAGE f s) PSUBSET s.
   This means   CARD (IMAGE f s) < CARD s, for FINITE s.
   For an injective map f, this violates the Pigeon-hole Principle.
*)
val FINITE_INJ_IMAGE_EQ = store_thm(
  "FINITE_INJ_IMAGE_EQ",
  ``!s f. FINITE s /\ INJ f s s ==> (IMAGE f s = s)``,
  REPEAT STRIP_TAC THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `FINITE (IMAGE f s)` by SRW_TAC [][IMAGE_FINITE] THEN
  `INJ f s (IMAGE f s)` by RW_TAC std_ss [INJ_INJ_IMAGE] THEN
  `!x. x IN s ==> f x IN s` by METIS_TAC [INJ_DEF] THEN
  `(IMAGE f s) SUBSET s` by SRW_TAC [][MAP_IMAGE_SUBSET] THEN
  `(IMAGE f s) PSUBSET s` by SRW_TAC [][PSUBSET_DEF] THEN
  `CARD (IMAGE f s) < CARD s` by SRW_TAC [][CARD_PSUBSET] THEN
  METIS_TAC [PHP]); (* invoke Pigeon-hole Principle *)

(* ------------------------------------------------------------------------- *)
(* Partitions                                                                *)
(* ------------------------------------------------------------------------- *)

(* Theorem: Memebers of partition class are in target set. *)
val PARTITION_INSIDE_ELEMENT = store_thm(
  "PARTITION_INSIDE_ELEMENT",
  ``!a e R s. a IN e /\ e IN (partition R s) ==> a IN s``,
  SRW_TAC [][partition_def, EXTENSION] THEN METIS_TAC []);

(* Theorem: When the partitions are equal size of n, CARD s = n * CARD (partition of s) *)
(* Proof:
   FINITE (partition R s)               by FINITE_partition
   CARD s = SIGMA CARD (partition R s)  by partition_CARD
          = n  * CARD (partition R s)   by SIGMA_CARD_CONSTANT
*)
val equal_partition_CARD = store_thm(
  "equal_partition_CARD",
  ``!R s n. R equiv_on s /\ FINITE s /\
    (!t. t IN partition R s ==> (CARD t = n)) ==>
    (CARD s = n * CARD (partition R s))``,
  METIS_TAC [partition_CARD, FINITE_partition, SIGMA_CARD_CONSTANT]);

(* Theorem: When the partition size has a factor n, then n divides CARD s  *)
(* Proof:
   FINITE (partition R s)               by FINITE_partition
   CARD s = SIGMA CARD (partition R s)  by partition_CARD
   n divides CARD e for e in (partition R s)
   ==> n divids SIGMA CARD (partition R s)  by DIVIDES_SIGMA_CARD
   hence n divdes CARD s.
*)
val factor_partition_CARD = store_thm(
  "factor_partition_CARD",
  ``!R s n. R equiv_on s /\ FINITE s /\
   (!t. t IN partition R s ==> divides n (CARD t)) ==> divides n (CARD s)``,
  METIS_TAC [partition_CARD, FINITE_partition, DIVIDES_SIGMA_CARD]);

(* ------------------------------------------------------------------------- *)
(* residue -- close-relative of COUNT                                        *)
(* ------------------------------------------------------------------------- *)

(* Define the set of residues = nonzero remainders *)
val residue_def = Define `residue n = { i | (0 < i) /\ (i < n) }`;

(* Theorem: residue 1 = EMPTY *)
val RESIDUE_1_EMPTY = store_thm(
  "RESIDUE_1_EMPTY",
  ``residue 1 = {}``,
  SRW_TAC [ARITH_ss][residue_def, EXTENSION]);

(* Theorem: For n > 0, residue (SUC n) = n INSERT residue n *)
val RESIDUE_INSERT = store_thm(
  "RESIDUE_INSERT",
  ``!n. 0 < n ==> (residue (SUC n) = n INSERT residue n)``,
  SRW_TAC [ARITH_ss][residue_def, EXTENSION]);

(* Theorem: (residue n) DELETE n = residue n *)
(* Proof: Because n is not in (residue n). *)
val RESIDUE_DELETE_N = store_thm(
  "RESIDUE_DELETE_N",
  ``!n. 0 < n ==> ((residue n) DELETE n = residue n)``,
  REPEAT STRIP_TAC THEN
  `n NOTIN (residue n)` by SRW_TAC [][residue_def] THEN
  METIS_TAC [DELETE_NON_ELEMENT]);

(* Theorem: count n = 0 INSERT (residue n) *)
val RESIDUE_COUNT = store_thm(
  "RESIDUE_COUNT",
  ``!n. 0 < n ==> (count n = 0 INSERT (residue n))``,
  SRW_TAC [ARITH_ss][residue_def, EXTENSION]);

(* Theorem: FINITE (residue n) *)
val FINITE_RESIDUE = store_thm(
  "FINITE_RESIDUE",
  ``!n. FINITE (residue n)``,
  Cases THEN1 SRW_TAC [][residue_def] THEN
  METIS_TAC [RESIDUE_COUNT, FINITE_INSERT, count_def, FINITE_COUNT,
             DECIDE ``0 < SUC n``]);

(* Theorem: For n > 0, CARD (residue n) = n-1 *)
(* Proof:
   Since 0 INSERT (residue n) = count n, the result follows.
*)
val CARD_RESIDUE = store_thm(
  "CARD_RESIDUE",
  ``!n. 0 < n ==> (CARD (residue n) = n-1)``,
  REPEAT STRIP_TAC THEN
  `0 NOTIN (residue n)` by SRW_TAC [][residue_def] THEN
  `0 INSERT (residue n) = count n`
    by SRW_TAC [][residue_def, EXTENSION, EQ_IMP_THM] THEN RW_TAC arith_ss [] THEN
  `SUC (CARD (residue n)) = n` by METIS_TAC [FINITE_RESIDUE, CARD_INSERT, CARD_COUNT] THEN
  RW_TAC arith_ss []);

(* Theorem: For prime m, a in residue m, n <= m, a*n MOD m <> a*x MOD m  for all x in residue n *)
(* Proof:
   Assume the contrary, that a*n MOD m = a*x MOD m
   Since a in residue m and m is prime, MOD_MULT_LCANCEL gives: n MOD m = x MOD m
   If n = m, n MOD m = 0, but x MOD m <> 0, hence contradication.
   If n < m, then since x < n <= m, n = x, contradicting x < n.
*)
val RESIDUE_PRIME_NEQ = store_thm(
  "RESIDUE_PRIME_NEQ",
  ``!p a n. prime p /\ a IN (residue p) /\ n <= p ==>
    !x. x IN (residue n) ==> (a*n) MOD p <> (a*x) MOD p``,
  SRW_TAC [][residue_def] THEN
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  `0 < p` by SRW_TAC [][dividesTheory.PRIME_POS] THEN
  `(a MOD p <> 0) /\ (x MOD p <> 0)` by RW_TAC arith_ss [] THEN
  `n MOD p = x MOD p` by METIS_TAC [MOD_MULT_LCANCEL] THEN
  Cases_on `n = p` THEN1 METIS_TAC [arithmeticTheory.DIVMOD_ID] THEN
  `n < p` by RW_TAC arith_ss [] THEN
  `(n MOD p = n) /\ (x MOD p = x)` by RW_TAC arith_ss [] THEN
  RW_TAC arith_ss []);

(* ------------------------------------------------------------------------- *)
(* PROD IMAGE and PROD_SET -- similar to SUM_IMAGE and SUM_SET               *)
(* Now in HOL-4 [Kananaskis 8]                                               *)
(* ------------------------------------------------------------------------- *)

(* Theorem: PROD_SET (residue n) = FACT (n-1) *)
(* true for all n because of the (ugly) way that 0-1 = 0 *)
(* Proof:
   By induction on n.
   Base case should be n = 1:
     PROD_SET (residue 1)
   = PROD_SET {0}
   = 1
   = FACT (0)
   = FACT (1-1)

   Step case has the inductive hypothesis, then:
     PROD_SET (residue (SUC n))
   = PROD_SET (n INSERT residue n)
   = n * PROD_SET ((residue n) DELETE n)
   = n * PROD_SET (residue n)     since n NOTIN (residue n)
   = n * FACT (n-1)
   = FACT (n)
   = FACT ((SUC n) - 1)
*)
val PROD_SET_RESIDUE = store_thm(
  "PROD_SET_RESIDUE",
  ``!n. PROD_SET (residue n) = FACT (n-1)``,
  GEN_TAC THEN
  `(n = 0) \/ 0 < n`  by DECIDE_TAC THEN1
    SRW_TAC [][residue_def, arithmeticTheory.FACT, PROD_SET_EMPTY] THEN
  Induct_on `n` THEN RW_TAC arith_ss [] THEN
  Cases_on `0 < n` THENL [
    `FINITE (residue n)` by SRW_TAC [][FINITE_RESIDUE] THEN
    `residue (SUC n) = n INSERT residue n` by SRW_TAC [][RESIDUE_INSERT] THEN
    `PROD_SET (n INSERT (residue n)) = n * PROD_SET ((residue n) DELETE n)`
       by SRW_TAC [][PROD_SET_THM] THEN
    `(residue n) DELETE n = residue n` by SRW_TAC [][RESIDUE_DELETE_N] THEN
    `n = SUC (n-1)` by RW_TAC arith_ss [] THEN
    METIS_TAC [arithmeticTheory.FACT],
    `n = 0` by DECIDE_TAC THEN
    SRW_TAC [][RESIDUE_1_EMPTY, arithmeticTheory.FACT, PROD_SET_THM]
  ]);

(* Theorem: partition elements are subset of target *)
(* Proof: by definition of partition. *)
val partition_element_SUBSET = store_thm(
  "partition_element_SUBSET",
  ``!R s t. t IN (partition R s) ==> t SUBSET s``,
  SRW_TAC [][partition_def, EXTENSION, EQ_IMP_THM] THEN
  METIS_TAC [SUBSET_DEF]);
(* Note: no need of R equiv_on s for pred_setTheory.partition_SUBSET *)

(* Theorem: FINITE s ==> FINITE (partition R s)  *)
(* Proof:
   Since !t. t IN (partition R s), t SUBSET s   by partition_element_SUBSET
   Hence  (partition R s) SUBSET (POW s)        by SUBSET_DEF
   hence FINITE (partition R s)                 by FINITE_POW
*)
val partition_FINITE = store_thm(
  "partition_FINITE",
  `` !R s. FINITE s ==> FINITE (partition R s)``,
  REPEAT STRIP_TAC THEN
  `!t. t IN (partition R s) ==> t SUBSET s` by METIS_TAC [partition_element_SUBSET] THEN
  `!t. t IN (partition R s) ==> t IN (POW s)` by SRW_TAC [][POW_DEF] THEN
  METIS_TAC [SUBSET_DEF, FINITE_POW, SUBSET_FINITE]);
(* Note: no need of R equiv_on s for pred_setTheory.FINITE_partition *)

(* Theorem: FINITE {P x | x < n}  *)
(* Proof:
   Since IMAGE (\i. P i) (count n) = {P x | x < n},
   this follows by
   - IMAGE_FINITE;
   > val it = |- !s. FINITE s ==> !f. FINITE (IMAGE f s) : thm
   - FINITE_COUNT;
> val it = |- !n. FINITE (count n) : thm
*)
val FINITE_COUNT_IMAGE = store_thm(
  "FINITE_COUNT_IMAGE",
  ``!P n. FINITE {P x | x < n }``,
  REPEAT STRIP_TAC THEN
  `IMAGE (\i. P i) (count n) = {P x | x < n}` by SRW_TAC [][IMAGE_DEF] THEN
  METIS_TAC [IMAGE_FINITE, FINITE_COUNT]);

(* ------------------------------------------------------------------------- *)
(* Pre-image of IN_IMAGE.                                                    *)
(* ------------------------------------------------------------------------- *)

(*
- IN_IMAGE;
> val it = |- !y s f. y IN IMAGE f s <=> ?x. (y = f x) /\ x IN s : thm
*)

(* Existence of pre-image: y IN IMAGE f s ==> ?x. (y = f x) /\ x IN s *)
(*
val lemma = prove(
  ``!f s y. ?x. y IN IMAGE f s ==> x IN s /\ (y = f x)``,
  METIS_TAC [IN_IMAGE]);
*)
(* Define preimage *)
(*
val preimage_def = new_specification(
    "preimage_def",
    ["preimage"],
    SIMP_RULE (bool_ss) [SKOLEM_THM] lemma);
Notes:
- use of srw_ss() will expand IN IMAGE, so use a plain one: bool_ss
- rename f' to f using CONV_RULE and RENAME_VARS_CONV
> val preimage_def =
    |- !f' s y. y IN IMAGE f' s ==> preimage f' s y IN s /\ (y = f' (preimage f' s y)) : thm
*)
(*
val preimage_def = new_specification(
    "preimage_def",
    ["preimage"],
    SIMP_RULE bool_ss [SKOLEM_THM] lemma
      |> CONV_RULE (RENAME_VARS_CONV ["g"] THENC
                    BINDER_CONV (RENAME_VARS_CONV ["f"])))
*)
(*
> val preimage_def =
    |- !f s y. y IN IMAGE f s ==> preimage f s y IN s /\ (y = f (preimage f s y)) : thm
*)
(* Michael reckons that this should be defined as a set:

   preimage f s y = { x | x IN s /\ f x = y }

  and then apply CHOICE on that result to get a particular element.
*)
val preimage_def = Define `preimage f s y = { x | x IN s /\ (f x = y) }`;

(* Theorem: x IN s ==> x IN preimage f s (f x) *)
(* Proof: by IN_IMAGE. preimage_def. *)
val preimage_of_image = store_thm(
  "preimage_of_image",
  ``!f s x. x IN s ==> x IN preimage f s (f x)``,
  SRW_TAC [][preimage_def]);

(* Theorem: y IN (IMAGE f s) ==> CHOICE (preimage f s y) IN s /\ f (CHOICE (preimage f s y)) = y *)
(* Proof:
   (1) prove: y IN IMAGE f s ==> CHOICE (preimage f s y) IN s
   By IN_IMAGE, this is to show:
   x IN s ==> CHOICE (preimage f s (f x)) IN s
   Now, preimage f s (f x) <> {}   since x is a pre-image.
   hence CHOICE (preimage f s (f x)) IN preimage f s (f x) by CHOICE_DEF
   hence CHOICE (preimage f s (f x)) IN s                  by preimage_def
   (2) prove: y IN IMAGE f s /\ CHOICE (preimage f s y) IN s ==> f (CHOICE (preimage f s y)) = y
   By IN_IMAGE, this is to show: x IN s ==> f (CHOICE (preimage f s (f x))) = f x
   Now, x IN preimage f s (f x)   by preimage_of_image
   hence preimage f s (f x) <> {}  by MEMBER_NOT_EMPTY
   thus  CHOICE (preimage f s (f x)) IN (preimage f s (f x))  by CHOICE_DEF
   hence f (CHOICE (preimage f s (f x))) = f x  by preimage_def
*)
val preimage_choice_property = store_thm(
  "preimage_choice_property",
  ``!f s y. y IN (IMAGE f s) ==> CHOICE (preimage f s y) IN s /\ (f (CHOICE (preimage f s y)) = y)``,
  REPEAT GEN_TAC THEN
  STRIP_TAC THEN
  CONJ_ASM1_TAC THENL [
    FULL_SIMP_TAC std_ss [IN_IMAGE] THEN
    `CHOICE (preimage f s (f x)) IN preimage f s (f x)` suffices_by SRW_TAC [][preimage_def] THEN
    METIS_TAC [CHOICE_DEF, preimage_of_image, MEMBER_NOT_EMPTY],
    FULL_SIMP_TAC std_ss [IN_IMAGE] THEN
    `x IN preimage f s (f x)` by RW_TAC std_ss [preimage_of_image] THEN
    `CHOICE (preimage f s (f x)) IN (preimage f s (f x))` by METIS_TAC [CHOICE_DEF, MEMBER_NOT_EMPTY] THEN
    FULL_SIMP_TAC std_ss [preimage_def, GSPECIFICATION]
  ]);

(* ------------------------------------------------------------------------- *)
(* Bijection property.                                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: BIJ f s t = (!x. x IN s ==> f x IN t) /\ (!y. y IN t ==> ?!x. x IN s /\ (f x = y)) *)
(* Proof:
   This is to prove:
   (1) y IN t ==> ?!x. x IN s /\ (f x = y)
       x exists by SURJ_DEF, and x is unique by INJ_DEF.
   (2) x IN s /\ y IN s /\ f x = f y ==> x = y
       true by INJ_DEF.
   (3) x IN t ==> ?y. y IN s /\ (f y = x)
       true by SURJ_DEF.
*)
val BIJ_ALT = store_thm(
  "BIJ_ALT",
  ``!f s t. BIJ f s t = (!x. x IN s ==> f x IN t) /\ (!y. y IN t ==> ?!x. x IN s /\ (f x = y))``,
  RW_TAC std_ss [BIJ_DEF, INJ_DEF, SURJ_DEF, EQ_IMP_THM] THEN
  METIS_TAC []);

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
