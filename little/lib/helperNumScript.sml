(* ------------------------------------------------------------------------- *)
(* Helper Theorems - a collection of useful results -- for Numbers.          *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "helperNum";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open arithmeticTheory dividesTheory gcdTheory;

(* ------------------------------------------------------------------------- *)
(* More for arithmeticTheory                                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: When 1 < n, 0 < a, a < a^n  *)
val EXP_LESS = store_thm(
  "EXP_LESS",
  ``!n a. 1 < n /\ 1 < a ==> a < a**n``,
  Induct_on `n` THEN SRW_TAC [][] THEN
  `a = a**1` by RW_TAC arith_ss [EXP] THEN
  METIS_TAC [EXP_BASE_LT_MONO]);

(* Theorem: When 1 < n, a <= a^n  *)
val EXP_LESS_EQ = store_thm(
  "EXP_LESS_EQ",
  ``!n a. 1 < n ==> a <= a**n``,
  REPEAT STRIP_TAC THEN
  Cases_on `a=0` THEN1 RW_TAC arith_ss [] THEN
  Cases_on `a=1` THEN1 RW_TAC arith_ss [] THEN
  `1 < a` by RW_TAC arith_ss [] THEN
  METIS_TAC [EXP_LESS, LESS_IMP_LESS_OR_EQ]);

(* Theorem: [Modulo Exponentiation]
            a**m MOD n = (a MOD n)**m MOD n *)
(* Proof:
   By induction on m, based on MOD_TIMES2.
*)
val EXP_MOD = store_thm(
  "EXP_MOD",
  ``!n m. 0 < n ==> (a**m MOD n = (a MOD n)**m MOD n)``,
  REPEAT STRIP_TAC THEN
  Induct_on `m` THEN
  SRW_TAC [][EXP] THEN
  `(a * a ** m) MOD n = (a MOD n * a ** m MOD n) MOD n` by SRW_TAC [][MOD_TIMES2] THEN
  `_ = (a MOD n * (a MOD n) ** m MOD n) MOD n` by SRW_TAC [][] THEN
  METIS_TAC [MOD_TIMES2, MOD_MOD]);

(* Theorem: [EXP_ZERO] !n. b**n = 0 ==> z = 0.
   Case z = 0 is trivial.
   Case z <> 0 is contracdtion:
   !m n. (SUC n) ** m <> 0   by NOT_EXP_0
*)
val EXP_ZERO = store_thm(
  "EXP_ZERO",
  ``!b n. (b**n = 0) ==> (b = 0)``,
  REPEAT STRIP_TAC THEN
  Cases_on `b` THEN1
  SRW_TAC [][] THEN
  METIS_TAC [NOT_EXP_0]);

(* Theorem: For any base b, 0 < n ==> b divides b**n  *)
(* Proof:
   Since b**n = b**(SUC k) = b*(b**k), b divides b*(b**k).
*)
val DIVIDES_BASE_BASE_EXP = store_thm(
  "DIVIDES_BASE_BASE_EXP",
  ``!b n. 0 < n ==> divides b (b**n)``,
  REPEAT STRIP_TAC THEN
  `?k. n = SUC k` by METIS_TAC [LESS_STRONG_ADD, ADD_0] THEN
  `b**n = b*(b**k)` by SRW_TAC [][EXP] THEN
  METIS_TAC [divides_def, MULT_SYM]);

(* Theorem: divides n x <=> x MOD n = 0 *)
(* Proof:
   if part: divides n x ==> x MOD n = 0
       divides n x
   ==> ?q. x = q * n         by divides_def
   ==>   x MOD n
       = (q * n) MOD n       by substitution
       = 0                   by MOD_EQ_0
   only-if part: x MOD n = 0 ==> divides n x
   x = (x DIV n) * n + (x MOD n)   by DIVISION
     = (x DIV n) * n               by ADD_0 and given
   Hence  divides n x              by divides_def
*)
val DIVIDES_MOD_0 = store_thm(
  "DIVIDES_MOD_0",
  ``!n x. 0 < n ==> (divides n x <=> (x MOD n = 0))``,
  SRW_TAC [][EQ_IMP_THM] THEN1
  METIS_TAC [divides_def, MOD_EQ_0] THEN
  METIS_TAC [DIVISION, ADD_0, divides_def]);

(* Theorem: If n > 0, (m MOD n = 0) <=> divides n m *)
(* Proof:
   The if part:
       m = (m DIV n)*n + (m MOD n)   by DIVISION.
   ==> m = (m DIV n)*n               by m MOD n = 0, or ADD_0: m + 0 = m.
   ==> divides n m                   by divides_def.
   The only-if part:
       divides n m
   ==> ?q. n = q*m                   by divides_def.
   ==> (q*m) MOD n = 0               by MOD_EQ_0: !k. (k * n) MOD n = 0
*)
val MOD_0_DIVIDES = store_thm(
  "MOD_0_DIVIDES",
  ``!n m. 0 < n ==> ((m MOD n = 0) <=> divides n m)``,
  SRW_TAC [][divides_def, EQ_IMP_THM] THEN
  METIS_TAC [DIVISION, ADD_0, MOD_EQ_0]);

(* Theorem: If n > 0, k MOD n = 0 ==> !x. (k*x) MOD n = 0 *)
(* Proof:
   (k*x) MOD n = (k MOD n * x MOD n) MOD n    by MOD_TIMES2
               = (0 * x MOD n) MOD n          by given
               = 0 MOD n                      by MULT_0 and MULT_COMM
               = 0                            by ZERO_MOD
*)
val MOD_MULITPLE_ZERO = store_thm(
  "MOD_MULITPLE_ZERO",
  ``!n k. 0 < n /\ (k MOD n = 0) ==> !x. ((k*x) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  METIS_TAC [MOD_TIMES2, MULT_0, MULT_COMM, ZERO_MOD]);

(* Theorem: If n > 0, a MOD n = b MOD n ==> (a - b) MOD n = 0 *)
(* Proof:
   a = (a DIV n)*n + (a MOD n)   by DIVISION
   b = (b DIV n)*n + (b MOD n)   by DIVISION
   Hence  a - b = ((a DIV n) - (b DIV n))* n
                = a multiple of n
   Therefore (a - b) MOD n = 0.
*)
val MOD_EQ_DIFF = store_thm(
  "MOD_EQ_DIFF",
  ``!n a b. 0 < n /\ (a MOD n = b MOD n) ==> ((a - b) MOD n = 0)``,
  REPEAT STRIP_TAC THEN
  `a = a DIV n * n + a MOD n` by METIS_TAC [DIVISION] THEN
  `b = b DIV n * n + b MOD n` by METIS_TAC [DIVISION] THEN
  `a - b = (a DIV n - b DIV n) * n` by RW_TAC arith_ss [] THEN
  METIS_TAC [MOD_EQ_0]);
(* Note: The reverse is true only when a >= b:
         (a-b) MOD n = 0 cannot imply a MOD n = b MOD n *)

(* Theorem: if n > 0, a >= b, then (a - b) MOD n = 0 <=> a MOD n = b MOD n *)
(* Proof:
         (a-b) MOD n = 0
   ==>   divides n (a-b)   by MOD_0_DIVIDES
   ==>   (a-b) = k*n       for some k by divides_def
   ==>       a = b + k*n   need b <= a to apply arithmeticTheory.SUB_ADD
   ==> a MOD n = b MOD n   by arithmeticTheory.MOD_TIMES

   The converse is given by MOD_EQ_DIFF.
*)
val MOD_EQ = store_thm(
  "MOD_EQ",
  ``!n a b. 0 < n /\ b <= a ==> (((a - b) MOD n = 0) <=> (a MOD n = b MOD n))``,
  SRW_TAC [][EQ_IMP_THM] THENL [
    `?k. a - b = k * n` by METIS_TAC [DIVIDES_MOD_0, divides_def] THEN
    `a = k*n + b` by RW_TAC arith_ss [] THEN
    METIS_TAC [MOD_TIMES],
    METIS_TAC [MOD_EQ_DIFF]
  ]);
(* Both MOD_EQ_DIFF and MOD_EQ are required in MOD_MULT_LCANCEL *)

(* Theorem: For n > 0, (j MOD n * k) MOD n = (j * k) MOD n *)
(* Proof:
   By division algorithm, k = (k DIV n)*n + (k MOD n)

     (j MOD n * k) MOD n
   = (j MOD n * ((k DIV n)*n + (k MOD n))) MOD n
   = (j MOD n * ((k DIV n)*n MOD n + (k MOD n) MOD n)
   = (j MOD n * (0 + (k MOD n) MOD n)
   = (j MOD n * k MOD n) MOD n
   = (j * k) MOD n    by MOD_TIMES2
*)
val MOD_TIMES_TWO = store_thm(
  "MOD_TIMES_TWO",
  ``!n. 0 < n ==> !j k. (j MOD n * k) MOD n = (j * k) MOD n``,
  REPEAT STRIP_TAC THEN
  `(j MOD n * k) MOD n = (((j MOD n) MOD n) * (k MOD n)) MOD n`
    by SRW_TAC [][MOD_TIMES2] THEN
  `_ = ((j MOD n) * (k MOD n)) MOD n` by SRW_TAC [][MOD_MOD] THEN
  SRW_TAC [][MOD_TIMES2]);

(* Theorem: If n > 0, ((n - x) MOD n + x) MOD n = 0  for x < n. *)
(* Proof:
     ((n - x) MOD n + x) MOD n
   = ((n - x) MOD n + x MOD n) MOD n    by LESS_MOD
   = (n - x + x) MOD n                  by MOD_PLUS
   = n MOD n                            by SUB_ADD and 0 <= n
   = (1*n) MOD n                        by MULT_LEFT_1
   = 0                                  by MOD_EQ_0
*)
val MOD_ADD_INV = store_thm(
  "MOD_ADD_INV",
  ``!n x. 0 < n /\ x < n ==> (((n - x) MOD n + x) MOD n = 0)``,
  METIS_TAC [LESS_MOD, MOD_PLUS, SUB_ADD, LESS_IMP_LESS_OR_EQ, MOD_EQ_0, MULT_LEFT_1]);

(* Theorem: Addition is associative in MOD: if x, y, z all < n,
            ((x + y) MOD n + z) MOD n = (x + (y + z) MOD n) MOD n. *)
(* Proof:
     ((x + y) MOD n + z) MOD n
   = ((x + y) MOD n + z MOD n) MOD n     by LESS_MOD
   = (x + y + z) MOD n                   by MOD_PLUS
   = (x + (y + z)) MOD n                 by ADD_ASSOC
   = (x MOD n + (y + z) MOD n) MOD n     by MOD_PLUS
   = (x + (y + z) MOD n) MOD n           by LESS_MOD
*)
val MOD_ADD_ASSOC = store_thm(
  "MOD_ADD_ASSOC",
  ``!n x y z. 0 < n /\ x < n /\ y < n /\ z < n ==>
   (((x + y) MOD n + z) MOD n = (x + (y + z) MOD n) MOD n)``,
  METIS_TAC [LESS_MOD, MOD_PLUS, ADD_ASSOC]);

(* Theorem: [Euclid's Lemma] A prime divides a product iff the prime divides a factor.
            [in MOD notation] For prime p, x*y MOD p = 0 <=> x MOD p = 0 or y MOD p = 0 *)
(* Proof:
   The if part is already in P_EUCLIDES:
   !p a b. prime p /\ divides p (a * b) ==> divides p a \/ divides p b
   Convert the divides to MOD by MOD_0_DIVIDES.
   The only-if part is:
   (1) divides p x ==> divides p (x * y)
   (2) divides p y ==> divides p (x * y)
   Both are true by DIVIDES_MULT: !a b c. divides a b ==> divides a (b * c).
   The symmetry of x and y can be taken care of by MULT_COMM.
*)
val EUCLID_LEMMA = store_thm(
  "EUCLID_LEMMA",
  ``!p x y. prime p ==> (((x * y) MOD p = 0) <=> (x MOD p = 0) \/ (y MOD p = 0))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][PRIME_POS] THEN
  SRW_TAC [][MOD_0_DIVIDES, EQ_IMP_THM] THEN
  METIS_TAC [P_EUCLIDES, DIVIDES_MULT, MULT_COMM]);

(* Theorem: [Cancellation Law for MOD p]
   For prime p, if x MOD p <> 0,
      (x*y) MOD p = (x*z) MOD p ==> y MOD p = z MOD p *)
(* Proof:
       (x*y) MOD p = (x*z) MOD p
   ==> ((x*y) - (x*z)) MOD p = 0   by MOD_EQ_DIFF
   ==>       (x*(y-z)) MOD p = 0   by arithmetic LEFT_SUB_DISTRIB
   ==>           (y-z) MOD p = 0   by EUCLID_LEMMA, x MOD p <> 0
   ==>               y MOD p = z MOD p    if z <= y

   Since this theorem is symmetric in y, z,
   First prove the theorem assuming z <= y,
   then use the same proof for y <= z.
*)
val MOD_MULT_LCANCEL1 = prove(
  ``!p x y z. (prime p) /\ (z <= y) ==>
     (((x * y) MOD p = (x * z) MOD p) /\ x MOD p <> 0 ==> (y MOD p = z MOD p))``,
  REPEAT STRIP_TAC THEN
  `0 < p` by SRW_TAC [][PRIME_POS] THEN
  `((x*y) - (x*z)) MOD p = 0` by SRW_TAC [][MOD_EQ_DIFF] THEN
  `(x*(y - z)) MOD p = 0` by SRW_TAC [][LEFT_SUB_DISTRIB] THEN
  METIS_TAC [EUCLID_LEMMA, MOD_EQ]);
val MOD_MULT_LCANCEL = store_thm(
  "MOD_MULT_LCANCEL",
  ``!p x y z. prime p ==>
      (((x * y) MOD p = (x * z) MOD p) /\ x MOD p <> 0 ==> (y MOD p = z MOD p))``,
  REPEAT STRIP_TAC THEN
  Cases_on `z <= y` THENL [ALL_TAC, `y <= z` by RW_TAC arith_ss []] THEN
  METIS_TAC [MOD_MULT_LCANCEL1]);

(* Theorem: For prime p, 0 < x < p ==> ?y. 0 < y /\ y < p /\ y*x MOD p = 1 *)
(* Proof:
       0 < x < p
   ==> ~ divides p x                    by NOT_LT_DIVIDES
   ==> gcd p x = 1                      by gcdTheory.PRIME_GCD
   ==> ?k q. k * x = q * p + 1          by gcdTheory.LINEAR_GCD
   ==> k*x MOD p = (q*p + 1) MOD p      by arithmetic
   ==> k*x MOD p = 1                    by MOD_MULT, 1 < p.
   ==> (k MOD p)*(x MOD p) MOD p = 1    by MOD_TIMES2
   ==> ((k MOD p) * x) MOD p = 1        by LESS_MOD, x < p.
   Now   k MOD p < p                    by MOD_LESS
   and   0 < k MOD p since (k*x) MOD p <> 0  (by 1 <> 0)
                       and x MOD p <> 0      (by ~ divides p x)
                                        by EUCLID_LEMMA
   Hence take y = k MOD p, then 0 < y < p.
*)
val MOD_MULT_INV_EXISTS = store_thm(
  "MOD_MULT_INV_EXISTS",
  ``!p x. prime p /\ 0 < x /\ x < p ==> ?y. 0 < y /\ y < p /\ ((y * x) MOD p = 1)``,
  REPEAT STRIP_TAC THEN
  `0 < p /\ 1 < p` by METIS_TAC [PRIME_POS, ONE_LT_PRIME] THEN
  `gcd p x = 1` by METIS_TAC [gcdTheory.PRIME_GCD, NOT_LT_DIVIDES] THEN
  `?k q. k * x = q * p + 1` by METIS_TAC [gcdTheory.LINEAR_GCD, NOT_ZERO_LT_ZERO] THEN
  `1 = (k * x) MOD p` by METIS_TAC [MOD_MULT] THEN
  `_ = ((k MOD p) * (x MOD p)) MOD p` by METIS_TAC [MOD_TIMES2] THEN
  `0 < k MOD p` by SRW_TAC [][] THENL [
    `1 <> 0` by DECIDE_TAC THEN
    `x MOD p <> 0` by METIS_TAC [DIVIDES_MOD_0, NOT_LT_DIVIDES] THEN
    `k MOD p <> 0` by METIS_TAC [EUCLID_LEMMA, MOD_MOD] THEN
    DECIDE_TAC,
    METIS_TAC [MOD_LESS, LESS_MOD]
  ]);

(* Convert this theorem into MUL_INV_DEF *)

(* Step 1: move ?y forward by collecting quantifiers *)
val lemma = prove(
  ``!p x. ?y. prime p /\ 0 < x /\ x < p ==> 0 < y /\ y < p /\ ((y * x) MOD p = 1)``,
  METIS_TAC [MOD_MULT_INV_EXISTS]);

(* Step 2: apply SKOLEM_THM *)
(*
- SKOLEM_THM;
> val it = |- !P. (!x. ?y. P x y) <=> ?f. !x. P x (f x) : thm
*)
val MOD_MULT_INV_DEF = new_specification(
  "MOD_MULT_INV_DEF",
  ["MOD_MULT_INV"], (* avoid MOD_MULT_INV_EXISTS: thm *)
  SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);
(*
> val MOD_MULT_INV_DEF =
    |- !p x.
         prime p /\ 0 < x /\ x < p ==>
         0 < MOD_MULT_INV p x /\ MOD_MULT_INV p x < p /\
         ((MOD_MULT_INV p x * x) MOD p = 1) : thm
*)

(* Theorem: mutliplication is associative in MOD:
            (x*y MOD n * z) MOD n = (x * y*Z MOD n) MOD n  *)
(* Proof:
     ((x * y) MOD n * z) MOD n
   = ((x * y) MOD n * z MOD n) MOD n    by z < n, LESS_MOD
   = (((x * y) * z) MOD n) MOD n        by MOD_TIMES2
   = ((x * (y * z)) MOD n) MOD n        by MULT_ASSOC
   = (x MOD n * (y * z) MOD n) MOD n    by MOD_TIMES2
   = (x * (y * z) MOD n) MOD n          by x < n, LESS_MOD
*)
val MOD_MULT_ASSOC = store_thm(
  "MOD_MULT_ASSOC",
  ``!n x y z. 0 < n /\ x < n /\ z < n ==> (((x * y) MOD n * z) MOD n = (x * (y * z) MOD n) MOD n)``,
  METIS_TAC [LESS_MOD, MOD_TIMES2, MULT_ASSOC]);

(* Theorem: ((a MOD m) ** n) MOD m = (a ** n) MOD m  *)
(* Proof: by induction on n. *)
val MOD_EXP = store_thm(
  "MOD_EXP",
  ``!a n m. 0 < m ==> (((a MOD m) ** n) MOD m = (a ** n) MOD m)``,
  REPEAT STRIP_TAC THEN
  Induct_on `n` THEN1
  RW_TAC std_ss [EXP] THEN
  `(a MOD m) ** SUC n MOD m = ((a MOD m) * (a MOD m) ** n) MOD m` by METIS_TAC [EXP] THEN
  `_ = ((a MOD m) * (((a MOD m) ** n) MOD m)) MOD m` by METIS_TAC [MOD_TIMES2, MOD_MOD] THEN
  `_ = ((a MOD m) * (a ** n MOD m)) MOD m` by METIS_TAC [] THEN
  `_ = (a * a ** n) MOD m` by METIS_TAC [MOD_TIMES2] THEN
  METIS_TAC [EXP]);

(* Theorem: For prime p, FACT (p-1) MOD p <> 0 *)
(* Proof:
   Change to induction on n of:
   For prime p, FACT (n-1) MOD p <> 0 for 0 < n <= p.
   When n = 1, FACT (1-1) MOD p = FACT 0 MOD p = 1 MOD p = 1 <> 0.
   In general,
     FACT n MOD p
   = (n * FACT (n-1)) MOD p
   = (n MOD p * FAC (n-1) MOD p) MOD p
   But           n MOD p <> 0  since 0 < n <= p
   also  FAC (n-1) MOD p <> 0 by inductive hypothesis
   Hence (n MOD p * FAC (n-1) MOD p) MOD p <> 0 by EUCLID_LEMMA.
*)
val MOD_PRIME_FACT = store_thm(
  "MOD_PRIME_FACT",
  ``!p n. prime p /\ 0 < n /\ n <= p ==> FACT (n-1) MOD p <> 0``,
  Induct_on `n` THEN SRW_TAC [][] THEN
  Cases_on `n = 0` THEN1 RW_TAC arith_ss [ONE_LT_PRIME, FACT] THEN
  `0 < n /\ n <= p` by RW_TAC arith_ss [] THEN
  `(FACT n) MOD p = (FACT (SUC (n-1))) MOD p` by RW_TAC arith_ss [ADD1] THEN
  `_ = (SUC (n-1) * FACT (n-1)) MOD p` by RW_TAC arith_ss [FACT] THEN
  `_ = (n * FACT (n-1)) MOD p` by RW_TAC arith_ss [ADD1] THEN
  `n MOD p <> 0` by RW_TAC arith_ss [] THEN
  `FACT (n - 1) MOD p <> 0` by SRW_TAC [][] THEN
  METIS_TAC [EUCLID_LEMMA]);

(* ------------------------------------------------------------------------- *)
(* Consequences of Relatively Prime, or Coprime.                             *)
(* ------------------------------------------------------------------------- *)

val _ = overload_on ("coprime", ``\x y. gcd x y = 1``);

(* Theorem: If 1 < n, !x. gcd n x = 1  ==> 0 < x /\ 0 < x MOD n *)
(* Proof:
   If x = 0, gcd n x = n. But n <> 1, hence x <> 0, or 0 < x.
   x MOD n = 0 ==> x a multiple of n ==> gcd n x = n <> 1  if n <> 1.
   Hence if 1 < n, gcd x n = 1 ==> x MOD n <> 0, or 0 < x MOD n.
*)
val MOD_NOZERO_WHEN_GCD_ONE = store_thm(
  "MOD_NOZERO_WHEN_GCD_ONE",
  ``!n. 1 < n ==> !x. (gcd n x = 1) ==> 0 < x /\ 0 < x MOD n``,
  NTAC 4 STRIP_TAC THEN CONJ_ASM1_TAC THENL [
    `1 <> n` by RW_TAC arith_ss [] THEN
    `x <> 0` by METIS_TAC [GCD_0R] THEN
    RW_TAC arith_ss [],
    `1 <> n /\ x <> 0` by RW_TAC arith_ss [] THEN
    `?k q. k * x = q * n + 1` by METIS_TAC [LINEAR_GCD] THEN
    `(k*x) MOD n = (q*n + 1) MOD n` by SRW_TAC [][] THEN
    `_ = 1` by METIS_TAC [MOD_MULT] THEN
    SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
    `x MOD n = 0` by RW_TAC arith_ss [] THEN
    `0 < n` by RW_TAC arith_ss [] THEN
    `(x*k) MOD n = 0` by METIS_TAC [MOD_MULITPLE_ZERO] THEN
    `1 <> 0` by RW_TAC arith_ss [] THEN
    METIS_TAC [MULT_COMM]
  ]);


(* Theorem: For n > 1, (gcd n x = 1) /\ (gcd n y = 1) ==> (gcd n (x*y) = 1) *)
(* Proof:
   gcd n x = 1 ==> no common factor between x and n
   gcd n y = 1 ==> no common factor between y and n
   Hence there is no common factor between (x*y) and n
   or gcd n (x*y) = 1

   gcd n (x * y) = gcd n y    by GCD_CANCEL_MULT, since gcd n x = 1.
                 = 1          by given
*)
val PRODUCT_WITH_GCD_ONE = store_thm(
  "PRODUCT_WITH_GCD_ONE",
  ``!n x y. 1 < n /\ (gcd n x = 1) /\ (gcd n y = 1) ==> (gcd n (x*y) = 1)``,
  METIS_TAC [GCD_CANCEL_MULT]);


(* Theorem: For n > 1, (gcd n x = 1) ==> (gcd n (x MOD n) = 1) *)
(* Proof:
   Since n <> 0,
   1 = gcd n x                         by given
     = gcd (x MOD n) n                 by GCD_EFFICIENTLY
     = gcd n (x MOD n)                 by GCD_SYM
*)
val MOD_WITH_GCD_ONE = store_thm(
  "MOD_WITH_GCD_ONE",
  ``!n x. 1 < n /\ (gcd n x = 1) ==> (gcd n (x MOD n) = 1)``,
  REPEAT STRIP_TAC THEN
  `0 <> n` by RW_TAC arith_ss [] THEN
  METIS_TAC [GCD_EFFICIENTLY, GCD_SYM]);


(* Theorem: If 0 < a, 0 < b, g = gcd a b, then 0 < g and a MOD g = 0 and b MOD g = 0 *)
(* Proof:
   0 < a ==> a <> 0, 0 < b ==> b <> 0,              by NOT_ZERO_LT_ZERO
   hence  g = gcd a b <> 0, or 0 < g.               by GCD_EQ_0
   g = gcd a b ==> (g divides a) /\ (g divides b)   by GCD_IS_GCD, is_gcd_def
               ==> (a MOD g = 0) /\ (b MOD g = 0)   by DIVIDES_MOD_0
*)
val GCD_DIVIDES = store_thm(
  "GCD_DIVIDES",
  ``!a b g. 0 < a /\ 0 < b /\ (g = gcd a b) ==> 0 < g /\ (a MOD g = 0) /\ (b MOD g = 0)``,
  REPEAT STRIP_TAC THEN1
  METIS_TAC [GCD_EQ_0, NOT_ZERO_LT_ZERO] THEN1
 (`0 < g` by METIS_TAC [GCD_EQ_0, NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [GCD_IS_GCD, is_gcd_def, DIVIDES_MOD_0]) THEN
  `0 < g` by METIS_TAC [GCD_EQ_0, NOT_ZERO_LT_ZERO] THEN
  METIS_TAC [GCD_IS_GCD, is_gcd_def, DIVIDES_MOD_0]);


(* Theorem: If 1 < n, gcd n x = 1 ==> ?k q. (k*x) MOD n = 1 /\ gcd n k = 1 *)
(* Proof:
       gcd n x = 1 ==> x <> 0           by GCD_0R
   Also,
       gcd n x = 1
   ==> ?k q. k * x = q * n + 1          by LINEAR_GCD
   ==> (k*x) MOD n = (q*n + 1) MOD n    by arithmetic
   ==> (k*x) MOD n = 1                  by MOD_MULT, 1 < n.

   Let g = gcd n k.
   Since 1 < n, 0 < n.
   Since q*n+1 <> 0, x <> 0, k <> 0, hence 0 < k.
   Hence 0 < g /\ (n MOD g = 0) /\ (k MOD g = 0)    by GCD_DIVIDES.
   Or  n = a*g /\ k = b*g    for some a, b.
   Therefore:
        (b*g)*x = q*(a*g) + 1
        (b*x)*g = (q*a)*g + 1      by arithmetic
   Hence g divides 1, or g = 1     since 0 < g.
*)
val GCD_ONE_PROPERTY = store_thm(
  "GCD_ONE_PROPERTY",
  ``!n x. 1 < n /\ (gcd n x = 1) ==> ?k. ((k*x) MOD n = 1) /\ (gcd n k = 1)``,
  REPEAT STRIP_TAC THEN
  `n <> 1` by RW_TAC arith_ss [] THEN
  `x <> 0` by METIS_TAC [GCD_0R] THEN
  `?k q. k * x = q * n + 1` by METIS_TAC [LINEAR_GCD] THEN
  `(k*x) MOD n = (q*n + 1) MOD n` by SRW_TAC [][] THEN
  `_ = 1` by METIS_TAC [MOD_MULT] THEN
  `?g. g = gcd n k` by SRW_TAC [][] THEN
  `n <> 0 /\ q*n + 1 <> 0` by RW_TAC arith_ss [] THEN
  `k <> 0` by METIS_TAC [MULT_EQ_0] THEN
  `0 < g /\ (n MOD g = 0) /\ (k MOD g = 0)` by METIS_TAC [GCD_DIVIDES, NOT_ZERO_LT_ZERO] THEN
  `divides g n /\ divides g k` by METIS_TAC [DIVIDES_MOD_0] THEN
  `divides g (n*q) /\ divides g (k*x)` by METIS_TAC [DIVIDES_MULT] THEN
  `divides g (n*q+1)` by METIS_TAC [MULT_COMM] THEN
  `divides g 1` by METIS_TAC [DIVIDES_ADD_2] THEN
  METIS_TAC [DIVIDES_ONE]);

(* ------------------------------------------------------------------------- *)
(* Factor of prime and prime exponents.                                      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (the missing EQ_MULT_RCANCEL)
   !m n p. (n * m = p * m) <=> (m = 0) \/ (n = p)  *)
(* Proof:
   By EQ_MULT_LCANCEL and MULT_SYM.
*)
val EQ_MULT_RCANCEL = store_thm(
  "EQ_MULT_RCANCEL",
  ``!m n p. (n * m = p * m) <=> (m = 0) \/ (n = p)``,
  METIS_TAC [EQ_MULT_LCANCEL, MULT_SYM]);

(* Theorem: [Factors of prime]
            For prime p, p = x*y ==> (x = 1 and y = p) or (x = p and y = 1) *)
(* Proof:
   Case analysis by divides_def and prime_def.
*)
val FACTORS_OF_PRIME = store_thm(
  "FACTORS_OF_PRIME",
  ``!p x y. prime p /\ (p = x*y) ==> ((x = 1) /\ (y = p)) \/ ((x = p) /\ (y = 1))``,
  REPEAT STRIP_TAC THEN
  `divides x p \/ divides y p` by METIS_TAC [divides_def] THENL [
    `(x = 1) \/ (x = p)` by METIS_TAC [prime_def] THEN1
    SRW_TAC [][] THEN
    `0 <> p` by METIS_TAC [PRIME_POS, NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [EQ_MULT_LCANCEL, MULT_RIGHT_1],
    `(y = 1) \/ (y = p)` by METIS_TAC [prime_def] THEN1
    SRW_TAC [][] THEN
    `0 <> p` by METIS_TAC [PRIME_POS, NOT_ZERO_LT_ZERO] THEN
    METIS_TAC [EQ_MULT_RCANCEL, MULT_LEFT_1]
  ]);

(* Theorem: [Factors of a square of prime]
            For prime p, x divides p*p ==> (x = 1) or (x = p) or (x = p*p) *)
(* Proof:
   Case analysis by divides_def and prime_def and Euclid's Lemma.
*)
val FACTORS_OF_PRIME_SQ = store_thm(
  "FACTORS_OF_PRIME_SQ",
  ``!p x. prime p /\ divides x (p*p) ==> (x = 1) \/ (x = p) \/ (x = p*p)``,
  REPEAT STRIP_TAC THEN
  `?a. p*p = a*x` by METIS_TAC [divides_def] THEN
  `divides p (a*x)` by METIS_TAC [divides_def] THEN
  `divides p a \/ divides p x` by METIS_TAC [P_EUCLIDES] THENL [
    `?b. a = b*p` by METIS_TAC [divides_def] THEN
    `p*p = p*(b*x)` by RW_TAC arith_ss [] THEN
    `0 <> p` by METIS_TAC [PRIME_POS, NOT_ZERO_LT_ZERO] THEN
    `p = b*x` by METIS_TAC [EQ_MULT_LCANCEL] THEN
    `((b = 1) /\ (x = p)) \/ ((b = p) /\ (x = 1))` by METIS_TAC [FACTORS_OF_PRIME] THEN1
    METIS_TAC [] THEN
    METIS_TAC [],
    `?b. x = b*p` by METIS_TAC [divides_def] THEN
    `p*p = (a*b)*p` by RW_TAC arith_ss [] THEN
    `0 <> p` by METIS_TAC [PRIME_POS, NOT_ZERO_LT_ZERO] THEN
    `p = a*b` by METIS_TAC [EQ_MULT_RCANCEL] THEN
    `((a = 1) /\ (b = p)) \/ ((a = p) /\ (b = 1))` by METIS_TAC [FACTORS_OF_PRIME] THEN1
    METIS_TAC [] THEN
    RW_TAC arith_ss []
  ]);

(* Theorem: [Factors of prime exponents]
            For prime p, n > 0, x divides p**n ==> ?k.  k <= n /\ x = p**k *)
(* Proof:
   By induction on n.
   Base case: x divides p**0 = 1 ==> x = 1, so x = p**0, choose k = 0.
   Step case: assume x divides p**n ==> ?k. k <= n /\ x = p**k.
   Now  x divides p**(SUC n) = p * (p**n)
   By definition of divides, ?a. p*(p**n) = a*x  (#1)
   Case x = 1, choose k = 0 so that x = p**0 = 1.
   Csae x <> 1, then a prime q divides x   by PRIME_FACTOR.
   So prime q divides x, and ?b. x = b*q.
   That is, prime q  divides p*(p**n)      by DIVIDES_TRANS.
   So  q divides p,  or q divides p**n
   Case q divides p, both p q are primes ==> q = p  by prime_divides_only_self
        x = b*q = b*p
        combine with #1,  p*(p**n) = x*a = p*(b*a)
        ==>            p**n = b*a             by EQ_MULT_LCANCEL
        Hence  b divides p**n, i.e. ?k. k <= n /\ b = p**k  by induction assumption
        Therefore x = b*p = p**k * p = p**(SUC k), and SUC k < SUC n.
   Case q divides p**n,
        ?k. k <= n /\ q = p**k   by induction assumption
        x = b*q = b*(p**k)
        combine with #1, p*(p**n) = x*a = (b*a)*(p**k)  with k <= n
        If k = 0, q = 1, but prime q <> 1, a contradiction.
        If k > 0, k = SUC h, p*(p**n) = p*(p**h * b*a)
        ==>     p**n = (p**h * b*a)           by EQ_MULT_LCANCEL
        Hence b divides p**n, and the same argument applies.
*)
val FACTORS_OF_PRIME_EXP = store_thm(
  "FACTORS_OF_PRIME_EXP",
  ``!p x n. prime p /\ divides x (p**n) ==> ?k. k <= n /\ (x = p**k)``,
  Induct_on `n` THEN1
  SRW_TAC [][EXP] THEN
  SRW_TAC [][EXP] THEN
  `0 <> p` by METIS_TAC [PRIME_POS, NOT_ZERO_LT_ZERO] THEN
  `?a. p*(p**n) = a*x` by METIS_TAC [divides_def] THEN
  Cases_on `x = 1` THENL [
    Q.EXISTS_TAC `0` THEN
    RW_TAC arith_ss [],
    `?q. prime q /\ divides q x` by SRW_TAC [][PRIME_FACTOR] THEN
    `?b. x = b*q` by METIS_TAC [divides_def] THEN
    `divides q (p*p**n)` by METIS_TAC [DIVIDES_TRANS] THEN
    `divides q p \/ divides q (p**n)` by METIS_TAC [P_EUCLIDES] THENL [
      `q = p` by SRW_TAC [][prime_divides_only_self] THEN
      `p*(p**n) = p*(a*b)` by RW_TAC arith_ss [] THEN
      `p**n = a*b` by METIS_TAC [EQ_MULT_LCANCEL] THEN
      `divides b (p**n)` by METIS_TAC [divides_def] THEN
      `?k. k <= n /\ (b = p**k)` by SRW_TAC [][] THEN
      `x = p * (p** k)` by RW_TAC arith_ss [] THEN
      Q.EXISTS_TAC `SUC k` THEN
      METIS_TAC [EXP, LESS_EQ_MONO],
      `?k. k <= n /\ (q = p**k)` by SRW_TAC [][] THEN
      Cases_on `k` THEN1
      METIS_TAC [EXP, NOT_PRIME_1] THEN
      `q = p*(p ** n')` by SRW_TAC [][EXP] THEN
      `p*(p**n) = p*((p**n')*a*b)` by RW_TAC arith_ss [] THEN
      `p**n = (p**n')*a*b` by METIS_TAC [EQ_MULT_LCANCEL] THEN
      `divides b (p**n)` by METIS_TAC [divides_def] THEN
      `?k. k <= n /\ (b = p**k)` by SRW_TAC [][] THEN
      `x = p**(SUC (k+n'))` by SRW_TAC [][EXP_ADD, ADD_SUC] THEN
      `p*(p**n) <> 0` by METIS_TAC [EXP_ZERO, EXP] THEN
      `x <= p*(p**n)` by METIS_TAC [DIVIDES_LE, NOT_ZERO_LT_ZERO] THEN
      `1 < p` by SRW_TAC [][ONE_LT_PRIME] THEN
      `SUC (k+n') <= SUC n` by METIS_TAC [EXP_BASE_LE_MONO, EXP] THEN
      Q.EXISTS_TAC `SUC (k + n')` THEN
      SRW_TAC [][]
    ]
  ]);

(* Theorem: For prime p, x divides p**n ==> x = 1 \/ p divides x *)
(* Proof:
   ?k. k <= n /\ (x = p**k)    by FACTORS_OF_PRIME_EXP
   Case k = 0, x = p**0 = 1.
   Case k > 0, p divides p**k = x  by DIVIDES_BASE_BASE_EXP
*)
val PRIME_EXP_FACTOR = store_thm(
  "PRIME_EXP_FACTOR",
  ``!p n x. prime p /\ divides x (p**n) ==> (x = 1) \/ divides p x``,
  REPEAT STRIP_TAC THEN
  `?k. k <= n /\ (x = p**k)` by SRW_TAC [][FACTORS_OF_PRIME_EXP] THEN
  Cases_on `k=0` THEN1
  RW_TAC arith_ss [] THEN
  METIS_TAC [DIVIDES_BASE_BASE_EXP, NOT_ZERO_LT_ZERO]);

(* Theorem: 0 < k /\ FUNPOW f k e = e  ==> !n. FUNPOW f (n*k) e = e *)
(* Proof:
   By induction on n:
   Base case: FUNPOW f (0 * k) e = e
     FUNPOW f (0 * k) e
   = FUNPOW f 0 e          by arithmetic
   = e                     by FUNPOW_0
   Step case: FUNPOW f (n * k) e = e ==> FUNPOW f (SUC n * k) e = e
     FUNPOW f (SUC n * k) e
   = FUNPOW f (k + n * k) e         by arithmetic
   = FUNPOW f k (FUNPOW (n * k) e)  by FUNPOW_ADD.
   = FUNPOW f k e                   by induction hypothesis
   = e                              by given
*)
val FUNPOW_MULTIPLE = store_thm(
  "FUNPOW_MULTIPLE",
  ``!f k e. 0 < k /\ (FUNPOW f k e = e)  ==> !n. FUNPOW f (n*k) e = e``,
  REPEAT STRIP_TAC THEN
  Induct_on `n` THEN1
  SRW_TAC [][FUNPOW_0] THEN
  METIS_TAC [MULT_COMM, MULT_SUC, FUNPOW_ADD]);

(* Theorem: 0 < k /\ FUNPOW f k e = e  ==> !n. FUNPOW f n e = FUNPOW f (n MOD k) e *)
(* Proof:
     FUNPOW f n e
   = FUNPOW f ((n DIV k) * k + (n MOD k)) e       by division algorithm
   = FUNPOW f ((n MOD k) + (n DIV k) * k) e       by arithmetic
   = FUNPOW f (n MOD k) (FUNPOW (n DIV k) * k e)  by FUNPOW_ADD
   = FUNPOW f (n MOD k) e                         by FUNPOW_MULTIPLE
*)
val FUNPOW_MOD = store_thm(
  "FUNPOW_MOD",
  ``!f k e. 0 < k /\ (FUNPOW f k e = e)  ==> !n. FUNPOW f n e = FUNPOW f (n MOD k) e``,
  REPEAT STRIP_TAC THEN
  `n = (n DIV k) * k + (n MOD k)` by RW_TAC arith_ss [DIVISION] THEN
  `_ = (n MOD k) + (n DIV k) * k` by METIS_TAC [ADD_COMM] THEN
  METIS_TAC [FUNPOW_ADD, FUNPOW_MULTIPLE]);

(* Theorem: n ** 2 = n * n *)
(* Proof:
   n ** 2 = n * (n ** 1) = n * (n * (n ** 0)) = n * (n * 1) = n * n
   or n ** 2 = n * (n ** 1) = n * n  by EXP_1:  !n. (1 ** n = 1) /\ (n ** 1 = n)
*)
val EXP_2 = store_thm(
  "EXP_2",
  ``!n. n ** 2 = n * n``,
  METIS_TAC [EXP, TWO, EXP_1]);


(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
