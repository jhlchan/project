(* Sublist Exercise - version 4 *)

(*
Define the type of binary trees

val _ = Hol_datatype`tree = Lf | Nd of 'a => tree => tree`

and define the functions

 elems  : 'a tree -> 'a list
 fringe : 'a tree -> 'a list

The elems function is defined

 elems Lf = []
 elems (Nd i t1 t2) = elems t1 ++ [i] ++ elems t2

The fringe function should return the elements of the tree that appear with leaves as their two descendents.
Something like

 fringe (Nd x Lf Lf) = [x]
 fringe ...

You should then be able to prove

 sublist (fringe t) (elems t)

*)

(* make assumptions explicit in proofs *)
show_assums := true;

(* Get sublist theory *)
load "sublistTheory";
open sublistTheory;
(*WAIT*)

(* The datatype: tree, with leaf Lf and node Nd *)
Hol_datatype `tree = Lf | Nd of 'a => tree => tree`;

(* The function elems puts all elements of the tree into a list *)
val elems_def = Define`
  (elems Lf = []) /\
  (elems (Nd n t1 t2) = elems t1 ++ [n] ++ elems t2)
`;
(*WAIT*)

(* Examples:

               1
              / \
             2   3
            /\   /\
           4 5  6 7
          /\ /\/\ /\
         * ** ****  *
*)
EVAL ``elems (Nd 5 Lf Lf)``; (* = [5] *)
(*WAIT*)
EVAL ``elems (Nd 2 (Nd 4 Lf Lf)
                   (Nd 5 Lf Lf))``; (* = [4; 2; 5] *)
(*WAIT*)
EVAL ``elems (Nd 1 (Nd 2 (Nd 4 Lf Lf)
                         (Nd 5 Lf Lf))
                   (Nd 3 (Nd 6 Lf Lf)
                         (Nd 7 Lf Lf)))``; (* = [4;2;5;1;6;3;7] *)
(*WAIT*)

(* The function fringe puts all fringe elements of the tree into a list *)
val fringe_def = Define`
  (fringe Lf = []) /\
  (fringe (Nd n Lf Lf) = [n]) /\
  (fringe (Nd n t1 t2) = (fringe t1) ++ (fringe t2))
`;
(*WAIT*)

(* Examples: same tree as above *)
EVAL ``fringe (Nd 5 Lf Lf)``; (* = [5] *)
(*WAIT*)

EVAL ``fringe (Nd 2 (Nd 4 Lf Lf)
                    (Nd 5 Lf Lf))``; (* = [4; 5] *)
(*WAIT*)

(* Theorem: sublist (fringe t) (elems t) *)
(* Proof:
   By induction on tree t, applying theorems of SUBLIST, including reflexivity and transitivity.
*)
val SUBLIST_FRINGE_ELEMS = store_thm(
  "SUBLIST_FRINGE_ELEMS",
  ``!t. fringe t <= elems t``,
  Induct THEN1 SRW_TAC [][fringe_def, sublist_def] THEN
  SRW_TAC [][elems_def] THEN MAP_EVERY Cases_on [`t`, `t'`] THEN SRW_TAC [][fringe_def] THENL [
    METIS_TAC [elems_def, listTheory.APPEND, SUBLIST_REFL],
    METIS_TAC [elems_def, SUBLIST_APPEND2, SUBLIST_TRANS],
    METIS_TAC [elems_def, SUBLIST_APPEND1, SUBLIST_TRANS],
    METIS_TAC [SUBLIST_APPENDR_I, SUBLIST_PAIR_APPEND, listTheory.APPEND_ASSOC]
  ]);
(*WAIT*)
