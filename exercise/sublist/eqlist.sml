(* Equal List exercise *)

(* Some theorems in listTheory are useful. *)
open listTheory;

(* make assumptions explicit in proofs *)
show_assums := true;

(* define equality of lists *)
val eqlist_def = Define`
    (eqlist [] [] = T) /\
    (eqlist [] (h::t) = F) /\
    (eqlist (h::t) [] = F) /\
    (eqlist (h::s) (k::t) = (h = k) /\ eqlist s t)
`;
(*PAUSE*)

(* set infix and overload for eqlist *)
val _ = set_fixity "==" (Infixl 480);
val _ = overload_on ("==", ``eqlist``);

(* Theorem: [Reflexive]  p == p *)
g `!p:'a list. p == p`;
(*WAIT*)
(* Proof:
   By induction on list p.
   Base case is to prove: [] == []
        This is true by eqlist definition.
   Step case is to prove: (t == t) ==> (h::t) == (h::t)
        This is true because:
        (h::t) == (h::t)
      = t == t               by eqlist definition
      = T                    by assumption
*)
e (Induct_on `p`);
e (ASM_REWRITE_TAC [eqlist_def]); (* solves base case *)
e (ASM_REWRITE_TAC [eqlist_def]); (* solves step case *)
(* OK *)
dropn 1;
val EQLIST_REFL = store_thm("EQLIST_REFL",
    ``!p:'a list. p == p``,
    Induct_on `p` THEN ASM_REWRITE_TAC [eqlist_def]);
(*PAUSE*)


(* Theorem: [Symmetric] (p == q) <=> (p = q) *)
(* Prove this in two parts. *)

(* Part 1 [easy]: (p = q) ==> (p == q)  *)
g `!p:'a list.!q. (p = q) ==> p == q`;
(*WAIT*)
(* Proof:
      p == q
   =  q == q     by assumption p = q
   =  T          by reflexivity
*)
e (REPEAT STRIP_TAC); (* remove for-alls *)
e (ASM_REWRITE_TAC []); (* reduce goal to: q == q *)
e (ASM_REWRITE_TAC [EQLIST_REFL]); (* apply reflexivity *)
(* OK *)
dropn 1;
val EQLIST_SYM1 = store_thm("EQLIST_SYM1",
    ``!p:'a list.!q. (p = q) ==> p == q``,
    REPEAT STRIP_TAC THEN ASM_REWRITE_TAC [EQLIST_REFL]);
(*PAUSE*)

(* Part 2 [hard]: (p == q) ==> (p = q)  *)
g `!p:'a list.!q. p == q ==> (p = q)`;
(*WAIT*)
(* Proof:
   Try induction on list p.
*)
e (Induct_on `p`);
(* Establish the base case as a lemma: *)

(* Lemma: !q. [] == q ==> ([] = q) *)
g `!q. [] == q ==> ([] = q)`;
(* This is a theorem on one list, easy to apply induction. *)
(* Proof:
   By induction on list q.
   Base case is to prove: [] == [] ==> [] = [].
        The conclusion [] = [] is obviously true.
   Step case is to prove: ([] == t ==> [] = t) ==> ([] == h::t ==> [] = h::t)
        Since [] == h::t  is false by eqlist definition,
        and [] = h::t is obviously false, the implication is F ==> F, which is true.
*)
e (Induct_on `q`);
e (ASM_REWRITE_TAC []);           (* solves base case *)
e (ASM_REWRITE_TAC [eqlist_def]); (* solves step case *)
(* OK *)
dropn 1;
val EQLIST_EMPTY1 = store_thm("EQLIST_EMPTY1",
    ``!q. [] == q ==> ([] = q)``,
    Induct_on `q` THEN ASM_REWRITE_TAC [eqlist_def]);

(* For convenience, the reflection of EQLIST_EMPTY1 can be proved similarly. *)
(* Lemma: !q. q == [] ==> (q = []) *)
val EQLIST_EMPTY2 = store_thm("EQLIST_EMPTY2",
    ``!q. q == [] ==> (q = [])``,
    Induct_on `q` THEN ASM_REWRITE_TAC [eqlist_def]);
(*PAUSE*)

(* back to current proof *)
p();
(* Apply EQLIST_EMPTY1, which is the base case, to solve it. *)
e (ASM_REWRITE_TAC [EQLIST_EMPTY1]); (* solves base case *)
(*WAIT*)

(* For the induction step, use cases on list q. *)
e (Cases_on `q`);
(* It turns out that the first case with [] is just EQLIST_EMPTY2. *)
e (ASM_REWRITE_TAC [EQLIST_EMPTY2]); (* solves the case [] *)
(* The remaining goal is to prove:
   !h'. h'::p == h::t ==> (h'::p = h::t)  *)
(* This can be split into two cases in order to apply eqlist definition:
   case 1: h = h'
   case 2: h �� h' *)
e (Cases_on `h = h'`);
(* The first case (h = h') simplfies to:
   h::p == h::t ==> (h::p = h::t)  *)
(* Proof:
     h::p == h::t
   = p == t          by eqlist definition
   = p = t           by inductive assumption
   = h::p = h::t     by EQ_LIST of listTheory
*)
EQ_LIST; (* theorem EQ_LIST of listTheory *)
(* [] |- !h1 h2. (h1 = h2) ==> !l1 l2. (l1 = l2) ==> (h1::l1 = h2::l2) : thm *)

e (ASM_REWRITE_TAC [eqlist_def, EQ_LIST]); (* not improving *)
e (PROVE_TAC [eqlist_def, EQ_LIST]); (* solves the case h = h' *)
(* The second case (h �� h') simplifies to:
   h'::p == h::t ==> (h'::p = h::t)  *)
(* Proof:
   h'::p == h::t  is F  when h �� h', by eqlist definition.
   h'::p = h::t   is F  when h �� h', by NOT_EQ_LIST of listTheory
   So this is F ==> F, which is true.
*)
NOT_EQ_LIST; (* theorem NOT_EQ_LIST of listTheory *)
(* [] |- !h1 h2. h1 �� h2 ==> !l1 l2. h1::l1 �� h2::l2 : thm *)

e (ASM_REWRITE_TAC [eqlist_def, NOT_EQ_LIST]); (* not improving *)
e (PROVE_TAC [eqlist_def, NOT_EQ_LIST]); (* solves the case h �� h' *)
(* This solves the induction step, hence completes the whole proof *)
(* OK *)
dropn 1;
val EQLIST_SYM2 = store_thm("EQLIST_SYM2",
    ``!p:'a list.!q. p == q ==> (p = q)``,
    Induct_on `p` THENL [
        ASM_REWRITE_TAC [EQLIST_EMPTY1],
        Cases_on `q` THENL [
            ASM_REWRITE_TAC [EQLIST_EMPTY2],
            Cases_on `h = h'` THENL [
                PROVE_TAC [eqlist_def, EQ_LIST],
                PROVE_TAC [eqlist_def, NOT_EQ_LIST]
            ]
        ]
    ]);
(*PAUSE*)

(* Theorem: [Symmetric] (p == q) <=> (p = q) *)
g `!p:'a list. !q. p == q <=> (p = q)`;
(*WAIT*)
(* Proof:
   By EQLIST_SYM1 and EQLIST_SYM2.
*)
e (REPEAT STRIP_TAC); (* remove for-alls *)
e (EQ_TAC); (* split equivalence into two *)
e (ASM_REWRITE_TAC [EQLIST_SYM1, EQLIST_SYM2]);
(* OK *)
dropn 1;
val EQLIST_SYM = store_thm("EQLIST_SYM",
    ``!p:'a list.!q. p == q <=> (p = q)``,
    REPEAT STRIP_TAC THEN
    EQ_TAC THEN
    ASM_REWRITE_TAC [EQLIST_SYM1, EQLIST_SYM2]);
(* PAUSE *)

(* Theorem: [Transitive] (p == q) /\ (q == r) ==> (p == r) *)
g `!p q r. (p == q) /\ (q == r) ==> (p == r)`;
(*WAIT*)
(* Proof:
   By symmetric property, (p == q) ==> p = q
                          (q == r) ==> q = r
   By transitivity of equality, p = r.
   Again by symmetric property, (p = r) ==> (p == r).
*)
e (REPEAT STRIP_TAC); (* remove for-alls *)
e (ASM_REWRITE_TAC [EQLIST_SYM]); (* not improving *)
e (PROVE_TAC [EQLIST_SYM]); (* solves the goal *)
(* OK *)
dropn 1;
val EQLIST_TRANS = store_thm("EQLIST_TRANS",
    ``!p q r. (p == q) /\ (q == r) ==> (p == r)``,
    PROVE_TAC [EQLIST_SYM]);
(*PAUSE*)

(* Now show all the main theorems *)
EQLIST_REFL;  (* eqlist Reflexive *)
EQLIST_SYM;   (* eqlist Symmetric *)
EQLIST_TRANS; (* eqlist Transitive *)
(*PAUSE*)

(*

Lessons:
* To keep things managable, use induction on one list only.
* Sometimes cases are better than induction.
* If a subgoal looks complicated, prove it separately as a lemma.

Questions:
* Sometimes ASM_REWRITE_TAC [thm] shows no improvement, but PROVE_TAC [thm] works.
  Is this because REWRITE rules are not using resolution to PROVE stuff?
* My eqlist definition is probably the same as the internal list "=" definiton.
  Why is it so hard to prove the symmetric property with identical definitions?
* How do I know I'm not "cheating" by using theorems in listTheory?

*)
