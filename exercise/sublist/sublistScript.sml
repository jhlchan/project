(* Sublist Theory *)

(*---------------------------------------------------------------------------

 A Theory for sublist: a partial-ordering of lists
 =================================================
 The notion of sublist is defined as:

    sublist l1 l2 <=> all the elements of l1 appear in l2,
                      in the order in which they appear in l1.

 We shall use the infix operator "<=" to denote the sublist relation.

 This infix notation is chosen as sublist shares many characteristics of the
 less-than-or-equal relationship between non-negative numbers, such as:

                 for Numbers             for Lists
 start element       0                       []
 constructor     n+1 = SUC n               l = h::t
 addition            a + b                 l1 ++ l2
 least element       0 <= n                [] <= l
 least element   ~(SUC n <= 0)           ~((h::t) <= [])
 least element   x <= 0 <=> x=0          x <= [] <=> x = []
 reflexive           x <= x                 x <= x
 anti-symmetric  x <= y /\ y <= x        x <= y /\ y <= x
                 ==> x = y               ==> x = y
 transitive      x <= y /\ y <= z        x <= y /\ y <= z
                 ==> x <= z              ==> x <= z
 prefix          n <= SUC n              p <= h::p
 prefix          n <= x + n              p <= x ++ p
 suffix          n <= n + 1              p <= p ++ [x]
 suffix          n <= n + x              p <= p ++ x
 ordering        p <= q = x+p <= x+q     p <= q = x++p <= x++q
 ordering        p <= q = p+x <= q+x     p <= q = p++x <= q++x
 ordering        a <= b /\ c <= d        a <= b /\ c <= d
                 ==> a+c <= b+d          ==> a++c <= b++d

 Key Lemma
 =========
 The sublist relation is a partial ordering because:
 (a) It is reflexive,
 (b) It is anti-symmetric, and
 (c) It is transitive.

 The reflexive property follows from definition, even the verbal one.

 The anti-symmetric property is proved via a key idea:

    p <= q ==> length(p) <= length(q).

 This forces lists p q with (p <= q) and (q <= p) to be of the same size,
 ultimately forcing their heads to be equal, with tails same size and sublist of each other,
 hence heads of tail are equal, and so on.

 The transitive property is evident from this picture of (p <= q) and (q <= r):

   p:     [1,1]
           | |
           | +-+
           |   |
   q:   [2,1,2,1]
         | | | |
         | | | +-----+
         | | |       |
   r: [3,2,1,2,2,3,2,1,3]

 Obviously, p <= r  because by "following the lines".

 This diagram describes how the sublist relation interacts with append:
 there can be extra elements without affecting the sublist relationship.

 The key lemma is this:        p <= q ==> p <= (h::q)   SUBLIST_CONSR_I
 or its equivalent form:  (h::p) <= q ==> p <= q        SUBLIST_CONSL_E

 Their equivalence follows from SUBLIST definition (see SUBLIST_CONS_EQ),
 so proving one will also prove the other.

 These key lemma can be generalized to:
 SUBLIST_APPENDR_I:         p <= q ==> p <= (x ++ q)
 SUBLIST_APPENDL_E:  (x ++ p) <= q ==> p <= q

 Or to be encapsulated in this statement:
 SUBLIST_EX_APPEND:   (h::t) <= q  <=> ?x y. (q = x ++ (h::y)) /\ (t <= y)

 The key lemma can be further generalized to:
 SUBLIST_PREFIX:       p <= q <=> (x ++ p) <= (x ++ q)
 SUBLIST_SUFFIX:       p <= q <=> (p ++ x) <= (q ++ x)

 These are useful in proving this result:
 SUBLIST_PAIR_APPEND:    (a <= b) /\ (c <= d) ==> (a ++ c) <= (b ++ d)
 Proof:
 Since by SUBLIST_SUFFIX, a <= b ==> a ++ c <= b ++ c   .... (1)
   and by SUBLIST_PREFIX, c <= d ==> b ++ c <= b ++ d   .... (2)
 Combining (1) and (2) by transitivity of sublist, the result follows.

 Sublist Theorems
 ================
 SUBLIST_NIL         !p. p <= [] <=> p = []
 SUBLIST_CONSL_E     !p q h. (h::p) <= q ==> p <= q
 SUBLIST_CONSR_I     !p q. p <= q ==> p <= (h::q)
 SUBLIST_APPENDL_E   !p q x. (x ++ p) <= q ==> p <= q
 SUBLIST_APPENDR_I   !p q x. p <= q ==> p <= (x ++ q)

 SUBLIST_NIL2        !p q. (p ++ q) <= q ==> p = []
 SUBLIST_APPEND1     !p q. p <= p ++ q
 SUBLIST_APPEND2     !p q. p <= q ++ p
 SUBLIST_APPEND3     !h p q. p <= q <=> (p ++ [h]) <= (q ++ [h])
 SUBLIST_PREFIX      !x p q. p <= q <=> (x ++ p) <= (x ++ q)
 SUBLIST_SUFFIX      !x p q. p <= q <=> (p ++ x) <= (q ++ x)

 SUBLIST_PAIR_APPEND !a b c d.  (a <= b) /\ (c <= d) ==> (a ++ c) <= (b ++ d)
 SUBLIST_EX_APPEND   !h t q. (h::t) <= q  <=> ?x y. (q = x ++ (h::y)) /\ (t <= y)

 SUBLIST_LENGTH      !p q. p <= q ==> LENGTH p <= LENGTH q

 SUBLIST_REFL        !p. p <= p
 SUBLIST_ANTISYM     !p q. (p <= q) /\ (q <= p) ==> (p = q)
 SUBLIST_TRANS       !p q r. p <= q /\ q <= r ==> p <= r

 Applications
 ============
 sublist2.sml   Apply sublist theory to prove another equivalent definition of sublist.
 sublist3.sml   Apply sublist theory to yet another definition based on inductive relation.
 sublist4.sml   Apply sublist theory to prove an interesting result involving binary trees.

 ---------------------------------------------------------------------------*)


(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "sublist";

(* use these from listTheory *)
val LENGTH = listTheory.LENGTH;
val APPEND = listTheory.APPEND;
val APPEND_ASSOC = listTheory.APPEND_ASSOC;
(*WAIT*)

(* Definition of sublist *)
val sublist_def = Define`
  (sublist [] x = T) /\
  (sublist (h1::t1) [] = F) /\
  (sublist (h1::t1) (h2::t2) = (h1 = h2) /\ sublist t1 t2 \/
                               ~(h1 = h2) /\ sublist (h1::t1) t2)
`;
val SUBLIST = sublist_def;
(*WAIT*)

(* set overloading *)
val _ = overload_on ("<=", ``sublist``);
(*WAIT*)

(*---------------------------------------------------------------------------
    Simple theorems about sublist.
 ---------------------------------------------------------------------------*)

(* Theorem: !p. p <= [] <=> p = [] *)
val SUBLIST_NIL = store_thm(
  "SUBLIST_NIL",
  ``!p:'a list. p <= [] <=> (p = [])``,
  Cases_on `p` THEN SRW_TAC [][SUBLIST]);
(*WAIT*)

(*---------------------------------------------------------------------------
    Relationship of sublist with other properties.
 ---------------------------------------------------------------------------*)

(* Theorem: !p q. p <= q ==> LENGTH p <= LENGTH q *)
(* Proof:
   By induction on list q.
   For the induction from q to h::q, the non-trivial cases are:
     When p = (h'::t),
       If h = h',
           (h::t) <= (h::q)
         = t <= q                          by SUBLIST definition, same head
       ==> LENGTH t <= LENGTH q            by inductive hypothesis
         = SUC(LENGTH t) <= SUC(LENGTH q)
         = LENGTH (h::t) <= LENGTH (h::q)
       If ~(h = h'),
           (h'::t) <= (h::q)
         = (h'::t) <= q                    by SUBLIST definition, different head
       ==> LENGTH (h'::t) <= LENGTH q      by inductive hypothesis
         = LENGTH (h'::t) <= SUC(LENGTH q) by smart arithemtic
         = LENGTH (h'::t) <= LENGTH (h::q)
*)
val SUBLIST_LENGTH = store_thm(
  "SUBLIST_LENGTH",
  ``!p q. p <= q ==> LENGTH p <= LENGTH q``,
  Induct_on `q` THEN SRW_TAC [][SUBLIST_NIL] THEN
  Cases_on `p` THEN FULL_SIMP_TAC (srw_ss()) [SUBLIST] THEN
  `LENGTH (h'::t) <= LENGTH q` by METIS_TAC [LENGTH] THEN
  FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []);
(*WAIT*)

(* Theorem:  !h (!p q. (h::p) <= q ==> p <= q) = (!p q. p <= q ==> p <= (h::q)) *)
val SUBLIST_CONS_EQ = store_thm(
  "SUBLIST_CONS_EQ",
  ``!h. (!p q. (h::p) <= q ==> p <= q) = (!p q. p <= q ==> p <= (h::q))``,
  SRW_TAC [][FUN_EQ_THM, EQ_IMP_THM] THEN METIS_TAC [SUBLIST]);
(*WAIT*)

(* Theorem: [Remove CONS from left]: !h p q. (h::p) <= q ==> p <= q *)
val SUBLIST_CONSL_E = store_thm(
  "SUBLIST_CONSL_E",
  ``!p q h. h::p <= q ==> p <= q``,
  Induct_on `q` THEN1 SRW_TAC [][SUBLIST_NIL] THEN
  Cases_on `p` THEN1 SRW_TAC [][SUBLIST] THEN
  Cases_on `h' = h''` THEN METIS_TAC [SUBLIST]);
(*WAIT*)

(* Theorem: [Include CONS to right] !p q. p <= q ==> p <= (h::q) *)
(* Proof:
   This follows from SUBLIST_CONS_EQ and SUBLIST_CONSL_E.
*)
val SUBLIST_CONSR_I = store_thm(
  "SUBLIST_CONSR_I",
  ``!p q. p <= q ==> !h. p <= h::q``,
  METIS_TAC [SUBLIST_CONS_EQ, SUBLIST_CONSL_E]);
(*WAIT*)

(*---------------------------------------------------------------------------
    Properties of sublist as partial ordering.
 ---------------------------------------------------------------------------*)

(* Theorem: [Reflexive] p <= p *)
(* Proof:
   By induction on list p and apply definition.
*)
val SUBLIST_REFL = store_thm(
  "SUBLIST_REFL",
  ``!p:'a list. p <= p``,
  Induct THEN SRW_TAC [][SUBLIST]);
(*WAIT*)

(* Theorem: [Anti-symmetric] !p q. (p <= q) /\ (q <= p) ==> (p = q) *)
(* Proof:
   By induction on list q.
   For the induction from q to h::q, the non-trivial cases are:
     When p = (h'::t),
       If h = h',
           ((h::t) <= (h::q)) /\ ((h::q) <= (h::t))
         = (t <= q) and (q <= t)       by SUBLIST definition, same head
       ==> t = q                       by inductive hypothesis
         = (h::t) = (h::q)             by list equality
       If ~(h = h'),
           ((h'::t) <= (h::q)) /\ ((h::q) <= (h'::t))
         = ((h'::t) <= q) /\ ((h::q) <= t)      by SUBLIST definition, different head
       ==> (LENGTH (h'::t) <= LENGTH q) /\ (LENGTH (h::q) <= LENGTH t)  by SUBLIST_LENGTH
       ==> (LENGTh t < LENGTH q) /\ (LENGTH q < LENGTH t)   by LENGTH definition
         = F                                    by arithmetic
         hence implication is T.
*)
val SUBLIST_ANTISYM = store_thm(
    "SUBLIST_ANTISYM",
    ``!p q:'a list. p <= q /\ q <= p ==> (p = q)``,
    Induct_on `q` THEN1 (SRW_TAC [][SUBLIST_NIL]) THEN
    MAP_EVERY Q.X_GEN_TAC [`h2`, `p`] THEN
    Cases_on `p` THEN
    SRW_TAC [][SUBLIST] THEN1 METIS_TAC [] THEN
    `LENGTH (h2::q) <= LENGTH t /\ LENGTH (h::t) <= LENGTH q` by METIS_TAC [SUBLIST_LENGTH] THEN
    FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []);
(*WAIT*)

(* Theorem [Transitive]: for all lists p, q, r; (p <= q) /\ (q <= r) ==> (p <= r) *)
(* Proof:
   By induction on list r and take note of cases.
*)
val SUBLIST_TRANS = store_thm("SUBLIST_TRANS",
    ``!p q r. p <= q /\ q <= r ==> p <= r``,
    Induct_on `r` THEN1 PROVE_TAC [SUBLIST_NIL] THEN
    SRW_TAC [][SUBLIST] THEN
    Cases_on `p` THEN1 PROVE_TAC [SUBLIST] THEN
    Cases_on `q` THEN1 PROVE_TAC [SUBLIST] THEN
    Cases_on `h = h''` THENL [
      `t' <= r` by PROVE_TAC [SUBLIST] THEN
      Cases_on `h = h'` THENL [
        `t <= t'` by PROVE_TAC [SUBLIST] THEN PROVE_TAC [SUBLIST],
        `h'::t <= t'` by PROVE_TAC [SUBLIST] THEN PROVE_TAC [SUBLIST]
      ],
      `h''::t' <= r` by PROVE_TAC [SUBLIST] THEN
      PROVE_TAC [SUBLIST_CONSR_I]
    ]);
(*WAIT*)

(*---------------------------------------------------------------------------
    More theorems about sublist, applying partial order properties.
 ---------------------------------------------------------------------------*)

(* Theorem: [Eliminate append from left]: (x ++ p) <= q ==> sublist p <= q *)
(* Proof:
   By induction on the extra list x.
   The induction step follows from SUBLIST_CONSL_E.
*)
val SUBLIST_APPENDL_E = store_thm(
  "SUBLIST_APPENDL_E",
  ``!p q x. x ++ p <= q ==> p <= q``,
  Induct_on `x` THEN METIS_TAC [SUBLIST_CONSL_E, APPEND]);
(*WAIT*)

(* Theorem: [Include append to right] p <= q ==> p <= (x ++ q) *)
(* Proof:
   By induction on list x.
   The induction step follows by SUBLIST_CONSR_I.
*)
val SUBLIST_APPENDR_I = store_thm(
  "SUBLIST_APPENDR_I",
  ``!p q x. p <= q ==> p <= x ++ q``,
  Induct_on `x` THEN SRW_TAC [][SUBLIST_CONSR_I]);
(*WAIT*)

(* Theorem: [append after] p <= (p ++ q) *)
(* Proof:
   By induction on list p, and note that p and (p ++ q) have the same head.
*)
val SUBLIST_APPEND1 = store_thm(
  "SUBLIST_APPEND1",
  ``!p q. p <= p ++ q``,
  Induct_on `p` THEN SRW_TAC [][SUBLIST]);
(*WAIT*)

(* Theorem: [append before] p <= (q ++ p) *)
val SUBLIST_APPEND2 = store_thm(
  "SUBLIST_APPEND2",
  ``!p q. p <= q ++ p``,
  Induct_on `q` THEN1 SRW_TAC [][SUBLIST_REFL] THEN
  METIS_TAC [APPEND, SUBLIST_CONSR_I, SUBLIST_TRANS]);
(*WAIT*)

(* Theorem: [prefix append] p <= q <=> (x ++ p) <= (x ++ q) *)
val SUBLIST_PREFIX = store_thm(
  "SUBLIST_PREFIX",
  ``!x p q. p <= q <=> (x ++ p) <= (x ++ q)``,
  Induct_on `x` THEN METIS_TAC [SUBLIST, APPEND]);
(*WAIT*)

(* Theorem: [no append to left] !p q. (p ++ q) <= q ==> p = [] *)
val SUBLIST_NIL2 = store_thm(
  "SUBLIST_NIL2",
  ``!p q. (p ++ q) <= q ==> (p = [])``,
  Induct_on `q` THEN Cases_on `p` THEN SRW_TAC [][SUBLIST] THEN
  Cases_on `h = h'` THEN SRW_TAC [][] THENL [
    `t ++ h::q = (t ++ [h])++ q` by SRW_TAC [][] THEN
    `~((t ++ [h]) = [])`  by SRW_TAC [][] THEN METIS_TAC [],
    `h::(t ++ h'::q) = ((h::t) ++ [h']) ++ q` by SRW_TAC [][] THEN
    `~((h::t ++ [h']) = [])` by SRW_TAC [][] THEN METIS_TAC [APPEND]
  ]);
(*WAIT*)

(* Theorem: [tail append - if] p <= q ==> (p ++ [h]) <= (q ++ [h]) *)
val SUBLIST_APPEND31 = store_thm(
  "SUBLIST_APPEND31",
  ``!p q. p <= q ==> !h. (p ++ [h]) <= (q ++ [h])``,
  Induct_on `q` THEN1 SRW_TAC [][SUBLIST_NIL, SUBLIST_REFL] THEN
  Cases_on `p` THEN SRW_TAC [][SUBLIST] THEN
  Cases_on `h = h'` THEN SRW_TAC [][] THEN
  METIS_TAC [SUBLIST_APPEND2, APPEND]);
(*WAIT*)

(* Theorem: [tail append - only if] p ++ [h] <= q ++ [h] ==> p <= q *)
val SUBLIST_APPEND32 = store_thm(
  "SUBLIST_APPEND32",
  ``!h p q. (p ++ [h]) <= (q ++ [h]) ==> p <= q``,
  Induct_on `q` THEN SRW_TAC [][SUBLIST] THEN1
    PROVE_TAC [SUBLIST_NIL2, SUBLIST] THEN
    Cases_on `p` THEN SRW_TAC [][SUBLIST] THEN
    Cases_on `h = h''` THEN SRW_TAC [][] THEN
    METIS_TAC [SUBLIST, APPEND]);
(*WAIT*)

(* Theorem: [tail append] p <= q <=> p ++ [h] <= q ++ [h] *)
val SUBLIST_APPEND3 = store_thm(
  "SUBLIST_APPEND3",
  ``!h p q. p <= q <=> (p ++ [h]) <= (q ++ [h])``,
  METIS_TAC [SUBLIST_APPEND31, SUBLIST_APPEND32]);
(*WAIT*)

(* Theorem: [suffix append] sublist p q ==> sublist (p ++ x) (q ++ x) *)
val SUBLIST_SUFFIX = store_thm(
  "SUBLIST_SUFFIX",
  ``!x p q. p <= q <=> (p ++ x) <= (q ++ x)``,
  Induct_on `x` THEN1 SRW_TAC [][] THEN
  REPEAT STRIP_TAC THEN
  `!z. z ++ h::x = (z ++ [h])++x` by SRW_TAC[][] THEN
  `(p ++ h::x <= q ++ h::x) = ((p ++ [h])++ x <= (q ++ [h])++ x)` by PROVE_TAC [] THEN
  `(p <= q) = (p ++ [h]) <= (q ++ [h])` by PROVE_TAC [SUBLIST_APPEND3] THEN
  PROVE_TAC []);
(*WAIT*)

(* Theorem : (a <= b) /\ (c <= d) ==> (a ++ c) <= (b ++ d) *)
val SUBLIST_PAIR_APPEND = store_thm(
  "SUBLIST_PAIR_APPEND",
  ``!a b c d.  (a <= b) /\ (c <= d) ==> (a ++ c) <= (b ++ d)``,
  REPEAT STRIP_TAC THEN
  `a ++ c <= a ++ d` by PROVE_TAC [SUBLIST_PREFIX] THEN
  `a ++ d <= b ++ d` by PROVE_TAC [SUBLIST_SUFFIX] THEN
  PROVE_TAC [SUBLIST_TRANS]);
(*WAIT*)

(* Theorem: [Extended Append, or Decomposition] (h::t) <= q <=> ?x y. (q = x ++ (h::y)) /\ (t <= y) *)
(* Proof:
   By induction on list q.
   Base case is to prove:
     sublist (h::t) []  <=> ?x y. ([] = x ++ (h::y)) /\ (sublist t y)
     Hypothesis sublist (h::t) [] is F by SUBLIST_NIL.
     In the conclusion, [] cannot be decomposed, hence implication is valid.
   Step case is to prove:
     (sublist (h::t) q  <=> ?x y. (q = x ++ (h::y)) /\ (sublist t y)) ==>
     (sublist (h::t) (h'::q)  <=> ?x y. (h'::q = x ++ (h::y)) /\ (sublist t y))
     Applying SUBLIST definition and split the if-and-only-if parts, there are 4 cases:
     When h = h', if part:
       sublist (h::t) (h::q) ==> ?x y. (h::q = x ++ (h::y)) /\ (sublist t y)
       For this case, choose x=[], y=q, and sublist (h::t) (h::q) = sublist t q, by SUBLIST same head.
     When h = h', only-if part:
       ?x y. (h::q = x ++ (h::y)) /\ (sublist t y) ==> sublist (h::t) (h::q)
       Take cases on x.
       When x = [],
         h::q = h::y ==> q = y,
         hence sublist t y = sublist t q ==> sublist (h::t) (h::q) by SUBLIST same head.
       When x = h''::t',
         h::q = (h''::t') ++ (h::y) = h''::(t' ++ (h::y)) ==>
         q = t' ++ (h::y),
         hence sublist t y ==> sublist t q (by SUBLIST_APPENDR_I) ==> sublist (h::t) (h::q).
     When ~(h=h'), if part:
       sublist (h::t) (h'::q) ==> ?x y. (h'::q = x ++ (h::y)) /\ (sublist t y)
       From hypothesis,
             sublist (h::t) (h'::q)
           = sublist (h::t) q           when ~(h=h'), by SUBLIST definition
         ==> ?x1 y1. (q = x1 ++ (h::y1)) /\ (sublist t y1))  by inductive hypothesis
         Now (h'::q) = (h'::(x1 ++ (h::y1)) = (h'::x1) ++ (h::y1) by APPEND associativity
         So taking x = h'::x1, y = y1, this gives the conclusion.
     When ~(h=h'), only-if part:
       ?x y. (h'::q = x ++ (h::y)) /\ (sublist t y) ==> sublist (h::t) (h'::q)
       The case x = [] is impossible by list equality, since ~(h=h').
       Hence x = h'::t', giving:
            q = t'++(h::y) /\ (sublist t y)
          = sublist (h::t) q                      by inductive hypothesis (only-if)
        ==> sublist (h::t) (h'::q)        by SUBLIST, different head.
*)
val SUBLIST_EX_APPEND = store_thm(
  "SUBLIST_EX_APPEND",
  ``!h t q. h::t <= q  <=> ?x y. (q = x ++ (h::y)) /\ (t <= y)``,
  Induct_on `q` THEN SRW_TAC [][SUBLIST_NIL] THEN
  Cases_on `h = h'` THEN SRW_TAC [][SUBLIST, EQ_IMP_THM] THENL [
    Q.EXISTS_TAC `[]` THEN SRW_TAC [][],
    Cases_on `x` THEN FULL_SIMP_TAC (srw_ss()) [] THEN METIS_TAC [APPEND_ASSOC, SUBLIST_APPENDR_I],
    Q.EXISTS_TAC `h::x` THEN SRW_TAC [][SUBLIST],
    Cases_on `x` THEN FULL_SIMP_TAC (srw_ss()) [] THEN METIS_TAC []
  ]);
(*WAIT*)

(* Theorem: [Induction] For any property P satisfying,
   (a)!y. P [] y = T
   (b)!h x y. P x y /\ sublist x y ==> P (h::x) (h::y)
   (c)!h x y. P x y /\ sublist x y ==> P x (h::y)
   then  !x y. sublist x y ==> P x y.
*)
val SUBLIST_IND = store_thm(
  "SUBLIST_IND",
  ``!P. (!y. P [] y) /\
        (!h x y. P x y /\ x <= y ==> P (h::x) (h::y)) /\
        (!h x y. P x y /\ x <= y ==> P x (h::y)) ==>
        !x y. x <= y ==> P x y``,
  GEN_TAC THEN STRIP_TAC THEN
  Induct_on `y` THEN1 SRW_TAC [][SUBLIST_NIL] THEN
  Cases_on `x` THEN SRW_TAC [][SUBLIST] THEN
  FULL_SIMP_TAC (srw_ss()) []);
(*WAIT*)

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
