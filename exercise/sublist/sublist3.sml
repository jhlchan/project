(* Sublist Exercise - version 3 *)

(*
1.  Define the notion of sublist.

     sublist l1 l2 <=> "all the elements of l1 appear in l2, in the order in which they appear in l1"

3.  Try an inductive relation definition

     val (sublist2_rules, sublist2_ind, sublist2_cases) = Hol_reln`
       (sublist2 [] []) /\
       (!h. sublist2 l1 l2 ==> sublist2 (h::l1) (h::l2)) /\
       (!h. sublist2 l1 l2 ==> sublist2 l1 (h::l2))
     `;

    Then, as before, you should show

    sublist l1 l2 = sublist2 l1 l2
*)

(* Proof Strategy
   ==============

   What the induction relation gives is a process to construct lists p q related by sublist2:
   (1) start with empty lists to have: sublist2 [] [].
   (2) if you have sublist2 p q, you can add same head to both, giving: sublist2 (h::p) (h::q).
   (3) if you have sublist2 p q, you can add a head to the second, giving: sublist2 p (h::q).
   That's all, which means, for example, you cannot just add a head to the first for sublist2 relation.

   These construction rules are given in sublist2_rules.

   To establish proofs based on the result of such constructions, the induction relation provide cases.

   Corresponding to the rules, there are 3 cases for sublist2 p q:
   (1) if p = [] and q = [], or
   (2) if ?h t1 t2. p = (h::t1) and q = (h::t2) and (sublist2 t1 t2), or
   (3) if ?h t2. q = (h::t2) and (sublist2 p t2).

   These cases are given in sublist2_cases.

   Such cases are similar in the definition of sublist, but not exactly. The most significant
   being case (3), which for sublist is the key lemma: sublist p q ==> sublist p (h::q).

   In proving the equivalence of sublist2 and sublist, both rules and cases are important.
*)

(* make assumptions explicit in proofs *)
show_assums := true;

(* Get sublist theory *)
load "sublistTheory";
open sublistTheory;
(*WAIT*)

(* Define sublist, using inductive relation *)
val (sublist2_rules, sublist2_ind, sublist2_cases) = Hol_reln`
    (sublist2 [] []) /\
    (!h. sublist2 l1 l2 ==> sublist2 (h::l1) (h::l2)) /\
    (!h. sublist2 l1 l2 ==> sublist2 l1 (h::l2))
`;
(*WAIT*)

(* Check sublist2 *)
EVAL ``sublist2 [1; 1] [2; 1; 2; 1]``; (* repeats definition *)
g `sublist2 [1; 1] [2; 1; 2; 1]`;
e (SRW_TAC [][sublist2_rules]); (* T *)
drop();
(*WAIT*)

(* get definitions *)
val SUBLIST2 = sublist2_rules;
val SUBLIST = sublist_def;
(*WAIT*)


(* Theorem: sublist2 [] q *)
(* Proof:
   By induction on list q.
   Base case is to prove: sublist2 [] [], which is T by rule (1).
   Step case is to prove: sublist [] q ==> sublist [] [h::q], which is T by rule (2).
*)
val SUBLIST2_EMPTY = store_thm(
  "SUBLIST2_EMPTY",
  ``!q. sublist2 [] q``,
  Induct_on `q` THEN SRW_TAC [][SUBLIST2]);
(*WAIT*)

(* Theorem: sublist2 (h::t) [] = F *)
(* Proof:
   By cases of sublist2.
   case (1) cannot be satisfied since a0=(h::t)  cannot be [].
   case (2) cannot be satisfied since a1=(h::l2) cannot be [].
   case (3) cannot be satisfied since a1=(h::l2) cannot be [].
*)
val SUBLIST2_NOT_EMPTY = store_thm(
  "SUBLIST2_NOT_EMPTY",
  ``sublist2 (h::t) [] = F``,
  SRW_TAC [][Once sublist2_cases]); (* the Once directive is essential *)
(*WAIT*)

(* Theorem: sublist2 p [] <=> p = [] *)
(* Proof:
   By cases of sublist2.
   case (1) can be satisfied by a0=[].
   case (2) cannot be satisfied since a1=(h::l2) cannot be [].
   case (3) cannot be satisfied since a1=(h::l2) cannot be [].
*)
val SUBLIST2_NIL = store_thm(
  "SUBLIST2_NIL",
  ``!p. sublist2 p [] <=> (p = [])``,
  SRW_TAC [][Once sublist2_cases]); (* the Once directive is essential *)
(*WAIT*)

(* Theorem: sublist2 p p *)
(* Proof:
   By induction on list p.
   Base case: sublist2 [] [] is T by rule (1).
   Step case: sublist2 p p ==> sublist2 (h::p) (h::p) is T by rule (2).
*)
val SUBLIST2_REFL = store_thm(
  "SUBLIST2_REFL",
  ``!p. sublist2 p p``,
  Induct THEN SRW_TAC [][SUBLIST2]);
(*WAIT*)

(* Theorem: sublist2 (h::p) q ==> sublist2 p q *)
(* Proof:
   By induction on list q.
   Base case is to prove:
     sublist2 (h::p) [] ==> sublist2 p []
     The hypothesis is F by SUBLIST2_NIL, hence implication is valid.
   Step case is to prove:
     (sublist2 (h::p) q ==> sublist2 p q) ==> (sublist2 (h::p) (h'::q) ==> sublist2 p (h'::q))
     By cases on sublist2,
     case (1) cannot fit (h::p) and (h'::q).
     case (2) can fit when h' = h,
         sublist2 (h::p) (h'::q)
       = sublist2 (h::p) (h::q)
       = sublist2 p q            by case (2)
     ==> sublist2 p (h'::q)      by rule (3).
     case (3) can fit (h'::q),
         sublist2 (h::p) (h'::q)
       = sublist2 (h::p) q       by case (3)
     ==> sublist2 p q            by induction hypothesis
     ==> sublist2 p (h'::q)      by rule (3).
*)
val SUBLIST2_CONS = store_thm(
  "SUBLIST2_CONS",
  ``!h p q. sublist2 (h::p) q ==> sublist2 p q``,
  Induct_on `q` THENL [
    SRW_TAC [][SUBLIST2_NIL],
    SRW_TAC [][Once sublist2_cases] THEN PROVE_TAC [SUBLIST2]
  ]);
(*WAIT*)

(* Theorem: sublist2 (h::p) (h::q) ==> sublist2 p q *)
(* Proof:
   By cases of sublist2.
     case (1) cannot fit (h::p) and (h::q).
     case (2) can fit (h::p) and (h::q),
         sublist2 (h::p) (h::q)
       = sublist2 p q            by case (2).
     case (3) can fit (h::q),
         sublist2 (h::p) (h::q)
       = sublist2 (h::p) q       by case (3)
     ==> sublist2 p q            by SUBLIST2_CONS.
*)
val SUBLIST2_SAME_HD = store_thm(
  "SUBLIST2_SAME_HD",
  ``!h p q. sublist2 (h::p) (h::q) ==> sublist2 p q``,
  SRW_TAC [][Once sublist2_cases] THEN PROVE_TAC [SUBLIST2_CONS]);
(*WAIT*)

(* Theorem: ~(h = k) /\ sublist2 (h::p) (k::q) ==> sublist2 (h::p) q *)
(* Proof:
   By cases of sublist2.
     case (1) cannot fit (h::p) and (k::q).
     case (2) can fit (h::p) and (k::q) with ~(h = k).
     case (3) can fit (k::q),
         sublist2 (h::p) (k::q)
       = sublist2 (h::p) q       by case (3).
*)
val SUBLIST2_DIFF_HD = store_thm(
  "SUBLIST2_DIFF_HD",
  ``!h k p q. ~(h = k) /\ sublist2 (h::p) (k::q) ==> sublist2 (h::p) q``,
  SRW_TAC [][Once sublist2_cases]);
(*WAIT*)

(* Theorem: sublist2 p q ==> sublist p q *)
(* Proof:
   By induction on list q.
   Base case is to prove:
     sublist2 p [] ==> sublist p [].
     Hypothesis gives p = [] by SUBLIST2_NIL.
     Conclusion becomes sublist [][], T by SUBLIST defintion.
   Step case is to prove:
     (sublist2 p q ==> sublist p q) ==> (sublist2 p (h::q) ==> sublist p (h::q))
     Take cases on list p.
     When p = [], the conclusion is trivial by SUBLIST definition.
     When p = h'::t,
       if h = h',    sublist2 (h::t) (h::q)
                 ==> sublist2 t q             by SUBLIST2_SAME_HD
                 ==> sublist t q             by inductive hypothesis
                   = sublist (h::t) (h::q)   by SUBLIST definition.
       if ~(h = h'), sublist2 (h::t) (h'::q)
                 ==> sublist2 (h::t) q        by SUBLIST2_DIFF_HD
                 ==> sublist (h::t) q        by inductive hypothesis
                   = sublist (h::t) (h'::q)  by SUBLIST definition when ~(h=h').
*)
val SUBLIST2_SUBLIST = store_thm(
  "SUBLIST2_SUBLIST",
  ``! p q. sublist2 p q ==> p <= q``,
  Induct_on `q` THEN SRW_TAC [][SUBLIST2_NIL, SUBLIST] THEN
  Cases_on `p` THEN1 SRW_TAC [][SUBLIST] THEN
  Cases_on `h = h'` THEN PROVE_TAC [SUBLIST2_SAME_HD, SUBLIST2_DIFF_HD, SUBLIST]);
(*WAIT*)

(* Theorem: sublist p q ==> sublist2 p q *)
(* Proof:
   By induction on list q.
   Base case is to prove:
     sublist p [] ==> sublist2 p []
     Hypothesis gives p = [] by SUBLIST_NIL.
     Conclusion becomes sublist2 [][], T by SUBLIST2 rule (1).
   Step case is to prove:
     (sublist p q ==> sublist2 p q) ==> (sublist p (h::q) ==> sublist2 p (h::q))
     Take cases on list p.
     When p = [], the conclusion is T by SUBLIST2_EMPTY.
     When p = h'::t, to prove:
           (sublist (h'::t) q ==> sublist2 (h'::t) q)
       ==> (sublist (h'::t) (h::q) ==> sublist2 (h'::t) (h::q))
       Take cases of sublist2.
       case (1) cannot fit sublist2 (h'::t) (h::q).
       case (2) can fit if h = h', which is to prove:
             (sublist p q ==> sublist2 p q)
         ==> (sublist (h::t) (h::q) ==> sublist2 t q)
         Now sublist (h::t) (h::q)
           = sublist t q               by SUBLIST definition, same head.
         ==> sublist2 t q               by inductive hypothesis.
       case (3) fits when ~(h=h'), which is to prove:
             (sublist p q ==> sublist2 p q)
         ==> (sublist (h'::t) (h::q) ==> sublist2 (h'::t) q)
         Now sublist (h'::t) (h::q)
           = sublist (h'::t) q         by SUBLIST definition, different head.
         ==> subiist3 (h'::t)           by inductive hypothesis.
*)
val SUBLIST_SUBLIST2 = store_thm(
  "SUBLIST_SUBLIST2",
  ``!p q. p <= q ==> sublist2 p q``,
  Induct_on `q` THEN1 SRW_TAC [][SUBLIST_NIL, SUBLIST2] THEN
  SRW_TAC [][SUBLIST, SUBLIST2] THEN
  Cases_on `p` THEN1 SRW_TAC [][SUBLIST2_EMPTY] THEN
  SRW_TAC [][Once sublist2_cases] THEN
  Cases_on `h = h'` THEN PROVE_TAC [SUBLIST]);
(*WAIT*)

(* Theorem: sublist2 = sublist *)
(* Proof:
   The      "if" part is proved by SUBLIST2_SUBLIST.
   The "only if" part is proved by SUBLIST_SUBLIST2.
*)
val SUBLIST2_EQ_SUBLIST = store_thm(
  "SUBLIST2_EQ_SUBLIST",
  ``sublist2 = sublist``,
  SRW_TAC [][FUN_EQ_THM, EQ_IMP_THM, SUBLIST2_SUBLIST, SUBLIST_SUBLIST2]);
(*WAIT*)
