(* Structural Induction Exercise *)

(* some list built-in functions *)
val x = 1::2::3::4::5::[];
length x;
fun sq x = x*x;
map sq x;
(* Theorem: length (map f x) = length x *)
g `!x:'a list. length (map f x) = length x`;
e (Induct_on `x`);
length;
length_def; (* no definition *)
map;
map_def; (* no definition *)
(* no definition to use for rewrite rules? *)
dropn 1;

(* Define len *)
val len_def = Define`
    (len [] = 0) /\
    (len (h::t) = 1 + len t)
`;

(* Define map *)
val mpp_def = Define`
    (mpp f [] = []) /\
    (mpp f (h::t) = (f h)::(mpp f t))
`;


(* Theorem: length (map f x) = length x *)
g `len (mpp f x) = len x`;
(* Proof:
   By induction on list x.
   Base case: to prove:
   length (map f []) = length []
   This is because:
   length (map f []) = length []  by map definition.
   Step case: to prove:
   (length (map f t) = length t) =>
   (length (map f (h::t) = length (h::t))
   This is because:
     length (map f (h::t)
   = length (f h)::(map f t)   by map defintion
   = 1 + length (map f t)      by length definition
   = 1 + lenght t              by assumption
   = length (h::t)             by length defintion
*)
e (Induct_on `x`);
(*
OK..
2 subgoals:
> val it =
!h. len (mpp f (h::x)) = len (h::x)
------------------------------------
len (mpp f x) = len x

len (mpp f []) = len []

: proof
*)
e (SRW_TAC [][mpp_def, len_def]);
(*
OK..

Goal proved.
|- len (mpp f []) = len []

Remaining subgoals:
> val it =
!h. len (mpp f (h::x)) = len (h::x)
------------------------------------
len (mpp f x) = len x
: proof
*)
e (SRW_TAC [][mpp_def, len_def]);
(*
OK..

Goal proved.
[.] |- !h. len (mpp f (h::x)) = len (h::x)
> val it =
Initial goal proved.
|- len (mpp f x) = len x : proof
*)
dropn 1;
val MAP_LEN = store_thm("MAP_LEN",
    ``len (mpp f x) = len x``,
    Induct_on `x` THEN SRW_TAC [][mpp_def, len_def]);
(*
> val MAP_LEN = |- len (mpp f x) = len x : thm
*)

(* append is defined, but no defintion *)
append;
append_def; (* no definition *)

(* Define append *)
val app_def = Define`
    (app [] x = x) /\
    (app (h::t) x = h::(app t x))
`;

(* Theorem: length (append x y) = (length x) + (length y) *)
g `len (app x y) = (len x) + (len y)`;
(* Proof:
   By induction on list x.
   Base case: to prove:
   length (append [] y) = length [] + length y
   This is because:
   length (append [] y) = length y,             by append definition
                        = 0 + length y
                        = length [] + length y, by length definition
   Step case: to prove:
   (length (append t y) = length t + length y) =>
   (length (append (h::t) y) = length (h::t) + length y)
   This is because:
     length (append (h::t) y
   = length (h::(append t y))   by append defintion
   = 1 + length (append t y)    by length definition
   = 1 + (lenght t + length y)  by assumption
   = (1 + length t) + length y
   = length (h::t) + length y   by length definition
*)
e (Induct_on `x`);
(*
OK..
2 subgoals:
> val it =
!h. len (app (h::x) y) = len (h::x) + len y
------------------------------------
len (app x y) = len x + len y

len (app [] y) = len [] + len y

: proof
*)
e (SRW_TAC [][app_def, len_def]);
(*
OK..

Goal proved.
|- len (app [] y) = len [] + len y

Remaining subgoals:
> val it =
!h. len (app (h::x) y) = len (h::x) + len y
------------------------------------
len (app x y) = len x + len y
: proof

*)
e (SRW_TAC [][app_def, len_def]);
(*
OK..
1 subgoal:
> val it =
1 + (len x + len y) = 1 + len x + len y
------------------------------------
len (app x y) = len x + len y
: proof
*)
e (RW_TAC arith_ss []);
(*
OK..

Goal proved.
|- 1 + (len x + len y) = 1 + len x + len y

Goal proved.
[.] |- !h. len (app (h::x) y) = len (h::x) + len y
> val it =
Initial goal proved.
|- len (app x y) = len x + len y : proof
*)
dropn 1;
val APPEND_LEN = store_thm("APPEND_LEN",
    ``len (app x y) = (len x) + (len y)``,
    Induct_on `x` THEN RW_TAC arith_ss [app_def, len_def]);
(*
> val APPEND_LEN = |- len (app x y) = len x + len y : thm
*)

(* Theorem: map f (append x y) = append (map f x) (map f y) *)
g `mpp f (app x y) = app (mpp f x) (mpp f y)`;
(* Proof:
   By induction on list x.
   Base case: to prove:
   map f (append [] y) = append (map f []) (map f y)
   This is because:
     map f (append [] y)
   = map f y                       by append definition
   = append [] (map f y)           by append definition
   = append (map f []) (map f y)   by map definition
   Step case: to prove:
   (map f (append t y) = append (map f t) (map f y)) ==>
   (map f (append (h::t) y) = append (map f (h::t)) (map f y))
   This is because:
     map f (append (h::t) y)
   = map f (h::(append t y))              by append definition
   = (f h)::(map f (append t y))          by map definition
   = (f h)::(append (map f t) (map f y))  by assumption
   = append (f h)::(map f t) (map f y)    by append definition
   = append (map f (h::t)) (map f y)      by map definition
*)
e (Induct_on `x`);
(*
OK..
2 subgoals:
> val it =
!h. mpp f (app (h::x) y) = app (mpp f (h::x)) (mpp f y)
------------------------------------
mpp f (app x y) = app (mpp f x) (mpp f y)

mpp f (app [] y) = app (mpp f []) (mpp f y)

: proof
*)
e (SRW_TAC [][app_def, mpp_def]);
(*
OK..

Goal proved.
|- mpp f (app [] y) = app (mpp f []) (mpp f y)

Remaining subgoals:
> val it =
!h. mpp f (app (h::x) y) = app (mpp f (h::x)) (mpp f y)
------------------------------------
mpp f (app x y) = app (mpp f x) (mpp f y)
: proof
*)
e (SRW_TAC [][app_def, mpp_def]);
(*
OK..

Goal proved.
[.] ? ?h. mpp f (app (h::x) y) = app (mpp f (h::x)) (mpp f y)
> val it =
Initial goal proved.
|- mpp f (app x y) = app (mpp f x) (mpp f y) : proof
*)
dropn 1;
val MAP_APPEND = store_thm("MAP_APPEND",
    ``mpp f (app x y) = app (mpp f x) (mpp f y)``,
    Induct_on `x` THEN SRW_TAC [][app_def, mpp_def]);
(*
> val MAP_APPEND = |- mpp f (app x y) = app (mpp f x) (mpp f y) : thm
*)

(* Define sum by recursion *)
val sum1_def = Define`
    (sum1 [] = 0) /\
    (sum1 (h::t) = h + sum1 t)
`;

(* Define sum by accumulator *)
val suma_def = Define`
    (suma x [] = x) /\
    (suma x (h::t) = suma (x+h) t)
`;
val sum2_def = Define`
    (sum2 x = suma 0 x)
`;

(* Theorem: sum1 x = sum2 x *)
g `sum1 x = sum2 x`;
(* Proof:
   By induction on list x.
   Base case: to prove:
   sum1 [] = sum2 []
   This is because:
   sum1 [] = 0           by sum1 definition
   sum2 [] = suma 0 []   by sum2 definition
           = 0           by suma definition
   Step case: to prove:
   (sum1 t = sum2 t) ==> (sum1 (h::t) = sum2 (h::t))
   This is because:
   sum1 (h::t) = h + sum1 t    by sum1 definition
   sum2 (h::t) = suma 0 (h::t) by sum2 definition
               = suma (0+h) t  by suma definition
               = suma h t
               (and we got stuck!)
*)
e (Induct_on `x`);
(*
OK..
2 subgoals:
> val it =
!h. sum1 (h::x) = sum2 (h::x)
------------------------------------
sum1 x = sum2 x

sum1 [] = sum2 []

: proof
*)
e (SRW_TAC [][sum1_def, sum2_def, suma_def]);
(*
OK..

Goal proved.
|- sum1 [] = sum2 []

Remaining subgoals:
> val it =
!h. sum1 (h::x) = sum2 (h::x)
------------------------------------
sum1 x = sum2 x
: proof
*)
e (SRW_TAC [][sum1_def, sum2_def, suma_def]);
(*
OK..
1 subgoal:
> val it =
h + suma 0 x = suma h x
------------------------------------
sum1 x = sum2 x
: proof
*)

(* Prove a lemma about suma: h + suma k x = suma (h+k) x *)
g `h + suma k x = suma (h+k) x`;
(* Proof:
   By induction on list x.
   Base case: to prove:
   h + suma k [] = suma (h+k) []
   This is because:
   h + suma k []
   = h + k           by suma definition
   = suma (h+k) []   by suma definition
   Step case: to prove:
   (h + suma k t = suma (h+k) t) ==>
   (h + suma k (s::t) = suma (h+k) (s::t))
   This is because:
     h + suma k (s::t)
   = h + suma (k+s) t    by suma definition
   = suma (h+k+s) t      by assumption
   = suma (h+k) (s::t)   by suma definition
*)
e (Induct_on `x`);
(*
OK..
2 subgoals:
> val it =
!h'. h + suma k (h'::x) = suma (h + k) (h'::x)
------------------------------------
h + suma k x = suma (h + k) x

h + suma k [] = suma (h + k) []

: proof
*)
e (SRW_TAC [][suma_def]);
(*
OK..

Goal proved.
|- h + suma k [] = suma (h + k) []

Remaining subgoals:
> val it =
!h'. h + suma k (h'::x) = suma (h + k) (h'::x)
------------------------------------
h + suma k x = suma (h + k) x
: proof
*)
e (SRW_TAC [][suma_def]);
(*
OK..
1 subgoal:
> val it =
h + suma (k + h') x = suma (h + k + h') x
------------------------------------
h + suma k x = suma (h + k) x
: proof
*)
(* Here, we can prove by:
   Replace "k" in assumption "h + suma k x = suma (h + k) x"
   by "(k + h')" will give the conclusion.
*)
(* But how to put this into a tactic? *)
