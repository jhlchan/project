(* Sublist Exercise *)

(*
1. Define the notion of sublist.

     sublist l1 l2 <=> "all the elements of l1 appear in l2,
                        in the order in which they appear in l1"

   You might like to define this
   (a) by recursion on l1,
   (b) by recursion on l2, or
   (c) perhaps with an inductive relation.

   In fact, it's an interesting exercise to figure out all three.

2. Prove that the relation is reflexive, anti-symmetric and transitive.
*)

(* For listTheory.LENGTH later *)
open listTheory;
(*WAIT*)
(* make assumptions explicit in proofs *)
show_assums := true;

(* sublist with ordering, using recursion on first argument *)
val SUBLIST = Define`
    (sublist [] l = T) /\
    (sublist (h::t) [] = F) /\
    (sublist (h1::t1) (h2::t2) =
            ((h1 = h2) /\ sublist t1 t2) \/
            (~(h1 = h2) /\ sublist (h1::t1) t2))
`;


(* set infix and overload for sublist *)
val _ = set_fixity "<=" (Infixl 480);
val _ = overload_on ("<=", ``sublist``);

(* The following is skipped to force explicit use of definition *)
(* val _ = export_rewrites ["sublist_def"]; *)
(*PAUSE*)

(* Some examples to show the sublist definition is OK *)

(*    [1,2] <= [3,4]
    = [1,2] <= [4]
    = [1,2] <= []
    = F
*)
g `(1::2::[]) <= (3::4::[])`;
e (SRW_TAC [][SUBLIST]); (* F *)
drop();
(*    [1,2] <= [2,1]
    = [1,2] <= [1]
    = [2] <= []
    = F
*)
g `(1::2::[]) <= (2::1::[])`;
e (SRW_TAC [][SUBLIST]); (* F *)
drop();
(*    [1,2] <= [1, 3, 2]
    = [2] <= [3, 2]
    = [2] <= [2]
    = [] <= []
    = T
*)
g `(1::2::[]) <= (1::3::2::[])`;
e (SRW_TAC [][SUBLIST]); (* T *)
drop();
(*
      [1,2] <= [0,1,3,2]
    = [1,2] <= [1,3,2]
    = ... = T
*)
g `(1::2::[]) <= (0::1::3::2::[])`;
e (SRW_TAC [][SUBLIST]); (* T *)
drop();
(*PAUSE*)

(* A useful theorem about sublist. *)

(* Theorem: The only sublist of [] is [] itself. *)
g `!p. p <= [] <=> (p = [])`;
(* Proof:
   By cases on list p.
   First case: p = [].
        The right-side [] = [] is T,
        and left-side [] <= [] is T by SUBLIST definition.
        Hence T <=> T.
   Next case: p = (h::t).
        The right-side h::t = [] is F,
        and left-side h::t <= [] is F by SUBLIST definition.
        Hence F <=> F.
*)
e (Cases_on `p`);
e (SRW_TAC [] [SUBLIST]); (* solves case p = [] *)
e (SRW_TAC [] [SUBLIST]); (* solves case p = (h::t) *)
(* OK *)
drop();
val SUBLIST_NIL = store_thm(
    "SUBLIST_NIL",
    ``!p. p <= [] <=> (p = [])``,
    Cases_on `p` THEN SRW_TAC [][SUBLIST]);

(* Skip this to make the use of theorems explicit. *)
(* val _ = export_rewrites ["SUBLIST_NIL"]; *)
(*PAUSE*)

(* The first sublist property. ---------------------------------------- *)

(* Theorem [Reflexive]: for all list p, p <= p is true. *)
g `!p. p <= p`;
(* Proof:
   By induction on list p.
   Base case: p = [], [] <= [] is T, by SUBLIST definition.
   Step case: t <= t ==> (h::t) <= (h::t) is T, by SUBLIST defintion.
*)
e (Induct); (* by induction on the only variable *)
e (SRW_TAC [][SUBLIST]); (* solves base case *)
e (SRW_TAC [][SUBLIST]); (* solves step case *)
(* OK *)
drop();
val SUBLIST_REFL = store_thm(
    "SUBLIST_REFL",
    ``!p:'a list. p <= p``,
    Induct THEN SRW_TAC [][SUBLIST]);
(* val _ = export_rewrites ["SUBLIST_REFL"]; -- skipped *)
(*PAUSE*)

(* The next sublist property. ----------------------------------------- *)

(*
Many attempts are not successful, so sit back, and think carefully.

All the simple cases, where p = [] or q = [], are easy to prove.

The hard case is this: (h1::t1) <= (h2::t2) and (h2::t2) <= (h1::t1) ==> (h1::t1) = (h2::t2)

How to show two lists are equal? Their heads are equal, so are their tails.

Why are their heads equal? What if their heads are not equal?

In that case we have (h1::t1) <= t2, and (h2::t2) <= t1, which is impossible if t1 = t2.

Why is this impossible? Because of their lengths: the lists are of equal length when sublist of one another.

This suggests first proving the following key lemma.
*)

(* Theorem [Relates SUBLIST and LENGTH]:
        for two lists p q,  p <= q ==> length(p) <= length(q)  *)
g `!p q. p <= q ==> (LENGTH p) <= (LENGTH q)`;
(* Proof:
   By induction on list q.
   Trivial cases are easy, so consider these non-trivial cases.
   case h = h',  h::t <= h::q ==> LENGTH (h::p) <= LENGTH (h::q) with inductive assumption.
       h::t <= h::q
   ==> t <= q                           by SUBLIST_SAME_HD
   ==> LENGTH t <= LENGTH q             by inductive assumption
   ==> SUC(LENGTH t) <= SUC(LENGTH q)   by arithmetic
   ==> LENGTH h::t <= LENGTH h::q       by LENGTH definition

   case ~(h = h'), h'::t <= h::q ==> LENGTH (h'::t) <= LENGTH (h::q)
       h'::t <= h::q
   ==> h'::t <= q                    by SUBLIST_DIFF_HD
   ==> LENGTH h'::t <= LENGTH q      by inductive assumption
   ==> LENGTH h'::t <= SUC(LENGTH q) by smart arithmetic
   ==> LENGTH h'::t <= LENGTH h::q   by LENGTH definition
*)
e (Induct_on `q`);
e (SRW_TAC [][SUBLIST_NIL]); (* solves base case *)
(* for the step case *)
(*
 !h p. p <= h::q ==> LENGTH p <= LENGTH (h::q)
------------------------------------
!p. p <= q ==> LENGTH p <= LENGTH q
*)
e (SRW_TAC [][SUBLIST]); (* apply definition *)
(*
 LENGTH p <= SUC (LENGTH q)
------------------------------------
0. !p. p <= q ==> LENGTH p <= LENGTH q
1. p <= h::q
*)
e (Cases_on `p`);
(*
 LENGTH (h'::t) <= SUC (LENGTH q)
------------------------------------
0. !p. p <= q ==> LENGTH p <= LENGTH q
1. h'::t <= h::q

LENGTH [] <= SUC (LENGTH q)
------------------------------------
0. !p. p <= q ==> LENGTH p <= LENGTH q
1. [] <= h::q
*)
e (FULL_SIMP_TAC (srw_ss()) [SUBLIST]); (* solves case p = [] *)
e (FULL_SIMP_TAC (srw_ss()) [SUBLIST]); (* again apply definition *)
(*
 LENGTH t <= LENGTH q
------------------------------------
0. !p. p <= q => LENGTH p <= LENGTH q
1. h' �� h
2. h'::t <= q
*)
e (`LENGTH (h'::t) <= LENGTH q` by METIS_TAC []); (* assert by inductive assumption *)
(*
 LENGTH t <= LENGTH q
------------------------------------
0. !p. p <= q ==> LENGTH p <= LENGTH q
1. h' �� h
2. h'::t <= q
3. LENGTH (h'::t) <= LENGTH q
*)
e (FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []); (* all simpsets and arithmetic *)
(* OK *)
drop();
val SUBLIST_LENGTH = store_thm(
    "SUBLIST_LENGTH",
    ``!p q. p <= q ==> LENGTH p <= LENGTH q``,
    Induct_on `q` THEN SRW_TAC [][SUBLIST_NIL, SUBLIST] THEN
    Cases_on `p` THEN FULL_SIMP_TAC (srw_ss()) [SUBLIST] THEN
    `LENGTH (h'::t) <= LENGTH q` by METIS_TAC [] THEN
    FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []);
(* a bit shorter proof *)
val SUBLIST_LENGTH = store_thm(
    "SUBLIST_LENGTH",
    ``!p q. p <= q ==> LENGTH p <= LENGTH q``,
    Induct_on `q` THEN SRW_TAC [][SUBLIST] THEN
    Cases_on `p` THEN FULL_SIMP_TAC (srw_ss()) [SUBLIST] THEN
    `LENGTH (h'::t) <= LENGTH q` by METIS_TAC [] THEN
    FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []);
(*PAUSE*)

(* Theorem [Anti-symmetric]: (p <= q) /\ (q <= p) ==> (p = q) *)
g `!p q. (p <= q) /\ (q <= p) ==> (p = q)`;
(* Proof:
   By induction on list q.
   Base case is to prove: (p <= []) /\ ([] <= p) ==> (p = [])
        [] <= p gives p = []  by SUBLIST_NIL.
   Step case is to prove: (p <= h::t) /\ (h::t <= p) ==> (p = h::t)
        with the assumption: !p. (p <= t) /\ (t <= p) ==> (p = t)
        Consider cases of list p.
        Case p = [],
            [] <= (h::t) is T by SUBLIST definition.
            (h::t) <= [] is F by SUBLIST definition.
            [] = (h::t)  is F.
            Hence  T /\ F ==> F, the implication is valid.
        Case p = (h1::t1),
            (h1::t1 <= h::t) /\ (h::t <= h1::t1) ==> (h1::t1 = h::t)
            Case h1 = h,
                (h::t1 <= h::t) /\ (h::t <= h::t1)
              = (t1 <= t) /\ (t <= t1)   by SUBLIST_SAME_HD
              = t1 = t                   by inductive assumption
              = h::t1 = h::t             for any list
            Case ~(h1 = h),
                (h1::t1 = h::t) = F      since heads differ.
            but (h1::t1 <= h::t) = h::t1 <= t  by SUBLIST_DIFF_HD
                ==> LENGTH h1::t1 <= LENGTH t  by SUBLIST_LENGTH
                ==> LENGTH t1 < LENGTH t       by arithmetic
            and (h::t <= h1::t1) = h::t <= t1  by SUBLIST_DIFF_HD
                ==> LENGTH h::t <= LENGTH t1   by SUBLIST_LENGTH
                ==> LENGTH t < LENGTH t1       by arithmetic
            Hence x < y and y < x, which is F.
            This is therefore F ==> F, a vaild implication.
*)
e (Induct_on `q`);
e (SRW_TAC [][SUBLIST_NIL]); (* sovles base case *)
(* for the step case *)
(*
 !h p. p <= h::q /\ h::q <= p ==> (p = h::q)
------------------------------------
!p. p <= q /\ q <= p ==> (p = q)
*)
e (Q.X_GEN_TAC `h2`); (* specialize (the quantifier h) to h2 *)
(*
 !p. p <= h2::q /\ h2::q <= p ==> (p = h2::q)
------------------------------------
!p. p <= q /\ q <= p ==> (p = q)
*)
e (Q.X_GEN_TAC `p`); (* specialize (the quantifier p) to p *)
(*
 p <= h2::q /\ h2::q <= p ==> (p = h2::q)
------------------------------------
!p. p <= q /\ q <= p ==> (p = q)
*)
b();
b();
(* for the qualifiers !h p, specialize h to h2, p to p. *)
e (MAP_EVERY Q.X_GEN_TAC [`h2`, `p`]);
(*
 p <= h2::q /\ h2::q <= p ==> (p = h2::q)
------------------------------------
!p. p <= q /\ q <= p ==> (p = q)
*)
e (`(p = []) \/ ?h1 t1. (p = h1::t1)` by (Cases_on `p` THEN SRW_TAC [][SUBLIST]));
(*
 p <= h2::q /\ h2::q <= p ==> (p = h2::q)
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. p = h1::t1

p <= h2::q /\ h2::q <= p ==> (p = h2::q)
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. p = []
*)
e (SRW_TAC [][SUBLIST]); (* solves case p = [] *)
(*
 p <= h2::q /\ h2::q <= p ==> (p = h2::q)
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. p = h1::t1
*)
e (SRW_TAC [][SUBLIST]);
(*
 t1 = q
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. h1 �� h2
2. h1::t1 <= q
3. h2 �� h1
4. h2::q <= t1

h1 = h2
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. h1 �� h2
2. h1::t1 <= q
3. h2 �� h1
4. h2::q <= t1

t1 = q
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. t1 <= q
2. q <= t1
*)
e (METIS_TAC []); (* solves first case: from p = h1::t1 with h1 = h2 *)
(* the remaining cases are from p = h1::t1 with ~(h1 = h2) *)
(*
 t1 = q
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. h1 �� h2
2. h1::t1 <= q
3. h2 �� h1
4. h2::q <= t1

h1 = h2
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. h1 �� h2
2. h1::t1 <= q
3. h2 �� h1
4. h2::q <= t1
*)
(* these goals are to prove, for the conclusion (h1::t1 = h2::q), first h1 = h2, then t1 = q. *)
e (`LENGTH (h2::q) <= LENGTH t1 /\ LENGTH (h1::t1) <= LENGTH q` by METIS_TAC [SUBLIST_LENGTH]);
(*
 h1 = h2
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. h1 �� h2
2. h1::t1 <= q
3. h2 �� h1
4. h2::q <= t1
5. LENGTH (h2::q) <= LENGTH t1
6. LENGTH (h1::t1) <= LENGTH q
*)
e (FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []); (* all simpsets and arithmetic *)
(*
 t1 = q
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. h1 �� h2
2. h1::t1 <= q
3. h2 �� h1
4. h2::q <= t1
*)
e (`LENGTH (h2::q) <= LENGTH t1 /\ LENGTH (h1::t1) <= LENGTH q` by METIS_TAC [SUBLIST_LENGTH]);
(*
 t1 = q
------------------------------------
0. !p. p <= q /\ q <= p ==> (p = q)
1. h1 �� h2
2. h1::t1 <= q
3. h2 �� h1
4. h2::q <= t1
5. LENGTH (h2::q) <= LENGTH t1
6. LENGTH (h1::t1) <= LENGTH q
*)
e (FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []); (* all simpsets and arithmetic *)
(* OK *)
drop();
val SUBLIST_ANTISYM = store_thm(
    "SUBLIST_ANTISYM",
    ``!p q:'a list. p <= q /\ q <= p ==> (p = q)``,
    Induct_on `q` THEN1 (SRW_TAC [][SUBLIST_NIL]) THEN
    MAP_EVERY Q.X_GEN_TAC [`h2`, `p`] THEN
     `(p = []) \/ ?h1 t1. (p = h1::t1)` by (Cases_on `p` THEN SRW_TAC [][SUBLIST]) THEN1 SRW_TAC [][SUBLIST] THEN
    SRW_TAC [][SUBLIST] THEN1 METIS_TAC [] THEN
    `LENGTH (h2::q) <= LENGTH t1 /\ LENGTH (h1::t1) <= LENGTH q` by METIS_TAC [SUBLIST_LENGTH] THEN
    FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []);
(* shorter proof *)
val SUBLIST_ANTISYM = store_thm(
    "SUBLIST_ANTISYM",
    ``!p q:'a list. p <= q /\ q <= p ==> (p = q)``,
    Induct_on `q` THEN1 (SRW_TAC [][SUBLIST_NIL]) THEN
    MAP_EVERY Q.X_GEN_TAC [`h2`, `p`] THEN
     `(p = []) \/ ?h1 t1. (p = h1::t1)` by (Cases_on `p` THEN SRW_TAC [][SUBLIST]) THEN
    SRW_TAC [][SUBLIST] THEN1 METIS_TAC [] THEN
    `LENGTH (h2::q) <= LENGTH t1 /\ LENGTH (h1::t1) <= LENGTH q` by METIS_TAC [SUBLIST_LENGTH] THEN
    FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []);
(* Still shorter proof *)
val SUBLIST_ANTISYM = store_thm(
    "SUBLIST_ANTISYM",
    ``!p q:'a list. p <= q /\ q <= p ==> (p = q)``,
    Induct_on `q` THEN1 (SRW_TAC [][SUBLIST_NIL]) THEN
    MAP_EVERY Q.X_GEN_TAC [`h2`, `p`] THEN
    Cases_on `p` THEN1 SRW_TAC [][SUBLIST] THEN
    SRW_TAC [][SUBLIST] THEN1 METIS_TAC [] THEN
    `LENGTH (h2::q) <= LENGTH t /\ LENGTH (h::t) <= LENGTH q` by METIS_TAC [SUBLIST_LENGTH] THEN
    FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []);
(* This also works *)
val SUBLIST_ANTISYM = store_thm("SUBLIST_ANTISYM",
    ``!p q. (p <= q) /\ (q <= p) ==> (p = q)``,
    Induct_on `q` THENL [
        SRW_TAC [][SUBLIST_NIL],
        SRW_TAC [][SUBLIST] THEN
        Cases_on `p` THENL [
            PROVE_TAC [SUBLIST],
            Cases_on `h = h'` THENL [
                PROVE_TAC [SUBLIST],
                `h'::t <= q /\ h::q <= t` by PROVE_TAC [SUBLIST] THEN
                `LENGTH (h'::t) <= LENGTH q /\ LENGTH (h::q) <= LENGTH t` by PROVE_TAC [SUBLIST_LENGTH] THEN
                FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) []
            ]
        ]
    ]);
(*PAUSE*)

(* The last sublist property. ---------------------------------------- *)

(* Theorem [Transitive]: for all lists p, q, r; (p <= q) /\ (q <= r) ==> (p <= r) *)

(*

Again, many attempts are not successful, time to think.

The sublist anti-symmetric property has been established via a key idea:

    p <= q ==> length(p) <= length(q).

This forces lists p q with (p <= q) and (q <= p) to be of the same size,
ultimately forcing their heads to be equal,
with tails same size and sublist of each other, hence head of tail is equal, and so on.

Maybe the sublist transitive property also requires a key idea.

Why is sublist property transitive?

A picture of the situation (p <= q) and (q <= r) looks like this:

   p:     [1,1]
           | |
           | +-+
           |   |
   q:   [2,1,2,1]
         | | | |
         | | | +-----+
         | | |       |
   r: [3,2,1,2,2,3,2,1,3]

Obviously, p <= r  because by "following the lines".

How to capture this idea?

The diagram actually describes how the sublist relation interacts with append: there can be extra elements
without affecting the sublist relationship.

This suggests the following lemma.
*)

(* Theorem: !x p q. (p <= q) ==> (p <= x::q)  *)
g `!x p q. (p <= q) ==> (p <= x::q)`;
(* Proof:
   By induction on list q.
   Base case is to prove: (p <= []) ==> (p <= x::[])
        p <= [] means p = []  by SUBLIST_NIL.
        [] <= [x] is T,       by SUBLIST definition.
   Step case is to prove: (p <= h::t) ==> (p <= x::h::t)
        with the assumption: !p. p <= t ==> p <= x::t
        Consider cases of list p.
        Case p = [],
            [] <= h::t is T     by SUBLIST definition.
            [] <= x::h::t is T  by SUBLIST definition.
            Hence T ==> T, the implication is valid.
        Case p = (h1::t1),
            (h1::t1 <= h::t) ==> (h1::t1 <= x::h::t)
            Case h1 = h,
                to prove: (h::t1 <= h::t) ==> (h::t1 <= x::h::t)
                  h::t1 <= h::t
                = t1 <= t       by SUBLIST definition, same head....(a)
                ==> t1 <= h::t  by inductive assumption (p = t1, x = h)....(b)
                If h = x,
                  h::t1 <= x::h::t
                = t1 <= h::t    by SUBLIST definition, same head.
                = T             by (b).
                If ~(h = x),
                  h::t1 <= x::h::t
                = h::t1 <= h::t     by SUBLIST definition, different head.
                = t1 <= t           by SUBLIST definition, same head.
                = T                 by (a).
            Case ~(h1 = h),
                to prove: (h1::t1 <= h::t) ==> (h1::t1 <= x::h::t)
                  h1::t1 <= h::t
                = h1::t1 <= t       by SUBLIST definition, different head...(c)
                ==> h1::t1 <= x::t  by inductive assumption (p = h1::t1)....(d)
                If h1 = x,
                (d) is the same as
                    t1 <= t         by SUBLIST definition, same head.
                ==> t1 <= h::t      by inductive assumption again (p = t1, x = h) ... (e)
                Now h1::t1 <= x::h::t
                = t1 <= h::t        by SUBLIST definition, same head.
                = T                 by (e).
                If ~(h1 = x),
                  h1::t1 <= x::h::t
                = h1::t1 <= h::t    by SUBLIST definition, different head.
                = h1::t1 <= t       by SUBLIST definition, different head.
                = T                 by (c).
*)
e (Induct_on `q`);
e (PROVE_TAC [SUBLIST_NIL, SUBLIST]); (* solves base case *)
(* Step case
 !h x p. p <= h::q ==> p <= x::h::q
------------------------------------
!x p. p <= q ==> p <= x::q
*)
e (SRW_TAC [][]); (* simplify *)
e (Cases_on `p`);
e (PROVE_TAC [SUBLIST]); (* solves case p = [] *)
(*
 h'::t <= x::h::q
------------------------------------
0. !x p. p <= q ==> p <= x::q
1. h'::t <= h::q
*)
e (Cases_on `h = h'`);
(* case h = h' *)
e (SRW_TAC [][]); (* simplifies *)
(*
 h::t <= x::h::q
------------------------------------
0. !x p. p <= q ==> p <= x::q
1. h::t <= h::q
*)
e (`t <= q` by PROVE_TAC [SUBLIST]); (* SUBLIST same head *)
e (`t <= h::q` by PROVE_TAC []); (* apply assumption, p = t, x = h. *)
e (Cases_on `h = x`);
e (PROVE_TAC [SUBLIST]); (* solves case: h = x, same head *)
(*
 h::t <= x::h::q
------------------------------------
0. !x p. p <= q ==> p <= x::q
1. h::t <= h::q
2. t <= q
3. t <= h::q
4. h �� x
*)
e (PROVE_TAC [SUBLIST]); (* solves case: ~(h = x), different head, then same head *)
(* remaining case: ~(h = h') *)
(*
 h'::t <= x::h::q
------------------------------------
0. !x p. p <= q ==> p <= x::q
1. h'::t <= h::q
2. h �� h'
*)
e (`h'::t <= q` by PROVE_TAC [SUBLIST]); (* SUBLIST different head *)
e (`h'::t <= x::q` by PROVE_TAC []); (* apply assumption, p = h'::t. *)
(*
 h'::t <= x::h::q
------------------------------------
0. !x p. p <= q ==> p <= x::q
1. h'::t <= h::q
2. h �� h'
3. h'::t <= q
4. h'::t <= x::q
*)
e (Cases_on `h' = x`);
e (`t <= q` by PROVE_TAC [SUBLIST]); (* SUBLIST same head *)
e (`t <= h::q` by PROVE_TAC []); (* apply assumption again, p = t, x = h. *)
e (PROVE_TAC [SUBLIST]); (* solves case h1 = x, SUBLIST same head *)
(* case ~(h' = x) *)
(*
 h'::t <= x::h::q
------------------------------------
0. !x p. p <= q ==> p <= x::q
1. h'::t <= h::q
2. h �� h'
3. h'::t <= q
4. h'::t <= x::q
5. h' �� x
*)
e (PROVE_TAC [SUBLIST]); (* solves case ~(h1 = x), SUBLIST different head *)
(* Yes! *)
drop();
val SUBLIST_EXTRA_HD = store_thm("SUBLIST_EXTRA_HD",
    ``!x p q. (p <= q) ==> (p <= x::q)``,
    Induct_on `q` THENL [
        PROVE_TAC [SUBLIST_NIL, SUBLIST],
        SRW_TAC [][] THEN
        Cases_on `p` THENL [
            PROVE_TAC [SUBLIST],
            Cases_on `h = h'` THENL [
                SRW_TAC [][] THEN
                `t <= q` by PROVE_TAC [SUBLIST] THEN
                `t <= h::q` by PROVE_TAC [] THEN
                Cases_on `h = x` THENL [
                    PROVE_TAC [SUBLIST],
                    PROVE_TAC [SUBLIST]
                ],
                `h'::t <= q` by PROVE_TAC [SUBLIST] THEN
                `h'::t <= x::q` by PROVE_TAC [] THEN
                Cases_on `h' = x` THENL [
                    `t <= q` by PROVE_TAC [SUBLIST] THEN
                    `t <= h::q` by PROVE_TAC [] THEN
                    PROVE_TAC [SUBLIST],
                    PROVE_TAC [SUBLIST]
                ]
            ]
        ]
    ]);
(* slightly shorter proof *)
val SUBLIST_EXTRA_HD = store_thm("SUBLIST_EXTRA_HD",
    ``!x p q. (p <= q) ==> (p <= x::q)``,
    Induct_on `q` THENL [
        PROVE_TAC [SUBLIST_NIL, SUBLIST],
        SRW_TAC [][] THEN
        Cases_on `p` THENL [
            PROVE_TAC [SUBLIST],
            Cases_on `h = h'` THENL [
                SRW_TAC [][] THEN
                `t <= q` by PROVE_TAC [SUBLIST] THEN
                `t <= h::q` by PROVE_TAC [] THEN
                Cases_on `h = x` THEN
                PROVE_TAC [SUBLIST],
                `h'::t <= q` by PROVE_TAC [SUBLIST] THEN
                `h'::t <= x::q` by PROVE_TAC [] THEN
                Cases_on `h' = x` THENL [
                    `t <= q` by PROVE_TAC [SUBLIST] THEN
                    `t <= h::q` by PROVE_TAC [] THEN
                    PROVE_TAC [SUBLIST],
                    PROVE_TAC [SUBLIST]
                ]
            ]
        ]
    ]);
(*PAUSE*)

(* Theorem [Transitive]: for all lists p, q, r; (p <= q) /\ (q <= r) ==> (p <= r) *)
g `!p q r. p <= q /\ q <= r ==> p <= r`;
(* Proof:
   By induction on list r.
   Base case is to prove: p <= q /\ q <= [] ==> p <= []
        q <= [] means q = []    by SUBLIST_NIL.
        p <= [] ==> p <= []     by logic.
   Step case is to prove: p <= q /\ q <= h::t ==> p <= h::t
        with the assumption: !p q. p <= q /\ q <= t ==> p <= t
        Consider cases of list p.
        Case p = [],
        to prove: [] <= q /\ q <= h::t ==> [] <= h::t
             [] <= h::t is T    by SUBLIST definition.
        Case p = h1::t1,
        to prove: h1::t1 <= q /\ q <= h::t ==> h1::t1 <= h::t
            Consider cases of list q.
            Case q = [],
            to prove: h1::t1 <= [] /\ [] <= h::t ==> h1::t1 <= h::t
                 h1::t1 <= [] is F    by SUBLIST definition.
                 hence implication is T, by logic: F can prove anything.
            Case q = h2::t2,
            to prove: h1::t1 <= h2::t2 /\ h2::t2 <= h::t ==> h1::t1 <= h::t .... (a)
            Note that induction assumption is: something-with-t to something-with-h::t.
                so concentrate on the term (h::t), and consider its cases.
            Note also that no matter h2 = h, or ~(h2 = h),
                SUBLIST definition will reduce h::t to t, able to apply induction.
            Case h2 = h,
                h2::t2 <= h::t = t2 <= t          by SUBLIST same head.
                Case h1 = h2,
                    h1::t1 <= h2::t2 = t1 <= t2   by SUBLIST same head.
                    t1 <= t2 /\ t2 <= t
                ==> t1 <= t                       by inductive assumption.
                  = h1::t1 <= h::t   since h1 = h by SUBLIST same head.
                Case ~(h1 = h2),
                    h1::t1 <= h2::t2 = h1::t1 <= t2   by SUBLIST different head.
                    h1::t1 <= t2 /\ t2 <= t
                ==> h1::t1 <= t                   by inductive assumption.
                  = h1::t1 <= h::t   since ~(h1 = h)  by SUBLIST different head.
            Case ~(h2 = h),
                h2::t2 <= h::t = h2::t2 <= t      by SUBLIST different head.
                h1::t1 <= h2::t2 /\ h2::t2 <= t
            ==> h1::t1 <= t                       by inductive assumption
              = h1::t1 <= h::t                    by SUBLIST_EXTRA_HD, the key lemma.
*)
e (Induct_on `r`);
e (PROVE_TAC [SUBLIST_NIL]); (* solves base case *)
(* the step case
 !h p q. p <= q /\ q <= h::r ==> p <= h::r
------------------------------------
!p q. p <= q /\ q <= r ==> p <= r
*)
e (SRW_TAC [][SUBLIST]); (* simplify by definition *)
(*
 p <= h::r
------------------------------------
0. !p q. p <= q /\ q <= r ==> p <= r
1. p <= q
2. q <= h::r
*)
e (Cases_on `p`);
e (PROVE_TAC [SUBLIST]); (* solves case p = [] *)
(*
 h'::t <= h::r
------------------------------------
0. !p q. p <= q /\ q <= r ==> p <= r
1. h'::t <= q
2. q <= h::r
*)
e (Cases_on `q`);
e (PROVE_TAC [SUBLIST]); (* solves case q = [] *)
(*
 h'::t <= h::r
------------------------------------
0. !p q. p <= q /\ q <= r ==> p <= r
1. h'::t <= h''::t'
2. h''::t' <= h::r
*)
(* no matter if h'' = h, or ~(h'' = h), SUBLIST definition will reduce h::r to r, able to apply induction *)
e (Cases_on `h = h''`);
e (SRW_TAC [][]); (* simplifies *)
(*
 h'::t <= h::r
------------------------------------
0. !p q. p <= q /\ q <= r ==> p <= r
1. h'::t <= h::t'
2. h::t' <= h::r
*)
e (`t' <= r` by PROVE_TAC [SUBLIST]); (* SUBLIST same head *)
e (Cases_on `h = h'`);
e (SRW_TAC [][]); (* simplifies *)
(*
 h::t <= h::r
------------------------------------
0. !p q. p <= q /\ q <= r ==> p <= r
1. h::t <= h::t'
2. h::t' <= h::r
3. t' <= r
*)
e (`t <= t'` by PROVE_TAC [SUBLIST]); (* SUBLIST same head *)
e (PROVE_TAC [SUBLIST]); (* solves case h = h' *)
(*
 h'::t <= h::r
------------------------------------
0. !p q. p <= q /\ q <= r ==> p <= r
1. h'::t <= h::t'
2. h::t' <= h::r
3. t' <= r
4. h �� h'
*)
e (`h'::t <= t'` by PROVE_TAC [SUBLIST]); (* SUBLIST different head *)
e (PROVE_TAC [SUBLIST]); (* solves case ~(h = h'), different head *)
(* now case ~(h = h'') *)
(*
 h'::t <= h::r
------------------------------------
0. !p q. p <= q /\ q <= r ==> p <= r
1. h'::t <= h''::t'
2. h''::t' <= h::r
3. h �� h''
*)
e (`h''::t' <= r` by PROVE_TAC [SUBLIST]); (* SUBLIST different head *)
e (PROVE_TAC [SUBLIST_EXTRA_HD]); (* apply the key lemma to solve the case ~(h = h'') *)
(* Yes! *)
drop();
val SUBLIST_TRANS = store_thm("SUBLIST_TRANS",
    ``!p q r. p <= q /\ q <= r ==> p <= r``,
    Induct_on `r` THENL [
        PROVE_TAC [SUBLIST_NIL],
        SRW_TAC [][SUBLIST] THEN
        Cases_on `p` THENL [
            PROVE_TAC [SUBLIST],
            Cases_on `q` THENL [
                PROVE_TAC [SUBLIST],
                Cases_on `h = h''` THENL [
                    SRW_TAC [][] THEN
                    `t' <= r` by PROVE_TAC [SUBLIST] THEN
                    Cases_on `h = h'` THENL [
                        SRW_TAC [][] THEN
                        `t <= t'` by PROVE_TAC [SUBLIST] THEN PROVE_TAC [SUBLIST],
                        `h'::t <= t'` by PROVE_TAC [SUBLIST] THEN PROVE_TAC [SUBLIST]
                    ],
                    `h''::t' <= r` by PROVE_TAC [SUBLIST] THEN
                    PROVE_TAC [SUBLIST_EXTRA_HD]
                ]
            ]
        ]
    ]);
(* slightly shorter proof *)
val SUBLIST_TRANS = store_thm("SUBLIST_TRANS",
    ``!p q r. p <= q /\ q <= r ==> p <= r``,
    Induct_on `r` THENL [
        PROVE_TAC [SUBLIST_NIL],
        SRW_TAC [][SUBLIST] THEN
        Cases_on `p` THENL [
            PROVE_TAC [SUBLIST],
            Cases_on `q` THENL [
                PROVE_TAC [SUBLIST],
                Cases_on `h = h''` THENL [
                    `t' <= r` by PROVE_TAC [SUBLIST] THEN
                    Cases_on `h = h'` THENL [
                        `t <= t'` by PROVE_TAC [SUBLIST] THEN PROVE_TAC [SUBLIST],
                        `h'::t <= t'` by PROVE_TAC [SUBLIST] THEN PROVE_TAC [SUBLIST]
                    ],
                    `h''::t' <= r` by PROVE_TAC [SUBLIST] THEN
                    PROVE_TAC [SUBLIST_EXTRA_HD]
                ]
            ]
        ]
    ]);
(*PAUSE*)

(* -------------------------------------------------------------------- *)

(* Main theorems: *)
SUBLIST_REFL;     (* sublist reflexive *)
SUBLIST_ANTISYM;  (* sublist anti-symmetric *)
SUBLIST_TRANS;    (* sublist transitive *)
(*PAUSE*)
