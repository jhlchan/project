(* Sublist Exercise - version 2 *)

(*
1.  Define the notion of sublist.

     sublist l1 l2 <=> "all the elements of l1 appear in l2, in the order in which they appear in l1"

2.  Try
     (sublist' [] l = T) /\
     (sublist' (h::t) l = ?p s. (l = p ++ [h] ++ s) /\ sublist' t s)

    Then show

     sublist l1 l2 = sublist' l1 l2
*)

(* Strategy of the Proof
   =====================

   Note that sublist is defined by cases:
       (sublist [] l = T) /\
       (sublist (h::t) [] = F) /\
       (sublist (h1::t1) (h2::t2) =
               ((h1 = h2) /\ sublist t1 t2) \/
               (~(h1 = h2) /\ sublist (h1::t1) t2))


   One direction: sublist l1 l2 ==> sublist' l1 l2
   is to show, for the non-trivial case,
       sublist (h::t) (h'::t') = ?x y. (h'::t' = x ++ [h] ++ y) /\ sublist t y

   This can be shown by suitable choices of x, y for cases of (h=h') and ~(h=h').


   The other direction: sublist' l1 l2 ==> sublist l1 l2
   is to show, for the non-trivial case,
   (1) when heads are the same, sublist' (h::t1) (h::t2) = sublist' t1 t2
   (2) when heads are different, sublist' (h1::t1) (h2::t2) = sublist' (h1::t1) t2

   All you know is this:
       sublist' (h1::t1) (h2::t2) = ?x y. (h2::t2 = x ++ [h1] ++ y) /\ sublist' t1 y

   The x and y are now chosen by definition, and the job is to show that,
   no matter what these chosen x, y are, they still give the results (1) and (2).

   Since the best we can get from the right side is: sublist' t1 y for some y,
   In order to arrive at: sublist' t1 t2,
   we need something like: sublist' t1 y ==> sublist' t1 t2, where t2 = tail of (something ++ y).

   This suggests looking at the relationship between sublist' and ++, the appending of lists.

   Indeed, the key lemma is: !z. sublist' x y ==>  sublist' x (z++y).
   By first proving this:    !h. sublist' x y ==>  sublist' x (h::y), the lemma follows by induction.

   A similar key lemma was used in proving the transitivity of sublist itself.
*)

(* make assumptions explicit in proofs *)
show_assums := true;

(* Get sublist theory *)
load "sublistTheory";
open sublistTheory;
(*WAIT*)

(* get definitions and theorems from listTheory *)
val APPEND = listTheory.APPEND;
val APPEND_ASSOC = listTheory.APPEND_ASSOC;
val NOT_CONS_NIL = listTheory.NOT_CONS_NIL;
val CONS_11 = listTheory.CONS_11;
(*WAIT*)

(* ------------------------------------------------------------------------- *)

(* Define sublist1 *)
val sublist1_def = Define`
  (sublist1 [] t = T) /\
  (sublist1 (h::t) l = ?p s. (l = p ++ [h] ++ s) /\ sublist1 t s)
`;
(*WAIT*)

(* Check sublist1 *)
EVAL ``sublist1 [1; 1] [2; 1; 2; 1]``; (* just expands by definition *)
g `sublist1 [1; 1] [2; 1; 2; 1]`;
e (SRW_TAC [][SUBLIST1] THEN
   MAP_EVERY Q.EXISTS_TAC [`[2]`, `[2; 1]`] THEN SRW_TAC [][APPEND] THEN
   MAP_EVERY Q.EXISTS_TAC [`[2]`, `[]`] THEN SRW_TAC [][APPEND]);
drop();

(* get definitions *)
val SUBLIST1 = sublist1_def;
val SUBLIST = sublist_def;
(*WAIT*)

(* Theorem: [head elimination] sublist1 (h::t1) (h::t2) ==> sublist1 t1 t2 *)
(* Proof:
   By cases of sublist1 defintion.
*)
val SUBLIST1_CONS_ELIM = store_thm(
  "SUBLIST1_CONS_ELIM",
  ``!t1 t2. sublist1 (h::t1) (h::t2) ==> sublist1 t1 t2``,
  SRW_TAC [][SUBLIST1] THEN
  Cases_on `p` THEN FULL_SIMP_TAC (srw_ss())[SUBLIST1] THEN
  Cases_on `t1` THEN FULL_SIMP_TAC (srw_ss())[SUBLIST1] THEN
  METIS_TAC [APPEND_ASSOC]);
(*WAIT*)

(* Theorem: [equivalence, if part] sublist1 p q ==> sublist p q *)
(* Proof:
   By induction on list p, and applying sublistTheory.SUBLIST_EX_APPEND.
*)
val SUBLIST1_SUBLIST = store_thm(
  "SUBLIST1_SUBLIST",
  ``!p q. sublist1 p q ==> p <= q``,
  Induct THEN SRW_TAC [][SUBLIST1, SUBLIST, SUBLIST_EX_APPEND]
  THEN METIS_TAC [SUBLIST1]);
(*WAIT*)

(* Theorem: sublist p q ==> sublist1 p q *)
(* Proof:
   By induction on list q, analyzing cases in sublist1 definition.
*)
val SUBLIST_SUBLIST1 = store_thm(
  "SUBLIST_SUBLIST1",
  ``!p q. p <= q ==> sublist1 p q``,
  Induct_on `q` THEN SRW_TAC [][SUBLIST_NIL, SUBLIST1] THEN
  Cases_on `p` THEN FULL_SIMP_TAC (srw_ss()) [SUBLIST1, SUBLIST] THENL [
    METIS_TAC [APPEND],
    `sublist1 (h'::t) q` by METIS_TAC [] THEN
    FULL_SIMP_TAC (srw_ss()) [SUBLIST1] THEN
    METIS_TAC [APPEND, APPEND_ASSOC]
  ]);
(*WAIT*)

(* Theorem: sublist1 = sublist *)
val SUBLIST1_EQ_SUBLIST = store_thm(
  "SUBLIST1_EQ_SUBLIST",
  ``sublist1 = sublist``,
  SRW_TAC [][FUN_EQ_THM, EQ_IMP_THM, SUBLIST1_SUBLIST, SUBLIST_SUBLIST1]);
(*WAIT*)
