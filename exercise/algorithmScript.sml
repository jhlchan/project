(* ------------------------------------------------------------------------- *)
(* Putting algorithms in HOL                                                 *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "algorithm";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory listTheory arithmeticTheory;
(* val _ = load "dividesTheory"; *)
(* val _ = load "gcdTheory"; *)
open dividesTheory gcdTheory;

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: If n > 0, (m MOD n = 0) <=> divides n m *)
(* Proof:
   The if part:
       m = (m DIV n)*n + (m MOD n)   by DIVISION.
   ==> m = (m DIV n)*n               by m MOD n = 0, or ADD_0: m + 0 = m.
   ==> divides n m                   by divides_def.
   The only-if part:
       divides n m
   ==> ?q. n = q*m                   by divides_def.
   ==> (q*m) MOD n = 0               by MOD_EQ_0: !k. (k * n) MOD n = 0
*)
val MOD_0_DIVIDES = store_thm(
  "MOD_0_DIVIDES",
  ``!n m. 0 < n ==> ((m MOD n = 0) <=> divides n m)``,
  SRW_TAC [][divides_def, EQ_IMP_THM] THEN
  METIS_TAC [DIVISION, ADD_0, MOD_EQ_0]);


(* ------------------------------------------------------------------------- *)
(* First attempt at a primality test                                         *)
(* ------------------------------------------------------------------------- *)

(* Algorithm: try each divisor from (n-1) to 2 *)

(* NoProperFactor n k: if this finds a proper factor of n by testing descending values of k, return F. *)
(* This is no good for SRW_TAC *)
val NoProperFactor_def = Define`
	NoProperFactor n k = if (k < 2) then T         (* k too small, no proper factor of n. *)
	                 else if (n MOD k) = 0 then F  (* k divides n, a factor k of n is found. *)
					 else NoProperFactor n (k-1)   (* otherwise, check if (k-1) is a factor of n. *)
`;
(* This is better *)
val NoProperFactor_def = Define`
	(NoProperFactor n 0 = F) /\           (* 0 is not regarded as prime. *)
	(NoProperFactor n (SUC k) =
	    if (k = 1) then T                 (* k down to 1, no proper factor of n found. *)
		else if (n MOD k) = 0 then F      (* k is a factor of n, since k divides n. *)
		else NoProperFactor n k)          (* otherwise, check if (k-1) is a factor of n. *)
`;

(* A simple primality test -- prime iff no proper factor. *)
val PrimeTest_def = Define`
	PrimeTest n = NoProperFactor n n
    (* find a factor of n, effectively testing k=(n-1) downwards for the 2nd NoProperFactor definition. *)
    (* can be improved with initial guess factor = floor(sqrt(n)) *)
`;

(* Testing:
- EVAL ``PrimeTest 0``;
> val it = |- PrimeTest 0 <=> F : thm
- EVAL ``PrimeTest 1``;
> val it = |- PrimeTest 1 <=> F : thm
- EVAL ``PrimeTest 2``;
> val it = |- PrimeTest 2 <=> T : thm
- EVAL ``PrimeTest 3``;
> val it = |- PrimeTest 3 <=> T : thm
- EVAL ``PrimeTest 4``;
> val it = |- PrimeTest 4 <=> F : thm
- EVAL ``PrimeTest 5``;
> val it = |- PrimeTest 5 <=> T : thm
- EVAL ``PrimeTest 6``;
> val it = |- PrimeTest 6 <=> F : thm
- EVAL ``PrimeTest 7``;
> val it = |- PrimeTest 7 <=> T : thm
*)

(* Theorem: if k does not divide n, NoProperFactor n (SUC k) = NoProperFactor n k. *)
(* Proof:
   Since k does not divide n, k <> 1  by ONE_DIVIDES_ALL
   Since (SUC k) <> 0, by NoProperFactor_def in this case:
   NoProperFactor n (SUC k) = if n MOD k = 0 then F else NoProperFactor n k
   Since n MOD k = 0 means the same as k divides n  by MOD_0_DIVIDES,
   the result follows.
*)
val NoProperFactor_for_non_factor = store_thm(
  "NoProperFactor_for_non_factor",
  ``!n k. ~divides k n ==> (NoProperFactor n (SUC k) = NoProperFactor n k)``,
  SRW_TAC [][NoProperFactor_def, EQ_IMP_THM] THEN1
  METIS_TAC [ONE_DIVIDES_ALL] THEN
  `k <> 1` by METIS_TAC [ONE_DIVIDES_ALL] THEN
  SRW_TAC [][] THEN
  Cases_on `k = 0` THEN1
  METIS_TAC [NoProperFactor_def] THEN
  METIS_TAC [MOD_0_DIVIDES, NOT_ZERO_LT_ZERO]);

(* Therefore:
   For an odd prime n,
   (n-1) does not divide n, so NoProperFactor n n = NoProperFactor n (n-1)
   (n-2) does not divide n, so NoProperFactor n (n-1) = NoProperFactor n (n-2)
   ...
   2     does not divide n, so NoProperFactor n 3 = NoProperFactor n 2
   but 1 divides n.
*)

(* Theorem: if NoProperFactor n (SUC k) = F, k divides n  *)
(* to be continued. *)



(* Theorem: if n has a proper factor, NoProperFactor n k = F *)

(* Theorem: prime n ==> PrimeTest n returns T *)
(* Proof:
   Since n is prime, only 1 and n divides n.
   For the NoProperFactor n k, since k starts with (n-1),
   n MOD k is non-zero until k decreases to 1,
   in which case NoProperFactor terminates and returns T.
*)

(* Theorem: n is prime iff PrimeTest n returns T *)

(* Need to prove termination for ascending loop *)
(* The termination condition is put to the Proof Manager for completion.

   ?R. WF R /\ !n k. ~(k * k > n) ==> R (n,SUC k) (n,k)

*)
(* Algorithm 1: try each divisor up to sqrt(n) *)
(*
val ALGO1_loop_def = Define`
	ALGO1_loop n k = if (k*k > n) then T
	                 else ALGO1_loop n (SUC k)
`;

val ALGO1_def = Define`
	ALGO1 n = if (n < 2) then F
	          else ALGO1_loop n 1
`;
*)

(* ------------------------------------------------------------------------- *)
(* Computation of LOG n (base 2)                                             *)
(* ------------------------------------------------------------------------- *)

(*
// Integer log_2 n: the largest k such that 2^k < n.
function log2(n) {
    var k = 0
    while (n >= 2) {
		n = n/2
        k++
    }
    return k // Math.floor(Math.log(n)/Math.log(2))
}
*)
val log2_loop_def = Define`
	(log2_loop 0 k = 0) /\
	(log2_loop 1 k = 0) /\
	(log2_loop n k = 1 + (log2_loop (n DIV 2) (SUC k)))
`;
val log2_def = Define`
	log2 n = log2_loop n n
`;

(*
- EVAL ``log2 2``;
> val it = |- log2 2 = 1 : thm
- EVAL ``log2 8``;
> val it = |- log2 8 = 3 : thm
- EVAL ``log2 1024``;
> val it = |- log2 1024 = 10 : thm
- EVAL ``log2 2048``;
> val it = |- log2 2048 = 11 : thm
- EVAL ``log2 1234``;
> val it = |- log2 1234 = 10 : thm
- EVAL ``log2 123456789``;
> val it = |- log2 123456789 = 26 : thm
*)

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
