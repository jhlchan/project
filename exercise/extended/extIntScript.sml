(* Extended Integers *)

open HolKernel bossLib boolLib Parse

open integerTheory  (* load "integerTheory" *)

val _ = new_theory "extInt"

val _ = Hol_datatype `extInt = EI of int => int`;
  (* intended interpretation is that the first int is the number of
     "infinities" and the second is the normal part. *)

val extInt_add_def = Define`
  extInt_add (EI inf1 i1) (EI inf2 i2) = EI (inf1 + inf2) (i1 + i2)
`;

(* ...  a product group; exercise to prove assoc, comm, identities, inverses *)

val _ = export_theory()
