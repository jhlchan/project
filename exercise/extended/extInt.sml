(* Exercise: Extended Integers *)

load "integerTheory";
(*WAIT*)
open integerTheory;
(*PAUSE*)

(* intended interpretation is that the first int is the number of
   "infinities" and the second is the normal part. *)
Hol_datatype `extInt = EI of int => int`;

val extInt_add_def = Define`
  extInt_add (EI inf1 i1) (EI inf2 i2) = EI (inf1 + inf2) (i1 + i2)
`;

overload_on("+", ``extInt_add``);
(*PAUSE*)

(* Theorem: Extended Integer add is commutative *)
g `!x y:extInt. x + y = y + x`;
(*WAIT*)

e (RW_TAC arith_ss [extInt_add_def]);

e (Cases_on `x`);

e (Cases_on `y`);

e (RW_TAC arith_ss [extInt_add_def]);

INT_ADD_COMM;
e (RW_TAC arith_ss [INT_ADD_COMM]);
e (RW_TAC arith_ss [INT_ADD_COMM]);
(* all goals proved *)

restart();
e (RW_TAC arith_ss [extInt_add_def] THEN Cases_on `x` THEN Cases_on `y`);

restart();
e (RW_TAC arith_ss [extInt_add_def] THEN Cases_on `x` THEN Cases_on `y` THEN
   RW_TAC arith_ss [INT_ADD_COMM] THEN RW_TAC arith_ss [INT_ADD_COMM]);
(* does not prove all the way *)

restart();
e (SRW_TAC [][] THEN Cases_on `x` THEN Cases_on `y` THEN
   SRW_TAC [][extInt_add_def, INT_ADD_COMM]);
(* this proves all the way *)
drop();
(*WAIT*)
val EXTADD_COMM = store_thm(
    "EXTADD_COMM",
    ``!x y:extInt. x + y = y + x``,
    SRW_TAC [][] THEN MAP_EVERY Cases_on [`x`, `y`] THEN
    SRW_TAC [][extInt_add_def, INT_ADD_COMM]);
(*PAUSE*)

(* Theorem: Extended Integer add is associative *)
INT_ADD_ASSOC;
(*WAIT*)
val EXTADD_ASSOC = store_thm(
    "EXTADD_ASSOC",
    ``!x y z:extInt. x + (y + z) = x + y + z``,
    SRW_TAC [][] THEN MAP_EVERY Cases_on [`x`, `y`, `z`] THEN
    SRW_TAC [][extInt_add_def, INT_ADD_ASSOC]);
(*PAUSE*)

(* Theorem: Extended Integer has identity (EI 0 0) *)
g `!x:extInt. x + (EI 0 0) = x`;

e (RW_TAC arith_ss [extInt_add_def]);

e (Cases_on `x`);

e (RW_TAC arith_ss [extInt_add_def]);

INT_ADD_RID;
(*WAIT*)
e (RW_TAC arith_ss [INT_ADD_RID]);
e (RW_TAC arith_ss [INT_ADD_RID]);
(* goal proved *)
(*PAUSE*)

(* do this again *)
restart();
e (SRW_TAC [][] THEN Cases_on `x` THEN
   SRW_TAC [][extInt_add_def, INT_ADD_RID]);
(* this proves all the way *)
drop();
(*WAIT*)
(* notation: (EI 0 0) can be simplified to just EI 0 0 *)
val EXTADD_RID = store_thm(
    "EXTADD_RID",
    ``!x:extInt. x + EI 0 0 = x``,
    SRW_TAC [][] THEN Cases_on `x` THEN
    SRW_TAC [][extInt_add_def, INT_ADD_RID]);
(*PAUSE*)

(* Now use left identity *)
INT_ADD_LID;
(*WAIT*)
val EXTADD_LID = store_thm(
    "EXTADD_LID",
    ``!x:extInt. EI 0 0 + x = x``,
    SRW_TAC [][] THEN Cases_on `x` THEN
    SRW_TAC [][extInt_add_def, INT_ADD_LID]);
(*PAUSE*)

(* Now deal with inverses *)

(* define the negation of extended integers *)
val extInt_neg_def = Define`
  extInt_neg (EI inf1 i1) = EI (-inf1) (-i1)
`;
(* to use unary minus, be paranoid with parentheses *)

(* Theorem: Extended Integer add has inverse, which is the negative *)
g `!x:extInt. x + extInt_neg x = EI 0 0`;
(*WAIT*)
e (RW_TAC arith_ss [extInt_add_def]);

e (Cases_on `x`);

e (RW_TAC arith_ss [extInt_neg_def]);

e (RW_TAC arith_ss [extInt_add_def]);

INT_ADD_RINV;
(*WAIT*)
e (RW_TAC arith_ss [INT_ADD_RINV]);

e (RW_TAC arith_ss [INT_ADD_RINV]);
(* all goals proved *)

drop();

val EXTINT_ADD_RINV = store_thm(
  "EXTINT_ADD_RINV",
  ``!x:extInt. x + extInt_neg x = EI 0 0``,
  SRW_TAC [][] THEN Cases_on `x` THEN
  SRW_TAC [][extInt_neg_def, extInt_add_def, INT_ADD_RINV]);
(*PAUSE*)

(* Now do left inverse *)
INT_ADD_LINV;
(*WAIT*)
val EXTINT_ADD_LINV = store_thm(
  "EXTINT_ADD_LINV",
  ``!x:extInt. extInt_neg x + x = EI 0 0``,
  SRW_TAC [][] THEN Cases_on `x` THEN
  SRW_TAC [][extInt_neg_def, extInt_add_def, INT_ADD_LINV]);
(*PAUSE*)

(* Prove uniqueness of inverse *)
INT_RNEG_UNIQ;
(*WAIT*)
INT_LNEG_UNIQ;
(*WAIT*)

(* Uniqueness of right inverse *)
val EXTINT_RNEG_UNIQ = store_thm(
  "EXTINT_RNEG_UNIQ",
  ``!x y:extInt. (x + y = EI 0 0) <=> (y = extInt_neg x)``,
  SRW_TAC [][] THEN MAP_EVERY Cases_on [`x`, `y`] THEN
  SRW_TAC [][extInt_add_def, extInt_neg_def, INT_RNEG_UNIQ]);

(* Uniqueness of left inverse *)
val EXTINT_LNEG_UNIQ = store_thm(
  "EXTINT_LNEG_UNIQ",
  ``!x y:extInt. (x + y = EI 0 0) <=> (x = extInt_neg y)``,
  SRW_TAC [][] THEN MAP_EVERY Cases_on [`x`, `y`] THEN
  SRW_TAC [][extInt_add_def, extInt_neg_def, INT_LNEG_UNIQ]);
(*PAUSE*)

(* Prove uniqueness of identity *)
INT_ADD_RID_UNIQ;
(*WAIT*)
INT_ADD_LID_UNIQ;
(*WAIT*)

(* Uniqueness of right identity for add *)
val EXTINT_ADD_RID_UNIQ = store_thm(
  "EXTINT_ADD_RID_UNIQ",
  ``!x y:extInt. (x + y = x) <=> (y = EI 0 0)``,
  SRW_TAC [][] THEN MAP_EVERY Cases_on [`x`, `y`] THEN
  SRW_TAC [][extInt_add_def, INT_ADD_RID_UNIQ]);

(* Uniqueness of left identity for add *)
val EXTINT_ADD_LID_UNIQ = store_thm(
  "EXTINT_ADD_LID_UNIQ",
  ``!x y:extInt. (x + y = y) <=> (x = EI 0 0)``,
  SRW_TAC [][] THEN MAP_EVERY Cases_on [`x`, `y`] THEN
  SRW_TAC [][extInt_add_def, INT_ADD_RID_UNIQ]);
(*PAUSE*)

(* All Done! *)

(* Exercise: Re-use theorems for proofs *)

(* Redo: Uniqueness of right inverse *)
g `!x y:extInt. (x + y = EI 0 0) <=> (y = extInt_neg x)`;

(*
To prove: (x + y = EI 0 0) ==> (y = extInt_neg x)
 1.  x + y = EI 0 0                          (assumption)
 2.  extInt_neg x + x = EI 0 0               (left inverse)
 3.  (extInt_neg x + x) + y = y              ((2), left identity)
 4.  extInt_neg x + (x + y) = y              (associativity)
 5.  extInt_neg x = y                        ((1), right identity)

The other direction is easy:
    (y = extInt_neg x) ==> (x + y = EI 0 0)  (right inverse)
*)

(* use the suggested pattern of proof ... *)
e (REPEAT STRIP_TAC THEN EQ_TAC THEN `extInt_neg x + x = EI 0 0` by SRW_TAC [][EXTINT_ADD_LINV]);

(* top = last subgoal: (x + y = EI 0 0) ==> (y = extInt_neg x) *)
(* But then, how to "add y" to both sides?  Try again from start: *)
restart();

(* get the subgoals *)
e (REPEAT STRIP_TAC);

(* split equivalent into two implications *)
e (EQ_TAC);

(* use a lemma to "add y": (extInt_neg x + x) + y = y *)
val EXTADD_lemma_addy = store_thm(
    "EXTADD_lemma_addy",
    ``(extInt_neg x + x) + y = y``,
    RW_TAC arith_ss [EXTINT_ADD_LINV, EXTADD_LID]);

(* apply the lemma, use associativity, and right identity *)
e (RW_TAC arith_ss [EXTADD_lemma_addy, EXTADD_ASSOC, EXTADD_RID]);

(* doesn't work, back up: *)
b();

(* use first-order logic *)
e (METIS_TAC [EXTADD_lemma_addy, EXTADD_ASSOC, EXTADD_RID]);

(* Works! Next subgoal: (y = extInt_neg x) ==> (x + y = EI 0 0)  is done by right identity *)
e (RW_TAC arith_ss [EXTINT_ADD_RINV]);
(* Done *)

(* Package up the proof *)
dropn 1;
val EXTINT_RNEG_UNIQ = store_thm(
    "EXTINT_RNEG_UNIQ",
    ``!x y:extInt. (x + y = EI 0 0) <=> (y = extInt_neg x)``,
    REPEAT STRIP_TAC THEN
    EQ_TAC THENL [
        METIS_TAC [EXTADD_lemma_addy, EXTADD_ASSOC, EXTADD_RID],
        RW_TAC arith_ss [EXTINT_ADD_RINV]
    ]);
(*PAUSE*)

(* Redo: Uniqueness of left inverse *)
g `!x y:extInt. (x + y = EI 0 0) <=> (x = extInt_neg y)`;

(*
To prove: (x + y = EI 0 0) ==> (x = extInt_neg y)
 1.  x + y = EI 0 0                          (assumption)
 2.  (x + y) + extInt_neg y = extInt_neg y   ((1), left identity)
 3.  x + (y + extInt_neg y) = extInt_neg y   (associativity)
 4.  x + EI 0 0 = extInt_neg y               (right inverse)
 5.  x = extInt_neg y                        (right identity)

The other direction is easy:
    (x = extInt_neg y) ==> (x + y = EI 0 0)  (left inverse)
*)

(* remove for-alls and split implicatins *)
e (REPEAT STRIP_TAC THEN EQ_TAC);

(* first subgoal: (x + y = EI 0 0) ==> (x = extInt_neg y) *)
e (METIS_TAC [EXTADD_LID, EXTADD_ASSOC, EXTINT_ADD_RINV, EXTADD_RID]);

(* remaining subgoal: (x = extInt_neg y) ==> (x + y = EI 0 0) *)
e (RW_TAC arith_ss [EXTINT_ADD_LINV]);
(* Done *)

(* Package up the proof *)
dropn 1 ;
val EXTINT_LNEG_UNIQ = store_thm(
    "EXTINT_LNEG_UNIQ",
    ``!x y:extInt. (x + y = EI 0 0) <=> (x = extInt_neg y)``,
    REPEAT STRIP_TAC THEN
    EQ_TAC THENL [
        METIS_TAC [EXTADD_LID, EXTADD_ASSOC, EXTINT_ADD_RINV, EXTADD_RID],
        RW_TAC arith_ss [EXTINT_ADD_LINV]
    ]);
(*PAUSE*)

(* Uniqueness of right-inverse without lemma -- just ask METIS_TAC to do more work! *)
val EXTINT_RNEG_UNIQ = store_thm(
    "EXTINT_RNEG_UNIQ",
    ``!x y:extInt. (x + y = EI 0 0) <=> (y = extInt_neg x)``,
    REPEAT STRIP_TAC THEN
    EQ_TAC THENL [
        METIS_TAC [EXTINT_ADD_LINV, EXTADD_LID, EXTADD_ASSOC, EXTADD_RID],
        RW_TAC arith_ss [EXTINT_ADD_RINV]
    ]);
(*PAUSE*)

(* Comments:

. Using the lemma, it seems that the proof proceeds along the lines of the word-proof.
  Without the lemma, how can we be sure that reasoning proceedss along the word-proof?

. All I did was to throw all assumptions of word-proof to METIS_TAC.
  I presume METIS_TAC is using first-order logic (i.e. resolution) and eventually says "OK!".
  But what is the reasoning path (or resolution path?) of METIS_TAC?
  Does the "metis: r[+0+7]+0+0+0+0+1+0+1+1#" show the path?

. The first proof is still not in the suggested pattern:

    REPEAT STRIP_TAC THEN EQ_TAC THEN
    `extInt_neg x + x = EI 0 0` by SRW_TAC [][EXTINT_ADD_LINV] THEN
    ...;

  Do you have a different tactic (not using METIS_TAC) in mind?
  I try replacing METIS_TAC by other rewrite tactics, without success.

*)
