(* Exercise: Extended Reals (take two ) *)

load "realTheory";
(*WAIT*)
open realTheory;
(*PAUSE*)

(* Define Extended Real *)
(* Try to introduce another element: Indefinite *)
Hol_datatype `ExtReal = PosInf | NegInf | Indefinite | Normal of real`;

val ExtReal_add_def = Define`
   (ExtReal_add PosInf NegInf = Indefinite) /\
   (ExtReal_add PosInf Indefinite = Indefinite) /\
   (ExtReal_add PosInf _ = PosInf) /\
   (ExtReal_add NegInf PosInf = Indefinite) /\
   (ExtReal_add NegInf Indefinite = Indefinite) /\
   (ExtReal_add NegInf _ = NegInf) /\
   (ExtReal_add Indefinite _ = Indefinite) /\
   (ExtReal_add (Normal r1) (Normal r2) = Normal (r1 + r2)) /\
   (ExtReal_add (Normal _) x = x)
 `;

overload_on("+", ``ExtReal_add``);

val _ = export_rewrites ["ExtReal_add_def"]

val EXTREAL_ADD_ASSOC = store_thm(
  "EXTREAL_ADD_ASSOC",
  ``!x y z:ExtReal. x + (y + z) = (x + y) + z``,
  SRW_TAC [][] THEN MAP_EVERY Cases_on [`x`, `y`, `z`] THEN
  SRW_TAC [][REAL_ADD_ASSOC]);

val EXTREAL_ADD_COMM = store_thm(
  "EXTREAL_ADD_COMM",
  ``!x y:ExtReal. x + y= y + x``,
  Cases_on `x` THEN Cases_on `y` THEN SRW_TAC [][REAL_ADD_COMM]);



(* Theorem: Extended Real add is associative *)
g `!x y z:ExtReal. x + (y + z) = (x + y) + z`;
(*PAUSE*)

e (RW_TAC arith_ss [ExtReal_add_def]);

e (Cases_on `x`);

e (Cases_on `y`);

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
(*PAUSE*)

(* another round *)
e (Cases_on `y`);

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
(*PAUSE*)

(* another round *)
e (Cases_on `y`);

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
(*PAUSE*)

(* last round *)
e (Cases_on `y`);

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

e (Cases_on `z`);
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)
e (RW_TAC arith_ss [ExtReal_add_def]); (* apply defintion for each subgoal *)

(* still 1 subgoal remains:
   r + (r' + r'') = r + r' + r'' *)
(*PAUSE*)

(* get help from realTheory *)
REAL_ADD_ASSOC;

e (RW_TAC arith_ss [REAL_ADD_ASSOC]); (* apply realTheory *)

(* initial goal proved *)
(*PAUSE*)

(* Stitch the proof together *)
restart();

e (RW_TAC arith_ss [ExtReal_add_def] THEN
   Cases_on `x` THENL
     [Cases_on `y` THENL
        [Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ]
      ,
      Cases_on `y` THENL
        [Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ]
      ,
      Cases_on `y` THENL
        [Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ]
      ,
      Cases_on `y` THENL
        [Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def] THEN
            RW_TAC arith_ss [REAL_ADD_ASSOC] ]
         ]
      ]
  );

val EADD_ASSOC = top_thm() before drop();
(*PAUSE*)

(* The whole proof again *)
val EADD_ASSOC = store_thm(
   "EADD_ASSOC",
   ``!x y z:ExtReal. x + (y + z) = (x + y) + z``,
   RW_TAC arith_ss [ExtReal_add_def] THEN
   Cases_on `x` THENL
     [Cases_on `y` THENL
        [Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ]
      ,
      Cases_on `y` THENL
        [Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ]
      ,
      Cases_on `y` THENL
        [Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ]
      ,
      Cases_on `y` THENL
        [Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def]]
         ,
         Cases_on `z` THENL
           [RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def],
            RW_TAC arith_ss [ExtReal_add_def] THEN
            RW_TAC arith_ss [REAL_ADD_ASSOC] ]
         ]
      ]);
(*PAUSE*)

(* Check if EADD_COMM still OK *)
val EADD_COMM = store_thm(
   "EADD_COMM",
   ``!x y:ExtReal. x + y = y + x``,
   RW_TAC arith_ss [ExtReal_add_def] THEN
   Cases_on `x` THENL
     [Cases_on `y` THENL
         [RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def]]
      ,
      Cases_on `y` THENL
         [RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def]]
      ,
      Cases_on `y` THENL
         [RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def]]
      ,
      Cases_on `y` THENL
         [RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def],
          RW_TAC arith_ss [ExtReal_add_def] THEN
          RW_TAC arith_ss [REAL_ADD_SYM]]
     ]);
(* Still proved OK *)
