(* Extended Reals *)

open HolKernel bossLib boolLib Parse realTheory

val _ = new_theory "extreal"

(* Define the type of "extended reals",
   where you have the normal real numbers as well as positive and negative infinity. *)
val _ = Hol_datatype `extreal = PosInf | NegInf | Normal of real`

val extreal_add_def = Define`
    (extreal_add PosInf NegInf = Normal 0) /\
    (extreal_add PosInf x = PosInf) /\
    (extreal_add (Normal r1) (Normal r2) = Normal (r1 + r2)) /\
    (extreal_add (Normal _) x = x) /\
    (extreal_add NegInf PosInf = Normal 0) /\
    (extreal_add NegInf _ = NegInf)
`
val _ = overload_on("+", ``extreal_add``);

(*
val EADD_COMM = store_thm(
    "EADD_COMM",
    ``!x y:extreal. x + y = y + x``,
    <your proof goes here>);
*)

(*
val EADD_ASSOC = store_thm(
    "EADD_ASSOC",
    ``!x y z:extreal. x + (y + z) = (x + y) + z``,
    <your proof goes here>);
*)

val _ = export_theory()
