
(*

HOL scripts on Group Theory (only in examples):

examples/elliptic/groupScript.sml
examples/elliptic/groupTools.sml
examples/miller/groups/abelian_groupScript.sml
examples/miller/groups/finite_groupContext.sml
examples/miller/groups/finite_groupScript.sml
examples/miller/groups/groupContext.sml
examples/miller/groups/groupScript.sml
examples/miller/groups/mult_groupContext.sml
examples/miller/groups/mult_groupScript.sml

HOL scripts on Ring Theory (in source):
src/integer/integerRingLib.sml
src/integer/integerRingScript.sml
src/integer/integerRingTheory.sml
src/rational/ratRingLib.sml
src/rational/ratRingScript.sml
src/rational/ratRingTheory.sml
src/ring/src/numRingLib.sml
src/ring/src/numRingScript.sml
src/ring/src/numRingTheory.sml
src/ring/src/ringLib.sml
src/ring/src/ringNormScript.sml
src/ring/src/ringNormTheory.sml
src/ring/src/ringScript.sml
src/ring/src/ringTheory.sml
src/ring/src/semi_ringScript.sml
src/ring/src/semi_ringTheory.sml

HOL scripts on Field Theory (only in examples):
examples/elliptic/fieldScript.sml
examples/elliptic/fieldTools.sml

*)


(* from examples/miller/groups/groupScript.sml *)
(*
val GROUP = Define`
    group (gp : 'a -> bool, star) =
        star IN (gp -> gp -> gp) /\
        (!x y z :: gp. star (star x y) z = star x (star y z)) /\
        ?e :: gp. !x :: gp. ?ix :: gp. (star e x = x) /\ (star ix x = e)`;
*)
(* not OK *)

(* from src/ring/src/ringScript.sml *)
(*
val _ = Hol_datatype `ring = <| R0 : 'a;
                                R1 : 'a;
                                RP : 'a -> 'a -> 'a;
                                RM : 'a -> 'a -> 'a;
                                RN : 'a -> 'a
                             |>`;
*)
(* OK *)

(* from examples/elliptic/groupScript.sml *)
(*
val () = Hol_datatype
  `group = <| carrier : 'a -> bool;
              id : 'a;
              inv : 'a -> 'a;
              mult : 'a -> 'a -> 'a |>`;
*)
(* OK *)

(* from examples/elliptic/fieldScript.sml *)
(*
val () = Hol_datatype
  `field = <| carrier : 'a -> bool;
              sum : 'a group;
              prod : 'a group |>`;
*)
(* OK with group above *)

(* -------------------------------------------------------------------- *)

(* Follow examples/elliptic/groupScript.sml *)

(* for res_quanTools.resq_ss, also subtypeTools.sml *)
load "res_quanTools";
(* open res_quanTools; *)
(*WAIT*)

(* The proper way to get: examples/elliptic/subtypeTools.sml *)
use "examples/elliptic/subtypeTools.sig";
(*WAIT*)
use "examples/elliptic/subtypeTools.sml";
(*WAIT*)
val context = subtypeTools.empty2;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;
alg_ss;
(*PAUSE*)

(* val () = loadPath := ["info\\Hol\\examples\\elliptic"] @ !loadPath; *)
(* Still cannot load Algebra.sml and primalityTools.sml *)
(*
val () = app load
  ["bossLib", "metisLib", "res_quanTools",
   "optionTheory", "listTheory",
   "arithmeticTheory", "dividesTheory", "gcdTheory",
   "pred_setTheory", "pred_setSyntax"];
*)
(* val () = quietdec := true; *)
(*
open HolKernel Parse boolLib bossLib metisLib res_quanTools;
open optionTheory listTheory arithmeticTheory dividesTheory gcdTheory;
open pred_setTheory;
*)
(* val () = quietdec := false; *)

(* Set up group type *)
Hol_datatype
    `group = <| carrier : 'a -> bool;
                id : 'a;
                inv : 'a -> 'a;
                mult : 'a -> 'a -> 'a |>`;

(* Group Definition *)
val GROUP = Define
   `Group =
   {(g : 'a group) |
     g.id IN g.carrier /\
     (!x y :: (g.carrier). g.mult x y IN g.carrier) /\
     (!x :: (g.carrier). g.inv x IN g.carrier) /\
     (!x :: (g.carrier). g.mult g.id x = x) /\
     (!x :: (g.carrier). g.mult (g.inv x) x = g.id) /\
     (!x y z :: (g.carrier). g.mult (g.mult x y) z = g.mult x (g.mult y z)) }`;

(* Define exponentiation operation for group elements *)
val GROUP_EXP = Define
  `(group_exp G g 0 = G.id) /\
   (group_exp G g (SUC n) = G.mult g (group_exp G g n))`;

(* Abelian Group *)
val ABELIAN_GROUP = Define
   `AbelianGroup =
    {(g : 'a group) |
     g IN Group /\
     !x y :: (g.carrier). g.mult x y = g.mult y x }`;

(* Finite Group *)
val FINITE_GROUP = Define
   `FiniteGroup =
   {(g : 'a group) | g IN Group /\ FINITE g.carrier }`;

(* Finite Abelian Group *)
val FINITE_ABELIAN_GROUP = Define
  `FiniteAbelianGroup =
   { (g : 'a group) | g IN FiniteGroup /\ g IN AbelianGroup }`;

(* How important is this? *)
val group_accessors = fetch "-" "group_accessors";

(* Follow examples/elliptic/fieldScript.sml *)

(* Set up field type *)
Hol_datatype
    `field = <| carrier : 'a -> bool;
                sum : 'a group;
                prod : 'a group |>`;

(* How important is this? *)
val field_accessors = fetch "-" "field_accessors";

(* Some field terminology *)
val FIELD_0 = Define `field_zero (f : 'a field) = f.sum.id`;
val FIELD_1 = Define `field_one (f : 'a field) = f.prod.id`;
val FIELD_NEG = Define `field_neg (f : 'a field) = f.sum.inv`;
val FIELD_INV = Define `field_inv (f : 'a field) = f.prod.inv`;
val FIELD_ADD = Define `field_add (f : 'a field) = f.sum.mult`;
val FIELD_MULT = Define `field_mult (f : 'a field) = f.prod.mult`;
val FIELD_NZ = Define `field_nonzero f = f.carrier DIFF {field_zero f}`;

(* Field Definition *)
val FIELD = Define
   `Field =
   {(f : 'a field) |
     f.sum IN AbelianGroup /\
     f.prod IN AbelianGroup /\
     (f.sum.carrier = f.carrier) /\
     (f.prod.carrier = field_nonzero f) /\
     (!x :: (f.carrier). field_mult f (field_zero f) x = field_zero f) /\
     (!x y z :: (f.carrier).
        field_mult f x (field_add f y z) =
        field_add f (field_mult f x y) (field_mult f x z)) }`;

(* Finite Field *)
val FINITE_FIELD = Define
   `FiniteField =
   {(f : 'a field) | f IN Field /\ FINITE f.carrier }`;


(* Some Group Theorems - also add context to alg_ss *)

(* from set theory *)
val GSPECIFICATION = pred_setTheory.GSPECIFICATION;
(* val GSPECIFICATION = |- !f v. v in GSPEC f = ?x. (v,T) = f x : thm *)

(* from res_quanTools *)
val resq_ss = res_quanTools.resq_ss;

(* Theorem: An abelian group is a group. *)
val AbelianGroup_Group = store_thm
  ("AbelianGroup_Group",
   ``!g. g IN AbelianGroup ==> g IN Group``,
   RW_TAC std_ss [ABELIAN_GROUP, GSPECIFICATION]);

val context = subtypeTools.add_judgement2 AbelianGroup_Group context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: A finite group is a group. *)
val FiniteGroup_Group = store_thm
  ("FiniteGroup_Group",
   ``!g. g IN FiniteGroup ==> g IN Group``,
   RW_TAC std_ss [FINITE_GROUP, GSPECIFICATION]);

val context = subtypeTools.add_judgement2 FiniteGroup_Group context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Group identity is an element, *)
val group_id_carrier = store_thm
  ("group_id_carrier",
   ``!g :: Group. g.id IN g.carrier``,
   RW_TAC resq_ss [GROUP, GSPECIFICATION]);

val context = subtypeTools.add_reduction2 group_id_carrier context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Group inverse is an element, *)
val group_inv_carrier = store_thm
  ("group_inv_carrier",
   ``!g :: Group. !x :: (g.carrier). g.inv x IN g.carrier``,
   RW_TAC resq_ss [GROUP, GSPECIFICATION]);

val context = subtypeTools.add_reduction2 group_inv_carrier context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Group product is an element. *)
val group_mult_carrier = store_thm
  ("group_mult_carrier",
   ``!g :: Group. !x y :: (g.carrier). g.mult x y IN g.carrier``,
   RW_TAC resq_ss [GROUP, GSPECIFICATION]);

val context = subtypeTools.add_reduction2 group_mult_carrier context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Group left identity: e x = x *)
val group_lid = store_thm
  ("group_lid",
   ``!g :: Group. !x :: (g.carrier). g.mult g.id x = x``,
   RW_TAC resq_ss [GROUP, GSPECIFICATION]);

val context = subtypeTools.add_rewrite2 group_lid context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Group left inverse: x' x = e *)
val group_linv = store_thm
  ("group_linv",
   ``!g :: Group. !x :: (g.carrier). g.mult (g.inv x) x = g.id``,
   RW_TAC resq_ss [GROUP, GSPECIFICATION]);

val context = subtypeTools.add_rewrite2 group_linv context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Group associative: (x y) z = x (y z) *)
val group_assoc = store_thm
  ("group_assoc",
   ``!g :: Group. !x y z :: (g.carrier).
       g.mult (g.mult x y) z = g.mult x (g.mult y z)``,
   RW_TAC resq_ss [GROUP, GSPECIFICATION]);

val context = subtypeTools.add_rewrite2'' group_assoc context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Group commutative: x y = y x  for Abelian Groups *)
val group_comm = store_thm
  ("group_comm",
   ``!g :: AbelianGroup. !x y :: (g.carrier). g.mult x y = g.mult y x``,
   RW_TAC resq_ss [ABELIAN_GROUP, GSPECIFICATION]);

(* Theorem: Abelian group commutative-associative: x (y z) = y (x z) *)
val group_comm' = store_thm
  ("group_comm'",
   ``!g :: AbelianGroup. !x y z :: (g.carrier).
        g.mult x (g.mult y z) = g.mult y (g.mult x z)``,
   RW_TAC resq_ss [] THEN
   RW_TAC alg_ss [GSYM group_assoc] THEN
   METIS_TAC [group_comm]);
(*
Exception raised at folTools.FOL_FIND:
no solution found.

Note: original script updates alg_ss after every theorem.
OK -- works after all the above context changes with alg_ss.
However, comm theorems are not added to alg_ss.
*)

(* Next theorem needs Helper proof tools *)
val resq_SS = res_quanTools.resq_SS;

val norm_rule =
    SIMP_RULE (simpLib.++ (pureSimps.pure_ss, resq_SS))
      [GSYM LEFT_FORALL_IMP_THM, GSYM RIGHT_FORALL_IMP_THM,
       AND_IMP_INTRO, GSYM CONJ_ASSOC];

fun match_tac th =
    let
      val th = norm_rule th
      val (_,tm) = strip_forall (concl th)
    in
      (if is_imp tm then MATCH_MP_TAC else MATCH_ACCEPT_TAC) th
    end;

(* Theorem: Group right inverse: x x' = e *)
val group_rinv = store_thm
  ("group_rinv",
   ``!g :: Group. !x :: (g.carrier). g.mult x (g.inv x) = g.id``,
   RW_TAC resq_ss [] THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult g.id (g.mult x (g.inv x))` THEN
   CONJ_TAC THEN1 (
       match_tac (GSYM group_lid) THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.mult (g.inv (g.inv x)) (g.inv x)) (g.mult x (g.inv x))` THEN
   CONJ_TAC THEN1 (
       REPEAT (AP_TERM_TAC ORELSE AP_THM_TAC) THEN
       match_tac (GSYM group_linv) THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.inv (g.inv x)) (g.mult (g.inv x) (g.mult x (g.inv x)))` THEN
   CONJ_TAC THEN1 (
       match_tac group_assoc THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.inv (g.inv x)) (g.mult (g.mult (g.inv x) x) (g.inv x))` THEN
   CONJ_TAC THEN1 (
       AP_TERM_TAC THEN
       match_tac (GSYM group_assoc) THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.inv (g.inv x)) (g.mult g.id (g.inv x))` THEN
   CONJ_TAC THEN1 (
       REPEAT (AP_TERM_TAC ORELSE AP_THM_TAC) THEN
       match_tac group_linv THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.inv (g.inv x)) (g.inv x)` THEN
   CONJ_TAC THEN1 (
       REPEAT (AP_TERM_TAC ORELSE AP_THM_TAC) THEN
       match_tac group_lid THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   match_tac group_linv THEN
   METIS_TAC [group_inv_carrier, group_mult_carrier]);
(* Yes! *)

val context = subtypeTools.add_rewrite2 group_rinv context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Group right identity: x e = x *)
val group_rid = store_thm
  ("group_rid",
   ``!g :: Group. !x :: (g.carrier). g.mult x g.id = x``,
   RW_TAC resq_ss [] THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult x (g.mult (g.inv x) x)` THEN
   CONJ_TAC THEN1 (
       REPEAT (AP_TERM_TAC ORELSE AP_THM_TAC) THEN
       match_tac (GSYM group_linv) THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.mult x (g.inv x)) x` THEN
   CONJ_TAC THEN1 (
       match_tac (GSYM group_assoc) THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult g.id x` THEN
   CONJ_TAC THEN1 (
       REPEAT (AP_TERM_TAC ORELSE AP_THM_TAC) THEN
       match_tac group_rinv THEN
       METIS_TAC [group_inv_carrier, group_mult_carrier]
       ) THEN
   match_tac group_lid THEN
   METIS_TAC [group_inv_carrier, group_mult_carrier]);
(* Yes! *)

val context = subtypeTools.add_rewrite2 group_rid context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Need a tactic *)
val Suff = Q_TAC SUFF_TAC;

(* Theorem: Left cancellation: x y = x z <=> y = z *)
val group_lcancel = store_thm
  ("group_lcancel",
   ``!g :: Group. !x y z :: (g.carrier). (g.mult x y = g.mult x z) = (y = z)``,
   RW_TAC resq_ss [] THEN
   REVERSE EQ_TAC THEN1 RW_TAC std_ss [] THEN
   RW_TAC std_ss [] THEN
   Suff `g.mult g.id y = g.mult g.id z` THEN1 METIS_TAC [group_lid] THEN
   Suff `g.mult (g.mult (g.inv x) x) y = g.mult (g.mult (g.inv x) x) z` THEN1 METIS_TAC [group_linv] THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.inv x) (g.mult x y)` THEN
   CONJ_TAC THEN1 (
       match_tac group_assoc THEN
       METIS_TAC [group_inv_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.inv x) (g.mult x z)` THEN
   REVERSE CONJ_TAC THEN1 (match_tac (GSYM group_assoc) THEN
   METIS_TAC [group_inv_carrier]) THEN
   RW_TAC std_ss []);
(* Yes! with Suff defined *)

val context = subtypeTools.add_rewrite2' group_lcancel context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Left cancellation (implication form): x y = x z ==> y = z *)
val group_lcancel_imp = store_thm
  ("group_lcancel_imp",
   ``!g :: Group. !x y z :: (g.carrier).
       (g.mult x y = g.mult x z) ==> (y = z)``,
   METIS_TAC [group_lcancel]);

(* Theorem: Left cancellation gives identity: x y = x <=> y = e *)
val group_lcancel_id = store_thm
  ("group_lcancel_id",
   ``!g :: Group. !x y :: (g.carrier). (g.mult x y = x) = (y = g.id)``,
   RW_TAC resq_ss [] THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult x y = g.mult x g.id` THEN
   CONJ_TAC THEN1 RW_TAC std_ss [group_rid] THEN
   match_tac group_lcancel THEN
   RW_TAC std_ss [group_id_carrier]);

val context = subtypeTools.add_rewrite2' group_lcancel_id context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Left cancellation gives identity (implication): x y = x ==> y = e *)
val group_lcancel_id_imp = store_thm
  ("group_lcancel_id_imp",
   ``!g :: Group. !x y :: (g.carrier). (g.mult x y = x) ==> (y = g.id)``,
   METIS_TAC [group_lcancel_id]);

(* Theorem: Left cancellation gives identity (implication): y = e ==> x y = x *)
val group_lcancel_id_imp' = store_thm
  ("group_lcancel_id_imp'",
   ``!g :: Group. !x y :: (g.carrier). (y = g.id) ==> (g.mult x y = x)``,
   METIS_TAC [group_lcancel_id]);

(* Theorem: Right cancellation: y x = z x <=> y = z *)
val group_rcancel = store_thm
  ("group_rcancel",
   ``!g :: Group. !x y z :: (g.carrier). (g.mult y x = g.mult z x) = (y = z)``,
   RW_TAC resq_ss [] THEN
   REVERSE EQ_TAC THEN1 RW_TAC std_ss [] THEN
   RW_TAC std_ss [] THEN
   Suff `g.mult y g.id = g.mult z g.id` THEN1 METIS_TAC [group_rid] THEN
   Suff `g.mult y (g.mult x (g.inv x)) = g.mult z (g.mult x (g.inv x))` THEN1 METIS_TAC [group_rinv] THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.mult y x) (g.inv x)` THEN
   CONJ_TAC THEN1 (
       match_tac (GSYM group_assoc) THEN
       METIS_TAC [group_inv_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult (g.mult z x) (g.inv x)` THEN
   REVERSE CONJ_TAC THEN1 (match_tac group_assoc THEN
   METIS_TAC [group_inv_carrier]) THEN
   RW_TAC std_ss []);

val context = subtypeTools.add_rewrite2' group_rcancel context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Right cancellation (implication form): y x = z x ==> y = z*)
val group_rcancel_imp = store_thm
  ("group_rcancel_imp",
   ``!g :: Group. !x y z :: (g.carrier).
       (g.mult y x = g.mult z x) ==> (y = z)``,
   METIS_TAC [group_rcancel]);

(* Theorem: Right cancellation gives identity: y x = x <=> y = e *)
val group_rcancel_id = store_thm
  ("group_rcancel_id",
   ``!g :: Group. !x y :: (g.carrier). (g.mult y x = x) = (y = g.id)``,
   RW_TAC resq_ss [] THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult y x = g.mult g.id x` THEN
   CONJ_TAC THEN1 RW_TAC std_ss [group_lid] THEN
   match_tac group_rcancel THEN
   RW_TAC std_ss [group_id_carrier]);

val context = subtypeTools.add_rewrite2' group_rcancel_id context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Right cancellation gives identity (implication): y x = x ==> y = e *)
val group_rcancel_id_imp = store_thm
  ("group_rcancel_id_imp",
   ``!g :: Group. !x y :: (g.carrier). (g.mult y x = x) ==> (y = g.id)``,
   METIS_TAC [group_rcancel_id]);

(* Theorem: Right cancellation gives identity (implication): y = e ==> y x = x *)
val group_rcancel_id_imp' = store_thm
  ("group_rcancel_id_imp'",
   ``!g :: Group. !x y :: (g.carrier). (y = g.id) ==> (g.mult y x = x)``,
   METIS_TAC [group_rcancel_id]);

(* Theorem: Inverses equal implies elements are equals: x' = y' ==> x = y *)
val group_inv_cancel_imp = store_thm
  ("group_inv_cancel_imp",
   ``!g :: Group. !x y :: (g.carrier). (g.inv x = g.inv y) ==> (x = y)``,
   RW_TAC resq_ss [] THEN
   match_tac group_lcancel_imp THEN
   Q.EXISTS_TAC `g` THEN
   Q.EXISTS_TAC `g.inv x` THEN
   RW_TAC std_ss [group_inv_carrier] THEN
   METIS_TAC [group_linv]);

(* Theorem: Inverses equal means elements are equal: x' = y' <=> x = y *)
val group_inv_cancel = store_thm
  ("group_inv_cancel",
   ``!g :: Group. !x y :: (g.carrier). (g.inv x = g.inv y) = (x = y)``,
   METIS_TAC [group_inv_cancel_imp]);

val context = subtypeTools.add_rewrite2' group_inv_cancel context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Inverse of inverse is itself: x'' = x *)
val group_inv_inv = store_thm
  ("group_inv_inv",
   ``!g :: Group. !x :: (g.carrier). g.inv (g.inv x) = x``,
   RW_TAC resq_ss [] THEN
   match_tac group_lcancel_imp THEN
   Q.EXISTS_TAC `g` THEN
   Q.EXISTS_TAC `g.inv x` THEN
   RW_TAC std_ss [group_inv_carrier] THEN
   METIS_TAC [group_inv_carrier, group_linv, group_rinv]);

val context = subtypeTools.add_rewrite2 group_inv_inv context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Inverse elements are paired (implication): x' = y ==> x = y' *)
val group_inv_eq_swap_imp = store_thm
  ("group_inv_eq_swap_imp",
   ``!g :: Group. !x y :: (g.carrier). (g.inv x = y) ==> (x = g.inv y)``,
   METIS_TAC [group_inv_inv]);

(* Theorem: Inverse elements are paired: x' = y <=> x = y' *)
val group_inv_eq_swap = store_thm
  ("group_inv_eq_swap",
   ``!g :: Group. !x y :: (g.carrier). (g.inv x = y) = (x = g.inv y)``,
   METIS_TAC [group_inv_eq_swap_imp]);

(* Theorem: Inverse elements are paired (implication): x = y' ==> x' = y *)
val group_inv_eq_swap_imp' = store_thm
  ("group_inv_eq_swap_imp'",
   ``!g :: Group. !x y :: (g.carrier). (x = g.inv y) ==> (g.inv x = y)``,
   METIS_TAC [group_inv_eq_swap]);

(* Theorem: Inverse of identity is identity itself: e' = e *)
val group_inv_id = store_thm
  ("group_inv_id",
   ``!g :: Group. g.inv g.id = g.id``,
   RW_TAC resq_ss [] THEN
   match_tac group_lcancel_imp THEN
   Q.EXISTS_TAC `g` THEN
   Q.EXISTS_TAC `g.id` THEN
   RW_TAC std_ss [group_inv_carrier, group_id_carrier, group_rinv] THEN
   RW_TAC std_ss [group_lid, group_id_carrier]);

val context = subtypeTools.add_rewrite2 group_inv_id context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Theorem: Inverse pairs up to identity (implication): x' = y ==> x y = e *)
val group_inv_eq_imp = store_thm
  ("group_inv_eq_imp",
   ``!g :: Group. !x y :: (g.carrier). (g.inv x = y) ==> (g.mult x y = g.id)``,
   RW_TAC resq_ss [group_rinv]);

(* Theorem: Inverse pairs up to identity (implication): x y = e ==> x' = y *)
val group_inv_eq_imp' = store_thm
  ("group_inv_eq_imp'",
   ``!g :: Group. !x y :: (g.carrier). (g.mult x y = g.id) ==> (g.inv x = y)``,
   RW_TAC resq_ss [] THEN
   match_tac group_lcancel_imp THEN
   Q.EXISTS_TAC `g` THEN
   Q.EXISTS_TAC `x` THEN
   RW_TAC std_ss [group_inv_carrier, group_rinv]);

(* Theorem: Inverse pairs up to identity: x' = y <=> x y = e *)
val group_inv_eq = store_thm
  ("group_inv_eq",
   ``!g :: Group. !x y :: (g.carrier). (g.inv x = y) = (g.mult x y = g.id)``,
   METIS_TAC [group_inv_eq_imp, group_inv_eq_imp']);

(* Theorem: Inverse of products: (x y)' = y' x' *)
val group_inv_mult = store_thm
  ("group_inv_mult",
   ``!g :: Group. !x y :: (g.carrier).
       g.inv (g.mult x y) = g.mult (g.inv y) (g.inv x)``,
   RW_TAC resq_ss [] THEN
   match_tac group_inv_eq_imp' THEN
   RW_TAC std_ss [group_mult_carrier, group_inv_carrier] THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult x (g.mult y (g.mult (g.inv y) (g.inv x)))` THEN
   CONJ_TAC THEN1 (
       match_tac group_assoc THEN
       METIS_TAC [group_mult_carrier, group_inv_carrier]
       ) THEN
   MATCH_MP_TAC EQ_TRANS THEN
   Q.EXISTS_TAC `g.mult x (g.mult (g.mult y (g.inv y)) (g.inv x))` THEN
   CONJ_TAC THEN1 (
       AP_TERM_TAC THEN
       match_tac (GSYM group_assoc) THEN
       METIS_TAC [group_mult_carrier, group_inv_carrier]
       ) THEN
   RW_TAC std_ss [group_rinv, group_lid, group_inv_carrier]);

val context = subtypeTools.add_rewrite2'' group_inv_mult context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* Next: Group exponentials = Group repeats *)

(* ------------------------------------------------------------------------- *)
(* Homomorphisms, isomorphisms, endomorphisms, automorphisms and subgroups.  *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The trivial group.                                                        *)
(* ------------------------------------------------------------------------- *)

(* Definition of Trivial Group: {e} *)
val TRIVIAL_GROUP = Define
  `trivial_group e : 'a group =
   <| carrier := {e}; id := e; inv := (\x. e); mult := (\x y. e) |>`;

(* Theorem: Trivial Group is a finite Abelian group *)
val trivial_group = store_thm
  ("trivial_group",
   ``!e. trivial_group e IN FiniteAbelianGroup``,
   RW_TAC resq_ss
     [FINITE_ABELIAN_GROUP, FINITE_GROUP, GROUP, ABELIAN_GROUP, TRIVIAL_GROUP,
      GSPECIFICATION,
      pred_setTheory.FINITE_INSERT,
      pred_setTheory.FINITE_EMPTY,
      pred_setTheory.IN_INSERT,
      pred_setTheory.NOT_IN_EMPTY,
      combinTheory.K_THM]);

val context = subtypeTools.add_reduction2 trivial_group context;
val {simplify = alg_ss, normalize = alg_ss'} = subtypeTools.simpset2 context;

(* ------------------------------------------------------------------------- *)
(* The cyclic group.                                                         *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The group of addition modulo n.                                           *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The group of multiplication modulo p.                                     *)
(* ------------------------------------------------------------------------- *)

(* ========================================================================= *)
(* Cryptography based on groups                                              *)
(* ========================================================================= *)

