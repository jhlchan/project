(* ------------------------------------------------------------------------- *)
(* Syntax: Pretty-printers, Parsers and Recognizers.                         *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

open stringTheory

val _ = new_theory "syntax"

(* The datatype: tree, with leaf Lf and node Nd *)
val _ = Hol_datatype `tree = Lf of 'a | Nd of 'b => tree => tree`;

val _ = Hol_datatype `opn = Plus | Times`

val example1 = ``Nd Plus (Lf 3) (Lf 4)``
val example2 = ``Nd Times (Nd Plus (Lf 3) (Lf 4)) (Lf 5)``

(* Example:  Godel numbering: x = 0, + = 1.

               +                      1
              / \                    / \
             +   x                  1  0
            /\   /\                /\  /\
           x x  $  $              0 0  $ $
          /\ /\                  /\ /\
         $ $$ $                 $ $$ $
*)

(* Check expression tree *)
val IsExpr_def = Define`
  (IsExpr Lf = T) /\
  (IsExpr (Nd 0 t1 t2) = (t1 = Lf) /\ (t2 = Lf)) /\
  (IsExpr (Nd 1 t1 t2) = (t1 <> Lf) /\ (t2 <> Lf) /\ (IsExpr t1) /\ (IsExpr t2))
`;

EVAL ``IsExpr (Nd 0 Lf Lf)``; (* T *)
EVAL ``IsExpr (Nd 1 (Nd 1 Lf Lf) (Nd 0 Lf Lf))``; (* F *)
EVAL ``IsExpr (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))``; (* T *)
EVAL ``IsExpr (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
                    (Nd 0 Lf Lf))``; (* T *)
EVAL ``IsExpr (Nd 0 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
                    (Nd 0 Lf Lf))``; (* F *)
EVAL ``IsExpr (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
                    (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)))``; (* T *)

(* Prettyprinters -------------------------------------------------- *)

(* Prefix pretty-printing of expression tree *)
val Prefix_def = Define`
  (Prefix lf nd (Lf a) = [lf a]) /\
  (Prefix lf nd (Nd op t1 t2) = [nd op] ++ Prefix lf nd t1 ++ Prefix lf nd t2)
`;

val _ = Hol_datatype `token = NUMTOK of num | OPTOK of opn`

EVAL ``Prefix NUMTOK OPTOK ^example2``

EVAL ``Prefix (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))``; (* ["+"; "x"; "x"] = [1;0;0] *)
EVAL ``Prefix (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)) (Nd 0 Lf Lf))``; (*  [1; 1; 0; 0; 0] *)
EVAL ``Prefix (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
                    (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)))``; (*  [1; 1; 0; 0; 1; 0; 0] *)
(*
val T1 = ``(Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))``;
EVAL ``Prefix T1``;
*)

(* Postfix pretty-printing of expression tree *)
val Postfix_def = Define`
  (Postfix lf nd (Lf a) = [lf a]) /\
  (Postfix lf nd (Nd op t1 t2) =
     Postfix lf nd t1 ++ Postfix lf nd t2 ++ [nd op])
`;
EVAL ``Postfix NUMTOK OPTOK ^example2``(Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))``; (* ["x"; "x"; "+"] = [0;0;1] *)
EVAL ``Postfix (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)) (Nd 0 Lf Lf))``; (*  [0; 0; 1; 0; 1] *)
EVAL ``Postfix (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
                     (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)))``; (*  [0; 0; 1; 0; 0; 1; 1] *)

(* Infix pretty-printing of expression tree *)
val Infix_def = Define`
  (Infix Lf = []) /\
  (Infix (Nd op t1 t2) = Infix t1 ++ [op] ++ Infix t2)
`;
EVAL ``Infix (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))``; (* ["x"; "+"; "x"] = [0;1;0] *)
(* Need to insert brackets for more levels *)
EVAL ``Infix (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)) (Nd 0 Lf Lf))``; (*  [0; 1; 0; 1; 0] *)
EVAL ``Infix (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
                   (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)))``; (* [0; 1; 0; 1; 0; 1; 0] *)

(* Parsers ---------------------------------------------------------------- *)

(* Inverse programming: turning pretty-printer into parser *)
(* How to convert the token list into an expression tree *)

(* For [1,0,0]    Lf
   ->    [0,0]    (Nd 1 Lf Lf)
   ->      [0]    (Nd 1 (Nd 0 Lf Lf) Lf)
   ->       []    (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
*)
(* Prefix parser to expression tree *)
val PrefixParse_defn = Hol_defn "PrefixParse" `
  (PrefixParse [] = NONE) /\
  (PrefixParse (OPTOK opn::t) =
      case PrefixParse t of
         NONE -> NONE
      || SOME (lt, rest1) ->
           (case PrefixParse rest1 of
               NONE -> NONE
            || SOME(rt, rest2) -> SOME (Nd opn lt rt, rest2))) /\
  (PrefixParse (NUMTOK n::t) = SOME(Lf n, t))`;

val wf_length = prove(``WF (measure (LENGTH:token list -> num))``,
                      SRW_TAC [][prim_recTheory.WF_measure])
val wfrec' = MATCH_MP relationTheory.WFREC_THM wf_length
             |> SIMP_RULE (srw_ss()) [relationTheory.RESTRICT_DEF,
                                      prim_recTheory.measure_thm]

val def = definition "PrefixParse_AUX_def" |> SIMP_RULE (srw_ss()) []

val rewrite =
    def
        |> Q.SPEC `measure (LENGTH: token list -> num)`
        |> SIMP_RULE (srw_ss())[FUN_EQ_THM]
        |> ONCE_REWRITE_RULE [wfrec']
        |> SIMP_RULE (srw_ss()) []
        |> REWRITE_RULE [GSYM def]

val lemma = prove(
  ``!t t' v. (PrefixParse_aux (measure LENGTH) t = SOME(v,t')) ==>
             LENGTH t' < LENGTH t``,
  GEN_TAC THEN completeInduct_on `LENGTH t` THEN
  SIMP_TAC (srw_ss())[Once rewrite] THEN
  Cases_on `t` THEN SIMP_TAC (srw_ss()) [] THEN
  Cases_on `h` THEN1 SRW_TAC [][] THEN
  ASM_SIMP_TAC (srw_ss()) [] THEN
  STRIP_TAC THEN REPEAT GEN_TAC THEN
  Cases_on `PrefixParse_aux (measure LENGTH) t'` THEN1 SRW_TAC [][] THEN
  Cases_on `x` THEN ASM_SIMP_TAC (srw_ss()) [] THEN
  FULL_SIMP_TAC (srw_ss() ++ boolSimps.DNF_ss) [] THEN
  `LENGTH r < LENGTH t'` by METIS_TAC [DECIDE ``v < SUC v``] THEN
  `LENGTH r < SUC (LENGTH t')` by DECIDE_TAC THEN
  ASM_SIMP_TAC (srw_ss()) [] THEN
  Cases_on `PrefixParse_aux (measure LENGTH) r` THEN1 SRW_TAC [][] THEN
  Cases_on `x` THEN ASM_SIMP_TAC (srw_ss()) [] THEN SRW_TAC [][] THEN
  `LENGTH r' < LENGTH r` by METIS_TAC [] THEN
  DECIDE_TAC);

val (PrefixParse_def, PrefixParse_ind) = Defn.tprove(PrefixParse_defn,
   WF_REL_TAC `measure LENGTH` THEN SRW_TAC [ARITH_ss][] THEN
   METIS_TAC [REWRITE_RULE [prim_recTheory.measure_def,
                            relationTheory.inv_image_def] lemma,
              DECIDE ``v < x ==> v < SUC x``]);

(* This works only for simple case: *)
EVAL ``PrefixParse [OPTOK Plus;NUMTOK 1;NUMTOK 3]``;
EVAL ``PrefixParse [OPTOK Plus;NUMTOK 1;NUMTOK 4; NUMTOK 5]``; (* [1; 0; 0] *)
(* This won't work for next case: *)
EVAL ``PrefixParse [OPTOK Plus; OPTOK Times; NUMTOK 1; NUMTOK 2; NUMTOK 3]``;

case PrefixParse arg of
  NONE -> ..
  SOME(t, rest) -> t

(* Should be: (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)) (Nd 0 Lf Lf)) *)
EVAL ``IsExpr (Nd 1 (Nd 1 Lf Lf) (Nd 0 Lf Lf))``; (* F *)
(* For [1,1,0,0,0]    Lf
   ->    [1,0,0,0]    (Nd 1 Lf Lf)
   ->      [0,0,0]    (Nd 1 (Nd 1 Lf Lf) Lf)
   ->        [0,0]    (Nd 1 (Nd 1 (Nd 0 Lf Lf) Lf) Lf)
   ->          [0]    (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)) Lf)
   ->           []    (Nd 1 (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf)) (Nd 0 Lf Lf))
*)
EVAL ``Prefix (PrefixParse [1; 1; 0; 0; 0] Lf)``; (* [1; 1; 0] *)

EVAL ``PrefixParse [1; 1; 0; 0; 1; 0; 0] Lf``; (* Nd 1 (Nd 0 Lf Lf) (Nd 1 (Nd 0 Lf Lf) (Nd 1 Lf Lf)) *)
EVAL ``Prefix (PrefixParse [1; 1; 0; 0; 1; 0; 0] Lf)``; (*  [1; 0; 1; 0; 1] *)

(* For [0,0,1]    Lf
   ->    [0,1]    (Nd 0 Lf Lf)
   ->      [1]    (Nd 0 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
   ->       []    (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
*)
(* Postfix parser to expression tree *)
val PostfixParse_def = Define`
  (PostfixParse [] tree = tree) /\
  (PostfixParse (1::t) (Nd 0 t1 t2) = PostfixParse t (Nd 1 t1 t2)) /\
  (PostfixParse (0::t) tree =
    if (tree = Lf) then PostfixParse t (Nd 0 Lf Lf) else
    PostfixParse t (Nd 0 tree (Nd 0 Lf Lf)))
`;

EVAL ``PostfixParse [0;0;1] Lf``; (* Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf) *)

EVAL ``Postfix (PostfixParse [0;0;1] Lf)``; (* [0; 0; 1] *)

(* For [0,1,0]    Lf
   ->    [1,0]    (Nd 0 Lf Lf)
   ->      [0]    (Nd 1 (Nd 0 Lf Lf) Lf)
   ->       []    (Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf))
*)
(* Infix parser to expression tree *)
val InfixParse_def = Define`
  (InfixParse [] tree = tree) /\
  (InfixParse (1::t) tree = InfixParse t (Nd 1 tree Lf)) /\
  (InfixParse (0::t) Lf = InfixParse t (Nd 0 Lf Lf)) /\
  (InfixParse (0::t) (Nd 1 tree Lf) = InfixParse t (Nd 1 tree (Nd 0 Lf Lf)))
`;

EVAL ``InfixParse [0;1;0] Lf``; (* Nd 1 (Nd 0 Lf Lf) (Nd 0 Lf Lf) *)

EVAL ``Infix (InfixParse [0;1;0] Lf)``; (* [0; 1; 0] *)

(* Recognizer *)

(* Prefix recognizer *)
val IsPrefix_def = Define`
  (IsPrefix [] tree = T) /\
  (IsPrefix (1::t) tree = IsPrefix t (Nd 1 Lf tree)) /\
  (IsPrefix (0::t) Lf = F) /\
  (IsPrefix (0::t) (Nd 0 t1 t2) = F) /\
  (IsPrefix (0::t) (Nd 1 t1 t2) =
    if (t1 = Lf) then IsPrefix t (Nd 1 (Nd 0 Lf Lf) t2) else
    if (t2 = Lf) then IsPrefix t (Nd 1 t1 (Nd 0 Lf Lf)) else
    IsPrefix t (Nd 1 t1 t2))
`;

EVAL ``IsPrefix [1;0;0] Lf``; (* T *)
EVAL ``IsPrefix [0;1;0] Lf``; (* F *)
EVAL ``IsPrefix [0;0;1] Lf``; (* F *)

(* Postfix recognizer -- cannot have IsPostfix [] tree = T *)
val IsPostfix_def = Define`
  (IsPostfix [] Lf = F) /\
  (IsPostfix [] (Nd 0 t1 t2) = F) /\
  (IsPostfix [] (Nd 1 t1 t2) = T) /\
  (IsPostfix (1::t) Lf = F) /\
  (IsPostfix (1::t) (Nd 0 t1 t2) = IsPostfix t (Nd 1 t1 t2)) /\
  (IsPostfix (1::t) (Nd 1 t1 t2) = F) /\
  (IsPostfix (0::t) tree =
    if (tree = Lf) then IsPostfix t (Nd 0 Lf Lf) else
    IsPostfix t (Nd 0 tree (Nd 0 Lf Lf)))
`;

EVAL ``IsPostfix [1;0;0] Lf``; (* F *)
EVAL ``IsPostfix [0;1;0] Lf``; (* F -- T if IsPostfix [] tree = T *)
EVAL ``IsPostfix [0;0;1] Lf``; (* T *)


(* Infix recognizer *)
val IsInfix_def = Define`
  (IsInfix [] tree = T) /\
  (IsInfix (1::t) tree = IsInfix t (Nd 1 tree Lf)) /\
  (IsInfix (0::t) Lf = IsInfix t (Nd 0 Lf Lf)) /\
  (IsInfix (0::t) (Nd 1 t1 t2) = if (t2 = Lf) then IsInfix t (Nd 1 t1 (Nd 0 Lf Lf)) else F) /\
  (IsInfix (0::t) (Nd 0 t1 t2) = F)
`;

EVAL ``IsInfix [1;0;0] Lf``; (* F *)
EVAL ``IsInfix [0;1;0] Lf``; (* T *)
EVAL ``IsInfix [0;0;1] Lf``; (* F *)
