open HolKernel boolLib bossLib Parse

val _ = new_theory "shunting"

val _ = Hol_datatype `
  token = Num of num | BinOp of (num => num => num) | LParen | RParen
`

val _ = Hol_datatype`assoc = L | R`

val _ = Hol_datatype`
  exprtree = ENum of num
           | BinOpNode of (num => num => num) => exprtree => exprtree
`

val linearise_def = Define`
  (linearise optable (ENum n) = [Num n]) /\
  (linearise optable (BinOpNode f e1 e2) =
     let r1 =
       case e1 of
          ENum n -> [Enum n]
       || BinOpNode g ->
            let fprec = ...


linearise e1 ++ [BinOp f] ++ linearise e2) /\
  (linearise (Paren e) = [LParen] ++ linearise e ++ [RParen])
`

val shunting_def = Define`
  shunt optable input stack =
     case input of
        [] -> []
     || t :: ts -> (case t of
                       Num n -> t :: shunt optable ts stack
                    || BinOp f ->
