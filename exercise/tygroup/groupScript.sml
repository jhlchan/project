(* ------------------------------------------------------------------------- *)
(* Group Theory -- axioms to exponentiation.                                 *)
(* ------------------------------------------------------------------------- *)

(*

Group Theory
============
based on: examples/elliptic/groupScript.sml

The idea behind this script is discussed in (Secton 2.1.1. Groups):

Formalizing Elliptic Curve Cryptography in Higher Order Logic (Joe Hurd)
http://www.gilith.com/research/papers/elliptic.pdf

*)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "group";

(* ------------------------------------------------------------------------- *)

(* open dependent theories *)
open pred_setTheory arithmeticTheory cardinalTheory

(* ------------------------------------------------------------------------- *)
(* Group Definition.                                                         *)
(* ------------------------------------------------------------------------- *)

(* Set up group type as a record
   A Group has:
   . a carrier set (set = function 'a -> bool, since MEM is a boolean function)
   . an identity element
   . an inverse function (unary operation)
   . a product function called multiplication (binary operation)
*)
val _ = Hol_datatype`
  pregroup = <| id: 'a;
                inv: 'a -> 'a;
                mult: 'a -> 'a -> 'a
           |>`;

(* Group Definition:
   A Group is a set with elements of g of type 'a group, such that
   . g.id is in the carrier set (e in G)
   . (g.mult x y) is in the carrier set (closure: x*y in G)
   . (g.inv x) is in the carrier set (existence of inverse: x^-1 in G)
   . (g.mult (g.inv x) x) = (g.id)  (property of inverse and identity: (x^-1)*(x) = e)
   . (g.mult (g.mult x y) z) = (g.mult x (g.mult y z)) (associativity: (x*y)*z = x*(y*z))
*)
(* Define Group by predicate *)
val GroupPred_def = Define`
  GroupPred (g: 'a pregroup) =
    (!x. g.mult g.id x = x) /\
    (!x. g.mult (g.inv x) x = g.id) /\
    (!x y z. g.mult (g.mult x  y) z = g.mult x  (g.mult y  z))
  `;


open boolSimps
val FINITE_BIJECTIONS = store_thm(
  "FINITE_BIJECTIONS",
  ``!s. FINITE s ==> ?f. BIJ f s (count (CARD s))``,
  HO_MATCH_MP_TAC pred_setTheory.FINITE_INDUCT THEN
  SRW_TAC [][BIJ_EMPTY] THEN
  Q.EXISTS_TAC `\a. if a = e then CARD s else f a` THEN
  FULL_SIMP_TAC (srw_ss() ++ DNF_ss) [BIJ_DEF, INJ_DEF, SURJ_DEF] THEN
  SRW_TAC [][] THEN RES_TAC THEN SRW_TAC [ARITH_ss][] THEN
  ASM_SIMP_TAC (srw_ss() ++ COND_elim_ss ++ DNF_ss) [] THEN
  Cases_on `x < CARD s` THEN1 METIS_TAC [] THEN
  SRW_TAC [ARITH_ss][]);

val two_lemma = prove(
  ``(2 * n) DIV 2 = n``,
  METIS_TAC [DECIDE ``0n < 2``, MULT_COMM, MULT_DIV]);

val symdiff_def = Define`symdiff s t = (s ∪ t) DIFF (s ∩ t)`

val symdiff_EMPTY = store_thm(
  "symdiff_EMPTY",
  ``(symdiff ∅ s = s) /\ (symdiff s ∅ = s)``,
  SRW_TAC [][EXTENSION, symdiff_def]);
val _ = export_rewrites ["symdiff_EMPTY"]

val symdiff_INV = store_thm(
  "symdiff_INV",
  ``symdiff s s = ∅``,
  SRW_TAC [][EXTENSION, symdiff_def]);
val _ = export_rewrites ["symdiff_INV"]

val FINITE_symdiff = store_thm(
  "FINITE_symdiff",
  ``FINITE s1 /\ FINITE s2 ==> FINITE (symdiff s1 s2)``,
  SRW_TAC[][symdiff_def]);

val symdiff_ASSOC = store_thm(
  "symdiff_ASSOC",
  ``symdiff s1 (symdiff s2 s3) = symdiff (symdiff s1 s2) s3``,
  SRW_TAC [][EXTENSION, symdiff_def] THEN DECIDE_TAC);

val GroupsExist = store_thm(
  "GroupsExist",
  ``?x:'a pregroup. GroupPred x``,
  Cases_on `FINITE univ(:'a)` THEN1
    (Q.ABBREV_TAC `N = CARD univ(:'a)` THEN
     `0 < N` by METIS_TAC [CARD_EQ_0, UNIV_NOT_EMPTY,
                           DECIDE ``0 < n <=> n <> 0``] THEN
     `?f. BIJ f univ(:'a) (count N)`
        by METIS_TAC [FINITE_BIJECTIONS] THEN
     `!a. f a < N` by METIS_TAC [BIJ_IFF_INV, IN_COUNT, IN_UNIV] THEN
     `?g : num -> 'a. (!n. n < N ==> (f (g n) = n)) /\ (!a. g (f a) = a)`
        by (FULL_SIMP_TAC (srw_ss()) [BIJ_IFF_INV] THEN
            Q.EXISTS_TAC `g` THEN SRW_TAC [][]) THEN
     SRW_TAC [][GroupPred_def] THEN
     Q.EXISTS_TAC `<| mult := (\a b. g ((f a + f b) MOD N));
                      id := g 0; inv := (\a. if f a = 0 then g 0
                                             else g (N - f a)) |>` THEN
     SRW_TAC [][] THENL [
       `N - f x < N /\ f x < N` by SRW_TAC [ARITH_ss][] THEN
       SRW_TAC [ARITH_ss][],
       METIS_TAC [arithmeticTheory.MOD_PLUS, arithmeticTheory.ADD_ASSOC,
                  arithmeticTheory.MOD_MOD]
     ]) THEN
  `∃f. BIJ f univ(:'a) { s | FINITE s /\ s ⊆ univ(:'a) }`
    by METIS_TAC [cardeq_def, finite_subsets_bijection] THEN
  FULL_SIMP_TAC (srw_ss()) [] THEN
  `(∀a. FINITE (f a)) ∧
   ∃g: ('a -> bool) -> 'a. (∀s. FINITE s ⇒ (f (g s) = s)) ∧ (∀a. g (f a) = a)`
     by (FULL_SIMP_TAC (srw_ss()) [BIJ_IFF_INV] THEN METIS_TAC[]) THEN
  Q.EXISTS_TAC `<| mult := \a b. g (symdiff (f a) (f b));
                   id := g ∅; inv := \a. a |>` THEN
  SRW_TAC [][GroupPred_def] THEN SRW_TAC[][FINITE_symdiff, symdiff_ASSOC]);

val group_type_bijections = define_new_type_bijections {
  ABS = "mkGroup", REP = "destGroup", name = "group_type_bijections",
  tyax = new_type_definition("group", GroupsExist)}

val group_inv_def = Define`group_inv g = (destGroup g).inv`
val group_id_def = Define`group_id g = (destGroup g).id`
val group_mult_def = Define`group_mult g = (destGroup g).mult`

val _ = app (fn s =>
                (clear_overloads_on (GrammarSpecials.recsel_special ^ s);
                 clear_overloads_on (GrammarSpecials.recfupd_special ^ s);
                 clear_overloads_on (s ^ "_fupd")))
            ["mult", "id", "inv"]

val _ = add_record_field ("inv", ``group_inv``)
val _ = add_record_field ("id", ``group_id``)
val _ = add_record_field ("mult", ``group_mult``)

(* set overloading  *)
val _ = overload_on ("*", ``g.mult``);
val _ = overload_on ("|/", ``g.inv``);
val _ = overload_on ("#e", ``g.id``);

val _ = add_rule { fixity = Suffix 2100, term_name = "|/",
                   block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                   paren_style = OnlyIfNecessary,
                   pp_elements = [TOK "⁻¹"]}

(* ------------------------------------------------------------------------- *)
(* More Group Defintions.                                                    *)
(* ------------------------------------------------------------------------- *)
(* Abelian Group: a Group with a commutative product: x y = y x. *)
val AbelianGroup_def = Define`
  AbelianGroup (g: 'a group) = !x y:'a. x * y = y * x
  `;

(* Finite Group: a Group with a finite carrier set. *)
val FiniteGroup_def = Define`
  FiniteGroup (g: 'a group) = FINITE univ(:'a)`

(* Finite Abelian Group: a Group that is bith Finite and Abelian. *)
val FiniteAbelianGroup_def = Define`
  FiniteAbelianGroup (g: 'a group) =
    FiniteGroup g /\ AbelianGroup g
  `;

(* Summary of theorems
   (Group with g.carrier G, x, y, z in G, e = g.id, x' = g.inv x, x y = g.mult x y):

group_id_carrier    e in G
group_inv_carrier   x' in G
group_mult_carrier  x y in G                 [closure]
group_lid           e x = x                  [left-identity]
group_linv          x' x = e                 [left-inverse]
group_assoc         (x y) z = x (y z)        [associativity]

group_rinv          x x' = e
group_rid           x e = x                  [uses group_rinv]
group_lsolve        x y = z <=> x = z y'     [uses group_rinv/linv, group_rid]
group_rsolve        x y = z <=> y = x' z     [uses group_rinv/linv, group_lid]
group_lcancel       x y = x z  <=> y = z     [uses group_rsolve, group_linv, group_lid]
group_rcancel       y x = z x <=> y = z      [uses group_lsolve, group_rinv, group_rid]
group_lid_unique    y x = x <=> y = e        [uses group_lsolve, group_rinv]
group_rid_unique    x y = x <=> y = e        [uses group_rsovle, group_linv]
group_linv_unique   x y = e <=> x = y'       [uses group_lsovle, group_linv, group_lid]
group_rinv_unique   x y = e <=> y = x'       [uses group_rsolve, group_rinv, group_rid]
group_inv_inv       (x')' = x                [uses group_linv_unique, group_rinv]
group_inv_cancel    x' = y' <=> x = y        [uses group_inv_inv]
group_inv_eq_swap   x' = y <=> x = y'        [uses group_inv_inv]
group_inv_id        e' = e                   [uses group_linv_unique, group_lid]
group_inv_mult      (x y)' = y' x'           [uses group_rinv_unique, group_rinv, group_rid]
group_id_fix        x*x = x ==> x = #e       [uses group_lid, group_rcancel]

group_exp_carrier   x**n in G
group_exp_zero      x**0 = #e
group_exp_one       x**1 = x                          [uses group_rid]
group_id_exp        #e**n = #e                        [uses group_lid]
group_comm_exp      x*y = y*x ==> x**n *y = y* x**n   [uses group_lid, group_rid, group_assoc]
group_exp_comm      x**n *x = x* x**n
group_mult_exp      x*y = y*x ==> (x*y)**n = (x**n)*(y**n)  [uses group_lid, group_assoc, group_comm_exp]
group_exp_add       x**(m + n) = (x**m)*(x**n)        [uses group_lid, group_assoc]
group_exp_mult      x**(m * n) = (x** m)** n          [uses group_exp_add, group_mult_exp, group_exp_comm]
group_exp_inv       |/(x**n) = (|/x)**n               [uses group_inv_id, group_exp_comm]
group_exp_eq        m < n, x**m = x**n ==> x**(n-m) = #e  [uses group_rid_unique]

*)

(* ------------------------------------------------------------------------- *)
(* Basic theorem from definition.                                            *)
(* ------------------------------------------------------------------------- *)

val forall_group = store_thm(
  "forall_group",
  ``(∀g:'a group. P g) ⇔ (∀p. GroupPred p ⇒ P (mkGroup p))``,
  METIS_TAC[group_type_bijections]);

val destGroup_mkGroup =
    group_type_bijections |> CONJUNCT2 |> SPEC_ALL |> EQ_IMP_RULE |> #1

(* Theorem: [Group left Identity] e x = x *)
val group_lid = store_thm(
  "group_lid",
  ``!g:'a group. #e * x = x``,
  SIMP_TAC (srw_ss()) [forall_group, group_mult_def, group_id_def, destGroup_mkGroup] THEN
  SIMP_TAC (srw_ss()) [GroupPred_def]);
val _ = export_rewrites ["group_lid"]

(* Theorem: [Group left inverse] x' x = e *)
val group_linv = store_thm(
  "group_linv",
  ``!g:'a group x. |/x * x = #e``,
  SIMP_TAC (srw_ss()) [forall_group, group_mult_def, group_inv_def, destGroup_mkGroup,
                       group_id_def] THEN
  SRW_TAC [][GroupPred_def]);
val _ = export_rewrites ["group_linv"]

(* Theorem: [Group associativity] (x y) z = x (y z) *)
val group_assoc = store_thm(
  "group_assoc",
  ``!(g:'a group) x y z. x * (y * z) = (x * y) * z``,
  SIMP_TAC (srw_ss()) [forall_group, group_mult_def, group_inv_def, destGroup_mkGroup,
                       group_id_def] THEN
  SRW_TAC [][GroupPred_def]);

(* ------------------------------------------------------------------------- *)
(* Theorems in basic Group Theory.                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: [Group right inverse] x x' = e *)
(* Proof:
   x x'
 = e(x x')          by left identity: X = e X
 = (e x)x'          by associativity
 = ((x')' x')x)x'   by left inverse: e = Y' Y
 = ((x')'(x' x)) x' by associativity
 = ((x')' e)x'      by left inverse: Y' Y = e
 = (x')'(e x')      by associativity
 = (x')' x'         by left idenity: e X = X
 = e                by left inverse: Y' Y = e
*)
val group_rinv = store_thm(
  "group_rinv",
  ``!(g:'a group) x. x * |/x = #e``,
  REPEAT GEN_TAC THEN
  `x * |/x = #e * (x * |/x)`       by SRW_TAC [][] THEN
  `_ = (#e * x) * |/x`             by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x * x * |/x`    by SIMP_TAC(srw_ss())[] THEN
  `_ = |/(|/x) * (|/x * x) * |/x`  by SIMP_TAC(srw_ss())[group_assoc] THEN
  `_ = |/(|/x) * #e * |/x`         by SRW_TAC [][] THEN
  `_ = |/(|/x) * (#e * |/x)`       by SRW_TAC [][group_assoc] THEN
  `_ = |/(|/x) * |/x`              by SRW_TAC [][] THEN
  `_ = #e`                         by SRW_TAC [][] THEN
  SRW_TAC [][]);
val _ = export_rewrites ["group_rinv"]

(* Theorem: [Group right identity] x e = x *)
(* Proof:
   x e
 = x (x' x)     by left inverse: Y' Y = e
 = (x x') x     by associativity
 = e x          by right inverse: Y Y' = e
 = x            by left identity: e X = X
*)
val group_rid = store_thm(
  "group_rid",
  ``!(g:'a group) x. x * #e = x``,
  SRW_TAC [][] THEN
  `x * #e = x * (|/x * x)` by SRW_TAC [][] THEN
  `_ = (x * |/x) * x`      by SRW_TAC [][group_assoc] THEN
  `_ = #e * x`             by SRW_TAC [][] THEN
  SRW_TAC [][]);
val _ = export_rewrites ["group_rid"]

(* Theorem: [Solve left unknown] x y = z  <=> x = z y' *)
(* Proof:
   For the if part:
      z y'
    = (x y) y'   by substituting z
    = x (y y')   by associativity
    = x e        by right inverse
    = x          by right identity
   For the only-if part:
      x y
    = (z y') y   by substituting x
    = z (y' y)   by associativity
    = z e        by left inverse
    = z          by right identity
*)
val group_lsolve = store_thm(
  "group_lsolve",
  ``!(g:'a group) x y z. (x * y = z) = (x = z * |/y)``,
  METIS_TAC [group_assoc, group_rinv, group_linv, group_rid]);

(* Theorem: [Solve right unknown] x y = z  <=> y = x' z *)
(* Proof:
   For the if part:
      x' z
    = x' (x y)   by substituting z
    = (x' x) y   by associativity
    = e y        by left inverse
    = y          by left identity
   For the only-if part:
      x y
    = x (x' z)   by substituting y
    = (x x') z   by associativity
    = e z        by right inverse
    = z          by left identity
*)
val group_rsolve = store_thm(
  "group_rsolve",
  ``!(g:'a group) x y z. (x * y = z) = (y = |/x * z)``,
  METIS_TAC [group_assoc, group_rinv, group_linv, group_lid]);

(* Theorem: [Left cancellation] x y = x z <=> y = z *)
(* Proof:
        x y = x z
   <=>  y = x' (x z)    by group_rsolve
          = (x' x) z    by associativity
          = e z         by left inverse
          = z           by left identity
*)
val group_lcancel = store_thm(
  "group_lcancel",
  ``!(g:'a group) x y z. (x * y = x * z) = (y = z)``,
  METIS_TAC [group_assoc, group_rsolve, group_linv, group_lid]);
val _ = export_rewrites ["group_lcancel"]

(* Theorem: [Right cancellation] y x = z x <=> y = z *)
(* Proof:
       y x = z x
   <=> y = (z x) x'   by group_lsolve
         = z (x x')   by associativity
         = z e        by right inverse
         = z          by right identity
*)
val group_rcancel = store_thm(
  "group_rcancel",
  ``!(g:'a group) x y z. (y * x = z * x) = (y = z)``,
  METIS_TAC [group_assoc, group_lsolve, group_rinv, group_rid]);

(* Theorem: [Left identity unique] y x = x <=> y = e *)
(* Proof:
       y x = x
   <=> y = x x'   by group_lsolve
         = e      by right inverse
*)
val group_lid_unique = store_thm(
  "group_lid_unique",
  ``!(g:'a group) x y. (y * x = x) = (y = #e)``,
  METIS_TAC [group_lsolve, group_rinv]);
val _ = export_rewrites ["group_lid_unique"]

(* Theorem: [Right identity unique] x y = x <=> y = e *)
(* Proof:
       x y = x
   <=> y = x' x    by group_rsolve
         = e       by left inverse
*)
val group_rid_unique = store_thm(
  "group_rid_unique",
  ``!(g:'a group) x y. (x * y = x) = (y = #e)``,
  METIS_TAC [group_rsolve, group_linv]);
val _ = export_rewrites ["group_rid_unique"]

(* Theorem: [Left inverse unique] x y = e <=> x = y' *)
(* Proof:
       x y = e
   <=> x = e y'    by group_lsolve
         = y'      by left identity
*)
val group_linv_unique = store_thm(
  "group_linv_unique",
  ``!g x y. (x * y = #e) = (x = |/y)``,
  METIS_TAC [group_lsolve, group_linv, group_lid]);

(* Theorem: [Right inverse unique] x y = e <=> y = x' *)
(* Proof:
       x y = e
   <=> y = x' e    by group_rsolve
         = x'      by right identity
*)
val group_rinv_unique = store_thm(
  "group_rinv_unique",
  ``(x * y = #e) = (y = |/x)``,
  METIS_TAC [group_rsolve, group_rinv, group_rid]);

(* Theorem: [Inverse of inverse] x'' = x *)
(* Proof:
       x x' = e     by right inverse
   <=>    x = (x')' by group_linv_unique
*)
val group_inv_inv = store_thm(
  "group_inv_inv",
  ``|/(|/x) = x``,
  METIS_TAC [group_rinv, group_linv_unique]);
val _ = export_rewrites ["group_inv_inv"]

(* Theorem: [Inverse cancellation] x' = y' <=> x = y *)
(* Proof:
   Only-if part is trivial. For the if part:
       x' = y'
   ==> (x')' = (y')'
   ==>     x = y       by group_inv_inv
*)
val group_inv_cancel = store_thm(
  "group_inv_cancel",
  ``(|/x = |/y) = (x = y)``,
  METIS_TAC [group_inv_inv]);
val _ = export_rewrites ["group_inv_cancel"]

(* Theorem: [Inverse equality swap]: x' = y <=> x = y' *)
(* Proof:
       x' = y
   <=> x'' = y'    by group_inv_cancel
   <=>   x = y'    by group_inv_inv
*)
val group_inv_eq_swap = store_thm(
  "group_inv_eq_swap",
  ``(|/x = y) = (x = |/y)``,
  METIS_TAC [group_inv_cancel, group_inv_inv]);

(* Theorem: [Inverse of identity] e' = e *)
(* Proof:
       e e = e    by left identity: e X = X
   <=>   e = e'   by group_linv_unique
*)
val group_inv_id = store_thm(
  "group_inv_id",
  ``(|/ #e = g.id)``,
  METIS_TAC [group_lid, group_linv_unique]);

(* Theorem: [Inverse of product] (x y)' = y' x' *)
(* Proof:
   First show this product:
     (x y) (y' x')
   = ((x y) y') x'     by associativity
   = (x (y y')) x'     by associativity
   = (x e) x'          by right inverse
   = x x'              by right identity
   = e                 by rigtt inverse
   Hence (x y)' = y' x'  by group_rinv_unique.
*)
val group_inv_mult = store_thm(
  "group_inv_mult",
  ``|/(x * y) = |/y * |/x``,
  `(x * y) * (|/y * |/x) = x * (y * |/y) * |/x` by SRW_TAC [][group_assoc] THEN
  `_ = #e`                                      by SRW_TAC [][] THEN
  POP_ASSUM MP_TAC THEN
  SRW_TAC [][group_rinv_unique]);

(* Theorem: The identity is a fixed point: x*x = x ==> x = #e. *)
(* Proof:
   For the if part:
       x * x = x
   ==> x * x = #e * x     by group_lid
   ==> x = #e             by group_rcancel
   For the only-if part:
       #e * #e = #e       by group_lid
*)
val group_id_fix = store_thm(
  "group_id_fix",
  ``((x * x = x) <=> (x = #e))``,
  METIS_TAC [group_lid, group_rcancel]);


(* ------------------------------------------------------------------------- *)
(* Application of basic Group Theory:                                        *)
(* Exponentiation - the FUNPOW version of Group operation.                   *)
(* ------------------------------------------------------------------------- *)

(* Define exponents of a group element:
   For x in Group g,   group_exp x 0 = g.id
                       group_exp x (SUC n) = g.mult x (group_exp x n)
*)
val group_exp_def = Define`
  (group_exp g x 0 = g.id) /\
  (group_exp g x (SUC n) = x * (group_exp g x n))
`;
val _ = export_rewrites ["group_exp_def"]

(* set overloading  *)
val _ = overload_on ("**", ``group_exp g``);

(* Theorem: x ** 0 = #e *)
(* Proof:
   x ** 0 = #e  by definition.
*)
val group_exp_zero = store_thm(
  "group_exp_zero",
  ``(x ** 0 = #e)``,
  SRW_TAC [][]);

(* Theorem: x ** 1 = x *)
(* Proof:
   x ** 1 = x ** SUC 0 = x * x**0 = x * #e = x   by group_rid.
*)
val group_exp_one = store_thm(
  "group_exp_one",
  ``(x ** 1 = x)``,
  `1 = SUC 0` by RW_TAC arith_ss [] THEN
  POP_ASSUM (fn th => SIMP_TAC bool_ss [group_exp_def, th]) THEN
  SRW_TAC[][]);
val _ = export_rewrites ["group_exp_one"]

(* Theorem: (g.id ** n) = g.id  *)
(* Proof:
   By induction on n.
   Base case: #e ** 0 = #e    by group_exp_def.
   Step case: True because !y :: g.carrier. #e*y = y  by group_lid.
*)
val group_id_exp = store_thm(
  "group_id_exp",
  ``g.id ** n = #e``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_lid]);

(* Theorem: For abelian group g,  (x ** n) * y = y * (x ** n) *)
(* Proof:
   By induction on n.
   Base case: (x ** 0) * y
             = #e * y           by group_exp_def
             = y * #e           by group_rid
             = y * (x ** 0)     by group_exp_def
   Step case:  (x * x** n) * y
             = x * ((x ** n)*y) by group_assoc
             = x * (y*(x ** n)) by abelian group
             = (x*y) * (x ** n) by group_assoc
             = (y*x) * (x ** n) by abelian group
             = y *(x* (x ** n)) by group_assoc
*)
val group_comm_exp = store_thm(
  "group_comm_exp",
  ``(x * y = y * x) ==> ((x ** n) * y = y * (x ** n))``,
  Induct_on `n` THEN SRW_TAC [][] THEN METIS_TAC [group_assoc]);

(* Theorem: (x ** n) * x = x * (x ** n) *)
(* Proof:
   Since x * x = x * x, this is true by group_comm_exp.
*)
val group_exp_comm = store_thm(
  "group_exp_comm",
  ``(x ** n) * x = x * (x ** n)``,
  METIS_TAC [group_comm_exp]);

(* Theorem: For abelian group, (x * y) ** n = (x ** n) * (y ** n) *)
(* Proof:
   By induction on n.
   Base case: (x*y) ** 0 = #e = #e * #e = (x ** 0) * (y ** 0).
   Step case: (x*y)*((x*y) ** n)
            = (x*y)*((x**n)*(y**n)) by inductive hypothesis
            = x*(y*((x**n)*(y**n))) by group_assoc
            = x*((y*(x**n))*(y**n)) by group_assoc
            = x*(((x**n)*y)*(y**n)) by group_comm_exp
            = x*((x**n)*(y*(y**n))) by group_assoc
            = (x*(x**n))*(y*(y**n)) by group_assoc
*)
val group_mult_exp = store_thm(
  "group_mult_exp",
  ``(x * y = y * x) ==> ((x * y) ** n = (x ** n) * (y ** n))``,
  Induct_on `n` THEN1 METIS_TAC [group_exp_def, group_lid] THEN
  SRW_TAC [][] THEN
  `x * y * (x * y) ** n = (x*y)*((x**n)*(y**n))` by METIS_TAC [] THEN
  `_ = x*(y*((x**n)*(y**n)))` by SRW_TAC [][group_assoc] THEN
  `_ = x*((y*(x**n))*(y**n))` by SRW_TAC [][group_assoc] THEN
  `_ = x*(((x**n)*y)*(y**n))` by METIS_TAC [group_comm_exp] THEN
  `_ = x*((x**n)*(y*(y**n)))` by SRW_TAC [][group_assoc] THEN
  SRW_TAC [][group_assoc]);

(* Theorem: x ** (m + n) = (x ** m) * (x ** n) *)
(* Proof:
   By induction on m.
   Base case: x ** (0 + n) = x ** n = #e * (x ** n) = (x ** 0) * (x ** n).
   Step case: x ** (SUC m + n)
            = x ** (SUC (m + n))   by arithmetic
            = x * (x ** (m + n))   by group_exp_def
            = x * ((x**m)*(x**n))  by inductive hypothesis
            = (x * (x**m))*(x**n)  by group_assoc
*)
val group_exp_add = store_thm(
  "group_exp_add",
  ``x ** (m + n) = (x ** m) * (x ** n)``,
  Induct_on `m` THEN1 SRW_TAC [][] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m + n = SUC (m+n)` by RW_TAC arith_ss [] THEN
  `x ** (SUC m + n) = x ** (SUC (m+n))` by SRW_TAC [][] THEN
  `_ = x * (x ** (m + n))` by SRW_TAC [][group_exp_def] THEN
  METIS_TAC [group_assoc]);

(* Theorem: x ** (m * n) = (x ** m) ** n  *)
(* Proof:
   By induction on m.
   Base case: x ** (0 * n) = x ** 0 = #e = (#e)**n = (x ** 0) ** n, by group_id_exp.
   Step case: x ** (SUC m * n)
            = x ** (m*n + n)       by arithmetic
            = (x**(m*n))*(x ** n)  by group_exp_add
            = ((x**m)**n)*(x**n)   by inductive hypothesis
            = ((x**m)*x)**n        by group_mult_exp and group_exp_comm
            = (x*(x**m))**n        by group_exp_comm
*)
val group_exp_mult = store_thm(
  "group_exp_mult",
  ``x ** (m * n) = (x ** m) ** n``,
  Induct_on `m` THEN1 SRW_TAC [][group_exp_def, group_id_exp] THEN
  SRW_TAC [][group_exp_def] THEN
  `SUC m * n = m * n + n` by METIS_TAC [arithmeticTheory.MULT] THEN
  `x ** (SUC m * n) = x ** (m*n + n)` by SRW_TAC [][] THEN
  `_ = (x**(m*n))*(x ** n)` by SRW_TAC [][group_exp_add] THEN
  `_ = ((x**m)**n)*(x**n)` by METIS_TAC [] THEN
  `_ = ((x**m)*x)**n` by SRW_TAC [][group_mult_exp, group_exp_comm] THEN
  SRW_TAC [][group_exp_comm]);

(* Theorem: Inverse of exponential:  |/(x**n) = (|/x) ** n *)
(* Proof:
   By induction on n.
   Base case: |/ x**0 = |/#e = #e = #e ** 0 = (|/#e)**0  since |/#e = #e by group_inv_id
   Step case: |/ x**(SUC n)
            = |/ x * (x ** n)      by group_exp_def
            = (|/ (x ** n))*(|/x)  by group_inv_mult
            = (|/x)**n * (|/x)     by inductive hypothesis
            = (|/x)* (|/x) ** n    by group_exp_comm
            = (|/x)*(SUC n)
*)
val group_exp_inv = store_thm(
  "group_exp_inv",
  ``(|/ (x ** n) = (|/ x) ** n)``,
  Induct_on `n` THEN
  METIS_TAC [group_exp_def, group_inv_id, group_inv_mult, group_exp_comm]);

(* Theorem: For m < n, x**m = x**n ==> x**(n-m) = #e *)
(* Proof:
   If m < n, then n = m + d,
     x**n
   = x**(m+d)             by n = m+d
   = (x**m)*(x**d)        by group_exp_add
   ==> x**d = #e          by group_rid_unique
*)
val group_exp_eq = store_thm(
  "group_exp_eq",
  ``m < n /\ (x**m = x**n) ==> (x**(n-m) = #e)``,
  REPEAT STRIP_TAC THEN
  `?d. (d = n - m) /\ 0 < d /\ (n = m + d)` by RW_TAC arith_ss [] THEN
  `x**n = x**(m+d)` by SRW_TAC [][] THEN
  `_ = x**m * x**d` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_rid_unique]);

(* ------------------------------------------------------------------------- *)
(* Application of Group Exponentiaton:                                       *)
(* The order of a group element in a Finite Group.                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: For Group g and x IN g.carrier, if x**n are all distinct, g.carrier is INFINITE *)
(* Proof:
   By contradiction.
   Let c = CARD g.carrier.
   The map (count (SUC c)) -> G such that n -> x**n is:
   (1) a map since each x**n IN g.carrier
   (2) injective since all x**n are distinct
   But c < SUC c = CARD (count (SUC c)), and this contradicts the Pigeon-hole Principle.
*)
val group_exp_all_distinct = store_thm(
  "group_exp_all_distinct",
  ``(!m n. (x**m = x**n) ==> (m = n)) ==> INFINITE univ(:'a)``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  Q.ABBREV_TAC `c = CARD univ(:'a)` THEN
  `c < SUC c` by RW_TAC arith_ss [] THEN
  `INJ (\n. x**n) (count (SUC c)) univ(:'a)`
    by SRW_TAC [][pred_setTheory.INJ_DEF] THEN
  METIS_TAC [pred_setTheory.CARD_COUNT, pred_setTheory.PHP]);

(* Theorem: For FINITE Group g and x IN g.carrier, there is a k > 0 such that x**k = #e. *)
(* Proof:
   Since g.carrier is FINITE,
   ?m n. m <> n and x**m = x**n      by group_exp_all_distinct
   Assume m < n, then x**(n-m) = #e  by group_exp_eq
   The case m > n is symmetric.

   Note: Probably can be improved to bound k <= CARD g.carrier.
*)
val group_exp_period_exists = store_thm(
  "group_exp_period_exists",
  ``!(g:'a group) x. FiniteGroup g ==> ?k. 0 < k /\ (x**k = #e)``,
  SRW_TAC [][FiniteGroup_def] THEN
  `?m n. m <> n /\ (x**m = x**n)` by METIS_TAC [group_exp_all_distinct] THEN
  Cases_on `m < n` THENL [
    `0 < n-m` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [group_exp_eq]);

(* Define order = LEAST period for an element x in Group g *)
val period_def = Define`
  period g x k = 0 < k /\ (x**k = g.id)
`;
val order_def = Define`
  order g x = LEAST k. period g x k
`;

(* Theorem: The finite group order is indeed a period. *)
val group_order_period = store_thm(
  "group_order_period",
  ``FiniteGroup g ==> period g x (order g x)``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LEAST_INTRO]);

(* Theorem: If k < order of finite group, then k is not a period. *)
val group_order_minimal = store_thm(
  "group_order_minimal",
  ``FiniteGroup g ==> !k. k < (order g x) ==> ~ period g x k``,
  METIS_TAC [order_def, period_def, group_exp_period_exists, whileTheory.LESS_LEAST]);

(* Theorem: The finite group order m satisfies: 0 < m and x**m = #e. *)
(* Proof: by order being a period, and apply period_def. *)
val group_order_property = store_thm(
  "group_order_property",
  ``FiniteGroup g ==> 0 < order g x /\ (x ** order g x = #e)``,
  METIS_TAC [group_order_period, period_def]);

(* Theorem: For finite group, |/ x = x ** (m-1) where m = order g x *)
(* Proof:
   Let m = order g x.
   Since x**(m-1)*x = x**m = #e    by group_exp_add
   |/x = x**(m-1)                  by group_linv_unique
*)
val group_order_inv = store_thm(
  "group_order_inv",
  ``FiniteGroup g ==> (|/x = x**((order g x)-1))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `x ** 1 = x` by METIS_TAC [group_exp_one] THEN
  `(m-1)+1 = m` by RW_TAC arith_ss [] THEN
  `x ** (m-1) * x = #e` by METIS_TAC [group_exp_add] THEN
  METIS_TAC [group_linv_unique]);

(* Theorem: For FINITE Group g, x**n = x**(n mod m), where m = order g x *)
(* Proof:
     x**n
   = x**(m*q + r)        by arithmetic and n = q*m + r
   = x**(m*q)*(x**r)     by group_exp_add
   = ((x**m)**q)*(x**r)  by group_exp_mult
   = (#e**q)*(x**r)      by group_order_property
   = #e*(x**r)           by group_id_exp
   = x**r                by group_lid
*)
val group_exp_mod = store_thm(
  "group_exp_mod",
  ``!g x n. FiniteGroup g ==> (x**n = x**(n MOD order g x))``,
  REPEAT STRIP_TAC THEN
  `?m. (m = order g x) /\ 0 < m /\ (x**m = #e)` by SRW_TAC [][group_order_property] THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  `n = (n DIV m)*m + (n MOD m)` by SRW_TAC [][arithmeticTheory.DIVISION] THEN
  `_ = m*(n DIV m) + (n MOD m)` by RW_TAC arith_ss [] THEN
  METIS_TAC [group_exp_add, group_exp_mult, group_id_exp, group_lid]);

(* Theorem: For FINITE group g, m, n < (order g x), x**m = x**n ==> m = n *)
(* Proof:
   Otherwise x**(m-n) = #e by group_exp_eq,
   contradicting minimal nature of m.
*)
val group_order_unique = store_thm(
  "group_order_unique",
  ``!g x m n. FiniteGroup g /\ m < order g x /\ n < order g x ==>
    (x**m = x**n) ==> (m = n)``,
  SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
  FULL_SIMP_TAC (srw_ss()) [FiniteGroup_def] THEN
  Cases_on `m < n` THENL [
    `0 < n-m /\ n-m < order g x` by RW_TAC arith_ss [],
    `n < m /\ 0 < m-n /\ m-n < order g x` by RW_TAC arith_ss []
  ] THEN
  METIS_TAC [FiniteGroup_def, group_exp_eq, group_order_minimal, period_def]);

(* ------------------------------------------------------------------------- *)

val GroupHoms_def = Define`
  GroupHoms G1 G2 = { f | ∀x y. f (G1.mult x y) = G2.mult (f x) (f y) }
`;
val _ = overload_on ("-->", ``GroupHoms``)

val _ = set_fixity "-->" (Infixr 750)
val _ = set_mapped_fixity {tok = ">->", fixity = Infixr 750, term_name = "inj_arrow"}

val GroupHoms_ID = store_thm(
  "GroupHoms_ID",
  ``f ∈ g --> h ⇒ (f g.id = h.id)``,
  SRW_TAC [][GroupHoms_def] THEN
  `f x = f (x * #e)` by SIMP_TAC (srw_ss()) [] THEN
  `_ = h.mult (f x) (f #e)` by METIS_TAC[] THEN
  METIS_TAC[group_rid_unique])

val GroupHoms_INV = store_thm(
  "GroupHoms_INV",
  ``f ∈ g --> h ⇒ (f (g.inv a) = h.inv (f a))``,
  ONCE_REWRITE_TAC [GSYM group_rinv_unique] THEN
  STRIP_TAC THEN
  `h.mult (f a) (f (g.inv a)) = f (g.mult a (g.inv a))`
    by FULL_SIMP_TAC (srw_ss()) [GroupHoms_def] THEN
  POP_ASSUM SUBST1_TAC THEN SRW_TAC [][GroupHoms_ID]);


val subgroup_def = Define`
  subgroup (g:'a group) (h:'b group) <=>
     ∃f. INJ f univ(:'a) univ(:'b) ∧
         ∀x y. f (g.mult x y) = h.mult (f x) (f y)
`;

val coset_def = Define`
  coset (g:'a group) X a = IMAGE (λz. a * z) X
`;

val coset_EMPTY = store_thm(
  "coset_EMPTY",
 ``coset g {} a = {}``,
  SRW_TAC [][coset_def]);

val coset_UNIV = store_thm(
  "coset_UNIV",
  ``coset g univ(:'a) x = univ(:'a)``,
  SRW_TAC [][coset_def, EXTENSION] THEN METIS_TAC [group_rsolve]);

val IN_coset = store_thm(
  "IN_coset",
  ``x IN coset g X a <=> ∃y. y IN X ∧ (x = a * y)``,
  SRW_TAC[][coset_def] THEN METIS_TAC[]);

val hom_coset_nonempty = store_thm(
  "hom_coset_nonempty",
  ``f ∈ (g:'a group) --> (h:'b group) ⇒ ∀a. a ∈ coset h (IMAGE f univ(:α)) a``,
  SRW_TAC [][IN_coset] THEN Q.EXISTS_TAC `h.id` THEN SRW_TAC [][] THEN
  Q.EXISTS_TAC `g.id` THEN SRW_TAC [][GroupHoms_ID]);

val hom_coset_relate = store_thm(
  "hom_coset_relate",
  ``f ∈ (h:'b group) --> (g:'a group) ∧ y ∈ coset g (IMAGE f univ(:β)) x ⇒
    ∃z. x = y * f z``,
  SIMP_TAC (srw_ss() ++ boolSimps.DNF_ss)[IN_coset] THEN Q.X_GEN_TAC `z` THEN
  STRIP_TAC THEN Q.EXISTS_TAC `h.inv z` THEN
  FULL_SIMP_TAC (srw_ss()) [GroupHoms_def] THEN
  METIS_TAC [group_rinv, group_assoc, group_rid]);

val hom_coset_eq1 = prove(
  ``f ∈ (h:'b group) --> (g:'a group) ∧ |/y * x ∈ IMAGE f univ(:β) ⇒
    (coset g (IMAGE f univ(:β)) x = coset g (IMAGE f univ(:β)) y)``,
  STRIP_TAC THEN
  `∃b. f b = y⁻¹ * x`
    by (FULL_SIMP_TAC (srw_ss()) [] THEN METIS_TAC[]) THEN
  SIMP_TAC (srw_ss() ++ boolSimps.DNF_ss)[coset_def, EXTENSION] THEN
  Q.X_GEN_TAC `a` THEN EQ_TAC THENL [
    DISCH_THEN (Q.X_CHOOSE_THEN `b1` SUBST_ALL_TAC) THEN
    `x = y * f b` by METIS_TAC [group_rsolve, group_inv_inv] THEN
    SRW_TAC [][GSYM group_assoc, group_lcancel] THEN
    FULL_SIMP_TAC (srw_ss()) [GroupHoms_def] THEN METIS_TAC[],
    DISCH_THEN (Q.X_CHOOSE_THEN `b1` SUBST_ALL_TAC) THEN
    `y = x * (f b)⁻¹`
      by METIS_TAC [group_lsolve, group_inv_inv, group_inv_mult] THEN
    SRW_TAC [][GSYM group_assoc, group_lcancel] THEN
    Q.EXISTS_TAC `h.mult (h.inv b) b1` THEN
    `f (h.mult (h.inv b) b1) = f (h.inv b) * f b1`
      by FULL_SIMP_TAC (srw_ss()) [GroupHoms_def] THEN
    SRW_TAC[][group_rcancel, GroupHoms_INV]
  ]);

val hom_coset_eq2 = prove(
  ``f ∈ h --> (g:'a group) ∧ (coset g (IMAGE f univ(:β)) x = coset g (IMAGE f univ(:β)) y) ⇒
    y⁻¹ * x ∈ IMAGE f univ(:β)``,
  STRIP_TAC THEN
  `y ∈ coset g (IMAGE f univ(:β)) x` by METIS_TAC [hom_coset_nonempty] THEN
  `∃z. x = y * f z` by METIS_TAC[hom_coset_relate] THEN
  SRW_TAC[][group_assoc] THEN METIS_TAC[]);

val hom_coset_eq = prove(
  ``f ∈ h --> (g:'a group) ⇒
    ((coset g (IMAGE f univ(:β)) x = coset g (IMAGE f univ(:β)) y) ⇔
     y⁻¹ * x ∈ IMAGE f univ(:β))``,
  METIS_TAC [hom_coset_eq1, hom_coset_eq2]);

val subset_bij = prove(
  ``∀a. BIJ (λb. a * b) B (coset g B a)``,
  SRW_TAC[][BIJ_DEF, SURJ_DEF, INJ_DEF, IN_coset] THEN
  SRW_TAC[][]);

(*
val hom_coset_bij = prove(
  ``f ∈ h --> (g:'a group) ⇒
    ∀a. BIJ (λb. a * f b) univ(:β) (coset g (IMAGE f univ(:β)) a)``,
  SRW_TAC [][BIJ_DEF, SURJ_DEF, INJ_DEF, IN_coset] THENL [
    METIS_TAC[],
    ALL_TAC,
    METIS_TAC[],
    METIS_TAC[]
*)

(* etc etc.

   But this all breaks down when we try to define the generated
   subgroup.

   The generatedGroup constant would need to take the enclosing group,
   and an element from that group, and so would have to have type

      :'a group -> 'a -> ?? group

   The problem is that the ?? has to depend on the *value* of the
   second parameter.

   For example, take a four element type to be 'a. Then imagine that
   the group defined is the "addition mod 4" group. If you take 0 as
   the value for the generator, then the ?? type parameter has to have
   cardinality one, but if it's 1, then the ?? type parameter has size
   4, and if it's 2, then the ?? type parameter has size 2. This is a
   great application of dependent typing, which we don't have in HOL.

*)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
