(* Ring Theory *)

(* based on src/ring/src/ringScript.sml *)

(* Below is the first part of the above script.

Comments:
. In order to understand the script, I've loaded only the mininum tools for the part so far.
. The datatype definition notion: <| .... |> is new.
. Sometimes terms are denoted by: --`....`--, is this the same as ``....`` ?
. The two "apps" is doing something about the names, even modifying the Parser. Why?
. After Parse change, the axioms must use: Term`....`, not ``....``.
. Is "is_ring_def" using pointers? The "^" looks like pointers.
. "set_assums" is used, this is new.
. Here theorems are proved using "asm_store_thm", not store_thm.
  Did try to work out the (given) proofs interactively, using goalstack,
  but cannot state the theorem in the form: g `....`;
*)

load "abs_tools";
open abs_tools;
(*WAIT*)

(* Define datatype for Ring *)
(* R0: zero = additive identity *)
(* R1: one = multiplicative identity *)
(* RP: add operation: x + y -> z *)
(* RM: multiply operation: x * y -> z *)
(* RN: negative = additive inverse: x -> -x *)
Hol_datatype `ring = <| R0 : 'a;
                        R1 : 'a;
                        RP : 'a -> 'a -> 'a;
                        RM : 'a -> 'a -> 'a;
                        RN : 'a -> 'a
                      |>`;

(* Define r of type Ring *)
(* val r = --`r:'a ring`--; *) (* original form *)
val r = ``r:'a ring``; (* this form also works *)

(* something about r and the names. This requires the abs_tools. *)
app (C add_impl_param [r]) ["R0","R1","RP","RM","RN"];
(* overload_on the names and change the Parser(?) *)
app (fn s => overload_on (s, Parse.Term [QUOTE ("ring_"^s)])) ["R0","R1","RP","RM","RN"];

(* the Ring axioms - after r is defined *)
(* These won't work:
val p_plus_sym = ``!n m.  RP n m = RP m n``;
val p_plus_assoc = ``!n m p.  RP n (RP m p) = RP (RP n m) p``;
val p_plus_zero_left = ``!n.  RP R0 n = n``;
val p_opp_def = ``!n.  RP n (RN n) = R0``;
val p_mult_sym = ``!n m.  RM n m = RM m n``;
val p_mult_assoc = ``!n m p.  RM n (RM m p) = RM (RM n m) p``;
val p_mult_one_left = ``!n.  RM R1 n = n``;
val p_distr_left = ``!n m p.  RM (RP n m) p = RP (RM n p) (RM m p)``;
*)
(* Change of Parse.Term forces the following: *)
val p_plus_sym = Term`!n m.  RP n m = RP m n`;
val p_plus_assoc = Term`!n m p.  RP n (RP m p) = RP (RP n m) p`;
val p_mult_sym = Term`!n m.  RM n m = RM m n`;
val p_mult_assoc = Term`!n m p.  RM n (RM m p) = RM (RM n m) p`;
val p_plus_zero_left = Term`!n.  RP R0 n = n`;
val p_mult_one_left = Term`!n.  RM R1 n = n`;
val p_opp_def = Term`!n.  RP n (RN n) = R0`;
val p_distr_left = Term`!n m p.  RM (RP n m) p = RP (RM n p) (RM m p)`;

(* Define Ring for r *)
val is_ring_def = Define `
    is_ring ^r =
       ^p_plus_sym
    /\ ^p_plus_assoc
    /\ ^p_plus_zero_left
    /\ ^p_opp_def
    /\ ^p_mult_sym
    /\ ^p_mult_assoc
    /\ ^p_mult_one_left
    /\ ^p_distr_left`;
(*PAUSE*)

(* Ring Theorems *)

(* We work on an abstract_ring r *)
set_assums [ --`is_ring ^r`-- ];

(* Each axiom is automatically a theorem *)
val ring_proj_tac = POP_ASSUM MP_TAC THEN REWRITE_TAC [is_ring_def] THEN STRIP_TAC;

(* These can be proved by asm_store_thm, how to use goalstck to see the subgoals interactively? *)
val plus_sym       = asm_store_thm("plus_sym",       p_plus_sym,       ring_proj_tac);
val plus_assoc     = asm_store_thm("plus_assoc",     p_plus_assoc,     ring_proj_tac);
val plus_zero_left = asm_store_thm("plus_zero_left", p_plus_zero_left, ring_proj_tac);
val opp_def        = asm_store_thm("opp_def",        p_opp_def,        ring_proj_tac);
val mult_sym       = asm_store_thm("mult_sym",       p_mult_sym,       ring_proj_tac);
val mult_assoc     = asm_store_thm("mult_assoc",     p_mult_assoc,     ring_proj_tac);
val mult_one_left  = asm_store_thm("mult_one_left",  p_mult_one_left,  ring_proj_tac);
val distr_left     = asm_store_thm("distr_left",     p_distr_left,     ring_proj_tac);
(*PAUSE*)

(* Derived Ring Theorems *)

(* Theorem: in a ring, left zero is also the right zero. *)
val plus_zero_right = asm_store_thm(
    "plus_zero_right",
    Term` !n. RP n R0 = n `,
    REPEAT GEN_TAC THEN
    ONCE_REWRITE_TAC [plus_sym] THEN
    REWRITE_TAC [plus_zero_left]);
(* This is proved, but how to use goalstck to see the subgoals interactively? *)

(* Theorem: in a ring, left multiply by zero gives zero. *)
fun EQ_TRANS_TAC t = MATCH_MP_TAC EQ_TRANS THEN EXISTS_TAC t THEN CONJ_TAC;

val mult_zero_left = asm_store_thm(
    "mult_zero_left",
    Term` !n. RM R0 n = R0 `,
    GEN_TAC THEN
    EQ_TRANS_TAC(Term` RP (RM (RP R0 R0) n) (RN (RM R0 n)) `) THENL [
        REWRITE_TAC[distr_left,GSYM plus_assoc],
        ALL_TAC
    ] THEN
    REWRITE_TAC[opp_def, plus_zero_right]);
(*PAUSE*)

(* Theorem: in a ring, right multiply by zero gives zero. *)
val mult_zero_right = asm_store_thm(
    "mult_zero_right",
    Term` !n. RM n R0 = R0 `,
    REPEAT GEN_TAC THEN
    ONCE_REWRITE_TAC [mult_sym] THEN
    REWRITE_TAC [mult_zero_left]);
(*PAUSE*)
