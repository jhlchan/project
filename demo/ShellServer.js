/*
 * Objective:
 * 1. Will respond to Shell.html
 * 2. Each input line from Shell.html is returns its uppercase
 * 3. (Eventually) when input is dir *.js, will return command result to webpage.
 * 4. (ToDo) Server side logging (logOpen and logClose)
 *
 * DOS> node ShellServer.js
 * To kill the server:
 * DOS> taskkill /F /IM node.exe && taskkill /F /IM mosml.exe
 *
 * ~/work/hol/demo$ node ShellServer.js 8080  (for MacOS)
 * ~/work/hol/fermat/petersen$ node ../../demo/ShellServer.js 8080
 *
 * For logging: just highlight and copy the web display.
 * 
 */
var sys = require("sys"),
   http = require("http"),
   path = require("path"),
   url = require("url"),
   fs = require("fs"),
   child = require('child_process')

var args = process.argv.slice(1)
if (args[0] == '-h') {
   console.log('Usage: node ShellServer.js [port] [shell] [prompt]')
   process.exit()
}
/* e.g.
 args[0]=C:\jc\www\atp\work\ShellServer.js
 args[1]=8080
 args[2]="go hol"
 args[3]="> "
 */

// var debug = true // true or false
var debug = false
var PORT = args[1] || 80 // default 80
// var SHELL = null // initial, set by /rpc/shell
var SHELL = args[2] || 'hol' // default to HOL
//var SHELL = 'go mo'
//var SHELL = 'go hol'
//var PROMPT = /-\ $/  // to match MOSML or HOL prompt
//var PROMPT = />\ $/  // to match PolyML HOL prompt
var PROMPT = new RegExp((args[3] || '> ') + '$') // default to HOL prompt
// Note: quiet mode has no PROMPT!
// val _ = quietdec := true;
console.log('PORT=' + PORT)
console.log('SHELL=' + SHELL)
console.log('PROMPT=' + PROMPT)

// where is this script
// var root = process.argv[1].match(/(.+)\\/)[1] // for Windows with \\
// var root = process.argv[1].match(/(.+)\//)[1] // for Mac with /
var root = args[0].match(new RegExp('(.+)' + path.sep))[1] // path.sep = path separator
var cwd = process.cwd()
console.log('root: ', root) // process starting root (where this script (ShellServer.js) is located)
console.log(' cwd: ', cwd) // process starting location (where you type: go node shell)
// process.execPath.match(/(.+)\\/)[1]) is process execution location (where go.bat is located)
// do GET


function doGet(request, response) {
   var name = url.parse(request.url).pathname
   if (debug) console.log('name=[' + name + ']')
   // special treatment for /Shell.html, /Shell.js and favicon.ico
   var dir = (name == '/Shell.html' || name == '/Shell.js' || name == 'favicon.ico') ? root : cwd
   var fileName = path.join(dir, name)
   sys.puts('GET: ' + fileName)
   fs.exists(fileName, function (exists) {
      if (!exists) {
         response.writeHeader(404, {
            "Content-Type": "text/plain"
         })
         response.write("404 Not Found\n")
         response.end()
      }
      else {
         fs.readFile(fileName, "binary", function (err, file) {
            if (err) {
               response.writeHeader(500, {
                  "Content-Type": "text/plain"
               })
               response.write(err + '\n')
               response.end()
            }
            else {
               response.writeHeader(200)
               response.write(file, "binary")
               response.end()
            }
         })
      }
   })
}

/* Launcher object */

// Launcher statc object
var Launcher = new function () {
   var map = [] // map from uid to Launcher
   var count = 0 // count number of launchers
   // get Launcher based on uid
   this.get = function (uid) {
      var launcher = map[uid]
      if (launcher == null) {
         console.log('New Launcher for [' + uid + ']')
         launcher = makeLauncher(uid)
         map[uid] = launcher
         count++
         if (count > 3) throw "Too Many Launchers!"
      }
      if (debug) console.log('Get Launcher for [' + uid + ']')
      return launcher
   }

   // make a launcher
   function makeLauncher(uid) {
      var launcher = {}
      launcher.uid = uid // identifier of launcher
      // associate launcher with a process
      launcher.process = child.exec(SHELL)
      // associate launcher with a response
      launcher.response = null // to be set         
      // set properties for this launcher (once only)
      // write to launcher.stdin
      launcher.write = function (data) {
         launcher.process.stdin.write(data)
      }
      // end of launcher.stdin
      launcher.end = function () {
         launcher.process.stdin.end()
      }
      // capture launcher.process.stdout via 'data' events
      launcher.process.stdout.on('data', function (data) {
         console.log('launcher data: [' + data + ']')
         if (launcher.response == null) {
            console.log('launcher response not set, discard data.')
            return
         }
         launcher.response.write(data)
         // if (data.match(PROMPT) || data.match(/#\ $/)) {
         if (data.match(PROMPT)) {
            launcher.response.end() // fire 'end' event
            if (debug) console.log('launcher data ends in PROMPT, fire end.')
         }
      })
      // finish launcher.process.stdout via 'end' events
      launcher.process.stdout.on('end', function () {
         if (launcher.response == null) {
            console.log('launcher response not set, discard end.')
            return
         }
         launcher.response.write('No more input.\n')
         launcher.response.end()
      })
      launcher.process.on('exit', function (code) {
         console.log('Child process ends with exit code ' + code)
         // There are two ways to deal with the launcher at map[uid]
         // Method 1: delete the key-value pair
         //delete map[uid] // delete the key uid from the map
         // This will enable another run of client with uid=0 to repeat the process
         // However, this will cause a Web client to repeat the process when
         // there is further input after 'exit'. Hence the second method:
         // Method 2: associate key with invalid launcher
         map[uid] = true // true is an invalid launcher
         // Therefore any extra input after 'exit' will receive '!@#$' from server.
         // This means only a different uid can initiate/end a process.
         // For a Web client, refresh will give a different uid.
         // For a non-Web client, its uid must be generated from something else, e.g. time.
         count--
      })
      return launcher
   }
}

// do POST
function doPost(request, response) {
   // inspect(request, 'request')
   // wait for data
   var data = ''
   request.on('data', function (chunk) {
      data += chunk
      if (debug) console.log('doPost: chunk')
   })
   // process data on end
   request.on('end', function () {
      // get POST query and data
      var query = request.url
      console.log('POST query: [' + query + ']')
      console.log('POST data: [' + data + ']')
      // check /rpc/shell first
      if (query == '/rpc/shell/') {
         SHELL = data || SHELL
         console.log('SHELL: ' + SHELL)
         // get response from launcher later
         query = '/rpc/command/'
         data = ''
      }
      // get uid from request headers
      var headers = request.headers
      if (debug) console.log('headers: ' + JSON.stringify(headers))
      var type = headers['content-type']
      var length = headers['content-length']
      var uid = headers['client-uid']
      if (debug) console.log('Content-type: ' + type)
      if (debug) console.log('Content-length: ' + length)
      if (debug) console.log('Client-UID: ' + uid)
      if (!uid) {
         // ignore snooping clients with no uid
         console.log('Unidentified client detected!')
         // no response, let it hang
         return
      }

      // check if SHELL is defined
      if (SHELL == null) {
         console.log('SHELL not defined.')
         // process.exit() -- too harsh
         // marker for Javascript evaluation
         response.write('!@')
         var text = ''
         try {
            text = eval(data)
         }
         catch (error) {
            text = '! Error: ' + error
         }
         // ensure text is string
         text = text + '\n- '
         response.write(text)
         response.end()
         return
      }

      // get launcher from uid
      var launcher = Launcher.get(uid)
      /* This check is for method 2 in launcher.process.on('exit') */
      if (launcher == true) {
         // no launcher
         console.log('No launcher for client ' + uid)
         response.write('!@#$')
         response.end()
         return
      }
      // process query by launcher
      if (query == '/rpc/command/') {
         // just send data to launcher for processing, wait for response
         launcher.write(data)
         launcher.response = response // will collect stdout to response
      }
      else if (query == '/rpc/control/') {
         // check control char and check
         if (data.match(/^\^/)) {
            launcher.write(data.charCodeAt(1) - 64) // convert to control code
            launcher.response = response // will collect stdout to response
         }
         else if (data == '$') {
            // close launcher (client calls for stop)
            launcher.end()
            response.write('!@#$')
            response.end()
            return
         }
         else if (data.indexOf('$') == 0) {
            // $cmd
            SHELL = data.slice(1)
         }
         // if (data == '^C') running = false
         // to shutdown this server -- an overkill?
      }
      else {
         console.log('POST: Unknown protocol: ' + data)
      }
   })
}

// File server with request detection
http.createServer(function (request, response) {
   var method = request.method
   if (method == 'GET') doGet(request, response)
   if (method == 'POST') doPost(request, response)
}).on('end', function () {
   console.log('Server dies.')
}).listen(PORT)

sys.puts("Shell Server Running on port " + PORT)

/**
 * Documentation
 *
 * 1. GET requests are local file retrieval (i.e. from the starting directory),
 *    except for special ones: Shell.html, Shell.js and favicon.ico.
 *    These special files always come from the same location as ShellServer.js (this file).
 * 2. POST data are commands, based on query type:
 *    query: /rpc/shell/     data: initialize Launcher by setting SHELL variable (e.g. "go hol").
 *    query: /rpc/command/   data: will be input data sent to Launcher.
 *    query: /rpc/control/   data: will be special command (e.g. interrupts, exit, or CRTL-chars.)
 * 3. For /rpc/command/, the response ends when the PROMPT is detected from Launcher.
 * 4. If server ends session, server sends to client (response): '!@#$' (4 top-keys)
 *    If client ends session, client sends to server  (request): CTRL-$ (simulated)
 */