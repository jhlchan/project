/*
 * Objective:
 * 1. Respond to Demo.html
 *
 * DOS> node DemoServer.js
 * To kill the server (CTRL-C)
 *
 * ~/work/hol/demo$ node DemoServer.js  (for MacOS)
 * ~/work/hol/fermat/petersen$ node ../../demo/DemoServer.js
 *
 * For logging: just highlight and copy the web display.
 * Simple version: response to chunks, with a prefix "!".
 */
var sys = require("sys"),
   http = require("http"),
   path = require("path"),
   url = require("url"),
   fs = require("fs"),
   child = require('child_process')

var args = process.argv.slice(1)
if (args[1] == '-h') {
   console.log('Usage: node DemoServer.js [port]')
   process.exit()
}
/* e.g.
 args[0]=C:\jc\www\atp\work\DemoServer.js
 args[1]=8080
 args[2]="go hol"
 args[3]="> "
 */

var debug = true // true or false
// var debug = false
var PORT = args[1] || 8080 // default 8080
console.log('PORT=' + PORT)

// where is this script
// var root = process.argv[1].match(/(.+)\\/)[1] // for Windows with \\
// var root = process.argv[1].match(/(.+)\//)[1] // for Mac with /
var root = args[0].match(new RegExp('(.+)' + path.sep))[1] // path.sep = path separator
var cwd = process.cwd()
console.log('root: ', root) // process starting root (where this script (DemoServer.js) is located)
console.log(' cwd: ', cwd) // process starting location (where you type: go node shell)
// process.execPath.match(/(.+)\\/)[1]) is process execution location (where go.bat is located)

// do GET
function doGet(request, response) {
   var name = url.parse(request.url).pathname
   if (debug) console.log('name=[' + name + ']')
   // special treatment for /Demo.html, /Demo.js and favicon.ico
   var dir = (name == '/Demo.html' || name == '/Demo.js' || name == 'favicon.ico') ? root : cwd
   var fileName = path.join(dir, name)
   sys.puts('GET: ' + fileName)
   fs.exists(fileName, function (exists) {
      if (!exists) {
         response.writeHeader(404, {
            "Content-Type": "text/plain"
         })
         response.write("404 Not Found\n")
         response.end()
      }
      else {
         fs.readFile(fileName, "binary", function(err, file) {
            if (err) {
               response.writeHeader(500, {
                  "Content-Type": "text/plain"
               })
               response.write(err + '\n')
               response.end()
            }
            else {
               response.writeHeader(200)
               response.write(file, "binary")
               response.end()
            }
         })
      }
   })
}

// Shell = process wrapper
var Shell = function(uid, exit) {
  var self = this
  this.uid = uid // uniquie identifier
  // response to be set by doPost
  this.response = null 
  // input to be set upon init
  this.input = null
  // write to be set upon init
  this.write = null
  // make up child process options (buffer size in units of K)
  function makeOption(size) {
    return { encoding: 'utf8',
              timeout: 0,
            maxBuffer: (size || 200)*1024, // default 200*1024
           killSignal: 'SIGTERM',
                  cwd: null,
                  env: null }
  }
  // start the process cmd, and handle process.stdout
  this.init = function(cmd, prompt, size) {
    var process = child.exec(cmd, makeOption(size))
    // handle process.stdout 'data' events
    process.stdout.on('data', function(data) {
      if (debug) console.log('data: [' + data + ']')
      // JC: client -- print(data) // show output (display)
      // JC: server -- accumulate to response
      if (self.response == null) console.log('Shell response not ready for data!');
      else self.response.write(data)
      if (data.match(prompt)) {
        if (debug) console.log('data ends in PROMPT, call ready.')
        // JC: client -- Scheduler.onReady() // emit 'ready' to Scheduler
        // JC: server -- end response
        if (self.response == null) console.log('Shell response not ready for end!');
        else self.response.end()
      }
    })
    // handle process.stdout 'end' events
    process.stdout.on('end', function() {
      if (debug) console.log('process output ends.')
      // JC: server -- notifiy client: ready for more input
    })
    // handle process 'exit' event
    process.on('exit', function(code) {
      if (debug) console.log('Child process ends with exit code ' + code)
      // JC: client -- ready = null // no reply
      // JC: client -- if (!step) quit() // end program.
      // JC: server -- tell client to quit, tell Launcher to delete
      exit(self.uid)
    })
    // expose these only
    self.input = process.stdin // to this.end
    self.write = function(data) { process.stdin.write(data) } // to doPost
  }
  // end the child process
  this.end = function() {
    step = false // to quit
    self.input.end() // will trigger exit
  }
}

/* Static Launcher for shells */

var LMAX = 10 // connect to a max of 10 clients
// Launcher statc object
var Launcher = new function() {
   var map = [] // map from uid to shell
   var count = 0 // shell count
   // get Shell based on uid
   this.get = function(uid) {
      var shell = map[uid]
      if (shell == null) {
         count++
         if (count > LMAX) throw "Too many shells!"
         console.log('New Shell for [' + uid + ']')
         shell = new Shell(uid, Launcher.drop)
         map[uid] = shell
      }
      if (debug) console.log('Get shell for [' + uid + ']')
      return shell
   }
   // drop the shell with uid
   this.drop = function(uid) {
      // There are two ways to drop with the shell at map[uid]
      // Method 1: delete the key-value pair
      //delete map[uid] // delete the key uid from the map
      // This will enable another run of client with uid=0 to repeat the process
      // However, this will cause a Web client to repeat the process when
      // there is further input after 'exit'. Hence the second method:
      // Method 2: associate key with invalid shell
      map[uid] = true // true is an invalid shell
      // Therefore any extra input after 'exit' will receive '!@#$' from server.
      // This means only a different uid can initiate/end a process.
      // For a Web client, refresh will give a different uid.
      // For a non-Web client, its uid must be generated from something else, e.g. time.
      count--
      if (debug) console.log('Number of shells: ' + count)
   }
}

// do POST
function doPost(request, response) {
   // inspect(request, 'request')
   // wait for data
   var data = ''
   request.on('data', function(chunk) {
      data += chunk
      if (debug) console.log('doPost: chunk')
   })
   // process data on end
   request.on('end', function() {
      // get POST query and data
      var query = request.url
      console.log('POST query: [' + query + ']')
      console.log('POST data: [' + data + ']')
      // get uid from request headers
      var headers = request.headers
      if (debug) console.log('headers: ' + JSON.stringify(headers))
      var type = headers['content-type']
      var length = headers['content-length']
      var uid = headers['client-uid']
      if (debug) console.log('Content-type: ' + type)
      if (debug) console.log('Content-length: ' + length)
      if (debug) console.log('Client-UID: ' + uid)
      if (!uid) {
         // ignore snooping clients with no uid
         console.log('Unidentified client detected!')
         // no response, let it hang
         return
      }

/* // dummy response
      response.write('@' + data)
      response.end()
      return
*/

      // get shell from uid
      var shell = Launcher.get(uid)
      /* This check is for method 2 in shell.process.on('exit') */
      if (shell == true) { // invalid shell
         console.log('No shell for client ' + uid)
         response.write('!@#$')
         response.end()
         return
      }
      // what follows need link up of response
      if (response == null) console.log('JC: doPost response is null!')
      shell.response = response // will collect shell.process.stdout to response
      if (shell.response == null) console.log('JC: doPost should have shell response!')

      if (debug) console.log('JC: process query by shell: ' + query)
      // process query by shell
      if (query == '/command/') {
         // unpack command into cmd, prompt, size.
         var unpack = JSON.parse(data)
         var cmd = unpack.cmd
         var prompt = new RegExp(unpack.prompt) // server: string to regular-expression
         var size = Number(unpack.size)
         console.log('JC: cmd=' + cmd + ', prompt=' + prompt + ', size=' + size)
         shell.init(cmd, prompt, size)
      }
      else if (query == '/data/') {
         // just send data to launcher for processing, wait for response
         shell.write(data)
      }
      else if (query == '/control/') {
         // check control char and check
         if (data.match(/^\^/)) {
            shell.write(data.charCodeAt(1) - 64) // convert to control code
         }
         else if (data == '$') {
            // close shell (client calls for stop)
            shell.end()
            response.write('!@#$')
            response.end()
            return
         }
         // if (data == '^C') running = false
         // to shutdown this server -- an overkill?
      }
      else {
         console.log('POST: Unknown protocol: ' + query + ', to handle: ' + data)
      }
   })
}

// File server with request detection
http.createServer(function (request, response) {
   var method = request.method
   if (method == 'GET') doGet(request, response)
   if (method == 'POST') doPost(request, response)
}).on('end', function () {
   console.log('Server dies.')
}).listen(PORT)

sys.puts("Demo Server Running on port " + PORT + ", stop by CTRL-C.")

/**
 * Documentation
 *
 * 1. GET requests are local file retrieval (i.e. from the starting directory),
 *    except for special ones: Shell.html, Shell.js and favicon.ico.
 *    These special files always come from the same location as DemoServer.js (this file).
 * 2. POST data are commands, based on query type:
 *    query: /rpc/shell/     data: initialize Launcher by setting SHELL variable (e.g. "go hol").
 *    query: /rpc/command/   data: will be input data sent to Launcher.
 *    query: /rpc/control/   data: will be special command (e.g. interrupts, exit, or CRTL-chars.)
 * 3. For /rpc/command/, the response ends when the PROMPT is detected from Launcher.
 * 4. If server ends session, server sends to client (response): '!@#$' (4 top-keys)
 *    If client ends session, client sends to server  (request): CTRL-$ (simulated)
 */