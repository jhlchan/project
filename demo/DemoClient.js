/*
 * DemoClient.js
 *
 * This is the client program for Demo Server (DemoServer.js)
 *
 * Example of use (assume Demo Server is running):
 * $ node DemoClient.js localhost 8080                    (to run default dmeo for MacOS)
 * $ node DemoClient.js localhost 8080 primes-demo.hol    (to run primes-demo.hol via server)
 * $ node DemoClient.js localhost 8080 -l demo.hol        (to run demo.hol locally)
 *
 * Note:
 * JSON is built into V8.
 * http://code.google.com/p/v8/source/browse/trunk/src/json.js
 *
 * Based on client part of HOLDemo.js
 * Proper version: send a chunk to server, display response, wait for keypress.
 */
var http = require("http"),
    os = require('os'),
    fs = require('fs')

// var debug = true // true or false
var debug = false
var step = true
// var step = false // true or false
var pause = false // start with false

var args = process.argv.slice(1)
/* e.g.
args[0]=/Users/josephchan/work/hol/demo/DemoClient.js
 */
/* e.g.
   for getTextFileLocal: node DemoClient.js '' '' demo.hol 
   for getTextFileRemote:  node DemoClient.js '' '' primes-demo.hol
 */

var echo = true //echo for input
// check args
if (args[1] == '-h') {
   console.log('Usage: node DemoClient.js [host] [port] [demo-file]')
   console.log('       node DemoClient.js [host] [port] [demo-file] [buffer-size]')
   console.log('       node DemoClient.js [host] [port] -i [local-file]')
   console.log('       node DemoClient.js [host] [port] -l [local-file] [buffer-size]')
   return
}
// JC: better use the first pattern, with default server localhost:8080
//     and insert -server host:port as needed
// Also, [demo-file] is assumed to be remote (from the server)
// Unless preceded by -l, for local [demo-file] (where client resides)

var HOST = args[1] || 'localhost' // default localhost
var PORT = args[2] || 8080 // default 8080
var UID = (new Date()).getTime() // unique identifier of this client
if (debug) console.log('HOST: ' + HOST)
if (debug) console.log('PORT: ' + PORT)
if (debug) console.log('UID: ' + UID)

// get a file by path via Server
function doGet(path, callback) {
   var options = {
      host: HOST, // e.g. "192.168.1.101"
      port: PORT, // e.g. 8080
      path: '/' + path, // e.g. "/primes-demo.hol"
      headers: {
         'content-type': 'text/plain; charset=utf-8',
      // 'content-length': 0,  // this is optional
         'client-uid': UID
      },
      method: 'GET'
   }

   if (debug) console.log('doGet: ' + path)
   if (debug) console.log('doGet: header=' + JSON.stringify(options.headers))
   if (debug) console.log('doGet: options=' + JSON.stringify(options))
   console.log('Loading file from ' + options.path)

   // send GET request, with response-handling callback to collect data for doGet callback
   var request = http.request(options, function(response) {
      if (debug) console.log('status: ' + response.statusCode)
      if (debug) console.log('headers: ' + JSON.stringify(response.headers))
      response.setEncoding('UTF8')
      var body = []
      response.on('data', function(chunk) {
         if (debug) console.log('data: ' + chunk)
         body.push(chunk)
      })
      response.on('end', function() {
         if (debug) console.log('JC: END response')
         // send body to callback
         callback(body.join(''))
      })
   })
   .on('error', function(e) {
      // tell request error
      console.log('problem with GET request: ' + e.message);    
   })
   // send GET data
   // request.write('') // this is optional
   request.end() // fire 'end' event
   // end of doGet
}

// set up POST option with query
function doPost(query, data, callback) {
   var options = {
      host: HOST,
      port: PORT,
      path: query, // query in path
      headers: {
         'content-type': 'text',
         'content-length': data.length,
         'client-uid': UID
      },
      method: 'POST'
   }
   if (debug) console.log('doPost: ' + query + '' + data)
   if (debug) console.log('doPost: header=' + JSON.stringify(options.headers))

   // send POST request, with response callback
   var request = http.request(options, function(response) {
      if (debug) console.log('status: ' + response.statusCode)
      if (debug) console.log('headers: ' + JSON.stringify(response.headers))
      response.setEncoding('UTF8')
      response.on('data', function(chunk) {
         if (chunk == '!@#$') {
           chunk = ''
           if (debug) console.log('No more input.')
         }
         print(chunk) // no CR, LF
      })
      response.on('end', function() {
         if (debug) console.log('JC: END response')
         callback()
      })
      response.on('close', function() { // when will this happen? for Web client?
         if (debug) console.log('JC: CLOSE response')
         callback()
      })
   })
   // tell request error
   request.on('error', function(e) {
      console.log('problem with POST request: ' + e.message);
   })
   // send POST data
   request.write(data)
   request.end() // fire 'end' event
   // end of doPost
}


// global constants
// these are initialized in Source.init()
var CMD, PROMPT   // command for child process and prompt
var KOPEN, KCLOSE // comment open and close markers
var KLINE         // for splitting comment within line
var KCMD          // for extracting comment word
var KPART         // for replacing comment parts in line
var BOPEN, BCLOSE // special bracket open and close markers

// global print
function print(s) {
  process.stdout.write(s)
}

// global quit
function quit(s) {
  if (s) console.log(s)
  var hour = (new Date()).getHours()
  console.log(hour < 12 ? 'Goodday' : hour > 20 ? 'Goodnight' : 'Goodbye')
  process.exit(0)
}

// flag to indicate if READY to respond to keystrokes
var ready = false

/*
In HOLDemo.js, this.init wraps a child process, and Shell.input = process.stdin
Now this is a client, child process is on remote server, so use a proxy Shell.
*/

// Shell = process wrapper (proxt)
var Shell = new function() {
  // tell the remote Server to start a process with cmd, and handle process.stdout
  this.init = function(cmd, prompt, size) {
    var command = JSON.stringify({cmd: cmd, prompt: prompt, size: size})
    // doPost(query, data, callback) callback on end of response.
    // doPost('/command/', command, Scheduler.act) // Scheduler.act has no interaction
    doPost('/command/', command, Scheduler.onReady) // Scheduler.onReady gives interaction
  }
  this.send = function(data) {
    // doPost(query, data, callback) callback on end of response.
    // doPost('/data/', data, Scheduler.act)  // Scheduler.act has no interaction
    doPost('/data/', data, Scheduler.onReady) // Scheduler.onReady gives interaction
  }
  // end the remote shell
  this.end = function() {
    step = false // to quit
    doPost('/control/', '$', quit) // will trigger exit
  }
}

// ANSI Color ESC code
var Color = {
  black:       '[0;30m',
  red:         '[0;31m',
  green:       '[0;32m', 
  brown:       '[0;33m', 
  blue:        '[0;34m',
  purple:      '[0;35m',
  cyan:        '[0;36m',
  lightGray:   '[0;37m',
  darkGray:    '[0;1;30m',
  lightRed:    '[0;1;31m',
  lightGreen:  '[0;1;32m',
  yellow:      '[0;1;33m',
  lightBlue:   '[0;1;34m',
  lightPurple: '[0;1;35m',
  lightCyan:   '[0;1;36m',
  white:       '[0;1;37m',
}
var ESC = String.fromCharCode(27)
var suffix = ESC + '[0m'

// end of line
var EOL = os.EOL

// String prototype extension

// paint text with color
String.prototype.paint = function(color) {
  return ESC + color + this + suffix
}
// remove comment
String.prototype.noComment = function() {
  // comment: [KOPEN][any space][anything][any space][KCLOSE] = KPART
  return this.replace(KPART, '')
}
// remove trailing spaces
String.prototype.noTrail = function() {
  return this.replace(/(\s+)$/, '')
}
// get last character
String.prototype.lastChar = function() {
  return this.slice(-1, this.length)
}
// escape every character
String.prototype.escape = function() {
  var s = this
  var buffer = []
  for (var j = 0; j < s.length; j++) buffer.push('\\' + s.charAt(j))
  return buffer.join('')
}

// Source = keeper of input chunks
var Source = new function() {
  // Source has a buffer
  var buffer = []
  // put lines to buffer
  this.put = function(data) {
    buffer.push(data)
  }
  // set the buffer
  this.set = function(lines) {
    buffer = lines
    if (debug) console.log('Buffer: ' + lines.length + ' lines.')
  }
  // get lines of text file (locally)
  this.getTextFileLocal = function(filename, callback) {
    var text = fs.readFileSync(filename, 'utf8')
    if (debug) console.log('Read: ' + text.length + ' bytes.')
    callback(text.split(EOL)) // can have trailing spaces of CR (\r)
  }
  // get lines of text file (via server, continuation-passing style)
  this.getTextFileRemote = function(filename, callback) {
    doGet(filename, function(text) {
       if (debug) console.log('Read: ' + text.length + ' bytes.')
       callback(text.split(EOL)) // can have trailing spaces of CR (\r)
    })
  }
  // local variables
  var quiet = false  // quiet check
  var level = 0      // level count for KOPEN/KCLOSE
  var blevel = 0     // level count for BOPEN/BCLOSE
  var c_color = Color.cyan    // comment color
  var d_color = Color.purple  // display color
  // make a line pretty using color
  function pretty(line) {
    // check in comment
    if (level > 0) return line.paint(c_color)
    // check unbalanced end-comment, i.e. just completes a block comment
    if (line.indexOf(KOPEN) == -1 && 
        line.indexOf(KCLOSE) !== -1) return line.paint(c_color)
    // Note: this only matches only one comment in the line 
    // match: [anything][KOPEN][something][KCLOSE][anything] = KLINE
    var match = line.match(KLINE)
    return match ?
           match[1].paint(d_color) + 
           (KOPEN + match[2] + KCLOSE).paint(c_color) +
           match[3].paint(d_color) :
           line.paint(d_color)
  }
  // process a line (check for command line)
  function process(line) {
    // match: [begin][KOPEN][space]![word][space][anything][KCLOSE][end] = KCMD
    var match = line.match(KCMD)
    if (!match) return line
    var word = match[1]
    if (debug) console.log('COMMAND: [' + line + '], word=' + word)
    // process the command word
    if (word == 'pause') if (!step && pause !== null) pause = true // keep null pause
    // when step = true, ignore pause.
    if (word == 'quit' || word == 'stop' ||
        word == 'exit' || word == 'bye') buffer = [] // empty buffer will end input
    return null // always return null for command
  }
  // check a trimmed line, return true if chunk is reached
  var isChunk = function(line) {
    // detect quiet mode
    if (line.indexOf('quietdec := true;') != -1) quiet = true
    if (line.indexOf('quietdec := false;') != -1) quiet = false
    // new detect of quiet mode
    if (line.indexOf('toggle_quietdec()') != -1) quiet = !quiet
    return !quiet && (level == 0) && (blevel == 0) && (line.lastChar() == ';')
  }
  // get a chunk from buffer, null if no chunk
  // Note: this line-based getChunk needs to be checked in test mode.
  function getChunk() {
    quiet = false
    level = 0
    var lines = []
    while (buffer.length > 0) {
      var line = buffer.shift().noTrail() // ensure no trailing spaces
      line = process(line) // check if line is special -- a command line
      // if a command line, returns a null
      if (line !== null) { // can be blank line, or '' empty line
        // count KOPEN and KCLOSE for balance
        if (line.indexOf(KOPEN) !== -1) level++
        if (level > 0 && line.indexOf(KCLOSE) !== -1) level--
        // count BOPEN and BCLOSE for balance
        if (line.indexOf(BOPEN) !== -1) blevel++
        if (blevel > 0 && line.indexOf(BCLOSE) !== -1) blevel--
        // should have an echo flag to determine if line is echoed.           
        print(pretty(line) + EOL) // show input (echo) before line is modified
        line = line.noComment().noTrail() // modify after show
        if (line) { // skip empty lines (e.g. comment in a line)
          lines.push(line) // store modified line
          if (isChunk(line)) return lines.join(EOL) + EOL // check modified line
        }  
      } 
      if (pause) return "doPause" // return anything not null 
    }
    if (debug) console.log('Last chunk: [' + lines.join(EOL) + EOL + ']')
    return null // no chunk
  }
  // action when 'ready' using send
  this.action = function(send) {
    // get a chunk from buffer and pass to input 
    var data = getChunk()
    if (data == null) {
      if (debug) console.log('end of buffer input -- Goodbye')
      print('\n') // or print('^Z\n')
      // tell Shell to end, then quit
      return Shell.end() // will quit
    }
    if (pause) return // no action (pause)
    if (debug) console.log('write: [' + data + ']')
    send(data) // Source just send data
    ready = false // wait for reply
  }
  // initialization -- check start line to extract globals
  this.init = function() {
    var platform = os.platform()  // get platform
    if (debug) console.log('platform=' + platform)
    var count = 5 // get !start or !mac or !win within first 5 lines
    var match = false
    while (!match && count > 0) {
      if (buffer.length == 0) return quit('No lines in Source buffer.')
      var line = buffer.shift().noTrail() // get line without trailing spaces
      // match: [begin][nonspaces][space]!start[anything][space][nonspace][end]
      match = line.match(/^([^s]+)\ \!start(.*)\ ([^s]+)$/)  // for general, e.g. Unix
            || (platform == 'darwin' &&
                line.match(/^([^s]+)\ \!mac(.*)\ ([^s]+)$/)) // for MacOS
            || (platform.indexOf('win') !== -1 &&
                line.match(/^([^s]+)\ \!win(.*)\ ([^s]+)$/)) // for Windows
      count--
    }
    if (!match) return quit('Source missing start comment.')
    KOPEN = match[1], KCLOSE = match[3]
    if (debug) console.log('KOPEN=[' + KOPEN + '], KCLOSE=[' + KCLOSE + ']')
    if (debug) console.log('COMMAND: [' + line + '], start args=' + match[2])
    // match: [space]"[cmd]"[space]"[prompt]"
    match = match[2].match(/^\ \"(.+)\"\ \"(.+)\"$/)        
    CMD = match ? match[1] : 'hol' // default to HOL
    PROMPT = (match ? match[2] : '> ') + '$' // default to PolyML prompt (a string)
    if (debug) console.log('CMD=[' + CMD + ']')
    if (debug) console.log('PROMPT=[' + PROMPT + ']')
    // match: [anything][KOPEN][something][KCLOSE][anything]
    KLINE = new RegExp('(.*)' + KOPEN.escape() + '(.+)' + KCLOSE.escape() + '(.*)')
    // match: [begin][KOPEN][space]![word][space][anything][KCLOSE][end]
    KCMD = new RegExp('^' + KOPEN.escape() + '\\ \\!([a-zA-z]+)\\ (.*)' + KCLOSE.escape() + '$')
    // match: [KOPEN][spaces][something][spaces][KCLOSE]
    KPART = new RegExp(KOPEN.escape() + '(\\s+)(.+)(\\s+)' + KCLOSE.escape(), 'g')
    if (debug) console.log('KLINE=[' + KLINE + ']')
    if (debug) console.log('KCMD=[' + KCMD + ']')
    if (debug) console.log('KPART=[' + KPART + ']')
/*
KOPEN=[(*], KCLOSE=[*)]
PROMPT=[> $]  (a string for client)
KLINE=[/(.*)\(\*(.+)\*\)(.*)/]
KCMD=[/^\(\*\ \!([a-zA-z]+)\ (.*)\*\)$/]
KPART=[/\(\*(\s+)(.+)(\s+)\*\)/g]
*/
    // Note: this is a quick fix only.
    // Todo: have a test mode (just emit chunks)
    // Todo: have special lines before !start to determine BOPEN, BCLOSE, TERMINATOR, quiet mode, etc.
    BOPEN = '<|'
    BCLOSE = '|>'
  }
}

// Scheduler = coordinator between Source and Shell
var Scheduler = new function() {
  // invoke action in Source with Shell input
  this.act = function() {
    // Shell.input is used only when data is ready with a chunk.
    // For client: when data is ready, doPOST to server.
    // Source.action(Shell.input) <-- JC: what is the input for Source.action?
    Source.action(Shell.send)
    if (pause) pause = false // keep null pause unchanged
  }
  // Scheduler handle 'ready' event -- now not used!
  this.onReady = function() {
    ready = true // reply complete
    if (step || pause) return // let keypress do ready action
    Scheduler.act()
  }
  // Scheduler initialization
  this.init = function() {
    // set process.stdin to raw for keystrokes
    process.stdin.setRawMode(true)
    // set encoding for keys
    process.stdin.setEncoding('utf8')
    // on any data into stdin
    process.stdin.on('data', function(key){
      if (key === '\u0004') { // CTRL-D (end of input)
        return quit('Abort')
      }
      if (key === '\u001a') { // CTRL-Z (end of stream)
        return quit('Done')
      }
      if (key === '\u0003') { // CTRL-C (end of text)
        pause = true // no action
        // tell Shell to end, then quit.
        return Shell.end()
      }
      if (key === 'j') { // j for noStep (jump)
        if (debug) console.log('j pressed.')
        step = false
      }
      if (key === 'q') { // q for noPause (quick)
        if (debug) console.log('q pressed.')
        step = false
        pause = null // a hard false
      }
      if (ready) Scheduler.act()
      // if (!ready) ignore key
      if (ready == null) console.log('Press CRTL-C to quit.')
    })
    // essential for Windows, optional for Mac
    process.stdin.resume()
  }
}

// wrap main function for continuation
function main(size) {
   // start line check by source
   Source.init()
   // start detecting keypress
   Scheduler.init()
   // start the shell with action
   if (debug) console.log('JC: tell Shell to init with: CMD=' + CMD + ', PROMPT=' + PROMPT)
   Shell.init(CMD, PROMPT, size)

   if (debug) console.log('Shell ready to START!')
}

// get demo file name and stdout buffer size from arguments
var NAME = args[3]
var SIZE = args[4]
if (NAME) {
   if (NAME == '-l') {
      NAME = args[4]
      SIZE = args[5]
      Source.getTextFileLocal(NAME, function(data) {
         Source.set(data)
         main(SIZE)
      })
   }
   else {
      Source.getTextFileRemote(NAME, function(data) {
         Source.set(data)
         main(SIZE)
      })
   }
}
else {
// prepare the Source buffer
Source.put('(* !mac "hol" "> " *)')
Source.put('(* !win "go hol" "- " *)')
Source.put('(* !start *)')
Source.put('(* Start of Demo *);')
Source.put('2 + 4*5; (* simple arithmetic *)')
Source.put('EVAL ``2 ** 100``; (* exact arithmetic *)')
Source.put('(* Theorem: 1 + 1 = 2 *)')
Source.put('(* Proof: by definition.')
Source.put('   Here is a nested comment; (* ok *)')
Source.put('*)')
Source.put('(* !pause *)')
Source.put('(* First, set a goal: *)')
Source.put('g `1 + 1 = 2`;')
Source.put('(* !pause *)')
Source.put('e (DECIDE_TAC);')
Source.put('(* capture the theorem *)')
Source.put('val one_add_one = save_thm("one_add_one", top_thm());')
Source.put('drop();')
Source.put('(* !pause *)')
Source.put('')
Source.put('val _ = HOL_Interactive.toggle_quietdec();')
Source.put('')
Source.put('(* open dependent theories *)')
Source.put('open pred_setTheory arithmeticTheory;')
Source.put('')
Source.put('val _ = HOL_Interactive.toggle_quietdec();')
Source.put('')
Source.put('ADD; (* Definition of ADD *)')
Source.put('(* !stop *)')
Source.put('(* !quit *)') // stop already
Source.put('"Hello nobody";') // will be ignored
main(SIZE)
}

/**
 * Documentation
 *
 * 1. Source holds the input lines.
 *    - Source initialization locates the start line, request Shell to call server to start child process.
 * 2. Shell is a proxy: sending doGet and doPost to server.
 * 3. Scheduler coordinates everything:
 *    - On 'ready', the Source acts by looking at its buffer.
 *    - Source reads lines from buffer, echoes as it reads.
 *    - Source ensures that a chunk (maybe multiple lines) will result in READY prompt.
 *    - Source sends the chunk to Shell, who pass the chunck to server.
 *    - The response callback will display server response (output from child process).
 *    - After response display, pass control to Scheduler.onReady, and the cycle repeats.
 * 4. Scheduler also handles step and pause, by keystrokes 'j' and 'q'.
 */