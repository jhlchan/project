
Example: Steps to run primes demo

Run HOL4 Demo in Client
=======================

0. Note where is the demo script.
~/work/hol/exercise/primes-demo.hol

1. Locate in script's directory, but run this DemoServer in NodeJs:
exercise$ node ../demo/DemoClient.js -h
exercise$ node ../demo/DemoClient.js localhost 8080 primes-demo.hol

Press SPACE BAR, if the following appears:
<<HOL message: link_parents: the following parents of "primes"
  should already be in the theory graph (but aren't): ("logroot",1442379315,385186), ("divides",1442379327,866108), ("rich_list",1442379413,667621), ("quantHeuristics",1442379462,826764)>>

Use CTRL-C to quit.
Do a Holmake.

Restart the DEMO client by the same command.

Only HOL4 banner is shown.

Press SPACE BAR to start

If all goes well, press SPACE BAR to keep going.

SPACE BAR -- keep going
KEY j     -- jump (that is, skip to next pause)
KEY q     -- quit (that is, skip until the end)

Run HOL4 Demo in Browser
========================

0. Note where is the demo script.
~/work/hol/exercise/primes-demo.hol

While there, better test the demo script can run by DemoClient first (see above).

Or just test by starting hol, then
> use "primes-demo.hol";

1. Locate in script's directory, but run this DemoServer in NodeJs:
exercise$ node ../demo/DemoServer.js -h
exercise$ node ../demo/DemoServer.js

A message is shown:
PORT=8080
root:  /Users/josephchan/work/hol/demo
 cwd:  /Users/josephchan/work/hol/exercise
Demo Server Running on port 8080, stop by CTRL-C.

2. Lauch a browser, open url: http://localhost:8080/Demo.html?primes-demo.hol

Only HOL4 banner is shown.

Press SPACE BAR to start talking to Server
Press SPACE BAR to start running demo script

If all goes well, press SPACE BAR to keep going.

SPACE BAR -- keep going
KEY j     -- jump (that is, skip to next pause)
KEY q     -- quit (that is, skip until the end)

Some browsers (e.g. Safari) has File > Export as PDF.
Some browsers (e.g. QtWeb) has File > Save as PDF.
If the system Print supports Save as PDF, just use browser print.

Files supporting HOL4 Demo
==========================
DemoServer.js   Server Engine to start HOL4 in the background, and interacts with clients.
DemoClient.js   A command based DEMO client to talk to DemoServer
Demo.js         The Javascript for Demo.html to talk to DemoServer
Demo.html       The HTML console for Demo.js for input and display

HOLdemo.js      NodeJs Server with its own NodeJs client
              = Run HOL in demo mode (a pure NodeJs solution)

Old versions
ShellServer.js  Server Engine
ShellClient.js  Command line client for ShellServer
Shell.js        Javascript client for ShellServer
Shell.html      Using Shell.js to talk to ShellServer
