/*
 * ShellClient.js
 *
 * This is the client program for Shell Server (ShellServer.js)
 *
 * Example of use (assume Shell Server is running):
 * $ node ShellClient.js localhost 8080     (for MacOS)
 *
 * Note:
 * JSON is built into V8.
 * http://code.google.com/p/v8/source/browse/trunk/src/json.js
 */
var http = require("http")

// var debug = true // true or false
var debug = false
var echo = true //echo for input
// check args
var args = process.argv.slice(2)
if (args[0] == '-h') {
   console.log('Usage: node ShellClient.js [host] [port]')
   process.exit()
}
var HOST = args[0] || 'localhost' // default localhost
var PORT = args[1] || 80 // default 80
var UID = (new Date()).getTime() // unique identifier of this client
console.log('HOST: ' + HOST)
console.log('PORT: ' + PORT)
console.log('UID:' + UID)

// set up POST option with query
function doPost(query, data, callback) {
   var options = {
      host: HOST,
      port: PORT,
      path: query,
      // query in path
      headers: {
         'content-type': 'text',
         'content-length': data.length,
         'client-uid': UID
      },
      method: 'POST'
   }
   if (debug) console.log('doPost: ' + query + '' + data)
   if (debug) console.log('doPost: header=' + JSON.stringify(options.headers))

   // send POST request, with response callback
   var request = http.request(options, function (response) {
      if (debug) console.log('status: ' + response.statusCode)
      if (debug) console.log('headers: ' + JSON.stringify(response.headers))
      response.setEncoding('UTF8')
      response.on('data', function (chunk) {
         console.log('body: ' + chunk)
         if (chunk == '!@#$') console.log('No more input.')
      })
      response.on('end', function () {
         if (debug) console.log('JC: END response')
         callback()
      })
      response.on('close', function () { // when will this happen? for Web client?
         if (debug) console.log('JC: CLOSE response')
         callback()
      })
   })
   // tell request error
   request.on('error', function (e) {
      console.log('problem with request: ' + e.message);
   })
   // send POST data
   request.write(data)
   request.end() // fire 'end' event
   // end of doPost
}

// Devise a Schedule driven by doPost
var Schedule = function (callback) {
   // internal buffer
   var buffer = []
   this.put = function (query, data) {
      buffer.push({
         query: query,
         data: data
      })
   }
   var self = this
   // Schedule action loop
   this.action = function () {
      if (debug) console.log('JC: Action')
      if (buffer.length == 0) {
         if (debug) console.log('JC: done!')
         return callback()
      }
      if (debug) console.log('JC: doPost!')
      // pop buffer and doPost
      var act = buffer.shift() // get items like queue
      var query = act.query
      var data = act.data
      // echo data for command
      if (query == '/rpc/command/') if (echo) process.stdout.write('send: ' + data)
      doPost(query, data, self.action) // allow doPost to continue action
   }
}

function main() {
// Put all requests in schedule buffer
var schedule = new Schedule(process.exit) // exit when done
// Note: response 'end' is fired from server on PROMPT detection.
//   JC: PROMPT detection works for hol, not poly -- maybe prompt in stderr??
//schedule.put('/rpc/shell/', 'poly')
schedule.put('/rpc/shell/', 'hol')
//schedule.put('/rpc/shell/', '') // use default shell
// must have \n to get PROMPT back for hol
schedule.put('/rpc/command/', '2 + 3*5;\n')
schedule.put('/rpc/command/', 'IntInf.pow(2,10);\n')
schedule.put('/rpc/command/', 'IntInf.pow(2,100);\n')
schedule.put('/rpc/control/', '$')
// start schedule action, exit when done.
schedule.action()
// program exit when action consumes all lines in buffer.
}

main()

// Collect input lines into chunks
/*
  // NodeJs

  var fs = require("fs");
  var os = process.platform; // 'win32' or 'linux', or 'darwin' for MacOS
  var isWin = os.indexOf('win') === 0;
  console.log('os: ' + os)
  console.log('isWin: ' + isWin)
  
  var stdin = isWin ? process.stdin : null; // cannot touch process.stdin if using /dev/stdin
  var stdout = process.stdout;
  var stderr = process.stderr;

  var buffer = new Buffer(1000); // buffer for readSync
  var fd = isWin ? process.stdin.fd : fs.openSync("/dev/stdin", "rs");
  
  function readLine() {
    try {
        var bytes = fs.readSync(fd, buffer, 0, buffer.length)
        if (bytes === 0) return null // EOF for: node mulisp < input
        return buffer.slice(0, bytes).toString('utf8');
    }
    catch (e) {
      // EOF for: type input | node mulisp.js
      // but not: node mulisp.js < input
      if (e.code === 'EOF') return null;
      throw e; 
    }
  }
*/

// a simple REPL based on NodeJs readline
var repl = require('readline').createInterface(process.stdin, process.stdout)

repl.setPrompt('> ')
repl.prompt() // show prompt and wait for input line
// emits 'line' event on input of a line (by ENTER)
repl.on('line', function(line) {
  doSomething(line)
  repl.prompt() // wait for next line
}).on('close', function() { // detects CTRL-C
  console.log('Have a great day!')
  repl.close()
})

var Action = new function() {

   // internal multiline buffer for a chunk
   var buffer = []
   
   this.put = function(line) {
      buffer.push(line)
      if (line.match(/;$/)) {
         console.log('a Chunk is ready!')
      }
   }
}

// line processing
function doSomething(line) {
  Action.put(line)
  console.log('< ' + eval(line))
}

/**
 * Documentation
 *
 * 1. Client issue the following commands via POST request:
 *    query: /rpc/shell/     data: initialize Laucncher by setting SHELL variable (e.g. "go hol").
 *    query: /rpc/command/   data: will be input data sent to Launcher.
 *    query: /rpc/control/   data: will be special command (e.g. interrupts, exit, or CRTL-chars.
 * 2. For /rpc/command/, the response ends when the PROMPT is detected from Launcher.
 *    Therefore, the client must ensure input data will cause the PROMPT to appear,
 *    e.g. detecting the end-of-statement character, perhaps a semicolon.
 * 3. Client can use GET to retrieve resources (e.g. text, graphics) relative to server directory.
 * 4. If server ends session, server sends to client (response): '!@#$' (4 top-keys).
 *    If client ends session, client sends to server  (request): CTRL-$ (simulated).
 */