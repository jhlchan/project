/*
 * Demo.js -- for Demo in Browser
 *
 * This uses the following idea:
 * 1. Input is POSTed to server, i.e send via POST request
 * 2. Server response is displayed
 * 3. doLoad GETs file content from server, i.e send via GET request
 * 4. Server response is wrapped in an Auto Source
 * 5. Inputs are taken from Source (either Console or Auto), in chunks.
 *    This is to ensure there must a server response for each chunk.
 *
 * Use of response callback, so no need to keep asking for server response.
 * See documentation of objects at the end.
 *
 * Example:
 * ~/work/hol/demo$ node DemoServer.js 8080
 * browser URL: http://localhost:8080/Demo.html?demo.hol
 *
 * ~/work/hol/exercise$ node ../demo/DemoServer.js
 * http://localhost:8080/Demo.html?primes-demo.hol
 * http://192.168.1.101:8080/Demo.html?primes-demo.hol
 *
 * Make this a recursion:
 * Client gather a chunk (ending in ;), send to server as request, callback is response.
 * Server response is display in browser (pretty-printed), callback is client send.
 * Also, this version has a simulated server on the client,
 *       and when SHELL is unspecified, server simulates by Javascript.
 * features: can specify load file in URL
 * URL: http://localhost/Demo.html?petersen.hol
 *
 * For logging: just highlight and copy the web display.
 */

// global variables
var _in, // the input
_out, // the output
_info // the info
var simulate = false // true or false
// simulate true URL: file:///G:/jc/www/js/nodejs/work/Demo.html
// simulate false URL: http://localhost/Demo.html
//var debug = true // true or false
var debug = false // true or false
var LF = '\n'
var TAB = '    ' // 4 spaces for tab
var second = 1000 // to milliseconds
var minute = 60 * second
var wait = 0 // in milliseconds
var delay = 0 // in milliseconds
// var PROMPT = /-\ $/  // to match MOSML or HOL prompt
var PROMPT = />\ $/ // to match PolyML HOL prompt
// Note: quiet mode has no PROMPT, so check this pair:
// val _ = quietdec := true;
// val _ = quietdec := false;
// initialization (called by HTML <body onload>)


function init() {
   _in = document.getElementById("input")
   _out = document.getElementById("output")
   _info = document.getElementById("info")
   Input.clear()
   Input.refocus()
   // Command.help(10 * second) // suggest doHelp for 10 seconds.
   // check if URL encodes the script to load
   var search = document.location.search
   if (search) { // do (* !load script *)
      var script = unescape(search.slice(1)) // skip '?'
      RPC.getFile(script) // callback will do Batch.run
      RPC.showInfo('load script=[' + script + ']')
   }
   else {
      Printer.printAlert('Type: doLoad [hol-script] to run demo of [hol-script]' + LF)
   }
}

// keep Textbox in focus (called by <html onclick>)


function keepFocusInTextbox(e) {
   var g = e.srcElement ? e.srcElement : e.target // IE vs. standard
   while (!g.tagName) g = g.parentNode
   var t = g.tagName.toLowerCase() // examine the tag
   if (t == "a" || t == "input") return; // clicking a link, or typing input: no focus
   // if selecting something, no focus
   if (window.getSelection) { // Mozilla
      if (String(window.getSelection())) return
   }
   else if (document.getSelection) { // Opera? Netscape 4?
      if (document.getSelection()) return
   }
   else { // IE
      if (document.selection.createRange().text) return
   }
   Input.refocus() // must focus
}

/*
// String prototype extension
String.prototype.noComment = function () {
   // comment: (*[any space][anything][any space]*)
   return this.replace(/\(\*(\s+)(.+)(\s+)\*\)/g, '')
}
String.prototype.noTrail = function () {
   return this.replace(/(\s+)$/, '')
}
String.prototype.lastChar = function () {
   return this.slice(-1, this.length)
}
*/

/**
 * Emitter is used in the following way:
 * For console input (browser)
 * -- Each ENTER key leads to emitter.emit('data', line)
 * -- The 'data' event will put the line in buffer
 * -- and check if a chunk has been accumulated.
 * -- If yes, do emitter.emit('send')
 * This is achieved by Console.doChunk() doing nothing.
 *
 * For file input (batch)
 * -- All lines are in a Queue
 * -- [@] lines are taken from Queue and call emitter.isChunk(line)
 * -- which is the function under 'data' event: put the line in buffer
 * -- and check if a chunk has been accumulated.
 * -- If yes, do emitter.emit('send'), return a null line
 * -- If line is null, chunk is sent, wait for reply, otherwise repeat [@]
 * This is achieved by Auto.doChunk() doing everything.
 */
/* 
// A simple Event Emitter
var SimpleEE = function () {
   this.events = {}
   // with an internal buffer
   var buffer = []
   // operations on buffer
   this.put = function (line) {
      buffer.push(line)
   }
   this.getAll = function () {
      return buffer.join('\n')
   }
   this.clear = function () {
      buffer = []
   }
   this.quiet = false // quiet mode
   this.level = 0 // comment level
}

SimpleEE.prototype.on = function (eventname, callback) {
   // initialize this.events[eventname] if necessary
   this.events[eventname] || (this.events[eventname] = [])
   this.events[eventname].push(callback)
}

SimpleEE.prototype.emit = function (eventname) {
   var args = Array.prototype.slice.call(arguments, 1)
   if (this.events[eventname]) {
      // IE does not support forEach nor apply
      if (false) {
       this.events[eventname].forEach(function(callback) {
       callback.apply(this, args)
       })
      }
      for (var j in this.events[eventname]) {
         var callback = this.events[eventname][j]
         callback(args.toString()) // for line
      }
   }
}

// init for chunk checks
SimpleEE.prototype.initChunk = function () {
   this.quiet = false
   this.level = 0
}
// put a line, return true if chunk is reached
SimpleEE.prototype.isChunk = function (line) {
   // detect quiet mode
   if (line.indexOf('quietdec := true;') != -1) this.quiet = true
   if (line.indexOf('quietdec := false;') != -1) this.quiet = false
   // new detect of quiet mode
   if (line.indexOf('toggle_quietdec()') != -1) this.quiet = !this.quiet
   // detect comment level
   if (line.indexOf('(* ') != -1) this.level++
   if (line.indexOf(' *)') != -1) this.level--
   // let Action process the line
   line = Action.process(line) // process line
   // put line in buffer, if a chunk is detected do send
   if (line) { // not (null or '')
      //Printer.printInput(line + LF) // echo line
      Printer.prettyPrint(line + LF) // echo line 
      // line is echoed, trim line to store in buffer, ready to send
      this.put(line.noComment().noTrail())
      if (!this.quiet && (this.level == 0) && line.noComment().noTrail().lastChar() == ';') {
         this.emit('send')
         return true
      }
   }
   return line == null // false for ''
}
*/

/*
// Emitter to get chunks of input
// Need chunk of input (ending in ;) to get server response
// Just a line may or may not get server response
var emitter = new SimpleEE()

emitter.on('greet', function (name) {
   Printer.printInfo('Hello, ' + name + '!\n')
})

// taken from Command.put(line)
emitter.on('data', function (line) {
   emitter.isChunk(line)
})

// send a chunk for processing
emitter.on('send', function () {
   var chunk = emitter.getAll() // get all from buffer
   emitter.clear() // clear buffer
   RPC.sendCommand(chunk)
})

// end of console input
emitter.on('end', function () {
   Action.next() // skip console input
})
*/

/*
// an auto source
var Auto = function (name, lines) {
   this.name = name
   // internal queue of lines
   var queue = lines

   this.end = function () {
      return queue.length == 0
   }
   this.doChunk = function () {
      // emitter has buffer and hasChunk checks
      emitter.initChunk()
      // empty while body must be ; or {}
      while (!emitter.isChunk(queue.shift())) {}
      return true
   }
}
*/

/*
// default source
var Console = {
   name: 'console',
   end: function () {
      return false
   },
   // in batch, AtEndOfStream
   doChunk: function () {
      return true
   } // a function to do a chunk from input
}

// These are used once only, as the Queue is mutable.
// For actual files, this will be OK
Auto['abc'] = new Auto('abc', ['2 + 3 * 4;', '3 + 4 * 5;', '(* !auto xyz *)', '(4 + 6)', '* (7 + 3);'])

Auto['xyz'] = new Auto('xyz', ['1 + 3 + 6 + 10; (* triangular numbers *)', '1 * 2 * 3 * 4 * 5; (* factorial *)', '(* !pause *)', '1 + 2 + 4 +', '8 + 16; (* sum of powers *)'])
*/

/*
// *** Action -- to parse and act on special lines
var Action = new function () {

   var source = [] // input sources
   // use unshift for stack push, so that peek is [0]
   source.unshift(Console) // default source
   var console = true // true if console input
   var pause = true // true or false
   var timer // for setTimeout
   // set pause to true
   this.doPause = function () {
      pause = true
   }
   // set pause to false
   this.noPause = function () {
      pause = false
   }
   // get value of pause
   this.Pause = function () {
      return pause
   }

   // continue action (one location to setTimeout)
   this.more = function () {
      clearTimeout(timer)
      timer = setTimeout(Action.doChunk, delay > 0 ? delay : wait)
      if (delay > 0) {
         setTimeout(function () {
            RPC.showInfo()
         }, delay)
         delay = 0 // delay is once only
      }
   }

   // a chunk must end by semicolon, true = success
   this.doChunk = function () {
      if (source.length == 0) return false
      var input = source[0] // do a peek
      if (debug) Printer.printInfo('doChunk from: ' + input.name + '\n')
      if (!input.end()) return input.doChunk()
      Action.next()
   }

   // next input
   this.next = function () {
      if (source.length == 0) return false
      var input = source[0] // do a peek
      console = input.name == 'console'
      if (console) if (debug) Printer.printInfo('End of ' + input.name + LF)
      if (!console) Printer.printInfo('End of ' + input.name + LF)
      source.shift() // discard input
      input = source[0] // do a peek
      console = input.name == 'console'
      if (!console) if (debug) Printer.printInfo('Resume input from ' + input.name + LF)
      Action.more() // send another chunk
   }

   // run in batch mode
   this.batch = function (name, response) {
      if (response.match(/^404 Not Found/)) {
         Printer.printError('Not found: ' + name + LF)
      }
      else {
         // split either by \n, or \r\n 
         var lines = (response.indexOf('\r') == -1) ? response.split(LF) : response.split('\r\n')
         // remove starting empty line
         var line = ''
         while (line == '' && lines.length > 0) line = lines.pop()
         if (lines == '') {
            Printer.printInfo('Empty file: ' + name + LF)
            return
         }
         lines.push(line) // put back the non-empty line
         RPC.showInfo('Number of lines: ' + lines.length)
         source.unshift(new Auto(name, lines))
      }
      Action.more() // kick off or resume
   }

   // parse for special line processing
   this.process = function (line) {
      if (debug) Printer.printInfo('Raw Line: ' + line)
      // convert old style: doXXX to new style: (* !xxx *)
      if (line.match(/^cls/) || line.match(/^do(\w+)/) || line.match(/^no(\w+)/)) {
         line = Command.run(line)
      }
      // pattern: (* !xxx yyy  *) check start
      var match = line.match(/^\(\* \!(\w+)(\s+)([\w.*\"]+)/)
      if (!match) return line
      // match.length == 4 due to pattern
      var key = match[1]
      var value = match[3]
      if (key == 'auto') {
         if (debug) Printer.printInfo('auto-run lines from TextBuffer.\n')
         source.unshift(Auto[value])
         Printer.printAlert('auto ' + value + ' added to Source.\n')
         Action.more() // kick off
         return null // for chunk
      }
      if (key == 'pause' || key == 'Pause') {
         if (pause) {
            value = getArg(line)
            if (value == '') value = 'Continue by SPACE bar' // (* !pause *)
            if (debug) Printer.printInfo('Switch to console input, value=[' + value + ']' + LF)
            Printer.printAlert(' Paused. ' + value + LF)
            source.unshift(Console)
            Action.more() // wait for console action
            return null // for chunk when pause
         }
         return '' // not chunk when nopause
      }
      if (key == 'say' || key == 'Say') {
         // either (* !say "...." *) or (* !say ..... *)
         value = getArg(line)
         Printer.printAlert(value + LF)
         return '' // not chunk when nopause
      }
      if (key == 'done') { // to force end-of-input (e.g. no ^X)
         Action.next()
         return '' // not chunk
      }
      if (key == 'jump' || key == 'nopause') { // to ignore all pause
         Action.noPause()
         if (debug) Printer.printInfo('No pause.' + LF)
         RPC.showInfo()
         return '' // not chunk
      }
      if (key == 'dopause') { // honour pause
         Action.doPause()
         if (debug) Printer.printInfo('With pause.' + LF)
         RPC.showInfo()
         return '' // not chunk
      }
      if (key == 'load') {
         if (debug) Printer.printInfo('Switch input to ' + value + LF)
         value = value.replace(/\"/g, '') // unquote, balance "
         RPC.getFile(value) // callback will do Action.batch
         return null // for chunk
      }
      if (key == 'noload') { // Console mode, do !done twice
         if (source.length > 0) source.shift() // discard console input
         Action.next() // discard this text input, onward to previous one
         return '' // not chunk
      }
      if (key == 'wait' || key == 'delay') {
         if (value.match(/^[\d]/)) { // check leading digit
            value = parseInt(eval(value))
            if (key == 'wait') if (value != NaN) {
               wait = value
               RPC.showInfo()
               if (debug) Printer.printInfo('Change wait to ' + wait + ' ms.' + LF)
            }
            if (key == 'delay') if (value != NaN) { // sleep(value) = change of wait just for once
               delay = value
               RPC.showInfo()
               if (debug) Printer.printInfo('Change delay to ' + delay + ' ms.' + LF)
            }
         }
         else Printer.printError('Line ignored: + ' + line + LF) 
         return '' // not chunk
      }
      if (key == 'PAUSE' || key == 'SAY') {
         // ignore, for non-browser version
         return '' // not chunk
      }
      if (key == 'shell') {
         // (* !shell "...." *) or (* !shell .... *) for browser mode
         value = getArg(line)
         if (value == '') return '' // ignore, not chunk
         // pass value to server over CTRL channel
         RPC.sendDemo(value)
         return null // for chunk
      }
      if (key == 'quit') {
         RPC.sendControl('$') // signal for exit
         return null // for chunk
      }
      if (key == 'cls') {
         shellCommands.cls()
         return '' // not chunk
      }
      if (key == 'help') {
         Command.doHelp()
         return '' // not chunk
      }
      if (key == 'start') {
         // hack to start server
         RPC.sendDemo('')
         return null // for chunk
      }
      // unknown, keep the line
      //return line
      Printer.printError('Unknown command: ' + line + '<br>')
      return '' // not chunk
   }

   // to get arg of (* !xxx *), (* !xxx "..." *), or (* !xxx ... *) from match
   function getArg(line) {
      var match = line.match(/^\(\* \!(\w+)(\s+)([\w.*\"]+)/)
      var value = match[3]
      if (value == '*') return '' // (* !xxx *)
      // (* !xxx "..." *) or (* !xxx ... *)
      match = line.match(/^\(\* \!(\w+)(\s+)\"([^\"]+)\"/)
      if (!match) match = line.match(/^\(\* \!(\w+)(\s+)([^\"]+)(\s+)\*\)$/)
      value = match[3]
      return value
   }
}
*/

/* ----------- New Part, to be similar to DemoClient.js ------------ */

var step = true
// var step = false // true or false
var pause = false // start with false
// var pause = true

// global constants
// these are initialized in Source.init()
var CMD, PROMPT   // command for child process and prompt
var KOPEN, KCLOSE // comment open and close markers
var KLINE         // for splitting comment within line
var KCMD          // for extracting comment word
var KPART         // for replacing comment parts in line
var BOPEN, BCLOSE // special bracket open and close markers

// global print
function print(s) {
  Printer.printOutput(s)
}
// * change if (debug) print back to console.print, either use a static object console, or browser's console.
// * Check if server release shells on quit (yes)

// global quit
function quit(s) {
  if (s) print(s)
  var hour = (new Date()).getHours()
  print(hour < 12 ? 'Goodday' : hour > 20 ? 'Goodnight' : 'Goodbye')
  //process.exit(0)
}

// flag to indicate if READY to respond to keystrokes
var ready = false

/*
In HOLDemo.js, this.init wraps a child process, and Shell.input = process.stdin
Now a client, only a remote child process on server, so use a dummy Shell.
*/

// Shell = process wrapper (dummy)
var Shell = new function() {
  // tell the remote Server to start a process with cmd, and handle process.stdout
  this.init = function(cmd, prompt, size) {
    // doPost(query, data, callback) callback on end of response.
    RPC.doPost('/command/', 
               JSON.stringify({cmd: cmd, prompt: prompt, size: size}),
               Shell.ready /* Scheduler.act */)
/*
    // handle process 'exit' event
    process.on('exit', function(code) {
      if (debug) print('Child process ends with exit code ' + code)
      ready = null // no reply
      if (!step) quit() // end program.
    })
*/
  }
  // Shell ready after init
  this.ready = function() {
    pause = true // after start from false
    step = false // simulate key j = jump, but need a dummy SPACE press
    Scheduler.onReady()
  }
  // Shell send data
  this.send = function(data) {
    // doPost(query, data, callback) callback on end of response.
    RPC.doPost('/data/', data, Scheduler.onReady /* Scheduler.act */)
  }
  // end the remote shell
  this.end = function() {
    step = false // to quit
    RPC.doPost('/control/', '$', Shell.exit /* quit */)
    // Shell.input.end() // will trigger exit
  }
  // exit the shell
  this.exit = function() {
    // quit()
    RPC.exit()
  }
}

// ANSI Color ESC code
var Color = {
  black:       '[0;30m',
  red:         '[0;31m',
  green:       '[0;32m', 
  brown:       '[0;33m', 
  blue:        '[0;34m',
  purple:      '[0;35m',
  cyan:        '[0;36m',
  lightGray:   '[0;37m',
  darkGray:    '[0;1;30m',
  lightRed:    '[0;1;31m',
  lightGreen:  '[0;1;32m',
  yellow:      '[0;1;33m',
  lightBlue:   '[0;1;34m',
  lightPurple: '[0;1;35m',
  lightCyan:   '[0;1;36m',
  white:       '[0;1;37m',
}
var ESC = String.fromCharCode(27)
var suffix = ESC + '[0m'

// end of line
//var EOL = os.EOL
var EOL = '\n'

// String prototype extension
var c_color = Color.cyan    // comment color
var d_color = Color.purple  // display color

// paint text with color
String.prototype.paint = function(color) {
  // return ESC + color + this + suffix
  // JC: or detect color and do something else
  return this // for now, use Printer.prettyPrint instead
}
// remove comment
String.prototype.noComment = function() {
  // comment: [KOPEN][any space][anything][any space][KCLOSE] = KPART
  return this.replace(KPART, '')
}
// remove trailing spaces
String.prototype.noTrail = function() {
  return this.replace(/(\s+)$/, '')
}
// get last character
String.prototype.lastChar = function() {
  return this.slice(-1, this.length)
}
// escape every character
String.prototype.escape = function() {
  var s = this
  var buffer = []
  for (var j = 0; j < s.length; j++) buffer.push('\\' + s.charAt(j))
  return buffer.join('')
}

// Source = keeper of input chunks
var Source = new function() {
  // Source has a buffer
  var buffer = []
  // put lines to buffer
  this.put = function(data) {
    buffer.push(data)
  }
  // set the buffer
  this.set = function(lines) {
    buffer = lines
    if (debug) print('Buffer: ' + lines.length + ' lines.')
  }
  // get lines of text file
  this.getTextFile = function(filename) {
    var text = fs.readFileSync(filename, 'utf8')
    if (debug) print('Read: ' + text.length + ' bytes.')
    return text.split(EOL) // can have trailing spaces of CR (\r)
  }
  // local variables
  var quiet = false  // quiet check
  var level = 0      // level count for KOPEN/KCLOSE
  var blevel = 0     // level count for BOPEN/BCLOSE
  //var c_color = Color.cyan    // comment color JC: made global
  //var d_color = Color.purple  // display color JC: made global
  // make a line pretty using color
  function zpretty(line) { return line }
  function pretty(line) { // probably need a HTML pretty
    // check in comment
    if (level > 0) return line.paint(c_color)
    // check unbalanced end-comment, i.e. just completes a block comment
    if (line.indexOf(KOPEN) == -1 && 
        line.indexOf(KCLOSE) !== -1) return line.paint(c_color)
    // Note: this only matches only one comment in the line 
    // match: [anything][KOPEN][something][KCLOSE][anything] = KLINE
    var match = line.match(KLINE)
    return match ?
           match[1].paint(d_color) + 
           (KOPEN + match[2] + KCLOSE).paint(c_color) +
           match[3].paint(d_color) :
           line.paint(d_color)
  }
  // process a line (check for command line)
  function process(line) {
    // match: [begin][KOPEN][space]![word][space][anything][KCLOSE][end] = KCMD
    var match = line.match(KCMD)
    if (!match) return line
    var word = match[1]
    if (debug) print('COMMAND: [' + line + '], word=' + word)
    // process the command word
    if (debug) if (word == 'pause') {
      print('JC: pause detected, step=' + step + ', pause =' + pause + ', test=' + (!step && pause !== null) + EOL)
    }
    if (word == 'pause') if (!step && pause !== null) pause = true // keep null pause
    // when step = true, ignore pause.
    if (word == 'quit' || word == 'stop' ||
        word == 'exit' || word == 'bye') buffer = [] // empty buffer will end input
    return null // always return null for command
  }
  // check a trimmed line, return true if chunk is reached
  var isChunk = function(line) {
    // detect quiet mode
    if (line.indexOf('quietdec := true;') != -1) quiet = true
    if (line.indexOf('quietdec := false;') != -1) quiet = false
    // new detect of quiet mode
    if (line.indexOf('toggle_quietdec()') != -1) quiet = !quiet
    return !quiet && (level == 0) && (blevel == 0) && (line.lastChar() == ';')
  }
  // get a chunk from buffer, null if no chunk
  // Note: this line-based getChunk needs to be checked in test mode.
  function getChunk() {
    quiet = false
    level = 0
    var lines = []
    while (buffer.length > 0) {
      var line = buffer.shift().noTrail() // ensure no trailing spaces
      line = process(line) // check if line is special -- a command line
      // if a command line, returns a null
      if (line !== null) { // can be blank line, or '' empty line
        // count KOPEN and KCLOSE for balance
        if (line.indexOf(KOPEN) !== -1) level++
        if (level > 0 && line.indexOf(KCLOSE) !== -1) level--
        // count BOPEN and BCLOSE for balance
        if (line.indexOf(BOPEN) !== -1) blevel++
        if (blevel > 0 && line.indexOf(BCLOSE) !== -1) blevel--
        // should have an echo flag to determine if line is echoed.           
        // print(pretty(line) + EOL) // show input (echo) before line is modified
        Printer.prettyPrint(line + EOL) // show input (echo) before line is modified
        line = line.noComment().noTrail() // modify after show
        if (line) { // skip empty lines (e.g. comment in a line)
          lines.push(line) // store modified line
          if (isChunk(line)) return lines.join(EOL) + EOL // check modified line
        }  
      } 
      if (pause) return "doPause" // return anything not null 
    }
    if (debug) print('Last chunk: [' + lines.join(EOL) + EOL + ']')
    return null // no chunk
  }
  // action when 'ready' using send
  this.action = function(send) {
    // get a chunk from buffer and pass to input 
    var data = getChunk()
    if (data == null) {
      if (debug) print('end of buffer input -- Goodbye')
      print('\n') // or print('^Z\n')
      // tell Shell to end, then quit
      return Shell.end() // will quit
    }
    if (pause) return // no action (pause)
    if (debug) print('write: [' + data + ']')
    // input.write(data)
    send(data)
    ready = false // wait for reply
  }
  // initialization -- check start line to extract globals
  this.init = function() {
    //var platform = os.platform()  // get platform
    var platform = 'darwin'
    if (debug) print('platform=' + platform)
    var count = 5 // get !start or !mac or !win within first 5 lines
    var match = false
    while (!match && count > 0) {
      if (buffer.length == 0) return quit('No lines in Source buffer.')
      var line = buffer.shift().noTrail() // get line without trailing spaces
      // match: [begin][nonspaces][space]!start[anything][space][nonspace][end]
      match = line.match(/^([^s]+)\ \!start(.*)\ ([^s]+)$/)  // for general, e.g. Unix
            || (platform == 'darwin' &&
                line.match(/^([^s]+)\ \!mac(.*)\ ([^s]+)$/)) // for MacOS
            || (platform.indexOf('win') !== -1 &&
                line.match(/^([^s]+)\ \!win(.*)\ ([^s]+)$/)) // for Windows
      count--
    }
    if (!match && count == 0) return quit('Source missing start comment.')
    KOPEN = match[1], KCLOSE = match[3]
    if (debug) print('KOPEN=[' + KOPEN + '], KCLOSE=[' + KCLOSE + ']')
    if (debug) print('COMMAND: [' + line + '], start args=' + match[2])
    // match: [space]"[cmd]"[space]"[prompt]"
    match = match[2].match(/^\ \"(.+)\"\ \"(.+)\"$/)        
    CMD = match ? match[1] : 'hol' // default to HOL
    PROMPT = (match ? match[2] : '> ') + '$' // default to PolyML prompt (a string)
    if (debug) print('CMD=[' + CMD + ']')
    if (debug) print('PROMPT=[' + PROMPT + ']')
    // match: [anything][KOPEN][something][KCLOSE][anything]
    KLINE = new RegExp('(.*)' + KOPEN.escape() + '(.+)' + KCLOSE.escape() + '(.*)')
    // match: [begin][KOPEN][space]![word][space][anything][KCLOSE][end]
    KCMD = new RegExp('^' + KOPEN.escape() + '\\ \\!([a-zA-z]+)\\ (.*)' + KCLOSE.escape() + '$')
    // match: [KOPEN][spaces][something][spaces][KCLOSE]
    KPART = new RegExp(KOPEN.escape() + '(\\s+)(.+)(\\s+)' + KCLOSE.escape(), 'g')
    if (debug) print('KLINE=[' + KLINE + ']')
    if (debug) print('KCMD=[' + KCMD + ']')
    if (debug) print('KPART=[' + KPART + ']')
/*
KOPEN=[(*], KCLOSE=[*)]
PROMPT=[> $]  (a string for client)
KLINE=[/(.*)\(\*(.+)\*\)(.*)/]
KCMD=[/^\(\*\ \!([a-zA-z]+)\ (.*)\*\)$/]
KPART=[/\(\*(\s+)(.+)(\s+)\*\)/g]
*/
    // Note: this is a quick fix only.
    // Todo: have a test mode (just emit chunks)
    // Todo: have special lines before !start to determine BOPEN, BCLOSE, TERMINATOR, quiet mode, etc.
    BOPEN = '<|'
    BCLOSE = '|>'
  }
}

// Scheduler = coordinator between Source and Shell
var Scheduler = new function() {
  // invoke action in Source with Shell input
  this.act = function() {
    // Shell.input is used only when data is ready with a chunk.
    // For client: when data is ready, doPOST to server.
    // Source.action(Shell.input) <-- JC: what is the input for Source.action?
    Source.action(Shell.send)
    if (pause) pause = false // keep null pause unchanged
  }
  // Scheduler handle 'ready' event
  this.onReady = function() {
    ready = true // reply complete
    if (step || pause) return // let keypress do ready action
    Scheduler.act()
  }
  // Scheduler initialization
  this.init = function() {
/*   
    // set process.stdin to raw for keystrokes
    process.stdin.setRawMode(true)
    // set encoding for keys
    process.stdin.setEncoding('utf8')
    // on any data into stdin
    process.stdin.on('data', function(key){
      if (key === '\u0004') { // CTRL-D (end of input)
        return quit('Abort')
      }
      if (key === '\u001a') { // CTRL-Z (end of stream)
        return quit('Done')
      }
      if (key === '\u0003') { // CTRL-C (end of text)
        pause = true // no action
        // tell Shell to end, then quit.
        return Shell.end()
      }
      if (key === 'j') { // j for noStep (jump)
        if (debug) print('j pressed.')
        step = false
      }
      if (key === 'q') { // q for noPause (quick)
        if (debug) print('q pressed.')
        step = false
        pause = null // a hard false
      }
      if (ready) Scheduler.act()
      // if (!ready) ignore key
      if (ready == null) print('Press CRTL-C to quit.')
    })
    // essential for Windows, optional for Mac
    process.stdin.resume()
*/    
  }
}

var SIZE = 200 // default child process stdout buffer size
var Batch = new function() {
   // run in batch mode
   this.run = function (name, response) {
      if (response.match(/^404 Not Found/)) {
         Printer.printError('Not found: ' + name + LF)
         return
      }
      // split either by \n, or \r\n 
      var lines = (response.indexOf('\r') == -1) ? response.split(LF) : response.split('\r\n')
      // remove starting empty line
      var line = ''
      while (line == '' && lines.length > 0) line = lines.pop()
      if (lines == '') {
         Printer.printInfo('Empty file: ' + name + LF)
         return
      }
      lines.push(line) // put back the non-empty line
      RPC.showInfo('Number of lines: ' + lines.length)
      // source.unshift(new Auto(name, lines))
      Source.set(lines)
      // start line check by source
      Source.init()
      // start detecting keypress
      Scheduler.init()
      // start the shell with action
      //log('JC: tell Shell to init with: CMD=' + CMD + ', PROMPT=' + PROMPT)
      Shell.init(CMD, PROMPT, SIZE)
   }
}

// *** Input ********************************************************
/* These event handlers responds to keystrokes due to:
 
 <textarea id="input" class="input" wrap="off" onkeydown="Input.keyDown(event)" rows="1" placeholder="Type your input here."></textarea>
 
*/
var Input = new function () {

   // public methods ---------------------------

   // handle the event e when key is down (called by HTML <input> field)
   this.keyDown = function (e) {
      if (debug) print('JC: e.shiftKey=' + e.shiftKey + ', e.ctrlKey=' + e.ctrlKey + ', e.keyCode=' + e.keyCode + EOL)
      if (e.shiftKey && e.keyCode == 13) { // shift-enter
         // don't do anything; allow the shift-enter to insert a line break as normal
      }
      // console input always in History
      else if (e.keyCode == 13) emitter.emit('data', History.put(getInput())) // enter
      // just pressing the CTRL key itself is an event
      else if (e.ctrlKey && e.keyCode > 64) Command.send(e.keyCode) // all CTRL-code pass to server
      else if (e.keyCode == 38) History.go(true) // up
      else if (e.keyCode == 40) History.go(false) // down
      else if (e.keyCode == 74) { // press j
         step = false
         getInput() // so input will be cleared
         Scheduler.act()
      }
      else if (e.keyCode == 81) { // press q
         step = false
         pause = null // a hard false
         getInput() // so input will be cleared
         Scheduler.act()
      }
      else if (e.keyCode == 32) { // press SPACE
         getInput() // so input SPACE will be cleared
         /* JC add */
         if (ready) Scheduler.act()
         // if (!ready) ignore key
         if (ready == null) print('Press CRTL-C to quit.')
         /* end JC add */
      }
      else if (e.keyCode == 32 && _in.value == '') { // first SPACE
         //emitter.emit('end') // same as CTRL-X
         getInput() // so input SPACE will be cleared
      }
      else {
         // RPC.showInfo('keyCode=' + e.keyCode) // no tab complete: e.keyCode == 9
      }
      setTimeout(recalculateInputHeight, 0) // expand <input> for any key, e.g. paste by CTRL-V
   }
   // refocus on <input> field
   this.refocus = function () {
      _in.blur() // needed for Mozilla to scroll correctly.
      _in.focus()
   }
   // clear input
   this.clear = function () {
      _in.value = '' // for print() output to go in the right place.
      recalculateInputHeight()
      Input.refocus() // cannot use: this.refocus(), why? RPC.exit() can use 'this'.
   }

   // private methods --------------------------

   // get input and record in History
   function getInput() {
      var s = _in.value
      setTimeout(Input.clear, 0) // do this later, so \n will be echoed first for <textarea>
      return s
   }

   // recompute input height for visibility and selection
   function recalculateInputHeight() {
      var rows = _in.value.split(/\n/).length // many lines when input is pasted
      + (window.opera ? 1 : 0) // leave room for scrollbar in Opera
      if (_in.rows != rows) _in.rows = rows // expand number of rows
   }
}

// *** History ********************************************************
var History = new function () {
   // internal attributes ---------------------
   var histList = [""] // list of commands
   var histPos = 0 // pointer along histList
   // public methods ---------------------------
   // get last command from history
   this.get = function () {
      return histPos == 0 ? '' : histList[histPos - 1]
   }
   // put command in history
   this.put = function (cmd) {
      histList[histList.length - 1] = cmd
      histList[histList.length] = ''
      histPos = histList.length - 1
      return cmd
   }

   // get command from history, depending on up/down
   this.go = function (up) {
      // histList[0] = first command entered, [1] = second, etc.
      // type something, press up: thing typed is now in "limbo"
      // (last item in histList) and should be reachable by pressing down again.
      var L = histList.length;
      if (L == 1) return

      if (up) {
         if (histPos == L - 1) {
            // Save this entry in case the user hits the down key.
            histList[histPos] = _in.value
         }
         if (histPos > 0) {
            histPos--
            // Use a timeout to prevent up from moving cursor within new text
            // Set to nothing first for the same reason
            setTimeout(function () {
               _in.value = ''
               _in.value = histList[histPos]
               var caretPos = _in.value.length
               if (_in.setSelectionRange) _in.setSelectionRange(caretPos, caretPos)
            }, 0)
         }
      }
      else { // down
         if (histPos < L - 1) {
            _in.value = histList[++histPos]
         }
         else if (histPos == L - 1) {
            // Already on the current entry: clear but save
            if (_in.value) {
               histList[histPos++] = _in.value
               _in.value = ''
            }
         }
      }
   }
}

// *** Command Handling ***********************************************
var Command = new function () {

   // display help information
   this.doHelp = function () {
      var sb = []
      sb.push('<h4>Special commands in Input line</h4>')
      sb.push('<table>')
      sb.push('<tr><td>doLoad [filename]</td><td></td><td>load text input from [filename].</td></tr>')
      sb.push('<tr><td>noLoad</td><td></td><td>discard all inputs from text.</td></tr>')
      sb.push('<tr><td>doPause</td><td></td><td>respect (*PAUSE*) on text input.</td></tr>')
      sb.push('<tr><td>noPause</td><td></td><td>ignore (*PAUSE*) on text input.</td></tr>')
      sb.push('<tr><td>cls</td><td></td><td>clear console Output.</td></tr>')
      sb.push('<tr><td>doHelp</td><td></td><td>display this Help.</td></tr>')
      sb.push('<tr><td>doExit</td><td></td><td>close this Session.</td></tr>')
      sb.push('</table>')
      sb.push('<h4>Special keys for Input line</h4>')
      sb.push('<ul>')
      sb.push('<li>UP: retrieve last command from History.</li>')
      sb.push('<li>DOWN: next command from History when doing UP.</li>')
      sb.push('<li>SHIFT-ENTER: soft-enter for mutli-line input (will not send line).</li>')
      sb.push('<li>ENTER: end of input (will send line/lines if ending in semicolon).</li>')
      sb.push('<li>CTRL-X or first SPACE: continue from Pause.</li>')
      sb.push('</ul>')
      Printer.printInfo(sb.join(''))
   }

   // run a command
   this.run = function (cmd) {
      if (cmd == 'cls') return '(* !cls *)'
      if (cmd.indexOf('doLoad') == 0) return '(* !load ' + cmd.substring('doLoad '.length) + ' *)'
      if (cmd == 'noLoad') return '(* !noload *)'
      if (cmd == 'doPause') return '(* !dopause *)'
      if (cmd == 'noPause') return '(* !nopause *)'
      if (cmd == 'doHelp') return '(* !help *)'
      if (cmd == 'doExit') return '(* !quit *)'
   }

   // send a control code
   this.send = function (code) {
      if (code == 88) { // CTRL-X for end of Console input
         emitter.emit('end')
         return
      }
      else if (code == 67 || code == 86) { // CTRL-C is copy, CTRL-V is paste
         return // no need to send to server (some server process takes CTRL-C as interrupt)
      }
      else if (code == 89 || code == 90) { // CTRL-Y is redo, CTRL-Z is undo
         return // no need to send to server (some server process takes CTRL-Z as end-of-file)
      }
      /*
       else if (code == 67) doControl('^C'); // CTRL-C (copy)
       else if (code == 77) doControl('^M'); // CTRL-M
       else if (code == 86) doControl('^V'); // CTRL-V (paste)
       else if (code == 89) doControl('^Y'); // CTRL-Y (FF redo)
       else if (code == 90) doControl('^Z'); // CTRL-Z (FF undo)
       */
      if (debug) Printer.printInfo('Control code=' + code)
      _in.value = '' // for print() output to go in the right place.
      RPC.sendControl('^' + String.fromCharCode(code))
   }

   // show help given duration
   this.help = function (duration) {
      var oldInfo = RPC.info
      RPC.showInfo('Type "doHelp" for help')
      setTimeout(function () {
         RPC.showInfo(oldInfo)
      }, duration)
   }
}

// *** Remote Procedure Call ******************************************
var RPC = new function () {
   // Browser IE does not define XMLHttpRequest by default, use a wrapper
   if (typeof XMLHttpRequest == 'undefined') {
      XMLHttpRequest = function () {
         try {
            return new ActiveXObject('Msxml2.XMLHTTP.6.0');
         } catch (e) {}
         try {
            return new ActiveXObject('Msxml2.XMLHTTP.3.0');
         } catch (e) {}
         try {
            return new ActiveXObject('Msxml2.XMLHTTP');
         } catch (e) {}
         try {
            return new ActiveXObject('Microsoft.XMLHTTP');
         } catch (e) {}
         throw new Error('No XMLHttpRequest')
      }
   }

   // internal attribute -----------------------
   var request = new XMLHttpRequest()
   // the unique identifier of this client
   var uid = 'Web' + (new Date()).getTime() + Math.floor(Math.random() * 100)
   // status
   var ready = true

   // public methods ---------------------------
   
   // get a file via Server
   this.getFile = function (file) {
      this.showInfo('Loading from ' + file)
      request.open('GET', '/' + file, true)
      request.setRequestHeader('Cache-Control', 'no-cache')
      request.setRequestHeader('Client-Uid', uid)
      request.setRequestHeader('Content-Type', 'text/plain; charset=utf-8')
      request.onreadystatechange = function () {
         if (request.readyState == 4) { // XHR_LOADED
            //Action.batch(file, request.responseText)
            Batch.run(file, request.responseText)
         }
      }
      request.send(null)
   }

   // do POST
   this.doPost = function doPost(query, data, callback) {
      request.open('POST', query, true)
      request.setRequestHeader('Cache-Control', 'no-cache') // This line sometimes has nonfatal error, why??
      request.setRequestHeader('Client-Uid', uid)
      request.setRequestHeader('Content-Type', 'text/plain; charset=utf-8')
      //request.setRequestHeader('Content-Length', content.length) // will set by agent (the browser)
      request.onreadystatechange = function () {
         if (request.readyState == 4) { // XHR_LOADED
            doResponse(request.responseText, callback)
         }
      }
      request.send(data)
   }

   // Sending a Demo Request by POST
   this.sendDemo = function (cmd) {
      request.open('POST', '/rpc/shell/', true)
      request.setRequestHeader('Cache-Control', 'no-cache') // This line sometimes has nonfatal error, why??
      request.setRequestHeader('Client-Uid', uid)
      request.setRequestHeader('Content-Type', 'text/plain; charset=utf-8')
      //request.setRequestHeader('Content-Length', content.length) // will set by agent (the browser)
      request.onreadystatechange = function () {
         if (request.readyState == 4) { // XHR_LOADED
            doResponse(request.responseText)
         }
      }
      request.send(cmd)
   }

   // Sending a Control Request by POST
   this.sendControl = function (code) {
      request.open('POST', '/rpc/control/', true)
      request.setRequestHeader('Cache-Control', 'no-cache') // This line sometimes has nonfatal error, why??
      request.setRequestHeader('Client-Uid', uid)
      request.setRequestHeader('Content-Type', 'text/plain; charset=utf-8')
      //request.setRequestHeader('Content-Length', content.length) // will set by agent (the browser)
      request.onreadystatechange = function () {
         if (request.readyState == 4) { // XHR_LOADED
            doResponse(request.responseText)
         }
      }
      request.send(code)
   }

   // Sending a Command Request by POST (no arg for setTimeout)
   this.sendCommand = function (cmd) {
      if (!ready) {
         Printer.printInfo('Server not Ready!')
         return
      }
      if (!uid) {
         Printer.printInfo('Lost my uid: [' + uid + ']')
         return
      }
      if (simulate) { // simulate server
         // cmd is a chunk, usually ends in semicolon (;)
         var text = cmd == null ? "Moscow ML version 2.01 (January 2004)\nEnter `quit();' to quit." : eval(cmd.noComment())
         // ensure text is string
         text = text + LF + '- '
         doResponse(text)
         return
      }
      // for mosml, cmd just need to end with ;
      // for HOL, cmd must end with LF
      if (cmd == null) cmd = ''
      else if (cmd.lastChar() != LF) cmd = cmd + LF

      request.open('POST', '/rpc/command/', true)
      request.setRequestHeader('Cache-Control', 'no-cache')
      request.setRequestHeader('Client-Uid', uid)
      request.setRequestHeader('Content-Type', 'text/plain; charset=utf-8')
      //request.setRequestHeader('Content-Length', content.length) // will set by agent (the browser)
      request.onreadystatechange = function () {
         if (request.readyState == 4) { // XHR_LOADED
            doResponse(request.responseText)
            ready = true
         }
      }
      request.send(cmd)
      ready = false
      //request.send(Command.get()); // cmd = Comment.get()
   }

   // Response Processing (private methods) --------------------------
   
   var ESC = String.fromCharCode(27)

/*

ONE_LT_PRIME:
val it = ⊢ ∀[0;32mp[0m. prime [0;32mp[0m ⇒ 1 < [0;32mp[0m: thm
ONE_DIVIDES_ALL:
val it = ⊢ ∀[0;32ma[0m. 1 ∣ [0;32ma[0m: thm
*/

   // process response
   function doResponse(sHTML, callback) {
      // apply post-processing for HTML
      var s = postProcess(sHTML)
      // check color escape codes
      if (s.indexOf(ESC + '[0m') > 0) { // this is [0m 
         s = convertColor(s)
      }
      // translate server message
      if (s.indexOf('!@#$') == 0) {
         s = 'Session Terminated.' + LF + '- '
         ended = true
      }
      showResponse(s) // show the response
      if (debug) print('JC: doResponse callback!')
      callback()
   }
   function xdoResponse(sHTML) {
      var ended = false
      var seen = false // whether prompt is seenn
      if (sHTML != '') {
         // check server response ends in PROMPT
         seen = sHTML.match(PROMPT)
         // if (!seen) Printer.printError('Server reply not ending in PROMPT' + LF)
         // apply post-processing for HTML
         sHTML = postProcess(sHTML)
         // check color escape codes
         if (sHTML.indexOf(ESC + '[0m ') > 0) { // this is [0m 
            sHTML = convertColor(sHTML)
         }
         // translate server message
         if (sHTML.indexOf('!@#$') == 0) {
            sHTML = 'Session Terminated.' + LF + '- '
            ended = true
         }
         if (sHTML.indexOf('!@') == 0) {
            RPC.showInfo('SHELL not defined, Javascript is used.')
            sHTML = sHTML.slice(2)
         }
         showResponse(sHTML) // show the response
         // handle ending of session
         if (seen) if (debug) Printer.printInfo('Server replied.' + LF)
      }
      ready = true
      if (ended) {
         RPC.exit()
         return
      }
      if (seen) {
         Action.more() // send another chunk
      }
      else {
         // only time to request more from server
         RPC.sendCommand(null) // hope next doResponse will see PROMPT
         // This works, server can send response whenever length > 80, or as soon as data is available
      }
   }

   // good for testing pretty printing of responses
   this.pp = function (s) {
      showResponse(s)
   }

   // pretty print output s
   function showResponse(s) {
      // SML/HOL response output detection
      var k = s.indexOf('!')
      if (k != -1 && (s.indexOf('! Toplevel') == 0 || s.indexOf('! Uncaught') >= 0 || s.indexOf('! Error') >= 0)) { // print error
         if (k != 0) { // k == 0 is starting with !
            k = s.indexOf('! Uncaught')
            if (k != -1) { // before ! is OK, after ! is error
               var j = s.indexOf('Type inference failure') // i.e. HOL unify error
               if (j != -1) {
                  j = s.indexOf('unify failed')
                  Printer.printInfo(s.slice(0, j + 12)) // 12 = 'unify failed'.length
                  s = s.slice(j + 12)
               }
               else {
                  // this part is almost repeated in two places, not ideal.
                  j = s.indexOf('&lt;&lt;HOL') // i.e. <<HOL
                  Printer.printAlert('*** j=' + j)
                  if (j != -1) {
                     var h = s.indexOf('&gt;&gt;', j) // i.e. detect >>
                     Printer.printOutput(s.slice(0, j))
                     Printer.printInfo(s.slice(j, h + 8)) // 8 = '&gt;&gt;'.length
                     s = s.slice(j + 8)
                  }
                  else {
                     Printer.printOutput(s.slice(0, k))
                     s = s.slice(k)
                  }
               }
            }
         }
         k = s.indexOf('- ') // the MOSML prompt
         if (k > 4) { // last prompt is not error
            Printer.printError(s.slice(0, k - 4)) // 4 = '<br>'.length
            Printer.printOutput(s.slice(k))
         }
         else {
            Printer.printError(s)
         }
      }
      else {
         k = s.indexOf('&lt;&lt;HOL') // i.e. <<HOL
         if (k != -1) {
            // this part is almost repeated in two places, not ideal (see above).
            var h = k
            k = s.indexOf('&gt;&gt;', k) // i.e. detect >>
            Printer.printOutput(s.slice(0, h))
            Printer.printInfo(s.slice(h, k + 8)) // 8 = '&gt;&gt;'.length
            Printer.printOutput(s.slice(k + 8))
         }
         else {
            Printer.printOutput(s)
         }
      }
   }

   // post processing of response for HTML display
   function postProcess(s) {
      // String s may contain unicodes, but there is no need to convert them to &#...;
      // replace characters for HTML display (order is important)
      return s.replace(/</g, '&lt;') // angular bracket open
      .replace(/>/g, '&gt;') // angular bracket close
      .replace(/\r\n/g, '<br>') // CRLF to break
      .replace(/\n\r/g, '<br>') // LFCR to break
      .replace(/\n/g, '<br>') // LF to break
      .replace(/\r/g, '<br>') // CR to break
      .replace(/\|-/g, '&#8866;') // |- to right-tack (symbol implies, &#8866;)
      .replace(/-\|/g, '&#8867;') // -| to left-tack (symbol back-implies, &#8867;)
      // (these are not visually appealing)
      // (to test: type in HOL shell input: (* symbol: &#8596; *)
      // .replace(/&lt;-&gt;/g, '&#8596;')  // <-> to double-arrow (symbol &#8596; )
      // .replace(/--&gt;/g, '&#8594;')  // --> to right-arrow (symbol &#8594;)
      // .replace(/&lt;--/g, '&#8592;') // <-- to left-arrow (symbol &#8592;)
      .replace(/--&gt;/g, '&#8212;&gt;') // -- to one line (symbol &#8212;)
      .replace(/&lt;--/g, '&lt;&#8212;') // -- to one line (symbol &#8212;)
   }

   
   // color conversion for HTML display
   function convertColor(s) {
      if (debug) Printer.printInfo('Color ESC code detected! ' + s.length + '<br>')
      var parts = s.split(ESC)
      var buffer = []
      // parts[0] is not leading by ESC, just output at the end
      for (var j = 1; j < parts.length; j++) {
         // if (debug) Printer.printInfo(''+j+':' + parts[j] + '<br>')
         /* Color Equivalence
          Black       0;30     Dark Gray     1;30
          Blue        0;34     Light Blue    1;34
          Green       0;32     Light Green   1;32
          Cyan        0;36     Light Cyan    1;36
          Red         0;31     Light Red     1;31
          Purple      0;35     Light Purple  1;35
          Brown       0;33     Yellow        1;33
          Light Gray  0;37     White         1;37
          */
         buffer.push(
         parts[j].replace('[0m', '</span>')
         .replace('[0;30m', '<span style="color:#000000">') // black
         .replace('[0;31m', '<span style="color:#FF0000">') // red
         .replace('[0;32m', '<span style="color:#008000">') // green
         .replace('[0;33m', '<span style="color:#A52A2A">') // brown
         .replace('[0;34m', '<span style="color:#0000FF">') // blue
         .replace('[0;35m', '<span style="color:#800080">') // purple
         .replace('[0;36m', '<span style="color:#00FFFF">') // cyan
         .replace('[0;37m', '<span style="color:#D3D3D3">') // light gray
         .replace('[0;1;30m', '<span style="color:#696969">') // dark gray
         .replace('[0;1;31m', '<span style="color:#CD5C5C">') // light red
         .replace('[0;1;32m', '<span style="color:#00FF7F">') // light green
         .replace('[0;1;33m', '<span style="color:#FFFF00">') // yellow
       //  .replace('[0;1;34m', '<span style="color:#87CEFA">') // light blue
         .replace('[0;1;34m', '<span style="color:#0000FF">') // JC: light blue to blue (for browser)
         .replace('[0;1;35m', '<span style="color:#9370DB">') // light purple
         .replace('[0;1;36m', '<span style="color:#E0FFFF">') // light cyan
         .replace('[0;1;37m', '<span style="color:#FFFFFF">') // white
         )
      }
      return parts[0] + buffer.join('') + '<br>'
   }

   // Info/Logging ------------------------
   
   // public attributes ------------------------
   this.info = '' // the info line
   //this.more = true // the more flag, if false, RPCMore() will not run
   // show info
   this.showInfo = function (s) {
      if (s != undefined) this.info = s
      _info.innerHTML = (this.info == '' ? '' : '[' + this.info + '] ')
                   //   + (Action.Pause() ? '' : '[no pause] ')
                      + (delay > 0 ? 'delay [' + delay + ' ms] ' : '') 
                      + 'wait [' + wait + ' ms]'
   }

   // close this client on server
   this.exit = function () {
      this.showInfo('No more Input - close or refresh this browser Tab.')
      Printer.printAlert((function (x) {
         return (x > 6) && (x < 20)
      })(new Date().getHours()) ? 'Goodbye.' : 'Goodnight.')
   }
}

// *** Demo Commands *************************************************
var shellCommands = {

   // shortcut to clear screen
   cls: function () {
      this.clear()
   },

   // clear screen
   clear: function () {
      var CHILDREN_TO_PRESERVE = 0; // 3 for original header
      while (_out.childNodes[CHILDREN_TO_PRESERVE]) {
         _out.removeChild(_out.childNodes[CHILDREN_TO_PRESERVE])
      }
   }
}

// *** Utilities ******************************************************
var Printer = new function () {

   // private method ---------------------------
   function print(s, type) {
      if ((s = String(s))) { // re-assign to ensure String, follow by non-null check
         var span = document.createElement("span")
         span.innerHTML = s // so that s can be HTML
         span.className = type
         _out.appendChild(span)
         span.scrollIntoView() // Yes! this is working (comment out to test)
         return span
      }
   }

   // public methods ---------------------------
   this.printInput = function (s) {
      print(s, "input")
   }
   this.printOutput = function (s) {
      print(s, "output")
   }
   this.printInfo = function (s) {
      print(s, "info")
   }
   this.printAlert = function (s) {
      print(s, "alert")
   }
   this.printComment = function (s) {
      print(s, "comment")
   }
   this.printError = function (s) {
      print(s, "error")
   }

   var inComment = false // the flag for pretty-printer

   // pretty print input s
   this.prettyPrint = function (s) {
      // allows HTML tags <nospace>
      s = s.replace(/ < /g, ' &lt; ').replace(/ > /g, ' &gt; ') // for innerHTML
      .replace(/<</g, '&lt;&lt;').replace(/>>/g, '&gt;&gt;').replace(/\t/g, TAB) // \t to TAB: some spaces
      .replace(/\n$/, '<br>') // \n at end to <br>
      .replace(/\n/g, '\\n') // other \n to '\n'
      s = toUnicode(s)
      if (!inComment) {
         var k = s.indexOf('(*')
         inComment = k != -1
         if (k > 0) { // split before and after comment
            Printer.printInput(s.slice(0, k)) // before is normal
            s = s.slice(k) // after is comment
         }
      }
      if (inComment) {
         var k = s.indexOf('*)')
         if (k != -1) {
            Printer.printComment(s.slice(0, k + 2)) // before is comment
            Printer.printInput(s.slice(k + 2)) // after is normal
         }
         else Printer.printComment(s)
      }
      else Printer.printInput(s)
      if (inComment) inComment = s.indexOf('*)') == -1
   }

   // unicode conversion
   function toUnicode(s) {
      var sb = []
      for (var j = 0; j < s.length; j++) {
         if (s.charCodeAt(j) < 128) sb.push(s.charAt(j))
         else sb.push('&#' + s.charCodeAt(j) + ';')
      }
      return sb.join('')
   }
}

/**
 * Documentation:
 *
 * String (Extensions)
 * String.noComment()
 * String.noTrail()
 * String.lastChar()
 *
 * SimpleEE (Type)
 * SimpleEE.prototype.on
 * SimpleEE.prototype.emit
 * SimpleEE.prototype.isChunk
 *
 * emitter.on('data', callback)
 * emitter.on('send', callback)
 * emitter.on('end', callback)
 * emitter.emit('data', line)
 * emitter.emit('send')
 * emitter.emit('end')
 *
 * Auto (Type)
 * Auot.end()
 * Auto.doChunk()
 *
 * Console (Source)
 * Auto['abc'] (Source)
 * Auto['xyz'] (Source)
 *
 * Action (Object)
 * Action.more()
 * Action.doChunk()
 * Action.next()
 * Action.process(line)
 *
 * Input (Object)
 * Input.keyDown(event)
 * Input.refocus()
 * Input.clear()
 *
 * History (Object)
 * History.get()
 * History.put(command)
 * History.go(flag)
 *
 * Command (Object)
 * Command.run(command)
 * Command.send(code)
 * Command.help()
 *
 * RPC (Object) Remote Procedure Call
 * RPC.getFile(file)
 * RPC.sendControl(code)
 * RPC.sendDemo(command)
 * RPC.sendCommand(command)
 * RPC.info
 * RPC.capture
 * RPC.showInfo()
 * RPC.logging()
 * RPC.exit()
 *
 * shellCommands (Object)
 *
 * Printer (Object)
 * Printer.printInput(s)
 * Printer.printOutput(s)
 * Printer.printInfo(s)
 * Printer.printComment(s)
 * Printer.printError(s)
 * Printer.printAlert(s)
 *
 * Event Flow of program
 * =====================
 * The whole program is driven by:
 * Command.request -- get input from source, analyze input,
 *  and send via RPC.send(input), callback is Command.response.
 * Command.response -- to process server reply, decide how to display,
 * then call Command.request again.
 *
 * If input is (* !load "file" *)
 * input source is swtich to lines from "file".
 * upon end-of-file of "file", source is switch back.
 *
 * 1. on init(), RPC.sendCommand('')  // get initial response from server
 *    After this, everything is handled by Input object
 * 2. When ENTER is pressed,
 *    Command.run(getInput())   // getInput() is effectively _in.value
 * 3. Command.run(cmd) checks the command cmd:
 *    If doLoad, TextBuffer.load(cmd.substring('doLoad '.length))
 *    If none of doXXX, or noXXX,
 *    (a) put the line in inBuffer
 *    (b) pretty print the line
 *    (c) If the line ends in ';' (after comment clean up),
 *        RPC.sendCommand(inBuffer.join('\n'))
 *        inBuffer = []
 * 4. If server ends session, server sends to client (response): '!@#$' (4 top-keys)
 *    If client ends session, client sends to server  (request): CTRL-$ (simulated)
 */