/*
 * Run HOL in demo mode (a pure NodeJs solution):
 * 1. Simple example of Shell wrapper in NodeJs.
 * 2. Use child_process of NodeJs to wrap a process.
 * 3. Chunks of input (multi-lines) to get a process response.
 * 4. Chunks of process response ends in READY prompt.
 *
 * DOS> node HOLdemo.js
 * $ node HOLdemo.js
 * $ node HOLdemo.js demo.hol       (with 200K output buffer)
 * $ node HOLdemo.js demo.hol 500   (use 500K output buffer)
 *
 */
var os = require('os'),
    fs = require('fs'),
    child = require('child_process')

// var debug = true // true or false
var debug = false
var step = true
// var step = false // true or false
var pause = false // start with false

var args = process.argv.slice(1)
/* e.g.
args[0]=/Users/josephchan/work/hol/demo/HOLdemo.js
 */
if (args[1] == '-h') {
   console.log('Usage: node HOLdemo.js')
   console.log('       node HOLdemo.js [demo-file]')
   console.log('       node HOLdemo.js [demo-file] [buffer-size]')
   process.exit()
}

// global constants
// these are initialized in Source.init()
var CMD, PROMPT   // command for child process and prompt
var KOPEN, KCLOSE // comment open and close markers
var KLINE         // for splitting comment within line
var KCMD          // for extracting comment word
var KPART         // for replacing comment parts in line
var BOPEN, BCLOSE // special bracket open and close markers

// global print
function print(s) {
  process.stdout.write(s)
}

// global quit
function quit(s) {
  if (s) console.log(s)
  var hour = (new Date()).getHours()
  console.log(hour < 12 ? 'Goodday' : hour > 20 ? 'Goodnight' : 'Goodbye')
  process.exit(0)
}

// flag to indicate if READY to respond to keystrokes
var ready = false

// Shell = process wrapper
var Shell = new function() {  
  // input to be set
  this.input = null
  // make up child process options (buffer size in units of K)
  function makeOption(size) {
    return { encoding: 'utf8',
              timeout: 0,
            maxBuffer: (size || 200)*1024, // default 200*1024
           killSignal: 'SIGTERM',
                  cwd: null,
                  env: null }
  }
  // start the process cmd, and handle process.stdout
  this.init = function(cmd, prompt, size) {
    var process = child.exec(cmd, makeOption(size))
    // handle process.stdout 'data' events
    process.stdout.on('data', function(data) {
      if (debug) console.log('data: [' + data + ']')
      print(data) // show output (display)
      if (data.match(prompt)) {
        if (debug) console.log('data ends in PROMPT, call ready.')
        Scheduler.onReady() // emit 'ready' to Scheduler
      }
    })
    // handle process.stdout 'end' events
    process.stdout.on('end', function() {
      if (debug) console.log('process output ends.')
    })
    // handle process 'exit' event
    process.on('exit', function(code) {
      if (debug) console.log('Child process ends with exit code ' + code)
      ready = null // no reply
      if (!step) quit() // end program.
    })
    // expose only process.stdin
    Shell.input = process.stdin
  }
  // end the child process
  this.end = function() {
    step = false // to quit
    Shell.input.end() // will trigger exit
  }
}

// ANSI Color ESC code
var Color = {
  black:       '[0;30m',
  red:         '[0;31m',
  green:       '[0;32m', 
  brown:       '[0;33m', 
  blue:        '[0;34m',
  purple:      '[0;35m',
  cyan:        '[0;36m',
  lightGray:   '[0;37m',
  darkGray:    '[0;1;30m',
  lightRed:    '[0;1;31m',
  lightGreen:  '[0;1;32m',
  yellow:      '[0;1;33m',
  lightBlue:   '[0;1;34m',
  lightPurple: '[0;1;35m',
  lightCyan:   '[0;1;36m',
  white:       '[0;1;37m',
}
var ESC = String.fromCharCode(27)
var suffix = ESC + '[0m'

// end of line
var EOL = os.EOL

// String prototype extension

// paint text with color
String.prototype.paint = function(color) {
  return ESC + color + this + suffix
}
// remove comment
String.prototype.noComment = function() {
  // comment: [KOPEN][any space][anything][any space][KCLOSE] = KPART
  return this.replace(KPART, '')
}
// remove trailing spaces
String.prototype.noTrail = function() {
  return this.replace(/(\s+)$/, '')
}
// get last character
String.prototype.lastChar = function() {
  return this.slice(-1, this.length)
}
// escape every character
String.prototype.escape = function() {
  var s = this
  var buffer = []
  for (var j = 0; j < s.length; j++) buffer.push('\\' + s.charAt(j))
  return buffer.join('')
}

// Source = keeper of input chunks
var Source = new function() {
  // Source has a buffer
  var buffer = []
  // put lines to buffer
  this.put = function(data) {
    buffer.push(data)
  }
  // set the buffer
  this.set = function(lines) {
    buffer = lines
    if (debug) console.log('Buffer: ' + lines.length + ' lines.')
  }
  // get lines of text file
  this.getTextFile = function(filename) {
    var text = fs.readFileSync(filename, 'utf8')
    if (debug) console.log('Read: ' + text.length + ' bytes.')
    return text.split(EOL) // can have trailing spaces of CR (\r)
  }
  // local variables
  var quiet = false  // quiet check
  var level = 0      // level count for KOPEN/KCLOSE
  var blevel = 0     // level count for BOPEN/BCLOSE
  var c_color = Color.green   // comment color
  var d_color = Color.purple  // display color
  // make a line pretty using color
  function pretty(line) {
    // check in comment
    if (level > 0) return line.paint(c_color)
    // check unbalanced end-comment, i.e. just completes a block comment
    if (line.indexOf(KOPEN) == -1 && 
        line.indexOf(KCLOSE) !== -1) return line.paint(c_color)
    // Note: this only matches only one comment in the line 
    // match: [anything][KOPEN][something][KCLOSE][anything] = KLINE
    var match = line.match(KLINE)
    return match ?
           match[1].paint(d_color) + 
           (KOPEN + match[2] + KCLOSE).paint(c_color) +
           match[3].paint(d_color) :
           line.paint(d_color)
  }
  // process a line (check for command line)
  function process(line) {
    // match: [begin][KOPEN][space]![word][space][anything][KCLOSE][end] = KCMD
    var match = line.match(KCMD)
    if (!match) return line
    var word = match[1]
    if (debug) console.log('COMMAND: [' + line + '], word=' + word)
    // process the command word
    if (word == 'pause') if (!step && pause !== null) pause = true // keep null pause
    // when step = true, ignore pause.
    if (word == 'quit' || word == 'stop' ||
        word == 'exit' || word == 'bye') buffer = [] // empty buffer will end input
    return null // always return null for command
  }
  // check a trimmed line, return true if chunk is reached
  var isChunk = function(line) {
    // detect quiet mode
    if (line.indexOf('quietdec := true;') != -1) quiet = true
    if (line.indexOf('quietdec := false;') != -1) quiet = false
    // new detect of quiet mode
    if (line.indexOf('toggle_quietdec()') != -1) quiet = !quiet
    return !quiet && (level == 0) && (blevel == 0) && (line.lastChar() == ';')
  }
  // get a chunk from buffer, null if no chunk
  function getChunk() {
    quiet = false
    level = 0
    var lines = []
    while (buffer.length > 0) {
      var line = buffer.shift().noTrail() // ensure no trailing spaces
      line = process(line) // check if line is special -- a command line
      // if a command line, returns a null
      if (line !== null) { // can be blank line, or '' empty line
        // count KOPEN and KCLOSE for balance
        if (line.indexOf(KOPEN) !== -1) level++
        if (level > 0 && line.indexOf(KCLOSE) !== -1) level--
        // count BOPEN and BCLOSE for balance
        if (line.indexOf(BOPEN) !== -1) blevel++
        if (blevel > 0 && line.indexOf(BCLOSE) !== -1) blevel--
        // should have an echo flag to determine if line is echoed.           
        print(pretty(line) + EOL) // show input (echo) before line is modified
        line = line.noComment().noTrail() // modify after show
        if (line) { // skip empty lines (e.g. comment in a line)
          lines.push(line) // store modified line
          if (isChunk(line)) return lines.join(EOL) + EOL // check modified line
        }  
      } 
      if (pause) return "doPause" // return anything not null 
    }
    if (debug) console.log('Last chunk: [' + lines.join(EOL) + EOL + ']')
    return null // no chunk
  }
  // action when 'ready' using input stream
  this.action = function(input) {
    // get a chunk from buffer and pass to input 
    var data = getChunk()
    if (data == null) {
      if (debug) console.log('end of buffer input -- Goodbye')
      print('\n') // or print('^Z\n')
      return Shell.end() // will quit
    }
    if (pause) return // no action (pause)
    if (debug) console.log('write: [' + data + ']')
    input.write(data)
    ready = false // wait for reply
  }
  // initialization -- check start line to extract globals
  this.init = function() {
    var platform = os.platform()  // get platform
    if (debug) console.log('platform=' + platform)
    var count = 5 // get !start or !mac or !win within first 5 lines
    var match = false
    while (!match && count > 0) {
      if (buffer.length == 0) return quit('No lines in Source buffer.')
      var line = buffer.shift().noTrail() // get line without trailing spaces
      // match: [begin][nonspaces][space]!start[anything][space][nonspace][end]
      match = line.match(/^([^s]+)\ \!start(.*)\ ([^s]+)$/)  // for general, e.g. Unix
            || (platform == 'darwin' &&
                line.match(/^([^s]+)\ \!mac(.*)\ ([^s]+)$/)) // for MacOS
            || (platform.indexOf('win') !== -1 &&
                line.match(/^([^s]+)\ \!win(.*)\ ([^s]+)$/)) // for Windows
      count--
    }
    if (!match && count == 0) return quit('Source missing start comment.')
    KOPEN = match[1], KCLOSE = match[3]
    if (debug) console.log('KOPEN=[' + KOPEN + '], KCLOSE=[' + KCLOSE + ']')
    if (debug) console.log('COMMAND: [' + line + '], start args=' + match[2])
    // match: [space]"[cmd]"[space]"[prompt]"
    match = match[2].match(/^\ \"(.+)\"\ \"(.+)\"$/)        
    CMD = match ? match[1] : 'hol' // default to HOL
    PROMPT = new RegExp((match ? match[2] : '> ') + '$') // default to PolyML prompt
    if (debug) console.log('CMD=[' + CMD + ']')
    if (debug) console.log('PROMPT=[' + PROMPT + ']')
    // match: [anything][KOPEN][something][KCLOSE][anything]
    KLINE = new RegExp('(.*)' + KOPEN.escape() + '(.+)' + KCLOSE.escape() + '(.*)')
    // match: [begin][KOPEN][space]![word][space][anything][KCLOSE][end]
    KCMD = new RegExp('^' + KOPEN.escape() + '\\ \\!([a-zA-z]+)\\ (.*)' + KCLOSE.escape() + '$')
    // match: [KOPEN][spaces][something][spaces][KCLOSE]
    KPART = new RegExp(KOPEN.escape() + '(\\s+)(.+)(\\s+)' + KCLOSE.escape(), 'g')
    if (debug) console.log('KLINE=[' + KLINE + ']')
    if (debug) console.log('KCMD=[' + KCMD + ']')
    if (debug) console.log('KPART=[' + KPART + ']')
/*
KOPEN=[(*], KCLOSE=[*)]
PROMPT=[/> $/]
KLINE=[/(.*)\(\*(.+)\*\)(.*)/]
KCMD=[/^\(\*\ \!([a-zA-z]+)\ (.*)\*\)$/]
KPART=[/\(\*(\s+)(.+)(\s+)\*\)/g]
*/
    // Note: this is a quick fix only.
    // Todo: have a test mode (just emit chunks)
    // Todo: have special lines before !start to determine BOPEN, BCLOSE, TERMINATOR, quiet mode, etc.
    BOPEN = '<|'
    BCLOSE = '|>'
  }
}

// Scheduler = coordinator between Source and Shell
var Scheduler = new function() {
  // invoke action in Source with Shell input
  this.act = function() {
    Source.action(Shell.input)
    if (pause) pause = false // keep null pause unchanged
  }
  // Scheduler handle 'ready' event
  this.onReady = function() {
    ready = true // reply complete
    if (step || pause) return // let keypress do ready action
    Scheduler.act()
  }
  // Scheduler initialization
  this.init = function() {
    // set process.stdin to raw for keystrokes
    process.stdin.setRawMode(true)
    // set encoding for keys
    process.stdin.setEncoding('utf8')
    // on any data into stdin
    process.stdin.on('data', function(key){
      if (key === '\u0003') { // CTRL-C (end of text)
        pause = true // no action
        return Shell.end()
      }
      if (key === 'j') { // j for noStep (jump)
        if (debug) console.log('j pressed.')
        step = false
      }
      if (key === 'q') { // q for noPause (quick)
        if (debug) console.log('q pressed.')
        step = false
        pause = null // a hard false
      }
      if (ready) Scheduler.act()
      // if (!ready) ignore key
      if (ready == null) console.log('Press CRTL-C to quit.')
    })
    // essential for Windows, optional for Mac
    process.stdin.resume()
  }
}

// get demo file name and stdout buffer size from arguments
var NAME = args[1]
var SIZE = args[2]
if (NAME) {
  Source.set(Source.getTextFile(NAME))
}
else {
// prepare the Source buffer
Source.put('(* !mac "hol" "> " *)')
Source.put('(* !win "go hol" "- " *)')
Source.put('(* !start *)')
Source.put('(* Start of Demo *);')
Source.put('2 + 4*5; (* simple arithmetic *)')
Source.put('EVAL ``2 ** 100``; (* exact arithmetic *)')
Source.put('(* Theorem: 1 + 1 = 2 *)')
Source.put('(* Proof: by definition.')
Source.put('   Here is a nested comment; (* ok *)')
Source.put('*)')
Source.put('(* !pause *)')
Source.put('(* First, set a goal: *)')
Source.put('g `1 + 1 = 2`;')
Source.put('(* !pause *)')
Source.put('e (DECIDE_TAC);')
Source.put('(* capture the theorem *)')
Source.put('val one_add_one = save_thm("one_add_one", top_thm());')
Source.put('drop();')
Source.put('(* !pause *)')
Source.put('')
Source.put('val _ = HOL_Interactive.toggle_quietdec();')
Source.put('')
Source.put('(* open dependent theories *)')
Source.put('open pred_setTheory arithmeticTheory;')
Source.put('')
Source.put('val _ = HOL_Interactive.toggle_quietdec();')
Source.put('')
Source.put('ADD; (* Definition of ADD *)')
Source.put('(* !stop *)')
Source.put('(* !quit *)') // stop already
Source.put('"Hello nobody";') // will be ignored
}
// start line check by source
Source.init()
// start detecting keypress
Scheduler.init()
// start the shell with action
Shell.init(CMD, PROMPT, SIZE)

if (debug) console.log('Shell ready to START!')

/**
 * Documentation
 *
 * 1. Shell holds the input lines.
 * 2. Shell holds the child process.
 * 3. Shell coordinates everything:
 *    - When child process gives a READY prompt, the Shell is 'ready'.
 *    - On 'ready', the Shell acts by looking at its buffer.
 *    - Shell reads lines from buffer, echoes as it reads.
 *    - Shell ensures that a chunk (maybe multiple lines) will result in READY prompt.
 *    - Shell writes the chunk to the input of child process.
 *    - Shell displays output from child process, checking READY prompt, and repeats.
 * 4. Scheduler also handles step and pause, by keystrokes 'j' and 'q'.
 */