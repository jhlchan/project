
14/11/20
========
Put combinatorics as a separate theory, in folder count.

24/11/20
========
Rework symmetric group and automorphism groups, of group, field, and fixing subfield.

04/02/20
========
Establish the Euler phi function is multiplicative for coprimes.
Formulate the set of coprimes of a prime power, and give its cardinality.

HOL4 Upgrade (14/02/21)
=======================
git pull coreHOL develop           (update to latest HOL4)
poly < tools/smart-configure.sml   (update configuration)
bin/build cleanAll                 (rebuild)
bin/build --nograph                (example/ is not compiled, cd examples/algebra  Holmake, etc)
cd examples/AKS/, Holmake          (should be ok)
cd examples/fermat/, Holmake       (hope is ok)

(if successful) git push           (update my own develop)
q exe holx                         (fix any unexpected differences)

new branch (15/02/21)
---------------------
git status               (shows in branch develop)
git checkout -b jhlchan  (create and work in a branch)
git status               (shows in branch fermat)

q exe holx    (all ok)

~/HOL/examples/algebra$ git push
fatal, use: git push --set-upstream origin jhlchan

Put all changes by git push first, then finally:

~/HOL/examples/fermat$ git push --set-upstream origin jhlchan

Pull request (17/02/21)
-----------------------
~/HOL/examples$ git push --set-upstream origin jhlchan
remote: Create a pull request for 'jhlchan' on GitHub by visiting:
remote:      https://github.com/jhlchan/HOL/pull/new/jhlchan
remote: 
To https://github.com/jhlchan/HOL.git
 * [new branch]      jhlchan -> jhlchan
Branch jhlchan set up to track remote branch jhlchan from origin.

Use: https://github.com/jhlchan/HOL/pull/new/jhlchan
(sign in, use Firefox)

Open a pull request 
base repository(left):
HOL-Theorem-Prover/HOL, base: develop
head repository (right):
jhlchan/HOL, compare: jhlchan
"Able to merge" shows green tick.

Write:
Improve theorems, remove duplicates, more helpers. Include combinatorics of sets and permutation using lists. Also prove properties of Euler phi function, and a simple version of Chinese Remainder Theorem.

click "Create pull request".

Jhlchan #892
show: all the commits (7)
show: 1 pending check.

(17/02/21)
mn200 3 hours ago Member 
This can be done via

Definition multiples_upto_def[nocompute]:
   ...
End


Pull request successfully merged and closed

You’re all set — the jhlchan:jhlchan branch can be safely deleted.
If you wish, you can also delete this fork of HOL-Theorem-Prover/HOL in the settings.

Click "Delete branch".
@jhlchan jhlchan deleted the jhlchan:jhlchan branch now.

Locally:
git checkout develop        (switch back to develop branch)
git pull coreHOL develop

now includes examples/fermat/count etc.

poly < tools/smart-configure.sml
bin/build cleanAll
bin/build --nograph             (example/ is not compiled, cd example/algebra  Holmake, etc)
(if successful) git push        (update my own develop)

git branch -d jhlchan           (delete local branch)

Tag all parts with jhlchan892.
cd ~/work/hol
git tag -m "Label after pull request #892." jhlchan892
git push origin jhlchan892



