(* ------------------------------------------------------------------------- *)
(* Windmills of the minds: Fermat's Two Squares Theorem (part 4)             *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "part4";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)

(* val _ = load "windmillTheory"; *)
open helperTwosqTheory;
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory; (* for divides_def, prime_def *)
open logPowerTheory; (* for prime_non_square *)

(* val _ = load "part2Theory"; *)
(* for fermat_two_squares_exists_windmill, fermat_two_squares_unique_odd_even *)
open windmillTheory;

(* val _ = load "involuteFixTheory"; *)
open involuteTheory; (* for involute_bij *)
open involuteFixTheory;

(* val _ = load "iterateComposeTheory"; *)
open iterationTheory;
open iterateComposeTheory;

(* val _ = load "iterateComputeTheory"; *)
open iterateComputeTheory;

(* for later computation *)
open listTheory;
open rich_listTheory; (* for MAP_REVERSE *)
open helperListTheory; (* for listRangeINC_LEN *)
open listRangeTheory; (* for listRangeINC_CONS *)


(* ------------------------------------------------------------------------- *)
(* Windmills of the minds (part 4) Documentation                             *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
   run n   = REVERSE (run_chain (1, 1, n DIV 4))
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   Helper Theorems:

   Fermat Two-Squares Algorithm:
   zagier_fixes_prime
                   |- !p. prime p /\ p MOD 4 = 1 ==>
                          fixes zagier (mills p) = {(1,1,p DIV 4)}
   flip_fixes_prime_sing
                   |- !p. prime p /\ p MOD 4 = 1 ==> SING (fixes flip (mills p))
   flip_fixes_prime_card_upper
                   |- !p. prime p /\ p MOD 4 = 1 ==> CARD (fixes flip (mills p)) <= 1
   flip_fixes_prime|- !p. prime p /\ p MOD 4 = 1 ==>
                          (let u = (1,1,p DIV 4) ;
                               n = iterate_period (zagier o flip) u
                            in fixes flip (mills p) = {FUNPOW (zagier o flip) (HALF n) u})
   flip_fixes_prime_alt
                   |- !p u n. prime p /\ p MOD 4 = 1 /\ u = (1,1,p DIV 4) /\
                              n = iterate_period (zagier o flip) u ==>
                              fixes flip (mills p) = {FUNPOW (zagier o flip) (HALF n) u}
   flip_fixes_iterates_prime
                   |- !p u n g. prime p /\ p MOD 4 = 1 /\ u = (1,1,p DIV 4) /\
                                n = iterate_period (zagier o flip) u /\
                                g = (\(x,y,z). y <> z) ==>
                                ~g (FUNPOW (zagier o flip) (HALF n) u) /\
                                !j. j < HALF n ==> g (FUNPOW (zagier o flip) j u)

   fermat_two_squares_algo
                   |- !p. prime p /\ p MOD 4 = 1 ==>
                         (let u = (1,1,p DIV 4) ;
                              n = iterate_period (zagier o flip) u ;
                              v = FUNPOW (zagier o flip) (HALF n) u
                           in u IN mills p /\ zagier u = u /\
                              v IN mills p /\ flip v = v)
   fermat_two_squares_algorithm
                   |- !p. prime p /\ p MOD 4 = 1 ==>
                         (let u = (1,1,p DIV 4) ;
                              n = iterate_period (zagier o flip) u ;
                              (x,y,z) = FUNPOW (zagier o flip) (HALF n) u
                           in y = z /\ p = x ** 2 + (2 * y) ** 2)

   Recursive computation of Fermat's two-squares, with correctness:
   zagier_flip_1_1_z_period
                     |- !z. iterate_period (zagier o flip) (1,1,z) = 1 <=> z = 1
   zagier_flip_period_property
                     |- !n p. ~square n /\ n MOD 4 = 1 /\
                              p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
                              0 < p /\ p <= CARD (mills n)
   compute_path_def  |- !n. compute_path n =
                            if square n \/ n MOD 4 <> 1 then []
                            else (let f = zagier o flip ;
                                      z = (1,1,n DIV 4) ;
                                      u = [f z]
                                   in iterate_search (\ls. f (HD ls)::ls)
                                                     (\ls. HD ls = z) u (n ** 3) [] 0)
   compute_path_nil  |- !n. square n \/ n MOD 4 <> 1 ==> compute_path n = []
   compute_path_thm  |- !n. ~square n /\ n MOD 4 = 1 ==>
                            (compute_path n =
                                (let f = zagier o flip ;
                                     z = (1,1,n DIV 4) ;
                                     p = iterate_period f z
                                  in MAP (\j. FUNPOW f j z) (p downto 1)))
   two_sq_period_def |- !n. two_sq_period n = LENGTH (compute_path n)
   two_sq_period_thm |- !n. two_sq_period n =
                            if ~square n /\ n MOD 4 = 1
                            then iterate_period (zagier o flip) (1,1,n DIV 4)
                            else 0
   two_sq_period_eqn |- !n p. p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
                              two_sq_period n = if ~square n /\ n MOD 4 = 1 then p else 0
   two_sq_period_alt |- !n p k. ~square n /\ n MOD 4 = 1 /\ k = n DIV 4 /\
                                p = iterate_period (zagier o flip) (1,1,k) ==>
                                two_sq_period n = p
   two_sq_path_def   |- !n. two_sq_path n = turn (REVERSE (compute_path n))
   two_sq_path_thm   |- !n. two_sq_path n =
                            if ~square n /\ n MOD 4 = 1
                            then (let f = zagier o flip ;
                                      z = (1,1,n DIV 4) ;
                                      p = iterate_period f z
                                   in MAP (\j. FUNPOW f j z) [0 ..< p])
                            else []
   two_sq_path_alt   |- !n p k. ~square n /\ n MOD 4 = 1 /\ k = n DIV 4 /\
                                p = iterate_period (zagier o flip) (1,1,k) ==>
                                two_sq_path n =
                                MAP (\j. FUNPOW (zagier o flip) j (1,1,k)) [0 ..< p]
   two_sq_path_length|- !n p. ~square n /\ n MOD 4 = 1 /\
                              p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
                              LENGTH (two_sq_path n) = p

   Compute the Flip Fix triple:
   compute_flip_fix_def
                     |- !n. compute_flip_fix n =
                            if square n \/ n MOD 4 <> 1 then (0,0,0)
                            else (let f = zagier o flip ;
                                      g (x,y,z) = y = z ;
                                      t = (1,1,n DIV 4)
                                   in iterate_search f g t (n ** 3) (0,0,0) 0)
   compute_flip_fix_thm
                     |- !p. prime p /\ p MOD 4 = 1 ==>
                            (let (x,y,z) = compute_flip_fix p
                              in p = windmill (x, y, z) /\ y = z)
   fermat_two_sq_thm |- !p. prime p /\ p MOD 4 = 1 ==>
                            (let (x,y,z) = compute_flip_fix p
                              in p = x ** 2 + (y + z) ** 2)

   A simple primality test:
   fsearch_def       |- !n k. fsearch n k =
                              if k * k > n then n
                              else if k divides n then k
                              else fsearch n (k + 2)
   factor_def        |- !n. factor n = if EVEN n then 2 else fsearch n 3
   is_prime_def      |- !n. is_prime n <=> 1 < n /\ (factor n = n)
   fermat_two_sq_def |- !n. fermat_two_sq n =
                            if ~square n /\ n MOD 4 = 1
                            then (\(x,y,z). (x,y + z)) o compute_flip_fix n
                            else (0,0)
   fermat_two_sq_eqn |- !p. prime p /\ p MOD 4 = 1 ==>
                            (let (x,y) = fermat_two_sq p in p = x ** 2 + y ** 2)

   Computation by WHILE loop:
   found_def       |- !x y z. found (x,y,z) <=> y = z
   found_not       |- $~ o found = (\(x,y,z). y <> z)
   two_sq_chain_def|- !n. two_sq_chain n =
                          (let f = zagier o flip ;
                               z = (1,1,n DIV 4) ;
                               p = iterate_period f z ;
                               h = HALF p
                            in MAP (\j. FUNPOW f j z) [0 .. h])
   two_sq_def      |- !n. two_sq n = WHILE ($~ o found) (zagier o flip) (1,1,n DIV 4)
   two_sq_alt      |- !n. two_sq n = WHILE (\(x,y,z). y <> z) (zagier o flip) (1,1,n DIV 4)
   two_sq_while_hoare  |- !p. prime p /\ p MOD 4 = 1 ==>
                              HOARE_SPEC (fixes zagier (mills p))
                                         (WHILE ($~ o found) (zagier o flip))
                                         (fixes flip (mills p))
   two_sq_while_thm    |- !p. prime p /\ p MOD 4 = 1 ==>
                              (let (x,y,z) =
                                   WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)
                                in p = x ** 2 + (y + z) ** 2)
   two_sq_while_def    |- !n. two_sq_while n =
                                (\(x,y,z). (x,y + z))
                                (WHILE ($~ o found) (zagier o flip) (1,1,n DIV 4))
   two_sq_while_eqn    |- !p. prime p /\ p MOD 4 = 1 ==>
                              (let (x,y) = two_sq_while p in p = x ** 2 + y ** 2)
   two_squares_def     |- !n. two_squares n = (let (x,y,z) = two_sq n in (x,y + z))
   two_squares_thm     |- !p. prime p /\ p MOD 4 = 1 ==>
                              (let (u,v) = two_squares p in p = u ** 2 + v ** 2)

   Run chain:
   grow_def        |- !f ls. grow f ls = f (HD ls)::ls
   run_chain_def   |- !u. run_chain u =
                          WHILE ($~ o found o HD) (grow (zagier o flip)) [u]

   Computation of Two Squares (for paper):
   quit_def    |- !x y z. exit (x,y,z) <=> x * y * z = 0 \/ y = z
   chain_def   |- !u. chain u =
                      WHILE (\(j,ls). ~(exit (HD ls) \/ 0 < j /\ u = HD ls))
                            (\(j,ls). (j + 1,(zagier o flip) (HD ls)::ls)) (0,[u])
   fermat_def  |- !p. fermat p =
                     (let vlist = chain (1,1,p DIV 4) ;
                          (x,y,z) = HD (SND vlist)
                       in [(x,2 * y); (x * x,4 * y * z); (FST vlist,windmill (x, y, z))])

   Recursive computation of Fermat's two-squares (old):
   two_sq_search_def |- !t j c. two_sq_search t c j =
                                if c <= j then (0,0,0)
                                else if found t then t
                                else two_sq_search ((zagier o flip) t) c (j + 1)
   fermat_two_sq_old_def
                     |- !n. fermat_two_sq_old n = two_sq_search (1,1,n DIV 4) (n ** 3) 0

*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* This part4 works on an algorithm for the two squares in Fermat's theorem. *)

(* ------------------------------------------------------------------------- *)
(* Fermat Two-Squares Algorithm                                              *)
(* ------------------------------------------------------------------------- *)

(*
part2Theory.zagier_fixes_sing;
|- !p. prime p /\ ( p MOD 4 = 1) ==> fixes zagier (mills p) = {(1,1,p DIV 4)}
*)

(* An alternative proof of the same theorem in part2, as exercise. *)

(* Theorem: prime p /\ p MOD 4 = 1 ==> fixes zagier (mills p) = {(1,1,p DIV 4)} *)
(* Proof:
   Let s = mills p,
       k = p DIV 4,
       a = fixes zagier s.
   The goal becomes: a = {(1,1,k)}

   Use EXTENSION to show two sets are equal:
   (1) (x,y,z) IN a ==> x = 1, y = 1, z = k.
       Note (x,y,z) IN s /\
            zagier (x,y,z) = (x,y,z)  by fixes_element
        but x <> 0                    by mills_prime_triple_nonzero
         so x = y                     by zagier_fix
        Now p = windmill (x, x, z)    by mills_element
        ==> x = 1, y = 1, z = k       by windmill_trivial_prime
   (2) (1,1,k) IN a
       Note p = k * 4 + p MOD 4       by DIVISION
              = 4 * k + 1             by p MOD 4 = 1
              = windmill (1, 1, k)    by windmill_trivial
         so (1,1,k) IN s              by mills_element
        and zagier (1,1,k) = (1,1,k)  by zagier_1_1_z
        ==> (1,1,k) IN a              by fixes_element
*)
Theorem zagier_fixes_prime:
  !p. prime p /\ p MOD 4 = 1 ==> fixes zagier (mills p) = {(1,1,p DIV 4)}
Proof
  rpt strip_tac >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `k = p DIV 4` >>
  qabbrev_tac `a = fixes zagier s` >>
  simp[EXTENSION, pairTheory.FORALL_PROD, EQ_IMP_THM] >>
  ntac 5 strip_tac >| [
    `(p_1,p_1',p_2) IN s /\ (zagier (p_1,p_1',p_2) = (p_1,p_1',p_2))` by metis_tac[fixes_element] >>
    `p_1 = p_1'` by metis_tac[zagier_fix, mills_prime_triple_nonzero] >>
    metis_tac[windmill_trivial_prime, mills_element],
    `p = k * 4 + p MOD 4` by rw[DIVISION, Abbr`k`] >>
    `_ = windmill (1, 1, k)` by rw[windmill_trivial] >>
    `(1,1,k) IN s` by rw[mills_element, Abbr`s`] >>
    fs[fixes_element, zagier_1_1_z, Abbr`a`]
  ]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==> SING (fixes flip (mills p)) *)
(* Proof:
   Let s = mills p,
       b = fixes flip s.
   The goal is: SING b.
   Note ?x y. p = windmill (x, y, y)
                               by fermat_two_squares_exists_windmill
    Let u = (x,y,y).
   Then u IN s                 by mills_element
    and u IN b                 by fixes_element, flip_fix
   By SING_DEF, EXTENSION, this is to show:
      !t. t IN b ==> t = u.
   Note t IN s                 by fixes_element
    and ?h k. t = (h,k,k)      by triple_parts, flip_fix
   Thus p = windmill (h, k, k) by mills_element

   Let c = 2 * y,
       d = 2 * k.
    Now ODD p                  by odd_by_mod_4, p MOD 4 = 1
    and p = x ** 2 + c ** 2 /\ ODD x   by windmill_x_y_y
    and p = h ** 2 + d ** 2 /\ ODD h   by windmill_x_y_y
    but EVEN c /\ EVEN d               by EVEN_DOUBLE
    ==> x = h /\ c = d         by fermat_two_squares_unique_odd_even
     so x = h /\ y = k         by EQ_MULT_LCANCEL
     or t = u.
*)
Theorem flip_fixes_prime_sing:
  !p. prime p /\ p MOD 4 = 1 ==> SING (fixes flip (mills p))
Proof
  rpt strip_tac >>
  `?x y. p = windmill (x, y, y)` by rw[part2Theory.fermat_two_squares_exists_windmill] >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `b = fixes flip s` >>
  qabbrev_tac `u = (x,y,y)` >>
  `u IN s` by rw[mills_element, Abbr`u`, Abbr`s`] >>
  `u IN b` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  simp[SING_DEF, EXTENSION] >>
  qexists_tac `u` >>
  (rewrite_tac[EQ_IMP_THM] >> simp[]) >>
  rpt strip_tac >>
  `x' IN s /\ ?h k. x' = (h,k,k)` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  `p = windmill (h, k, k)` by fs[mills_element, Abbr`s`] >>
  qabbrev_tac `c = 2 * y` >>
  qabbrev_tac `d = 2 * k` >>
  `ODD p` by rfs[odd_by_mod_4] >>
  `p = x ** 2 + c ** 2 /\ ODD x` by metis_tac[windmill_x_y_y] >>
  `p = h ** 2 + d ** 2 /\ ODD h` by metis_tac[windmill_x_y_y] >>
  `EVEN c /\ EVEN d` by rw[EVEN_DOUBLE, Abbr`c`, Abbr`d`] >>
  `x = h /\ c = d` by metis_tac[part2Theory.fermat_two_squares_unique_odd_even] >>
  `y = k` by fs[Abbr`c`, Abbr`d`] >>
  simp[Abbr`u`]
QED


(* Theorem: prime p /\ p MOD 4 = 1 ==> CARD (fixes flip (mills p)) <= 1 *)
(* Proof:
   Let s = mills p,
       b = fixes flip s.
   The goal is: CARD b <= 1.
   By contradiction, suppose CARD b > 1.
   Then CARD b <> 0,
     so ?u. u IN b             by CARD_EMPTY, MEMBER_NOT_EMPTY
   Note u IN s                 by fixes_element
    and ?x y. u = (x,y,y)      by triple_parts, flip_fix
   Thus p = windmill (x, y, y) by mills_element

   Also CARD b <> 1,
   Then ~SING b                by SING_CARD_1
     so b <> EMPTY             by MEMBER_NOT_EMPTY
    and ?v. v IN b /\ v <> u   by SING_ONE_ELEMENT, b <> EMPTY
   Note v IN s                 by fixes_element
    and ?h k. v = (h,k,k)      by triple_parts, flip_fix
   Thus p = windmill (h, k, k) by mills_element

   Let c = 2 * y,
       d = 2 * k.
    Now ODD p                  by odd_by_mod_4, p MOD 4 = 1
    and p = x ** 2 + c ** 2 /\ ODD x   by windmill_x_y_y
    and p = h ** 2 + d ** 2 /\ ODD h   by windmill_x_y_y
    but EVEN c /\ EVEN d               by EVEN_DOUBLE
    ==> x = h /\ c = d         by fermat_two_squares_unique_odd_even
     so x = h /\ y = k         by EQ_MULT_LCANCEL
     or v = u                  by PAIR_EQ
   This contradicts v <> u.
*)
Theorem flip_fixes_prime_card_upper:
  !p. prime p /\ p MOD 4 = 1 ==> CARD (fixes flip (mills p)) <= 1
Proof
  rpt strip_tac >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `b = fixes flip s` >>
  spose_not_then strip_assume_tac >>
  `CARD b <> 0 /\ CARD b <> 1` by decide_tac >>
  `?u. u IN b` by metis_tac[CARD_EMPTY, MEMBER_NOT_EMPTY] >>
  `u IN s /\ ?x y. u = (x,y,y)` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  `p = windmill (x, y, y)` by fs[mills_element, Abbr`s`] >>
  `~SING b` by metis_tac[SING_CARD_1] >>
  `b <> EMPTY` by metis_tac[MEMBER_NOT_EMPTY] >>
  `?v. v IN b /\ v <> u` by metis_tac[SING_ONE_ELEMENT] >>
  `v IN s /\ ?h k. v = (h,k,k)` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  `p = windmill (h, k, k)` by fs[mills_element, Abbr`s`] >>
  qabbrev_tac `c = 2 * y` >>
  qabbrev_tac `d = 2 * k` >>
  `ODD p` by rfs[odd_by_mod_4] >>
  `p = x ** 2 + c ** 2 /\ ODD x` by metis_tac[windmill_x_y_y] >>
  `p = h ** 2 + d ** 2 /\ ODD h` by metis_tac[windmill_x_y_y] >>
  `EVEN c /\ EVEN d` by rw[EVEN_DOUBLE, Abbr`c`, Abbr`d`] >>
  `x = h /\ c = d` by metis_tac[part2Theory.fermat_two_squares_unique_odd_even] >>
  `y = k` by fs[Abbr`c`, Abbr`d`] >>
  metis_tac[pairTheory.PAIR_EQ]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==> SING (fixes flip (mills p)) *)
(* Proof:
   Let s = mills p,
       b = fixes flip s.
   The goal is: SING b.
   Note ?x y. p = windmill (x, y, y)
                            by fermat_two_squares_exists_windmill
    Let u = (x,y,y).
   Then u IN s              by mills_element
    and u IN b              by fixes_element, flip_fix
   Note ~square p           by prime_non_square
     so FINITE s            by mills_non_square_finite
    and FINITE b            by fixes_finite
    ==> CARD b <> 0         by CARD_EQ_0, MEMBER_NOT_EMPTY
    But CARD b <= 1         by flip_fixes_prime_card_upper
     so CARD b = 1          by arithmetic
    ==> SING b              by CARD_EQ_1, FINITE b
*)
Theorem flip_fixes_prime_sing[allow_rebind]:
  !p. prime p /\ p MOD 4 = 1 ==> SING (fixes flip (mills p))
Proof
  rpt strip_tac >>
  `?x y. p = windmill (x, y, y)` by rw[part2Theory.fermat_two_squares_exists_windmill] >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `b = fixes flip s` >>
  qabbrev_tac `u = (x,y,y)` >>
  `u IN s` by rw[mills_element, Abbr`u`, Abbr`s`] >>
  `u IN b` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  `FINITE s` by rw[mills_non_square_finite, prime_non_square, Abbr`s`] >>
  `FINITE b` by rw[fixes_finite, Abbr`b`] >>
  `CARD b <> 0` by metis_tac[CARD_EQ_0, MEMBER_NOT_EMPTY] >>
  `CARD b <= 1` by rw[flip_fixes_prime_card_upper, Abbr`b`, Abbr`s`] >>
  `CARD b = 1` by decide_tac >>
  rfs[CARD_EQ_1]
QED

(* The above proofs require fermat_two_squares_unique_odd_even, for the next two theorems. *)

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            fixes flip (mills p) =
               {(let u = (1,1,p DIV 4) ;
                     n = iterate_period (zagier o flip) u
                  in FUNPOW (zagier o flip) (HALF n) u)} *)
(* Proof:
   Let s = mills p,
       v = FUNPOW (zagier o flip) (HALF n) u,
       a = fixes zagier s,
       b = fixes flip s.
   The goal is: b = {v}.

   Note a = {u}              by zagier_fixes_prime
     so u IN a               by IN_SING
   Also ~square p            by prime_non_square
     so FINITE s             by mills_non_square_finite
    ==> zagier involute s    by zagier_involute_mills, p MOD 4 = 1
    and flip involute s      by flip_involute_mills
    ==> ODD n                by involute_involute_fix_sing_period_odd
     so v IN b               by involute_involute_fix_orbit_fix_odd
   Thus b = {v}              by flip_fixes_prime_sing, SING_DEF, IN_SING
*)
Theorem flip_fixes_prime:
  !p. prime p /\ p MOD 4 = 1 ==>
      let u = (1, 1, p DIV 4);
          n = iterate_period (zagier o flip) u
       in fixes flip (mills p) = {FUNPOW (zagier o flip) (n DIV 2) u}
Proof
  rw_tac bool_ss[] >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `v = FUNPOW (zagier o flip) (HALF n) u` >>
  qabbrev_tac `a = fixes zagier s` >>
  qabbrev_tac `b = fixes flip s` >>
  `a = {u}` by metis_tac[zagier_fixes_prime] >>
  `u IN a` by fs[] >>
  `~square p` by metis_tac[prime_non_square] >>
  `FINITE s` by fs[mills_non_square_finite, Abbr`s`] >>
  `zagier involute s` by metis_tac[zagier_involute_mills, ONE_NOT_ZERO] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
  last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
  `ODD n` by rfs[] >>
  drule_then strip_assume_tac involute_involute_fix_orbit_fix_odd >>
  last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
  `v IN b` by rfs[] >>
  metis_tac[flip_fixes_prime_sing, SING_DEF, IN_SING]
QED

(* Theorem: prime p /\ p MOD 4 = 1 /\ u = (1, 1, p DIV 4) /\
            n = iterate_period (zagier o flip) u ==>
            fixes flip (mills p) = {FUNPOW (zagier o flip) (n DIV 2) u} *)
(* Proof: by flip_fixes_prime *)
Theorem flip_fixes_prime_alt:
  !p u n. prime p /\ p MOD 4 = 1 /\ u = (1, 1, p DIV 4) /\
          n = iterate_period (zagier o flip) u ==>
          fixes flip (mills p) = {FUNPOW (zagier o flip) (n DIV 2) u}
Proof
  metis_tac[flip_fixes_prime]
QED

(* Theorem: prime p /\ p MOD 4 = 1 /\ u = (1, 1, p DIV 4) /\
            n = iterate_period (zagier o flip) u /\ g = (\(x,y,z). y <> z) ==>
            !j. j < n DIV 2 ==> g (FUNPOW (zagier o flip) j u) /\
            ~g (FUNPOW (zagier o flip) (n DIV 2) u) *)
(* Proof:
   Let s = mills p,
       f = zagier o flip,
       h = n DIV 2,
       v = FUNPOW f h u.
   The goal is: ~g v /\ !j. j < h ==> g (FUNPOW f j u)

   Note ~square p                 by prime_non_square
     so FINITE s                  by mills_non_square_finite
    and zagier involute s         by zagier_involute_mills
    and flip involute s           by flip_involute_mills
     so f PERMUTES s              by involute_involute_permutes
   Also u IN s                    by mills_element_trivial
    and 0 < n                     by iterate_period_pos
     so h < n                     by HALF_LT, 0 < n
   Note fixes flip s = {v}        by flip_fixes_prime

   Claim: ~g v
   Note v IN fixes flip s         by IN_SING
    and ?x y z. v = (x,y,z)       by triple_parts
     so y = z                     by fixes_element, flip_fix
     or ~g v                      by definition of g

   Claim: !j. j < h ==> g (FUNPOW f j u)
   By contradiction, suppose ?j. j < h /\ ~g (FUNPOW f j u).
   Let w = FUNPOW f j u.
   Note ?x y z. w = (x,y,z)       by triple_parts
     so y = z                     by ~g w
    Now w IN s                    by FUNPOW_closure
    and flip w = w                by flip_fix
    ==> w IN (fixes flip u}       by fixes_element
   Thus       w = v               by IN_SING
    ==> j MOD n = h MOD n         by iterate_period_mod_eq
    ==>       j = h               by LESS_MOD
   which contradicts j < h.
*)
Theorem flip_fixes_iterates_prime:
  !p u n g. prime p /\ p MOD 4 = 1 /\ u = (1, 1, p DIV 4) /\
            n = iterate_period (zagier o flip) u /\ g = (\(x,y,z). y <> z) ==>
            ~g (FUNPOW (zagier o flip) (n DIV 2) u) /\
            (!j. j < n DIV 2 ==> g (FUNPOW (zagier o flip) j u))
Proof
  ntac 5 strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `h = n DIV 2` >>
  qabbrev_tac `v = FUNPOW f h u` >>
  `~square p` by rw[prime_non_square] >>
  `FINITE s` by metis_tac[mills_non_square_finite] >>
  `zagier involute s` by metis_tac[zagier_involute_mills, ONE_NOT_ZERO] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `f PERMUTES s` by fs[involute_involute_permutes, Abbr`f`] >>
  `u IN s` by rw[mills_element_trivial, Abbr`s` ] >>
  `0 < n` by metis_tac[iterate_period_pos] >>
  `h < n` by rw[HALF_LT, Abbr`h`] >>
  `fixes flip s = {v}` by metis_tac[flip_fixes_prime] >>
  `~g v` by
  (`?x y z. v = (x,y,z)` by rw[triple_parts] >>
  `y = z` by metis_tac[fixes_element, flip_fix, IN_SING] >>
  fs[]) >>
  rpt strip_tac >>
  spose_not_then strip_assume_tac >>
  qabbrev_tac `w = FUNPOW f j u` >>
  `?x y z. w = (x,y,z)` by rw[triple_parts] >>
  `~g w <=> y = z` by fs[] >>
  `y = z` by fs[] >>
  `w IN s` by rw[FUNPOW_closure, Abbr`w`] >>
  `w IN (fixes flip s)` by fs[flip_fix, fixes_element] >>
  `w = v` by metis_tac[IN_SING] >>
  `j MOD n = h MOD n` by metis_tac[iterate_period_mod_eq] >>
  `j = h` by rfs[] >>
  decide_tac
QED

(* The above proof requires flip_fixes_prime,
   which depends on fermat_two_squares_unique_odd_even.
   The later proof is the same theorem, but just using properties of the orbit.
   See recursive computation with correctness.
*)

(* Theorem: prime p /\ p MOD 4 = 1 ==>
        let u = (1, 1, p DIV 4);
            n = iterate_period (zagier o flip) u;
            v = FUNPOW (zagier o flip) (n DIV 2) u
         in u IN (mills p) /\ zagier u = u /\
            v IN (mills p) /\ flip v = v *)
(* Proof:
   Let s = mills p,
       a = fixes zagier s,
       b = fixes flip s,
       k = p DIV 4,
    so u = (1, 1, k).
   Then a = {u}                 by zagier_fixes_prime
    and u IN a                  by IN_SING
   Also b = {v}                 by flip_fixes_prime
    and v IN b                  by IN_SING
   Thus v IN s /\ flip v = v    by fixes_element, v IN b
    and u IN s /\ zagier u = u  by fixes_element, u IN a
*)
Theorem fermat_two_squares_algo:
  !p. prime p /\ p MOD 4 = 1 ==>
      let u = (1, 1, p DIV 4);
          n = iterate_period (zagier o flip) u;
          v = FUNPOW (zagier o flip) (n DIV 2) u
       in u IN (mills p) /\ zagier u = u /\
          v IN (mills p) /\ flip v = v
Proof
  rpt strip_tac >>
  qabbrev_tac `u = (1,1,p DIV 4)` >>
  qabbrev_tac `n = iterate_period (zagier o flip) u` >>
  qabbrev_tac `v = FUNPOW (zagier o flip) (HALF n) u` >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `a = fixes zagier s` >>
  qabbrev_tac `b = fixes flip s` >>
  `a = {u}` by metis_tac[zagier_fixes_prime] >>
  `u IN a` by fs[] >>
  `b = {v}` by metis_tac[flip_fixes_prime] >>
  `v IN b` by fs[] >>
  fs[fixes_element, Abbr`a`, Abbr`b`]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==>
           let u = (1, 1, p DIV 4);
               n = iterate_period (zagier o flip) u;
               (x,y,z) = FUNPOW (zagier o flip) (n DIV 2) u
             in y = z /\ p = x ** 2 + (2 * y) ** 2 *)
(* Proof:
   Let v = (x,y,z).
   Then v IN (mills p) /\ flip v = v     by fermat_two_squares_algo
    Now flip v = v ==> y = z             by flip_fix
    and v IN (mills p)
    ==> p = windmill (x, y, y)           by mills_element
          = x ** 2 + (2 * y) ** 2        by windmill_by_squares
*)
Theorem fermat_two_squares_algorithm:
  !p. prime p /\ p MOD 4 = 1 ==>
      let u = (1, 1, p DIV 4);
          n = iterate_period (zagier o flip) u;
          (x,y,z) = FUNPOW (zagier o flip) (n DIV 2) u
       in y = z /\ p = x ** 2 + (2 * y) ** 2
Proof
  rpt strip_tac >>
  qabbrev_tac `u = (1,1,p DIV 4)` >>
  qabbrev_tac `n = iterate_period (zagier o flip) u` >>
  qabbrev_tac `v = FUNPOW (zagier o flip) (HALF n) u` >>
  `v IN (mills p) /\ flip v = v` by metis_tac[fermat_two_squares_algo] >>
  `?x y z. v = (x,y,z)` by rw[triple_parts] >>
  `y = z` by fs[flip_fix] >>
  `p = windmill (x, y, y)` by fs[mills_element] >>
  fs[windmill_by_squares]
QED

(* ------------------------------------------------------------------------- *)
(* Recursive computation of Fermat's two-squares, with correctness.          *)
(* ------------------------------------------------------------------------- *)

(* Theorem: iterate_period (zagier o flip) (1,1,z) = 1 <=> z = 1 *)
(* Proof:
   By iterate_period_thm, this is to show:
   (1) zagier (flip (1,1,z)) = (1,1,z) ==> z = 1
       This is true                by zagier_flip_1_1_z
   (2) zagier (flip (1,1,1)) = (1,1,1)
       This is true                by zagier_flip_1_1_z
*)
Theorem zagier_flip_1_1_z_period:
  !z. iterate_period (zagier o flip) (1,1,z) = 1 <=> z = 1
Proof
  rw[iterate_period_thm, zagier_flip_1_1_z]
QED

(* Theorem: ~square n /\ n MOD 4 = 1 /\
            p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
            0 < p /\ p <= CARD (mills n) *)
(* Proof:
   Let s = mills n.
   Then FINITE s                     by mills_non_square_finite
     so zagier involute s            by zagier_involute_mills
    and flip involute s              by flip_involute_mills
    ==> (zagier o flip) PERMUTES s   by involute_involute_permutes
    and (1,1,n DIV 4) IN s           by mills_element_trivial
   Thus 0 < p                        by iterate_period_pos
    and p <= CARD s                  by iterate_period_upper
*)
Theorem zagier_flip_period_property:
  !n p. ~square n /\ n MOD 4 = 1 /\
        p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
        0 < p /\ p <= CARD (mills n)
Proof
  ntac 3 strip_tac >>
  qabbrev_tac `s = mills n` >>
  `FINITE s` by metis_tac[mills_non_square_finite] >>
  `zagier involute s` by metis_tac[zagier_involute_mills, ONE_NOT_ZERO] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `(zagier o flip) PERMUTES s` by fs[involute_involute_permutes] >>
  `(1,1,n DIV 4) IN s` by rw[mills_element_trivial, Abbr`s`] >>
  metis_tac[iterate_period_pos, iterate_period_upper]
QED

(* Theorem: prime p /\ p MOD 4 = 1 /\ u = (1, 1, p DIV 4) /\
            n = iterate_period (zagier o flip) u /\ g = (\(x,y,z). y <> z) ==>
            !j. j < n DIV 2 ==> g (FUNPOW (zagier o flip) j u) /\
            ~g (FUNPOW (zagier o flip) (n DIV 2) u) *)
(* Proof:
   Let s = mills p,
       f = zagier o flip,
       h = n DIV 2,
       v = FUNPOW f h u.
   The goal is: ~g v /\ !j. j < h ==> g (FUNPOW f j u)

   Note ~square p                 by prime_non_square
     so FINITE s                  by mills_non_square_finite
    and zagier involute s         by zagier_involute_mills
    and flip involute s           by flip_involute_mills
     so f PERMUTES s              by involute_involute_permutes
   Also u IN s                    by mills_element_trivial
    and 0 < n                     by iterate_period_pos
     so h < n                     by HALF_LT, 0 < n

   Without assuming the following:
   Note fixes flip s = {v}        by flip_fixes_prime
   But use:
   Note fixes zagier s = {u}      by zagier_fixes_prime
     so ODD n                     by involute_involute_fix_sing_period_odd
   Then use involute_involute_fix_odd_period_fix:
   !j. 0 < j /\ j < n ==>
       (FUNPOW (zagier o flip) j u IN fixes flip s <=> (j = h))

   Case: n = 1,
   Then h = 0                     by HALF_EQ_0, n <> 0
     so v = u                     by FUNPOW_0
          = (1,1,1)               by zagier_flip_1_1_z_period
     or ~g v                      by definition of g
    and the other case needs j < 0, which is irrelevant.

   Case: n <> 1.
    and h <> 0, so 0 < h          by HALF_EQ_0

   Claim: ~g v
   Note 0 < h < n,
     so v IN fixes flip s         by involute_involute_fix_odd_period_fix
    Now ?x y z. v = (x,y,z)       by triple_parts
    and y = z                     by fixes_element, flip_fix
     or ~g v                      by definition of g

   Claim: !j. j < h ==> g (FUNPOW f j u)
   By contradiction, suppose ?j. j < h /\ ~g (FUNPOW f j u).
   Let w = FUNPOW f j u.
   Note ?x y z. w = (x,y,z)       by triple_parts
     so y = z                     by ~g w
    Now w IN s                    by FUNPOW_closure
    and flip w = w                by flip_fix
    ==> w IN (fixes flip u}       by fixes_element
   If j = 0, w = u                by FUNPOW_0
   Then z = 1, so n = 1           by zagier_flip_1_1_z_period, PAIR_EQ
   which contradicts n <> 1.
   If j <> 0,
   Then j = h                     by involute_involute_fix_odd_period_fix
   which contradicts j < h.
*)
Theorem flip_fixes_iterates_prime[allow_rebind]:
  !p u n g. prime p /\ p MOD 4 = 1 /\ u = (1, 1, p DIV 4) /\
            n = iterate_period (zagier o flip) u /\ g = (\(x,y,z). y <> z) ==>
            ~g (FUNPOW (zagier o flip) (n DIV 2) u) /\
            (!j. j < n DIV 2 ==> g (FUNPOW (zagier o flip) j u))
Proof
  ntac 5 strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `h = n DIV 2` >>
  qabbrev_tac `v = FUNPOW f h u` >>
  `~square p` by rw[prime_non_square] >>
  `FINITE s` by metis_tac[mills_non_square_finite] >>
  `zagier involute s` by metis_tac[zagier_involute_mills, ONE_NOT_ZERO] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `f PERMUTES s` by fs[involute_involute_permutes, Abbr`f`] >>
  `u IN s` by rw[mills_element_trivial, Abbr`s` ] >>
  `0 < n` by metis_tac[iterate_period_pos] >>
  `h < n` by rw[HALF_LT, Abbr`h`] >>
  `fixes zagier s = {u}` by metis_tac[zagier_fixes_prime] >>
  drule_then strip_assume_tac (involute_involute_fix_sing_period_odd |> ISPEC ``zagier``) >>
  last_x_assum (qspecl_then [`flip`, `n`, `u`] strip_assume_tac) >>
  `ODD n` by rfs[] >>
  Cases_on `n = 1` >| [
    `h = 0` by rw[Abbr`h`] >>
    `v = u` by rw[Abbr`v`] >>
    `_ = (1,1,1)` by metis_tac[zagier_flip_1_1_z_period] >>
    rfs[],
    `h <> 0` by rw[HALF_EQ_0, Abbr`h`] >>
    `0 < h` by decide_tac >>
    drule_then strip_assume_tac (involute_involute_fix_odd_period_fix |> ISPEC ``f: num # num # num -> num # num # num``) >>
    `!j. 0 < j /\ j < n ==>
        (FUNPOW (zagier o flip) j u IN fixes flip s <=> (j = HALF n))` by rfs[] >>
    `~g v` by
  (`FUNPOW (zagier o flip) h u IN fixes flip s <=> (h = HALF n)` by rfs[] >>
    `v IN fixes flip s` by metis_tac[] >>
    `?x y z. v = (x,y,z)` by rw[triple_parts] >>
    `y = z` by metis_tac[fixes_element, flip_fix] >>
    fs[]) >>
    rpt strip_tac >>
    spose_not_then strip_assume_tac >>
    qabbrev_tac `w = FUNPOW f j u` >>
    `?x y z. w = (x,y,z)` by rw[triple_parts] >>
    `~g w <=> y = z` by fs[] >>
    `y = z` by fs[] >>
    `w IN s` by rw[FUNPOW_closure, Abbr`w`] >>
    `w IN (fixes flip s)` by fs[flip_fix, fixes_element] >>
    Cases_on `j = 0` >| [
      `w = u` by fs[Abbr`w`] >>
      metis_tac[zagier_flip_1_1_z_period, pairTheory.PAIR_EQ],
      `0 < j` by decide_tac >>
      `FUNPOW (zagier o flip) j u IN fixes flip s <=> (j = HALF n)` by rfs[] >>
      `j = h` by metis_tac[] >>
      decide_tac
    ]
  ]
QED

(* This is an independent proof, in contrast to the previous one, using:
   zagier_flip_1_1_z_period and involute_involute_fix_odd_period_fix. *)

(* Now compute the period, using iterate_search_thm. *)

(* Define compute_path of n by iterate_search *)
Definition compute_path_def:
    compute_path n =
       if (square n) \/ (n MOD 4 <> 1) then []
       (* square n has INFINITE (mills n) *)
       (* only n MOD 4 = 1 has windmill 1 1 (n DIV 4) *)
       else let f = zagier o flip;
                z = (1,1,n DIV 4);
                u = [f z]
            in iterate_search (\ls. (f (HD ls))::ls)
                              (\ls. (HD ls) = z) u (n ** 3) [] 0
End
(* later, REVERSE, then turn, to have the nice sequence. *)


(*
> EVAL ``compute_path 17``;
 = [(1,1,4); (1,4,1); (3,2,1); (1,2,2); (3,1,2)]: thm
> EVAL ``compute_path 97``;
 = [(1,1,24); (1,24,1); (3,22,1); (5,18,1); (7,12,1); (9,4,1); (1,4,6);
       (7,3,4); (1,3,8); (5,6,3); (7,2,6); (3,2,11); (1,12,2); (5,9,2);
       (9,2,2); (5,2,9); (1,2,12); (3,11,2); (7,6,2); (5,3,6); (1,8,3);
       (7,4,3); (1,6,4); (9,1,4); (7,1,12); (5,1,18); (3,1,22)]: thm
> EVAL ``compute_path 65``;
 = [(1,1,16); (1,16,1); (3,14,1); (5,10,1); (7,4,1); (1,4,4); (7,1,4);
       (5,1,10); (3,1,14)]: thm
> EVAL ``compute_path 64``; = []: thm
> EVAL ``compute_path 12``; = []: thm
*)

(* Theorem: square n \/ n MOD 4 <> 1 ==> compute_path n = [] *)
(* Proof: by compute_path_def *)
Theorem compute_path_nil:
  !n. square n \/ n MOD 4 <> 1 ==> compute_path n = []
Proof
  simp[compute_path_def]
QED

(* Theorem: ~square n /\ n MOD 4 = 1 ==>
            (compute_path n =
               let f = zagier o flip;
                   z = (1,1,n DIV 4);
                   p = iterate_period f z
                in MAP (\j. FUNPOW f j z) (p downto 1)) *)
(* Proof:
   Let u = [f z],
       h = (\ls. f (HD ls)::ls),
       g = (\ls. HD ls = z),
       q = (\j. FUNPOW f j z),
       c = n ** 3,
       s = mills n.
   By compute_path_def, this is to show:
      iterate_search h g u c [] 0 = MAP q (p downto 1)

   Note 0 < p /\ p <= CARD s             by metis_tac[zagier_flip_period_property
    and CARD s < c                       by mills_non_square_card_upper
     so p < c                            by arithmetic

   Claim: !i. i < p - 1 ==> ~g (FUNPOW h i u)   [1]
   Proof: Let j = i + 1, then j = i + 1 < p, and 0 < j.
            HD (FUNPOW h i u)
          = FUNPOW f i (f z)             by FUNPOW_cons_head
          = FUNPOW f j z                 by FUNPOW
          <> z                           by iterate_period_minimal, 0 < j < p
          so ~g (FUNPOW h i u)
   Claim: g (FUNPOW h (p - 1) u)         [2]
   Proof:   HD (FUNPOW h (p-1) u)
          = FUNPOW f (p-1) (f z)         by FUNPOW_cons_head
          = FUNPOW f p z                 by FUNPOW
          = z                            by iterate_period_property
          so g (FUNPOW h (p-1) u)

   Thus   iterate_search h g u c [] 0
        = FUNPOW h (p - 1) u             by iterate_search_thm, [1][2]
        = MAP q (p downto 1)             by FUNPOW_cons_eq_map_1, 0 < p
*)
Theorem compute_path_thm:
  !n. ~square n /\ n MOD 4 = 1 ==>
      compute_path n =
         let f = zagier o flip;
             z = (1,1,n DIV 4);
             p = iterate_period f z
          in MAP (\j. FUNPOW f j z) (p downto 1)
Proof
  rw_tac bool_ss[compute_path_def] >>
  qabbrev_tac `q = \j. FUNPOW f j z` >>
  qabbrev_tac `h = \ls. f (HD ls)::ls` >>
  qabbrev_tac `g = \ls. HD ls = z` >>
  qabbrev_tac `c = n ** 3` >>
  qabbrev_tac `s = mills n` >>
  `0 < p /\ p <= CARD s` by metis_tac[zagier_flip_period_property] >>
  `CARD s < c` by rw[mills_non_square_card_upper, Abbr`s`, Abbr`c`] >>
  `p < c` by decide_tac >>
  assume_tac (iterate_search_thm
 |> ISPEC ``h :(num # num # num) list -> (num # num # num) list``) >>
  last_x_assum (qspecl_then [`g`, `u`, `c`, `[]`, `p-1`] strip_assume_tac) >>
  `!i. i < p - 1 ==> ~g (FUNPOW h i u)` by
  (rpt strip_tac >>
  `i + 1 < p /\ i = i + 1 - 1 /\ 0 < i + 1` by decide_tac >>
  qabbrev_tac `j = i + 1` >>
  `HD (FUNPOW h i u) = FUNPOW f i (f z)` by rw[FUNPOW_cons_head, Abbr`h`, Abbr`u`] >>
  `_ = FUNPOW f j z` by rw[GSYM FUNPOW, ADD1, Abbr`j`] >>
  `FUNPOW f j z = z` by fs[Abbr`g`] >>
  metis_tac[iterate_period_minimal]) >>
  `g (FUNPOW h (p - 1) u)` by
    (`HD (FUNPOW h (p - 1) u) = FUNPOW f (p - 1) (f z)` by rw[FUNPOW_cons_head, Abbr`h`, Abbr`u`] >>
  `_ = FUNPOW f p z` by rw[GSYM FUNPOW, ADD1] >>
  rw[Abbr`g`, Abbr`q`] >>
  rw[iterate_period_property, Abbr`p`]) >>
  `iterate_search h g u c [] 0 = FUNPOW h (p - 1) u` by fs[] >>
  assume_tac (FUNPOW_cons_eq_map_1 |> ISPEC ``f :(num # num # num) -> (num # num # num)``) >>
  last_x_assum (qspecl_then [`z`, `p`] strip_assume_tac) >>
  simp[Abbr`h`, Abbr`u`]
QED

(* Compute the (zagier o flip) period of n. *)
Definition two_sq_period_def:
    two_sq_period n = LENGTH (compute_path n)
End

(*
> EVAL ``two_sq_period 17``; = 5: thm
> EVAL ``two_sq_period 37``; = 7: thm
> EVAL ``two_sq_period 61``; = 11: thm
> EVAL ``two_sq_period 97``; = 27: thm
> EVAL ``two_sq_period 65``; = 9: thm
> EVAL ``two_sq_period 64``; = 0: thm
*)

(* Convert the (zagier o flip) path to a better form. *)
Definition two_sq_path_def:
    two_sq_path n = turn (REVERSE (compute_path n))
End

(*
> EVAL ``two_sq_path 17``;
|- two_sq_path 17 = [(1,1,4); (3,1,2); (1,2,2); (3,2,1); (1,4,1)]: thm
> EVAL ``two_sq_path 37``;
|- two_sq_path 37 =
      [(1,1,9); (3,1,7); (5,1,3); (1,3,3); (5,3,1); (3,7,1); (1,9,1)]: thm
> EVAL ``two_sq_path 61``;
|- two_sq_path 61 =
      [(1,1,15); (3,1,13); (5,1,9); (7,1,3); (1,5,3); (5,3,3); (1,3,5);
       (7,3,1); (5,9,1); (3,13,1); (1,15,1)]: thm
> EVAL ``two_sq_path 97``;
|- two_sq_path 97 =
      [(1,1,24); (3,1,22); (5,1,18); (7,1,12); (9,1,4); (1,6,4); (7,4,3);
       (1,8,3); (5,3,6); (7,6,2); (3,11,2); (1,2,12); (5,2,9); (9,2,2);
       (5,9,2); (1,12,2); (3,2,11); (7,2,6); (5,6,3); (1,3,8); (7,3,4);
       (1,4,6); (9,4,1); (7,12,1); (5,18,1); (3,22,1); (1,24,1)]: thm
> EVAL ``two_sq_path 65``;
|- two_sq_path 65 =
      [(1,1,16); (3,1,14); (5,1,10); (7,1,4); (1,4,4); (7,4,1); (5,10,1);
       (3,14,1); (1,16,1)]: thm
> EVAL ``two_sq_path 64``;
- two_sq_path 64 = []: thm
*)

(* Theorem: two_sq_period n =
            if ~square n /\ n MOD 4 = 1
            then iterate_period (zagier o flip) (1,1,n DIV 4)
            else 0 *)
(* Proof:
   Let p = iterate_period (zagier o flip) (1,1,n DIV 4).
   By two_sq_period_def, compute_path_thm, this is to show:
   (1) LENGTH [1 .. p] = p, true     by listRangeINC_LEN
   (2) ~(~square n /\ n MOD 4 = 1) ==> compute_path n = []
       This is true                  by compute_path_nil
*)
Theorem two_sq_period_thm:
  !n. two_sq_period n =
          if ~square n /\ n MOD 4 = 1
          then iterate_period (zagier o flip) (1,1,n DIV 4)
          else 0
Proof
  rw[two_sq_period_def, compute_path_thm] >-
  simp[listRangeINC_LEN] >>
  metis_tac[compute_path_nil]
QED

(* Theorem: p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
            two_sq_period n = if ~square n /\ n MOD 4 = 1 then p else 0 *)
(* Proof: by two_sq_period_thm *)
Theorem two_sq_period_eqn:
  !n p. p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
        two_sq_period n = if ~square n /\ n MOD 4 = 1 then p else 0
Proof
  simp[two_sq_period_thm]
QED

(* Theorem: ~square n /\ n MOD 4 = 1 /\ k = n DIV 4 /\
            p = iterate_period (zagier o flip) (1,1,k) ==> two_sq_period n = p *)
(* Proof: by two_sq_period_thm *)
Theorem two_sq_period_alt:
  !n p k. ~square n /\ n MOD 4 = 1 /\ k = n DIV 4 /\
          p = iterate_period (zagier o flip) (1,1,k) ==> two_sq_period n = p
Proof
  simp[two_sq_period_thm]
QED

(* Theorem: two_sq_path n =
            if ~square n /\ n MOD 4 = 1
            then let f = zagier o flip;
                     z = (1,1,n DIV 4);
                     p = iterate_period f z
                  in MAP (\j. FUNPOW f j z) [0 ..< p]
            else [] *)
(* Proof:
   Let g = (\j. FUNPOW f j z).
   Note 0 < p          by zagier_flip_period_property
    and z in s         by mills_element_trivial
   By two_sq_path_def, this is to show:
   (1) turn (REVERSE (compute_path n)) = MAP g [0 ..< p]
         turn (REVERSE (compute_path n))
       = turn (REVERSE (MAP g (p downto 1)))     by compute_path_thm
       = turn (MAP g (REVERSE (p downto 1)))     by MAP_REVERSE
       = turn (MAP g [1 .. p])                   by REVERSE_REVERSE
       = turn (MAP g (SNOC p [1 .. (p-1)]))      by listRangeINC_SNOC, 0 < p
       = turn (SNOC (g p) (MAP g [1 .. (p-1)]))  by MAP_SNOC
       = (g p) :: (MAP g [1 .. (p-1)])           by turn_snoc
       = z :: (MAP g [1 .. (p-1)])               by iterate_period_property
       = (g 0) :: (MAP g [1 .. (p-1)])           by FUNPOW_0
       = MAP g (0 :: [1 .. (p-1)])               by MAP
       = MAP g [0 .. (p - 1)]                    by listRangeINC_CONS
       = MAP g [0 ..< p]                         by listRangeLHI_to_INC
   (2) ~(~square n /\ n MOD 4 = 1) ==> turn (REVERSE (compute_path n)) = []
         turn (REVERSE (compute_path n))
       = turn (REVERSE [])      by compute_path_nil
       = turn []                by REVERSE_DEF
       = []                     by turn_def
*)
Theorem two_sq_path_thm:
  !n. two_sq_path n =
           if ~square n /\ n MOD 4 = 1
           then let f = zagier o flip;
                    z = (1,1,n DIV 4);
                    p = iterate_period f z
                 in MAP (\j. FUNPOW f j z) [0 ..< p]
           else []
Proof
  rw_tac std_ss[two_sq_path_def] >| [
    qabbrev_tac `g = (\j. FUNPOW f j z)` >>
    `0 < p` by metis_tac[zagier_flip_period_property] >>
    `1 <= p /\ (p = p - 1 + 1)` by decide_tac >>
    `turn (REVERSE (compute_path n)) =
    turn (REVERSE (MAP g (p downto 1)))` by rw[compute_path_thm] >>
    `_ = turn (MAP g (REVERSE (p downto 1)))` by simp[MAP_REVERSE] >>
    `_ = turn (MAP g [1 .. p])` by simp[] >>
    `_ = turn (MAP g (SNOC p [1 .. (p-1)]))` by metis_tac[listRangeINC_SNOC] >>
    `_ = turn (SNOC (g p) (MAP g [1 .. (p-1)]))` by rw[MAP_SNOC] >>
    `_ = (g p) :: (MAP g [1 .. (p-1)])` by metis_tac[turn_snoc] >>
    `_ = z :: (MAP g [1 .. (p-1)])` by fs[iterate_period_property, mills_element_trivial, Abbr`g`, Abbr`p`] >>
    `_ = (g 0) :: (MAP g [1 .. (p-1)])` by rw[Abbr`g`] >>
    `_ = MAP g [0 .. (p - 1)]` by rw[listRangeINC_CONS] >>
    simp[GSYM listRangeLHI_to_INC],
    metis_tac[compute_path_nil, turn_nil, REVERSE_DEF]
  ]
QED

(* Theorem: ~square n /\ n MOD 4 = 1 /\ k = n DIV 4 /\
            p = iterate_period (zagier o flip) (1,1,k) ==>
            two_sq_path n = MAP (\j. FUNPOW (zagier o flip) j (1,1,k)) [0 ..< p] *)
(* Proof: by two_sq_path_thm *)
Theorem two_sq_path_alt:
  !n p k. ~square n /\ n MOD 4 = 1 /\ k = n DIV 4 /\
          p = iterate_period (zagier o flip) (1,1,k) ==>
          two_sq_path n = MAP (\j. FUNPOW (zagier o flip) j (1,1,k)) [0 ..< p]
Proof
  simp[two_sq_path_thm]
QED

(* Theorem: ~square n /\ n MOD 4 = 1 /\ k = n DIV 4 /\
            p = iterate_period (zagier o flip) (1,1,k) ==>
            two_sq_path n = MAP (\j. FUNPOW (zagier o flip) j (1,1,k)) [0 ..< p] *)
(* Proof:
   Let g = (\j. FUNPOW (zagier o flip) j (1,1,k)).
      LENGTH (two_sq_path n)
    = LENGTH (MAP g [0 ..< p])    by two_sq_path_alt
    = LENGTH [0 ..< p]            by LENGTH_MAP
    = p                           by listRangeLHI_LEN
*)
Theorem two_sq_path_length:
  !n p. ~square n /\ n MOD 4 = 1 /\
        p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
        LENGTH (two_sq_path n) = p
Proof
  metis_tac[two_sq_path_alt, LENGTH_MAP, listRangeLHI_LEN, SUB_0]
QED

(* ------------------------------------------------------------------------- *)
(* Compute the Flip Fix triple.                                              *)
(* ------------------------------------------------------------------------- *)

(* Method 1: search by recursion with cutoff. *)
Definition compute_flip_fix_def:
    compute_flip_fix n =
       if square n \/ n MOD 4 <> 1 then (0,0,0)
       else let f = zagier o flip;
                g = (\(x,y,z). y = z);
                t = (1,1,n DIV 4)
             in iterate_search f g t (n ** 3) (0,0,0) 0
End

(*
> EVAL ``compute_flip_fix 5``; = (1,1,1): thm
> EVAL ``compute_flip_fix 17``; = (1,2,2): thm
> EVAL ``compute_flip_fix 97``; = (9,2,2): thm
> EVAL ``compute_flip_fix 65``; = (1,4,4): thm
> EVAL ``compute_flip_fix 64``; = (0,0,0): thm
*)


(* Theorem: prime p /\ p MOD 4 = 1 ==>
            let (x,y,z) = compute_flip_fix p
             in p = windmill (x, y, z) /\ y = z *)
(* Proof:
   Let f = zagier o flip,
       g = (\(x,y,z). y = z),
       t = (1,1,p DIV 4),
       c = p ** 3,
       b = (0,0,0),
       s = mills n,
       n = iterate_period f t,
       h = n DIV 2.

   Note ~square p                  by prime_non_square
     so 0 < n /\ n <= CARD s       by zagier_flip_period_property
   also CARD s < c                 by mills_non_square_card_upper
    and h < n                      by HALF_LT, 0 < n
     so h < c                      by arithmetic

   Note zagier involute s          by zagier_involute_mills, ~square p
    and flip involute s            by flip_involute_mills
     so f PERMUTES s               by involute_involute_permutes
    and t IN s                     by mills_element_trivial
    and FINITE s                   by mills_non_square_finite

   Note g (FUNPOW f h z) /\ !i. i < h ==> ~g (FUNPOW f i z) [1][2]
                                   by flip_fixes_iterates_prime
       compute_flip_fix p
     = iterate_search f g t c b 0  by compute_flip_fix_def
     = FUNPOW f h t                by iterate_search_thm, [1][2]

   Let u = FUNPOW f h t.
   Then u IN s                     by FUNPOW_closure, f PERMUTES s, z IN s.
    Now ?x y z. u = (x,y,z)        by triple_parts
    ==> y = z                      by g u
   Thus p = windmill (x, y, z)         by mills_element
*)
Theorem compute_flip_fix_thm:
  !p. prime p /\ p MOD 4 = 1 ==>
         let (x,y,z) = compute_flip_fix p
          in p = windmill (x, y, z) /\ y = z
Proof
  rpt strip_tac >>
  assume_tac compute_flip_fix_def >>
  last_x_assum (qspecl_then [`p`] strip_assume_tac) >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `g = \(x:num,y:num,z:num). y = z` >>
  qabbrev_tac `z = (1,1,p DIV 4)` >>
  qabbrev_tac `c = p ** 3` >>
  qabbrev_tac `b = (0,0,0)` >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `n = iterate_period f z` >>
  qabbrev_tac `h = n DIV 2` >>
  assume_tac (iterate_search_thm |> ISPEC ``f :(num # num # num) -> (num # num # num)``) >>
  last_x_assum (qspecl_then [`g`, `z`, `c`, `b`, `h`] strip_assume_tac) >>
  `~square p` by rw[prime_non_square] >>
  `0 < n /\ n <= CARD s` by metis_tac[zagier_flip_period_property] >>
  `CARD s < c` by rw[mills_non_square_card_upper, Abbr`s`, Abbr`c`] >>
  `h < n` by rw[HALF_LT, Abbr`h`] >>
  `h < c` by decide_tac >>
  `zagier involute s` by metis_tac[zagier_involute_mills, ONE_NOT_ZERO] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `f PERMUTES s` by fs[involute_involute_permutes, Abbr`f`] >>
  `z IN s` by rw[mills_element_trivial, Abbr`z`, Abbr`s`] >>
  `FINITE s` by metis_tac[mills_non_square_finite] >>
  `g (FUNPOW f h z) /\ !i. i < h ==> ~g (FUNPOW f i z)` by
  (qabbrev_tac `q = (\(x:num,y:num,z:num). y <> z)` >>
  `~q (FUNPOW f h z) /\ !i. i < h ==> q (FUNPOW f i z)` by metis_tac[flip_fixes_iterates_prime] >>
  `!x y z. g (x,y,z) = ~q (x,y,z)` by fs[Abbr`g`, Abbr`q`] >>
  metis_tac[triple_parts]) >>
  `compute_flip_fix p = FUNPOW f h z` by fs[] >>
  qabbrev_tac `t = FUNPOW f h z` >>
  `?x y u. t = (x,y,u)` by fs[triple_parts] >>
  `y = u` by fs[Abbr`g`] >>
  `t IN s` by fs[FUNPOW_closure, Abbr`t`] >>
  `p = windmill (x, y, u)` by rfs[mills_element, Abbr`s`] >>
  simp[]
QED

(* A milestone theorem! *)

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            let (x,y,z) = compute_flip_fix p
             in (p = x ** 2 + (y + z) ** 2) *)
(* Proof:
   Note p = windmill (x, y, z) and y = z     by compute_flip_fix_thm
     so   = windmill (x, y, y) and y = z     by z = y
          = x ** 2 + (2 * y) ** 2        by windmill_by_squares
          = x ** 2 + (y + z) ** 2        by arithmetic
*)
Theorem fermat_two_sq_thm:
  !p. prime p /\ p MOD 4 = 1 ==>
         let (x,y,z) = compute_flip_fix p
          in (p = x ** 2 + (y + z) ** 2)
Proof
  rw_tac bool_ss[] >>
  drule_then strip_assume_tac compute_flip_fix_thm >>
  rfs[] >>
  simp[windmill_by_squares]
QED

(* ------------------------------------------------------------------------- *)
(* A simple primality test                                                   *)
(* ------------------------------------------------------------------------- *)

(* Search for factor *)
val fsearch_def = tDefine "fsearch" `
  fsearch n k =
  if k * k > n then n
  else if k divides n then k
  else fsearch n (k + 2)
`(WF_REL_TAC `measure (\(n,k). n - k)` >>
  rw[divides_def] >>
  `n <> k ** 2` by metis_tac[EXP_2] >>
  `n <= n ** 2` by rw[X_LE_X_SQUARED] >>
  `k ** 2 < n ** 2` by decide_tac >>
  `k < n` by fs[EXP_EXP_LT_MONO] >>
  decide_tac);

(* Need to cast into iterate_search for simple proof of correctness. *)

(* factor of n, search from 3 for odd n *)
Definition factor_def:
    factor n =
       if EVEN n then 2
       else fsearch n 3
End

(*
EVAL ``factor 6``; 2
EVAL ``factor 7``; 7
EVAL ``factor 91``; 7
EVAL ``factor 97``; 97
EVAL ``factor 49``; 7
*)

(* For use, need to show (factor n) divides n. *)

(* traditional primality test by factor *)
Definition is_prime_def:
    is_prime n <=> (1 < n /\ (factor n = n))
End

(*
EVAL ``is_prime 1``; F
EVAL ``is_prime 2``; T
EVAL ``is_prime 3``; T
EVAL ``is_prime 4``; F
EVAL ``is_prime 5``; T
EVAL ``is_prime 71``; T
*)

(* For use, need to show prime n <=> is_prime n. *)

(* Compute the two squares for Fermat's theorem. *)
Definition fermat_two_sq_def:
    fermat_two_sq n =
    (* if is_prime n /\ n MOD 4 = 1 *)
       if ~square n /\ n MOD 4 = 1
       then ((\(x,y,z). (x, y + z)) o compute_flip_fix) n
       else (0,0)
End

(*
EVAL ``fermat_two_sq 17``; = (1,4)
EVAL ``fermat_two_sq 97``; = (9,4)
EVAL ``fermat_two_sq 65``; = (1,8)
*)

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            let (x,y) = fermat_two_sq p in (p = x ** 2 + y ** 2) *)
(* Proof: by fermat_two_sq_def, fermat_two_sq_thm *)
Theorem fermat_two_sq_eqn:
  !p. prime p /\ p MOD 4 = 1 ==>
      let (x,y) = fermat_two_sq p in (p = x ** 2 + y ** 2)
Proof
  rw[fermat_two_sq_def] >| [
    qabbrev_tac `t = compute_flip_fix p` >>
    `?x y z. t = (x,y,z)` by rw[triple_parts] >>
    simp[] >>
    drule_then strip_assume_tac fermat_two_sq_thm >>
    rfs[],
    rfs[prime_non_square]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Computation by WHILE loop.                                                *)
(* ------------------------------------------------------------------------- *)

(* Define the exit condition *)
Definition found_def:
    found (x:num, y:num, z:num) <=> y = z
End

(* Theorem: $~ o found = (\(x,y,z). y <> z) *)
(* Proof: by found_def, FUN_EQ_THM. *)
Theorem found_not:
  $~ o found = (\(x,y,z). y <> z)
Proof
  rw[found_def, pairTheory.FORALL_PROD, FUN_EQ_THM]
QED

(* Define the expected result. *)

Definition two_sq_chain_def[nocompute]:
    two_sq_chain n =
       let f = zagier o flip;
           z = (1,1,n DIV 4);
           p = iterate_period f z;
           h = p DIV 2
        in MAP (\j. FUNPOW f j z) [0 .. h]
End
(* use [nocompute], as iterate_period is not in computeLib, in general. *)

(* Idea: use WHILE for search. Develop theory in iterateCompute. *)

(* Compute two squares of Fermat's theorem by WHILE loop. *)
Definition two_sq_def:
    two_sq n = WHILE ($~ o found) (zagier o flip) (1,1,n DIV 4)
End

(*
> EVAL ``two_sq 5``; = (1,2): thm   (1,1,1)
> EVAL ``two_sq 13``; = (3,2): thm  (3,1,1)
> EVAL ``two_sq 17``; = (1,4): thm  (1,2,2)
> EVAL ``two_sq 29``; = (5,2): thm  (5,1,1)
> EVAL ``two_sq 97``; = (9,4): thm  (9,2,2)
*)

(* Theorem: two_sq n = WHILE (\(x,y,z). y <> z) (zagier o flip) (1, 1, n DIV 4) *)
(* Proof: by two_sq_def, found_not. *)
Theorem two_sq_alt:
  !n. two_sq n = WHILE (\(x,y,z). y <> z) (zagier o flip) (1, 1, n DIV 4)
Proof
  simp[two_sq_def, found_not]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            HOARE_SPEC (fixes zagier (mills p))
                       (WHILE ($~ o found) (zagier o flip))
                       (fixes flip (mills p)) *)
(* Proof:
   Let s = mills p,
       f = zagier o flip,
       u = (1,1,p DIV 4),
       n = iterate_period f u,
       h = n DIV 2,
       v = FUNPOW f h u,
       g = $~ o found,  use ~g to exit WHILE loop,
       a = fixes zagier s,
       b = fixes flip s.
   The goal is: HOARE_SPEC a (WHILE g f) b

   Note a = {u}             by zagier_fixes_prime
    and b = {v}             by flip_fixes_prime

   If n = 1, that is, period = 1 for prime p = 5.
   then u = (1,1,1)         by zagier_flip_1_1_z_period, triple_parts
     so ~g u                by definition of g, found_def
    but u IN s              by mills_element_trivial
     so u IN b              by fixes_element, flip_fix
    ==> b = a               by EQUAL_SING, IN_SING
   Thus HOARE_SPEC {u} (WHILE g f) {u}
                            by iterate_while_hoare_0
     or HOARE_SPEC a (WHILE g f) b.

   If n <> 1,
   Note ~square p           by prime_non_square
     so FINITE s            by mills_non_square_finite
    and zagier involute s   by zagier_involute_mills, ~square p
    and flip involute s     by flip_involute_mills
     so f PERMUTES s        by involute_involute_permutes
   Also 0 < n               by iterate_period_pos, u IN s
    and ODD n               by involute_involute_fix_sing_period_odd
     so n <> 2              by EVEN_2, ODD_EVEN
    ==> 1 + h < n           by HALF_ADD1_LT, 2 < n
     or h < n - 1           by arithmetic

    Now ~g v /\ (!j. j < h ==> g (FUNPOW f j u))
                                      by flip_fixes_iterates_prime, found_not
    Thus HOARE_SPEC a (WHILE g f) b   by iterate_while_hoare
*)
Theorem two_sq_while_hoare:
  !p. prime p /\ p MOD 4 = 1 ==>
      HOARE_SPEC (fixes zagier (mills p))
                 (WHILE ($~ o found) (zagier o flip))
                 (fixes flip (mills p))
Proof
  rpt strip_tac >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `u = (1,1,p DIV 4)` >>
  qabbrev_tac `n = iterate_period f u` >>
  qabbrev_tac `h = n DIV 2` >>
  qabbrev_tac `v = FUNPOW f h u` >>
  qabbrev_tac `g = $~ o found` >>
  qabbrev_tac `a = fixes zagier s` >>
  qabbrev_tac `b = fixes flip s` >>
  `u IN s` by rw[mills_element_trivial, Abbr`u`, Abbr`s`] >>
  `a = {u}` by rw[zagier_fixes_prime, Abbr`a`, Abbr`u`, Abbr`s`] >>
  `b = {v}` by metis_tac[flip_fixes_prime, SING_DEF] >>
  Cases_on `n = 1` >| [
    `u = (1,1,1)` by metis_tac[zagier_flip_1_1_z_period, triple_parts] >>
    `~g u` by fs[found_def, Abbr`g`] >>
    `u IN s` by rw[mills_element_trivial, Abbr`u`, Abbr`s`] >>
    `u IN b` by rw[fixes_element, flip_fix, Abbr`b`] >>
    `b = a` by metis_tac[EQUAL_SING, IN_SING] >>
    rw[iterate_while_hoare_0],
    `~square p` by rw[prime_non_square] >>
    `FINITE s` by metis_tac[mills_non_square_finite] >>
    `zagier involute s` by metis_tac[zagier_involute_mills, ONE_NOT_ZERO] >>
    `flip involute s` by metis_tac[flip_involute_mills] >>
    `f PERMUTES s` by fs[involute_involute_permutes, Abbr`f`] >>
    `0 < n` by metis_tac[iterate_period_pos] >>
    drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
    last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
    `ODD n` by rfs[] >>
    `n <> 2` by metis_tac[EVEN_2, ODD_EVEN] >>
    `1 + h < n` by rw[HALF_ADD1_LT, Abbr`h`] >>
    `h < n - 1` by decide_tac >>
    drule_then strip_assume_tac iterate_while_hoare >>
    last_x_assum (qspecl_then [`u`, `f`, `n-1`, `n`, `g`, `h`] strip_assume_tac) >>
    `~g v /\ (!j. j < h ==> g (FUNPOW f j u))` by metis_tac[found_not, flip_fixes_iterates_prime] >>
    rfs[]
  ]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            let (x,y,z) = WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)
            in p = x ** 2 + (y + z) ** 2 *)
(* Proof:
   Let s = mills p,
       u = (1,1,p DIV 4),
       a = fixes zagier s,
       b = fixes flip s.

   Note HOARE_SPEC a
        (WHILE ($~ o found) (zagier o flip)) b
                                  by two_sq_while_hoare
    and a = {u}                   by zagier_fixes_prime

   By HOARE_SPEC_DEF, this is to show:
   !s. a s ==> b (WHILE ($~ o found) (zagier o flip) s)
   Thus s = u                     by IN_SING, function as set
   Let w = WHILE ($~ o found) (zagier o flip) u.
   Then ?x y z. w = (x,y,z)       by triple_parts
    and w IN b                    by b w, function as set
     so w IN s /\ y = z           by fixes_element, flip_fix
   Thus p
      = windmill (x, y, z)            by mills_element, w IN s
      = windmill (x, y, y)            by y = z
      = x ** 2 + (2 * y) ** 2     by windmill_by_squares
      = x ** 2 + (y + z) ** 2     by arithmetic, y = z
*)
Theorem two_sq_while_thm:
  !p. prime p /\ p MOD 4 = 1 ==>
      let (x,y,z) = WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)
       in p = x ** 2 + (y + z) ** 2
Proof
  rw_tac bool_ss[] >>
  drule_then strip_assume_tac two_sq_while_hoare >>
  rfs[whileTheory.HOARE_SPEC_DEF] >>
  qabbrev_tac `u = (1,1,p DIV 4)` >>
  `fixes zagier (mills p) = {u}` by rw[zagier_fixes_prime, Abbr`u`] >>
  last_x_assum (qspecl_then [`u`] strip_assume_tac) >>
  `(x,y,z) IN fixes flip (mills p)` by rfs[SPECIFICATION] >>
  `(x,y,z) IN (mills p) /\ y = z` by fs[fixes_element, flip_fix] >>
  metis_tac[mills_element, windmill_by_squares, DECIDE``y + y = 2 * y``]
QED

(* A beautiful theorem! *)

(* Define the algorithm. *)
Definition two_sq_while_def:
    two_sq_while n =
      (\(x,y,z). (x,y+z)) (WHILE ($~ o found) (zagier o flip) (1, 1, n DIV 4))
End

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            let (x,y,z) = two_sq_while p in (p = (x ** 2) + (y + z) ** 2) *)
(* Proof: by two_sq_while_def, two_sq_while_thm. *)
Theorem two_sq_while_eqn:
  !p. prime p /\ p MOD 4 = 1 ==>
      let (x,y) = two_sq_while p in (p = x ** 2 + y ** 2)
Proof
  rw[two_sq_while_def] >>
  qabbrev_tac `loop = WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)` >>
  `?x y z. loop = (x,y,z)` by rw[triple_parts] >>
  drule_then strip_assume_tac two_sq_while_thm >>
  rfs[Abbr`loop`]
QED

(*
> EVAL ``two_sq_while 5``; = (1,2)
> EVAL ``two_sq_while 13``; = (3,2)
> EVAL ``two_sq_while 17``; = (1,4)
> EVAL ``two_sq_while 29``; = (5,2)
> EVAL ``two_sq_while 97``;  = (9,4)
*)

(* A better algorithm, just reformulate two_sq n. *)
Definition two_squares_def:
    two_squares n = let (x,y,z) = two_sq n in (x, y + z)
End

(*
> EVAL ``two_squares 5``; = (1,2)
> EVAL ``two_squares 13``; = (3,2)
> EVAL ``two_squares 17``; = (1,4)
> EVAL ``two_squares 29``; = (5,2)
> EVAL ``two_squares 97``;  = (9,4)
> EVAL ``MAP two_squares [5; 13; 17; 29; 37; 41; 53; 61; 73; 89; 97]``;
= [(1,2); (3,2); (1,4); (5,2); (1,6); (5,4); (7,2); (5,6); (3,8); (5,8); (9,4)]: thm
*)

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            let (u,v) = two_squares p in (p = u ** 2 + v ** 2) *)
(* Proof: by two_squares_def, two_sq_def, two_sq_while_thm. *)
Theorem two_squares_thm:
  !p. prime p /\ p MOD 4 = 1 ==>
      let (u,v) = two_squares p in (p = u ** 2 + v ** 2)
Proof
  rw[two_squares_def, two_sq_def] >>
  drule_then strip_assume_tac two_sq_while_thm >>
  qabbrev_tac `loop = WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)` >>
  `?x y z. loop = (x,y,z)` by fs[triple_parts] >>
  fs[]
QED

(* ------------------------------------------------------------------------- *)
(* Run chain.                                                                *)
(* ------------------------------------------------------------------------- *)

(* This part is the investigation of run traces, mainly for paper. *)

(* Define the list growth by function f *)
Definition grow_def:
    grow f ls = f (HD ls)::ls
End

(* Use WHILE to produce a run chain, half of the orbit. *)
Definition run_chain_def:
    run_chain u =
       WHILE ($~ o found o HD) (grow (zagier o flip)) [u]
End

(* Overload using run for a number. *)
val _ = overload_on("run", ``\n. REVERSE (run_chain (1, 1, n DIV 4))``);

(*
> EVAL ``run 5``; = [(1,1,1)]: thm
> EVAL ``run 13``; = [(1,1,3); (3,1,1)]: thm
> EVAL ``run 17``; = [(1,1,4); (3,1,2); (1,2,2)]: thm
> EVAL ``run 29``; = [(1,1,7); (3,1,5); (5,1,1)]: thm
> EVAL ``run 37``; = [(1,1,9); (3,1,7); (5,1,3); (1,3,3)]: thm
> EVAL ``run 41``; = [(1,1,10); (3,1,8); (5,1,4); (3,4,2); (1,2,5); (5,2,2)]: thm
> EVAL ``run 53``; = [(1,1,13); (3,1,11); (5,1,7); (7,1,1)]: thm
> EVAL ``run 61``; = [(1,1,15); (3,1,13); (5,1,9); (7,1,3); (1,5,3); (5,3,3)]: thm
> EVAL ``run 73``; = [(1,1,18); (3,1,16); (5,1,12); (7,1,6); (5,6,2); (1,9,2); (3,2,8);
                      (7,2,3); (1,6,3); (5,3,4); (3,4,4)]: thm
> EVAL ``run 89``; = [(1,1,22); (3,1,20); (5,1,16); (7,1,10); (9,1,2); (5,8,2); (1,11,2);
                      (3,2,10); (7,2,5); (3,5,4); (5,4,4)]: thm
> EVAL ``run 97``; = [(1,1,24); (3,1,22); (5,1,18); (7,1,12); (9,1,4); (1,6,4); (7,4,3);
                      (1,8,3); (5,3,6); (7,6,2); (3,11,2); (1,2,12); (5,2,9); (9,2,2)]: thm
*)

(* To see the changes of gap: |y - z| in the trace. Is it always reducing? *)

(*
> EVAL ``MAP gap (run 17)``; = [3; 1; 0]: thm
> EVAL ``MAP gap (run 97)``; = [23; 21; 17; 11; 3; 2; 1; 5; 3; 4; 9; 10; 7; 0]: thm
*)

(* To see the changes of mind in the trace. Is it always growing? *)

(*
> EVAL ``MAP mind (run 17)``; = [1; 3; 3]: thm
> EVAL ``MAP mind (run 97)``; = [1; 3; 5; 7; 9; 9; 7; 7; 5; 7; 7; 3; 5; 9]: thm
*)


(* ------------------------------------------------------------------------- *)
(* Computation of Two Squares (for paper).                                   *)
(* ------------------------------------------------------------------------- *)

(*
Most of the following are now refactored in iterate_search,
especially the part with cutoff.
Keep just for the paper presentation.
*)


(* Exit condition of a triple. *)
Definition quit_def:
    quit (x,y,z) <=>
       x * y * z = 0 \/ (* improper windmill *)
       y = z (* a flip fix *)
       (* \/ (x = y) -- a zagier fix *)
End

(* No cutoff m, as the exit conditon check should cover all cases. *)
Definition chain_def:
    chain u = (* u = starting triple *)
       WHILE (\(j,ls). ~(quit (HD ls) \/ (0 < j /\ u = HD ls)))
             (\(j,ls). (j+1, ((zagier o flip) (HD ls))::ls))
             (0,[u]) (* starting j = 0, ls = [u] *)
End

(*
EVAL ``chain (1,1,4)``; (3,[(1,2,2); (3,1,2); (1,1,4)])
EVAL ``chain (1,1,1)``; (1,[(1,1,1)])
EVAL ``chain (1,1,2)``; (2,[(3,2,0); (1,1,2)]) hit 0!
EVAL ``chain (1,1,3)``; (2,[(3,1,1); (1,1,3)])
*)

(* Fermat's two squares algorithm *)
Definition fermat_def:
    fermat p = (* p should be a prime, with p MOD 4 = 1. *)
      let vlist = chain (1, 1, p DIV 4); (* value and list *)
          (x,y,z) = HD (SND vlist)
       in [(x,2*y); (x * x, 4 * y * z); (FST vlist, windmill (x, y, z))]
         (* (u,v)   (u ** 2, v ** 2)    p = windmill (x, y, z) *)
End
(* A list has all elements of the same type, which is num # num in this case. *)

(*
EVAL ``fermat 5``; [(1,2); (1,4); (0,5)]
EVAL ``fermat 13``; [(3,2); (9,4); (1,13)]
EVAL ``fermat 17``; [(1,4); (1,16); (2,17)]
EVAL ``fermat 7``; [(1,2); (1,4); (0,5)]  5 <> 7.
EVAL ``fermat 73``; [(3,8); (9,64); (10,73)]
EVAL ``fermat 97``; [(9,4); (81,16); (13,97)]

For 97, the period should be: 2 * 13 + 1 = 27.
EVAL ``foo (97 DIV 4) 30``;
EVAL ``LENGTH (SND (foo (97 DIV 4) 30))``; = 31.
EVAL ``EL (30 - 13) (SND (foo (97 DIV 4) 30))``; (9,2,2) the flip fix
EVAL ``foo (97 DIV 4) 27``;
EVAL ``HD (SND (foo (97 DIV 4) 27))``; (1,1,24) the start zagier fix
*)

(*
(* val _ = load "logPowerTheory"; *)
open logPowerTheory;
> EVAL ``SQRT 20``;
val it = |- SQRT 20 = 4: thm
*)

(* Fermat algorithm with testing *)
Definition Fermat_def:
    Fermat n =
      if n MOD 4 = 1 /\ is_prime n then fermat n
      else [(0,n MOD 4); (1,factor n)]
End

(*
> EVAL ``Fermat 0``; = [(0,0); (1,2)]: thm
> EVAL ``Fermat 1``; = [(0,1); (1,1)]: thm
> EVAL ``Fermat 2``; = [(0,2); (1,2)]: thm
> EVAL ``Fermat 3``; = [(0,3); (1,3)]: thm
> EVAL ``Fermat 4``; = [(0,0); (1,2)]: thm
> EVAL ``Fermat 5``; = [(1,2); (1,4); (0,5)]: thm
> EVAL ``Fermat 7``; = [(0,3); (1,7)]: thm
> EVAL ``Fermat 9``; = [(0,1); (1,3)]: thm
> EVAL ``Fermat 11``; = [(0,3); (1,11)]: thm
> EVAL ``Fermat 13``; = [(3,2); (9,4); (1,13)]: thm
> EVAL ``Fermat 15``; = [(0,3); (1,3)]: thm
> EVAL ``Fermat 17``; = [(1,4); (1,16); (2,17)]: thm
> EVAL ``Fermat 29``; = [(5,2); (25,4); (2,29)]: thm
*)


(* ------------------------------------------------------------------------- *)
(* Recursive computation of Fermat's two-squares (old).                      *)
(* ------------------------------------------------------------------------- *)

(* Recursion with cutoff c, starting count j, and initial triple t. *)
val two_sq_search_def = tDefine "two_sq_search" `
  two_sq_search t c j =
  if (c <= j) then (0,0,0)
  else if (found t) then t
  else two_sq_search ((zagier o flip) t) c (j + 1)
`(WF_REL_TAC `measure (\(t,c,j). c - j)`);

(* new syntax:

Definition two_sq_search_def:
    two_sq_search t c j =
    if (c <= j) then (0,0,0)
    else if (found t) then t
    else two_sq_search ((zagier o flip) t) c (j + 1)
Termination
    WF_REL_TAC `measure (\(t,c,j). c - j)`
End

*)

(* For mills n, the very rough estimate is that there are less than n ** 3 windmills. *)
(* A better estimate maybe:  x ** 2 < n, y < n, z < n, so n + n * n = n(n + 1)) *)
val fermat_two_sq_old_def = Define`
    fermat_two_sq_old n =
       two_sq_search (1,1,n DIV 4) (n ** 3) 0
`;

(*
EVAL ``fermat_two_sq_old 17``; = (1,2,2)
EVAL ``fermat_two_sq_old 97``; = (9,2,2)
EVAL ``fermat_two_sq_old 65``; = (1,4,4)
*)


(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
