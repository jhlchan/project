(* ------------------------------------------------------------------------- *)
(* Windmills of the minds: Fermat's Two Squares Theorem (part 2)             *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "part2";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
val _ = load("involuteFixTheory");
open helperTwosqTheory;
open helperFunctionTheory;
open helperSetTheory;
open helperNumTheory;
open logPowerTheory; (* for prime_non_square *)

val _ = load("windmillTheory");
open windmillTheory;

open involuteTheory;
open involuteFixTheory;

(* arithmeticTheory -- load by default *)
open arithmeticTheory pred_setTheory;

open dividesTheory; (* for divides_def *)
open gcdTheory; (* GCD_IS_GREATEST_COMMON_DIVISOR *)


(* ------------------------------------------------------------------------- *)
(* Windmills of the minds (part 2) Documentation                             *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   Helper Theorems:

   Fermat's Two Squares Theorem:
   zagier_fixes_sing       |- !p. prime p /\ p MOD 4 = 1 ==>
                                  fixes zagier (mills p) = {(1,1,p DIV 4)}
   fermat_two_squares_exists_windmill
                           |- !p. prime p /\ p MOD 4 = 1 ==> ?x y. p = windmill (x, y, y)
   fermat_two_squares_exists_odd_even
                           |- !p. prime p /\ p MOD 4 = 1 ==>
                                  ?u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2
   fermat_two_squares_exists
                           |- !p. prime p /\ p MOD 4 = 1 ==> ?u v. p = u ** 2 + v ** 2
   fermat_two_squares_unique
                           |- !p. prime p ==> !a b c d.
                                  p = a ** 2 + b ** 2 /\ p = c ** 2 + d ** 2 ==>
                                  (a = c /\ b = d) \/ (a = d /\ b = c)
   fermat_two_squares_unique_odd_even
                           |- !p. prime p ==> !a b c d.
                                  ODD a /\ EVEN b /\ p = a ** 2 + b ** 2 /\
                                  ODD c /\ EVEN d /\ p = c ** 2 + d ** 2 ==>
                                  (a = c /\ b = d)
   fermat_two_squares_thm  |- !p. prime p /\ p MOD 4 = 1 ==>
                                  ?!u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2

   Two Squares Theorem (bidirectional):
   fermat_two_squares_iff  |- !p. prime p ==>
                                 (p MOD 4 = 1 <=>
                                  ?!u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2)
*)

(*
QUse.useScript "windmill.hol";
quse "windmill.hol";
*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Fermat's Two Squares Theorem                                              *)
(* ------------------------------------------------------------------------- *)

(* With zagier and flip both involutions on (mills p),
    and zagier with a unique fixed point,
   this implies flip has at least a fixed point,
   giving the existence of Fermat's two squares.
   Proof based on involute_two_fixes_both_odd.
*)

(* Idea: a direct proof of part1Theory.zagier_fixes_sing. *)

(* Theorem: prime p /\ p MOD 4 = 1 ==> fixes zagier (mills p) = {(1,1,p DIV 4)} *)
(* Proof:
   By fixes_def, mills_def, this is to show:
   (1) p = windmill x y z /\ zagier (x,y,z) = (x,y,z) ==> (x,y,z) = (1,1,p DIV 4)
       Note ~square p                 by prime_non_square
        and x <> 0                    by mills_element, mills_triple_nonzero
         so x = y                     by zagier_fix
        ==> (x,y,z) = (1,1,p DIV 4)   by windmill_trivial_prime
   (2) p MOD 4 = 1 ==> p = windmill 1 1 (p DIV 4)
       This is true                   by windmill_trivial_prime
   (3) zagier (1,1,p DIV 4) = (1,1,p DIV 4)
       This is true                   by zagier_fix
*)
Theorem zagier_fixes_sing:
  !p. prime p /\ p MOD 4 = 1 ==> fixes zagier (mills p) = {(1,1,p DIV 4)}
Proof
  rw[fixes_def, mills_def, EXTENSION] >>
  simp[EQ_IMP_THM] >>
  rpt strip_tac >| [
    `~square p` by metis_tac[prime_non_square] >>
    `p MOD 4 <> 0` by decide_tac >>
    `x' = y` by metis_tac[zagier_fix, mills_element, mills_triple_nonzero] >>
    metis_tac[windmill_trivial_prime],
    metis_tac[windmill_trivial_prime],
    fs[zagier_fix]
  ]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==> ?x y. p = windmill (x, y, y) *)
(* Proof:
   Let m = mills p, the solutions (x,y,z) of p = x ** 2 + 4 * y * z.
   Note ~square p                      by prime_non_square
     so FINITE m                       by mills_non_square_finite
    and zagier involute m              by zagier_involute_mills_prime
    and flip involute m                by flip_involute_mills

   Now work out fixed points:
   Let a = fixes zagier m, b = fixes flip m.
   Let k = p DIV 4.
   Then a = {(1,1,k)}                  by zagier_fixes_sing

   The punch line:
   Note ODD (CARD a) <=> ODD (CARD b)  by involute_two_fixes_both_odd
    now CARD a = 1                     by CARD_SING
     so ODD (CARD b)                   by ODD_1
    ==> CARD b <> 0                    by ODD
     or b <> EMPTY                     by CARD_EMPTY
   Thus ? (x, y, z) IN b               by MEMBER_NOT_EMPTY
    and (x, y, z) IN m /\
        (flip (x, y, z) = (x, y, z))   by fixes_element
     so y = z                          by flip_fix
     or (x,y,y) in m                   by above
   Thus p = windmill (x, y, y)         by mills_element
*)
Theorem fermat_two_squares_exists_windmill:
  !p. prime p /\ p MOD 4 = 1 ==> ?x y. p = windmill (x, y, y)
Proof
  rpt strip_tac >>
  qabbrev_tac `m = mills p` >>
  `~square p` by metis_tac[prime_non_square] >>
  `FINITE m` by fs[mills_non_square_finite, Abbr`m`] >>
  `zagier involute m` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute m` by metis_tac[flip_involute_mills] >>
  qabbrev_tac `a = fixes zagier m` >>
  qabbrev_tac `b = fixes flip m` >>
  qabbrev_tac `k = p DIV 4` >>
  `a = {(1,1,k)}` by rw[zagier_fixes_sing, Abbr`a`, Abbr`m`] >>
  `CARD a = 1` by rw[] >>
  `ODD (CARD a) <=> ODD (CARD b)` by rw[involute_two_fixes_both_odd, Abbr`a`, Abbr`b`] >>
  `ODD (CARD b)` by metis_tac[ODD_1] >>
  `CARD b <> 0` by metis_tac[ODD] >>
  `b <> EMPTY` by metis_tac[CARD_EMPTY] >>
  `?t. t IN b` by rw[MEMBER_NOT_EMPTY] >>
  `?x y z. t = (x, y, z)` by rw[triple_parts] >>
  `t IN m /\ (flip (x, y, z) = (x, y, z))` by metis_tac[fixes_element] >>
  `y = z` by fs[flip_fix] >>
  metis_tac[mills_element]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==> ?u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2 *)
(* Proof:
   Note ?x y. p = windmill (x, y, y)      by fermat_two_squares_exists_windmill
                = x ** 2 + (2 * y) ** 2   by windmill_by_squares
   Put u = x, v = 2 * y.
   Then p = u ** 2 + v ** 2               by above
          = u * u + v * v                 by EXP_2
     or p - u * u = v * v                 by arithmetic
    and EVEN v                            by EVEN_DOUBLE
    and EVEN (v * v)                      by EVEN_MULT
    Now ODD p                             by odd_by_mod_4, p MOD 4 = 1
     so ODD (u * u)                       by EVEN_SUB, EVEN_ODD
     or ODD u                             by ODD_MULT
*)
Theorem fermat_two_squares_exists_odd_even:
  !p. prime p /\ p MOD 4 = 1 ==>
      ?u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2
Proof
  rpt strip_tac >>
  `?x y. p = windmill (x, y, y)` by rw[fermat_two_squares_exists_windmill] >>
  `_ = x ** 2 + (2 * y) ** 2` by rw[windmill_by_squares] >>
  qabbrev_tac `u = x` >>
  qabbrev_tac `v = 2 * y` >>
  `p = u * u + v * v` by simp[] >>
  `v * v = p - u * u` by decide_tac >>
  `EVEN v` by rw[EVEN_DOUBLE, Abbr`v`] >>
  `EVEN (v * v)` by rw[EVEN_MULT] >>
  `ODD p` by rw[odd_by_mod_4] >>
  `u * u <= p` by decide_tac >>
  `ODD (u * u)` by metis_tac[EVEN_SUB, EVEN_ODD] >>
  metis_tac[ODD_MULT]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==> ?u v. p = u ** 2 + v ** 2 *)
(* Proof: by fermat_two_squares_exists_odd_even *)
Theorem fermat_two_squares_exists:
  !p. prime p /\ p MOD 4 = 1 ==> ?u v. p = u ** 2 + v ** 2
Proof
  metis_tac[fermat_two_squares_exists_odd_even]
QED

(* Yah, a wondeful proof of a wonderful theorem! *)

(* ------------------------------------------------------------------------- *)
(* The Classical Uniqueness Proof                                            *)
(* ------------------------------------------------------------------------- *)

(*
Classical uniqueness proof:
https://proofwiki.org/wiki/Fermat%27s_Two_Squares_Theorem

*)

(* This is the original proof of the uniqueness part. *)

(* Theorem: prime p ==>
            !a b c d. p = a ** 2 + b ** 2 /\ p = c ** 2 + d ** 2 ==>
                      (a = c /\ b = d) \/ (a = d /\ b = c) *)
(* Proof:
   Assume a <> d, b <> c. This is to show a = c, b = d.

   Step 1: establish identities.

   Claim: (a * d - b * c) * (a * d + b * c) = (d * d - b * b) * p
   Proof: Note a * a = p - b * b         by p = a * a + b * b
           and c * c = p - d * d         by p = c * c + d * d

            (a * d - b * c) * (a * d + b * c)
          = (a * d) * (a * d) - (b * c) * (b * c)        by difference_of_squares_alt
          = (a * a) * (d * d) - (c * c) * (b * b)
          = (p - b * b) * d * d - (p - d * d) * b * b    by substitution of a * a, c * c.
          = (p * d * d - b * b * d * d) - (p * b * b - d * d * b * b)
          = p * d * d - b * b * d * d + d * d * b * b - p * b * b    by SUB_SUB
          = p * d * d - p * b * b                                    by SUB_ADD
          = p * (d * d - b * b)

   Claim: (b * c - a * d) * (a * d + b * c) = (b * b - d * d) * p
   Proof: Note a * a = p - b * b         by p = a * a + b * b
           and c * c = p - d * d         by p = c * c + d * d

            (b * c - a * d) * (a * d + b * c)
          = (b * c) * (b * c) - (a * d) * (a * d)        by difference_of_squares_alt
          = (c * c) * (b * b) - (a * a) * (d * d)
          = (p - d * d) * b * b - (p - b * b) * d * d    by substitution of a * a, c * c.
          = (p * b * b - d * d * b * b) - (p * d * d - b * b * d * d)
          = p * b * b - d * d * b * b + b * b * d * d - p * d * d    by SUB_SUB
          = p * b * b - p * d * d                                    by SUB_ADD
          = p * (b * b - d * d)

   Step 2: establish properties
   Note p <> 0                                 by NOT_PRIME_0
    and a <> 0 /\ b <> 0 /\ c <> 0 /\ d <> 0   by prime_non_square, square_def

   Claim: a * d < p
   Proof: Note b * b <> 0 /\ c * c <> 0        by MULT_EQ_0
          Thus a * a < p /\ d * d < p          by p = a * a + b * b = c * c + d * d
           ==> a * a * (d * d) < p * p         by LT_MONO_MULT2
           ==>    (a * d) ** 2 < p ** 2        by EXP_2
           ==>           a * d < p             by EXP_EXP_LT_MONO

   Claim: b * c < p
   Proof: Note a * a <> 0 /\ d * d <> 0        by MULT_EQ_0
          Thus b * b < p /\ c * c < p          by p = a * a + b * b = c * c + d * d
           ==> b * b * (c * c) < p * p         by LT_MONO_MULT2
           ==>    (b * c) ** 2 < p ** 2        by EXP_2
           ==>           b * c < p             by EXP_EXP_LT_MONO

   Claim: coprime a b
   Proof: Let g = gcd a b.
          Then g divides a /\ g divides b       by GCD_IS_GREATEST_COMMON_DIVISOR
           ==> ?h k. (a = h * g) /\ (b = k * g) by divides_def
          Thus p = a * a + b * b
                 = (h * g) * (h * g) + (k * g) * (k * g)
                 = (g * g) * (h * h + k * k)
                 = (h ** 2 + k ** 2) * g ** 2
            so g ** 2 divides p         by divides_def
           ==> g ** 2 = 1               by prime_def, prime_non_square, square_def
           ==>      g = 1               by EXP_EQ_1

   Step 3:  make use of these facts and prime p.
   Note p divides (a * d - b * c) * (a * d + b * c)   by divides_def
    and p divides (b * c - a * d) * (a * d + b * c)   by divides_def
    ==> (p divides (a * d - b * c) /\ p divides (b * c - a * d))
     or (p divides (a * d + b * c)                    by euclid_prime

   Case 1: p divides (a * d - b * c) /\ p divides (b * c - a * d)
           Note (a * d - b * c) < p            by a * d < p, b * c <> 0
            and (b * c - a * d) < p            by b * c < p, a * d <> 0
           Thus  a * d - b * c = 0             by DIVIDES_LEQ_OR_ZERO
            and  b * c - a * d = 0             by DIVIDES_LEQ_OR_ZERO
            ==>  a * d = b * c                 by arithmetic
           Thus  a divides (b * c)
           but   coprime a b                   by gcd a b = 1
           Hence a divides c                   by euclid_coprime
            ==> ?t. c = t * a                  by divides_def
           Then d * a = b * (t * a) = (t * b) * a
            ==> d = t * b                      by EQ_MULT_RCANCEL
           Therefore,
                 p = c * c + d * d
                   = (t * a) * (t * a) + (t * b) * (t * b)
                   = t * t * (a * a + b * b)
                   = t * t * p
            Thus t * t = 1                     by EQ_MULT_RCANCEL, MULT_LEFT_1
             ==>     t = 1                     by MULT_EQ_1
            This gives c = a, d = b.

   Case 2: p divides (a * d + b * c)

           Note a * d + b * c < 2 * p          by a * d < p, b * c < p
            and a * d + b * c <> 0             by ADD_EQ_0, a * d <> 0, b * c <> 0
            Now ?k. a * d + b * c = k * p      by divides_def
                                  < 2 * p      by above, and k <> 0.
           Thus k = 1, or a * d + b * c = p    [3]

           To apply the four_squares_identity,
           If b * d <= a * c,
              p ** 2
            = p * p                                        by EXP_2
            = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)        by given
            = (a * d + b * c) ** 2 + (a * c - b * d) ** 2  by four_squares_identity
            = p ** 2 + (a * c - b * d) ** 2                by [3]
           Thus (a * c - b * d) ** 2 = 0     by EQ_ADD_LCANCEL
            ==> a * c - b * d = 0            by EXP_EQ_0
             or a * c <= b * d
            ==>  a * c = b * d               by b * d <= a * c
             so b divides a * c              by divides_def
            but coprime a b                  by gcd a b = 1
            ==> b divides c                  by euclid_coprime
            ==> ?t . c = t * b               by divides_def
           then      d = t * a
           giving    t = 1, or c = b, d = a.
           This contradicts a <> d, b <> c.

           If ~(b * d <= a * c), then a * c < b * d, implies a * c <= b * d.
              p ** 2
            = p * p                                        by EXP_2
            = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)        by given
            = (a * d + b * c) ** 2 + (b * d - a * c) ** 2  by four_squares_identity_alt
            = p ** 2 + (b * d - a * c) ** 2                by [3]
           Thus (b * d - a * c) ** 2 = 0     by EQ_ADD_LCANCEL
            ==> b * d - a * c = 0            by EXP_EQ_0
            ==>  b * d <= a * c
             or   b * d = a * c              by a * c <= b * d
             so b divides a * c              by divides_def
            but coprime a b                  by gcd a b = 1
            ==> b divides c                  by euclid_coprime
            ==> ?t . c = t * b               bu divides_def
            then     d = t * a
           giving    t = 1, or c = b, d = a.
           This contradicts a <> d, b <> c.

*)
Theorem fermat_two_squares_unique:
  !p. prime p ==>
      !a b c d. p = a ** 2 + b ** 2 /\ p = c ** 2 + d ** 2 ==>
                (a = c /\ b = d) \/ (a = d /\ b = c)
Proof
  rpt strip_tac >>
  Cases_on `(a = d /\ b = c)` >-
  simp[] >>
  simp[] >>
  `(a * d - b * c) * (a * d + b * c) = (d * d - b * b) * p` by
  (`a * a = p - b * b` by simp[] >>
  `c * c = p - d * d` by simp[] >>
  `(a * d - b * c) * (a * d + b * c) = (a * d) * (a * d) - (b * c) * (b * c)` by rw[difference_of_squares_alt] >>
  `_ = (a * a) * (d * d) - (c * c) * (b * b)` by fs[] >>
  `_ = (p - b * b) * d * d - (p - d * d) * b * b` by fs[] >>
  `_ = (p * d * d - b * b * d * d) - (p * b * b - d * d * b * b)` by decide_tac >>
  `_ = p * d * d - b * b * d * d + d * d * b * b - p * b * b` by simp[] >>
  `_ = p * d * d - p * b * b` by simp[] >>
  decide_tac) >>
  `(b * c - a * d) * (a * d + b * c) = (b * b - d * d) * p` by
    (`a * a = p - b * b` by simp[] >>
  `c * c = p - d * d` by simp[] >>
  `(b * c - a * d) * (a * d + b * c) = (b * c) * (b * c) - (a * d) * (a * d)` by rw[difference_of_squares_alt] >>
  `_ = (c * c) * (b * b) - (a * a) * (d * d)` by fs[] >>
  `_ = (p - d * d) * b * b - (p - b * b) * d * d` by fs[] >>
  `_ = (p * b * b - d * d * b * b) - (p * d * d - b * b * d * d)` by simp[] >>
  `_ = p * b * b - d * d * b * b + b * b * d * d - p * d * d` by simp[SUB_SUB] >>
  `_ = p * b * b - p * d * d` by simp[] >>
  simp[]) >>
  `p <> 0` by metis_tac[NOT_PRIME_0] >>
  `a <> 0 /\ b <> 0 /\ c <> 0 /\ d <> 0` by
      (`0 ** 2 = 0` by rw[] >>
  metis_tac[prime_non_square, square_def, EXP_2, ADD_CLAUSES]) >>
  `a * d < p` by
        (`p = a * a + b * b` by simp[] >>
  `p = c * c + d * d` by simp[] >>
  `b * b <> 0 /\ c * c <> 0` by rw[MULT_EQ_0] >>
  `a * a < p /\ d * d < p` by rw[] >>
  `a * a * (d * d) < p * p` by rw[LT_MONO_MULT2] >>
  `a * a * (d * d) = (a * d) * (a * d)` by simp[] >>
  metis_tac[EXP_EXP_LT_MONO, EXP_2, DECIDE``0 < 2``]) >>
  `b * c < p` by
          (`p = a * a + b * b` by simp[] >>
  `p = c * c + d * d` by simp[] >>
  `a * a <> 0 /\ d * d <> 0` by rw[MULT_EQ_0] >>
  `b * b < p /\ c * c < p` by rw[] >>
  `b * b * (c * c) < p * p` by rw[LT_MONO_MULT2] >>
  `b * b * (c * c) = (b * c) * (b * c)` by simp[] >>
  metis_tac[EXP_EXP_LT_MONO, EXP_2, DECIDE``0 < 2``]) >>
  `gcd a b = 1` by
            (qabbrev_tac `g = gcd a b` >>
  `g divides a /\ g divides b` by rw[GCD_IS_GREATEST_COMMON_DIVISOR, Abbr`g`] >>
  `?h k. (a = h * g) /\ (b = k * g)` by metis_tac[divides_def] >>
  `p = a * a + b * b` by fs[] >>
  `_ = (h * g) * (h * g) + (k * g) * (k * g)` by metis_tac[] >>
  `_ = (g * g) * (h * h + k * k)` by simp[] >>
  `_ = (h ** 2 + k ** 2) * g ** 2` by simp[] >>
  `g ** 2 divides p` by metis_tac[divides_def] >>
  `g ** 2 = 1` by metis_tac[prime_def, prime_non_square, square_def, EXP_2] >>
  metis_tac[EXP_EQ_1, DECIDE``2 <> 0``]) >>
  `p divides (a * d - b * c) * (a * d + b * c)` by metis_tac[divides_def] >>
  `p divides (b * c - a * d) * (a * d + b * c)` by metis_tac[divides_def] >>
  `(p divides (a * d - b * c) /\ p divides (b * c - a * d)) \/ p divides (a * d + b * c)` by metis_tac[euclid_prime] >| [
    `b * c <> 0 /\ a * d <> 0` by metis_tac[MULT_EQ_0] >>
    `(a * d - b * c) < p /\ (b * c - a * d) < p` by fs[] >>
    `(a * d - b * c = 0) /\ (b * c - a * d = 0)` by metis_tac[DIVIDES_LEQ_OR_ZERO, NOT_LESS] >>
    `b * c = d * a` by fs[] >>
    `a divides (b * c)` by metis_tac[divides_def] >>
    `a divides c` by metis_tac[euclid_coprime, GCD_SYM] >>
    `?t. c = t * a` by metis_tac[divides_def] >>
    `d * a = b * (t * a)` by fs[] >>
    `_ = (t * b) * a` by fs[] >>
    `d = t * b` by metis_tac[EQ_MULT_RCANCEL] >>
    `p = c * c + d * d` by fs[] >>
    `_ = (t * a) * (t * a) + (t * b) * (t * b)` by metis_tac[] >>
    `_ = t * t * (a * a + b * b)` by fs[] >>
    `_ = t * t * p` by fs[] >>
    `t * t = 1` by metis_tac[EQ_MULT_RCANCEL, MULT_LEFT_1] >>
    metis_tac[MULT_EQ_1, MULT_LEFT_1],
    `a * d + b * c = p` by
  (`a * d + b * c < 2 * p` by fs[] >>
    `?k. a * d + b * c = k * p` by metis_tac[divides_def] >>
    `k <> 0` by metis_tac[MULT, ADD_EQ_0, MULT_EQ_0] >>
    `k < 2` by fs[] >>
    `k = 1` by fs[] >>
    simp[]) >>
    Cases_on `b * d <= a * c` >| [
      `p ** 2 = p * p` by simp[] >>
      `_ = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = (a * d + b * c) ** 2 + (a * c - b * d) ** 2` by fs[four_squares_identity] >>
      `_ = p ** 2 + (a * c - b * d) ** 2` by fs[] >>
      `(a * c - b * d) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `a * c - b * d = 0` by metis_tac[EXP_EQ_0, DECIDE``0 < 2``] >>
      `a * c = d * b` by fs[] >>
      `b divides a * c` by metis_tac[divides_def] >>
      `b divides c` by metis_tac[euclid_coprime] >>
      `?t . c = t * b` by metis_tac[divides_def] >>
      `d * b = a * (t * b)` by fs[] >>
      `_ = (t * a) * b` by fs[] >>
      `d = t * a` by metis_tac[EQ_MULT_RCANCEL] >>
      `p = c * c + d * d` by fs[] >>
      `_ = (t * b) * (t * b) + (t * a) * (t * a)` by metis_tac[] >>
      `_ = t * t * (b * b + a * a)` by simp[] >>
      `_ = t * t * p` by gs[] >>
      `t * t = 1` by metis_tac[EQ_MULT_RCANCEL, MULT_LEFT_1] >>
      `t = 1` by metis_tac[MULT_EQ_1] >>
      fs[],
      `p ** 2 = p * p` by simp[] >>
      `_ = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = (a * d + b * c) ** 2 + (b * d - a * c) ** 2` by fs[four_squares_identity_alt] >>
      `_ = p ** 2 + (b * d - a * c) ** 2` by fs[] >>
      `(b * d - a * c) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `b * d - a * c = 0` by metis_tac[EXP_EQ_0, DECIDE``0 < 2``] >>
      `a * c = d * b` by fs[] >>
      `b divides a * c` by metis_tac[divides_def] >>
      `b divides c` by metis_tac[euclid_coprime] >>
      `?t . c = t * b` by metis_tac[divides_def] >>
      `d * b = a * (t * b)` by fs[] >>
      `_ = (t * a) * b` by fs[] >>
      `d = t * a` by metis_tac[EQ_MULT_RCANCEL] >>
      `p = c * c + d * d` by fs[] >>
      `_ = (t * b) * (t * b) + (t * a) * (t * a)` by metis_tac[] >>
      `_ = t * t * (b * b + a * a)` by fs[] >>
      `_ = t * t * p` by fs[] >>
      `t * t = 1` by metis_tac[EQ_MULT_RCANCEL, MULT_LEFT_1] >>
      `t = 1` by metis_tac[MULT_EQ_1] >>
      fs[]
    ]
  ]
QED

(* This is fantastic! *)

(* Theorem: prime p ==> prime p ==>
   !a b c d. ODD a /\ EVEN b /\ p = a ** 2 + b ** 2 /\
             ODD c /\ EVEN d /\ p = c ** 2 + d ** 2 ==> (a = c /\ b = d) *)
(* Proof:
   Note (a = c /\ b = d) \/ (a = d /\ b = c)   by fermat_two_squares_unique
   But (a <> d) \/ (b <> c)                    by EVEN_ODD
    so (a = c /\ b = d).
*)
Theorem fermat_two_squares_unique_odd_even:
  !p. prime p ==>
      !a b c d. ODD a /\ EVEN b /\ p = a ** 2 + b ** 2 /\
                ODD c /\ EVEN d /\ p = c ** 2 + d ** 2 ==> (a = c /\ b = d)
Proof
  ntac 7 strip_tac >>
  `(a = c /\ b = d) \/ (a = d /\ b = c)` by fs[fermat_two_squares_unique] >-
  simp[] >>
  fs[EVEN_ODD]
QED

(* Theorem: !p. prime p /\ p MOD 4 = 1 ==>
            ?!u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2 *)
(* Proof:
   Existence part    by fermat_two_squares_exists_odd_even
   Uniqueness part   by fermat_two_squares_unique_odd_even
*)
Theorem fermat_two_squares_thm:
  !p. prime p /\ p MOD 4 = 1 ==>
      ?!u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2
Proof
  rw[Once EXISTS_UNIQUE_THM] >| [
    `?u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2` by rw[fermat_two_squares_exists_odd_even] >>
    qexists_tac `u` >>
    rw[Once EXISTS_UNIQUE_THM],
    metis_tac[fermat_two_squares_unique_odd_even]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Two Squares Theorem (bidirectional)                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: prime p ==>
            (p MOD 4 = 1 <=> ?!u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2) *)
(* Proof:
   If part: p MOD 4 = 1 ==> ?!u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2
      This is true                       by fermat_two_squares_thm
   Only-if part: ?!u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2 ==> p MOD 4 = 1
      Note ?u v. ODD u /\ EVEN v /\
                 p = u ** 2 + v ** 2     by EXISTS_UNIQUE_THM
       but ODD (u ** 2)                  by ODD_EXP
       and EVEN (v ** 2)                 by EVEN_EXP
      Thus ODD p                         by ODD_ADD, EVEN_ODD
        so p MOD 4 = 1 or p MOD 4 = 3    by mod_4_odd
       but p MOD 4 <> 3                  by mod_4_not_squares
       ==> p MOD 4 = 1
*)
Theorem fermat_two_squares_iff:
  !p. prime p ==> (p MOD 4 = 1 <=> ?!u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2)
Proof
  rw[EQ_IMP_THM] >-
  rw[fermat_two_squares_thm] >>
  `?u v. ODD u /\ EVEN v /\ p = u ** 2 + v ** 2` by metis_tac[EXISTS_UNIQUE_THM] >>
  `ODD (u ** 2)` by rw[ODD_EXP] >>
  `EVEN (v ** 2)` by rw[EVEN_EXP] >>
  `ODD p` by fs[ODD_ADD, EVEN_ODD] >>
  `p MOD 4 = 1 \/ (p MOD 4 = 3)` by fs[mod_4_odd] >>
  fs[mod_4_not_squares]
QED


(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
