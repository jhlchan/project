(* ------------------------------------------------------------------------- *)
(* Windmills of the minds: Fermat's Two Squares Theorem (part 1)             *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "part1";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)
(* val _ = load "windmillTheory"; *)
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory; (* for divides_def *)
open gcdTheory; (* for GCD_IS_GREATEST_COMMON_DIVISOR *)
open logPowerTheory; (* for prime_non_square *)

open helperTwosqTheory;
open windmillTheory;


(* ------------------------------------------------------------------------- *)
(* Windmills of the minds (part 1) Documentation                             *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   Alpha-partition of (mills n):
   mills_alpha_same_def    |- !n. mills_alpha_same n = {(x,y,z) | (x,y,z) IN mills n /\ x = y}
   mills_alpha_less_def    |- !n. mills_alpha_less n = {(x,y,z) | (x,y,z) IN mills n /\ x < y}
   mills_alpha_more_def    |- !n. mills_alpha_more n = {(x,y,z) | (x,y,z) IN mills n /\ y < x}
   mills_alpha_less_subset |- !n. mills_alpha_less n SUBSET mills n
   mills_alpha_same_subset |- !n. mills_alpha_same n SUBSET mills n
   mills_alpha_more_subset |- !n. mills_alpha_more n SUBSET mills n
   mills_alpha_less_element|- !n x y z. (x,y,z) IN mills_alpha_less n ==> (x,y,z) IN mills n /\ x < y
   mills_alpha_same_element|- !n x y z. (x,y,z) IN mills_alpha_same n ==> (x,y,z) IN mills n /\ x = y
   mills_alpha_more_element|- !n x y z. (x,y,z) IN mills_alpha_more n ==> (x,y,z) IN mills n /\ y < x
   mills_alpha_inter       |- !n. mills_alpha_less n INTER mills_alpha_more n = {}
   mills_alpha_union       |- !n. mills_alpha_less n UNION mills_alpha_more n = mills n DIFF mills_alpha_same n
   mills_alpha_same_sing   |- !p. prime p /\ p MOD 4 = 1 ==> mills_alpha_same p = {(1,1,p DIV 4)}

   Beta-partition of (mills n):
   mills_beta_same_def     |- !n. mills_beta_same n = {(x,y,z) | (x,y,z) IN mills n /\ y = z}
   mills_beta_less_def     |- !n. mills_beta_less n = {(x,y,z) | (x,y,z) IN mills n /\ y < z}
   mills_beta_more_def     |- !n. mills_beta_more n = {(x,y,z) | (x,y,z) IN mills n /\ z < y}
   mills_beta_less_subset  |- !n. mills_beta_less n SUBSET mills n
   mills_beta_same_subset  |- !n. mills_beta_same n SUBSET mills n
   mills_beta_more_subset  |- !n. mills_beta_more n SUBSET mills n
   mills_beta_less_element |- !n x y z. (x,y,z) IN mills_beta_less n <=> (x,y,z) IN mills n /\ y < z
   mills_beta_same_element |- !n x y z. (x,y,z) IN mills_beta_same n <=> (x,y,z) IN mills n /\ y = z
   mills_beta_more_element |- !n x y z. (x,y,z) IN mills_beta_more n <=> (x,y,z) IN mills n /\ z < y
   mills_beta_inter        |- !n. mills_beta_less n INTER mills_beta_more n = {}
   mills_beta_union        |- !n. mills_beta_less n UNION mills_beta_more n =
                                  mills n DIFF mills_beta_same n

   More Beta-partition sets of (mills n):
   mills_beta_less_more_bij|- !n. BIJ flip (mills_beta_less n) (mills_beta_more n)
   mills_beta_more_less_card
        |- !n. ~square n ==>
               CARD (mills n DIFF mills_beta_same n) = 2 * CARD (mills_beta_less n)
   mills_beta_card
        |- !n. ~square n ==>
               CARD (mills n) = 2 * CARD (mills_beta_less n) + CARD (mills_beta_same n)

   More Alpha-partition sets of (mills n):
   mills_alpha_less_left_def
                   |- !n. mills_alpha_less_left n =
                          {(x,y,z) | (x,y,z) IN mills_alpha_less n /\ x < y - z}
   mills_alpha_less_middle_def
                   |- !n. mills_alpha_less_middle n =
                          {(x,y,z) | (x,y,z) IN mills_alpha_less n /\ ~(x < y - z)}
   mills_alpha_more_middle_def
                   |- !n. mills_alpha_more_middle n =
                          {(x,y,z) | (x,y,z) IN mills_alpha_more n /\ x < 2 * y}
   mills_alpha_more_right_def
                   |- !n. mills_alpha_more_right n =
                          {(x,y,z) | (x,y,z) IN mills_alpha_more n /\ ~(x < 2 * y)}
   mills_alpha_less_left_element
                   |- !n x y z. (x,y,z) IN mills_alpha_less_left n ==>
                                (x,y,z) IN mills_alpha_less n /\ x < y - z
   mills_alpha_less_middle_element
                   |- !n x y z. (x,y,z) IN mills_alpha_less_middle n ==>
                                (x,y,z) IN mills_alpha_less n /\ ~(x < y - z)
   mills_alpha_more_middle_element
                   |- !n x y z. (x,y,z) IN mills_alpha_more_middle n ==>
                                (x,y,z) IN mills_alpha_more n /\ x < 2 * y
   mills_alpha_more_right_element
                   |- !n x y z. (x,y,z) IN mills_alpha_more_right n ==>
                                (x,y,z) IN mills_alpha_more n /\ ~(x < 2 * y)
   mills_alpha_less_no_right |- !x y z. x < y ==> x < 2 * y
   mills_alpha_more_no_left  |- !x y z. y < x ==> ~(x < y - z)
   mills_alpha_less_inter    |- !n. mills_alpha_less_left n INTER mills_alpha_less_middle n = {}
   mills_alpha_less_union    |- !n. mills_alpha_less_left n UNION mills_alpha_less_middle n =
                                    mills_alpha_less n
   mills_alpha_more_inter    |- !n. mills_alpha_more_middle n INTER mills_alpha_more_right n = {}
   mills_alpha_more_union    |- !n. mills_alpha_more_middle n UNION mills_alpha_more_right n =
                                    mills_alpha_more n

   Zagier bijection with (mills n):
   zagier_beta_less_to_more
                   |- !n x y z. n MOD 4 <> 0 /\ (x,y,z) IN mills_alpha_less_left n ==>
                                zagier (x,y,z) IN mills_alpha_more_right n
   zagier_beta_middle_to_middle
                   |- !n x y z. n MOD 4 <> 0 /\ (x,y,z) IN mills_alpha_less_middle n ==>
                                zagier (x,y,z) IN mills_alpha_more_middle n
   zagier_beta_more_to_less
                   |- !n x y z. ~square n /\ (x,y,z) IN mills_alpha_more_right n ==>
                                zagier (x,y,z) IN mills_alpha_less_left n
   zagier_beta_middle_back_to_middle
                   |- !n x y z. (x,y,z) IN mills_alpha_more_middle n ==>
                                zagier (x,y,z) IN mills_alpha_less_middle n
   mills_alpha_less_left_more_right_bij
                   |- !n. ~square n /\ n MOD 4 <> 0 ==>
                          BIJ zagier (mills_alpha_less_left n) (mills_alpha_more_right n)
   mills_alpha_less_middle_more_middle_bij
                   |- !n. ~square n /\ n MOD 4 <> 0 ==>
                          BIJ zagier (mills_alpha_less_middle n) (mills_alpha_more_middle n)
   mills_alpha_less_more_bij
                   |- !n. ~square n /\ n MOD 4 <> 0 ==>
                          BIJ zagier (mills_alpha_less n) (mills_alpha_more n)
   mills_alpha_more_less_card
                   |- !n. ~square n /\ n MOD 4 <> 0 ==>
                          CARD (mills n DIFF mills_alpha_same n) = 2 * CARD (mills_alpha_less n)
   mills_alpha_card |- !n. ~square n /\ n MOD 4 <> 0 ==>
                          CARD (mills n) = 2 * CARD (mills_alpha_less n) + CARD (mills_alpha_same n)

   Fermat's Two Squares Theorem:
   fermat_two_squares_exists |- !p. prime p /\ p MOD 4 = 1 ==>
                                    ?u v. p = u ** 2 + v ** 2
   fermat_two_squares_unique |- !p. prime p ==> !a b c d.
              p = a ** 2 + b ** 2 /\ p = c ** 2 + d ** 2 ==>
              a = c /\ b = d \/ a = d /\ b = c
   fermat_two_squares_unique_thm
                             |- !p a b c d. prime p /\ p = a ** 2 + b ** 2 /\
                                            p = c ** 2 + d ** 2 ==> {a; b} = {c; d}
   fermat_two_squares_unique_odd_even
                             |- !p. prime p ==>
                                !a b c d. ODD a /\ EVEN b /\ p = a ** 2 + b ** 2 /\
                                          ODD c /\ EVEN d /\ p = c ** 2 + d ** 2 ==>
                                          a = c /\ b = d

*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Alpha-partition of (mills n)                                              *)
(* ------------------------------------------------------------------------- *)

(* Partition mills of n with (x,y,z) by comparing x and y. *)

Definition mills_alpha_same_def[nocompute]:
    mills_alpha_same n = {(x,y,z) | (x, y, z) IN mills n /\ x = y}
End
Definition mills_alpha_less_def[nocompute]:
    mills_alpha_less n = {(x,y,z) | (x, y, z) IN mills n /\ x < y}
End
Definition mills_alpha_more_def[nocompute]:
    mills_alpha_more n = {(x,y,z) | (x, y, z) IN mills n /\ y < x}
End
(* use [nocompute] as this is not effective. *)

(* Theorem: (mills_alpha_less n) SUBSET (mills n) *)
(* Proof: by mills_alpha_less_def, SUBSET_DEF *)
Theorem mills_alpha_less_subset:
  !n. (mills_alpha_less n) SUBSET (mills n)
Proof
  rw[mills_alpha_less_def, SUBSET_DEF] >>
  simp[]
QED

(* Theorem: (mills_alpha_same n) SUBSET (mills n) *)
(* Proof: by mills_alpha_same_def, SUBSET_DEF *)
Theorem mills_alpha_same_subset:
  !n. (mills_alpha_same n) SUBSET (mills n)
Proof
  rw[mills_alpha_same_def, SUBSET_DEF] >>
  simp[]
QED

(* Theorem: (mills_alpha_more n) SUBSET (mills n) *)
(* Proof: by mills_alpha_more_def, SUBSET_DEF *)
Theorem mills_alpha_more_subset:
  !n. (mills_alpha_more n) SUBSET (mills n)
Proof
  rw[mills_alpha_more_def, SUBSET_DEF] >>
  simp[]
QED

(* Theorem: (x, y, z) IN mills_alpha_less n ==> (x, y, z) IN mills n /\ x < y *)
(* Proof: by mills_alpha_less_def *)
Theorem mills_alpha_less_element:
  !n x y z. (x, y, z) IN mills_alpha_less n ==> (x, y, z) IN mills n /\ x < y
Proof
  rw[mills_alpha_less_def]
QED

(* Theorem: (x, y, z) IN mills_alpha_same n ==> (x, y, z) IN mills n /\ x = y *)
(* Proof: mills_alpha_same_def *)
Theorem mills_alpha_same_element:
  !n x y z. (x, y, z) IN mills_alpha_same n ==> (x, y, z) IN mills n /\ x = y
Proof
  rw[mills_alpha_same_def]
QED

(* Theorem: (x, y, z) IN mills_alpha_more n ==> (x, y, z) IN mills n /\ y < x *)
(* Proof: by mills_alpha_more_def *)
Theorem mills_alpha_more_element:
  !n x y z. (x, y, z) IN mills_alpha_more n ==> (x, y, z) IN mills n /\ y < x
Proof
  rw[mills_alpha_more_def]
QED

(* Theorem: (mills_alpha_less n) INTER (mills_alpha_more n) = EMPTY *)
(* Proof: by mills_alpha_less_def, mills_alpha_more_def, IN_INTER. *)
Theorem mills_alpha_inter:
  !n. (mills_alpha_less n) INTER (mills_alpha_more n) = EMPTY
Proof
  rw[mills_alpha_less_def, mills_alpha_more_def, EXTENSION, pairTheory.FORALL_PROD] >>
  decide_tac
QED

(* Theorem: (mills_alpha_less n) UNION (mills_alpha_more n) =
            (mills n) DIFF (mills_alpha_same n) *)
(* Proof: by mills_alpha_same_def, mills_alpha_less_def, mills_alpha_more_def, IN_UNION, IN_DIFF. *)
Theorem mills_alpha_union:
  !n. (mills_alpha_less n) UNION (mills_alpha_more n) =
      (mills n) DIFF (mills_alpha_same n)
Proof
  rw[mills_alpha_same_def, mills_alpha_less_def, mills_alpha_more_def, EXTENSION, pairTheory.FORALL_PROD] >>
  spose_not_then strip_assume_tac >>
  (fs[EQ_IMP_THM] >> fs[]) >>
  `p_1' = p_1` by decide_tac >>
  fs[]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==> mills_alpha_same p = {(1,1,p DIV 4)} *)
(* Proof:
   By mills_alpha_same_def, mills_def, this is to show:
    (?x z. t = (x,x,z) /\ p = windmill (x, x, z)) <=> (t = (1,1,p DIV 4))
   This is true             by windmill_trivial_prime
*)
Theorem mills_alpha_same_sing:
  !p. prime p /\ p MOD 4 = 1 ==> mills_alpha_same p = {(1,1,p DIV 4)}
Proof
  rw[mills_alpha_same_def, mills_def, EXTENSION] >>
  metis_tac[windmill_trivial_prime]
QED

(* ------------------------------------------------------------------------- *)
(* Beta-partition of (mills n)                                               *)
(* ------------------------------------------------------------------------- *)

(* Partition mills of n with (x,y,z) by comparing y and z. *)

Definition mills_beta_same_def[nocompute]:
    mills_beta_same n = {(x,y,z) | (x, y, z) IN mills n /\ y = z}
End
Definition mills_beta_less_def[nocompute]:
    mills_beta_less n = {(x,y,z) | (x, y, z) IN mills n /\ (y < z)}
End
Definition mills_beta_more_def[nocompute]:
    mills_beta_more n = {(x,y,z) | (x, y, z) IN mills n /\ (z < y)}
End
(* use [nocompute] as this is not effective. *)

(* Theorem: (mills_beta_less n) SUBSET (mills n) *)
(* Proof: by mills_beta_less_def, SUBSET_DEF *)
Theorem mills_beta_less_subset:
  !n. (mills_beta_less n) SUBSET (mills n)
Proof
  rw[mills_beta_less_def, SUBSET_DEF] >>
  simp[]
QED

(* Theorem: (mills_beta_same n) SUBSET (mills n) *)
(* Proof: by mills_beta_same_def, SUBSET_DEF *)
Theorem mills_beta_same_subset:
  !n. (mills_beta_same n) SUBSET (mills n)
Proof
  rw[mills_beta_same_def, SUBSET_DEF] >>
  simp[]
QED;

(* Theorem: (mills_beta_more n) SUBSET (mills n) *)
(* Proof: by mills_beta_more_def, SUBSET_DEF *)
Theorem mills_beta_more_subset:
  !n. (mills_beta_more n) SUBSET (mills n)
Proof
  rw[mills_beta_more_def, SUBSET_DEF] >>
  simp[]
QED

(* Theorem: (x, y, z) IN mills_beta_less n <=> (x, y, z) IN mills n /\ y < z *)
(* Proof: by mills_beta_less_def *)
Theorem mills_beta_less_element:
  !n x y z. (x, y, z) IN mills_beta_less n <=> (x, y, z) IN mills n /\ y < z
Proof
  rw[mills_beta_less_def]
QED

(* Theorem: (x, y, z) IN mills_beta_same n <=> (x, y, z) IN mills n /\ y = z *)
(* Proof: mills_beta_same_def *)
Theorem mills_beta_same_element:
  !n x y z. (x, y, z) IN mills_beta_same n <=> (x, y, z) IN mills n /\ y = z
Proof
  rw[mills_beta_same_def] >>
  metis_tac[]
QED

(* Theorem: (x, y, z) IN mills_beta_more n <=> (x, y, z) IN mills n /\ z < y *)
(* Proof: by mills_beta_more_def *)
Theorem mills_beta_more_element:
  !n x y z. (x, y, z) IN mills_beta_more n <=> (x, y, z) IN mills n /\ z < y
Proof
  rw[mills_beta_more_def]
QED

(* Theorem: (mills_beta_less n) INTER (mills_beta_more n) = EMPTY *)
(* Proof: by mills_beta_less_def, mills_beta_more_def, IN_INTER. *)
Theorem mills_beta_inter:
  !n. (mills_beta_less n) INTER (mills_beta_more n) = EMPTY
Proof
  rw[mills_beta_less_def, mills_beta_more_def, EXTENSION, pairTheory.FORALL_PROD] >>
  decide_tac
QED

(* Theorem: (mills_beta_less n) UNION (mills_beta_more n) =
            (mills n) DIFF (mills_beta_same n) *)
(* Proof: by mills_beta_same_def, mills_beta_less_def, mills_beta_more_def, IN_UNION, IN_DIFF. *)
Theorem mills_beta_union:
  !n. (mills_beta_less n) UNION (mills_beta_more n) =
       (mills n) DIFF (mills_beta_same n)
Proof
  rw[mills_beta_same_def, mills_beta_less_def, mills_beta_more_def, EXTENSION, pairTheory.FORALL_PROD] >>
  spose_not_then strip_assume_tac >>
  (fs[EQ_IMP_THM] >> fs[]) >>
  `p_1' = p_2` by decide_tac >>
  fs[]
QED

(* ------------------------------------------------------------------------- *)
(* More Beta-partition sets of (mills n)                                     *)
(* ------------------------------------------------------------------------- *)

(* Flip is a bijection between mills_beta_less and mills_beta_more *)

(* Theorem: BIJ flip (mills_beta_less n) (mills_beta_more n) *)
(* Proof:
   By BIJ_DEF, INJ_DEF, SURJ_DEF, mills_beta_less_def, mills_beta_more_def,
   this is to show:
   (1) (x,y,z) IN mills n /\ y < z ==>
       ?x' y' z'. (flip (x,y,z) = (x',y',z')) /\ (x',y',z') IN mills n /\ z' < y'
       Put x' = x, y' = z, z' = y, then true    by flip_def, mills_element_flip
   (2) flip (x,y,z) = flip (x',y',z') ==> (x = x') /\ (y = y') /\ (z = z')
       This is true                             by flip_def, pairTheory.PAIR_EQ
   (3) same as (1).
   (4) (x,y,z) IN mills n /\ z < y ==>
       ?t. (?x' y' z'. (t = (x',y',z')) /\ (x',y',z') IN mills n /\ y' < z') /\
           (flip t = (x,y,z))
       Put t = (x,z,y),
      then x' = x, y' = z, z' = y, and true     by flip, mills_element_flip
*)
Theorem mills_beta_less_more_bij:
  !n. BIJ flip (mills_beta_less n) (mills_beta_more n)
Proof
  rw[BIJ_DEF, INJ_DEF, SURJ_DEF, mills_beta_less_def, mills_beta_more_def] >-
  metis_tac[flip_def, mills_element_flip] >-
  metis_tac[flip_def, pairTheory.PAIR_EQ] >-
  metis_tac[flip_def, mills_element_flip] >>
  metis_tac[flip_def, mills_element_flip]
QED

(* Theorem: ~square n ==>
            CARD (mills n DIFF mills_beta_same n) = 2 * CARD (mills_beta_less n) *)
(* Proof:
   Note FINITE (mills n)       by mills_non_square_finite
   Let s = mills n DIFF mills_beta_same n,
       a = mills_beta_less n, b = mills_beta_more n.
   Note FINITE s               by FINITE_DIFF
    and s = a UNION b          by mills_beta_union
    and a INTER b = EMPTY      by mills_beta_inter
   also BIJ flip a b           by mills_beta_less_more_bij
    ==> CARD s = 2 * CARD a    by set_partition_bij_card
*)
Theorem mills_beta_more_less_card:
  !n. ~square n ==>
      CARD (mills n DIFF mills_beta_same n) = 2 * CARD (mills_beta_less n)
Proof
  metis_tac[mills_non_square_finite, mills_beta_union, mills_beta_inter,
            mills_beta_less_more_bij, FINITE_DIFF, set_partition_bij_card]
QED

(* Theorem: ~square n ==>
            CARD (mills n) = 2 * CARD (mills_beta_less n) + CARD (mills_beta_same n) *)
(* Proof:
   Note FINITE (mills n)                   by mills_non_square_finite
   Let s = mills n, t = mills_beta_same n, b = mills_beta_less n.
   Note t SUBSET s                         by mills_beta_same_subset
    and CARD t <= CARD s                   by CARD_SUBSET
    ==> CARD (s DIFF t) = CARD s - CARD t  by SUBSET_DIFF_CARD
   Thus CARD s = CARD (s DIFF t) + CARD t  by arithmetic
               = 2 * CARD b + CARD t       by mills_beta_more_less_card
*)
Theorem mills_beta_card:
  !n. ~square n ==>
      CARD (mills n) = 2 * CARD (mills_beta_less n) + CARD (mills_beta_same n)
Proof
  rpt strip_tac >>
  `FINITE (mills n)` by fs[mills_non_square_finite] >>
  imp_res_tac mills_beta_more_less_card >>
  qabbrev_tac `s = mills n` >>
  qabbrev_tac `t = mills_beta_same n` >>
  `t SUBSET s` by rw[mills_beta_same_subset, Abbr`s`, Abbr`t`] >>
  `CARD t <= CARD s` by rw[CARD_SUBSET] >>
  `CARD (s DIFF t) = CARD s - CARD t` by rw[GSYM SUBSET_DIFF_CARD] >>
  decide_tac
QED

(* ------------------------------------------------------------------------- *)
(* More Alpha-partition sets of (mills n)                                    *)
(* ------------------------------------------------------------------------- *)

(* Zagier is a bijection between mills_alpha_less and mills_alpha_more *)

Definition mills_alpha_less_left_def[nocompute]:
    mills_alpha_less_left n =
          {(x,y,z) | (x, y, z) IN mills_alpha_less n /\ x < y - z}
End
Definition mills_alpha_less_middle_def[nocompute]:
    mills_alpha_less_middle n =
          {(x,y,z) | (x, y, z) IN mills_alpha_less n /\ ~(x < y - z)}
End
Definition mills_alpha_more_middle_def[nocompute]:
    mills_alpha_more_middle n =
          {(x,y,z) | (x, y, z) IN mills_alpha_more n /\ x < 2 * y}
End
Definition mills_alpha_more_right_def[nocompute]:
    mills_alpha_more_right n =
          {(x,y,z) | (x, y, z) IN mills_alpha_more n /\ ~(x < 2 * y)}
End
(* use [nocompute] as this is not effective. *)

(* Theorem: (x, y, z) IN mills_alpha_less_left n ==>
            (x, y, z) IN mills_alpha_less n /\ x < y - z *)
(* Proof: by mills_alpha_less_left_def *)
Theorem mills_alpha_less_left_element:
  !n x y z. (x, y, z) IN mills_alpha_less_left n ==>
            (x, y, z) IN mills_alpha_less n /\ x < y - z
Proof
  rw[mills_alpha_less_left_def]
QED

(* Theorem: (x, y, z) IN mills_alpha_less_middle n ==>
            (x, y, z) IN mills_alpha_less n /\ ~(x < y - z) *)
(* Proof: mills_alpha_less_middle_def *)
Theorem mills_alpha_less_middle_element:
  !n x y z. (x, y, z) IN mills_alpha_less_middle n ==>
            (x, y, z) IN mills_alpha_less n /\ ~(x < y - z)
Proof
  rw[mills_alpha_less_middle_def]
QED

(* Theorem: (x, y, z) IN mills_alpha_more_middle n ==>
            (x, y, z) IN mills_alpha_more n /\ x < 2 * y *)
(* Proof: by mills_alpha_more_middle_def *)
Theorem mills_alpha_more_middle_element:
  !n x y z. (x, y, z) IN mills_alpha_more_middle n ==>
            (x, y, z) IN mills_alpha_more n /\ x < 2 * y
Proof
  rw[mills_alpha_more_middle_def]
QED

(* Theorem: (x, y, z) IN mills_alpha_more_right n ==>
            (x, y, z) IN mills_alpha_more n /\ ~(x < 2 * y) *)
(* Proof: by mills_alpha_more_right_def *)
Theorem mills_alpha_more_right_element:
  !n x y z. (x, y, z) IN mills_alpha_more_right n ==>
            (x, y, z) IN mills_alpha_more n /\ ~(x < 2 * y)
Proof
  rw[mills_alpha_more_right_def]
QED

(* Theorem: x < y ==> x < 2 * y *)
(* Proof: x < y ==> x < y + y = 2 * y *)
Theorem mills_alpha_less_no_right:
  !x y z. x < y ==> x < 2 * y
Proof
  decide_tac
QED

(* Theorem: y < x ==> ~(x < y - z) *)
(* Proof: by contraction: y < x < y - z ==> y < y - z, which is false. *)
Theorem mills_alpha_more_no_left:
  !x y z. y < x ==> ~(x < y - z)
Proof
  decide_tac
QED

(* Theorem: (mills_alpha_less_left n) INTER (mills_alpha_less_middle n) = EMPTY *)
(* Proof: by mills_alpha_less_left_def, mills_alpha_less_middle_def *)
Theorem mills_alpha_less_inter:
  !n. (mills_alpha_less_left n) INTER (mills_alpha_less_middle n) = EMPTY
Proof
  rw[mills_alpha_less_left_def, mills_alpha_less_middle_def, EXTENSION, pairTheory.FORALL_PROD] >>
  metis_tac[]
QED

(* Theorem: (mills_alpha_less_left n) UNION (mills_alpha_less_middle n) = mills_alpha_less n *)
(* Proof: by mills_alpha_less_left_def, mills_alpha_less_middle_def *)
Theorem mills_alpha_less_union:
  !n. (mills_alpha_less_left n) UNION (mills_alpha_less_middle n) = mills_alpha_less n
Proof
  rw[mills_alpha_less_left_def, mills_alpha_less_middle_def, EXTENSION, pairTheory.FORALL_PROD] >>
  metis_tac[]
QED

(* Theorem: (mills_alpha_more_middle_def n) INTER (mills_alpha_more_right_def n) = EMPTY *)
(* Proof: by mills_alpha_more_middle_def, mills_alpha_more_right_def *)
Theorem mills_alpha_more_inter:
  !n. (mills_alpha_more_middle n) INTER (mills_alpha_more_right n) = EMPTY
Proof
  rw[mills_alpha_more_middle_def, mills_alpha_more_right_def, EXTENSION, pairTheory.FORALL_PROD] >>
  metis_tac[]
QED

(* Theorem: (mills_alpha_more_middle n) UNION (mills_alpha_more_right n) = mills_alpha_more n *)
(* Proof: by mills_alpha_more_middle_def, mills_alpha_more_right_def *)
Theorem mills_alpha_more_union:
  !n. (mills_alpha_more_middle n) UNION (mills_alpha_more_right n) = mills_alpha_more n
Proof
  rw[mills_alpha_more_middle_def, mills_alpha_more_right_def, EXTENSION, pairTheory.FORALL_PROD] >>
  metis_tac[]
QED

(* ------------------------------------------------------------------------- *)
(* Zagier bijection with (mills n)                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: n MOD 4 <> 0 /\ (x, y, z) IN mills_alpha_less_left n ==>
            zagier (x, y, z) IN mills_alpha_more_right n *)
(* Proof:
   By mills_alpha_less_left_def, mills_alpha_more_right_def,
      mills_alpha_less_def, mills_alpha_more_def, this is to show:
      ?x' y' z'. (zagier (x,y,z) = (x',y',z')) /\
                 ((x',y',z') IN mills n /\ y' < x') /\ ~(x' < 2 * y')
   In this case, zagier (x, y, z) = (x + 2 * z, z, y - z - x).
   Let x' = x + 2 * z, y' = z, z' = y - z - x.
   Since 2 * z <= x + 2 * z, so 2 * y' <= x', or ~(x' < 2 * y').
   Since (n MOD 4 <> 0), x <> 0     by mills_with_no_mind
    Thus z < x + 2 * z, or y' < x'.
     and zagier (x,y,z) IN mills n  by zagier_closure
*)
Theorem zagier_beta_less_to_more:
  !n x y z. n MOD 4 <> 0 /\ (x, y, z) IN mills_alpha_less_left n ==>
            zagier (x, y, z) IN mills_alpha_more_right n
Proof
  rw[mills_alpha_less_left_def, mills_alpha_more_right_def, mills_alpha_less_def, mills_alpha_more_def] >>
  qexists_tac `x + 2 * z` >>
  qexists_tac `z` >>
  qexists_tac `y - z - x` >>
  `~(x + 2 * z < 2 * z)` by decide_tac >>
  `x <> 0` by metis_tac[mills_with_no_mind] >>
  `z < x + 2 * z` by decide_tac >>
  metis_tac[zagier_def, zagier_closure]
QED

(* Theorem: n MOD 4 <> 0 /\ (x, y, z) IN mills_alpha_less_middle n ==>
            zagier (x, y, z) IN mills_alpha_more_middle n *)
(* Proof:
   By mills_alpha_less_middle_def, mills_alpha_more_middle_def,
      mills_alpha_less_def, mills_alpha_more_def, this is to show:
      ?x' y' z'. (zagier (x,y,z) = (x',y',z')) /\
                 ((x',y',z') IN mills n /\ y' < x') /\ x' < 2 * y'
   Note x < y ==> x < 2 * y.
   In this case, zagier (x, y, z) = (2 * y - x, y, x + z - y).
   Let x' = 2 * y - x, y' = y, z' = x + z - y.
   Since y < 2 * y - x, so y' < x'.
   Since (n MOD 4 <> 0), x <> 0     by mills_with_no_mind
    Thus 2 * y - x < 2 * y, or x' < 2 * y'.
     and zagier (x,y,z) IN mills n  by zagier_closure
*)
Theorem zagier_beta_middle_to_middle:
  !n x y z. n MOD 4 <> 0 /\ (x, y, z) IN mills_alpha_less_middle n ==>
            zagier (x, y, z) IN mills_alpha_more_middle n
Proof
  rw[mills_alpha_less_middle_def, mills_alpha_more_middle_def, mills_alpha_less_def, mills_alpha_more_def] >>
  `x < 2 * y` by decide_tac >>
  qexists_tac `2 * y - x` >>
  qexists_tac `y` >>
  qexists_tac `x + z - y` >>
  `y < 2 * y - x` by decide_tac >>
  `x <> 0` by metis_tac[mills_with_no_mind] >>
  `2 * y - x < 2 * y` by decide_tac >>
  metis_tac[zagier_def, zagier_closure]
QED

(* Theorem: ~square n /\ (x, y, z) IN mills_alpha_more_right n ==>
            zagier (x, y, z) IN mills_alpha_less_left n *)
(* Proof:
   By mills_alpha_less_left_def, mills_alpha_more_right_def,
      mills_alpha_less_def, mills_alpha_more_def, this is to show:
      ?x' y' z'. (zagier (x,y,z) = (x',y',z')) /\
                 ((x',y',z') IN mills n /\ x' < y') /\ x' < y' - z'
   Since y < x, so y - z < x, implies y - z <= x, or ~(x < y - z).
   In this case, zagier (x, y, z) = (x - 2 * y, x + z - y, y).
   Let x' = x - 2 * y, y' = x + z - y, z' = y.
   Since (n MOD 4 <> 0), x <> 0     by mills_with_no_mind
    Thus 2 * z <= 2 * z + x, or y' < x'.
     and zagier (x,y,z) IN mills n  by zagier_closure
*)
Theorem zagier_beta_more_to_less:
  !n x y z. ~square n /\ (x, y, z) IN mills_alpha_more_right n ==>
            zagier (x, y, z) IN mills_alpha_less_left n
Proof
  rw[mills_alpha_less_left_def, mills_alpha_more_right_def, mills_alpha_less_def, mills_alpha_more_def] >>
  `~(x < y - z)` by decide_tac >>
  qexists_tac `x - 2 * y` >>
  qexists_tac `x + z - y` >>
  qexists_tac `y` >>
  `z <> 0` by metis_tac[mills_with_all_arms] >>
  `x - 2 * y < x + z - y` by decide_tac >>
  `x - 2 * y < x + z - y - y` by decide_tac >>
  metis_tac[zagier_def, zagier_closure]
QED

(* Theorem: (x, y, z) IN mills_alpha_more_middle n ==>
             zagier (x, y, z) IN mills_alpha_less_middle n *)
(* Proof:
   By mills_alpha_less_middle_def, mills_alpha_more_middle_def,
      mills_alpha_less_def, mills_alpha_more_def, this is to show:
      ?x' y' z'. (zagier (x,y,z) = (x',y',z')) /\
                 ((x',y',z') IN mills n /\ x' < y') /\ ~(x' < y' - z')
   Note y < x ==> x <> 0, hence no need of (n MOD 4 <> 0) to give x <> 0.
   Also y < x ==> y - z < x, implies y - z <= x, or ~(x < y - z).
   With x < 2 * y, zagier (x, y, z) = (2 * y - x, y, x + z - y).
   Let x' = 2 * y - x, y' = y, z' = x + z - y.
   Since y < 2 * y - x, so y' < x'.
   Since x <> 0 by y < x for elements of (mills_alpha_more_middle n)
    Thus 2 * y - x < 2 * y, or x' < 2 * y'.
     and zagier (x,y,z) IN mills n  by zagier_closure
*)
Theorem zagier_beta_middle_back_to_middle:
  !n x y z. (x, y, z) IN mills_alpha_more_middle n ==>
            zagier (x, y, z) IN mills_alpha_less_middle n
Proof
  rw[mills_alpha_less_middle_def, mills_alpha_more_middle_def, mills_alpha_less_def, mills_alpha_more_def] >>
  `~(x < y - z)` by decide_tac >>
  qexists_tac `2 * y - x` >>
  qexists_tac `y` >>
  qexists_tac `x + z - y` >>
  `2 * y - x < y` by decide_tac >>
  `~(2 * y - x < y - (x + z - y))` by decide_tac >>
  metis_tac[zagier_def, zagier_closure]
QED

(* Theorem: ~square n /\ n MOD 4 <> 0 ==>
       BIJ zagier (mills_alpha_less_left n) (mills_alpha_more_right n) *)
(* Proof:
   By BIJ_DEF, INJ_DEF, SURJ_DEF, this is to show:
   (1) (x, y, z) IN mills_alpha_less_left n ==>
       zagier (x, y, z) IN mills_alpha_more_right n, true by zagier_beta_less_to_more
   (2)(3)(4)
       (x,y,z) IN mills_alpha_less_left n /\
       (x',y',z') IN mills_alpha_less_left n /\
       zagier (x,y,z) = zagier (x',y',z') ==> x = x' /\ y = y' /\ z = z'
       By mills_alpha_less_left_def and zagier_def,
       (x + 2 * z,z,y - z - x) = (x' + 2 * z',z',y' - z' - x')
       Thus x + 2 * z = x' + 2 * z' and z = z', so x = x'.
       Also y - z - x = y' - z' - x', so y = y'.
   (5) same as (1).
   (6) (x, y, z) IN mills_alpha_more_right n ==>
       ?t. t IN mills_alpha_less_left n /\ (zagier t = (x, y, z))
       Let t = zagier (x, y, z).
       Then t IN mills_alpha_less_left n   by zagier_beta_more_to_less
       Since (x, y, z) IN mills n          by mills_alpha_more_right_element,
                                              mills_alpha_more_element
         so x <> 0                         by mills_with_all_mind, n MOD 4 <> 0
        and y <> 0 and z <> 0              by mills_with_all_arms, !k. n <> k ** 2
       Thus zagier t
          = zagier (zagier (x, y, z))
          = (x, y, z)                      by zagier_involute
*)
Theorem mills_alpha_less_left_more_right_bij:
  !n. ~square n /\ n MOD 4 <> 0 ==>
      BIJ zagier (mills_alpha_less_left n) (mills_alpha_more_right n)
Proof
  rw[BIJ_DEF, INJ_DEF, SURJ_DEF, pairTheory.FORALL_PROD] >-
  metis_tac[zagier_beta_less_to_more] >-
 (fs[mills_alpha_less_left_def] >>
  `(p_1 + 2 * p_2,p_2,p_1' - p_2 - p_1) = (p_1'' + 2 * p_2',p_2',p_1'3' - p_2' - p_1'')` by metis_tac[zagier_def] >>
  `(p_1 + 2 * p_2 = p_1'' + 2 * p_2') /\ (p_2 = p_2')` by rw[pairTheory.PAIR_EQ] >>
  decide_tac) >-
 (fs[mills_alpha_less_left_def] >>
  `(p_1 + 2 * p_2,p_2,p_1' - p_2 - p_1) = (p_1'' + 2 * p_2',p_2',p_1'3' - p_2' - p_1'')` by metis_tac[zagier_def] >>
  `(p_1 + 2 * p_2 = p_1'' + 2 * p_2') /\ (p_2 = p_2') /\ (p_1' - p_2 - p_1 = p_1'3' - p_2' - p_1'')` by rw[pairTheory.PAIR_EQ] >>
  decide_tac) >-
 (fs[mills_alpha_less_left_def] >>
  metis_tac[zagier_def, pairTheory.PAIR_EQ]) >-
  metis_tac[zagier_beta_less_to_more] >>
  qexists_tac `zagier (p_1,p_1',p_2)` >>
  `p_1 <> 0` by metis_tac[mills_with_all_mind, mills_alpha_more_right_element, mills_alpha_more_element] >>
  `p_1' <> 0 /\ p_2 <> 0` by metis_tac[mills_with_all_arms, mills_alpha_more_right_element, mills_alpha_more_element] >>
  metis_tac[zagier_beta_more_to_less, zagier_involute]
QED

(* Theorem: ~square n /\ n MOD 4 <> 0 ==>
            BIJ zagier (mills_alpha_less_middle n) (mills_alpha_more_middle n) *)
(* Proof:
   By BIJ_DEF, INJ_DEF, SURJ_DEF, this is to show:
   (1) (x,y,z) IN mills_alpha_less_middle n ==>
       zagier (x,y,z) IN mills_alpha_more_middle n, true by zagier_beta_middle_to_middle
   (2)(3)(4)
       (x,y,z) IN mills_alpha_less_middle n /\
       (x',y',z') IN mills_alpha_less_middle n /\
       zagier (x,y,z) = zagier (x',y',z') ==> x = x' /\ y = y' /\ z = z'
       By mills_alpha_less_middle_def and zagier_def,
       (2 * y - x, y, x + z - y) = (2 * y' - x', y', x + z - y')
       Thus 2 * y - x = 2 * y' - x' and y = y', so x = x'.
       Also x + z - y = x' + z' - y', so z = z'.
   (5) same as (1).
   (6) (x, y, z) IN mills_alpha_more_middle n ==>
       ?t. t IN mills_alpha_more_middle n /\ (zagier t = (x, y, z))
       Let t = zagier (x, y, z).
       Then t IN mills_alpha_more_middle n by zagier_beta_middle_back_to_middle
       Since (x, y, z) IN mills n          by mills_alpha_more_middle_element,
                                              mills_alpha_more_element
         so x <> 0                         by mills_with_all_mind, n MOD 4 <> 0
        and y <> 0 and z <> 0              by mills_with_all_arms, !k. n <> k ** 2
       Thus zagier t
          = zagier (zagier (x, y, z))
          = (x, y, z)                      by zagier_involute
*)
Theorem mills_alpha_less_middle_more_middle_bij:
  !n. ~square n /\ n MOD 4 <> 0 ==>
      BIJ zagier (mills_alpha_less_middle n) (mills_alpha_more_middle n)
Proof
  rw[BIJ_DEF, INJ_DEF, SURJ_DEF, pairTheory.FORALL_PROD] >-
  metis_tac[zagier_beta_middle_to_middle] >-
 (fs[mills_alpha_less_middle_def] >>
  `p_1 < p_1' /\ p_1'' < p_1'3'` by metis_tac[mills_alpha_less_element] >>
  `p_1 < 2 * p_1' /\ p_1'' < 2 * p_1'3'` by decide_tac >>
  `(2 * p_1' - p_1,p_1',p_1 + p_2 - p_1') = (2 * p_1'3' - p_1'',p_1'3',p_1'' + p_2' - p_1'3')` by metis_tac[zagier_def] >>
  `(2 * p_1' - p_1 = 2 * p_1'3' - p_1'') /\ (p_1' = p_1'3')` by rw[pairTheory.PAIR_EQ] >>
  decide_tac) >-
 (fs[mills_alpha_less_middle_def] >>
  `p_1 < p_1' /\ p_1'' < p_1'3'` by metis_tac[mills_alpha_less_element] >>
  `p_1 < 2 * p_1' /\ p_1'' < 2 * p_1'3'` by decide_tac >>
  `(2 * p_1' - p_1,p_1',p_1 + p_2 - p_1') = (2 * p_1'3' - p_1'',p_1'3',p_1'' + p_2' - p_1'3')` by metis_tac[zagier_def] >>
  rw[pairTheory.PAIR_EQ]) >-
 (fs[mills_alpha_less_middle_def] >>
  `p_1 < p_1' /\ p_1'' < p_1'3'` by metis_tac[mills_alpha_less_element] >>
  `p_1 < 2 * p_1' /\ p_1'' < 2 * p_1'3'` by decide_tac >>
  `(2 * p_1' - p_1,p_1',p_1 + p_2 - p_1') = (2 * p_1'3' - p_1'',p_1'3',p_1'' + p_2' - p_1'3')` by metis_tac[zagier_def] >>
  `(2 * p_1' - p_1 = 2 * p_1'3' - p_1'') /\ (p_1' = p_1'3') /\ (p_1 + p_2 - p_1' = p_1'' + p_2' - p_1'3')` by rw[pairTheory.PAIR_EQ] >>
  decide_tac) >-
  metis_tac[zagier_beta_middle_to_middle] >>
  qexists_tac `zagier (p_1,p_1',p_2)` >>
  `p_1 <> 0` by metis_tac[mills_with_all_mind, mills_alpha_more_middle_element, mills_alpha_more_element] >>
  `p_1' <> 0 /\ p_2 <> 0` by metis_tac[mills_with_all_arms, mills_alpha_more_middle_element, mills_alpha_more_element] >>
  metis_tac[zagier_beta_middle_back_to_middle, zagier_involute]
QED

(* Theorem: ~square n /\ n MOD 4 <> 0 ==>
            BIJ zagier (mills_alpha_less n) (mills_alpha_more n) *)
(* Proof:
   Note mills_alpha_less n =
        mills_alpha_less_left n UNION
        mills_alpha_less_middle n               by mills_alpha_less_union
    and mills_alpha_less_left n INTER
        mills_alpha_less_middle n = EMPTY       by mills_alpha_less_inter
   Also mills_alpha_more n =
        mills_alpha_more_middle n UNION
        mills_alpha_more_right n                by mills_alpha_more_union
    and mills_alpha_more_middle n INTER
        mills_alpha_more_right n = EMPTY        by mills_alpha_more_inter
    Now BIJ zagier (mills_alpha_less_left n)
                   (mills_alpha_more_right n)   by mills_alpha_less_left_more_right_bij
    and BIJ zagier (mills_alpha_less_middle n)
                   (mills_alpha_more_middle n)  by mills_alpha_less_middle_more_middle_bij
   Thus BIJ zagier (mills_alpha_less n)
                   (mills_alpha_more n)         by set_partition_bij_give_bij
*)
Theorem mills_alpha_less_more_bij:
  !n. ~square n /\ n MOD 4 <> 0 ==>
      BIJ zagier (mills_alpha_less n) (mills_alpha_more n)
Proof
  rpt strip_tac >>
  `mills_alpha_less n = mills_alpha_less_left n UNION mills_alpha_less_middle n` by rw[mills_alpha_less_union] >>
  `mills_alpha_less_left n INTER mills_alpha_less_middle n = EMPTY` by rw[mills_alpha_less_inter] >>
  `mills_alpha_more n = mills_alpha_more_middle n UNION mills_alpha_more_right n` by rw[mills_alpha_more_union] >>
  `mills_alpha_more_middle n INTER mills_alpha_more_right n = EMPTY` by rw[mills_alpha_more_inter] >>
  `BIJ zagier (mills_alpha_less_left n) (mills_alpha_more_right n)` by fs[mills_alpha_less_left_more_right_bij] >>
  `BIJ zagier (mills_alpha_less_middle n) (mills_alpha_more_middle n)` by fs[mills_alpha_less_middle_more_middle_bij] >>
  metis_tac[set_partition_bij_give_bij]
QED

(* This is a great achievement! *)

(* Theorem: ~square n /\ n MOD 4 <> 0 ==>
       CARD (mills n DIFF mills_alpha_same n) = 2 * CARD (mills_alpha_less n) *)
(* Proof:
   Note FINITE (mills n)       by mills_non_square_finite
   Let s = mills n DIFF mills_alpha_same n,
       a = mills_alpha_less n, b = mills_alpha_more n.
   Note FINITE s               by FINITE_DIFF
    and s = a UNION b          by mills_alpha_union
    and a INTER b = EMPTY      by mills_alpha_inter
   also BIJ zagier a b         by mills_alpha_less_more_bij
    ==> CARD s = 2 * CARD a    by set_partition_bij_card
*)
Theorem mills_alpha_more_less_card:
  !n. ~square n /\ n MOD 4 <> 0 ==>
      CARD (mills n DIFF mills_alpha_same n) = 2 * CARD (mills_alpha_less n)
Proof
  metis_tac[mills_non_square_finite, mills_alpha_union, mills_alpha_inter,
            mills_alpha_less_more_bij, FINITE_DIFF, set_partition_bij_card]
QED

(* Theorem: ~square n /\ n MOD 4 <> 0 ==>
            CARD (mills n) = 2 * CARD (mills_alpha_less n) + CARD (mills_alpha_same n) *)
(* Proof:
   Note FINITE (mills n)                   by mills_non_square_finite
   Let s = mills n, t = mills_alpha_same n, b = mills_alpha_less n.
   Note t SUBSET s                         by mills_alpha_same_subset
    and CARD t <= CARD s                   by CARD_SUBSET
    ==> CARD (s DIFF t) = CARD s - CARD t  by SUBSET_DIFF_CARD
   Thus CARD s = CARD (s DIFF t) + CARD t  by arithmetic
               = 2 * CARD b + CARD t       by mills_alpha_more_less_card
*)
Theorem mills_alpha_card:
  !n. ~square n /\ n MOD 4 <> 0 ==>
      CARD (mills n) = 2 * CARD (mills_alpha_less n) + CARD (mills_alpha_same n)
Proof
  rpt strip_tac >>
  `FINITE (mills n)` by fs[mills_non_square_finite] >>
  imp_res_tac mills_alpha_more_less_card >>
  qabbrev_tac `s = mills n` >>
  qabbrev_tac `t = mills_alpha_same n` >>
  `t SUBSET s` by rw[mills_alpha_same_subset, Abbr`s`, Abbr`t`] >>
  `CARD t <= CARD s` by rw[CARD_SUBSET] >>
  `CARD (s DIFF t) = CARD s - CARD t` by rw[GSYM SUBSET_DIFF_CARD] >>
  decide_tac
QED

(* ------------------------------------------------------------------------- *)
(* Application to an odd prime p with p MOD 4 = 1.                           *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Fermat's Two Squares Theorem                                              *)
(* ------------------------------------------------------------------------- *)

(* Theorem: prime p /\ p MOD 4 = 1 ==> ?u v. p = u ** 2 + v ** 2 *)
(* Proof:
   Note ~square p               by prime_non_square
    and p MOD 4 <> 0            by p MOD 4 = 1
   Let k = p DIV 4.
   Then p = k * 4 + p MOD 4     by DIVISION
          = windmill (1, 1, k)  by windmill_trivial
   Thus p can be represented by a windmill.

   Consider all the windmills of prime p, let s = mills p.
   Note FINITE s                by mills_non_square_finite

   Step 1: the alpha-partition of s:
   Let a = mills_beta_less p, b = mills_beta_same p.
   Then CARD s = 2 * CARD a + CARD b    by mills_beta_card, [1]

   Step 2: the beta-partition of s:
   Let c = mills_alpha_less p, d = mills_alpha_same p.
   Then CARD s = 2 * CARD c + CARD d    by mills_alpha_card, [2]

   Note d = {(1,1,k)}           by mills_alpha_same_sing
     so CARD d = 1              by CARD_SING
   thus ODD (CARD s)            by ODD_DOUBLE, ADD1, [2]
    but EVEN (2 * CARD a)       by EVEN_DOUBLE
     so ODD (CARD b)            by EVEN_ADD, EVEN_ODD, [1]
    ==> CARD b <> 0             by EVEN, EVEN_ODD
     so b <> EMPTY              by CARD_EMPTY
    ==> ? x y z. (x,y,z) IN b   by MEMBER_NOT_EMPTY, triple_parts
    Now y = z,
     so (x, y, y) IN s             by mills_beta_same_element
    ==> p = windmill (x, y, y)     by mills_element
          = x ** 2 + 4 * y * y     by windmill_def
          = x ** 2 + (2 * y) ** 2  by EXP_2
   Take u = x, v = 2 * y,
   Then p = u ** 2 + v ** 2.
*)
Theorem fermat_two_squares_exists:
  !p. prime p /\ p MOD 4 = 1 ==> ?u v. p = u ** 2 + v ** 2
Proof
  rpt strip_tac >>
  `~square p` by metis_tac[prime_non_square] >>
  `p MOD 4 <> 0` by decide_tac >>
  qabbrev_tac `k = p DIV 4` >>
  `p = k * 4 + p MOD 4` by rw[DIVISION, Abbr`k`] >>
  `_ = windmill (1, 1, k)` by rw[windmill_trivial] >>
  qabbrev_tac `s = mills p` >>
  `FINITE s` by fs[mills_non_square_finite, Abbr`s`] >>
  qabbrev_tac `a = mills_beta_less p` >>
  qabbrev_tac `b = mills_beta_same p` >>
  `CARD s = 2 * CARD a + CARD b` by fs[mills_beta_card, Abbr`s`, Abbr`a`, Abbr`b`] >>
  qabbrev_tac `c = mills_alpha_less p` >>
  qabbrev_tac `d = mills_alpha_same p` >>
  `CARD s = 2 * CARD c + CARD d` by fs[GSYM mills_alpha_card, Abbr`s`, Abbr`c`, Abbr`d`] >>
  `d = {(1,1,k)}` by rw[mills_alpha_same_sing, Abbr`d`, Abbr`k`] >>
  `CARD d = 1` by rw[] >>
  `ODD (CARD s)` by metis_tac[ODD_DOUBLE, ADD1] >>
  `EVEN (2 * CARD a)` by rw[EVEN_DOUBLE] >>
  `ODD (CARD b)` by metis_tac[EVEN_ADD, EVEN_ODD] >>
  `CARD b <> 0` by metis_tac[EVEN, EVEN_ODD] >>
  `b <> EMPTY` by metis_tac[CARD_EMPTY] >>
  `?t. t IN b` by rw[MEMBER_NOT_EMPTY] >>
  `?x y z. t = (x, y, z)` by rw[triple_parts] >>
  `(x, y, y) IN s` by metis_tac[mills_beta_same_element] >>
  qexists_tac `x` >>
  qexists_tac `2 * y` >>
  `p = x ** 2 + 4 * y * y` by fs[mills_element, windmill_def, Abbr`s`] >>
  `_ = x ** 2 + (2 * y) * (2 * y)` by decide_tac >>
  `_ = x ** 2 + (2 * y) ** 2` by metis_tac[EXP_2] >>
  decide_tac
QED

(* Very good! *)

(* ------------------------------------------------------------------------- *)
(* The Classical Uniqueness Proof                                            *)
(* ------------------------------------------------------------------------- *)

(*
Classical uniqueness proof:
https://proofwiki.org/wiki/Fermat%27s_Two_Squares_Theorem

*)

(* Theorem: prime p ==>
            !a b c d. p = a ** 2 + b ** 2 /\ p = c ** 2 + d ** 2 ==>
                      (a = c /\ b = d) \/ (a = d /\ b = c) *)
(* Proof:
   Assume a <> d, b <> c. This is to show a = c, b = d.

   Step 1: establish identities.

   Claim: (a * d - b * c) * (a * d + b * c) = (d * d - b * b) * p
   Proof: Note a * a = p - b * b         by p = a * a + b * b
           and c * c = p - d * d         by p = c * c + d * d

            (a * d - b * c) * (a * d + b * c)
          = (a * d) * (a * d) - (b * c) * (b * c)        by difference_of_squares_alt
          = (a * a) * (d * d) - (c * c) * (b * b)
          = (p - b * b) * d * d - (p - d * d) * b * b    by substitution of a * a, c * c.
          = (p * d * d - b * b * d * d) - (p * b * b - d * d * b * b)
          = p * d * d - b * b * d * d + d * d * b * b - p * b * b    by SUB_SUB
          = p * d * d - p * b * b                                    by SUB_ADD
          = p * (d * d - b * b)

   Claim: (b * c - a * d) * (a * d + b * c) = (b * b - d * d) * p
   Proof: Note a * a = p - b * b         by p = a * a + b * b
           and c * c = p - d * d         by p = c * c + d * d

            (b * c - a * d) * (a * d + b * c)
          = (b * c) * (b * c) - (a * d) * (a * d)        by difference_of_squares_alt
          = (c * c) * (b * b) - (a * a) * (d * d)
          = (p - d * d) * b * b - (p - b * b) * d * d    by substitution of a * a, c * c.
          = (p * b * b - d * d * b * b) - (p * d * d - b * b * d * d)
          = p * b * b - d * d * b * b + b * b * d * d - p * d * d    by SUB_SUB
          = p * b * b - p * d * d                                    by SUB_ADD
          = p * (b * b - d * d)

   Step 2: establish properties
   Note p <> 0                                 by NOT_PRIME_0
    and a <> 0 /\ b <> 0 /\ c <> 0 /\ d <> 0   by prime_non_square, square_def

   Claim: a * d < p
   Proof: Note b * b <> 0 /\ c * c <> 0        by MULT_EQ_0
          Thus a * a < p /\ d * d < p          by p = a * a + b * b = c * c + d * d
           ==> a * a * (d * d) < p * p         by LT_MONO_MULT2
           ==>    (a * d) ** 2 < p ** 2        by EXP_2
           ==>           a * d < p             by EXP_EXP_LT_MONO

   Claim: b * c < p
   Proof: Note a * a <> 0 /\ d * d <> 0        by MULT_EQ_0
          Thus b * b < p /\ c * c < p          by p = a * a + b * b = c * c + d * d
           ==> b * b * (c * c) < p * p         by LT_MONO_MULT2
           ==>    (b * c) ** 2 < p ** 2        by EXP_2
           ==>           b * c < p             by EXP_EXP_LT_MONO

   Claim: coprime a b
   Proof: Let g = gcd a b.
          Then g divides a /\ g divides b       by GCD_IS_GREATEST_COMMON_DIVISOR
           ==> ?h k. (a = h * g) /\ (b = k * g) by divides_def
          Thus p = a * a + b * b
                 = (h * g) * (h * g) + (k * g) * (k * g)
                 = (g * g) * (h * h + k * k)
                 = (h ** 2 + k ** 2) * g ** 2
            so g ** 2 divides p         by divides_def
           ==> g ** 2 = 1               by prime_def, prime_non_square, square_def
           ==>      g = 1               by EXP_EQ_1

   Step 3:  make use of these facts and prime p.
   Note p divides (a * d - b * c) * (a * d + b * c)   by divides_def
    and p divides (b * c - a * d) * (a * d + b * c)   by divides_def
    ==> (p divides (a * d - b * c) /\ p divides (b * c - a * d))
     or (p divides (a * d + b * c)                    by euclid_prime

   Case 1: p divides (a * d - b * c) /\ p divides (b * c - a * d)
           Note (a * d - b * c) < p            by a * d < p, b * c <> 0
            and (b * c - a * d) < p            by b * c < p, a * d <> 0
           Thus  a * d - b * c = 0             by DIVIDES_LEQ_OR_ZERO
            and  b * c - a * d = 0             by DIVIDES_LEQ_OR_ZERO
            ==>  a * d = b * c                 by arithmetic
           Thus  a divides (b * c)
           but   coprime a b                   by gcd a b = 1
           Hence a divides c                   by euclid_coprime
            ==> ?t. c = t * a                  by divides_def
           Then d * a = b * (t * a) = (t * b) * a
            ==> d = t * b                      by EQ_MULT_RCANCEL
           Therefore,
                 p = c * c + d * d
                   = (t * a) * (t * a) + (t * b) * (t * b)
                   = t * t * (a * a + b * b)
                   = t * t * p
            Thus t * t = 1                     by EQ_MULT_RCANCEL, MULT_LEFT_1
             ==>     t = 1                     by MULT_EQ_1
            This gives c = a, d = b.

   Case 2: p divides (a * d + b * c)

           Note a * d + b * c < 2 * p          by a * d < p, b * c < p
            and a * d + b * c <> 0             by ADD_EQ_0, a * d <> 0, b * c <> 0
            Now ?k. a * d + b * c = k * p      by divides_def
                                  < 2 * p      by above, and k <> 0.
           Thus k = 1, or a * d + b * c = p    [3]

           To apply the four_squares_identity,
           If b * d <= a * c,
              p ** 2
            = p * p                                        by EXP_2
            = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)        by given
            = (a * d + b * c) ** 2 + (a * c - b * d) ** 2  by four_squares_identity
            = p ** 2 + (a * c - b * d) ** 2                by [3]
           Thus (a * c - b * d) ** 2 = 0     by EQ_ADD_LCANCEL
            ==> a * c - b * d = 0            by EXP_EQ_0
             or a * c <= b * d
            ==>  a * c = b * d               by b * d <= a * c
             so b divides a * c              by divides_def
            but coprime a b                  by gcd a b = 1
            ==> b divides c                  by euclid_coprime
            ==> ?t . c = t * b               by divides_def
           then      d = t * a
           giving    t = 1, or c = b, d = a.
           This contradicts a <> d, b <> c.

           If ~(b * d <= a * c), then a * c < b * d, implies a * c <= b * d.
              p ** 2
            = p * p                                        by EXP_2
            = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)        by given
            = (a * d + b * c) ** 2 + (b * d - a * c) ** 2  by four_squares_identity_alt
            = p ** 2 + (b * d - a * c) ** 2                by [3]
           Thus (b * d - a * c) ** 2 = 0     by EQ_ADD_LCANCEL
            ==> b * d - a * c = 0            by EXP_EQ_0
            ==>  b * d <= a * c
             or   b * d = a * c              by a * c <= b * d
             so b divides a * c              by divides_def
            but coprime a b                  by gcd a b = 1
            ==> b divides c                  by euclid_coprime
            ==> ?t . c = t * b               bu divides_def
            then     d = t * a
           giving    t = 1, or c = b, d = a.
           This contradicts a <> d, b <> c.

*)
Theorem fermat_two_squares_unique:
  !p. prime p ==>
      !a b c d. p = a ** 2 + b ** 2 /\ p = c ** 2 + d ** 2 ==>
                (a = c /\ b = d) \/ (a = d /\ b = c)
Proof
  rpt strip_tac >>
  Cases_on `a = d /\ b = c` >-
  simp[] >>
  simp[] >>
  `(a * d - b * c) * (a * d + b * c) = (d * d - b * b) * p` by
  (`a * a = p - b * b` by simp[] >>
  `c * c = p - d * d` by simp[] >>
  `(a * d - b * c) * (a * d + b * c) = (a * d) * (a * d) - (b * c) * (b * c)` by rw[difference_of_squares_alt] >>
  `_ = (a * a) * (d * d) - (c * c) * (b * b)` by fs[] >>
  `_ = (p - b * b) * d * d - (p - d * d) * b * b` by fs[] >>
  `_ = (p * d * d - b * b * d * d) - (p * b * b - d * d * b * b)` by decide_tac >>
  `_ = p * d * d - b * b * d * d + d * d * b * b - p * b * b` by simp[] >>
  `_ = p * d * d - p * b * b` by simp[] >>
  decide_tac) >>
  `(b * c - a * d) * (a * d + b * c) = (b * b - d * d) * p` by
    (`a * a = p - b * b` by simp[] >>
  `c * c = p - d * d` by simp[] >>
  `(b * c - a * d) * (a * d + b * c) = (b * c) * (b * c) - (a * d) * (a * d)` by rw[difference_of_squares_alt] >>
  `_ = (c * c) * (b * b) - (a * a) * (d * d)` by fs[] >>
  `_ = (p - d * d) * b * b - (p - b * b) * d * d` by fs[] >>
  `_ = (p * b * b - d * d * b * b) - (p * d * d - b * b * d * d)` by simp[] >>
  `_ = p * b * b - d * d * b * b + b * b * d * d - p * d * d` by simp[SUB_SUB] >>
  `_ = p * b * b - p * d * d` by simp[] >>
  simp[]) >>
  `p <> 0` by metis_tac[NOT_PRIME_0] >>
  `a <> 0 /\ b <> 0 /\ c <> 0 /\ d <> 0` by
      (`0 ** 2 = 0` by rw[] >>
  metis_tac[prime_non_square, square_def, EXP_2, ADD_CLAUSES]) >>
  `a * d < p` by
        (`p = a * a + b * b` by simp[] >>
  `p = c * c + d * d` by simp[] >>
  `b * b <> 0 /\ c * c <> 0` by rw[MULT_EQ_0] >>
  `a * a < p /\ d * d < p` by rw[] >>
  `a * a * (d * d) < p * p` by rw[LT_MONO_MULT2] >>
  `a * a * (d * d) = (a * d) * (a * d)` by simp[] >>
  metis_tac[EXP_EXP_LT_MONO, EXP_2, DECIDE``0 < 2``]) >>
  `b * c < p` by
          (`p = a * a + b * b` by simp[] >>
  `p = c * c + d * d` by simp[] >>
  `a * a <> 0 /\ d * d <> 0` by rw[MULT_EQ_0] >>
  `b * b < p /\ c * c < p` by rw[] >>
  `b * b * (c * c) < p * p` by rw[LT_MONO_MULT2] >>
  `b * b * (c * c) = (b * c) * (b * c)` by simp[] >>
  metis_tac[EXP_EXP_LT_MONO, EXP_2, DECIDE``0 < 2``]) >>
  `gcd a b = 1` by
            (qabbrev_tac `g = gcd a b` >>
  `g divides a /\ g divides b` by rw[GCD_IS_GREATEST_COMMON_DIVISOR, Abbr`g`] >>
  `?h k. (a = h * g) /\ (b = k * g)` by metis_tac[divides_def] >>
  `p = a * a + b * b` by fs[] >>
  `_ = (h * g) * (h * g) + (k * g) * (k * g)` by metis_tac[] >>
  `_ = (g * g) * (h * h + k * k)` by simp[] >>
  `_ = (h ** 2 + k ** 2) * g ** 2` by simp[] >>
  `g ** 2 divides p` by metis_tac[divides_def] >>
  `g ** 2 = 1` by metis_tac[prime_def, prime_non_square, square_def, EXP_2] >>
  metis_tac[EXP_EQ_1, DECIDE``2 <> 0``]) >>
  `p divides (a * d - b * c) * (a  * d + b * c)` by metis_tac[divides_def] >>
  `p divides (b * c - a * d) * (a * d + b * c)` by metis_tac[divides_def] >>
  `(p divides (a * d - b * c) /\ p divides (b * c - a * d)) \/ p divides (a * d + b * c)` by metis_tac[euclid_prime] >| [
    `b * c <> 0 /\ a * d <> 0` by metis_tac[MULT_EQ_0] >>
    `(a * d - b * c) < p /\ (b * c - a * d) < p` by fs[] >>
    `(a * d - b * c = 0) /\ (b * c - a * d = 0)` by metis_tac[DIVIDES_LEQ_OR_ZERO, NOT_LESS] >>
    `b * c = d * a` by fs[] >>
    `a divides (b * c)` by metis_tac[divides_def] >>
    `a divides c` by metis_tac[euclid_coprime, GCD_SYM] >>
    `?t. c = t * a` by metis_tac[divides_def] >>
    `d * a = b * (t * a)` by fs[] >>
    `_ = (t * b) * a` by fs[] >>
    `d = t * b` by metis_tac[EQ_MULT_RCANCEL] >>
    `p = c * c + d * d` by fs[] >>
    `_ = (t * a) * (t * a) + (t * b) * (t * b)` by metis_tac[] >>
    `_ = t * t * (a * a + b * b)` by fs[] >>
    `_ = t * t * p` by fs[] >>
    `t * t = 1` by metis_tac[EQ_MULT_RCANCEL, MULT_LEFT_1] >>
    metis_tac[MULT_EQ_1, MULT_LEFT_1],
    `a * d + b * c = p` by
  (`a * d + b * c < 2 * p` by fs[] >>
    `?k. a * d + b * c = k * p` by metis_tac[divides_def] >>
    `k <> 0` by metis_tac[MULT, ADD_EQ_0, MULT_EQ_0] >>
    `k < 2` by fs[] >>
    `k = 1` by fs[] >>
    simp[]) >>
    Cases_on `b * d <= a * c` >| [
      `p ** 2 = p * p` by simp[] >>
      `_ = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = (a * d + b * c) ** 2 + (a * c - b * d) ** 2` by fs[four_squares_identity] >>
      `_ = p ** 2 + (a * c - b * d) ** 2` by fs[] >>
      `(a * c - b * d) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `a * c - b * d = 0` by metis_tac[EXP_EQ_0, DECIDE``0 < 2``] >>
      `a * c = d * b` by fs[] >>
      `b divides a * c` by metis_tac[divides_def] >>
      `b divides c` by metis_tac[euclid_coprime] >>
      `?t . c = t * b` by metis_tac[divides_def] >>
      `d * b = a * (t * b)` by fs[] >>
      `_ = (t * a) * b` by fs[] >>
      `d = t * a` by metis_tac[EQ_MULT_RCANCEL] >>
      `p = c * c + d * d` by fs[] >>
      `_ = (t * b) * (t * b) + (t * a) * (t * a)` by metis_tac[] >>
      `_ = t * t * (b * b + a * a)` by simp[] >>
      `_ = t * t * p` by gs[] >>
      `t * t = 1` by metis_tac[EQ_MULT_RCANCEL, MULT_LEFT_1] >>
      `t = 1` by metis_tac[MULT_EQ_1] >>
      fs[],
      `p ** 2 = p * p` by simp[] >>
      `_ = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = (a * d + b * c) ** 2 + (b * d - a * c) ** 2` by fs[four_squares_identity_alt] >>
      `_ = p ** 2 + (b * d - a * c) ** 2` by fs[] >>
      `(b * d - a * c) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `b * d - a * c = 0` by metis_tac[EXP_EQ_0, DECIDE``0 < 2``] >>
      `a * c = d * b` by fs[] >>
      `b divides a * c` by metis_tac[divides_def] >>
      `b divides c` by metis_tac[euclid_coprime] >>
      `?t . c = t * b` by metis_tac[divides_def] >>
      `d * b = a * (t * b)` by fs[] >>
      `_ = (t * a) * b` by fs[] >>
      `d = t * a` by metis_tac[EQ_MULT_RCANCEL] >>
      `p = c * c + d * d` by fs[] >>
      `_ = (t * b) * (t * b) + (t * a) * (t * a)` by metis_tac[] >>
      `_ = t * t * (b * b + a * a)` by fs[] >>
      `_ = t * t * p` by fs[] >>
      `t * t = 1` by metis_tac[EQ_MULT_RCANCEL, MULT_LEFT_1] >>
      `t = 1` by metis_tac[MULT_EQ_1] >>
      fs[]
    ]
  ]
QED

(* This is fantastic! *)

(* Another proof of the same theorem *)

(* Theorem: prime p /\ p = a ** 2 + b ** 2 /\ p = c ** 2 + d ** 2 ==>
          {a; b} = {c; d} *)
(* Proof:
   This is to show:
        a = c /\ b = d
     \/ a = d /\ b = c                     by doublet_eq

   Step 1: basic properties
   Note 0 < p                              by NOT_PRIME_0
    and 0 < a /\ 0 < b /\ 0 < c /\ 0 < d   by SQ_0, prime_non_square, square_def
    and a * d < p                          by squares_sum_inequality, ADD_COMM
    and b * c < p                          by squares_sum_inequality, ADD_COMM

   Step 2: use identities and prime divisibility
   Note (a * d - b * c) * (a * d + b * c)
      = (d ** 2 - b ** 2) * p              by squares_sum_identity_1
    and (b * c - a * d) * (a * d + b * c)
      = (b ** 2 - d ** 2) * p              by squares_sum_identity_2
   Thus p divides (a * d - b * c) * (a * d + b * c)   by divides_def
    and p divides (b * c - a * d) * (a * d + b * c)   by divides_def
    ==> (p divides (a * d - b * c) /\ p divides (b * c - a * d))
      \/ p divides (a * d + b * c)                    by euclid_prime

   Case: p divides a * d - b * c /\ p divides b * c - a * d
   Note a * d - b * c < p             by a * d < p
    and b * c - a * d < p             by b * c < p
   Thus (a * d - b * c = 0)           by DIVIDES_LEQ_OR_ZERO
    and (b * c - a * d = 0)           by DIVIDES_LEQ_OR_ZERO
    ==> a * d = b * c                 by arithmetic
   Hence a = c and b = d          by squares_sum_prime_thm

   Case: p divides a * d + b * c
   Note 0 < (a * d + b * c) < 2 * p   by a * d < p, b * c < p, and a,b,c,d <> 0
   Thus a * d + b * c = p             by divides_eq_thm
   For four_squares_identity, break into cases:
   If b * d <= a * c,
      Note p ** 2
         = p * p                      by EXP_2
         = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)
         = p ** 2 + (a * c - b * d) ** 2
                                      by four_squares_identity
      Thus (a * c - b * d) ** 2 = 0   by EQ_ADD_LCANCEL
        or a * c - b * d = 0          by EXP_2_EQ_0
       ==> a * c = b * d              by b * d <= a * c
       ==> a = c /\ b = d         by squares_sum_prime_thm, ADD_COMM
   If a * c <= b * d,
      Note p ** 2
         = p * p                      by EXP_2
         = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)
         = p ** 2 + (b * d - a * c) ** 2
                                      by four_squares_identity_alt
      Thus (b * d - a * c) ** 2 = 0   by EQ_ADD_LCANCEL
        or b * d - a * c = 0          by EXP_2_EQ_0
       ==> a * c = b * d              by a * c <= b * d
       ==> a = c /\ b = d         by squares_sum_prime_thm, ADD_COMM
*)
Theorem fermat_two_squares_unique_thm:
  !p a b c d. prime p /\ p = a ** 2 + b ** 2 /\ p = c ** 2 + d ** 2 ==>
              {a; b} = {c; d}
Proof
  rpt strip_tac >>
  simp[doublet_eq] >>
  spose_not_then strip_assume_tac >>
  `0 < p` by metis_tac[NOT_PRIME_0, NOT_ZERO] >>
  `0 < a /\ 0 < b /\ 0 < c /\ 0 < d` by metis_tac[SQ_0, prime_non_square, square_def, EXP_2, ADD, ADD_0, NOT_ZERO] >>
  `a * d < p` by metis_tac[squares_sum_inequality, ADD_COMM] >>
  `b * c < p` by metis_tac[squares_sum_inequality, ADD_COMM] >>
  `(a * d - b * c) * (a * d + b * c) = (d ** 2 - b ** 2) * p` by rw[squares_sum_identity_1] >>
  `(b * c - a * d) * (a * d + b * c) = (b ** 2 - d ** 2) * p` by rw[squares_sum_identity_2] >>
  `p divides (a * d - b * c) * (a * d + b * c)` by metis_tac[divides_def] >>
  `p divides (b * c - a * d) * (a * d + b * c)` by metis_tac[divides_def] >>
  `(p divides (a * d - b * c) /\ p divides (b * c - a * d)) \/ p divides (a * d + b * c)` by metis_tac[euclid_prime] >| [
    `a * d - b * c < p /\ b * c - a * d < p` by fs[] >>
    `(a * d - b * c = 0) /\ (b * c - a * d = 0)` by metis_tac[DIVIDES_LEQ_OR_ZERO, NOT_LESS] >>
    `a * d = b * c` by fs[] >>
    metis_tac[squares_sum_prime_thm],
    `a * d + b * c = p` by
  (`a * d + b * c < 2 * p` by fs[] >>
    `0 < a * d + b * c` by metis_tac[MULT, ADD_EQ_0, MULT_EQ_0, NOT_ZERO] >>
    rw[divides_eq_thm]) >>
    Cases_on `b * d <= a * c` >| [
      `p ** 2 = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = p ** 2 + (a * c - b * d) ** 2` by rfs[four_squares_identity] >>
      `(a * c - b * d) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `a * c - b * d = 0` by metis_tac[EXP_2_EQ_0] >>
      `a * c = b * d` by fs[] >>
      metis_tac[squares_sum_prime_thm, ADD_COMM],
      `p ** 2 = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = p ** 2 + (b * d - a * c) ** 2` by rfs[four_squares_identity_alt] >>
      `(b * d - a * c) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `b * d - a * c = 0` by metis_tac[EXP_2_EQ_0] >>
      `a * c = b * d` by fs[] >>
      metis_tac[squares_sum_prime_thm, ADD_COMM]
    ]
  ]
QED

(* Theorem: prime p ==>
            !a b c d. ODD a /\ EVEN b /\ p = a ** 2 + b ** 2 /\
                      ODD c /\ EVEN d /\ p = c ** 2 + d ** 2 ==>
                      a = c /\ b = d *)
(* Proof:
   By contradiction, suppose a = c, but b <> d.

   Step 1: Establish identities and other things
   Note (a * d - b * c) * (a * d + b * c)
      = p * (d ** 2 - b ** 2)                 by squares_sum_identity_1
    and (b * c - a * d) * (a * d + b * c)
      = p * (b ** 2 - d ** 2)                 by squares_sum_identity_2
   Also p <> 0                                by NOT_PRIME_0
     so a <> 0 /\ b <> 0 /\ c <> 0 /\ d <> 0  by SQ_0, prime_non_square, square_def

   Step 2: show a * d < p, /b * c < p, and coprime a b
   Note a * d < p                             by squares_sum_inequality, ADD_COMM
    and b * c < p                             by squares_sum_inequality, ADD_COMM
    and gcd a b = 1                           by squares_sum_prime_coprime

   Step 3: make use of these facts and prime p.
   Thus p divides (a * d - b * c) * (a * d + b * c)   by divides_def, MULT_COMM
    and p divides (b * c - a * d) * (a * d + b * c)   by divides_def, MULT_COMM
    ==> (p divides (a * d - b * c) /\ p divides (b * c - a * d))
      \/ p divides (a * d + b * c)                    by euclid_prime

   Case: p divides (a * d - b * c) /\ p divides (b * c - a * d)
   Note a * d - b * c < p             by a * d < p
    and b * c - a * d < p             by b * c < p
     so a * d - b * c = 0             by DIVIDES_LEQ_OR_ZERO
    and b * c - a * d = 0             by DIVIDES_LEQ_OR_ZERO
    ==> a * d = b * c                 by arithmetic
   so a divides b * c. But a and b are coprime, so a divides c.
    ==> a = c and b = d               by squares_sum_prime_thm
   This contradicts a = c, but b <> d.

   Case: p divides a * d + b * c
   Note 0 < (a * d + b * c) < 2 * p   by a * d < p, b * c < p
     so a * d + b * c = p             by p divides a * d + b * c
   Then p ** 2
      = (a * a + b * b) * (c * c + d * d)
      = (a * d + b * c) ** 2 + (a * c - b * d) ** 2
                                         by four_squares_identity (two cases)
      = p ** 2 + (a * c - b * d) ** 2    by p = a * d + b * c
   or = p ** 2 + (b * d - a * c) ** 2    for the case a * c <= b * d
   Hence, a * c - b * d = 0,
       or a * c = b * d                  by two cases: a * c <= b * d, otherwise.
    ==> a = d /\ b = c                   by squares_sum_prime_thm, ADD_COMM
   This contradicts ODD c, EVEN b
                 or ODD a, EVEN d        by ODD_EVEN
*)
Theorem fermat_two_squares_unique_odd_even:
  !p. prime p ==>
      !a b c d. ODD a /\ EVEN b /\ p = a ** 2 + b ** 2 /\
                ODD c /\ EVEN d /\ p = c ** 2 + d ** 2 ==> (a = c /\ b = d)
Proof
  spose_not_then strip_assume_tac >>
  `(a * d - b * c) * (a * d + b * c) = p * (d ** 2 - b ** 2)` by rw[squares_sum_identity_1] >>
  `(b * c - a * d) * (a * d + b * c) = p * (b ** 2 - d ** 2)` by rw[squares_sum_identity_2] >>
  `p <> 0` by metis_tac[NOT_PRIME_0] >>
  `a <> 0 /\ b <> 0 /\ c <> 0 /\ d <> 0` by metis_tac[SQ_0, prime_non_square, square_def, EXP_2, ADD_CLAUSES] >>
  `a * d < p` by metis_tac[squares_sum_inequality, ADD_COMM, NOT_ZERO] >>
  `b * c < p` by metis_tac[squares_sum_inequality, ADD_COMM, NOT_ZERO] >>
  `gcd a b = 1` by metis_tac[squares_sum_prime_coprime] >>
  `p divides (a * d - b * c) * (a * d + b * c)` by metis_tac[divides_def, MULT_COMM] >>
  `p divides (b * c - a * d) * (a * d + b * c)` by metis_tac[divides_def, MULT_COMM] >>
  `(p divides (a * d - b * c) /\ p divides (b * c - a * d)) \/ p divides (a * d + b * c)` by metis_tac[euclid_prime] >| [
    `a * d - b * c < p /\ b * c - a * d < p` by fs[] >>
    `(a * d - b * c = 0) /\ (b * c - a * d = 0)` by metis_tac[DIVIDES_LEQ_OR_ZERO, NOT_LESS] >>
    `a * d = b * c` by fs[] >>
    metis_tac[squares_sum_prime_thm],
    `a * d + b * c = p` by
  (`a * d + b * c < 2 * p` by fs[] >>
    `?k. a * d + b * c = k * p` by metis_tac[divides_def] >>
    `k <> 0` by metis_tac[MULT, ADD_EQ_0, MULT_EQ_0] >>
    `k < 2` by fs[] >>
    `k = 1` by fs[] >>
    simp[]) >>
    Cases_on `b * d <= a * c` >| [
      `p ** 2 = p * p` by simp[] >>
      `_ = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = (a * d + b * c) ** 2 + (a * c - b * d) ** 2` by fs[four_squares_identity] >>
      `_ = p ** 2 + (a * c - b * d) ** 2` by fs[] >>
      `(a * c - b * d) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `a * c - b * d = 0` by metis_tac[EXP_EQ_0, DECIDE``0 < 2``] >>
      `a * c = b * d` by fs[] >>
      `a = d /\ b = c` by metis_tac[squares_sum_prime_thm, ADD_COMM] >>
      metis_tac[ODD_EVEN],
      `p ** 2 = p * p` by simp[] >>
      `_ = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = (a * d + b * c) ** 2 + (b * d - a * c) ** 2` by fs[four_squares_identity_alt] >>
      `_ = p ** 2 + (b * d - a * c) ** 2` by fs[] >>
      `(b * d - a * c) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `b * d - a * c = 0` by metis_tac[EXP_EQ_0, DECIDE``0 < 2``] >>
      `a * c = b * d` by fs[] >>
      `a = d /\ b = c` by metis_tac[squares_sum_prime_thm, ADD_COMM] >>
      metis_tac[ODD_EVEN]
    ]
  ]
QED

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
