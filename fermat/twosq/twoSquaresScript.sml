(* ------------------------------------------------------------------------- *)
(* Windmills of the minds: Fermat's Two Squares Theorem                      *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "twoSquares";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)

(* val _ = load "windmillTheory"; *)
open helperTwosqTheory;
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory; (* for divides_def, prime_def *)
open logPowerTheory; (* for prime_non_square *)

open windmillTheory;

(* val _ = load "involuteFixTheory"; *)
open involuteTheory; (* for involute_bij *)
open involuteFixTheory;

(* val _ = load "iterateComposeTheory"; *)
open iterationTheory;
open iterateComposeTheory;

(* val _ = load "iterateComputeTheory"; *)
open iterateComputeTheory;

(* for later computation *)
open listTheory;
open rich_listTheory; (* for MAP_REVERSE *)
open helperListTheory; (* for listRangeINC_LEN *)
open listRangeTheory; (* for listRangeINC_CONS *)
open gcdTheory; (* for GCD_IS_GREATEST_COMMON_DIVISOR *)

(* for group action *)
(* val _ = load "involuteActionTheory"; *)
open involuteActionTheory;
open groupActionTheory;
open groupInstancesTheory;

(* for pairs *)
open pairTheory; (* for ELIM_UNCURRY, PAIR_FST_SND_EQ, PAIR_EQ, FORALL_PROD *)


(* ------------------------------------------------------------------------- *)
(* Windmills of the minds Documentation                                      *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
   (a,b) o (d,c)    = multiply (a,b) (c,d)
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   Helper Theorems:

   Fermat Two-Squares Uniqueness:
   fermat_two_squares_unique_thm
                   |- !p a b c d. prime p /\ (p = a ** 2 + b ** 2) /\
                         (p = c ** 2 + d ** 2) ==> ({a; b} = {c; d})
   fermat_two_squares_unique_odd_even
                   |- !p a b c d. prime p /\
                         ODD a /\ EVEN b /\ (p = a ** 2 + b ** 2) /\
                         ODD c /\ EVEN d /\ (p = c ** 2 + d ** 2) ==>
                         (a = c) /\ (b = d)

   Two Squares Fixes:
   zagier_fixes_prime
                   |- !p. prime p /\ (p MOD 4 = 1) ==>
                          (fixes zagier (mills p) = {(1,1,p DIV 4)})
   flip_fixes_prime_card_upper
                   |- !p. prime p /\ (p MOD 4 = 1) ==> CARD (fixes flip (mills p)) <= 1

   Fermat Two-Squares Existence:
   fermat_two_squares_exists_windmill
                   |- !p. prime p /\ (p MOD 4 = 1) ==> ?x y. p = windmill (x, y, y)
   fermat_two_squares_exists_odd_even
                   |- !p. prime p /\ (p MOD 4 = 1) ==>
                          ?(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2)

   Fermat Two-Squares Theorem:
   flip_fixes_prime_sing
                   |- !p. prime p /\ (p MOD 4 = 1) ==> SING (fixes flip (mills p))
   flip_fixes_prime|- !p. prime p /\ (p MOD 4 = 1) ==>
                          (let u = (1,1,p DIV 4) ;
                               n = iterate_period (zagier o flip) u
                            in fixes flip (mills p) = {FUNPOW (zagier o flip) (HALF n) u})
   flip_fixes_prime_alt
                   |- !p u n. prime p /\ (p MOD 4 = 1) /\ (u = (1,1,p DIV 4)) /\
                              (n = iterate_period (zagier o flip) u) ==>
                              (fixes flip (mills p) = {FUNPOW (zagier o flip) (HALF n) u}
   fermat_two_squares_thm
                   |- !p. prime p /\ (p MOD 4 = 1) ==>
                          ?!(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2)
   fermat_two_squares_iff
                   |- !p. prime p ==> ((p MOD 4 = 1) <=>
                                      ?!(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2))

   Fermat Two-Squares Algorithm:
   zagier_flip_1_1_z_period
                   |- !z. (iterate_period (zagier o flip) (1,1,z) = 1) <=> (z = 1)
   flip_fixes_iterates_prime
                   |- !p u n g. prime p /\ (p MOD 4 = 1) /\ (u = (1,1,p DIV 4)) /\
                                (n = iterate_period (zagier o flip) u) /\
                                (g = (\(x,y,z). y <> z)) ==>
                                ~g (FUNPOW (zagier o flip) (HALF n) u) /\
                                !j. j < HALF n ==> g (FUNPOW (zagier o flip) j u)
   Computation by WHILE loop:
   found_def       |- !x y z. found (x,y,z) <=> (y = z)
   found_not       |- $~ o found = (\(x,y,z). y <> z)
   two_sq_def      |- !n. two_sq n = WHILE ($~ o found) (zagier o flip) (1,1,n DIV 4)
   two_sq_alt      |- !n. two_sq n = WHILE (\(x,y,z). y <> z) (zagier o flip) (1,1,n DIV 4)
   two_sq_thm      |- !p. prime p /\ (p MOD 4 = 1) ==> two_sq p IN fixes flip (mills p)
   two_sq_while_hoare
                   |- !p. prime p /\ (p MOD 4 = 1) ==>
                          HOARE_SPEC (fixes zagier (mills p))
                                     (WHILE ($~ o found) (zagier o flip))
                                     (fixes flip (mills p))
   two_sq_while_thm|- !p. prime p /\ (p MOD 4 = 1) ==>
                          (let (x,y,z) = WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)
                            in p = x ** 2 + (y + z) ** 2)
   two_squares_def |- !n. two_squares n = (let (x,y,z) = two_sq n in (x,y + z))
   two_squares_thm |- !p. prime p /\ (p MOD 4 = 1) ==>
                          (let (u,v) = two_squares p in p = u ** 2 + v ** 2)

   Fermat's Two Squares Theorem by Group Action:

   Relation to Fixes and Pairs:
   involute_fixed_points_eq_fixes
                   |- !f X. f involute X ==>
                            fixed_points (FUNPOW f) Z2 X = fixes f X
   involute_multi_orbits_union_eq_pairs
                   |- !f X. f involute X ==>
                            BIGUNION (multi_orbits (FUNPOW f) Z2 X) = pairs f X

   Fermat's Two-Squares Theorem (Existence):
   zagier_fixed_points_sing
                   |- !p. prime p /\ (p MOD 4 = 1) ==>
                         (fixed_points (FUNPOW zagier) Z2 (mills p) = {(1,1,p DIV 4)})
   fermat_two_squares_exists_alt
                   |- !p. prime p /\ (p MOD 4 = 1) ==> ?(u,v). p = u ** 2 + v ** 2


   Application of Diophantus identity:
   norm_def        |- !a b. norm (a,b) = a ** 2 + b ** 2
   multiply_def    |- !a b c d. (a,b) o (c,d) = (a * c - b * d,a * d + b * c)
   norm_sym        |- !a b. norm (a,b) = norm (b,a)
   norm_eq_0       |- !a b. norm (a,b) = 0 <=> a = 0 /\ b = 0
   norm_eq_1       |- !a b. norm (a,b) = 1 <=> a = 1 /\ b = 0 \/ a = 0 /\ b = 1
   norm_gt_1       |- !a b. 0 < a /\ 0 < b ==> 1 < norm (a,b)
   norm_product    |- !a b c d. norm (a,b) * norm (c,d) =
                                norm (a * d + b * c,
                                      if b * d <= a * c then a * c - b * d else b * d - a * c)
   norm_multiplicative
                   |- !a b c d. b * d <= a * c ==>
                                norm ((a,b) o (c,d)) = norm (a,b) * norm (c,d)

   Alternative Proof of Uniqueness:
   reduce_def          |- !a b. reduce (a,b) = (let g = gcd a b in (a DIV g,b DIV g))
   forward_def         |- !a b c d. forward (a,b) (c,d) =
                                    ((if b * d <= a * c then a * c - b * d else b * d - a * c,
                                      a * d + b * c),
                                     a * c + b * d,a * d - b * c)
   backward_def        |- !x y u v. backward (x,y) (u,v) =
                                (let e = x + u; f = y - v; g = y + v
                                  in (reduce (e,f),reduce (e,g)))
   reduce_eqn          |- !a b m n. (m,n) = reduce (a,b) ==> a * n = b * m
   forward_thm         |- !a b c d x y u v. ((x,y),u,v) = forward (a,b) (c,d) ==>
                                            norm (a,b) * norm (c,d) = norm (x,y) /\
                                            norm (a,b) * norm (c,d) = norm (u,v)
   forward_thm_alt     |- !a b c d. (let j = forward (a,b) (c,d)
                                      in norm (a,b) * norm (c,d) = norm (FST j) /\
                                         norm (a,b) * norm (c,d) = norm (SND j))
   norm_eq_imp         |- !x y u v. norm (x,y) = norm (u,v) ==>
                                    (x < u <=> v < y) /\ (x = u <=> v = y)
   norm_eq_property    |- !x y u v. norm (x,y) = norm (u,v) /\ v <= y ==>
                                    (y - v) * (y + v) = (u - x) * (u + x) /\
                                    (EVEN (y + v) <=> EVEN (u - x))
   norm_eq_coprime_gcd_product
                       |- !x y u v. norm (x,y) = norm (u,v) /\ v <= y /\
                                    coprime x y /\ EVEN (y + v) ==>
                                    gcd (x + u) (y - v) * gcd (x + u) (y + v) = 2 * (x + u)
   backward_thm        |- !x y u v a b c d. norm (x,y) = norm (u,v) /\ v <= y /\
                                            coprime x y /\ ODD x /\ ODD u /\
                                            ((a,b),c,d) = backward (x,y) (u,v) ==>
                                            norm (a,b) * norm (c,d) = norm (x,y) /\
                                            (v < y ==> 1 < norm (a,b) /\ 1 < norm (c,d))
   backward_thm_alt    |- !x y u v. norm (x,y) = norm (u,v) /\ v <= y /\
                                    coprime x y /\ ODD x /\ ODD u ==>
                                    (let j = backward (x,y) (u,v)
                                      in norm (FST j) * norm (SND j) = norm (x,y) /\
                                         (v < y ==> 1 < norm (FST j) /\ 1 < norm (SND j)))
   two_norms_imp_factors
                       |- !n x y u v. n = norm (x,y) /\ n = norm (u,v) /\
                                      v < y /\ coprime x y /\ ODD x /\ ODD u ==>
                                      ?p q. n = p * q /\ 1 < p /\ 1 < q
   prime_two_squares_coprime
                       |- !p a b. prime p /\ p = a ** 2 + b ** 2 ==> coprime a b
   fermat_two_squares_unique_odd
                       |- !n a b c d. prime n /\
                                      n = a ** 2 + b ** 2 /\ ODD a /\
                                      n = c ** 2 + d ** 2 /\ ODD c ==> a = c /\ b = d

*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Fermat Two-Squares Uniqueness.                                            *)
(* ------------------------------------------------------------------------- *)

(* Theorem: prime p /\ (p = a ** 2 + b ** 2) /\ (p = c ** 2 + d ** 2) ==>
          ({a; b} = {c; d}) *)
(* Proof:
   This is to show:
        (a = c) /\ (b = d)
     \/ (a = d) /\ (b = c)                 by doublet_eq

   Step 1: basic properties
   Note 0 < p                              by NOT_PRIME_0
    and 0 < a /\ 0 < b /\ 0 < c /\ 0 < d   by SQ_0, prime_non_square, square_def
    and a * d < p                          by squares_sum_inequality, ADD_COMM
    and b * c < p                          by squares_sum_inequality, ADD_COMM

   Step 2: use identities and prime divisibility
   Note (a * d - b * c) * (a * d + b * c)
      = (d ** 2 - b ** 2) * p              by squares_sum_identity_1
    and (b * c - a * d) * (a * d + b * c)
      = (b ** 2 - d ** 2) * p              by squares_sum_identity_2
   Thus p divides (a * d - b * c) * (a * d + b * c)   by divides_def
    and p divides (b * c - a * d) * (a * d + b * c)   by divides_def
    ==> (p divides (a * d - b * c) /\ p divides (b * c - a * d))
      \/ p divides (a * d + b * c)                    by euclid_prime

   Case: p divides a * d - b * c /\ p divides b * c - a * d
   Note a * d - b * c < p             by a * d < p
    and b * c - a * d < p             by b * c < p
   Thus (a * d - b * c = 0)           by DIVIDES_LEQ_OR_ZERO
    and (b * c - a * d = 0)           by DIVIDES_LEQ_OR_ZERO
    ==> a * d = b * c                 by arithmetic
   Hence (a = c) and (b = d)          by squares_sum_prime_thm

   Case: p divides a * d + b * c
   Note 0 < (a * d + b * c) < 2 * p   by a * d < p, b * c < p, and a,b,c,d <> 0
   Thus a * d + b * c = p             by divides_eq_thm
   For four_squares_identity, break into cases:
   If b * d <= a * c,
      Note p ** 2
         = p * p                      by EXP_2
         = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)
         = p ** 2 + (a * c - b * d) ** 2
                                      by four_squares_identity
      Thus (a * c - b * d) ** 2 = 0   by EQ_ADD_LCANCEL
        or a * c - b * d = 0          by EXP_2_EQ_0
       ==> a * c = b * d              by b * d <= a * c
       ==> (a = c) /\ (b = d)         by squares_sum_prime_thm, ADD_COMM
   If a * c <= b * d,
      Note p ** 2
         = p * p                      by EXP_2
         = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)
         = p ** 2 + (b * d - a * c) ** 2
                                      by four_squares_identity_alt
      Thus (b * d - a * c) ** 2 = 0   by EQ_ADD_LCANCEL
        or b * d - a * c = 0          by EXP_2_EQ_0
       ==> a * c = b * d              by a * c <= b * d
       ==> (a = c) /\ (b = d)         by squares_sum_prime_thm, ADD_COMM
*)
Theorem fermat_two_squares_unique_thm:
  !p a b c d. prime p /\ (p = a ** 2 + b ** 2) /\ (p = c ** 2 + d ** 2) ==>
               ({a; b} = {c; d})
Proof
  rpt strip_tac >>
  simp[doublet_eq] >>
  spose_not_then strip_assume_tac >>
  `0 < p` by metis_tac[NOT_PRIME_0, NOT_ZERO] >>
  `0 < a /\ 0 < b /\ 0 < c /\ 0 < d` by metis_tac[SQ_0, prime_non_square, square_def, EXP_2, ADD, ADD_0, NOT_ZERO] >>
  `a * d < p` by metis_tac[squares_sum_inequality, ADD_COMM] >>
  `b * c < p` by metis_tac[squares_sum_inequality, ADD_COMM] >>
  `(a * d - b * c) * (a * d + b * c) = (d ** 2 - b ** 2) * p` by rw[squares_sum_identity_1] >>
  `(b * c - a * d) * (a * d + b * c) = (b ** 2 - d ** 2) * p` by rw[squares_sum_identity_2] >>
  `p divides (a * d - b * c) * (a * d + b * c)` by metis_tac[divides_def] >>
  `p divides (b * c - a * d) * (a * d + b * c)` by metis_tac[divides_def] >>
  `(p divides (a * d - b * c) /\ p divides (b * c - a * d)) \/ p divides (a * d + b * c)` by metis_tac[euclid_prime] >| [
    `a * d - b * c < p /\ b * c - a * d < p` by fs[] >>
    `(a * d - b * c = 0) /\ (b * c - a * d = 0)` by metis_tac[DIVIDES_LEQ_OR_ZERO, NOT_LESS] >>
    `a * d = b * c` by fs[] >>
    metis_tac[squares_sum_prime_thm],
    `a * d + b * c = p` by
  (`a * d + b * c < 2 * p` by fs[] >>
    `0 < a * d + b * c` by metis_tac[MULT, ADD_EQ_0, MULT_EQ_0, NOT_ZERO] >>
    rw[divides_eq_thm]) >>
    Cases_on `b * d <= a * c` >| [
      `p ** 2 = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = p ** 2 + (a * c - b * d) ** 2` by rfs[four_squares_identity] >>
      `(a * c - b * d) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `a * c - b * d = 0` by metis_tac[EXP_2_EQ_0] >>
      `a * c = b * d` by fs[] >>
      metis_tac[squares_sum_prime_thm, ADD_COMM],
      `p ** 2 = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)` by fs[] >>
      `_ = p ** 2 + (b * d - a * c) ** 2` by rfs[four_squares_identity_alt] >>
      `(b * d - a * c) ** 2 = 0` by metis_tac[EQ_ADD_LCANCEL, ADD_0] >>
      `b * d - a * c = 0` by metis_tac[EXP_2_EQ_0] >>
      `a * c = b * d` by fs[] >>
      metis_tac[squares_sum_prime_thm, ADD_COMM]
    ]
  ]
QED

(* Theorem: prime p /\ ODD a /\ EVEN b /\ (p = a ** 2 + b ** 2) /\
            ODD c /\ EVEN d /\ (p = c ** 2 + d ** 2) ==> ((a = c) /\ (b = d)) *)
(* Proof:
   Note {a; b} = {c; d}                          by fermat_two_squares_unique_thm
   Thus (a = c) /\ (b = d) \/ (a = d) /\ (b = c) by EXTENSION
   But (a <> d) \/ (b <> c)                      by EVEN_ODD
    so (a = c) /\ (b = d).
*)
Theorem fermat_two_squares_unique_odd_even:
  !p a b c d. prime p /\ ODD a /\ EVEN b /\ (p = a ** 2 + b ** 2) /\
               ODD c /\ EVEN d /\ (p = c ** 2 + d ** 2) ==> ((a = c) /\ (b = d))
Proof
  ntac 7 strip_tac >>
  `{a; b} = {c; d}` by fs[fermat_two_squares_unique_thm] >>
  fs[EXTENSION] >>
  metis_tac[EVEN_ODD]
QED

(* ------------------------------------------------------------------------- *)
(* Two Squares Fixes.                                                        *)
(* ------------------------------------------------------------------------- *)

(* Note: Zagier fixes is simple. *)

(* Theorem: prime p /\ (p MOD 4 = 1) ==> (fixes zagier (mills p) = {(1,1,p DIV 4)}) *)
(* Proof:
   Let s = mills p,
       k = p DIV 4,
       a = fixes zagier s.
   The goal becomes: a = {(1,1,k)}

   Use EXTENSION to show two sets are equal:
   (1) (x,y,z) IN a ==> x = 1, y = 1, z = k.
       Note (x,y,z) IN s /\
            zagier (x,y,z) = (x,y,z)  by fixes_element
        but x <> 0                    by mills_prime_triple_nonzero
         so x = y                     by zagier_fix
        Now p = windmill (x, x, z)    by mills_element
        ==> x = 1, y = 1, z = k       by windmill_trivial_prime
   (2) (1,1,k) IN a
       Note p = k * 4 + p MOD 4       by DIVISION
              = 4 * k + 1             by p MOD 4 = 1
              = windmill (1, 1, k)    by windmill_trivial
         so (1,1,k) IN s              by mills_element
        and zagier (1,1,k) = (1,1,k)  by zagier_1_1_z
        ==> (1,1,k) IN a              by fixes_element
*)
Theorem zagier_fixes_prime:
  !p. prime p /\ (p MOD 4 = 1) ==> (fixes zagier (mills p) = {(1,1,p DIV 4)})
Proof
  rpt strip_tac >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `k = p DIV 4` >>
  qabbrev_tac `a = fixes zagier s` >>
  simp[EXTENSION, FORALL_PROD, EQ_IMP_THM] >>
  ntac 5 strip_tac >| [
    `(p_1,p_1',p_2) IN s /\ (zagier (p_1,p_1',p_2) = (p_1,p_1',p_2))` by metis_tac[fixes_element] >>
    `p_1 = p_1'` by metis_tac[zagier_fix, mills_prime_triple_nonzero] >>
    metis_tac[windmill_trivial_prime, mills_element],
    `p = k * 4 + p MOD 4` by rw[DIVISION, Abbr`k`] >>
    `_ = windmill (1, 1, k)` by rw[windmill_trivial] >>
    `(1,1,k) IN s` by rw[mills_element, Abbr`s`] >>
    fs[fixes_element, zagier_1_1_z, Abbr`a`]
  ]
QED

(* Note: Flip fixes is at most 1 depends on uniqueness, otherwise can be more than 1. *)

(* Theorem: prime p /\ (p MOD 4 = 1) ==> CARD (fixes flip (mills p)) <= 1 *)
(* Proof:
   Let s = mills p,
       b = fixes flip s.
   The goal is: CARD b <= 1.
   By contradiction, suppose CARD b > 1.
   Then CARD b <> 0,
     so ?u. u IN b             by CARD_EMPTY, MEMBER_NOT_EMPTY
   Note u IN s                 by fixes_element
    and ?x y. u = (x,y,y)      by triple_parts, flip_fix
   Thus p = windmill (x, y, y) by mills_element

   Also CARD b <> 1,
   Then ~SING b                by SING_CARD_1
     so b <> EMPTY             by MEMBER_NOT_EMPTY
    and ?v. v IN b /\ v <> u   by SING_ONE_ELEMENT, b <> EMPTY
   Note v IN s                 by fixes_element
    and ?h k. v = (h,k,k)      by triple_parts, flip_fix
   Thus p = windmill (h, k, k) by mills_element

   Let c = 2 * y,
       d = 2 * k.
    Now ODD p                  by odd_by_mod_4, p MOD 4 = 1
    and (p = x ** 2 + c ** 2) /\ ODD x   by windmill_x_y_y
    and (p = h ** 2 + d ** 2) /\ ODD h   by windmill_x_y_y
    but EVEN c /\ EVEN d                 by EVEN_DOUBLE
    ==> (x = h) /\ (c = d)     by fermat_two_squares_unique_odd_even
     so (x = h) /\ (y = k)     by EQ_MULT_LCANCEL
     or v = u                  by PAIR_EQ
   This contradicts v <> u.
*)
Theorem flip_fixes_prime_card_upper:
  !p. prime p /\ (p MOD 4 = 1) ==> CARD (fixes flip (mills p)) <= 1
Proof
  rpt strip_tac >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `b = fixes flip s` >>
  spose_not_then strip_assume_tac >>
  `CARD b <> 0 /\ CARD b <> 1` by decide_tac >>
  `?u. u IN b` by metis_tac[CARD_EMPTY, MEMBER_NOT_EMPTY] >>
  `u IN s /\ ?x y. u = (x,y,y)` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  `p = windmill (x, y, y)` by fs[mills_element, Abbr`s`] >>
  `~SING b` by metis_tac[SING_CARD_1] >>
  `b <> EMPTY` by metis_tac[MEMBER_NOT_EMPTY] >>
  `?v. v IN b /\ v <> u` by metis_tac[SING_ONE_ELEMENT] >>
  `v IN s /\ ?h k. v = (h,k,k)` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  `p = windmill (h, k, k)` by fs[mills_element, Abbr`s`] >>
  qabbrev_tac `c = 2 * y` >>
  qabbrev_tac `d = 2 * k` >>
  `ODD p` by rfs[odd_by_mod_4] >>
  `(p = x ** 2 + c ** 2) /\ ODD x` by metis_tac[windmill_x_y_y] >>
  `(p = h ** 2 + d ** 2) /\ ODD h` by metis_tac[windmill_x_y_y] >>
  `EVEN c /\ EVEN d` by rw[EVEN_DOUBLE, Abbr`c`, Abbr`d`] >>
  `(x = h) /\ (c = d)` by metis_tac[fermat_two_squares_unique_odd_even] >>
  `y = k` by fs[Abbr`c`, Abbr`d`] >>
  metis_tac[PAIR_EQ]
QED

(* ------------------------------------------------------------------------- *)
(* Fermat Two-Squares Existence.                                             *)
(* ------------------------------------------------------------------------- *)

(* Theorem: prime p /\ (p MOD 4 = 1) ==> ?x y. p = windmill (x, y, y) *)
(* Proof:
   Let m = mills p, the solutions (x,y,z) of p = x ** 2 + 4 * y * z.
   Note ~square p                 by prime_non_square
     so FINITE m                  by mills_non_square_finite
    and zagier involute m         by zagier_involute_mills_prime
    and flip involute m           by flip_involute_mills

   Now work out fixed points:
   Let a = fixes zagier m, b = fixes flip m.
   Let k = p DIV 4.
   Then a = {(1,1,k)}                  by zagier_fixes_prime

   The punch line:
   Note ODD (CARD a) <=> ODD (CARD b)  by involute_two_fixes_both_odd
    now CARD a = 1                     by CARD_SING
     so ODD (CARD b)                   by ODD_1
    ==> CARD b <> 0                    by ODD
     or b <> EMPTY                     by CARD_EMPTY
   Thus ? (x, y, z) IN b               by MEMBER_NOT_EMPTY
    and (x, y, z) IN m /\
        (flip (x, y, z) = (x, y, z))   by fixes_element
     so y = z                          by flip_fix
     or (x,y,y) in m                   by above
   Thus p = windmill (x, y, y)         by mills_element
*)
Theorem fermat_two_squares_exists_windmill:
  !p. prime p /\ (p MOD 4 = 1) ==> ?x y. p = windmill (x, y, y)
Proof
  rpt strip_tac >>
  qabbrev_tac `m = mills p` >>
  `~square p` by metis_tac[prime_non_square] >>
  `FINITE m` by fs[mills_non_square_finite, Abbr`m`] >>
  `zagier involute m` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute m` by metis_tac[flip_involute_mills] >>
  qabbrev_tac `a = fixes zagier m` >>
  qabbrev_tac `b = fixes flip m` >>
  qabbrev_tac `k = p DIV 4` >>
  `a = {(1,1,k)}` by rw[zagier_fixes_prime, Abbr`a`, Abbr`m`] >>
  `CARD a = 1` by rw[] >>
  `ODD (CARD a) <=> ODD (CARD b)` by rw[involute_two_fixes_both_odd, Abbr`a`, Abbr`b`] >>
  `ODD (CARD b)` by metis_tac[ODD_1] >>
  `CARD b <> 0` by metis_tac[ODD] >>
  `b <> EMPTY` by metis_tac[CARD_EMPTY] >>
  `?t. t IN b` by rw[MEMBER_NOT_EMPTY] >>
  `?x y z. t = (x, y, z)` by rw[triple_parts] >>
  `t IN m /\ (flip (x, y, z) = (x, y, z))` by metis_tac[fixes_element] >>
  `y = z` by fs[flip_fix] >>
  metis_tac[mills_element]
QED

(* Theorem: prime p /\ (p MOD 4 = 1) ==>
            ?(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2) *)
(* Proof:
   Note ?x y. p = windmill (x, y, y)      by fermat_two_squares_exists_windmill
                = x ** 2 + (2 * y) ** 2   by windmill_by_squares
   Put (u, v) = (x, 2 * y).
   Remains to show: ODD u /\ EVEN v.
   Note EVEN v                            by EVEN_DOUBLE
    and EVEN (v * v)                      by EVEN_MULT
   Also p = u ** 2 + v ** 2               by above
          = u * u + v * v                 by EXP_2
     or p - u * u = v * v                 by arithmetic
    Now ODD p                             by odd_by_mod_4, p MOD 4 = 1
     so ODD (u * u)                       by EVEN_SUB, EVEN_ODD
     or ODD u                             by ODD_MULT
*)
Theorem fermat_two_squares_exists_odd_even:
  !p. prime p /\ (p MOD 4 = 1) ==>
   ?(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2)
Proof
  rw[ELIM_UNCURRY] >>
  `?x y. p = windmill (x, y, y)` by rw[fermat_two_squares_exists_windmill] >>
  `_ = x ** 2 + (2 * y) ** 2` by rw[windmill_by_squares] >>
  qabbrev_tac `u = x` >>
  qabbrev_tac `v = 2 * y` >>
  qexists_tac `(u,v)` >>
  rfs[] >>
  `EVEN v` by rw[EVEN_DOUBLE, Abbr`v`] >>
  `EVEN (v * v)` by rw[EVEN_MULT] >>
  `ODD p` by rw[odd_by_mod_4] >>
  `p = u * u + v * v` by simp[] >>
  `u * u <= p` by decide_tac >>
  `v * v = p - u * u` by decide_tac >>
  `ODD (u * u)` by metis_tac[EVEN_SUB, EVEN_ODD] >>
  fs[ODD_MULT]
QED

(* ------------------------------------------------------------------------- *)
(* Fermat Two-Squares Theorem.                                               *)
(* ------------------------------------------------------------------------- *)

(* Theorem: prime p /\ (p MOD 4 = 1) ==> SING (fixes flip (mills p)) *)
(* Proof:
   Let s = mills p,
       b = fixes flip s.
   The goal is: SING b.
   Note ?x y. p = windmill (x, y, y)
                            by fermat_two_squares_exists_windmill
    Let u = (x,y,y).
   Then u IN s              by mills_element
    and u IN b              by fixes_element, flip_fix
   By SING_DEF, EXTENSION, this is to show:
      !t. t IN b ==> t = u.
   Note t IN s              by fixes_element
    and ?h k. t = (h,k,k)   by triple_parts, flip_fix
   Thus p = windmill (h, k, k) by mills_element

   Let c = 2 * y,
       d = 2 * k.
    Now ODD p               by odd_by_mod_4, p MOD 4 = 1
    and (p = x ** 2 + c ** 2) /\ ODD x   by windmill_x_y_y
    and (p = h ** 2 + d ** 2) /\ ODD h   by windmill_x_y_y
    but EVEN c /\ EVEN d                 by EVEN_DOUBLE
    ==> (x = h) /\ (c = d)  by fermat_two_squares_unique_odd_even
     so (x = h) /\ (y = k)  by EQ_MULT_LCANCEL
     or t = u.
*)
Theorem flip_fixes_prime_sing:
  !p. prime p /\ (p MOD 4 = 1) ==> SING (fixes flip (mills p))
Proof
  rpt strip_tac >>
  `?x y. p = windmill (x, y, y)` by rw[fermat_two_squares_exists_windmill] >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `b = fixes flip s` >>
  qabbrev_tac `u = (x,y,y)` >>
  `u IN s` by rw[mills_element, Abbr`u`, Abbr`s`] >>
  `u IN b` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  simp[SING_DEF, EXTENSION] >>
  qexists_tac `u` >>
  (rewrite_tac[EQ_IMP_THM] >> simp[]) >>
  rpt strip_tac >>
  `x' IN s /\ ?h k. x' = (h,k,k)` by metis_tac[fixes_element, triple_parts, flip_fix] >>
  `p = windmill (h, k, k)` by fs[mills_element, Abbr`s`] >>
  qabbrev_tac `c = 2 * y` >>
  qabbrev_tac `d = 2 * k` >>
  `ODD p` by rfs[odd_by_mod_4] >>
  `(p = x ** 2 + c ** 2) /\ ODD x` by metis_tac[windmill_x_y_y] >>
  `(p = h ** 2 + d ** 2) /\ ODD h` by metis_tac[windmill_x_y_y] >>
  `EVEN c /\ EVEN d` by rw[EVEN_DOUBLE, Abbr`c`, Abbr`d`] >>
  `(x = h) /\ (c = d)` by metis_tac[fermat_two_squares_unique_odd_even] >>
  `y = k` by fs[Abbr`c`, Abbr`d`] >>
  simp[Abbr`u`]
QED

(* Theorem: prime p /\ (p MOD 4 = 1) ==>
            (fixes flip (mills p) =
               {(let u = (1,1,p DIV 4) ;
                     n = iterate_period (zagier o flip) u
                  in FUNPOW (zagier o flip) (HALF n) u)}) *)
(* Proof:
   Let s = mills p,
       v = FUNPOW (zagier o flip) (HALF n) u,
       a = fixes zagier s,
       b = fixes flip s.
   The goal is: b = {v}.

   Note a = {u}              by zagier_fixes_prime
     so u IN a               by IN_SING
   Also ~square p            by prime_non_square
     so FINITE s             by mills_non_square_finite
    ==> zagier involute s    by zagier_involute_mills_prime
    and flip involute s      by flip_involute_mills
    ==> ODD n                by involute_involute_fix_sing_period_odd
     so v IN b               by involute_involute_fix_orbit_fix_odd
   Thus b = {v}              by flip_fixes_prime_sing, SING_DEF, IN_SING
*)
Theorem flip_fixes_prime:
  !p. prime p /\ (p MOD 4 = 1) ==>
       let u = (1, 1, p DIV 4);
           n = iterate_period (zagier o flip) u
        in fixes flip (mills p) = {FUNPOW (zagier o flip) (n DIV 2) u}
Proof
  rw_tac bool_ss[] >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `v = FUNPOW (zagier o flip) (HALF n) u` >>
  qabbrev_tac `a = fixes zagier s` >>
  qabbrev_tac `b = fixes flip s` >>
  `a = {u}` by metis_tac[zagier_fixes_prime] >>
  `u IN a` by fs[] >>
  `~square p` by metis_tac[prime_non_square] >>
  `FINITE s` by fs[mills_non_square_finite, Abbr`s`] >>
  `zagier involute s` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
  last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
  `ODD n` by rfs[] >>
  drule_then strip_assume_tac involute_involute_fix_orbit_fix_odd >>
  last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
  `v IN b` by rfs[] >>
  metis_tac[flip_fixes_prime_sing, SING_DEF, IN_SING]
QED

(* Theorem: prime p /\ (p MOD 4 = 1) /\ (u = (1, 1, p DIV 4)) /\
           (n = iterate_period (zagier o flip) u) ==>
           (fixes flip (mills p) = {FUNPOW (zagier o flip) (n DIV 2) u}) *)
(* Proof: by flip_fixes_prime *)
Theorem flip_fixes_prime_alt:
  !p u n. prime p /\ (p MOD 4 = 1) /\ (u = (1, 1, p DIV 4)) /\
           (n = iterate_period (zagier o flip) u) ==>
           (fixes flip (mills p) = {FUNPOW (zagier o flip) (n DIV 2) u})
Proof
  metis_tac[flip_fixes_prime]
QED

(* Theorem: !p. prime p /\ (p MOD 4 = 1) ==>
            ?!(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2) *)
(* Proof:
   Existence part    by fermat_two_squares_exists_odd_even
   Uniqueness part   by fermat_two_squares_unique_odd_even
*)
Theorem fermat_two_squares_thm:
  !p. prime p /\ (p MOD 4 = 1) ==>
   ?!(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2)
Proof
  rw[ELIM_UNCURRY] >>
  rw[EXISTS_UNIQUE_THM] >| [
    drule_then strip_assume_tac fermat_two_squares_exists_odd_even >>
    first_x_assum (drule_all_then strip_assume_tac) >>
    fs[ELIM_UNCURRY] >>
    metis_tac[],
    metis_tac[fermat_two_squares_unique_odd_even, PAIR_FST_SND_EQ]
  ]
QED

(* Theorem: prime p ==>
            ((p MOD 4 = 1) <=> ?!(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2)) *)
(* Proof:
   If part: p MOD 4 = 1 ==> ?!(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2)
      This is true                       by fermat_two_squares_thm
   Only-if part: ?!(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2) ==> p MOD 4 = 1
      Note ?(u,v). ODD u /\ EVEN v /\
                  (p = u ** 2 + v ** 2)  by EXISTS_UNIQUE_THM
       but ODD (u ** 2)                  by ODD_EXP
       and EVEN (v ** 2)                 by EVEN_EXP
      Thus ODD p                         by ODD_ADD, EVEN_ODD
        so p MOD 4 = 1 or p MOD 4 = 3    by mod_4_odd
       but p MOD 4 <> 3                  by mod_4_not_squares
       ==> p MOD 4 = 1
*)
Theorem fermat_two_squares_iff:
  !p. prime p ==>
  ((p MOD 4 = 1) <=> ?!(u,v). ODD u /\ EVEN v /\ (p = u ** 2 + v ** 2))
Proof
  rw[EQ_IMP_THM] >-
  rw[fermat_two_squares_thm] >>
  fs[ELIM_UNCURRY, EXISTS_UNIQUE_THM] >>
  `p MOD 4 = 1` suffices_by simp[] >>
  qabbrev_tac `u = FST x` >>
  qabbrev_tac `v = SND x` >>
  `ODD (u ** 2)` by rw[ODD_EXP] >>
  `EVEN (v ** 2)` by rw[EVEN_EXP] >>
  `ODD p` by fs[ODD_ADD, EVEN_ODD] >>
  `(p MOD 4 = 1) \/ (p MOD 4 = 3)` by fs[mod_4_odd] >>
  fs[mod_4_not_squares]
QED

(* ------------------------------------------------------------------------- *)
(* Fermat Two-Squares Algorithm.                                             *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (iterate_period (zagier o flip) (1,1,z) = 1) <=> (z = 1) *)
(* Proof:
   By iterate_period_thm, this is to show:
   (1) zagier (flip (1,1,z)) = (1,1,z) ==> z = 1
       This is true                by zagier_flip_1_1_z
   (2) zagier (flip (1,1,1)) = (1,1,1)
       This is true                by zagier_flip_1_1_z
*)
Theorem zagier_flip_1_1_z_period:
  !z. (iterate_period (zagier o flip) (1,1,z) = 1) <=> (z = 1)
Proof
  rw[iterate_period_thm, zagier_flip_1_1_z]
QED

(* Theorem: prime p /\ (p MOD 4 = 1) /\ (u = (1, 1, p DIV 4)) /\
            (n = iterate_period (zagier o flip) u) /\ (g = \(x,y,z). y <> z) ==>
            !j. j < n DIV 2 ==> g (FUNPOW (zagier o flip) j u) /\
            ~g (FUNPOW (zagier o flip) (n DIV 2) u) *)
(* Proof:
   Let s = mills p,
       f = zagier o flip,
       h = n DIV 2,
       v = FUNPOW f h u.
   The goal is: ~g v /\ !j. j < h ==> g (FUNPOW f j u)

   Note ~square p                 by prime_non_square
     so FINITE s                  by mills_non_square_finite
    and zagier involute s         by zagier_involute_mills_prime
    and flip involute s           by flip_involute_mills
     so f PERMUTES s              by involute_involute_permutes
   Also u IN s                    by mills_element_trivial
    and 0 < n                     by iterate_period_pos
     so h < n                     by HALF_LT, 0 < n

   Without assuming the following:
   Note fixes flip s = {v}        by flip_fixes_prime
   But use:
   Note fixes zagier s = {u}      by zagier_fixes_prime
     so ODD n                     by involute_involute_fix_sing_period_odd
   Then use involute_involute_fix_odd_period_fix:
   !j. j < n ==> (FUNPOW (zagier o flip) j u IN fixes flip s <=> (j = h))

   Case: n = 1,
   Then h = 0                     by HALF_EQ_0, n <> 0
     so v = u                     by FUNPOW_0
          = (1,1,1)               by zagier_flip_1_1_z_period
     or ~g v                      by definition of g
    and the other case needs j < 0, which is irrelevant.

   Case: n <> 1.
    and h <> 0, so 0 < h          by HALF_EQ_0

   Claim: ~g v
   Note 0 < h < n,
     so v IN fixes flip s         by involute_involute_fix_odd_period_fix
    Now ?x y z. v = (x,y,z)       by triple_parts
    and y = z                     by fixes_element, flip_fix
     or ~g v                      by definition of g

   Claim: !j. j < h ==> g (FUNPOW f j u)
   By contradiction, suppose ?j. j < h /\ ~g (FUNPOW f j u).
   Let w = FUNPOW f j u.
   Note ?x y z. w = (x,y,z)       by triple_parts
     so y = z                     by ~g w
    Now w IN s                    by FUNPOW_closure
    and flip w = w                by flip_fix
    ==> w IN (fixes flip u}       by fixes_element
   Then j = h                     by involute_involute_fix_odd_period_fix, j < n
   which contradicts j < h.
*)
Theorem flip_fixes_iterates_prime:
  !p u n g. prime p /\ (p MOD 4 = 1) /\ (u = (1, 1, p DIV 4)) /\
             (n = iterate_period (zagier o flip) u) /\ (g = \(x,y,z). y <> z) ==>
             (~g (FUNPOW (zagier o flip) (n DIV 2) u) /\
              (!j. j < n DIV 2 ==> g (FUNPOW (zagier o flip) j u)))
Proof
  ntac 5 strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `h = n DIV 2` >>
  qabbrev_tac `v = FUNPOW f h u` >>
  `~square p` by rw[prime_non_square] >>
  `FINITE s` by metis_tac[mills_non_square_finite] >>
  `zagier involute s` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `fixes zagier s = {u}` by metis_tac[zagier_fixes_prime] >>
  qabbrev_tac `X = (u = (1,1,p DIV 4))` >>
  qabbrev_tac `Y = (n = iterate_period f u)` >>
  qabbrev_tac `Z = (g = (\(x,y,z). y <> z))` >>
  drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
  last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
  rfs[] >>
  `f PERMUTES s` by fs[involute_involute_permutes, Abbr`f`] >>
  `u IN s` by rw[mills_element_trivial, Abbr`s`, Abbr`X`] >>
  `0 < n` by metis_tac[iterate_period_pos] >>
  `h < n` by rw[HALF_LT, Abbr`h`] >>
  Cases_on `n = 1` >| [
    `h = 0` by rw[Abbr`h`] >>
    `v = u` by rw[Abbr`v`] >>
    `_ = (1,1,1)` by metis_tac[zagier_flip_1_1_z_period] >>
    rfs[Abbr`Z`],
    `h <> 0` by rw[HALF_EQ_0, Abbr`h`] >>
    `0 < h` by decide_tac >>
    drule_then strip_assume_tac involute_involute_fix_odd_period_fix >>
    last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
    rfs[] >>
    qunabbrev_tac `Z` >>
    `~g v` by
  (`FUNPOW (zagier o flip) h u IN fixes flip s <=> (h = HALF n)` by metis_tac[] >>
    `v IN fixes flip s` by metis_tac[] >>
    `?x y z. v = (x,y,z)` by rw[triple_parts] >>
    `y = z` by metis_tac[fixes_element, flip_fix] >>
    fs[]) >>
    rpt strip_tac >>
    spose_not_then strip_assume_tac >>
    qabbrev_tac `w = FUNPOW f j u` >>
    `?x y z. w = (x,y,z)` by rw[triple_parts] >>
    `~g w <=> (y = z)` by fs[] >>
    `y = z` by fs[] >>
    `w IN s` by rw[FUNPOW_closure, Abbr`w`] >>
    `w IN (fixes flip s)` by fs[flip_fix, fixes_element] >>
    `FUNPOW (zagier o flip) j u IN fixes flip s <=> (j = HALF n)` by metis_tac[LESS_TRANS] >>
    `j = h` by metis_tac[] >>
    decide_tac
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Computation by WHILE loop.                                                *)
(* ------------------------------------------------------------------------- *)

(* Define the exit condition *)
Definition found_def:
    found (x:num, y:num, z:num) <=> (y = z)
End

(* Theorem: $~ o found = (\(x,y,z). y <> z) *)
(* Proof: by found_def, FUN_EQ_THM. *)
Theorem found_not:
  $~ o found = (\(x,y,z). y <> z)
Proof
  rw[found_def, FORALL_PROD, FUN_EQ_THM]
QED

(* Idea: use WHILE for search. Develop theory in iterateCompute. *)

(* Compute two squares of Fermat's theorem by WHILE loop. *)
Definition two_sq_def:
    two_sq n = WHILE ($~ o found) (zagier o flip) (1,1,n DIV 4)
End

(*
> EVAL ``two_sq 5``; = (1,2): thm   (1,1,1)
> EVAL ``two_sq 13``; = (3,2): thm  (3,1,1)
> EVAL ``two_sq 17``; = (1,4): thm  (1,2,2)
> EVAL ``two_sq 29``; = (5,2): thm  (5,1,1)
> EVAL ``two_sq 97``; = (9,4): thm  (9,2,2)
*)

(* Theorem: two_sq n = WHILE (\(x,y,z). y <> z) (zagier o flip) (1, 1, n DIV 4) *)
(* Proof: by two_sq_def, found_not. *)
Theorem two_sq_alt:
  !n. two_sq n = WHILE (\(x,y,z). y <> z) (zagier o flip) (1, 1, n DIV 4)
Proof
  simp[two_sq_def, found_not]
QED

(* Using direct WHILE, no need for Hoare specification. *)

(* Theorem: prime p /\ (p MOD 4 = 1) ==> two_sq p IN fixes flip (mills p) *)
(* Proof:
   Let s = mills p,
       f = zagier o flip,
       u = (1,1,p DIV 4),
       n = iterate_period f u.
   By two_sq_def, this is to show: WHILE ($~ o found) f u IN fixes flip s

   Note (~) o found = (\t. flip t <> t)    by found_def, flip_def, FUN_EQ_THM
   Thus the goal is: WHILE (\t. flip t <> t) f u IN fixes flip s

   Note ~square p               by prime_non_square
     so FINITE s                by mills_non_square_finite
    and zagier involute s       by zagier_involute_mills_prime
    and flip involute s         by flip_involute_mills
   also fixes zagier s = {u}    by zagier_fixes_prime
     so u IN fixes zagier s     by IN_SING
    and ODD n                   by involute_involute_fix_sing_period_odd
    ==> WHILE (\y. flip y <> y) f u IN fixes flip s
                                by involute_involute_fix_odd_period_fix_while
*)
Theorem two_sq_thm:
  !p. prime p /\ (p MOD 4 = 1) ==> two_sq p IN fixes flip (mills p)
Proof
  rw[two_sq_def] >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `u = (1,1,p DIV 4)` >>
  qabbrev_tac `n = iterate_period f u` >>
  `(~) o found = \t. flip t <> t` by
  (rw[FUN_EQ_THM] >>
  `?x y z. t = (x,y,z)` by rw[triple_parts] >>
  rw[found_def, flip_def]) >>
  simp[] >>
  assume_tac (involute_involute_fix_odd_period_fix_while |> ISPEC ``zagier``) >>
  last_x_assum (qspecl_then [`flip`, `s`, `n`, `u`] strip_assume_tac) >>
  `~square p` by rw[prime_non_square] >>
  `FINITE s` by rw[mills_non_square_finite, Abbr`s`] >>
  `zagier involute s` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `fixes zagier s = {u}` by rw[zagier_fixes_prime, Abbr`s`, Abbr`u`] >>
  drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
  last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
  rfs[Abbr`f`]
QED

(* Very good -- nice and simple! *)

(* Theorem: prime p /\ (p MOD 4 = 1) ==>
            HOARE_SPEC (fixes zagier (mills p))
                       (WHILE ($~ o found) (zagier o flip))
                       (fixes flip (mills p)) *)
(* Proof:
   Let s = mills p,
       f = zagier o flip,
       u = (1,1,p DIV 4),
       n = iterate_period f u,
       h = n DIV 2,
       v = FUNPOW f h u,
       g = $~ o found,  use ~g to exit WHILE loop,
       a = fixes zagier s,
       b = fixes flip s.
   The goal is: HOARE_SPEC a (WHILE g f) b

   Note a = {u}             by zagier_fixes_prime
    and b = {v}             by flip_fixes_prime

   If n = 1, that is, period = 1 for prime p = 5.
   then u = (1,1,1)         by zagier_flip_1_1_z_period, triple_parts
     so ~g u                by definition of g, found_def
    but u IN s              by mills_element_trivial
     so u IN b              by fixes_element, flip_fix
    ==> b = a               by EQUAL_SING, IN_SING
   Thus HOARE_SPEC {u} (WHILE g f) {u}
                            by iterate_while_hoare_0
     or HOARE_SPEC a (WHILE g f) b.

   If n <> 1,
   Note ~square p           by prime_non_square
     so FINITE s            by mills_non_square_finite
    and zagier involute s   by zagier_involute_mills_prime
    and flip involute s     by flip_involute_mills
     so f PERMUTES s        by involute_involute_permutes
   Also 0 < n               by iterate_period_pos, u IN s
    and ODD n               by involute_involute_fix_sing_period_odd
     so n <> 2              by EVEN_2, ODD_EVEN
    ==> 1 + h < n           by HALF_ADD1_LT, 2 < n
     or h < n - 1           by arithmetic

    Now ~g v /\ (!j. j < h ==> g (FUNPOW f j u))
                                      by flip_fixes_iterates_prime, found_not
    Thus HOARE_SPEC a (WHILE g f) b   by iterate_while_hoare
*)
Theorem two_sq_while_hoare:
  !p. prime p /\ (p MOD 4 = 1) ==>
       HOARE_SPEC (fixes zagier (mills p))
                  (WHILE ($~ o found) (zagier o flip))
                  (fixes flip (mills p))
Proof
  rpt strip_tac >>
  qabbrev_tac `s = mills p` >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `u = (1,1,p DIV 4)` >>
  qabbrev_tac `n = iterate_period f u` >>
  qabbrev_tac `h = n DIV 2` >>
  qabbrev_tac `v = FUNPOW f h u` >>
  qabbrev_tac `g = $~ o found` >>
  qabbrev_tac `a = fixes zagier s` >>
  qabbrev_tac `b = fixes flip s` >>
  `u IN s` by rw[mills_element_trivial, Abbr`u`, Abbr`s`] >>
  `a = {u}` by rw[zagier_fixes_prime, Abbr`a`, Abbr`u`, Abbr`s`] >>
  `b = {v}` by metis_tac[flip_fixes_prime, SING_DEF] >>
  Cases_on `n = 1` >| [
    `u = (1,1,1)` by metis_tac[zagier_flip_1_1_z_period, triple_parts] >>
    `~g u` by fs[found_def, Abbr`g`] >>
    `u IN s` by rw[mills_element_trivial, Abbr`u`, Abbr`s`] >>
    `u IN b` by rw[fixes_element, flip_fix, Abbr`b`] >>
    `b = a` by metis_tac[EQUAL_SING, IN_SING] >>
    rw[iterate_while_hoare_0],
    `~square p` by rw[prime_non_square] >>
    `FINITE s` by metis_tac[mills_non_square_finite] >>
    `zagier involute s` by metis_tac[zagier_involute_mills_prime] >>
    `flip involute s` by metis_tac[flip_involute_mills] >>
    `f PERMUTES s` by fs[involute_involute_permutes, Abbr`f`] >>
    `0 < n` by metis_tac[iterate_period_pos] >>
    drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
    last_x_assum (qspecl_then [`zagier`, `flip`, `n`, `u`] strip_assume_tac) >>
    `ODD n` by rfs[] >>
    `n <> 2` by metis_tac[EVEN_2, ODD_EVEN] >>
    `1 + h < n` by rw[HALF_ADD1_LT, Abbr`h`] >>
    `h < n - 1` by decide_tac >>
    drule_then strip_assume_tac iterate_while_hoare >>
    last_x_assum (qspecl_then [`u`, `f`, `n-1`, `n`, `g`, `h`] strip_assume_tac) >>
    `~g v /\ (!j. j < h ==> g (FUNPOW f j u))` by metis_tac[found_not, flip_fixes_iterates_prime] >>
    rfs[]
  ]
QED

(* Theorem: prime p /\ (p MOD 4 = 1) ==>
            let (x,y,z) = WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)
            in p = x ** 2 + (y + z) ** 2 *)
(* Proof:
   Let s = mills p,
       u = (1,1,p DIV 4),
       a = fixes zagier s,
       b = fixes flip s.

   Note HOARE_SPEC a
        (WHILE ($~ o found) (zagier o flip)) b
                                  by two_sq_while_hoare
    and a = {u}                   by zagier_fixes_prime

   By HOARE_SPEC_DEF, this is to show:
   !s. a s ==> b (WHILE ($~ o found) (zagier o flip) s)
   Thus s = u                     by IN_SING, function as set
   Let w = WHILE ($~ o found) (zagier o flip) u.
   Then ?x y z. w = (x,y,z)       by triple_parts
    and w IN b                    by b w, function as set
     so w IN s /\ y = z           by fixes_element, flip_fix
   Thus p
      = windmill (x, y, z)        by mills_element, w IN s
      = windmill (x, y, y)        by y = z
      = x ** 2 + (2 * y) ** 2     by windmill_by_squares
      = x ** 2 + (y + z) ** 2     by arithmetic, y = z
*)
Theorem two_sq_while_thm:
  !p. prime p /\ (p MOD 4 = 1) ==>
       let (x,y,z) = WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)
        in p = x ** 2 + (y + z) ** 2
Proof
  rw_tac bool_ss[] >>
  drule_then strip_assume_tac two_sq_while_hoare >>
  rfs[whileTheory.HOARE_SPEC_DEF] >>
  qabbrev_tac `u = (1,1,p DIV 4)` >>
  `fixes zagier (mills p) = {u}` by rw[zagier_fixes_prime, Abbr`u`] >>
  last_x_assum (qspecl_then [`u`] strip_assume_tac) >>
  `(x,y,z) IN fixes flip (mills p)` by rfs[SPECIFICATION] >>
  `(x,y,z) IN (mills p) /\ (y = z)` by fs[fixes_element, flip_fix] >>
  metis_tac[mills_element, windmill_by_squares, DECIDE``y + y = 2 * y``]
QED

(* A beautiful theorem! *)

(* Define the algorithm. *)
Definition two_squares_def:
    two_squares n = let (x,y,z) = two_sq n in (x, y + z)
End

(*
> EVAL ``two_squares 5``; = (1,2)
> EVAL ``two_squares 13``; = (3,2)
> EVAL ``two_squares 17``; = (1,4)
> EVAL ``two_squares 29``; = (5,2)
> EVAL ``two_squares 97``;  = (9,4)
> EVAL ``MAP two_squares [5; 13; 17; 29; 37; 41; 53; 61; 73; 89; 97]``;
= [(1,2); (3,2); (1,4); (5,2); (1,6); (5,4); (7,2); (5,6); (3,8); (5,8); (9,4)]: thm
*)

(* Theorem: prime p /\ (p MOD 4 = 1) ==>
            let (u,v) = two_squares p in (p = u ** 2 + v ** 2) *)
(* Proof: by two_squares_def, two_sq_def, two_sq_while_thm. *)
Theorem two_squares_thm:
  !p. prime p /\ (p MOD 4 = 1) ==>
          let (u,v) = two_squares p in (p = u ** 2 + v ** 2)
Proof
  rw[two_squares_def, two_sq_def] >>
  drule_then strip_assume_tac two_sq_while_thm >>
  qabbrev_tac `loop = WHILE ($~ o found) (zagier o flip) (1,1,p DIV 4)` >>
  `?x y z. loop = (x,y,z)` by fs[triple_parts] >>
  fs[]
QED

(* ------------------------------------------------------------------------- *)
(* Fermat's Two Squares Theorem by Group Action.                             *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Relation to Fixes and Pairs.                                              *)
(* ------------------------------------------------------------------------- *)

(* Theorem: f involute X ==> fixed_points (FUNPOW f) Z2 X = fixes f X *)
(* Proof:
   By fixed_points_def, fixes_def, EXTENSION, this is to show:
   (1) !a. a < 2 ==> (FUNPOW f a x = x) ==> f x = x
       Note f x = FUNPOW f 1 x = x          by FUNPOW_1, 1 < 2
   (2) f x = x ==> !a. a < 2 ==> FUNPOW f a x = x
       When a = 0, FUNPOW f 0 x = x         by FUNPOW_0
       When a = 1, FUNPOW f 1 x = f x = x   by FUNPOW_1, f x = x
*)
Theorem involute_fixed_points_eq_fixes:
  !f X. f involute X ==> fixed_points (FUNPOW f) Z2 X = fixes f X
Proof
  rw[fixed_points_def, fixes_def, EXTENSION, EQ_IMP_THM] >-
  metis_tac[FUNPOW_1, DECIDE``1 < 2``] >>
  (`a = 0 \/ a = 1` by decide_tac >> simp[])
QED

(* Theorem: f involute X ==> BIGUNION (multi_orbits (FUNPOW f) Z2 X) = pairs f X *)
(* Proof:
   By multi_orbits_def, pairs_def, BIGUNION, EXTENSION, this is to show:
   (1) x IN e /\ e IN orbits (FUNPOW f) Z2 X /\ ~SING e ==> x IN X /\ f x <> x
       Note x IN X                           by involute_orbits_element_element
        and e = orbit (FUNPOW f) Z2 x        by involute_orbits_element_is_orbit
       By contradiction, suppose f x = x,
       Then e = {x}                          by involute_orbit_fixed
       with contradicts ~SING e              by SING
   (2) x IN X /\ f x <> x ==> ?e. x IN e /\ e IN orbits (FUNPOW f) Z2 X /\ ~SING e
       Let e = {x; f x}.
       Then x IN e, and ~SING e              by f x <> x
       The goal is to show: {x; f x} IN orbits (FUNPOW f) Z2 X

       Note {x; f x}
          = orbit (FUNPOW f) Z2 x            by involute_orbit_not_fixed
       which is IN orbits (FUNPOW f) Z2 X    by funpow_orbit_in_orbits
*)
Theorem involute_multi_orbits_union_eq_pairs:
  !f X. f involute X ==> BIGUNION (multi_orbits (FUNPOW f) Z2 X) = pairs f X
Proof
  rw[multi_orbits_def, pairs_def, EXTENSION, EQ_IMP_THM] >-
  metis_tac[involute_orbits_element_element] >-
 (`x IN X` by metis_tac[involute_orbits_element_element] >>
  `s = orbit (FUNPOW f) Z2 x` by metis_tac[involute_orbits_element_is_orbit] >>
  metis_tac[involute_orbit_fixed, SING]) >>
  qexists_tac `{x; f x}` >>
  simp[] >>
  `{x; f x} = orbit (FUNPOW f) Z2 x` by metis_tac[involute_orbit_not_fixed] >>
  rw[funpow_orbit_in_orbits]
QED

(* ------------------------------------------------------------------------- *)
(* Fermat's Two-Squares Theorem (Existence)                                  *)
(* ------------------------------------------------------------------------- *)

(* With zagier and flip both involutions on (mills p),
    and zagier with a unique fixed point,
   this implies flip has at least a fixed point,
   giving the existence of Fermat's two squares.
*)

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            fixed_points (FUNPOW zagier) Z2 (mills p) = {(1,1,p DIV 4)} *)
(* Proof:
   Let X = mills p, a = fixed_points (FUNPOW zagier) Z2 X.
   The goal is: a = {(1,1,p DIV 4)}.

   Note zagier involute X           by zagier_involute_mills_prime

   By EXTENSION, this is to show:
   (1) t IN X ==> t = (1,1,p DIV 4)
       Note t IN X                  by involute_fixed_points_element_element
        and zagier t = t            by involute_fixed_points
        Now ~square p               by prime_non_square
         so ?x y z. t = (x, y, z)   by triple_parts
        and x <> 0                  by mills_triple_nonzero, ~square p, p MOD 4 = 1
        ==> x = y                   by zagier_fix
       Note p = windmill (x, y, z)      by mills_element
        ==> (x,y,z) = (1,1,p DIV 4) by windmill_trivial_prime

   (2) (1,1,p DIV 4) IN a
       Note (1,1,p DIV 4) IN X      by mills_element_trivial
        and zagier (1,1,p DIV 4)
          = zagier (1,1,p DIV 4)    by zagier_1_1_z
         so (1,1,p DIV 4) IN a      by involute_fixed_points_iff
*)
Theorem zagier_fixed_points_sing:
  !p. prime p /\ p MOD 4 = 1 ==>
      fixed_points (FUNPOW zagier) Z2 (mills p) = {(1,1,p DIV 4)}
Proof
  rpt strip_tac >>
  qabbrev_tac `X = mills p` >>
  qabbrev_tac `a = fixed_points (FUNPOW zagier) Z2 X` >>
  `zagier involute X` by  metis_tac[zagier_involute_mills_prime] >>
  rw[EXTENSION, EQ_IMP_THM] >| [
    `x IN X` by metis_tac[involute_fixed_points_element_element] >>
    `zagier x = x` by metis_tac[involute_fixed_points] >>
    `?x1 y z. x = (x1, y, z)` by rw[triple_parts] >>
    `x1 <> 0` by metis_tac[mills_triple_nonzero, prime_non_square, ONE_NOT_ZERO] >>
    `x1 = y` by fs[zagier_fix] >>
    metis_tac[windmill_trivial_prime, mills_element],
    `(1,1,p DIV 4) IN X` by rw[mills_element_trivial, Abbr`X`] >>
    metis_tac[zagier_1_1_z, involute_fixed_points_iff]
  ]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==> ?(u,v). p = u ** 2 + v ** 2 *)
(* Proof:
   Let X = mills p, the solutions (x,y,z) of p = x ** 2 + 4 * y * z.
   Note ~square p                      by prime_non_square
     so FINITE X                       by mills_non_square_finite
    Now !x y z. (x,y,z) IN X ==>
        x <> 0 /\ y <> 0 /\ z <> 0     by mills_triple_nonzero
    and zagier involute X              by zagier_involute_mills_prime
    and flip involute X                by flip_involute_mills

   Let a = fixed_points (FUNPOW zagier) Z2 X,
       b = fixed_points (FUNPOW flip) Z2 X.
   Then ODD (CARD a) <=> ODD (CARD b)  by involute_two_fixed_points_both_odd

   The punch line:
   Let k = p DIV 4.
   Then a = {(1,1,k)}                  by zagier_fixed_points_sing
   Thus CARD a = 1                     by CARD_SING
     so ODD (CARD b)                   by ODD_1, above
    ==> CARD b <> 0                    by EVEN_0
     or b <> EMPTY                     by CARD_EMPTY
   thus ?x y z. (x,y,z) IN b           by MEMBER_NOT_EMPTY, triple_parts
     so flip (x, y, z) = (x, y, z)     by involute_fixed_points
    ==> y = z                          by flip_fix
   Note (x,y,z) IN X                   by fixed_points_element
   Put (u,v) = (x,2 * y).
   Then p = u ** 2 + v ** 2            by mills_element, windmill_by_squares
*)
Theorem fermat_two_squares_exists_alt:
  !p. prime p /\ p MOD 4 = 1 ==> ?(u,v). p = u ** 2 + v ** 2
Proof
  rw[ELIM_UNCURRY] >>
  qabbrev_tac `X = mills p` >>
  `~square p` by metis_tac[prime_non_square] >>
  `FINITE X` by fs[mills_non_square_finite, Abbr`X`] >>
  `zagier involute X` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute X` by metis_tac[flip_involute_mills] >>
  qabbrev_tac `a = fixed_points (FUNPOW zagier) Z2 X` >>
  qabbrev_tac `b = fixed_points (FUNPOW flip) Z2 X` >>
  `ODD (CARD a) <=> ODD (CARD b)` by rw[involute_two_fixed_points_both_odd, Abbr`a`, Abbr`b`] >>
  qabbrev_tac `k = p DIV 4` >>
  `a = {(1,1,k)}` by rw[zagier_fixed_points_sing, Abbr`a`, Abbr`X`, Abbr`k`] >>
  `CARD a = 1` by rw[] >>
  `CARD b <> 0` by metis_tac[ODD_1, EVEN_0, ODD_EVEN] >>
  `b <> EMPTY` by metis_tac[CARD_EMPTY] >>
  `?x y z. (x,y,z) IN b` by metis_tac[MEMBER_NOT_EMPTY, triple_parts] >>
  `flip (x, y, z) = (x, y, z)` by metis_tac[involute_fixed_points] >>
  `y = z` by fs[flip_fix] >>
  `(x,y,z) IN X` by fs[fixed_points_element, Abbr`b`] >>
  qexists_tac `(x, 2 * y)` >>
  simp[] >>
  metis_tac[mills_element, windmill_by_squares]
QED

(* Very good! *)

(* ------------------------------------------------------------------------- *)
(* References                                                                *)
(* ------------------------------------------------------------------------- *)

(*

Biscuits of Number Theory (Arthur T. BenjamIN snd Ezra Brown editors)
ISBN: 9780883853405
Part IV: Sum of Squares and Polygonal Numbers
p.143
A One-Sentence Proof that Every Prime p = 1 (mod 4) is a Sum of Two Squares
Don Zagier


An Algorithm to List All the Fixed-Point Free Involutions on a Finite Set
Cyril Prissette (20 Jun 2010)
https://hal.archives-ouvertes.fr/hal-00493605/document

The One-Sentence Proof in Multiple Sentences
Marcel Moosbrugger (Feb 3, 2020)
https://medium.com/cantors-paradise/the-one-sentence-proof-in-multiple-sentences-ab2657efc576

*)

(* ------------------------------------------------------------------------- *)
(* Application of Diophantus identity                                        *)
(* ------------------------------------------------------------------------- *)

(* The Diophantus identity, or Brahmagupta–Fibonacci identity, is:

four_squares_identity;
|- !a b c d. b * d <= a * c ==>
             (a ** 2 + b ** 2) * (c ** 2 + d ** 2) = (a * d + b * c) ** 2 + (a * c - b * d) ** 2

four_squares_identity_alt;
|- !a b c d. a * c <= b * d ==>
             (a ** 2 + b ** 2) * (c ** 2 + d ** 2) = (a * d + b * c) ** 2 + (b * d - a * c) ** 2
*)

(* Idea: these show that complex numbers have multiplicative norm. *)

(* Treat pair (a,b) as complex number a + bi *)

(* norm (a + bi) = (a + bi)(a - bi) = a^2 + b^2 *)
Definition norm_def:
   norm (a,b) = a ** 2 + b ** 2
End

(* (a + bi)(c + di) = (ac - bd) + (ad + bc)i *)
Definition multiply_def:
   multiply (a,b) (c,d) = (a * c - b * d, a * d + b * c)
End

(* overload complex multiplication as infix o *)
Overload "o" = ``multiply``;

(* Theorem: norm (a,b) = norm (b,a) *)
(* Proof:
      norm (a,b)
   = a ** 2 + b ** 2           by norm_def
   = b ** 2 + a ** 2           by ADD_COMM
   = norm (b,a)                by norm_def
*)
Theorem norm_sym:
  !a b. norm (a,b) = norm (b,a)
Proof
  simp[norm_def]
QED

(* Theorem: norm (a,b) = 0 <=> a = 0 /\ b = 0 *)
(* Proof:
       norm (a,b) = 0
   <=> a ** 2 + b ** 2 = 0         by norm_def
   <=> a ** 2 = 0 /\ b ** 2 = 0    by ADD_EQ_0
   <=>      a = 0 /\ b = 0         by EXP_2_EQ_0
*)
Theorem norm_eq_0:
  !a b. norm (a,b) = 0 <=> a = 0 /\ b = 0
Proof
  simp[norm_def]
QED

(* Theorem: norm (a,b) = 1 <=> (a = 1 /\ b = 0 \/ a = 0 /\ b = 1) *)
(* Proof:
       norm (a,b) = 1
   <=> a ** 2 + b ** 2 = 1         by norm_def
   <=> a ** 2 = 1 /\ b ** 2 = 0 or
       a ** 2 = 0 /\ b ** 2 = 1    by ADD_EQ_1
   <=> a = 1 /\ b = 0 or
       a = 0 /\ b = 1              by EXP_2_EQ_0, EXP_2_EQ_1
*)
Theorem norm_eq_1:
  !a b. norm (a,b) = 1 <=> (a = 1 /\ b = 0 \/ a = 0 /\ b = 1)
Proof
  simp[norm_def, ADD_EQ_1, EXP_2_EQ_0, EXP_2_EQ_1]
QED

(* Theorem: 0 < a /\ 0 < b ==> 1 < norm (a,b) *)
(* Proof:
       0 < a /\ 0 < b
   <=> 0 < a ** 2 /\ 0 < b ** 2    by EXP_EXP_LT_MONO
   <=> 1 <= a ** 2 /\ 1 <= b ** 2  by inequality
   ==> 2 <= a ** 2 + b ** 2        by LE_MONO_ADD2
   Thus 1 < norm (a,b)             by norm_def
*)
Theorem norm_gt_1:
  !a b. 0 < a /\ 0 < b ==> 1 < norm (a,b)
Proof
  rpt strip_tac >>
  `1 <= a ** 2 /\ 1 <= b ** 2` by simp[EXP_EXP_LT_MONO] >>
  simp[norm_def]
QED

(* Theorem: norm (a,b) * norm (c,d)
          = norm (a * d + b * c, if b * d <= a * c then a * c - b * d else b * d - a * c) *)
(* Proof:
   If b * d <= a * c,
        norm (a,b) * norm (c, d)
      = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)          by norm_def
      = (a * d + b * c) ** 2 + (a * c - b * d) ** 2    by four_squares_identity
      = norm (a * d + b * c, a * c - b * d)            by norm_def

   Otherwise, a * c < b * d,
      or a * c <= b * d.
        norm (a,b) * norm (c, d)
      = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)          by norm_def
      = (a * d + b * c) ** 2 + (b * d - a * c) ** 2    by four_squares_identity_alt
      = norm (a * d + b * c, b * d - a * c)            by norm_def
*)
Theorem norm_product:
  !a b c d. norm (a,b) * norm (c,d) =
             norm (a * d + b * c, if b * d <= a * c then a * c - b * d else b * d - a * c)
Proof
  rw[norm_def, four_squares_identity, four_squares_identity_alt]
QED

(*
multiply_def;
val it = |- !a b c d. (a,b) o (c,d) = (a * c - b * d,a * d + b * c): thm
*)

(* Note: for type num, this defintion only works when b * d <= a * c *)
(* Note: also for type num, complex (a,b) has no conjugate (a, -b)   *)

(* Theorem: b * d <= a * c ==> norm ((a,b) o (c,d)) = norm (a,b) * norm (c,d) *)
(* Proof:
     norm ((a,b) o (c,d))
   = norm (a * c - b * d, a * d + b * c)           by multiply_def
   = (a * c - b * d) ** 2 + (a * d + b * c) ** 2   by norm_def
   = (a ** 2 + b ** 2) * (c ** 2 + d ** 2          by four_squares_identity
   = norm (a,b) * norm (c,d)                       by norm_def
*)
Theorem norm_multiplicative:
  !a b c d. b * d <= a * c ==> norm ((a,b) o (c,d)) = norm (a,b) * norm (c,d)
Proof
  rw[multiply_def, norm_def] >>
  simp[four_squares_identity]
QED

(* This is not ideal, but this is the best under multiply_def with num type. *)

(* ------------------------------------------------------------------------- *)
(* Alternative Proof of Uniqueness                                           *)
(* ------------------------------------------------------------------------- *)

(* Idea: if a number n has two forms of sum of squares, then n is composite.

Example: a product of sum of squares gives two sum of squares.

Note 65 = 5x13 = (1² + 2²)(3² + 2²), so a = 1, b = 2, c = 3, d = 2.
by four_squares_identity,
     65 = (1x3 - 2x2)² + (1x2 + 2x3)² = (-1)² + 8² = 1² + 8² = x² + y²
by four_squares_identity_alt,
     65 = (1x3 + 2x2)² + (1x2 - 2x3)² = 7² + (-4)² = 7² + 4² = u² + v²
So 65 = x² + y² = u² + v², where
    x = ac - bd, y = ad + bc, u = ac + bd, v = ad - bc.

Given (a,b,c,d), we can compute (x,y,u,v). But given (x,y,u,v), how to compute (a,b,c,d)?

Working: two sum of squares as a product of sum of squares.

Given (x,y,u,v), x + u = 2ac, y + v = 2ad, u - x = 2bd, y - v = 2bc
(x + u)/(y - v) = a/b = (y + v)/(u - x), this gives a, b in a ratio.
(x + u)/(y + v) = c/d = (y - v)/(u - x), this gives c, d in a ratio.

For 65 = 1² + 8² = 7² + 4², x = 1, y = 8, u = 7, v = 4.
Thus a/b = (1 + 7)/(8 - 4) = 8/4 = 2/1, simplest is a = 2, b = 1.
Thus c/d = (1 + 7)/(8 + 4) = 8/12 = 2/3, simplest is c = 2, d = 3.
Indeed 65 = (2² + 1²)(2² + 3²) = 5x13.

Given (x,y,u,v), ensure y is largest, v is smallest (all unequal) by swap (x,y), swap (u,v), and swap pairs.
Compute e = x + u, f = y - v, g = y + v, all positive, let h = gcd(e,f), k = gcd(e,g).
Take a = e/h, b = f/h, c = e/k, d = g/k.

Example: 50 = 1² + 7² = 5² + 5², x = 1, y = 7, u = 5, v = 5.
e = 1 + 5 = 6, f = 7 - 5 = 2, g = 7 + 5 = 12, h = gcd(6,2) = 2, k = gcd(6,12) = 6
Take a = 6/2 = 3, b = 2/2 = 1, c = 6/6 = 1, d = 12/6 = 2.
50 = (3² + 1²)(1² + 2²) = 10x5.

Theorem: If n = norm (x,y) = norm (u,v) with x <> u, then n = norm (a,b) * norm (c,d) for some (a,b), (c,d).

This theorem as stated is not true when there is more than two norms. Counter-examples:
5x5x13 = 325 = 1² + 18² = 6² + 17² = 10² + 15²
x,y,v,v:
6,17,10,15 gives a,b,c,d: 8,1,1,2,  with p = 65, q = 5, p * q = 325 ok
1,18,10,15 gives a,b,c,d: 11,3,1,3, with p = 130, q = 10, p * q = 1300 !== 325, not ok
1,18,6,17  gives a,b,c,d: 7,1,1,5,  with p = 50, q = 26, p * q = 1300 !== 325, not ok.

No! The theorem seems still true as long as (x,y) and (u,v) have matching parity:
5x5x13 = 325 = 1² + 18² = 17² + 6² = 15² + 10²
x,y,v,v:
1,18,17,6  gives a,b,c,d: 3,2,3,4,  with p = 13, q = 25, p * q = 325 ok.
17,6,15,10 gives a,b,c,d: 8,1,1,2,  with p = 65, q = 5, p * q = 325 ok.
1,18,15,10 gives a,b,c,d: 2,1,4,7,  with p = 5, q = 65, p * q = 325 ok.


Not-a-proof:
     a = e/h, b = f/h ==> a/b = e/f = (x + u)/(y - v),
         so x + u = (2 * c) * a, y - v = (2 * c) * b         ???
     c = e/k, d = g/k ==> c/d = e/g = (x + u)/(y + v) = (y - v)/(u - x)    by norm (x,y) = norm (u,v)
         so y - v = (2 * b) * c, u - x = (2 * b) * d         ???
         or x + u = (2 * a) * c, y + v = (2 * a) * d         ???

     Now this can be satisfied by:
         x = ac - bd, y = ad + bc, u = ac + bd, v = ad - bc  ???
         so norm (a,b) * norm (c,d) = x ** 2 + y ** 2 = norm (x,y)       by four_squares_identity

Proof:
     norm (x,y) = norm (u,v)
 ==> x ** 2 + y ** 2 = u ** 2 + v ** 2
 ==> y ** 2 - v ** 2 = u ** 2 - x ** 2
 ==> (y - v) * (y + v) = (u - x) * (u + x)
     (u + x)/(y - v) = (y + v)/(u - x)
     (u + x)/(y + v) = (y - v)/(u - x)

   norm (a,b) * norm (c,d)
 = ((e/h) ** 2 + (f/h) ** 2)((e/k) ** 2 + (g/k) ** 2)
 = ((e/h) * (g/k) + (f/h) * (e/k)) ** 2 + ((e/h) * (e/k) - (f/h) * (g/k)) ** 2
 = (e * g + f * e) ** 2 / (h * k) ** 2 + (e * e - f * g) ** 2 / (h * k) ** 2
 = [((x + u) * (y + v) + (y - v) * (x + u)) ** 2 + ((x + u) * (x + u) - (y - v) * (y + v)) ** 2] / (h * k) ** 2
 = [(x + u) * 2 * y] ** 2 + ((x + u) ** 2 - (y ** 2 - v ** 2)) ** 2] / (h * k) ** 2
 =

    ac - bd
  = (e/h)(e/k) - (f/h)(g/k)
  = (e * e - f * g)/(hk)
  = ((x + u) ** 2 - (y - v)(y + v)) / (hk)                    (8 ** 2 - (4)(12))/16 = (64 - 48)/16 = 16/16 = 1
  = (x ** 2 + 2xu + u ** 2 - (y ** 2 - v ** 2)) / (hk)
  = (x ** 2 + 2xu + v ** 2 + (u ** 2 - y ** 2)) / (hk)
  = (x ** 2 + 2xu + v ** 2 + (x ** 2 - v ** 2)) / (hk)   by norm (x,y) = norm (u,v)
  = (2xx + 2xu) / (hk)
  = 2x(x + u)/(hk)  = x  unless 2(x+u) = hk = gcd(x + u, y - v) gcd (x + u, y + v)
  = 2xe/gcd(e,f)/gcd(e,g)

  For x = 1, y = 8, u = 7, v = 4
  e = x + u, f = y - v, g = y + v, all positive, let h = gcd(e,f), k = gcd(e,g).
  so e = 8, f = 4, g = 12, h = gcd(8,4) = 4, k = gcd(8,12) = 4
  2(x + u) = 2(8) = 16, hk = 16, equal!
  But  a = e/h = 2, b = f/h = 1, c = e/k = 2, d = g/k = 3
  ac - bd = 4 - 3 = 1 = x, equal!!
*)

(* Treat pair (a,b) as ratio a : b. *)

(* reduce (a : b) = a/g : b/g where g = gcd (a, b) *)
Definition reduce_def:
   reduce (a,b) = let g = gcd a b in (a DIV g, b DIV g)
End

(* A forward transform: (a,b) and (c,d) gives (x,y) and (u,v):
   forward (a,b) (c,d) = let x = a * c - b * d; y = a * d + b * c; u = a * c + b * d; v = a * d - b * c in ((x,y), (u,v))
   This only works if b * d <= a * c and b *  c <= a * d *)
Definition forward_def:
   forward (a,b) (c,d) =
       ((if b * d <= a * c then a * c - b * d else b * d - a * c,a * d + b * c),
        (a * c + b * d, if b *  c <= a * d then a * d - b * c else b * c - a * d))
End

(* A backward transform: (x,y) and (u,v) gives (a,b) and (c,d):
   backward (x,y) (u,v) = let e = x + u; f = y - v; g = y + v; (a,b) = reduce (e,f); (c,d) = reduce (e, g) in ((a,b), (c,d))  *)
Definition backward_def:
   backward (x,y) (u,v) = let e = x + u; f = y - v; g = y + v in (reduce (e,f), reduce (e, g))
End
(* This definition only works if v <= y *)
(* Cannot do: if u <= x then backward (y,x) (v,u) else ... as termination cannot be proved. *)

(* 65 = 5x13 = (1² + 2²)(3² + 2²), so a = 2, b = 1, c = 3, d = 2. b * d = 2, a * c = 6, b * c = 3, a * d = 4

EVAL ``forward (2,1) (3,2)``;
> val it = |- forward (2,1) (3,2) = ((4,7),8,1): thm

EVAL ``norm (2,1) * norm (3,2)``; = 65
EVAL ``norm (4,7)``; = 65
EVAL ``norm (8,1)``; = 65
EVAL ``norm (2,1) * norm (3,2) = norm (4,7) /\ norm (2,1) * norm (3,2) = norm (8,1)``; <=> T
*)

(* Idea: reduce is just a ratio: m = a/g and n = b/g ==> m/n = a/b, or a * n = b * m *)

(* Theorem: (m,n) = reduce (a,b) ==> a * n = b * m *)
(* Proof:
   Let g = gcd a b,
   Then m = a DIV g, n = b DIV g       by reduce_def
    and a = m * g, b = n * g           by pair_div_gcd_eqn
        a * n
      = (m * g) * n                    by a = m * g
      = (n * g) * m                    by arithmetic
      = b * m                          by b = n * g
*)
Theorem reduce_eqn:
  !a b m n. (m,n) = reduce (a,b) ==> a * n = b * m
Proof
  rw[reduce_def] >>
  qabbrev_tac `h = gcd a b` >>
  qabbrev_tac `x = a DIV h` >>
  qabbrev_tac `y = b DIV h` >>
  `a = x * h /\ b = y * h` by metis_tac[pair_div_gcd_eqn] >>
  rfs[]
QED

(* Theorem: ((x,y), (u,v)) = forward (a,b) (c,d) ==>
             norm (a,b) * norm (c, d) = norm (x,y) /\ norm (a,b) * norm (c,d) = norm (u,v) *)
(* Proof:
   By forward_def;
      (x,y) = (if b * d <= a * c then a * c - b * d else b * d - a * c, a * d + b * c)
      (u,v) = (a * c + b * d, if b * c <= a * d then a * d - b * c else b * c - a * d)
   If b * d <= a * c,
      norm (a,b) * norm (c,d)
    = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)      by norm_def
    = x ** 2 + y ** 2                            by four_squares_identity
    = norm (x,y)                                 by norm_def

      norm (a,b) * norm (c,d)
    = norm (a,b) * norm (d,c)                    by norm_sym
    = (a ** 2 + b ** 2) * (d ** 2 + c ** 2)      by norm_def
    = u ** 2 + v ** 2                            by four_squares_identity_alt
    = norm (u,v)                                 by norm_def
   Otherwise a * c < b * d,
      so a * c <= b * d
      norm (a,b) * norm (c,d)
    = norm (b,a) * norm (d,c)                    by norm_sym
    = (b ** 2 + a ** 2) * (d ** 2 + c ** 2)      by norm_def
    = y ** 2 + x ** 2                            by four_squares_identity
    = norm (x,y)                                 by norm_sym

      norm (a,b) * norm (c,d)
    = norm (a,b) * norm (d,c)                    by norm_sym
    = (a ** b + a ** 2) * (d ** 2 + c ** 2)      by norm_def
    = u ** 2 + v ** 2                            by four_squares_identity_alt
    = norm (u,v)                                 by norm_sym
   If b * c <= a * d,
      norm (a,b) * norm (c,d)
    = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)      by norm_def
    = u ** 2 + v ** 2                            by four_squares_identity_alt
    = norm (u,v)                                 by norm_def

      norm (a,b) * norm (c,d)
    = norm (a,b) * norm (d,c)                    by norm_sym
    = (a ** 2 + b ** 2) * (d ** 2 + c ** 2)      by norm_def
    = x ** 2 + y ** 2                            by four_squares_identity
    = norm (x,y)                                 by norm_def
   Otherwise a * d < b * c,
      so a * d <= b * c
      norm (a,b) * norm (c,d)
    = norm (b,a) * norm (d,c)                    by norm_sym
    = (b ** 2 + a ** 2) * (d ** 2 + c ** 2)      by norm_def
    = v ** 2 + u ** 2                            by four_squares_identity_alt
    = norm (u,v)                                 by norm_sym

      norm (a,b) * norm (c,d)
    = norm (a,b) * norm (d,c)                    by norm_sym
    = (a ** 2 + b ** 2) * (d ** 2 + c ** 2)      by norm_def
    = x ** 2 + y ** 2                            by four_squares_identity
    = norm (x,y)                                 by norm_sym

*)
Theorem forward_thm:
  !a b c d x y u v. ((x,y), (u,v)) = forward (a,b) (c,d) ==>
             norm (a,b) * norm (c, d) = norm (x,y) /\ norm (a,b) * norm (c,d) = norm (u,v)
Proof
  rw[forward_def, norm_def] >-
  simp[four_squares_identity] >-
  metis_tac[four_squares_identity, ADD_COMM] >-
  simp[four_squares_identity] >-
 (`a * d <= b * c` by decide_tac >>
  metis_tac[four_squares_identity, ADD_COMM]) >-
  simp[four_squares_identity_alt] >-
 (`a * c <= b * d` by decide_tac >>
  metis_tac[four_squares_identity, ADD_COMM]) >-
  simp[four_squares_identity_alt] >>
  `a * d <= b * c` by decide_tac >>
  metis_tac[four_squares_identity, ADD_COMM]
QED


(* Another formulation of forward_thm *)

(* Theorem: let j = forward (a,b) (c,d)
             in norm (a,b) * norm (c,d) = norm (FST j) /\
                norm (a,b) * norm (c,d) = norm (SND j) *)
(* Proof: by forward_thm *)
Theorem forward_thm_alt:
  !a b c d. let j = forward (a,b) (c,d)
             in norm (a,b) * norm (c,d) = norm (FST j) /\
                norm (a,b) * norm (c,d) = norm (SND j)
Proof
  metis_tac[forward_thm, pairTheory.PAIR]
QED

(* 65 = 1² + 8² = 7² + 4², x = 1, y = 8, u = 7, v = 4.

e = 1 + 7 = 8, f = 8 - 4 = 4, g = 8 + 4 = 12, h = gcd(8,4) = 4, k = gcd(8,12) = 4
Take (a,b) = (8/4,4/4) = (2,1), (c,d) = (8/4,12/4) = (2,3)
Then norm(2,1) = 5, norm (2,3) = 13, and 65 = 5x13.

EVAL ``backward (1,8) (7,4)``;
> val it = |- backward (1,8) (7,4) = ((2,1),2,3): thm

EVAL ``norm (2,1) * norm (2,3)``; = 65
EVAL ``norm (1,8)``; = 65
EVAL ``norm (7,4)``; = 65
EVAL ``norm (2,1) * norm (2,3) = norm (1,8) /\ norm (2,1) * norm (2,3) = norm (7,4)``; <=> T
*)

(* 50 = 1² + 7² = 5² + 5², x = 1, y = 7, u = 5, v = 5.

e = 1 + 5 = 6, f = 7 - 5 = 2, g = 7 + 5 = 12, h = gcd(6,2) = 2, k = gcd(6,12) = 6
Take a = 6/2 = 3, b = 2/2 = 1, c = 6/6 = 1, d = 12/6 = 2.
50 = (3² + 1²)(1² + 2²) = 10x5.

EVAL ``backward (1,7) (5,5)``;
> val it = |- backward (1,7) (5,5) = ((3,1),1,2): thm

EVAL ``norm (3,1) * norm (1,2)``; = 50
EVAL ``norm (1,7)``; = 50
EVAL ``norm (5,5)``; = 50
EVAL ``norm (3,1) * norm (1,2) = norm (1,7) /\ norm (3,1) * norm (1,2) = norm (5,5)``; <=> T
*)

(* Theorem: norm (x,y) = norm (u,v) ==> (x < u <=> v < y) /\ (x = u <=> v = y) *)
(* Proof:
       norm (x,y) = norm (u,v)
   <=> x ** 2 + y ** 2 = u ** 2 + v ** 2       by norm_def
   ==> x ** 2 < u ** 2 <=> v ** 2 < y ** 2     by pair_eq_imp
   ==>           x < u <=> v < y               by EXP_EXP_LT_MONO, 0 < 2

   If x = u, then y ** 2 = v ** 2              by EQ_ADD_RCANCEL
               so      y = v                   by EXP_EXP_INJECTIVE
   If y = v, then x ** 2 = u ** 2              by EQ_ADD_LCANCEL,
               so      x = u                   by EXP_EXP_INJECTIVE
*)
Theorem norm_eq_imp:
  !x y u v. norm (x,y) = norm (u,v) ==> (x < u <=> v < y) /\ (x = u <=> v = y)
Proof
  rw[norm_def] >| [
    `x ** 2 < u ** 2 <=> v ** 2 < y ** 2`  by decide_tac >>
    fs[EXP_EXP_LT_MONO],
    metis_tac[EQ_ADD_LCANCEL, EQ_ADD_RCANCEL, EXP_EXP_INJECTIVE, DECIDE``2 <> 0``]
  ]
QED

(* Theorem: norm (x,y) = norm (u,v) /\ v <= y ==>
             ((y - v) * (y + v) = (u - x) * (u + x)) /\
             (EVEN (y + v) <=> EVEN (u - x)) *)
(* Proof:
      (y - v) * (y + v)
    = y ** 2 - v ** 2              by difference_of_squares
    = u ** 2 - x ** 2              by norm equal, EXP_EXP_LT_MONO
    = (u - x) * (u + x)            by difference_of_squares

    Note x <= u                          by norm_eq_imp, v <= y
      so (u + x) + (u - x) = 2 * u       by arithmetic
     now EVEN (2 * u)                    by EVEN_EXISTS
     ==> EVEN (u + x) <=> EVEN (u - x)   by EVEN_ADD
    also (y + v) + (y - v) = 2 * y       by arithmetic
     now EVEN (2 * y)                    by EVEN_EXISTS
     ==> EVEN (y + v) <=> EVEN (y - v)   by EVEN_ADD

         EVEN (y + v)
     <=> EVEN ((y - v) * (y + v))  by EVEN_MULT
     <=> EVEN ((u - x) * (u + x))  by above
     <=> EVEN (u - x)              by EVEN_MULT, ODD_EVEN
*)
Theorem norm_eq_property:
  !x y u v. norm (x,y) = norm (u,v) /\ v <= y ==>
             ((y - v) * (y + v) = (u - x) * (u + x)) /\
             (EVEN (y + v) <=> EVEN (u - x))
Proof
  ntac 5 strip_tac >>
  `x <= u` by metis_tac[norm_eq_imp, LESS_OR_EQ] >>
  fs[norm_def] >>
  `(y - v) * (y + v) = y ** 2 - v ** 2` by simp[difference_of_squares] >>
  `_ = u ** 2 - x ** 2` by simp[EXP_EXP_LT_MONO] >>
  `_ = (u - x) * (u + x)` by simp[difference_of_squares] >>
  `(u + x) + (u - x) = 2 * u` by decide_tac >>
  `(y + v) + (y - v) = 2 * y` by decide_tac >>
  `EVEN (2 * u) /\ EVEN (2 * y)` by simp[EVEN_EXISTS] >>
  `EVEN (u + x) <=> EVEN (u - x)` by simp[GSYM EVEN_ADD] >>
  `EVEN (y + v) <=> EVEN (y - v)` by simp[GSYM EVEN_ADD] >>
  metis_tac[EVEN_MULT, ADD_COMM, MULT_COMM]
QED

(*

Idea for backward.

Given norm(x,y) = norm(u,v), odd x, even y, odd u, even v, v < y.
Compute: e = x + u, f = y - v, g = y + v, h = gcd(e,f), k = gcd(e,g)
Let a = e/h, b = f/h, c = e/k, d = g/k,
To show: norm(a,b) norm(c,d) = norm(x,y).

Known: norm(a,b) norm(c,d) = norm(a * d + b * c, a * c - b * d).

   a * d + b * c
 = (e/h)(g/k) + (f/h)(e/k)
 = e (g + f)/(hk)
 = (x + u)(2y)/(hk)
 = y(2(x+u)/hk)

   a * c - b * d
 = (e/h)(e/k) - (f/h)(g/k)
 = (e² - f * g)/(hk)
 = ((x + u)² - (y² - v²))/hk
 = ((x + u)² - (u² - x²))/hk, by x² + y² = u² + v²
 = (u + x)((u + x) - (u - x))/hk
 = (x + u)(2x)/(hk) = x(2(x+u)/hk)

 Therefore, all good if 2(x+u) = hk = gcd(x+u,y-v) gcd(x+u,y+v).
 Test ok!

EVAL ``let (x,y,u,v) = (1,18,17,6) in gcd (x + u) (y - v) * gcd (x + u) (y + v) = 2 * (x + u)``; = T

Everything is good if  h * k = 2 * (x + u),  h = gcd (x + u) (y - v), k = gcd (x + u) (y + v).

    x² + y² = u² + v², so y² - v² = u² - x², v < y <=> x < u, odd x, even y, odd u, even v.

    h * k
  = gcd (y - v) (x + u) * gcd (y + v) (x + u)      by GCD_SYM
  = gcd ((y - v) * (y + v)) (x + u)                if coprime (y - v) (y + v), but both are even, so false.

    h * k
  = ((y - v) o (x + u)) * ((y + v) o (x + u))      by GCD_SYM
  = 4 GCD of products, messy!                      by GCD_MULT
  = ((y - v) * (y + v)) o ((y - v) * (x + u)) o ((x + u) * (y + v) o ((x + u) * (x + u)))
  = (y²  - v² ) o ((y - v) * (x + u)) o ((x + u) * (y + v) o ((x + u)²)
  = (u²  - x² ) o  ((x + u)²) o (((y - v) o (y + v)) * (x + u))
  = ((u - x) * (u + x)) o ((u + x) * (u + x)) o (((y - v) o (y + v)) * (x + u))
  = (((u - x) o (u + x)) * (u + x)) o (((y - v) o (y + v)) * (x + u))
  = (((u - x) o (u + x)) o ((y - v) o (y + v))) * (x + u)
  = ((u - x) o (u + x) o (y - v) o (y + v)) * (x + u)

  somehow, (u - x) o (u + x) o (y - v) o (y + v) = 2 ???

    (u - x) o (u + x)
  = ((u + x) - (u - x)) o (u + x)      by GCD_SUB_L, x < u
  = (2 * x) o (u + x)

    (y - v) o (y + v)
   = ((y + v) + (y - v)) o (y + v)     by GCD_ADD_L
   = (2 * y) o (y + v)

     (u - x) o (u + x) o (y - v) o (y + v)
   = (2 * x) o (u + x) o (2 * y) o (y + v)
   = 2 * (x o (u + x) o y o (y + v))
   = 2 * (x o u o y o v)               by GCD_ADD
   = 2    because both (u + x) and (y + v) are even?, and  x o y o u o v = 1, all coprime?

    possible to show:  h * k = 2 * (x + u) * (x o u o y o v)
*)


(* Theorem: norm (x,y) = norm (u,v) /\ v <= y /\ coprime x y /\ EVEN (y + v) ==>
             gcd (x + u) (y - v) * gcd (x + u) (y + v) = 2 * (x + u) *)
(* Proof:

   Note x <= u                           by norm_eq_imp, v <= y
   also (y - v) * (y + v) = (u - x) * (u + x)
    and EVEN (y + v) <=> EVEN (u - x)    by norm_eq_property

   Thus EVEN (u - x)                     by EVEN_SUB, ODD_EVEN
    ==> ?h. u - x = 2 * h                by EVEN_EXISTS
    and EVEN (y + v)                     by EVEN_ADD
    ==> ?k. y + v = 2 * k                by EVEN_EXISTS

   Also gcd (u + x) (u - x)
      = gcd ((u + x) - (u - x)) (u - x)  by GCD_SUB_L
      = gcd (2 * x) (u - x)              by SUB_SUB

   Also gcd (y + v) (y - v)
      = gcd (y + v) ((y + v) + (y - v))  by GCD_ADD_R
      = gcd (y + v) (2 * y)              by SUB_LEFT_ADD

   Now compute:
        gcd (x + u) (y - v) * gcd (x + u) (y + v)
      = gcd ((x + u) * (x + u)) (gcd ((x + u) * (y + v)) (gcd ((y - v) * (x + u)) ((y - v) * (y + v))))  by GCD_MULT_COMM
      = gcd ((x + u) * (x + u)) (gcd ((x + u) * (y + v)) (gcd ((y - v) * (x + u)) ((u - x) * (u + x))))  by above
      = gcd ((x + u) * (x + u)) (gcd ((x + u) * (y + v)) (gcd ((x + u) * (y - v)) ((x + u) * (u - x))))  by arithmetic
      = (x + u) * (gcd (x + u) (gcd (y + v) (gcd (y - v) (u - x))))                                      by GCD_COMMON_FACTOR
      = (x + u) * (gcd (x + u) (gcd (y + v) (gcd (u - x) (y - v))))   by GCD_SYM
      = (x + u) * (gcd (x + u) (gcd (u - x) (gcd (y + v) (y - v))))   by GCD_SWAP
      = (x + u) * (gcd (gcd (x + u) (u - x)) (gcd (y + v) (y - v)))   by GCD_ASSOC
      = (x + u) * (gcd (gcd (2 * x) (2 * h)) (gcd (2 * k) (2 * y)))   by above
      = 2 * (x + u) * (gcd (gcd x h) (gcd k y))                       by GCD_COMMON_FACTOR
      = 2 * (x + u) * (gcd x (gcd h (gcd k y)))                       by GCD_ASSOC
      = 2 * (x + u) * (gcd x (gcd y (gcd h k)))                       by GCD_CYCLE
      = 2 * (x + u) * (gcd (gcd x y) (gcd h k))                       by GCD_ASSOC
      = 2 * (x + u) * (gcd 1 (gcd h k))                               by gcd x y = 1
      = 2 * (x + u)                                                   by GCD_1
*)
Theorem norm_eq_coprime_gcd_product:
  !x y u v. norm (x,y) = norm (u,v) /\ v <= y /\ coprime x y /\ EVEN (y + v) ==>
             gcd (x + u) (y - v) * gcd (x + u) (y + v) = 2 * (x + u)
Proof
  rpt strip_tac >>
  `x <= u` by metis_tac[norm_eq_imp, LESS_OR_EQ] >>
  `((y - v) * (y + v) = (u - x) * (u + x)) /\ EVEN (u - x)` by metis_tac[norm_eq_property] >>
  `?h. u - x = 2 * h /\ ?k. y + v = 2 * k` by simp[GSYM EVEN_EXISTS] >>
  `gcd (u + x) (u - x) = gcd ((u + x) - (u - x)) (u - x)` by simp[GCD_SUB_L] >>
  `_ = gcd (2 * x) (u - x)` by simp[SUB_SUB] >>
  `gcd (y + v) (y - v) = gcd (y + v) ((y + v) + (y - v))` by simp[GCD_ADD_R] >>
  `_ = gcd (y + v) (2 * y)` by metis_tac[DECIDE ``v <= y ==> (y + v) + (y - v) = 2 * y``] >>
  `gcd (x + u) (y - v) * gcd (x + u) (y + v) = gcd ((x + u) * (x + u)) (gcd ((x + u) * (y + v)) (gcd ((y - v) * (x + u)) ((y - v) * (y + v))))` by simp[GCD_MULT_COMM] >>
  `_ = gcd ((x + u) * (x + u)) (gcd ((x + u) * (y + v)) (gcd ((x + u) * (y - v)) ((x + u) * (u - x))))` by simp[] >>
  `_ = (x + u) * (gcd (x + u) (gcd (y + v) (gcd (y - v) (u - x))))` by simp[GSYM GCD_COMMON_FACTOR] >>
  `_ = (x + u) * (gcd (x + u) (gcd (u - x) (gcd (y + v) (y - v))))` by simp[GCD_SWAP, GCD_SYM] >>
  `_ = (x + u) * (gcd (gcd (x + u) (u - x)) (gcd (y + v) (y - v)))` by simp[GCD_ASSOC] >>
  `_ = (x + u) * (gcd (gcd (2 * x) (2 * h)) (gcd (2 * k) (2 * y)))` by rfs[] >>
  `_ = 2 * (x + u) * (gcd (gcd x h) (gcd k y))` by simp[GCD_COMMON_FACTOR] >>
  `_ = 2 * (x + u) * (gcd x (gcd h (gcd k y)))` by simp[GCD_ASSOC] >>
  `_ = 2 * (x + u) * (gcd x (gcd y (gcd h k)))` by simp[GCD_CYCLE] >>
  `_ = 2 * (x + u) * (gcd (gcd x y) (gcd h k))` by simp[GCD_ASSOC] >>
  rfs[GCD_1]
QED

(* This is surprising! *)

(* Idea: given (x,y) and (u,v). Ensure v < y.
         If n = norm (x,y) = norm (u,v), then n = p * q for some p, q *)


(* Theorem: norm (x,y) = norm (u,v) /\ v <= y /\ coprime x y /\ ODD x /\ ODD u /\
            ((a,b),(c,d)) = backward (x,y) (u,v) ==>
            norm (a,b) * norm (c,d) = norm (x,y) /\
            (v < y ==> 1 < norm (a,b) /\ 1 < norm (c,d)) *)
(* Proof:
   Let h = gcd (u + x) (y - v),
       k = gcd (u + x) (v + y),
       a = (x + u) DIV h,
       b = (y - v) DIV h,
       c = (x + u) DIV k,
       d = (y + v) DIV k.

   By backward_def, reduce_def, this is to show:
      norm (a,b) * norm (c,d) = norm (x,y)

   Note x <= u                     by norm_eq_imp
    and EVEN (x - u)               by EVEN_SUB, ODD_EVEN
    ==>`EVEN (y + v)               by norm_eq_property
    ==> h * k = 2 * (x + u)        by norm_eq_coprime_gcd_product

   Step 1: show x <> 0 and u <> 0 and h <> 0 and k <> 0
      Note x <> 0 and u <> 0       by ODD
      If h = 0 or k = 0,
         Then u + x = 0            by GCD_EQ_0
           so u = 0, x = 0         by ADD_EQ_0
         this contradicts ODD x    by ODD

   Note norm (a,b) * norm (c,d)
      = norm (a * d + b * c, if b * d <= a * c then a * c - b * d else b * d - a * c)
                                               by norm_product
    and h divides (x + u),
        h divides (y - v),
        k divides (x + u),
        k divides (y + v).         by GCD_IS_GREATEST_COMMON_DIVISOR

   Step 2: compute h * k times cross terms
          h * k * (a * d)
        = (a * h) * (d * k)        by arithmetic
        = (x + u) * (y + v)        by DIV_MULT_EQ
          h * k * (b * c)
        = (b * h) * (c * k)        by arithmetic
        = (y - v) * (x + u)        by DIV_MULT_EQ
          h * k * (b * d)
        = (b * h) * (d * k)        by arithmetic
        = (y - v) * (y + v)        by DIV_MULT_EQ
          h * k * (a * c)
        = (a * h) * (c * k)        by arithmetic
        = (x + u) * (x + u)        by DIV_MULT_EQ

   Step 3: show y = a * d + b * c
        h * k * (a * d + b * c)
      = h * k * (a * d) + h * k * (b * c)      by LEFT_ADD_DISTRIB
      = (x + u) * (y + v) + (y - v) * (x + u)  by above
      = (x + u) * ((y + v) + (y - v))          by LEFT_ADD_DISTRIB
      = (x + u) * (2 * y)                      by SUB_LEFT_ADD
      = h * k * y                              by h * k = 2 * (x + u)

      Thus a * d + b * c = y                   by EQ_MULT_LCANCEL, MULT_EQ_0

   Step 4: show x = if b * d <= a * c then a * c - b * d else (b * d - a * c)
         h * k * (a * c - b * d)
       = h * k * (a * c) - h * k * (b * d)     by LEFT_ADD_DISTRIB
       = (x + u) * (x + u) - (y - v) * (y + v) by above
       = (x + u) * (x + u) - (u - x) * (u + x) by norm_eq_property
       = (x + u) * ((x + u) - (u - x))         by LEFT_SUB_DISTRIB
       = (x + u) * (2 * x)                     by SUB_LEFT_SUB
       = h * k * x                             by h * k = 2 * (x + u)

      Thus a * c - b * d = x                   by EQ_MULT_LCANCEL, MULT_EQ_0
      Note ODD x ==> x <> 0                    by ODD_POS
        so b * d < a * c                       by SUB_LESS_0
        or b * d <= a * c                      by LT_IMP_LE

      Thus x = if b * d <= a * c then a * c - b * d else (b * d - a * c)

   Therefore,
      norm (a,b) * norm (c,d) = norm (y,x)
                              = norm (x,y)     by norm_sym

   Note 0 < a, 0 < c                           by DIV_POS, DIVIDES_LE
   If v < y, 0 < b                             by DIV_POS, DIVIDES_LE
   If v < y, then 2 * v < v + y, so 0 < d      by DIV_POS, DIVIDES_LE
   Thus 1 < norm (a,b)                         by norm_gt_1
    and 1 < norm (c,d)                         by norm_gt_1
*)
Theorem backward_thm:
  !x y u v a b c d. norm (x,y) = norm (u,v) /\ v <= y /\ coprime x y /\ ODD x /\ ODD u /\
                     ((a,b),(c,d)) = backward (x,y) (u,v) ==>
                     norm (a,b) * norm (c,d) = norm (x,y) /\
                     (v < y ==> 1 < norm (a,b) /\ 1 < norm (c,d))
Proof
  ntac 9 strip_tac >>
  fs[backward_def, reduce_def] >>
  qabbrev_tac `h = gcd (u + x) (y - v)` >>
  qabbrev_tac `k = gcd (u + x) (v + y)` >>
  `norm (a,b) * norm (c,d) = norm (x,y) /\ (v < y ==> 1 < norm (a,b) /\ 1 < norm (c,d))` suffices_by simp[] >>
  `x <= u` by metis_tac[norm_eq_imp, LESS_OR_EQ] >>
  `EVEN (y + v)` by metis_tac[EVEN_SUB, ODD_EVEN, norm_eq_property] >>
  `h * k = 2 * (u + x)` by metis_tac[norm_eq_coprime_gcd_product, ADD_COMM] >>
  `x <> 0 /\ u <> 0 /\ h <> 0 /\ k <> 0` by metis_tac[GCD_EQ_0, ADD_EQ_0, ODD] >>
  `h divides (u + x) /\ h  divides (y - v)` by metis_tac[GCD_IS_GREATEST_COMMON_DIVISOR] >>
  `k divides (u + x) /\ k divides (v + y)` by metis_tac[GCD_IS_GREATEST_COMMON_DIVISOR] >>
  `h * k * (a * d) = (a * h) * (d * k)` by decide_tac >>
  `_ = (u + x) * (v + y)` by metis_tac[DIV_MULT_EQ, NOT_ZERO] >>
  `h * k * (b * c) = (b * h) * (c * k)` by decide_tac >>
  `_ = (y - v) * (u + x)` by metis_tac[DIV_MULT_EQ, NOT_ZERO] >>
  `h * k * (b * d) = (b * h) * (d * k)` by decide_tac >>
  `_ = (y - v) * (v + y)` by metis_tac[DIV_MULT_EQ, NOT_ZERO] >>
  `h * k * (a * c) = (a * h) * (c * k)` by decide_tac >>
  `_ = (u + x) * (u + x)` by metis_tac[DIV_MULT_EQ, NOT_ZERO] >>
  `y = a * d + b * c` by
  (`h * k * (a * d + b * c) = h * k * (a * d) + h * k * (b * c)` by decide_tac >>
  `_ = (u + x) * (v + y) + (y - v) * (u + x)` by fs[] >>
  `_ = (u + x) * (v + y + (y - v))` by simp[GSYM LEFT_ADD_DISTRIB] >>
  `_ = h * k * y` by simp[] >>
  fs[]) >>
  `x = if b * d <= a * c then a * c - b * d else (b * d - a * c)` by
    (`h * k * (a * c - b * d) = h * k * (a * c) - h * k * (b * d)` by decide_tac >>
  `_ = (u + x) * (u + x) - (y - v) * (v + y)` by fs[] >>
  `_ = (u + x) * (u + x) - (u - x) * (u + x)` by metis_tac[norm_eq_property, ADD_COMM] >>
  `_ = (u + x) * (u + x - (u - x))` by simp[LEFT_SUB_DISTRIB] >>
  `_ = h * k * x` by simp[] >>
  `a * c - b * d = x` by fs[] >>
  rfs[]) >>
  `norm (a,b) * norm (c,d) = norm (x,y)` by fs[norm_product, norm_sym] >>
  `v < y ==> 1 < norm (a,b) /\ 1 < norm (c,d)` suffices_by fs[] >>
  strip_tac >>
  `0 < a /\ 0 < b /\ 0 < c /\ 0 < d` by rfs[DIV_POS, DIVIDES_LE] >>
  simp[norm_gt_1]
QED

(* This is fantastic! *)

(* Another formulation of backward_thm *)

(* Theorem: norm (x,y) = norm (u,v) /\ v <= y /\ coprime x y /\ ODD x /\ ODD u ==>
             let j = backward (x,y) (u,v) in norm (FST j) * norm (SND j) = norm (x,y) /\
             (v < y ==> 1 < norm (FST j) /\ 1 < norm (SND j)) *)
(* Proof: by backward_thm *)
Theorem backward_thm_alt:
  !x y u v. norm (x,y) = norm (u,v) /\ v <= y /\ coprime x y /\ ODD x /\ ODD u ==>
             let j = backward (x,y) (u,v) in norm (FST j) * norm (SND j) = norm (x,y) /\
             (v < y ==> 1 < norm (FST j) /\ 1 < norm (SND j))
Proof
  metis_tac[backward_thm, pairTheory.PAIR]
QED

(* Idea: if number n has two different norms, it must be composite. *)

(* Theorem: n = norm (x,y) /\ n = norm (u,v) /\
            v < y /\ coprime x y /\ ODD x /\ ODD u ==>
            ?p q. n = p * q /\ 1 < p /\ 1 < q *)
(* Proof:
   Let j = backward (x,y) (u,v)
   Take p = norm (FST j),
        q = norm (SND j).
   Note v < y ==> v <= y           by LT_IMP_LE
     so n = p * q                  by backward_thm_alt
   With v < y, 1 < p and 1 < q     by backward_thm_alt
*)
Theorem two_norms_imp_factors:
  !n x y u v. n = norm (x,y) /\ n = norm (u,v) /\
               v < y /\ coprime x y /\ ODD x /\ ODD u ==>
               ?p q. n = p * q /\ 1 < p /\ 1 < q
Proof
  rpt strip_tac >>
  qabbrev_tac `j = backward (x,y) (u,v)` >>
  qexists_tac `norm (FST j)` >>
  qexists_tac `norm (SND j)` >>
  metis_tac[backward_thm_alt, LT_IMP_LE]
QED

(* Idea: GCD of both sides of a norm equality are the same.
   But this is not true!
   Counter-example: 5x5x13 = 325 = 1² + 18² = 6² + 17² = 10² + 15²
   gcd (1,18) = 1, gcd (6,17) = 1, gcd (10,15) = 5.
 *)

(* Theorem: prime p /\ p = a ** 2 + b ** 2 ==> coprime a b *)
(* Proof:
   Let g = gcd a b
   Then g divides a, g divides b               by GCD_IS_GREATEST_COMMON_DIVISOR
     so ?h k. a = h * g, b = k * g             by divides_def
        p = a ** 2 + b ** 2
          = (h * g) ** 2 + (k * g) ** 2        by a = h * g, b = k * g
          = h ** 2 * g ** 2 + k ** 2 * g ** 2  by EXP_BASE_MULT
          = (h ** 2 + k ** 2) * g ** 2         by RIGHT_ADD_DISTRIB
          = (h ** 2 + k ** 2) * (g * g)        by EXP_2
   Thus (g * g) divides p                      by divides_def
     so g * g = 1 or g * g = p                 by prime_def
    But p <> g * g                             by prime_non_square, square_def
     so g * g = 1, or g = 1                    by MULT_EQ_1
*)
Theorem prime_two_squares_coprime:
  !p a b. prime p /\ p = a ** 2 + b ** 2 ==> coprime a b
Proof
  rpt strip_tac >>
  qabbrev_tac `g = gcd a b` >>
  `g divides a /\ g divides b` by simp[GCD_IS_GREATEST_COMMON_DIVISOR, Abbr`g`] >>
  `?h k. a = h * g /\ b = k * g` by metis_tac[divides_def] >>
  `p = (h * g) ** 2 + (k * g) ** 2` by fs[] >>
  `_ = h ** 2 * g ** 2 + k ** 2 * g ** 2` by simp[EXP_BASE_MULT] >>
  `_ = (h ** 2 + k ** 2) * (g * g)` by simp[GSYM EXP_2] >>
  `(g * g) divides p` by simp[GSYM divides_def] >>
  `g * g = 1` by metis_tac[prime_def, prime_non_square, square_def] >>
  fs[]
QED

(*
fermat_two_squares_unique_odd_even
|- !p a b c d. prime p /\
               ODD a /\ EVEN b /\ p = a ** 2 + b ** 2 /\
               ODD c /\ EVEN d /\ p = c ** 2 + d ** 2 ==>
               a = c /\ b = d
*)

(* Theorem: prime n /\
            n = a ** 2 + b ** 2 /\ ODD a /\
            n = c ** 2 + d ** 2 /\ ODD c ==> a = c /\ b = d *)
(* Proof:
   Note n = norm (a,b) and n = norm (c,d)      by norm_def
     so gcd a b = 1 and gcd c d = 1            by prime_two_squares_coprime
    and a = b ==> c = d                        by norm_eq_imp
     so only need to show: a = c.
   By contradication, suppose a <> c.
   Then a < c or c < a.

   If a < c,
      Note two_norms_imp_factors |> SPEC ``n:num`` |> SPEC ``a:num`` |> SPEC ``b:num`` |> SPEC ``c:num`` |> SPEC ``d:num``;
           |- n = norm (a,b) /\ n = norm (c,d) /\ d < b /\ coprime a b /\ ODD a /\ ODD c ==> ?p q. n = p * q /\ 1 < p /\ 1 < q
       and d < b                               by norm_eq_imp
        so ?p q. n = p * q /\ 1 < p /\ 1 < q   by two_norms_imp_factors, norm_sym, GCD_SYM
       ==> p divides n                         by divides_def
       but p <> 1                              by LESS_NOT_EQ
        so p = n                               by prime_def
      This gives q = 1, contradicting 1 < q.

   If c < a,
      Note two_norms_imp_factors |> SPEC ``n:num`` |> SPEC ``c:num`` |> SPEC ``d:num`` |> SPEC ``a:num`` |> SPEC ``b:num``;
           |- n = norm (c,d) /\ n = norm (a,b) /\ b < d /\ coprime c d /\ ODD c /\ ODD a ==> ?p q. n = p * q /\ 1 < p /\ 1 < q
       and b < d                               by norm_eq_imp
        so ?p q. n = p * q /\ 1 < p /\ 1 < q   by two_norms_imp_factors
       ==> q divides n                         by divides_def
       but q <> 1                              by LESS_NOT_EQ
        so q = n                               by prime_def
      This gives p = 1, contradicting 1 < p.
*)
Theorem fermat_two_squares_unique_odd:
  !n a b c d. prime n /\
               n = a ** 2 + b ** 2 /\ ODD a /\
               n = c ** 2 + d ** 2 /\ ODD c ==>
               a = c /\ b = d
Proof
  ntac 6 strip_tac >>
  `n = norm (a,b) /\ n = norm (c,d)` by simp[norm_def] >>
  `coprime a b /\ coprime c d` by metis_tac[prime_two_squares_coprime] >>
  `a = c` suffices_by metis_tac[norm_eq_imp] >>
  spose_not_then strip_assume_tac >>
  `a < c \/ c < a` by decide_tac >| [
    assume_tac two_norms_imp_factors >>
    first_x_assum (qspecl_then [`n`, `a`, `b`, `c`, `d`] strip_assume_tac) >>
    `d < b` by metis_tac[norm_eq_imp] >>
    `?p q. n = p * q /\ 1 < p /\ 1 < q` by metis_tac[two_norms_imp_factors, norm_sym, GCD_SYM] >>
    `q = n` by metis_tac[divides_def, prime_def, prim_recTheory.LESS_NOT_EQ] >>
    rfs[],
    assume_tac two_norms_imp_factors >>
    first_x_assum (qspecl_then [`n`, `c`, `d`, `a`, `b`] strip_assume_tac) >>
    `b < d` by metis_tac[norm_eq_imp] >>
    `?p q. n = p * q /\ 1 < p /\ 1 < q` by metis_tac[two_norms_imp_factors] >>
    `q = n` by metis_tac[divides_def, prime_def, prim_recTheory.LESS_NOT_EQ] >>
    rfs[]
  ]
QED

(* Finally, another proof of the uniqueness of two squares! *)



(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
