(* ------------------------------------------------------------------------- *)
(* Windmills of the minds: Fermat's Two Squares Theorem (part 3)             *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "part3";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)

(* val _ = load "windmillTheory"; *)
open helperTwosqTheory;
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory; (* for divides_def, prime_def *)
open logPowerTheory; (* for prime_non_square *)

open windmillTheory;

(* val _ = load "involuteFixTheory"; *)
open involuteTheory;
open involuteFixTheory; (* for pairs_def, fixes_def *)

(* val _ = load "involuteActionTheory"; *)
open involuteActionTheory;
open groupActionTheory;
open groupInstancesTheory; (* for Zadd_element *)

open pairTheory; (* for ELIM_UNCURRY *)


(* ------------------------------------------------------------------------- *)
(* Windmills of the minds (part 3) Documentation                             *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   Helper Theorems:

   Relation to Fixes and Pairs:
   involute_fixed_points_eq_fixes
                   |- !f X. f involute X ==>
                            fixed_points (FUNPOW f) Z2 X = fixes f X
   involute_multi_orbits_union_eq_pairs
                   |- !f X. f involute X ==>
                            BIGUNION (multi_orbits (FUNPOW f) Z2 X) = pairs f X

   Fermat's Two-Squares Theorem (Existence):
   zagier_fixed_points_sing
                   |- !p. prime p /\ p MOD 4 = 1 ==>
                          fixed_points (FUNPOW zagier) Z2 (mills p) = {(1,1,p DIV 4)}
   fermat_two_squares_exists_alt
                   |- !p. prime p /\ p MOD 4 = 1 ==> ?(u,v). p = u ** 2 + v ** 2

   Group-theoretic view of Involution:
   involute_stabilizer
                   |- !f X x. f involute X /\ x IN X ==>
                              stabilizer (FUNPOW f) Z2 x = if f x = x then {0; 1} else {0}
*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* This part3 makes use of fixed points and multi-orbits of group action. *)
(* Note: the group Z2 = Zadd 2, the cyclic addition of modulo 2. *)

(* ------------------------------------------------------------------------- *)
(* Relation to Fixes and Pairs.                                              *)
(* ------------------------------------------------------------------------- *)

(* Theorem: f involute X ==> fixed_points (FUNPOW f) Z2 X = fixes f X *)
(* Proof:
   By fixed_points_def, fixes_def, EXTENSION, this is to show:
   (1) !a. a < 2 ==> (FUNPOW f a x = x) ==> f x = x
       Note f x = FUNPOW f 1 x = x          by FUNPOW_1, 1 < 2
   (2) f x = x ==> !a. a < 2 ==> FUNPOW f a x = x
       When a = 0, FUNPOW f 0 x = x         by FUNPOW_0
       When a = 1, FUNPOW f 1 x = f x = x   by FUNPOW_1, f x = x
*)
Theorem involute_fixed_points_eq_fixes:
  !f X. f involute X ==> fixed_points (FUNPOW f) Z2 X = fixes f X
Proof
  rw[fixed_points_def, fixes_def, EXTENSION, EQ_IMP_THM] >-
  metis_tac[FUNPOW_1, DECIDE``1 < 2``] >>
  (`a = 0 \/ a = 1` by decide_tac >> simp[])
QED

(* Theorem: f involute X ==> BIGUNION (multi_orbits (FUNPOW f) Z2 X) = pairs f X *)
(* Proof:
   By multi_orbits_def, pairs_def, BIGUNION, EXTENSION, this is to show:
   (1) x IN e /\ e IN orbits (FUNPOW f) Z2 X /\ ~SING e ==> x IN X /\ f x <> x
       Note x IN X                           by involute_orbits_element_element
        and e = orbit (FUNPOW f) Z2 x        by involute_orbits_element_is_orbit
       By contradiction, suppose f x = x,
       Then e = {x}                          by involute_orbit_fixed
       with contradicts ~SING e              by SING
   (2) x IN X /\ f x <> x ==> ?e. x IN e /\ e IN orbits (FUNPOW f) Z2 X /\ ~SING e
       Let e = {x; f x}.
       Then x IN e, and ~SING e              by f x <> x
       The goal is to show: {x; f x} IN orbits (FUNPOW f) Z2 X

       Note {x; f x}
          = orbit (FUNPOW f) Z2 x            by involute_orbit_not_fixed
       which is IN orbits (FUNPOW f) Z2 X    by funpow_orbit_in_orbits
*)
Theorem involute_multi_orbits_union_eq_pairs:
  !f X. f involute X ==> BIGUNION (multi_orbits (FUNPOW f) Z2 X) = pairs f X
Proof
  rw[multi_orbits_def, pairs_def, EXTENSION, EQ_IMP_THM] >-
  metis_tac[involute_orbits_element_element] >-
 (`x IN X` by metis_tac[involute_orbits_element_element] >>
  `s = orbit (FUNPOW f) Z2 x` by metis_tac[involute_orbits_element_is_orbit] >>
  metis_tac[involute_orbit_fixed, SING]) >>
  qexists_tac `{x; f x}` >>
  simp[] >>
  `{x; f x} = orbit (FUNPOW f) Z2 x` by metis_tac[involute_orbit_not_fixed] >>
  rw[funpow_orbit_in_orbits]
QED

(* ------------------------------------------------------------------------- *)
(* Fermat's Two-Squares Theorem (Existence)                                  *)
(* ------------------------------------------------------------------------- *)

(* With zagier and flip both involutions on (mills p),
    and zagier with a unique fixed point,
   this implies flip has at least a fixed point,
   giving the existence of Fermat's two squares.
*)

(* Theorem: prime p /\ p MOD 4 = 1 ==>
            fixed_points (FUNPOW zagier) Z2 (mills p) = {(1,1,p DIV 4)} *)
(* Proof:
   Let X = mills p,
       a = fixed_points (FUNPOW zagier) Z2 X.
   The goal is: a = {(1,1,p DIV 4)}.

   Note ~square p                   by prime_non_square
     so zagier involute X           by zagier_involute_mills

   By EXTENSION, this is to show:
   (1) t IN X ==> t = (1,1,p DIV 4)
       Note t IN X                  by involute_fixed_points_element_element
        and zagier t = t            by involute_fixed_points
        Now ?x y z. t = (x, y, z)   by triple_parts
        and x <> 0                  by mills_triple_nonzero, ~square p
        ==> x = y                   by zagier_fix
       Note p = windmill (x, y, z)      by mills_element
        ==> (x,y,z) = (1,1,p DIV 4) by windmill_trivial_prime

   (2) (1,1,p DIV 4) IN a
       Note (1,1,p DIV 4) IN X      by mills_element_trivial
        and zagier (1,1,p DIV 4)
          = zagier (1,1,p DIV 4)    by zagier_1_1_z
         so (1,1,p DIV 4) IN a      by involute_fixed_points_iff
*)
Theorem zagier_fixed_points_sing:
  !p. prime p /\ p MOD 4 = 1 ==>
      fixed_points (FUNPOW zagier) Z2 (mills p) = {(1,1,p DIV 4)}
Proof
  rpt strip_tac >>
  qabbrev_tac `X = mills p` >>
  qabbrev_tac `a = fixed_points (FUNPOW zagier) Z2 X` >>
  `~square p` by metis_tac[prime_non_square] >>
  `p MOD 4 <> 0` by decide_tac >>
  `zagier involute X` by  metis_tac[zagier_involute_mills] >>
  rw[EXTENSION, EQ_IMP_THM] >| [
    `x IN X` by metis_tac[involute_fixed_points_element_element] >>
    `zagier x = x` by metis_tac[involute_fixed_points] >>
    `?x1 y z. x = (x1, y, z)` by rw[triple_parts] >>
    `x1 <> 0` by metis_tac[mills_triple_nonzero] >>
    `x1 = y` by fs[zagier_fix] >>
    metis_tac[windmill_trivial_prime, mills_element],
    `(1,1,p DIV 4) IN X` by rw[mills_element_trivial, Abbr`X`] >>
    metis_tac[zagier_1_1_z, involute_fixed_points_iff]
  ]
QED

(* Theorem: prime p /\ p MOD 4 = 1 ==> ?u v. p = u ** 2 + v ** 2 *)
(* Proof:
   Let X = mills p, the solutions (x,y,z) of p = x ** 2 + 4 * y * z.
   Note ~square p                      by prime_non_square
     so FINITE X                       by mills_non_square_finite
    Now !x y z. (x,y,z) IN X ==>
        x <> 0 /\ y <> 0 /\ z <> 0     by mills_triple_nonzero
    and zagier involute X              by zagier_involute_mills
    and flip involute X                by flip_involute_mills

   Let a = fixed_points (FUNPOW zagier) Z2 X,
       b = fixed_points (FUNPOW flip) Z2 X.
   Then ODD (CARD a) <=> ODD (CARD b)  by involute_two_fixed_points_both_odd

   The punch line:
   Let k = p DIV 4.
   Then a = {(1,1,k)}                  by zagier_fixed_points_sing
   Thus CARD a = 1                     by CARD_SING
     so ODD (CARD b)                   by ODD_1, above
    ==> CARD b <> 0                    by EVEN_0
     or b <> EMPTY                     by CARD_EMPTY
   thus ?x y z. (x,y,z) IN b           by MEMBER_NOT_EMPTY, triple_parts
     so flip (x, y, z) = (x, y, z)     by involute_fixed_points
    ==> y = z                          by flip_fix
   Note (x,y,z) IN X                   by fixed_points_element
   Put u = x, v = 2 * y.
   Then p = u ** 2 + v ** 2            by mills_element, windmill_by_squares
*)
Theorem fermat_two_squares_exists_alt:
  !p. prime p /\ p MOD 4 = 1 ==> ?(u,v). p = u ** 2 + v ** 2
Proof
  rw[ELIM_UNCURRY] >>
  qabbrev_tac `X = mills p` >>
  `~square p` by metis_tac[prime_non_square] >>
  `FINITE X` by fs[mills_non_square_finite, Abbr`X`] >>
  `zagier involute X` by metis_tac[zagier_involute_mills, DECIDE``1 <> 0``] >>
  `flip involute X` by metis_tac[flip_involute_mills] >>
  qabbrev_tac `a = fixed_points (FUNPOW zagier) Z2 X` >>
  qabbrev_tac `b = fixed_points (FUNPOW flip) Z2 X` >>
  `ODD (CARD a) <=> ODD (CARD b)` by rw[involute_two_fixed_points_both_odd, Abbr`a`, Abbr`b`] >>
  qabbrev_tac `k = p DIV 4` >>
  `a = {(1,1,k)}` by rw[zagier_fixed_points_sing, Abbr`a`, Abbr`X`, Abbr`k`] >>
  `CARD a = 1` by rw[] >>
  `CARD b <> 0` by metis_tac[ODD_1, EVEN_0, ODD_EVEN] >>
  `b <> EMPTY` by metis_tac[CARD_EMPTY] >>
  `?x y z. (x,y,z) IN b` by metis_tac[MEMBER_NOT_EMPTY, triple_parts] >>
  `flip (x, y, z) = (x, y, z)` by metis_tac[involute_fixed_points] >>
  `y = z` by fs[flip_fix] >>
  `(x,y,z) IN X` by fs[fixed_points_element, Abbr`b`] >>
  qexists_tac `(x,2 * y)` >>
  simp[] >>
  metis_tac[mills_element, windmill_by_squares]
QED

(* Very good! *)

(* ------------------------------------------------------------------------- *)
(* Group-theoretic view of Involution                                        *)
(* ------------------------------------------------------------------------- *)

(* Theory:

   A group g has elements a, b, c, ... etc., with group operation o, so that c = a o b.
   A set X has elements x, y, z, ... etc.
   A group action A, (g act X) A  x IN X ==>
            (!a. a IN G ==> A a x IN X) /\ A g.id x = x /\
            !a b. a IN G /\ b IN G ==> A a (A b x) = A (a o b) x
   has a group, acting on the set X, with action A a map from an element of g to an element of X, giving an element of X.
   The action orbit of element x, orbit A g x = IMAGE (\a. A a x) G
   The action stabilizer of element x, stabilizer A g x = {a | a IN G /\ A a x = x}
   The fixed points of action, fixed_points A g X = {x | x IN X /\ !a. a IN G ==> A a x = x}

   For involution f,
   The set X = mills n.
   The group g = Z2 = Zadd 2.
   The action A = FUNPOW f: 0 maps to (FUNPOW f 0) = I, the identity function,
                            1 maps to (FUNPOW f 1) = f, the involution function,
                            and f o f = I mimics 1 + 1 = 0 (MOD 2).
   funpow_action   |- !f X. f involute X ==> (Z2 act X) (FUNPOW f)
   funpow_orbit    |- !f x. orbit (FUNPOW f) Z2 x = {FUNPOW f a x | a < 2}
                         or orbit (FUNPOW f) Z2 x = {FUNPOW f a x | a IN Z2.carrier}
   involute_stabilizer        !f X x. f involute X /\ x IN X ==> stabilizer (FUNPOW f) Z2 x = if (f x = x) then {0; 1} else {0}
   involute_fixed_points_iff  !f X x. f involute X /\ x IN X ==> (x IN fixed_points (FUNPOW f) Z2 X <=> f x = x)
   involute_fixed_points_eq_fixes        |- !f X. f involute X ==> fixed_points (FUNPOW f) Z2 X = fixes f X
   involute_multi_orbits_union_eq_pairs  |- !f X. f involute X ==> BIGUNION (multi_orbits (FUNPOW f) Z2 X) = pairs f X

   These are no good for iteration, based on two involutions: f o g.
*)

(* Theorem: f involute X /\ x IN X ==> stabilizer (FUNPOW f) Z2 x = if (f x = x) then {0; 1} else {0} *)
(* Proof:
     stabilizer (FUNPOW f) Z2 x
   = {a | a IN Z2 /\ (FUNPOW f) a x = x}       by stabilizer_def
   = {a < 2 /\ (FUNPOW f) a x = x}             by Zadd_element
   = {a | a = 0 \/ a = 1 /\ (FUNPOW f) a x = x}
   = {0} UNION (if (f x = x) then {1} else {}) by FUNPOW_0, FUNPOW_1
   = if (f x = x) then {0; 1} else {0}
*)
Theorem involute_stabilizer:
  !f X x. f involute X /\ x IN X ==> stabilizer (FUNPOW f) Z2 x = if (f x = x) then {0; 1} else {0}
Proof
  simp[stabilizer_def, EXTENSION] >>
  ntac 5 strip_tac >>
  rename1 `a < 2` >>
  rw[EQ_IMP_THM] >-
  simp[FUNPOW_0] >-
  simp[FUNPOW_1] >>
  spose_not_then strip_assume_tac >>
  `a = 1` by decide_tac >>
  fs[FUNPOW_1]
QED

(*

Thus stabilizers are either singleton or doublets.
Those x with singleton stabilizers are elements of pairs f X, stabilizer (FUNPOW f) Z2 x = {0}
Those x with doublet stabilizers are elements of fixes f X, stabilizer (FUNPOW f) Z2 x = {0; 1}
The orbits are NOT the iteration orbits, as f is an involution but iteration is on f o g.
orbit_stabilizer_thm
FiniteGroup g /\ (g act X) A /\ x IN X /\ FINITE X ==>
           CARD G = CARD (orbit A g x) * CARD (stabilizer A g x)

If stabilizer (FUNPOW f) Z2 x = {0}, x IN pairs f x, orbit (FUNPOW f) Z2 x = {x; f x}, |Z2.carrier| = 2 = 2 * 1.
If stabilizer (FUNPOW f) Z2 x = {0; 1}, x IN fixes f x, orbit (FUNPOW f) Z2 x = {x},   |Z2.carrier| = 2 = 1 * 2.

*)

(* Theory:

   For a permutation h, h permutes X.
   The set X = mills n.
   The group g = Z_add, all integers under addition.
   The action A = FUNPOW h,
    so that (orbit A g x) = {y | y = FUNPOW h n x where n = integer} = {x; f x; f (f x); ...} = iteration orbit
   and (stabilizer A g x) = {n * p | p = iterate_period h x, n = integer} = {0; +p; -p; +2p; -2p; ...}
   and (fixed_points A g x) = {}, as no point x is fixed by all integers.
   target_card_by_fixed_points
   |- !A g X. Group g /\ (g act X) A /\ FINITE X ==>
              CARD X = CARD (fixed_points A g X) + SIGMA CARD (multi_orbits A g X)
   Since (fixed_points A g x) = {}, this simplifies to CARD X = SIGMA CARD (multi_orbits A g X)
   giving not much information!
*)


(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
