(* ------------------------------------------------------------------------- *)
(* Windmills of the minds: Fermat's Two Squares Theorem                      *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "moreTwosq";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)

(* val _ = load "windmillTheory"; *)
open helperTwosqTheory;
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory; (* for divides_def, prime_def *)
open logPowerTheory; (* for prime_non_square *)

(* val _ = load "quarityTheory"; *)
open quarityTheory;

(* val _ = load "involuteFixTheory"; *)
open involuteTheory; (* for involute_bij *)
open involuteFixTheory;

(* val _ = load "iterateComposeTheory"; *)
open iterationTheory;
open iterateComposeTheory;

(* val _ = load "iterateComputeTheory"; *)
open iterateComputeTheory;

(* for later computation *)
open listTheory;
open rich_listTheory; (* for MAP_REVERSE *)
open helperListTheory; (* for listRangeINC_LEN *)
open listRangeTheory; (* for listRangeINC_CONS *)
open gcdTheory; (* for GCD_IS_GREATEST_COMMON_DIVISOR *)

(* for group action *)
(* val _ = load "involuteActionTheory"; *)
open involuteActionTheory;
open groupActionTheory;
open groupInstancesTheory;
open binomialTheory; (* for Gauss' formula *)
open EulerTheory; (* for natural n *)

(* for pairs *)
open pairTheory; (* for ELIM_UNCURRY, PAIR_FST_SND_EQ, PAIR_EQ, FORALL_PROD *)


(* ------------------------------------------------------------------------- *)
(* More about Fermat's Two Squares Theorem Documentation                     *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
   ONE_to_ONE f   = !x1 x2. f x1 = f x2 ==> x1 = x2
   EXCLUSIVE f s  = !x. x IN s <=> f x IN (IMAGE f s)
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   Helper Theorems:
   natural_one_less    |- !n. natural (n - 1) = {j | 0 < j /\ j < n}
   tik_eqn             |- !n. tik n ==> n = 4 * (n DIV 4) + 1
   pairs_by_diff       |- !f s. pairs f s = s DIFF fixes f s
   fixes_by_diff       |- !f s. fixes f s = s DIFF pairs f s

   One-to-One Investigation:
   ONE_to_ONE_iff      |- !f. ONE_to_ONE f <=> !s. INJ f s univ(:'a)
   INJ_11              |- !f s. INJ f s s ==> ONE_to_ONE (\x. if x IN s then f x else x)

   Exclusive function on elements:
   pairs_doublet_exclusive     |- !f s. IMAGE f s = s ==> EXCLUSIVE (\x. {x; f x}) (pairs f s)
   pairs_doublet_exclusive_2   |- !f s. f involute s ==> EXCLUSIVE (\x. {x; f x}) (pairs f s)

   More on Arithmetic:
   SUB_SUB_ID          |- !n x. x <= n ==> n - (n - x) = x
   PROPER_FACTOR       |- !n. 1 < n /\ ~prime n ==> ?a. 1 < a /\ a < n /\ a divides n
   PROPER_FACTOR_PAIR  |- !n. 1 < n /\ ~prime n ==> ?a b. n = a * b /\ 1 < a /\ 1 < b /\ a < n /\ b < n

   More on MOD arithmetic:
   MOD_SUM             |- !j k n. (j + k) MOD n = (j MOD n + k MOD n) MOD n
   MOD_PRODUCT         |- !j k n. (j * k) MOD n = (j MOD n * k MOD n) MOD n
   MOD_OF_MOD          |- !k n. k MOD n MOD n = k MOD n
   TIMES_MOD_MOD       |- !n a b. (a * b MOD n) MOD n = (a * b) MOD n
   MOD_ADD             |- !m n q. 0 < n ==> (m + n * q) MOD n = m MOD n
   MOD_EQ_0_ID         |- !m n. 0 < m /\ m < 2 * n ==> (m MOD n = 0 <=> m = n)
   MOD_ADD_EQ_0_ID     |- !n x y. 0 < x + y /\ x < n /\ y < n /\ (x + y) MOD n = 0 ==> x + y = n
   MOD_NEG_MULT        |- !n x y. x <= n ==> ((n - x) * y) MOD n = (n - (x * y) MOD n) MOD n
   MOD_NEG_SUB         |- !n x. 0 < x /\ x < n ==> (n - x) MOD n = n - x MOD n
   MOD_NEG_NEG         |- !n x. 0 < n /\ 0 < x MOD n ==> n - (n - x MOD n) MOD n = x MOD n
   MOD_SQ_EQ_1_prime   |- !n x. prime n ==> (x ** 2 MOD n = 1 <=> x MOD n = 1 \/ x MOD n = n - 1)

   More for helperSet:
   equiv_class_subset          |- !R s x. equiv_class R s x SUBSET s
   equiv_class_finite          |- !R s x. FINITE s ==> FINITE (equiv_class R s x)
   equiv_pairs_every_finite    |- !f s. FINITE s ==> EVERY_FINITE (equiv_pairs f s)
   INSERT_UNION_SWAP           |- !x s t. (x INSERT s) UNION t = s UNION (x INSERT t)
   PAIR_DISJOINT_INSERT        |- !e s. e NOTIN s /\ PAIR_DISJOINT (e INSERT s) ==> DISJOINT e (BIGUNION s)
   DISJOINT_UNION_SPLIT        |- !s t. DISJOINT s t ==> s UNION t =|= s # t
   PROD_SET_doublet            |- !a b. a <> b ==> PROD_SET {a; b} = a * b
   PROD_IMAGE_UNION_EQN        |- !s t. FINITE s /\ FINITE t ==>
                                        !f. PI f (s UNION t) * PI f (s INTER t) = PI f s * PI f t
   PROD_SET_UNION              |- !s t. FINITE s /\ FINITE t ==>
                                        PROD_SET (s UNION t) * PROD_SET (s INTER t) = PROD_SET s * PROD_SET t
   set_multiplicative_pi       |- !f. SET_MULTIPLICATIVE (PI f)
   disjoint_bigunion_pi        |- !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
                                  !f. PI f (BIGUNION P) = PI (PI f) P
   set_pi_by_partition         |- !R s. R equiv_on s /\ FINITE s ==> !f. PI f s = PI (PI f) (partition R s)
   PROD_IMAGE_AS_PROD_SET      |- !s. FINITE s ==> !f. ONE_to_ONE f ==> PI f s = PROD_SET (IMAGE f s)
   SUM_SET_UNION_DISJOINT      |- !s t. FINITE s /\ FINITE t /\ DISJOINT s t ==>
                                        SUM_SET (s UNION t) = SUM_SET s + SUM_SET t
   SUM_SET_SUM_BY_SPLIT        |- !s. FINITE s ==> !u v. s =|= u # v ==> SUM_SET s = SUM_SET u + SUM_SET v
   PROD_SET_PRODUCT_BY_SPLIT   |- !s. FINITE s ==> !u v. s =|= u # v ==> PROD_SET s = PROD_SET u * PROD_SET v
   PROD_SET_UNION_DISJOINT     |- !s t. FINITE s /\ FINITE t /\ DISJOINT s t ==>
                                        PROD_SET (s UNION t) = PROD_SET s * PROD_SET t
   SUM_SET_BIGUNION            |- !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
                                      SUM_SET (BIGUNION P) = SIGMA SUM_SET P
   PROD_SET_BIGUNION           |- !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
                                      PROD_SET (BIGUNION P) = PI PROD_SET P
   PROD_IMAGE_INJ_o            |- !s f g. FINITE s /\ INJ g s univ(:'a) ==> PI (f o g) s = PI f (IMAGE g s)
   PROD_SET_MOD_BIGUNION       |- !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
                                  !n. PROD_SET (BIGUNION P) MOD n = PI (\s. PROD_SET s MOD n) P MOD n

   More for helperList:
   PROD_SUC            |- !m n. m <= n ==> PROD [m .. n] = m * PROD [m + 1 .. n]
   PROD_FRONT_LAST     |- !ls. ls <> [] ==> PROD ls = LAST ls * PROD (FRONT ls)
   PROD_NEG_MOD_EQN    |- !m n. 1 < n /\ m < n ==>
                                PROD [n - m .. n - 1] MOD n =
                                if EVEN m then FACT m MOD n else (n - FACT m MOD n) MOD n
   PROD_MAP_CONS       |- !f h t. PROD (MAP f (h::t)) = f h * PROD (MAP f t)
   PROD_K_LIST         |- !ls k. EVERY ($= k) ls ==> PROD ls = k ** LENGTH ls

   More for helperFunction:
   FACT_EQN            |- !n. FACT n = if n = 0 then 1 else n * FACT (n - 1)
   FACT_BY_PROD        |- !n. FACT n = PROD [1 .. n]
   FACT_BY_PROD_SET    |- !n. FACT n = PROD_SET (natural n)
   FACT_IN_FACT        |- !m n. m <= n ==> FACT n = FACT m * PROD [m + 1 .. n]
   FACT_factor         |- !m n. 0 < m /\ m <= n ==> m divides FACT n
   FACT_divides_FACT   |- !m n. m <= n ==> FACT m divides FACT n
   FACT_factor_product |- !n a b. 0 < a /\ 0 < b /\ a <= n /\ b <= n /\ a <> b ==> a * b divides FACT n

   SET_TO_LIST Theorems:
   PROD_SET_MOD_BIGUNION_eq_PROD_MAP_MOD
                       |- !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
                          !n. PROD_SET (BIGUNION P) MOD n =
                              PROD (MAP (\s. PROD_SET s MOD n) (SET_TO_LIST P)) MOD n

   Fast Computations:
   fact_mod_def        |- (!m. fact_mod 0 m = 1 MOD m) /\
                           !n m. fact_mod (SUC n) m = (SUC n * fact_mod n m) MOD m
   fact_mod_thm        |- !m n. fact_mod n m = FACT n MOD m
   binomial_fast_def   |- (!m. binomial_fast m 0 = 1) /\
                           !m n. binomial_fast m (SUC n) = (m - n) * binomial_fast m n DIV SUC n
   binomial_fast_thm   |- !m n. binomial_fast m n = binomial m n

   Gauss' formula for Fermat's Two Squares:
   two_sq_gauss_def    |- !n. two_sq_gauss n =
                                 (let k = n DIV 4;
                                      u = HALF (binomial (2 * k) k) MOD n;
                                      v = (u * FACT (2 * k)) MOD n
                                   in (if u <= HALF n then u else n - u,
                                       if v <= HALF n then v else n - v))
!  two_sq_gauss_alt    |- !n. two_sq_gauss n =
                                 (let k = n DIV 4;
                                      u = HALF (binomial_fast (2 * k) k) MOD n;
                                      v = (u * fact_mod (2 * k) n) MOD n
                                   in (if u <= HALF n then u else n - u,
                                       if v <= HALF n then v else n - v))
   binomial_central_eqn|- !n. 0 < n ==> binomial (2 * n) n = 2 * binomial (2 * n - 1) (n - 1)
   tik_fact_mod_eqn    |- !n. tik n ==> FACT (n - 1) MOD n = FACT (2 * (n DIV 4)) ** 2 MOD n

   Computation of FACT (n - 1) MOD n:
   FACT_one_less_composite
                       |- !n. ~prime n ==> FACT (n - 1) MOD n = if n = 0 then 1 else if n = 4 then 2 else 0

   Multiplicative Inverse MOD n (outside Group theory):
   mod_inv_def         |- !n x. prime n /\ 0 < x /\ x < n ==>
                                0 < mod_inv n x /\ mod_inv n x < n /\ (x * mod_inv n x) MOD n = 1
   mod_inv_inv         |- !n x. prime n /\ 0 < x /\ x < n ==> mod_inv n (mod_inv n x) = x
   mod_inv_unique      |- !n x. prime n /\ 0 < x /\ x < n ==>
                          !y. 0 < y /\ y < n /\ (x * y) MOD n = 1 <=> y = mod_inv n x
   mod_inv_endo                |- !n. prime n ==> mod_inv n endo natural (n - 1)
   mod_inv_involute    |- !n. prime n ==> mod_inv n involute natural (n - 1)
   mod_inv_eq_self     |- !n x. prime n /\ 0 < x /\ x < n ==> (mod_inv n x = x <=> x = 1 \/ x = n - 1)
   mod_inv_fixes       |- !n. prime n ==> fixes (mod_inv n) (natural (n - 1)) = {1; n - 1}
   mod_inv_pairs       |- !n. prime n ==> pairs (mod_inv n) (natural (n - 1)) = natural (n - 1) DIFF {1; n - 1}
   mod_inv_fixes_card  |- !n. prime n ==> CARD (fixes (mod_inv n) (natural (n - 1))) = if n = 2 then 1 else 2
   mod_inv_pairs_card  |- !n. prime n ==> CARD (pairs (mod_inv n) (natural (n - 1))) = n - 3
   mod_inv_pairs_empty |- !n. prime n ==> (pairs (mod_inv n) (natural (n - 1)) = {} <=> n <= 3)
   mod_inv_11          |- !n. prime n ==> ONE_to_ONE (\x. if x IN natural (n - 1) then mod_inv n x else x)
   mod_inv_equiv_pairs_thm     |- !n. prime n ==>
                                      EVERY ($= 1)
                                            (MAP (\s. PROD_SET s MOD n)
                                                 (SET_TO_LIST (equiv_pairs (mod_inv n) (natural (n - 1)))))
   mod_inv_pairs_prod_mod      |- !n. prime n ==>
                                      PROD_SET (pairs (mod_inv n) (natural (n - 1))) MOD n = 1
   mod_inv_fixes_prod_mod      |- !n. prime n ==>
                                      PROD_SET (fixes (mod_inv n) (natural (n - 1))) MOD n = n - 1
   FACT_one_less_prime         |- !n. prime n ==> FACT (n - 1) MOD n = n - 1

   Wilson's Theorem (1770):
   Wilson_thm                  |- !n. prime n <=> 1 < n /\ FACT (n - 1) MOD n = n - 1
   Wilson_congruence           |- !n. prime n ==> FACT (n - 1) MOD n = n - 1

   Fermat's Little Theorem (induction proof):
   Fermat_congruence           |- !n a. prime n ==> a ** n MOD n = a MOD n

   Moser Theorem (1956):
   Moser_congruence            |- !n a. prime n ==> (FACT (n - 1) * a ** n + (n - 1) ** 2 * a) MOD n = 0

*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* A convenient form of natural (n - 1) *)

(* Theorem: natural (n - 1) = {j | 0 < j /\ j < n} *)
(* Proof:
     natural (n - 1)
   = {j | 0 < j /\ j <= n - 1}     by natural_property
   = {j | 0 < j /\ j < n}          by EXTENSION
*)
Theorem natural_one_less:
  !n. natural (n - 1) = {j | 0 < j /\ j < n}
Proof
  simp[natural_property] >>
  simp[EXTENSION]
QED

(* Theorem: tik n ==> n = 4 * (n DIV 4) + 1 *)
(* Proof:
     n
   = (n DIV 4) * 4 + (n MOD 4) by DIVISION
   = 4 * (n DIV 4) + 1         by MULT_COMM, tik n
*)
Theorem tik_eqn:
  !n. tik n ==> n = 4 * (n DIV 4) + 1
Proof
  metis_tac[DIVISION, MULT_COMM, DECIDE ``0 < 4``]
QED

(* Theorem: pairs f s = s DIFF (fixes f s) *)
(* Proof:
     pairs f s
   = {x | x IN s /\ f x <> x}            by pairs_def
   = {x | x IN s /\ x NOTIN fixes f s}   by fixes_def
   = s DIFF (fixes f s)                  by DIFF_DEF
*)
Theorem pairs_by_diff:
  !f s. pairs f s = s DIFF (fixes f s)
Proof
  rw[pairs_def, fixes_def, DIFF_DEF, EXTENSION, EQ_IMP_THM] >>
  metis_tac[]
QED

(* Theorem: fixes f s = s DIFF (pairs f s) *)
(* Proof:
     fixes f s
   = {x | x IN s /\ f x = x}             by fixes_def
   = {x | x IN s /\ x NOTIN pairs f s}   by pairs_def
   = s DIFF (pairs f s)                  by DIFF_DEF
*)
Theorem fixes_by_diff:
  !f s. fixes f s = s DIFF (pairs f s)
Proof
  rw[fixes_def, pairs_def, DIFF_DEF, EXTENSION, EQ_IMP_THM]
QED

(* ------------------------------------------------------------------------- *)
(* One-to-One Investigation                                                  *)
(* ------------------------------------------------------------------------- *)

(* In pred_setTheory:
ONE_ONE_DEF  |- ONE_ONE = (\f. !x1 x2. f x1 = f x2 ==> x1 = x2)
ONE_ONE_THM  |- !f. ONE_ONE f <=> !x1 x2. f x1 = f x2 ==> x1 = x2
*)

(* Overload on the 1_to_1 condition *)
Overload "ONE_to_ONE" = ``\f. !x1 x2. f x1 = f x2 ==> x1 = x2``;

(*
Many theorem using 1_to_1:
match [] ``ONE_to_ONE f``;

SUM_IMAGE_AS_SUM_SET       |- !s. FINITE s ==> !f. ONE_to_ONE f ==> SIGMA f s = SUM_SET (IMAGE f s)
PROD_IMAGE_AS_PROD_SET     |- !s. FINITE s ==> !f. ONE_to_ONE f ==> SIGMA f s = SUM_SET (IMAGE f s)
IMAGE_ELEMENT_CONDITION    |- !f. ONE_to_ONE f ==> !s e. e IN s <=> f e IN IMAGE f s
*)

(* Note: very hard to use: ONE_ONE g!
Use a trick: for INJ f s t,
define: g x = if x IN s then f x else x
*)

(* Theorem: ONE_to_ONE f <=> !s. INJ f s univ(:'a) *)
(* Proof:
   If part: ONE_to_ONE f ==> !s. INJ f s univ(:'a)
      This is to show: !x y. x IN s /\ y IN s ==> f x = f y ==> x = y
      which is trivially true  by ONE_to_ONE.

   Only-if part: !s. INJ f s univ(:'a) ==> ONE_to_ONE f
      This is to show: !s x y. x IN s /\ y IN s ==> f x = f y ==> x = y
                   ==> f x1 = f x2 ==> x1 = x2
   Let the set s = {x1, x2}. The result follows.
*)
Theorem ONE_to_ONE_iff:
  !f. ONE_to_ONE f <=> !s. INJ f s univ(:'a)
Proof
  rw[INJ_DEF, EQ_IMP_THM] >>
  first_x_assum (qspec_then `{x1; x2}` strip_assume_tac) >>
  fs[]
QED

(* Idea: a trick to make a ONE_to_ONE function from INJ f s s *)

(* Theorem: INJ f s s ==> ONE_to_ONE (\x. if x IN s then f x else x) *)
(* Proof:
   This is to show:
        (if x1 IN s then f x1 else x1) = (if x2 IN s then f x2 else x2)
     ==> x1 = x2
   If x1 IN s, x2 IN s,
          f x1 = f x2 ==> x1 = x2  by INJ_DEF
   If x1 NOTIN s, x2 NOTIN s,
          x1 = x2 ==> x1 = x2 is trivially true.
   If x1 IN s, x2 NOTIN s,
          f x1 IN s                by IN_IMAGE
          so f x1 = x2 is impossible
   If x1 NOTIN s, x2 IN s,
          f x2 IN s                by IN_IMAGE
          so x1 = f x2 is impossible
*)
Theorem INJ_11:
  !f s. INJ f s s ==> ONE_to_ONE (\x. if x IN s then f x else x)
Proof
  rw[] >>
  Cases_on `x1 IN s` >| [
    Cases_on `x2 IN s` >-
    fs[INJ_DEF] >>
    metis_tac[INJ_DEF],
    Cases_on `x2 IN s` >-
    metis_tac[INJ_DEF] >>
    fs[]
  ]
QED

(* Note:
IMAGE_IN                 |- !x s. x IN s ==> !f. f x IN IMAGE f s
IMAGE_ELEMENT_CONDITION  |- !f. ONE_to_ONE f ==> !s e. e IN s <=> f e IN IMAGE f s
no theorem involves:            f x IN IMAGE f s ==> something.
*)

(* ------------------------------------------------------------------------- *)
(* Exclusive function on elements                                            *)
(* ------------------------------------------------------------------------- *)

(* Overload on element exclusive *)
Overload "EXCLUSIVE" = ``\f s. !x. x IN s <=> f x IN (IMAGE f s)``;

(* Theorem: IMAGE f s = s ==> EXCLUSIVE (\x. {x; f x}) (pairs f s) *)
(* Proof:
   If part: x IN pairs f s ==> {x; f x} IN (IMAGE {x; f x} (pairs f s))
      This is true                 by IMAGE_IN
   Only-if {x; f x} IN (IMAGE {x; f x} (pairs f s)) ==> x IN pairs f s
      This is to show:
           {x; f x} = {y; f y} /\ y IN pairs f s ==> x IN pairs f s
      Note y IN s /\ f y <> y      by pairs_element
      to show: x IN s /\ f x <> x  by pairs_element
      Case x = y, f x = f y. This is trivial.
      Case x = f y, f x = y.
           Then x = f (f x) ==> x IN s   by IN_IMAGE, IMAGE f s = s
            and f x = y <> f y = x       by f y <> y.
*)
Theorem pairs_doublet_exclusive:
  !f s. IMAGE f s = s ==> EXCLUSIVE (\x. {x; f x}) (pairs f s)
Proof
  rw[EQ_IMP_THM] >-
  metis_tac[] >>
  fs[EXTENSION, pairs_def] >>
  metis_tac[]
QED

(* Theorem: f involute s ==> EXCLUSIVE (\x. {x; f x}) (pairs f s) *)
(* Proof:
   Note f involute s
    ==> SURJ f s s                             by involute_surj
    ==> IMAGE f s = s                          by IMAGE_SURJ
    ==> EXCLUSIVE (\x. {x; f x}) (pairs f s)   by pairs_doublet_exclusive
*)
Theorem pairs_doublet_exclusive_2:
  !f s. f involute s ==> EXCLUSIVE (\x. {x; f x}) (pairs f s)
Proof
  simp[involute_surj, GSYM IMAGE_SURJ, GSYM pairs_doublet_exclusive]
QED

(*
These idea of exclusive theorems are intended to replace the ONE_to_ONE road block,
especially in PROD_IMAGE_INJ_o. In fact, this does not, as there is no way to tackle:
EXCLUSIVE f (e INSERT s), cannot show f e NOTIN IMAGE f s /\ EXCLUSIVE f s.
*)

(*
In MOD 7,                 s = {1; 2; 3; 4; 5; 6}
f = mod_inv 7,    IMAGE f s = {1; 4; 5; 2; 3; 6}
t = pairs f s,            t =    {2; 3; 4; 5}
g = \x. {x; f x}, IMAGE g t = {{2;4}; {3;5}; {4;2}; {5;3}} = {{2;4}; {3;5}} as set is unordered.
Thus g maps 4 elements to 2 elements, cannot be INJ!
However, I do want to multiply all the numbers, and the result should be equal:
PROD_SET t = PROD_SET { PROD_SET {2;4}, PROD_SET {3;5}}
*)

(* ------------------------------------------------------------------------- *)
(* More on Arithmetic                                                        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: x <= n ==> n - (n - x) = x *)
(* Proof:
     n - (n - x)
   = n + x - n         by SUB_SUB, x <= n
   = x + n - n         by ADD_COMM
   = x                 by arithmetic
*)
Theorem SUB_SUB_ID:
  !n x. x <= n ==> n - (n - x) = x
Proof
  decide_tac
QED

(*
primePowerTheory.prime_power_or_coprime_factors
|- !n. 1 < n ==> (?p k. 0 < k /\ prime p /\ n = p ** k) \/
                  ?a b. n = a * b /\ coprime a b /\ 1 < a /\ 1 < b /\ a < n /\ b < n
prime_iff_no_proper_factor
|- !n. prime n <=> 1 < n /\ !j. 1 < j /\ j < n ==> ~(j divides n)
*)

(* Idea: a non-prime number has at least one proper factor. *)

(* Theorem: 1 < n /\ ~prime n ==> ?a. 1 < a /\ a < n /\ a divides n *)
(* Proof: by prime_iff_no_proper_factor *)
Theorem PROPER_FACTOR:
  !n. 1 < n /\ ~prime n ==> ?a. 1 < a /\ a < n /\ a divides n
Proof
  metis_tac[prime_iff_no_proper_factor]
QED

(* Idea: a non-prime number has a pair of proper factors. *)

(* Theorem: 1 < n /\ ~prime n ==> ?a b. n = a * b /\ 1 < a /\ 1 < b /\ a < n /\ b < n *)
(* Proof:
   Note ?b. 1 < b /\ b < n /\ b divides n      by PROPER_FACTOR, 1 < n
    and ?a. n = a * b                          by divides_def
   also a divides n, so a <= n                 by divides_def, DIVIDES_LE
    But a <> n because b <> 1                  by MULT_EQ_SELF
    and a <> 1 because b <> n                  by MULT_LEFT_1
     so 1 < a amd a < n
*)
Theorem PROPER_FACTOR_PAIR:
  !n. 1 < n /\ ~prime n ==> ?a b. n = a * b /\ 1 < a /\ 1 < b /\ a < n /\ b < n
Proof
  rpt strip_tac >>
  `?b. 1 < b /\ b < n /\ b divides n` by simp[PROPER_FACTOR] >>
  `?a. n = a * b /\ a divides n` by metis_tac[divides_def, MULT_COMM] >>
  `a <= n` by simp[DIVIDES_LE] >>
  `1 < a /\ a < n` by fs[] >>
  metis_tac[]
QED

(* ------------------------------------------------------------------------- *)
(* More on MOD arithmetic                                                    *)
(* ------------------------------------------------------------------------- *)

(* Idea: MOD_PLUS2 has 0 < n, but valid for n = 0 due to MOD_0 *)

(* Theorem: (j + k) MOD n = ((j MOD n) + (k MOD n)) MOD n *)
(* Proof:
   If n = 0, true              by MOD_0
   Otherwise 0 < n, true       by MOD_PLUS2
*)
Theorem MOD_SUM:
  !j k n. (j + k) MOD n = ((j MOD n) + (k MOD n)) MOD n
Proof
  rpt strip_tac >>
  (Cases_on `n = 0` >> simp[])
QED

(* Idea: MOD_TIMES2 has 0 < n, but valid for n = 0 due to MOD_0 *)

(* Theorem: (j * k) MOD n = ((j MOD n) * (k MOD n)) MOD n *)
(* Proof:
   If n = 0, true              by MOD_0
   Otherwise 0 < n, true       by MOD_TIMES2
*)
Theorem MOD_PRODUCT:
  !j k n. (j * k) MOD n = ((j MOD n) * (k MOD n)) MOD n
Proof
  rpt strip_tac >>
  (Cases_on `n = 0` >> simp[])
QED

(* Idea: MOD_MOD has 0 < n, but valid for n = 0 due to MOD_0 *)

(* Theorem: (k MOD n) MOD n = k MOD n *)
(* Proof:
   If n = 0, true              by MOD_0
   Otherwise 0 < n, true       by MOD_MOD
*)
Theorem MOD_OF_MOD:
  !k n. (k MOD n) MOD n = k MOD n
Proof
  rpt strip_tac >>
  (Cases_on `n = 0` >> simp[])
QED

(* Theorem: (a * (b MOD n)) MOD n = (a * b) MOD n *)
(* Proof:
   If n = 0,
        (a * b MOD 0) MOD 0
      = a * b                                  by MOD_0
      = (a * b) MOD 0                          by MOD_0
   If n <> 0,
        (a * b MOD n) MOD n
      = (a MOD n * (b MOD n) MOD n) MOD n      by MOD_TIMES2
      = (a MOD n * b MOD n) MOD n              by MOD_MOD
      = (a * b) MOD n                          by MOD_TIMES2
*)
Theorem TIMES_MOD_MOD:
  !n a b. (a * (b MOD n)) MOD n = (a * b) MOD n
Proof
  rpt strip_tac >>
  `n = 0 \/ 0 < n` by decide_tac >-
  simp[] >>
  rw[MOD_TIMES2, MOD_MOD]
QED

(*
In general, (a - b) MOD n = (a MOD n) - (b MOD n)  is false.
Counter-example: (15 - 13) MOD 7 = 2, but 15 MOD 7 = 1, 13 MOD 7 = 6.
*)

(*
MOD_SUB     |- 0 < n /\ n * q <= m ==> (m - n * q) MOD n = m MOD n
no MOD_ADD  (already in simplifier)
*)

(* Theorem: 0 < n ==> (m + n * q) MOD n = m MOD n *)
(* Proof:
     (m + n * q) MOD n
   = (m MOD n + (n * q) MOD n) MOD n           by MOD_PLUS
   = (m MOD n + 0) MOD n                       by MOD_EQ_0, 0 < n
   = m MOD n                                   by MOD_MOD
*)
Theorem MOD_ADD:
  !m n q. 0 < n ==> (m + n * q) MOD n = m MOD n
Proof
  simp[]
QED

(* Theorem: 0 < m /\ m < 2 * n ==> (m MOD n = 0 <=> m = n) *)
(* Proof:
   If part: m MOD n = 0 ==> m = n
      Note n <> 0              by 0 < m < 2 * n
       and ?k. m = k * n       by MOD_EQ_0_DIVISOR
       ==> k * n < 2 * n       by m < 2 * n
        or     k < 2           by LT_MULT_RCANCEL
        so k = 1, or m = n.

   Only-if part: m = n ==> m MOD n = 0
      True by DIVMOD_ID
*)
Theorem MOD_EQ_0_ID:
  !m n. 0 < m /\ m < 2 * n ==> (m MOD n = 0 <=> m = n)
Proof
  rw[EQ_IMP_THM] >>
  `?k. m = k * n` by simp[GSYM MOD_EQ_0_DIVISOR] >>
  fs[]
QED

(* Theorem: 0 < x + y /\ x < n /\ y < n /\ (x + y) MOD n = 0 ==> x + y = n *)
(* Proof:
   Note x + y < 2 * n          by LT_MONO_ADD2
     so x + y = n              by MOD_EQ_0_ID
*)
Theorem MOD_ADD_EQ_0_ID:
  !n x y. 0 < x + y /\ x < n /\ y < n /\ (x + y) MOD n = 0 ==> x + y = n
Proof
  rpt strip_tac >>
  `x + y < 2 * n` by decide_tac >>
  rfs[MOD_EQ_0_ID]
QED

(* Idea:  ((-x) * y) MOD n = (- (x * y)) MOD n *)

(* Theorem: x <= n ==> ((n - x) * y) MOD n = (n - (x * y) MOD n) MOD n *)
(* Proof:
   If n = 0,
      LHS = (0 * y) MOD 0 = 0      by MOD_0
      RHS = 0 MOD 0 = 0 = LHS      by MOD_0

   Otherwise 0 < n.
   If x = n,
      LHS = (0 * y) MOD n = 0      by ZERO_MOD, 0 < n
      RHS = (n - 0) MOD n          by MOD_EQ_0, 0 < n
          = 0 = LHS                by DIVMOD_ID

   Otherwise, x < n.
   If (x * y) MOD n = 0,
      RHS = (n - 0) MOD n = 0      by DIVMOD_ID
      Note (n - x) * y = n * y - x * y
                                   by RIGHT_SUB_DISTRIB
       and n divides (n * y)       by factor_divides
       and n divides (x * y)       by DIVIDES_MOD_0
       ==> n divides ((n - x) * y) by DIVIDES_SUB
        or ((n - x) * y) MOD n = 0 by DIVIDES_MOD_0
      Thus LHS = RHS

   Otherwise, (x * y) MOD n <> 0.
      Thus n - (x * y) MOD n < n   by (x * y) MOD n <> 0
           (n - (x * y) MOD n) MOD n
         = n - (x * y) MOD n       by LESS_MOD
      and the goal becomes:
          ((n - x) * y) MOD n = n - (x * y) MOD n

   Let p = ((n - x) * y) MOD n,
       q = (x * y) MOD n.
   Note p < n and q < n            by MOD_LESS
     so p = n - q <=> p + q = n    by SUB_LEFT_EQ
   Need to show: p + q = n.
       (p + q) MOD n
     = ((n - x) * y + x * y) MOD n by MOD_PLUS
     = ((n - x + x) * y) MOD n     by RIGHT_ADD_DISTRIB
     = (n * y) MOD n               by arithmetic, x <= n
     = 0                           by MOD_EQ_0, 0 < n

   But 0 < p + q                   by q <> 0
    so p + q = n                   by MOD_ADD_EQ_0_ID
*)
Theorem MOD_NEG_MULT:
  !n x y. x <= n ==> ((n - x) * y) MOD n = (n - (x * y) MOD n) MOD n
Proof
  rpt strip_tac >>
  `n = 0 \/ 0 < n` by decide_tac >-
  simp[] >>
  `x = n \/ x < n` by decide_tac >-
  simp[] >>
  Cases_on `(x * y) MOD n = 0` >| [
    `(n - x) * y = n * y - x * y` by decide_tac >>
    `n divides (n * y)` by simp[factor_divides] >>
    `n divides (x * y)` by simp[DIVIDES_MOD_0] >>
    `((n - x) * y) MOD n = 0` by metis_tac[DIVIDES_SUB, DIVIDES_MOD_0] >>
    simp[],
    `((n - x) * y) MOD n = n - (x * y) MOD n` suffices_by simp[] >>
    qabbrev_tac `p = ((n - x) * y) MOD n` >>
    qabbrev_tac `q = (x * y) MOD n` >>
    `p < n /\ q < n` by simp[Abbr`p`, Abbr`q`] >>
    `p + q = n` suffices_by fs[] >>
    `0 < p + q` by decide_tac >>
    `(p + q) MOD n = ((n - x) * y + x * y) MOD n` by simp[MOD_PLUS, Abbr`p`, Abbr`q`] >>
    `_ = ((n - x + x) * y) MOD n` by simp[RIGHT_ADD_DISTRIB] >>
    `_ = (n * y) MOD n` by simp[] >>
    `_ = 0` by simp[MOD_EQ_0] >>
    rfs[MOD_ADD_EQ_0_ID]
  ]
QED

(* Idea: (-x) MOD n = - (x MOD n) *)

(* Theorem: 0 < x /\ x < n ==> (n - x) MOD n = n - (x MOD n) *)
(* Proof:
   Note 0 < n                            by inequality
    and ((n - x) MOD n + x) MOD n = 0    by MOD_ADD_INV
    but (n - x) MOD n < n                by MOD_LESS
    and             x < n                by given
     so 0 < (n - x) MOD n + x            by 0 < x
   Thus (n - x) MOD n + x = n            by MOD_ADD_EQ_0_ID
     or     (n - x) MOD n = n - x        by arithmetic, x < n
*)
Theorem MOD_NEG_SUB:
  !n x. 0 < x /\ x < n ==> (n - x) MOD n = n - (x MOD n)
Proof
  rpt strip_tac >>
  `0 < n /\ 0 < (n - x) MOD n + x` by decide_tac >>
  `(n - x) MOD n < n` by simp[] >>
  `((n - x) MOD n + x) MOD n = 0` by simp[MOD_ADD_INV] >>
  simp[MOD_ADD_EQ_0_ID]
QED

(* Idea: - (- x MOD n) = x MOD n *)

(* Theorem: 0 < n /\ 0 < x MOD n ==> n - (n - x MOD n) MOD n = x MOD n *)
(* Proof:
   Note x MOD n < n                by MOD_LESS
     n - (n - x MOD n) MOD n
   = n - (n - (x MOD n) MOD n)     by MOD_NEG_SUB, 0 < x MOD n
   = n - (n - x MOD n)             by MOD_MOD
   = x MOD n                       by SUB_SUB_ID
*)
Theorem MOD_NEG_NEG:
  !n x. 0 < n /\ 0 < x MOD n ==> n - (n - x MOD n) MOD n = x MOD n
Proof
  rpt strip_tac >>
  `x MOD n < n` by simp[] >>
  ` n - (n - x MOD n) MOD n = n - (n - (x MOD n) MOD n)` by simp[MOD_NEG_SUB] >>
  `_ = n - (n - x MOD n)` by simp[] >>
  `_ = x MOD n` by simp[] >>
  fs[]
QED

(* Idea: for prime n, the only solutions of (x ** 2) MOD n = 1 are x = +1 or -1. *)

(* Theorem: prime n ==> ((x ** 2) MOD n = 1 <=> (x MOD n = 1 \/ x MOD n = n - 1)) *)
(* Proof:
   Note 0 < n and 1 < n                        by PRIME_POS, ONE_LT_PRIME

   If part: (x ** 2) MOD n = 1 ==> (x MOD n = 1 \/ x MOD n = n - 1)

       (x ** 2) MOD n = 1
   <=> (x ** 2) MOD n = 1 MOD n                by ONE_MOD, 1 < n
   <=> (x ** 2 - 1) MOD n = 0                  by MOD_EQ_DIFF, 0 < n
   <=> (x ** 2 - 1 ** 2) MOD n = 0             by arithmetic
   <=> (x - 1) * (x + 1) MOD n = 0             by difference_of_squares
   <=>           (x - 1) MOD n = 0 \/
                 (x + 1) MOD n = 0             by EUCLID_LEMMA, prime n

   For (x - 1) MOD n = 0,
       Note x <> 0 as (0 ** 2) MOD n = 0       by SQ_0, ZERO_MOD
         so 1 <= x                             by inequality
            x MOD n = 1 MOD n                  by MOD_EQ, 1 <= x
            x MOD n = 1                        by ONE_MOD

   For (x + 1) MOD n = 0
      Let z = x MOD n,
      Then (z + 1) MOD n = 0                   by MOD_PLUS, OND_MOD, 0 < n
      But z < n, 0 < z + 1                     by MOD_LESS
      ==> z + 1 = n                            by MOD_ADD_EQ_0_ID
       or     z = n - 1                        by arithmetic, 1 < n

   Only-if part: (x MOD n = 1 \/ x MOD n = n - 1) ==> (x ** 2) MOD n = 1
   Note 0 < n and 1 < n                        by PRIME_POS, ONE_LT_PRIME
   Note   (x ** 2) MOD n
        = (x * x) MOD n                        by EXP_2
        = ((x MOD n) * (x MOD n)) MOD n        by MOD_TIMES2, 0 < n
   If x MOD n = 1,
          (x ** 2) MOD n = 1 MOD n = 1         by ONE_MOD, 1 < n
   If x MOD n = n - 1,
          (x ** 2) MOD n
        = (x * x) MOD n                        by EXP_2
        = ((x MOD n) * (x MOD n)) MOD n        by MOD_TIMES2, 0 < n
        = ((n - 1) * (n - 1)) MOD n            by x MOD n = n - 1
        = ((n - 1) ** 2) MOD n                 by EXP_2
        = (n ** 2 + 1 ** 2 - 2 * n * 1) MOD n  by binomial_sub, 1 <= n
        = (1 + n * n - 2 * n) MOD n            by EXP_2
        = (1 + (n * n - 2 * n)) MOD n          by LESS_EQ_ADD_SUB, 2 * n <= n * n
        = (1 + (n - 2) * n) MOD n              by RIGHT_SUB_DISTRIB
        = 1 MOD n                              by MOD_ADD
        = 1                                    by ONE_MOD, 1 < n
*)
Theorem MOD_SQ_EQ_1_prime:
  !n x. prime n ==> ((x ** 2) MOD n = 1 <=> (x MOD n = 1 \/ x MOD n = n - 1))
Proof
  rpt strip_tac >>
  `0 < n /\ 1 < n` by simp[PRIME_POS, ONE_LT_PRIME] >>
  rw[EQ_IMP_THM] >| [
    `(x ** 2 - 1 ** 2) MOD n = 0` by simp[MOD_EQ_DIFF, ONE_MOD, EXP_2] >>
    `((x - 1) * (x + 1)) MOD n = 0` by fs[difference_of_squares] >>
    `(x - 1) MOD n = 0 \/ (x + 1) MOD n = 0` by simp[GSYM EUCLID_LEMMA] >| [
      `x <> 0` by metis_tac[SQ_0, ZERO_MOD, ONE_NOT_ZERO] >>
      `1 <= x` by decide_tac >>
      metis_tac[MOD_EQ, ONE_MOD],
      qabbrev_tac `z = x MOD n` >>
      `(z + 1) MOD n = 0` by simp[MOD_PLUS, Abbr`z`] >>
      `z < n` by simp[MOD_LESS, Abbr`z`] >>
      `z + 1 = n` by fs[MOD_ADD_EQ_0_ID] >>
      decide_tac
    ],
    `x ** 2 MOD n = ((x MOD n) * (x MOD n)) MOD n` by simp[GSYM EXP_2, MOD_TIMES2] >>
    fs[],
    `2 * n <= n * n` by simp[] >>
    `x ** 2 MOD n = ((x MOD n) * (x MOD n)) MOD n` by simp[GSYM EXP_2, MOD_TIMES2] >>
    `_ = ((n - 1) ** 2) MOD n` by simp[GSYM EXP_2] >>
    `_ = (1 + n ** 2 - 2 * n) MOD n` by simp[binomial_sub] >>
    `_ = (1 + n * n - 2 * n) MOD n` by simp[GSYM EXP_2] >>
    `_ = (1 + (n * n - 2 * n)) MOD n` by simp[LESS_EQ_ADD_SUB] >>
    `_ = (1 + (n - 2) * n) MOD n` by simp[RIGHT_SUB_DISTRIB] >>
    simp[]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* More for helperSet                                                        *)
(* ------------------------------------------------------------------------- *)

(* Theorem: equiv_class R s x SUBSET s *)
(* Proof:
   By equiv_class_element,
      y IN equiv_class R s x <=> y IN s /\ R x y
   Thus equiv_class R s x SUBSET s by SUBSET_DEF
*)
Theorem equiv_class_subset:
  !R s x. equiv_class R s x SUBSET s
Proof
  simp[equiv_class_element, SUBSET_DEF]
QED

(* Theorem: FINITE s ==> FINITE (equiv_class R s x) *)
(* Proof:
   Note equiv_class R s x SUBSET s by equiv_class_subset
   Thus FINITE (equiv_class R s x) by SUBSET_FINITE
*)
Theorem equiv_class_finite:
  !R s x. FINITE s ==> FINITE (equiv_class R s x)
Proof
  metis_tac[equiv_class_subset, SUBSET_FINITE]
QED

(* Theorem: FINITE s ==> EVERY_FINITE (equiv_pairs f s) *)
(* Proof:
   This is to show:
      FINITE s /\ x IN equiv_pairs f s ==> FINITE x
   Note x IN equiv_pairs f s <=>
        ?y. y IN pairs f s /\
            x = equiv_class (pair_by f) s y    by equiv_pairs_element
   Thus FINITE x                               by equiv_class_finite
*)
Theorem equiv_pairs_every_finite:
  !f s. FINITE s ==> EVERY_FINITE (equiv_pairs f s)
Proof
  metis_tac[equiv_pairs_element, equiv_class_finite]
QED

(* Theorem: (x INSERT s) UNION t = s UNION (x INSERT t) *)
(* Proof:
     (x INSERT s) UNION t
   = x INSERT (s UNION t)      by INSERT_UNION_EQ
   = x INSERT (t UNION s)      by UNION_COMM
   = (x INSERT t) UNION s      by INSERT_UNION_EQ
   = s UNION (x INSERT t)      by UNION_COMM
*)
Theorem INSERT_UNION_SWAP:
  !x s t. (x INSERT s) UNION t = s UNION (x INSERT t)
Proof
  metis_tac[INSERT_UNION_EQ, UNION_COMM]
QED

(* Theorem: e NOTIN s /\ PAIR_DISJOINT (e INSERT s) ==> DISJOINT e (BIGUNION s) *)
(* Proof:
   Note !t. t IN s ==> DISJOINT e t            by IN_INSERT
    ==> DISJOINT t (BIGUNION s)                by DISJOINT_BIGUNION
*)
Theorem PAIR_DISJOINT_INSERT:
  !e s. e NOTIN s /\ PAIR_DISJOINT (e INSERT s) ==> DISJOINT e (BIGUNION s)
Proof
  rpt strip_tac >>
  `!t. t IN s ==> DISJOINT e t` by metis_tac[IN_INSERT] >>
  simp[DISJOINT_BIGUNION]
QED

(* Idea: a disjoint union splits a set *)

(* Theorem: DISJOINT s t ==> (s UNION t) =|= s # t *)
(* Proof: by notation *)
Theorem DISJOINT_UNION_SPLIT:
  !s t. DISJOINT s t ==> (s UNION t) =|= s # t
Proof
  simp[]
QED

(* Theorem: a <> b ==> PROD_SET {a; b} = a * b *)
(* Proof:
   Note FINITE {b}                 by FINITE_SING
    and a NOTIN {b}                by IN_SING

     PROD_SET {a; b}
   = PROD_SET (a INSERT {b})       by notation
   = a * PROD_SET {b}              by PROD_SET_INSERT
   = a * b                         by PROD_SET_SING
*)
Theorem PROD_SET_doublet:
  !a b. a <> b ==> PROD_SET {a; b} = a * b
Proof
  simp[PROD_SET_INSERT, PROD_SET_SING]
QED

(* pred_setTheory has
SUM_IMAGE_UNION
|- !f s t. FINITE s /\ FINITE t ==>
           SIGMA f (s UNION t) = SIGMA f s + SIGMA f t - SIGMA f (s INTER t)

SUM_IMAGE_UNION_EQN
|- !s t. FINITE s /\ FINITE t ==>
   !f. SIGMA f (s UNION t) + SIGMA f (s INTER t) = SIGMA f s + SIGMA f t

but no PROD_IMAGE_UNION_EQN

Also:
EVAL ``SIGMA I {2; 4}``; = 6
EVAL ``PI I {2; 4}``; = PI I {2; 4}
EVAL ``SIGMA SUC {2; 4}``; = 8
EVAL ``PI SUC {2; 4}``; = PI SUC {2; 4}

Problem with s evalution: no CHOICE s, no REST s!
So, how does EVAL get (count 5), SUM_SET {3;5}, FINITE {3;5} ?

EVAL ``SUM_SET {0;1;2}``; = 3
EVAL ``SUM_SET {x | x < 3}``; = SIGMA I {x | x < 3}

That is, SUM_SET can be evaluated when some theorems are included in [simp], the simplifier!

IN_COUNT[simp]   |- m IN count n <=> m < n
COUNT_ZERO[simp] |- count 0 = {}
*)


(* Theorem: FINITE s /\ FINITE t ==> !f. PI f (s UNION t) * PI f (s INTER t) = PI f s * PI f t *)
(* Proof:
   By FINITE induction on s.
   Base: FINITE t ==> PI f ({} UNION t) * PI f ({} INTER t) = PI f {} * PI f t
        PI f ({} UNION t) * PI f ({} INTER t)
      = PI f t * PI f {}                       by UNION_EMPTY, INTER_EMPTY
      = PI f {} * PI f t                       by MULT_COMM
   Step: (!t. FINITE t ==> !f. PI f (s UNION t) * PI f (s INTER t) = PI f s * PI f t) /\
         e NOTIN s /\ FINITE t
     ==> PI f ((e INSERT s) UNION t) * PI f ((e INSERT s) INTER t) = PI f (e INSERT s) * PI f t
      If e IN t,
         PI f ((e INSERT s) UNION t) * PI f ((e INSERT s) INTER t)
       = PI f (s UNION t) * PI f (e INSERT (s INTER t))        by INSERT_UNION, INSERT_INTER
       = PI f (s UNION t) * ((f e) * PI f (s INTER t))         by PROD_IMAGE_INSERT, IN_INTER
       = (f e) * (PI f s * PI f t)                             by induction hypothesis
       = ((f e) * PI f s) * PI f t                             by MULT_ASSOC
       = PI f (e INSERT s) * PI f t                            by PROD_IMAGE_INSERT
     If e NOTIN t,
         PI f ((e INSERT s) UNION t) * PI f ((e INSERT s) INTER t)
       = PI f (e INSERT (s UNION t)) * PI f (s INTER t)        by INSERT_UNION, INSERT_INTER
       = (f e * PI f (s UNION t)) * PI f (s INTER t)           by PROD_IMAGE_INSERT, IN_UNION
       = (f e) * PI f s * PI f t                               by induction hypothesis
       = PI f (e INSERT s) * PI f t                            by PROD_IMAGE_INSERT
*)
Theorem PROD_IMAGE_UNION_EQN:
  !s t. FINITE s /\ FINITE t ==> !f. PI f (s UNION t) * PI f (s INTER t) = PI f s * PI f t
Proof
  `!s. FINITE s ==> !t. FINITE t ==> !f. PI f (s UNION t) * PI f (s INTER t) = PI f s * PI f t` suffices_by rw[] >>
  Induct_on `FINITE` >>
  rpt strip_tac >-
  simp[] >>
  Cases_on `e IN t` >| [
    `e NOTIN (s INTER t)` by simp[] >>
    `PI f ((e INSERT s) UNION t) * PI f ((e INSERT s) INTER t) = PI f (s UNION t) * PI f (e INSERT (s INTER t))` by simp[INSERT_UNION, INSERT_INTER] >>
    `_ = f e * (PI f (s UNION t) * PI f (s INTER t))` by simp[PROD_IMAGE_INSERT] >>
    `_ = f e * PI f s * PI f t` by metis_tac[MULT_ASSOC] >>
    simp[PROD_IMAGE_INSERT],
    `e NOTIN (s UNION t)` by simp[] >>
    `PI f ((e INSERT s) UNION t) * PI f ((e INSERT s) INTER t) = PI f (e INSERT (s UNION t)) * PI f (s INTER t)` by simp[INSERT_UNION, INSERT_INTER] >>
    `_ = f e * (PI f (s UNION t) * PI f (s INTER t))` by simp[PROD_IMAGE_INSERT] >>
    `_ = f e * PI f s * PI f t` by metis_tac[MULT_ASSOC] >>
    simp[PROD_IMAGE_INSERT]
  ]
QED

(* Idea: product of PROD_SETs is the product of PROD_SET unions and intersections. *)

(* Theorem: FINITE s /\ FINITE t ==> PROD_SET (s UNION t) * PROD_SET (s INTER t) = PROD_SET s * PROD_SET t *)
(* Proof:
     PROD_SET s * PROD_SET t
   = PI I s * PI I t                               by PROD_SET_DEF
   = PI I (s UNION t) * PI I (s INTER t)           by PROD_IMAGE_UNION_EQN
   = PROD_SET (s UNION t) * PROD_SET (s INTER t)   by PROD_SET_DEF
*)
Theorem PROD_SET_UNION:
  !s t. FINITE s /\ FINITE t ==>
         PROD_SET (s UNION t) * PROD_SET (s INTER t) = PROD_SET s * PROD_SET t
Proof
  metis_tac[PROD_SET_DEF, PROD_IMAGE_UNION_EQN]
QED

(* Theorem: SET_MULTIPLICATIVE (PI f) *)
(* Proof:
   Since PI f {} = 1                           by PROD_IMAGE_EMPTY
     and !s t. FINITE s /\ FINITE t
     ==> PI f (s UNION t) * PI f (s INTER t)
       = PI f s * PI f t                       by PROD_IMAGE_UNION_EQN
   Hence SET_MULTIPLICATIVE (PI f)             by notation
*)
Theorem set_multiplicative_pi:
  !f. SET_MULTIPLICATIVE (PI f)
Proof
  rw[PROD_IMAGE_EMPTY, PROD_IMAGE_UNION_EQN]
QED

(* Theorem: FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==> !f. PI f (BIGUNION P) = PI (PI f) P *)
(* Proof: by disjoint_bigunion_mult_fun, set_multiplicative_pi *)
Theorem disjoint_bigunion_pi:
  !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==> !f. PI f (BIGUNION P) = PI (PI f) P
Proof
  rw[disjoint_bigunion_mult_fun, set_multiplicative_pi]
QED

(* Theorem: R equiv_on s /\ FINITE s ==> !f. PI f s = PI (PI f) (partition R s) *)
(* Proof: by set_mult_fun_by_partition, set_multiplicative_pi *)
Theorem set_pi_by_partition:
  !R s. R equiv_on s /\ FINITE s ==> !f. PI f s = PI (PI f) (partition R s)
Proof
  rw[set_mult_fun_by_partition, set_multiplicative_pi]
QED

(* Theorem: FINITE s ==> !f. (!x y. (f x = f y) ==> (x = y)) ==> (PI f s = PROD_SET (IMAGE f s)) *)
(* Proof:
   By FINITE induction on s.
   Base case: PI f {} = PROD_SET (IMAGE f {})
        PI f {}
      = 1                          by PROD_IMAGE_THM
      = PROD_SET {}                by PROD_SET_EMPTY
      = PROD_SET (IMAGE f {})      by IMAGE_EMPTY
   Step case: !f. (!x y. (f x = f y) ==> (x = y)) ==> PI f s = PROD_SET (IMAGE f s) ==>
              e NOTIN s ==> PI f (e INSERT s) = PROD_SET (IMAGE f (e INSERT s))
      Note FINITE s ==> FINITE (IMAGE f s)         by IMAGE_FINITE
       and e NOTIN s ==> f e NOTIN (IMAGE f s)     by the injective property
        PI f (e INSERT s)
      = f e * PI f (s DELETE e)                    by PROD_IMAGE_THM
      = f e * PI f s                               by DELETE_NON_ELEMENT
      = f e * PROD_SET (IMAGE f s)                 by induction hypothesis
      = f e * PROD_SET ((IMAGE f s) DELETE (f e))  by DELETE_NON_ELEMENT, f e NOTIN f s
      = PROD_SET (f e INSERT IMAGE f s)            by PROD_SET_THM
      = PROD_SET (IMAGE f (e INSERT s))            by IMAGE_INSERT
*)
Theorem PROD_IMAGE_AS_PROD_SET:
  !s. FINITE s ==> !f. (!x y. (f x = f y) ==> (x = y)) ==> (PI f s = PROD_SET (IMAGE f s))
Proof
  Induct_on `FINITE` >>
  rpt strip_tac >-
  simp[PROD_IMAGE_THM, PROD_SET_EMPTY] >>
  `f e NOTIN (IMAGE f s)` by metis_tac[IN_IMAGE] >>
  `PI f (e INSERT s) = f e * PI f (s DELETE e)` by simp[PROD_IMAGE_THM] >>
  `_ = f e * PI f s` by metis_tac[DELETE_NON_ELEMENT] >>
  `_ = f e * PROD_SET (IMAGE f s)` by fs[] >>
  `_ = f e * PROD_SET ((IMAGE f s) DELETE (f e))` by metis_tac[DELETE_NON_ELEMENT] >>
  simp[PROD_SET_THM]
QED

(* Both theorems:

SUM_IMAGE_AS_SUM_SET    |- !s. FINITE s ==> !f. ONE_to_ONE f ==> SIGMA f s = SUM_SET (IMAGE f s)
PROD_IMAGE_AS_PROD_SET  |- !s. FINITE s ==> !f. ONE_to_ONE f ==> PI f s = PROD_SET (IMAGE f s)

SUM_SET_DEF  |- SUM_SET = SIGMA I
PROD_SET_DEF |- PROD_SET = PI I

SIGMA is overload for SUM_IMAGE, SUM_IMAGE_DEF     |- !f s. SIGMA f s = ITSET (\e acc. f e + acc) s 0
PI    is overload for PROD_IMAGE, PROD_IMAGE_DEF   |- !f s. PI f s = ITSET (\e acc. f e * acc) s 1

That is, SIGMA and PI are iterative functions.
When we have, PI f s = PROD_SET (IMAGE f s)
for a FINITE set s with, say, 5 elements,
PI will multiply the 5 function values f x, x IN s.
These function values (f x) may or may not be distinct.
If the values are not distinct, IMAGE f s will remove the duplicates,
thus making PROD_SET (which just multiplies all elements in a set) different from PI.

That's why the ONE_to_ONE feature of f is important: it keeps the values distinct.
In fact, ONE_to_ONE_iff  |- !f. ONE_to_ONE f <=> !s. INJ f s univ(:'a)
*)


(* Idea: SUM_SET of a disjoint union is the sum of SUM_SETs. *)

(* Theorem: FINITE s /\ FINITE t /\ DISJOINT s t ==> SUM_SET (s UNION t) = SUM_SET s + SUM_SET t *)
(* Proof:
     SUM_SET s + SUM_SET t
   = SIGMA I s + SIGMA I t                     by SUM_SET_DEF
   = SIGMA I (s UNION t) + SIGMA I (s INTER t) by SUM_IMAGE_UNION_EQN
   = SIGMA I (s UNION t) + SIGMA I {}          by DISJOINT_DEF
   = SIGMA I (s UNION t)                       by SUM_IMAGE_EMPTY
   = SUM_SET (s UNION t)                       by SUM_SET_DEF
*)
Theorem SUM_SET_UNION_DISJOINT:
  !s t. FINITE s /\ FINITE t /\ DISJOINT s t ==> SUM_SET (s UNION t) = SUM_SET s + SUM_SET t
Proof
  metis_tac[DISJOINT_DEF, SUM_SET_DEF, SUM_IMAGE_UNION_EQN, SUM_IMAGE_EMPTY, ADD_0]
QED

(* Theorem: FINITE s ==> !u v. s =|= u # v ==> (SUM_SET s = SUM_SET u + SUM_SET v) *)
(* Proof:
   This is to show:
      FINITE s /\ s = u UNION v /\ DISJOINT u v ==> SUM_SET s = SUM_SET u + SUM_SET v
   Note FINITE u /\ FINITE v       by FINITE_UNION
   Thus the result follows         by SUM_SET_UNION_DISJOINT
*)
Theorem SUM_SET_SUM_BY_SPLIT:
  !s. FINITE s ==> !u v. s =|= u # v ==> (SUM_SET s = SUM_SET u + SUM_SET v)
Proof
  rpt strip_tac >>
  fs[SUM_SET_UNION_DISJOINT]
QED

(* Theorem alias *)
Theorem PROD_SET_PRODUCT_BY_SPLIT = helperSetTheory.PROD_SET_PRODUCT_BY_PARTITION;

(* Idea: PROD_SET of a disjoint union is the product of PROD_SETs. *)

(* Theorem: FINITE s /\ FINITE t /\ DISJOINT s t ==> PROD_SET (s UNION t) = PROD_SET s * PROD_SET t *)
(* Proof:
   Let u = s UNITON t.
   Then u is split by s and t: u =|= s # t     by DISJOINT_UNION_SPLIT
   Thus PROD_SET u = PROD_SET s * PROD_SET t   by PROD_SET_PRODUCT_BY_SPLIT
*)
Theorem PROD_SET_UNION_DISJOINT:
  !s t. FINITE s /\ FINITE t /\ DISJOINT s t ==> PROD_SET (s UNION t) = PROD_SET s * PROD_SET t
Proof
  rpt strip_tac >>
  `FINITE (s UNION t)` by simp[] >>
  simp[PROD_SET_PRODUCT_BY_SPLIT]
QED

(* Idea: for a set of sets P, SUM_SET of its BIGUNION is the product of SUM_SET of each set. *)

(* Theorem: FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==> SUM_SET (BIGUNION P) = SIGMA SUM_SET P *)
(* Proof:
   By induciton on FINITE set P.
   Base: SUM_SET (BIGUNION {}) = SIGMA SUM_SET {}
        SUM_SET (BIGUNION {})
      = SUM_SET {}                 by BIGUNION_EMPTY
      = 0                          by SUM_SET_EMPTY
      = SIGMA SUM_SET {}           by SUM_IMAGE_EMPTY

   Step: EVERY_FINITE P /\ PAIR_DISJOINT P ==> SUM_SET (BIGUNION P) = SIGMA SUM_SET P
     ==> e NOTIN P /\ EVERY_FINITE (e INSERT P) /\ PAIR_DISJOINT (e INSERT P) ==>
         SUM_SET (BIGUNION (e INSERT P)) = SIGMA SUM_SET (e INSERT P)

      Note FINITE e /\ FINITE (BIGUNION P) /\
           EVERY_FINITE P                      by FINITE_BIGUNION, IN_INSERT
       and DISJOINT e (BIGUNION P) /\
           PAIR_DISJOINT P                     by DISJOINT_BIGUNION, IN_INSERT

        SUM_SET (BIGUNION (e INSERT P))
      = SUM_SET (e UNION (BIGUNION P))         by BIGUNION_INSERT
      = (SUM_SET e) + (SUM_SET (BIGUNION P))   by SUM_SET_UNION_DISJOINT
      = (SUM_SET e) + SIGMA SUM_SET P          by induction hypothesis
      = SIGMA SUM_SET (e INSERT P)             by SUM_IMAGE_INSERT
*)
Theorem SUM_SET_BIGUNION:
  !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==> SUM_SET (BIGUNION P) = SIGMA SUM_SET P
Proof
  Induct_on `FINITE` >>
  rpt strip_tac >-
  simp[SUM_SET_EMPTY, SUM_IMAGE_EMPTY] >>
  `FINITE e /\ FINITE (BIGUNION P) /\ EVERY_FINITE P` by simp[] >>
  `DISJOINT e (BIGUNION P) /\ PAIR_DISJOINT P` by metis_tac[DISJOINT_BIGUNION, IN_INSERT] >>
  `SUM_SET (BIGUNION (e INSERT P)) = SUM_SET (e UNION (BIGUNION P))` by simp[] >>
  `_ = (SUM_SET e) + (SUM_SET (BIGUNION P))` by simp[SUM_SET_UNION_DISJOINT] >>
  `_ = (SUM_SET e) + SIGMA SUM_SET P` by fs[] >>
  simp[SUM_IMAGE_INSERT]
QED

(* Idea: for a set of sets P, PROD_SET of its BIGUNION is the product of PROD_SET of each set. *)

(* Theorem: FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==> PROD_SET (BIGUNION P) = PI PROD_SET P *)
(* Proof:
   By induciton on FINITE set P.
   Base: PROD_SET (BIGUNION {}) = PI PROD_SET {}
        PROD_SET (BIGUNION {})
      = PROD_SET {}                by BIGUNION_EMPTY
      = 1                          by PROD_SET_EMPTY
      = PI PROD_SET {}             by PROD_IMAGE_EMPTY

   Step: EVERY_FINITE P /\ PAIR_DISJOINT P ==> PROD_SET (BIGUNION P) = PI PROD_SET P
     ==> e NOTIN P /\ EVERY_FINITE (e INSERT P) /\ PAIR_DISJOINT (e INSERT P) ==>
         PROD_SET (BIGUNION (e INSERT P)) = PI PROD_SET (e INSERT P)

      Note FINITE e /\ FINITE (BIGUNION P) /\
           EVERY_FINITE P                      by FINITE_BIGUNION, IN_INSERT
       and DISJOINT e (BIGUNION P) /\
           PAIR_DISJOINT P                     by DISJOINT_BIGUNION, IN_INSERT

        PROD_SET (BIGUNION (e INSERT P))
      = PROD_SET (e UNION (BIGUNION P))        by BIGUNION_INSERT
      = (PROD_SET e) * (PROD_SET (BIGUNION P)) by PROD_SET_UNION_DISJOINT
      = (PROD_SET e) * PI PROD_SET P           by induction hypothesis
      = PI PROD_SET (e INSERT P)               by PROD_IMAGE_INSERT
*)
Theorem PROD_SET_BIGUNION:
  !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==> PROD_SET (BIGUNION P) = PI PROD_SET P
Proof
  Induct_on `FINITE` >>
  rpt strip_tac >-
  simp[PROD_SET_EMPTY, PROD_IMAGE_EMPTY] >>
  `FINITE e /\ FINITE (BIGUNION P) /\ EVERY_FINITE P` by simp[] >>
  `DISJOINT e (BIGUNION P) /\ PAIR_DISJOINT P` by metis_tac[DISJOINT_BIGUNION, IN_INSERT] >>
  `PROD_SET (BIGUNION (e INSERT P)) = PROD_SET (e UNION (BIGUNION P))` by simp[] >>
  `_ = (PROD_SET e) * (PROD_SET (BIGUNION P))` by simp[PROD_SET_UNION_DISJOINT] >>
  `_ = (PROD_SET e) * PI PROD_SET P` by fs[] >>
  simp[PROD_IMAGE_INSERT]
QED

(* Theorem: FINITE s /\ INJ g s univ(:'a) ==> PI (f o g) s = PI f (IMAGE g s) *)
(* Proof:
   By FINITE induction on s.
   Base: PI (f o g) {} = PI f (IMAGE g {})
        PI f (IMAGE g {})
      = PI f {}                    by IMAGE_EMPTY
      = 1                          by PROD_IMAGE_EMPTY
      = PI (f o g) {}              by PROD_IMAGE_EMPTY
   Step: !f g t. INJ g s univ(:'a) ==> PI (f o g) s = PI f (IMAGE g s)
     ==> e NOTIN s /\ INJ g (e INSERT s) univ(:'a)
     ==> PI (f o g) (e INSERT s) = PI f (IMAGE g (e INSERT s))

     Note INJ g s /\ g e NOTIN (IMAGE g s)     by INJ_INSERT, IN_IMAGE

        PI f (IMAGE g (e INSERT s))
      = PI f (g e INSERT IMAGE g s)            by IMAGE_INSERT
      = f (g e) * PI f (IMAGE g s)             by PROD_IMAGE_INSERT, g e NOTIN IMAGE g s
      = f (g e) * PI (f o g) s                 by induction hypothesis
      = (f o g) e * PI (f o g) s               by FUN compose
      = PI (f o g) (e INSERT s)                by PROD_IMAGE_INSERT, e NOTIN s
*)
Theorem PROD_IMAGE_INJ_o:
  !s f g. FINITE s /\ INJ g s univ(:'a) ==> PI (f o g) s = PI f (IMAGE g s)
Proof
  Induct_on `FINITE` >>
  rpt strip_tac >-
  simp[PROD_IMAGE_EMPTY] >>
  `INJ g s univ(:'a) /\ g e NOTIN (IMAGE g s)` by metis_tac[INJ_INSERT, IN_IMAGE] >>
  simp[PROD_IMAGE_INSERT]
QED

(*

PROD_IMAGE_INJ_o   |- !s f g. FINITE s /\ INJ g s univ(:'a) ==> PI (f o g) s = PI f (IMAGE g s)
INJ_COMPOSE        |- !f g s t u. INJ f s t /\ INJ g t u ==> INJ (g o f) s u

A conceptual proof but with more requirements,
  PI (f o g) s
= PI (IMAGE (f o g) s)       by PROD_IMAGE_AS_PROD_SET, need ONE_to_ONE (f o g)
= PI (IMAGE f (IMAGE g s))   by IMAGE_o
= PI f (IMAGE g s)           by PROD_IMAGE_AS_PROD_SETm, need ONE_to_ONE f

*)

(* Note: INJ g s univ(:'a) is needed for ONE_to_ONE g,
         to keep FINITE s and (IMAGE g s) same number of elements. *)

(* Idea: PROD_SET_BIGUNION for MOD n. *)

(* Theorem: FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
            !n. PROD_SET (BIGUNION P) MOD n = (PI (\s. (PROD_SET s) MOD n) P) MOD n *)
(* Proof:
   By induciton on FINITE set P.
   Base: PROD_SET (BIGUNION {}) MOD n = PI (\s. PROD_SET s MOD n) {} MOD n
        PROD_SET (BIGUNION {}) MOD n
      = PROD_SET {} MOD n                      by BIGUNION_EMPTY
      = 1 MOD n                                by PROD_SET_EMPTY
      = (PI (\s. PROD_SET s MOD n) {}) MOD n   by PROD_IMAGE_EMPTY

   Step: EVERY_FINITE P /\ PAIR_DISJOINT P ==>
         !n. PROD_SET (BIGUNION P) MOD n = PI (\s. PROD_SET s MOD n) P MOD n
     ==> e NOTIN P /\ EVERY_FINITE (e INSERT P) /\ PAIR_DISJOINT (e INSERT P) /\ 1 < n
         ==> PROD_SET (BIGUNION (e INSERT P)) MOD n = PI (\s. PROD_SET s MOD n) (e INSERT P) MOD n

      Note FINITE e /\ FINITE (BIGUNION P) /\
           EVERY_FINITE P                      by FINITE_BIGUNION, IN_INSERT
       and DISJOINT e (BIGUNION P) /\
           PAIR_DISJOINT P                     by DISJOINT_BIGUNION, IN_INSERT

      Let f = \s. PROD_SET s MOD n.

        PROD_SET (BIGUNION (e INSERT P)) MOD n
      = PROD_SET (e UNION (BIGUNION P)) MOD n                          by BIGUNION_INSERT
      = ((PROD_SET e) * (PROD_SET (BIGUNION P))) MOD n                 by PROD_SET_UNION_DISJOINT
      = ((PROD_SET e) MOD n *  (PROD_SET (BIGUNION P)) MOD n) MOD n    by MOD_PRODUCT
      = ((PROD_SET e) MOD n * (PI f P) MOD n) MOD n                    by induction hypothesis
      = ((f e) * (PI f P) MOD n) MOD n                                 by function application
      = (f e * PI f P) MOD n                                           by MOD_PRODUCT, MOD_MOD
      = (PI f MOD n) (e INSERT P)) MOD n                               by PROD_IMAGE_INSERT
*)
Theorem PROD_SET_MOD_BIGUNION:
  !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
   !n. PROD_SET (BIGUNION P) MOD n = (PI (\s. (PROD_SET s) MOD n) P) MOD n
Proof
  Induct_on `FINITE` >>
  rpt strip_tac >-
  simp[PROD_SET_EMPTY, PROD_IMAGE_EMPTY] >>
  qabbrev_tac `f = \s. PROD_SET s MOD n` >>
  `FINITE e /\ FINITE (BIGUNION P) /\ EVERY_FINITE P` by simp[] >>
  `DISJOINT e (BIGUNION P) /\ PAIR_DISJOINT P` by metis_tac[DISJOINT_BIGUNION, IN_INSERT] >>
  `PROD_SET (BIGUNION (e INSERT P)) MOD n = PROD_SET (e UNION (BIGUNION P)) MOD n` by simp[] >>
  `_ = ((PROD_SET e) * (PROD_SET (BIGUNION P))) MOD n` by simp[GSYM PROD_SET_UNION_DISJOINT] >>
  `_ = ((PROD_SET e) MOD n * (PROD_SET (BIGUNION P)) MOD n) MOD n` by simp[GSYM MOD_PRODUCT] >>
  `_ = ((PROD_SET e) MOD n * (PI f P) MOD n) MOD n` by fs[Abbr`f`] >>
  `_ = ((f e) * (PI f P) MOD n) MOD n` by simp[Abbr`f`] >>
  `_ = (f e * PI f P) MOD n` by metis_tac[MOD_PRODUCT, MOD_OF_MOD] >>
  simp[PROD_IMAGE_INSERT]
QED

(* ------------------------------------------------------------------------- *)
(* More for helperList                                                       *)
(* ------------------------------------------------------------------------- *)

(* Theorem: m <= n ==> PROD [m .. n] = m * PROD [m + 1 .. n] *)
(* Proof:
     PROD [m .. n]
   = PROD (m::[m + 1 .. n])    by listRangeINC_CONS, m <= n
   = m * PROD [m + 1 .. n]     by PROD_CONS
*)
Theorem PROD_SUC:
  !m n. m <= n ==> PROD [m .. n] = m * PROD [m + 1 .. n]
Proof
  simp[listRangeINC_CONS, PROD_CONS]
QED

(* Theorem: ls <> [] ==> PROD ls = (LAST ls) * PROD (FRONT ls) *)
(* Proof:
     PROD ls
   = PROD (FRONT ls ++ [LAST ls])              by APPEND_FRONT_LAST, ls <> []
   = PROD (FRONT ls) * PROD [LAST ls]          by PROD_APPEND
   = (LAST ls) * PROD (FRONT ls)               by PROD_SING
*)
Theorem PROD_FRONT_LAST:
  !ls. ls <> [] ==> PROD ls = (LAST ls) * PROD (FRONT ls)
Proof
  metis_tac[APPEND_FRONT_LAST, PROD_APPEND, PROD_SING, MULT_COMM]
QED

(* Theorem: 1 < n /\ m < n ==>
            (PROD [(n - m) .. (n - 1)]) MOD n = if (EVEN m) then (FACT m) MOD n else (n - (FACT m) MOD n) MOD n *)
(* Proof:
   By induction on m.
   Base: 0 < n ==>
         PROD [n - 0 .. n - 1] MOD n = if EVEN 0 then FACT 0 MOD n else (n - FACT 0 MOD n) MOD n
      LHS = PROD [n - 0 .. n - 1] MOD n
          = PROD [] MOD n                      by listRangeINC_NIL
          = 1 MOD n                            by PROD_NIL
      RHS = FACT 0 MOD n                       by EVEN
          = 1 MOD n = LHS                      by FACT_0
   Step: m < n ==>
         PROD [n - m .. n - 1] MOD n = if EVEN m then FACT m MOD n else (n - FACT m MOD n) MOD n
     ==> SUC m < n ==>
         PROD [n - SUC m .. n - 1] MOD n = if EVEN (SUC m) then FACT (SUC m) MOD n else (n - FACT (SUC m) MOD n) MOD n

        PROD [n - SUC m .. n - 1] MOD n
      = PROD ((n - SUC m)::[n - m .. n - 1]) MOD n     by listRangeINC_CONS
      = ((n - SUC m) * PROD [n - m .. n - 1]) MOD n    by PROD_CONS
      = (((n - SUC m) MOD n) * (PROD [n - m .. n - 1] MOD n)) MOD n
                                                       by MOD_TIMES2
      = ((n - SUC m) * (PROD [n - m .. n - 1] MOD n)) MOD n
                                                       by LESS_MOD, SUC m < n
      = ((n - SUC m) * (if EVEN m then FACT m MOD n else (n - FACT m MOD n) MOD n)) MOD n
                                                       by induction hypothesis

      If EVEN m, then ~(EVEN (SUC m))                  by EVEN
        Note FACT m MOD n < n                          by LESS_MOD

        PROD [n - SUC m .. n - 1] MOD n
      = ((n - SUC m) * (FACT m MOD n)) MOD n           by above
      = (n - (SUC m * (FACT m MOD n)) MOD n) MOD n     by MOD_NEG_MULT
      = (n - ((SUC m MOD n) * (FACT m MOD n)) MOD n) MOD n
                                                       by LESS_MOD
      = (n - (SUC m * FACT m) MOD n) MOD n             by MOD_TIMES2, MOD_MOD
      = (n - FACT (SUC m) MOD n) MOD n                 by FACT

      If ~EVEN m, then EVEN (SUC m)                    by EVEN
        Note FACT m MOD n < n                          by LESS_MOD
         and (SUC m * FACT m) MOD n < n                by LESS_MOD

        PROD [n - SUC m .. n - 1] MOD n
      = ((n - SUC m) * (n - FACT m MOD n) MOD n) MOD n by above
      = (n - ((SUC m) * (n - FACT m MOD n)) MOD n) MOD n       by MOD_NEG_MULT
      = (n - (n - (FACT m MOD n * SUC m) MOD n) MOD n) MOD n   by MOD_NEG_MULT
      = (n - (n - (SUC m * FACT m) MOD n) MOD n) MOD n         by MOD_TIMES2, LESS_MOD, MOD_MOD
      = (n - (n - (FACT (SUC m)) MOD n) MOD n) MOD n           by FACT

      If (FACT (SUC m)) MOD n = 0,
        PROD [n - SUC m .. n - 1] MOD n
      = (n - (n - 0) MOD n) MOD n                      by above
      = 0 = (FACT (SUC m)) MOD n                       by DIVMOD_ID, 0 < n

      Otherwise 0 < (FACT (SUC m)) MOD n.
        PROD [n - SUC m .. n - 1] MOD n
      = (n - (n - (FACT (SUC m)) MOD n) MOD n) MOD n   by above
      = (n - (n - (FACT (SUC m)) MOD n)) MOD n         by MOD_NEG_SUB, MOD_MOD
      = (FACT (SUC m)) MOD n) MOD n                    by SUB_SUB_ID
      = FACT (SUC m) MOD n                             by LESS_MOD

MOD_NEG_MULT |> SPEC ``n:num`` |> SPEC ``(FACT m) MOD n`` |> SPEC ``SUC m``;
|- FACT m MOD n <= n ==> ((n - FACT m MOD n) * SUC m) MOD n = (n - (FACT m MOD n * SUC m) MOD n) MOD n

*)
Theorem PROD_NEG_MOD_EQN:
  !m n. 1 < n /\ m < n ==>
         (PROD [(n - m) .. (n - 1)]) MOD n = if (EVEN m) then (FACT m) MOD n else (n - (FACT m) MOD n) MOD n
Proof
  rpt strip_tac >>
  Induct_on `m` >| [
    rw[] >>
    `[n .. n - 1] = []` by simp[listRangeINC_NIL] >>
    fs[FACT_0],
    rpt strip_tac >>
    `m < n /\ 0 < n` by decide_tac >>
    `n - SUC m + 1 = n - m /\ n - SUC m <= n - 1` by decide_tac >>
    `PROD [n - SUC m .. n - 1] MOD n = PROD ((n - SUC m)::[n - m .. n - 1]) MOD n` by metis_tac[listRangeINC_CONS] >>
    `_ = ((n - SUC m) * PROD [n - m .. n - 1]) MOD n` by rfs[PROD_CONS] >>
    `_ = (((n - SUC m) MOD n) * (PROD [n - m .. n - 1] MOD n)) MOD n` by rfs[MOD_TIMES2] >>
    `_ = ((n - SUC m) * (PROD [n - m .. n - 1] MOD n)) MOD n` by rfs[GSYM LESS_MOD] >>
    Cases_on `EVEN m` >| [
      `~EVEN (SUC m)` by simp[EVEN] >>
      `PROD [n - SUC m .. n - 1] MOD n = ((n - SUC m) * (FACT m MOD n)) MOD n` by rfs[] >>
      `_ = (n - (SUC m * (FACT m MOD n)) MOD n) MOD n` by rfs[MOD_NEG_MULT] >>
      `_ = (n - ((SUC m) * FACT m) MOD n) MOD n` by rfs[LESS_MOD] >>
      rfs[FACT],
      `EVEN (SUC m)` by simp[EVEN] >>
      `FACT m MOD n < n /\ (SUC m * FACT m) MOD n < n` by simp[] >>
      `PROD [n - SUC m .. n - 1] MOD n = ((n - SUC m) * (n - FACT m MOD n) MOD n) MOD n` by rfs[] >>
      `_ = (n - ((SUC m) * (n - FACT m MOD n)) MOD n) MOD n` by rfs[GSYM MOD_NEG_MULT] >>
      `_ = (n - (n - (FACT m MOD n * SUC m) MOD n) MOD n) MOD n` by simp[GSYM MOD_NEG_MULT] >>
      `_ = (n - (n - (SUC m * FACT m MOD n) MOD n) MOD n) MOD n` by metis_tac[MULT_COMM] >>
      `_ = (n - (n - (SUC m * FACT m) MOD n) MOD n) MOD n` by rfs[] >>
      `_ = (n - (n - (FACT (SUC m)) MOD n) MOD n) MOD n` by metis_tac[FACT] >>
      qabbrev_tac `z = (FACT (SUC m)) MOD n` >>
      `z = 0 \/ 0 < z` by rfs[] >-
      simp[] >>
      `z < n` by simp[Abbr`z`] >>
      `PROD [n - SUC m .. n - 1] MOD n = (n - (n - z)) MOD n` by rfs[MOD_NEG_SUB] >>
      metis_tac[SUB_SUB_ID, LT_IMP_LE, LESS_MOD]
    ]
  ]
QED

(* This is excellent! *)

(* Theorem: PROD (MAP f (h::t)) = f h * PROD (MAP f t *)
(* Proof:
     PROD (MAP f (h::t))
   = PROD (f h :: MAP f t)         by MAP
   = f h * PROD (MAP f t)          by PROD
*)
Theorem PROD_MAP_CONS:
  !f h t. PROD (MAP f (h::t)) = f h * PROD (MAP f t)
Proof
  simp[PROD]
QED

(* Theorem: EVERY ($= k) ls ==> PROD ls = k ** LENGTH ls *)
(* Proof:
   By induction on ls.
   Base: !k. EVERY ($= k) [] ==> PROD [] = k ** LENGTH []
      Note EVERY ($= k) [] is True
        PROD []
      = 1                      by PROD
      = k ** 0                 by EXP_0
      = k ** LENGTH []         by LENGTH

   Step: !k. EVERY ($= k) ls ==> PROD ls = k ** LENGTH ls
     ==> !h k. EVERY ($= k) (h::ls) ==>  = k ** LENGTH (h::ls)
      Note EVERY ($= k) (h::ls)
       ==> h = k /\ EVERY ($= k) ls            by EVERY_DEF

        PROD (h::ls)
      = h * PROD ls            by PROD
      = h * k ** LENGTH ls     by induction hypothesis
      = k * k ** LENGTH ls     by h = k
      = k ** SUC (LENGTH ls)   by EXP
      = k ** LENGTH (h::ls)    by LENGTH
*)
Theorem PROD_K_LIST:
  !ls k. EVERY ($= k) ls ==> PROD ls = k ** LENGTH ls
Proof
  Induct >-
  simp[] >>
  rw[EVERY_DEF] >>
  fs[EXP]
QED

(* ------------------------------------------------------------------------- *)
(* More for helperFunction                                                   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: FACT n = if n = 0 then 1 else n * FACT (n - 1) *)
(* Proof:
   If n = 0,
     FACT 0 = 1                by FACT_0
   Otherwise, n <> 0
   ==> ?m. n = SUC m           by num_CASES
     FACT n
   = FACT (SUC m)              by n = SUC m
   = n * FACT m                by FACT
   = n * FACT (n - 1)          by SUC_SUB1
*)
Theorem FACT_EQN:
  !n. FACT n = if n = 0 then 1 else n * FACT (n - 1)
Proof
  metis_tac[FACT, SUC_SUB1, num_CASES]
QED

(* in helperList,
prod_1_to_n_eq_fact_n  |- !n. PROD [1 .. n] = FACT n
*)

(* Theorem: FACT n = PROD [1 .. n] *)
(* Proof:
   By induction on n.
   Base: FACT 0 = PROD [1 .. 0]
        FACT 0
      = 1                          by FACT_0
      = PROD [] = PROD [1 .. 0]    by PROD_NIL, listRangeINC_NIL
   Step: FACT n = PROD [1 .. n]
     ==> FACT (SUC n) = PROD [1 .. SUC n]
     Let ls = [1 .. (SUC n)].
     Then ls <> []                 by listRangeINC_NIL
        FACT (SUC n)
      = (SUC n) * FACT n           by FACT
      = (SUC n) * PROD [1 .. n]    by induction hypothesis
      = LAST ls * PROD (FRONT ls)  by listRangeINC_LAST, listRangeINC_FRONT, ADD1
      = PROD ls                    by PROD_FRONT_LAST
*)
Theorem FACT_BY_PROD:
  !n. FACT n = PROD [1 .. n]
Proof
  Induct >| [
    `[1 .. 0] = []` by simp[listRangeINC_NIL] >>
    simp[FACT_0],
    `[1 .. SUC n] <> []` by simp[listRangeINC_NIL] >>
    `FACT (SUC n) = (SUC n) * FACT n` by simp[FACT] >>
    `_ = (SUC n) * PROD [1 .. n]` by fs[] >>
    `_ = LAST [1 .. (SUC n)] * PROD (FRONT [1 .. (SUC n)])` by rw[listRangeINC_LAST, listRangeINC_FRONT, ADD1] >>
    simp[PROD_FRONT_LAST]
  ]
QED
(* Note: merge this with helperList: prod_1_to_n_eq_fact_n, maybe the same proof!. *)

(* Theorem alias *)
Theorem FACT_BY_PROD_SET = helperFunctionTheory.FACT_EQ_PROD;
(* val FACT_BY_PROD_SET = |- !n. FACT n = PROD_SET (natural n): thm *)

(* Theorem: m <= n ==> FACT n = FACT m * PROD [m + 1 .. n] *)
(* Proof:
   If m = 0,
      RHS = 1 * PROD [1 .. n]                  by FACT_0
          = FACT n = LHS                       by FACT_BY_PROD
   Otherwise, 0 < m, or 1 <= m.
     FACT n
   = PROD [1 .. n]                             by FACT_BY_PROD
   = PROD ([1 .. m] ++ [m + 1 .. n])           by listRangeINC_APPEND, 1 <= m <= n
   = (PROD [1 .. m]) * (PROD [m + 1 .. n])     by PROD_APPEND
   = FACT m * PROD [m + 1 .. n]                by FACT_BY_PROD
*)
Theorem FACT_IN_FACT:
  !m n. m <= n ==> FACT n = FACT m * PROD [m + 1 .. n]
Proof
  rpt strip_tac >>
  `m = 0 \/ 1 <= m` by decide_tac >| [
    simp[FACT_0] >>
    simp[FACT_BY_PROD],
    metis_tac[FACT_BY_PROD, listRangeINC_APPEND, PROD_APPEND]
  ]
QED

(* Theorem: 0 < m /\ m <= n ==> m divides (FACT n) *)
(* Proof:
   By induction on n.
   Base: 0 < m /\ m <= 0 ==> m divides FACT 0
      True by 0 < m /\ m <= 0 is F.
   Step: 0 < m /\ m <= n ==> m divides FACT n
     ==> m <= SUC n ==> m divides FACT (SUC n)
      Note FACT (SUC n) = (SUC n) * FACT n     by FACT
      If m = SUC n,
         Then m divides FACT (SUC n)           by divides_def
      Otherwise m < SUC n, or m <= n
         Thus m divides (FACT n)               by induction hypothesis
           so m divides FACT (SUC n)           by DIVIDES_MULT

   A direct proof can be based on:
       FACT n
     = PROD [1 .. n]
     = PROD ([1 .. m - 1] ++ [m .. n])
     = (PROD [1 .. m - 1]) * (PROD [m .. n])
     = (PROD [m .. n]) * (PROD [1 .. m - 1])
     = (PROD (m::[m + 1 .. n])) * (PROD [1 .. m - 1])
     = m * (PROD [m + 1 .. n]) * (PROD [1 .. m - 1])

   A simple proof:
      FACT n
    = FACT m * PROD [m + 1 .. n]               by FACT_IN_FACT, m <= n
    = m * FACT (m - 1) * PROD [m+1 .. n]       by FACT_EQN, 0 < m
   Thus m divides FACT n                       by divides_def

*)
Theorem FACT_factor:
  !m n. 0 < m /\ m <= n ==> m divides (FACT n)
Proof
  rpt strip_tac >>
  `FACT n = FACT m * PROD [m + 1 .. n]` by simp[FACT_IN_FACT] >>
  `_ = m * FACT (m - 1) * PROD [m + 1 .. n]` by metis_tac[FACT_EQN, NOT_ZERO] >>
  `_ = (FACT (m - 1) * PROD [m + 1 .. n]) * m` by decide_tac >>
  metis_tac[divides_def]
QED

(* Theorem: m <= n ==> (FACT m) divides (FACT n) *)
(* Proof:
   By induction on n.
   Base: m <= 0 ==> FACT m divides FACT 0
      This makes m = 0,
       and FACT 0 divides FACT 0   by DIVIDES_REFL
   Step: m <= n ==> FACT m divides FACT n
     ==> m <= SUC n ==> FACT m divides FACT (SUC n)
      If m = SUC n,
         FACT (SUC n) divides FACT (SUC n)     by DIVIDES_REFL
      Otherwise, m <= n.
         FACT (SUC n) = (SUC n) * FACT n       by FACT
         and (FACT m) divides (FACT n)         by induction hypothesis
          so (FACT m) divides FACT (SUC n)     by DIVIDES_MULT

   A simple proof:
     FACT n
   = FACT m * PROD [m + 1 .. n]    by FACT_IN_FACT, m <= n
   Thus (FACT m) divides (FACT n)  by divides_def
*)
Theorem FACT_divides_FACT:
  !m n. m <= n ==> (FACT m) divides (FACT n)
Proof
  metis_tac[FACT_IN_FACT, divides_def, MULT_COMM]
QED

(* Theorem: 0 < a /\ 0 < b /\ a <= n /\ b <= n /\ a <> b ==> (a * b) divides (FACT n) *)
(* Proof:
   If a < b,
       FACT n
     = FACT b * PROD [b + 1 .. n]             by FACT_IN_FACT, b <= n
     = b * FACT (b - 1) * PROD [b + 1 .. n]   by FACT_EQN, 0 < b
     = b * (FACT a * PROD [a + 1 .. b - 1]) * PROD [b + 1 .. n]
                                              by FACT_IN_FACT, a <= b - 1
     = b * (a * FACT (a - 1) * PROD [a + 1 .. b - 1]) * PROD [b + 1 .. n]
                                              by FACT_EQN, 0 < a
     = (FACT (a - 1) * PROD [a + 1 .. b - 1] * PROD [b + 1 .. n]) * (a * b)
                                              by arithmetic
     Thus (a * b) divides (FACT n)            by divides_def

   Similarly for b < a.
*)
Theorem FACT_factor_product:
  !n a b. 0 < a /\ 0 < b /\ a <= n /\ b <= n /\ a <> b ==> (a * b) divides (FACT n)
Proof
  rpt strip_tac >>
  `!x y. 0 < x /\ 0 < y /\ x <= n /\ y <= n /\ x < y ==> (x * y) divides (FACT n)` by
  (rpt strip_tac >>
  `x <> 0 /\ y <> 0 /\ x <= y - 1` by decide_tac >>
  `FACT n = y * FACT (y - 1) * PROD [y + 1 .. n]` by metis_tac[FACT_IN_FACT, FACT_EQN] >>
  `_ = y * (x * FACT (x - 1) * PROD [x + 1 .. y - 1]) * PROD [y + 1 .. n]` by metis_tac[FACT_IN_FACT, FACT_EQN] >>
  `_ = (FACT (x - 1) * PROD [x + 1 .. y - 1] * PROD [y + 1 .. n]) * (x * y)` by decide_tac >>
  metis_tac[divides_def]) >>
  `a < b \/ b < a` by decide_tac >-
  simp[] >>
  metis_tac[MULT_COMM]
QED

(* ------------------------------------------------------------------------- *)
(* SET_TO_LIST Theorems                                                      *)
(* ------------------------------------------------------------------------- *)

(*
Note that SET_TO_LIST forms an unordered list, as a set is unordered.
Therefore, any attempt to impose an order for SET_TO_LIST is doomed.
For example, to show SET_TO_LIST (e INSERT s) = e :: SET_TO_LIST s
will never succeed, as the result puts e IN s as the head of the list.

This means theorems about (SET_TO_LIST s) cannot be proved by FINITE induction on s.
The only thing allowed is SET_TO_LIST_THM:
|- FINITE s ==> SET_TO_LIST s = if s = {} then [] else CHOICE s::SET_TO_LIST (REST s)

Therefore, SET_TO_LIST theorems are proved by another form of induction:
SET_TO_LIST_IND
|- !P. (!s. (FINITE s /\ s <> {} ==> P (REST s)) ==> P s) ==> !v. P v

This is because:
CHOICE_INSERT_REST  |- !s. s <> {} ==> CHOICE s INSERT REST s = s

Assuming a property P about sets is true for (REST s), show that it is true for the whole set s.
which is any (CHOICE s) inserted into (REST s). Then the property P about sets is true.
*)

(* Theorem: *)
(* Proof: *)
(* <<SET_TO_LIST_APPEND>>
g `!s t. FINITE s /\ FINITE t /\ DISJOINT s t ==>
       SET_TO_LIST (s UNION t) = (SET_TO_LIST s) ++ (SET_TO_LIST t)`;
drop();
The result imposes an ordering, so cannot be proved.
In fact, s UNION t = t UNION s, but ls1 ++ ls2 <> ls2 ++ ls1.
*)

(* Theorem: *)
(* Proof: *)
(* <<SET_TO_LIST_INSERT>>
g `!s. FINITE s ==> !e. SET_TO_LIST (e INSERT s) = if e IN s then SET_TO_LIST s else e::SET_TO_LIST s`;
e (ho_match_mp_tac SET_TO_LIST_IND);
e (rpt strip_tac);
(* goal: SET_TO_LIST (e INSERT s) = if e IN s then SET_TO_LIST s else e::SET_TO_LIST s *)
The result imposes an ordering, so cannot be proved.
In fact, s UNION t = t UNION s, but ls1 ++ ls2 <> ls2 ++ ls1.
*)

(* Theorem: FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
            !n. PROD_SET (BIGUNION P) MOD n = PROD (MAP (\s. (PROD_SET s) MOD n) (SET_TO_LIST P)) MOD n *)
(* Proof:
   By SET_TO_LIST induction on P, or s as renamed.
   Let f = \s. PROD_SET s MOD n.
   Base: s = {} ==> PROD_SET (BIGUNION s) MOD n = PROD (MAP f (SET_TO_LIST s)) MOD n
        PROD_SET (BIGUNION {}) MOD n
      = PROD_SET {} MOD n                      by BIGUNION_EMPTY
      = 1 MOD n                                by PROD_SET_EMPTY
      = PROD [] MOD n                          by PROD_NIL
      = PROD (MAP f (SET_TO_LIST {})) MOD n    by MAP, SET_TO_LIST_EMPTY

   Step: FINITE s /\ s <> {} ==> FINITE (REST s) /\ EVERY_FINITE (REST s) /\ PAIR_DISJOINT (REST s) ==>
         !n. PROD_SET (BIGUNION (REST s)) MOD n = PROD (MAP (\s. PROD_SET s MOD n) (SET_TO_LIST (REST s))) MOD n
     ==> FINITE s /\ s <> {} /\ EVERY_FINITE s /\ PAIR_DISJOINT s ==>
         PROD_SET (BIGUNION s) MOD n = PROD (MAP f (SET_TO_LIST s)) MOD n

      Let e = CHOICE s,
          t = REST s.
      Then s = e INSERT t                      by CHOICE_INSERT_REST, s <> {}
       and e NOTIN t                           by CHOICE_NOT_IN_REST
      Also FINITE t                            by FINITE_REST
       and FINITE e /\ EVERY_FINITE t          by EVERY_FINITE s, IN_INSERT
       and PAIR_DISJOINT t                     by PAIR_DISJOINT s, IN_INSERT
       and FINITE (BIGUNION t)                 by FINITE_BIGUNION
       and DISJOINT e (BIGUNION t)             by DISJOINT_BIGUNION, IN_INSERT

        PROD_SET (BIGUNION s) MOD n
      = PROD_SET (BIGUNION (e INSERT t)) MOD n                         by s = e INSERT t
      = PROD_SET (e UNION (BIGUNION t)) MOD n                          by BIGUNION_INSERT
      = ((PROD_SET e) * (PROD_SET (BIGUNION t))) MOD n                 by PROD_SET_UNION_DISJOINT
      = ((PROD_SET e) MOD n * (PROD_SET (BIGUNION t)) MOD n) MOD n     by MOD_PRODUCT
      = ((PROD_SET e) MOD n * (PROD (MAP f (SET_TO_LIST t)) MOD n)) MOD n    by induction hypothesis
      = ((f e) * (PROD (MAP f (SET_TO_LIST t)) MOD n)) MOD n                 by function application
      = (f e * PROD (MAP f (SET_TO_LIST t))) MOD n                     by MOD_PRODUCT, MOD_MOD
      = (PROD (MAP f (e::SET_TO_LIST t))) MOD n                        by PROD_MAP_CONS
      = PROD (MAP f (SET_TO_LIST s)) MOD n                             by SET_TO_LIST_THM
*)
Theorem PROD_SET_MOD_BIGUNION_eq_PROD_MAP_MOD:
  !P. FINITE P /\ EVERY_FINITE P /\ PAIR_DISJOINT P ==>
   !n. PROD_SET (BIGUNION P) MOD n = PROD (MAP (\s. (PROD_SET s) MOD n) (SET_TO_LIST P)) MOD n
Proof
  ho_match_mp_tac SET_TO_LIST_IND >>
  rpt strip_tac >>
  qabbrev_tac `f = \s. PROD_SET s MOD n` >>
  Cases_on `s = {}` >-
  simp[PROD_SET_EMPTY, Abbr`f`] >>
  qabbrev_tac `e = CHOICE s` >>
  qabbrev_tac `t = REST s` >>
  `s = e INSERT t /\ e NOTIN t` by simp[CHOICE_INSERT_REST, CHOICE_NOT_IN_REST, Abbr`e`, Abbr`t`] >>
  `FINITE t` by simp[Abbr`t`] >>
  `FINITE e /\ EVERY_FINITE t /\ PAIR_DISJOINT t` by metis_tac[IN_INSERT] >>
  `FINITE (BIGUNION t)` by simp[FINITE_BIGUNION] >>
  `DISJOINT e (BIGUNION t)` by metis_tac[DISJOINT_BIGUNION, IN_INSERT] >>
  `PROD_SET (BIGUNION s) MOD n = PROD_SET (BIGUNION (e INSERT t)) MOD n` by fs[] >>
  `_ = PROD_SET (e UNION (BIGUNION t)) MOD n` by simp[] >>
  `_ = ((PROD_SET e) * (PROD_SET (BIGUNION t))) MOD n` by simp[GSYM PROD_SET_UNION_DISJOINT] >>
  `_ = ((PROD_SET e) MOD n * (PROD_SET (BIGUNION t)) MOD n) MOD n` by simp[GSYM MOD_PRODUCT] >>
  `_ = ((PROD_SET e) MOD n * (PROD (MAP f (SET_TO_LIST t)) MOD n)) MOD n` by fs[Abbr`f`] >>
  `_ = ((f e) * (PROD (MAP f (SET_TO_LIST t)) MOD n)) MOD n` by simp[Abbr`f`] >>
  `_ = (f e * PROD (MAP f (SET_TO_LIST t))) MOD n` by metis_tac[MOD_PRODUCT, MOD_OF_MOD] >>
  `_ = (PROD (MAP f (e::SET_TO_LIST t))) MOD n` by simp[PROD_MAP_CONS] >>
  simp[SET_TO_LIST_THM, Abbr`e`, Abbr`t`]
QED

(* This is very very good! *)

(* ------------------------------------------------------------------------- *)
(* Fast Computations                                                         *)
(* ------------------------------------------------------------------------- *)

(* Define factorial n in MOD m *)
(*
Definition fact_mod_def:
   fact_mod n m = FST (WHILE (\(k,j). 1 < j) (\(k,j). ((k * j) MOD m, j - 1)) (1, n))
End
*)
(* \(k,j). k is the multiplier, starting with 1 = FACT 1, j is the decreasing index, starting with n. *)
(* better just use linear recursion on n *)
Definition fact_mod_def:
   fact_mod 0 m = 1 MOD m /\
   fact_mod (SUC n) m = (SUC n * fact_mod n m) MOD m
End

(*
EVAL ``(FACT 10) MOD 13``; = 6
EVAL ``(FACT 10) MOD 17``; = 14
EVAL ``(FACT 10) MOD 19``; = 9
EVAL ``fact_mod 10 13``; = 6
EVAL ``fact_mod 10 17``; = 14
EVAL ``fact_mod 10 19``; = 9
*)

(* Theorem: fact_mod n m = (FACT n) MOD m *)
(* Proof:
   By induction on n.
   Base: fact_mod 0 m = (FACT 0) MOD m
       LHS = fact_mod 0 m
           = 1 MOD m               by fact_mod_def
           = (FACT 0) MOD m        by FACT_0
           = RHS
   Step: fact_mod (SUC n) m = (FACT (SUC n)) MOD m
       LHS = fact_mod (SUC n) m
           = (SUC n * fact_mod n m) MOD m      by fact_mod_def
           = (SUC n * (FACT n) MOD m) MOD m    by induction hypothesis
           = (SUC n * FACT n) MOD m            by TIMES_MOD_MOD
           = FACT (SUC n) MOD m                by FACT
           = RHS
*)
Theorem fact_mod_thm:
  !m n. fact_mod n m = (FACT n) MOD m
Proof
  rpt strip_tac >>
  Induct_on `n` >-
  simp[fact_mod_def, FACT_0] >>
  simp[fact_mod_def, FACT, TIMES_MOD_MOD]
QED

(* in binomialTheory.

(* Define Binomials:
   C(n,0) = 1
   C(0,k) = 0 if k > 0
   C(n+1,k+1) = C(n,k) + C(n,k+1)
*)
val binomial_def = Define`
    (binomial 0 0 = 1) /\
    (binomial (SUC n) 0 = 1) /\
    (binomial 0 (SUC k) = 0)  /\
    (binomial (SUC n) (SUC k) = binomial n k + binomial n (SUC k))
`;

Thus the computation is recursive, but additions only, not compute by factorials.
This effectively traces Pascal's triangle from the top.
*)


(* Define binomial fast *)
(*)
Definition binomial_fast_def:
   binomial_fast m n = FST (WHILE (\(k,j). j < n) (\(k,j). (((m - j) * k) DIV (j + 1), j + 1)) (1, 0))
End
*)
(* \(k,j). k is the multiplier, starting with binomial m 0 = 1, j is the increasing index, starting with 0. *)
(* binomial_right |> SPEC ``m:num``;
val it = |- 0 < m ==> !k. binomial m (k + 1) = (m - k) * binomial m k DIV (k + 1): thm *)
(* better just use linear recursion on n *)
Definition binomial_fast_def:
   binomial_fast m 0 = 1 /\
   binomial_fast m (SUC n) = ((m - n) * binomial_fast m n) DIV (SUC n)
End
(* This is tracing the m-th row of Pascal's triangle *)

(*
val binomial_fast_def =
   |- (!n. binomial_fast 0 n = if n = 0 then 1 else 0) /\
      (!v2. binomial_fast (SUC v2) 0 = 1) /\
      !v3 n.
        binomial_fast (SUC v3) (SUC n) =
        (SUC v3 - n) * binomial_fast (SUC v3) n DIV SUC n: thm
*)

(*
EVAL ``binomial 6 1``; = 1
EVAL ``binomial 6 1``; = 6
EVAL ``binomial 6 2``; = 15
EVAL ``binomial 6 3``; = 20
EVAL ``binomial 6 7``; = 0       by binomial 0 (SUC k) = 0
EVAL ``binomial_fast 6 0``; = 1
EVAL ``binomial_fast 6 1``; = 6
EVAL ``binomial_fast 6 2``; = 15
EVAL ``binomial_fast 6 3``; = 20
EVAL ``binomial_fast 6 7``; = 0  due to subtraction (m - j)
*)

(* Theorem: binomial_fast m n = binomial m n *)
(* Proof:
   By induction on n.
   Base: binomial_fast m 0 = binomial m 0
         binomial_fast m 0
       = 1                                           by binomial_fast_def
       = binomial m 0                                by binomial_n_0
   Step: binomial_fast m n = binomial m n ==>
         binomial_fast m (SUC n) = binomial m (SUC n)
       If m = 0.
         binomial_fast 0 (SUC n)
       = ((0 - n) * binomial_fast 0 n) DIV (SUC n)   by binomial_fast_def
       = 0                                           by integer arithmetic
       = binomial 0 (SUC n)                          by binomial_0_n
       If m <> 0,
         binomial_fast m (SUC n)
       = ((m - n) * binomial_fast m n) DIV (SUC n)   by binomial_fast_def
       = ((m - n) * binomial m n DIV (SUC n))        by induction hypothesis
       = binomial m (SUC n)                          by binomial_right, 0 < m
*)
Theorem binomial_fast_thm:
  !m n. binomial_fast m n = binomial m n
Proof
  rpt strip_tac >>
  Induct_on `n` >-
  simp[binomial_fast_def, binomial_n_0] >>
  `m = 0 \/ 0 < m` by decide_tac >-
  simp[binomial_fast_def, binomial_0_n] >>
  simp[binomial_fast_def, binomial_right, ADD1]
QED

(* ------------------------------------------------------------------------- *)
(* Gauss' formula for Fermat's Two Squares                                   *)
(* ------------------------------------------------------------------------- *)

(* Gauss' formula *)
Definition two_sq_gauss_def[nocompute]:
   two_sq_gauss n = let k = (n DIV 4); u = ((binomial (2 * k) k) DIV 2) MOD n;
                                       v = (u * FACT (2 * k)) MOD n in
                    (if u <= n DIV 2 then u else (n - u),
                     if v <= n DIV 2 then v else (n - v))
End
(* Note: for ODD n, n DIV 2 = (n - 1) DIV 2, so use u <= ... and v <= ... *)
(* later, use [nocompute] to suppress evaluation, then provide another evaluation in two_sq_gauss_alt. *)

(* Gauss' formula for computation *)
(* Theorem: two_sq_gauss n =
               let k = (n DIV 4);
                   u = ((binomial_fast (2 * k) k) DIV 2) MOD n;
                   v = (u * fact_mod (2 * k) n) MOD n
                in (if u <= n DIV 2 then u else (n - u),
                    if v <= n DIV 2 then v else (n - v)) *)
(* Proof:
   By two_sq_gauss_def, fact_mod_thm, binomial_fast_thm
*)
Theorem two_sq_gauss_alt[compute]:
  !n. two_sq_gauss n = let k = (n DIV 4);
                            u = ((binomial_fast (2 * k) k) DIV 2) MOD n;
                            v = (u * fact_mod (2 * k) n) MOD n in
                        (if u <= n DIV 2 then u else (n - u),
                         if v <= n DIV 2 then v else (n - v))
Proof
  rpt strip_tac >>
  simp[two_sq_gauss_def] >>
  qabbrev_tac `k = n DIV 4` >>
  qabbrev_tac `p = binomial (2 * k) k` >>
  qabbrev_tac `q = binomial_fast (2 * k) k` >>
  `p = q` by simp[binomial_fast_thm, Abbr`p`, Abbr`q`] >>
  qabbrev_tac `a = (FACT (2 * k) * HALF p MOD n) MOD n` >>
  qabbrev_tac `b = (HALF q MOD n * fact_mod (2 * k) n) MOD n` >>
  `a = b` by simp[fact_mod_thm, TIMES_MOD_MOD, Abbr`a`, Abbr`b`] >>
  simp[]
QED

(*
EVAL ``two_sq_gauss 5``; = (1,2)
EVAL ``two_sq_gauss 13``; = (3,2)
EVAL ``two_sq_gauss 17``; = (1,4)
EVAL ``two_sq_gauss 29``; = (5,2) long, now quick
EVAL ``two_sq_gauss 37``; = (1,6) very long, now quick
EVAL ``two_sq_gauss 41``; = (5,4) very long, very quick now
EVAL ``two_sq_gauss 61``; = (5,6)
EVAL ``two_sq_gauss 97``; = (9,4)
EVAL ``two_sq_gauss 101``; = (1,10)
*)

(* Idea for Gauss' formula.
   In MOD n, for n = 4 * k + 1

      (4 * k) ≡ (n - 1)
      (4 * k - 1) ≡ (n - 2)
      ...
      (4 * k - j) ≡ (n - 1 - j) = (n - (j + 1))
      ...
      (2 * k + 1) ≡ (n - 2 * k)     for j = 2 * k - 1

      Thus PROD [(2 * k + 1) .. (4 * k)] ≡ PROD [(n - 2 * k) .. (n - 1)]
      There are even number of factors, from 1 to 2 * k,
      and expanding the factors gives a polynomial of in n.
      However, in (MOD n) all powers of n are 0, so only the constants remain.
      As there are en even number of negatives, the final sign is +1, so:
      PROD [(2 * k + 1) .. (4 * k)] ≡ FACT (2 * k)

      Thus  FACT (2 * k) *  PROD [(2 * k + 1) .. (4 * k)] ≡ FACT (2 * k) * FACT (2 * k)
      or    PROD [1 .. (2 * k)] * PROD [(2 * k + 1) .. (4 * k)] ≡ FACT (2 * k) * FACT (2 * k)
      so    FACT (4 * k) ≡ FACT (2 * k) * FACT (2 * k)
      switching around,  FACT (2 * k) ** 2 ≡ FACT (n - 1)    as n = 4 * k + 1.

   Next, for prime n, the Wilson's theorem gives FACT (n - 1) ≡ (n - 1).
   Thus   FACT (2 * k) ** 2 ≡ (n - 1),   or FACT (2 * k) is a root of -1 in MOD n.
*)

(* Idea: a formula for half of the central binomial coefficient. *)

(* Theorem: 0 < n ==> binomial (2 * n) n = 2 * binomial (2 * n - 1) (n - 1) *)
(* Proof:
   Let y = binomial (2 * n - 1) (n - 1),
       z = binomial (2 * n) n.
   Note 2 * n = n + n                          by arithmetic
    and y * (FACT n * FACT (n - 1)) = FACT (2 * n - 1)
        z * (FACT n * FACT n) = FACT (2 * n)   by binomial_formula
     so z * FACT n * (n * FACT (n - 1)) = (2 * n) * FACT (2 * n - 1)
                                               by FACT_EQN
        z * FACT n * FACT (n - 1) = 2 * FACT (n + (n - 1))
                                               by EQ_MULT_LCANCEL, 0 < n
   Thus z * FACT n * FACT (n - 1) = 2 * y * FACT n * FACT (n - 1)
    But 0 < FACT n and 0 < FACT (n - 1)        by FACT_LESS
     so z = 2 * y                              by EQ_MULT_LCANCEL
*)
Theorem binomial_central_eqn:
  !n. 0 < n ==> binomial (2 * n) n = 2 * binomial (2 * n - 1) (n - 1)
Proof
  rpt strip_tac >>
  qabbrev_tac `y = binomial (2 * n - 1) (n - 1)` >>
  qabbrev_tac `z = binomial (2 * n) n` >>
  `n + n = 2 * n /\ n + (n - 1) = 2 * n - 1 /\ 0 < 2 * n` by decide_tac >>
  `y * (FACT n * FACT (n - 1)) = FACT (2 * n - 1)` by metis_tac[binomial_formula] >>
  `z * (FACT n * FACT n) = FACT (2 * n)` by metis_tac[binomial_formula] >>
  `FACT (2 * n) = 2 * n * FACT (2 * n - 1) /\ FACT n = n * FACT (n - 1)` by metis_tac[FACT_EQN, NOT_ZERO] >>
  `z * (FACT n * (n * FACT (n - 1))) = 2 * n * (y * (FACT n * FACT (n - 1)))` by metis_tac[] >>
  `n * FACT n * FACT (n - 1) * z = n * FACT n * FACT (n - 1) * (2 * y)` by decide_tac >>
  `0 < n * FACT n * FACT (n - 1)` by simp[FACT_LESS] >>
  metis_tac[EQ_MULT_LCANCEL, NOT_ZERO]
QED

(* Theorem: tik n ==> (FACT (n - 1)) MOD n = ((FACT (2 * (n DIV 4))) ** 2) MOD n *)
(* Proof:
   Note n <> 0, so 0 < n       by MOD_0
   If n = 1,
      LHS = (FACT 0) MOD 1
          = 1 MOD 1            by FACT_0
      RHS = ((FACT (2 * 0)) ** 2) MOD 1
          = 1 MOD 1 = LHS      by FACT_0
   Otherwise, 1 < n            by 0 < n, n <> 1
      Let k = n DIV 4.
      Then n = 4 * k + 1       by tik_eqn
        so 2 * k  < n          by inequality
       and n - 2 * k = 2 * k + 1   by arithmetic
       Now EVEN (2 * k)        by EVEN_DOUBLE

      (FACT (n - 1)) MOD n
    = (PROD [1 .. n - 1]) MOD n                                by FACT_BY_PROD
    = (PROD ([1 .. 2 * k] ++ [2 * k + 1 .. n - 1])) MOD n      by listRangeINC_APPEND
    = (PROD [1 .. 2 * k] * PROD [2 * k + 1 .. n - 1]) MOD n    by PROD_APPEND
    = (FACT (2 * k) * PROD [2 * k + 1 .. n - 1]) MOD n         by FACT_BY_PROD
    = (FACT (2 * k) MOD n * (PROD [2 * k + 1 .. n - 1]) MOD n) MOD n
                                                               by MOD_TIMES2
    = (FACT (2 * k) MOD n * FACT (2 * k) MOD n) MOD n          by PROD_NEG_MOD_EQN, 1 < n
    = (FACT (2 * k) * FACT (2 * k)) MOD n                      by MOD_TIMES2
    = ((FACT (2 * k)) ** 2) MOD n                              by EXP_2


PROD_NEG_MOD_EQN |> SPEC ``2 * k``;
|- !n. 1 < n /\ 2 * k < n ==>
          PROD [n - 2 * k .. n - 1] MOD n = if EVEN (2 * k) then FACT (2 * k) MOD n else (n - FACT (2 * k) MOD n) MOD n
*)
Theorem tik_fact_mod_eqn:
  !n. tik n ==> (FACT (n - 1)) MOD n = ((FACT (2 * (n DIV 4))) ** 2) MOD n
Proof
  rpt strip_tac >>
  `0 < n` by metis_tac[MOD_0, ONE_NOT_ZERO, NOT_ZERO] >>
  `n = 1 \/ 1 < n` by decide_tac >-
  simp[] >>
  qabbrev_tac `k = n DIV 4` >>
  `n = 4 * k + 1` by simp[tik_eqn, Abbr`k`] >>
  `n - 2 * k = 2 * k + 1 /\ 2 * k < n` by decide_tac >>
  `EVEN (2 * k)` by simp[EVEN_DOUBLE] >>
  `(FACT (n - 1)) MOD n = (PROD [1 .. n - 1]) MOD n` by simp[FACT_BY_PROD] >>
  `_ = (PROD ([1 .. 2 * k] ++ [2 * k + 1 .. n - 1])) MOD n` by simp[listRangeINC_APPEND] >>
  `_ = (PROD [1 .. 2 * k] * PROD [2 * k + 1 .. n - 1]) MOD n` by simp[PROD_APPEND] >>
  `_ = (FACT (2 * k) * PROD [2 * k + 1 .. n - 1]) MOD n` by simp[FACT_BY_PROD] >>
  `_ = (FACT (2 * k) MOD n * (PROD [2 * k + 1 .. n - 1]) MOD n) MOD n` by simp[] >>
  `_ = (FACT (2 * k) MOD n * FACT (2 * k) MOD n) MOD n` by metis_tac[PROD_NEG_MOD_EQN] >>
  `_ = (FACT (2 * k) * FACT (2 * k)) MOD n` by simp[] >>
  simp[GSYM EXP_2]
QED


(* ------------------------------------------------------------------------- *)
(* Computation of FACT (n - 1) MOD n                                         *)
(* ------------------------------------------------------------------------- *)

(* Idea: a composite number n has FACT (n - 1) MOD n = 0, almost. *)

(* Theorem: ~prime n ==>
            FACT (n - 1) MOD n = if n = 0 then 1 else if n = 4 then 2 else 0 *)
(* Proof:
   If n = 0,
      FACT (0 - 1) MOD 0 = 1 MOD 0 = 1         by FACT_0, MOD_0
   If n = 1,
      FACT (1 - 1) MOD 1 = 1 MOD 1 = 0         by FACT_0, MOD_1
   Otherwise, 1 < n.
   Note n <> 2                                 by PRIME_2
   and n <> 3                                  by PRIME_3
   If n = 4,
      FACT (4 - 1) MOD 4 = 6 MOD 4 = 2         by computation
   Otherwise, n <> 4.
      Thus ?a b. n = a * b /\
                 1 < a /\ a < n /\
                 1 < b /\ b < n                by PROPER_FACTOR_PAIR, 1 < n
        so a <= n - 1, b <= n - 1              by inequality
        or a divides FACT (n - 1),
           b divides FACT (n - 1)              by FACT_factor
      If a <> b,
         Then (a * b) divides FACT (n - 1)     by FACT_factor_product
           so n divides FACT (n - 1)
           or FACT (n - 1) MOD n = 0           by DIVIDES_MOD_0
      Otherwise, a = b.
          but a <> 2 as a * a = n <> 4
         Thus 2 < a,
              2 * a < a * a = n                by LT_MULT_RCANCEL
             and 2 * 2 < a * a = n             by LT_MONO_MULT2
              so n <> 4.
             Let c = 2 * a.
             Then a <> c                       by 1 < a
              and 1 < c /\ c < n               by 1 < a, above
               so c divides FACT (n - 1)       by FACT_factor
             Thus (a * c) divides FACT (n - 1) by FACT_factor_product
              but n = a * a divides (a * c)    by divides_def
               so n divides FACT (n - 1)       by DIVIDES_TRANS
            or FACT (n - 1) MOD n = 0          by DIVIDES_MOD_0
*)
Theorem FACT_one_less_composite:
  !n. ~prime n ==> FACT (n - 1) MOD n = if n = 0 then 1 else if n = 4 then 2 else 0
Proof
  rpt strip_tac >>
  `n = 0 \/ n = 1 \/ 1 < n` by decide_tac >-
  simp[FACT_0] >-
  simp[FACT_0] >>
  simp[] >>
  `n <> 2 /\ n <> 3` by metis_tac[PRIME_2, PRIME_3] >>
  Cases_on `n = 4` >| [
    `FACT 3 = 6` by EVAL_TAC >>
    simp[],
    simp[] >>
    `?a b. n = a * b /\ 1 < a /\ a < n /\ 1 < b /\ b < n` by metis_tac[PROPER_FACTOR_PAIR] >>
    `a <= n - 1 /\ b <= n - 1` by decide_tac >>
    qabbrev_tac `z = FACT (n - 1)` >>
    `a divides z /\ b divides z` by simp[FACT_factor, Abbr`z`] >>
    Cases_on `a = b` >| [
      `a <> 2` by metis_tac[DECIDE``2 * 2 = 4``] >>
      `2 * a < a * a /\ a <> 2 * a /\ 1 < 2 * a` by fs[] >>
      qabbrev_tac `c = 2 * a` >>
      `c <= n - 1` by rfs[] >>
      `c divides z` by simp[FACT_factor, Abbr`z`] >>
      `(a * c) divides z` by simp[FACT_factor_product, Abbr`z`] >>
      `n divides (a * c)` by simp[GSYM divides_def, Abbr`c`] >>
      `n divides z` by metis_tac[DIVIDES_TRANS] >>
      rfs[DIVIDES_MOD_0],
      `(a * b) divides z` by simp[FACT_factor_product, Abbr`z`] >>
      rfs[DIVIDES_MOD_0]
    ]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Multiplicative Inverse MOD n (outside Group theory)                       *)
(* ------------------------------------------------------------------------- *)

(* Use Skolemization to define the multiplicative inverse *)

(* Lemma: to introduce mod_inv *)
(* Proof:
MOD_MULT_INV_EXISTS
|- !p x. prime p /\ 0 < x /\ x < p ==> ?y. 0 < y /\ y < p /\ (y * x) MOD p = 1
*)
Theorem lemma[local]:
  !n x. ?y. prime n /\ 0 < x /\ x < n ==> 0 < y /\ y < n /\ (x * y) MOD n = 1
Proof
  metis_tac[MOD_MULT_INV_EXISTS, MULT_COMM]
QED

(*
SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma;
> val it =
   |- ?f. !n x.
        prime n /\ 0 < x /\ x < n ==>
        0 < f n x /\ f n x < n /\ (x * f n x) MOD n = 1: thm
*)

(* Define mod_inv x n = multiplicative inverse of x in MOD n. *)
val mod_inv_def = new_specification(
    "mod_inv_def",
    ["mod_inv"],
    SIMP_RULE (srw_ss()) [SKOLEM_THM] lemma);

(*
val mod_inv_def =
   |- !n x. prime n /\ 0 < x /\ x < n ==>
            0 < mod_inv n x /\ mod_inv n x < n /\ (x * mod_inv n x) MOD n = 1: thm
*)

(* Idea: for prime n, the inverse of inverse of MOD n is itself. *)

(* Theorem: prime n /\ 0 < x /\ x < n ==> mod_inv n (mod_inv n x) = x *)
(* Proof:
   Let y = mod_inv n x,
       z = mod_inv n y.
   This is to show z = x.
   By mod_inv_def,
       0 < y /\ y < n /\ (x * y) MOD n = 1
   and 0 < z /\ z < n /\ (y * z) MOD n = 1
   Thus  (x * y) MOD n = (z * y) MOD n
    Now y MOD n = y <> 0           by LESS_MOD
    ==> x MOD n = z MOD n          by MOD_MULT_RCANCEL, prime n
    ==>       x = z                by LESS_MOD
*)
Theorem mod_inv_inv:
  !n x. prime n /\ 0 < x /\ x < n ==> mod_inv n (mod_inv n x) = x
Proof
  rpt strip_tac >>
  qabbrev_tac `y = mod_inv n x` >>
  qabbrev_tac `z = mod_inv n y` >>
  `0 < y /\ y < n /\ (x * y) MOD n = 1` by metis_tac[mod_inv_def] >>
  `0 < z /\ z < n /\ (y * z) MOD n = 1` by metis_tac[mod_inv_def] >>
  `(x * y) MOD n = (z * y) MOD n` by fs[] >>
  `y MOD n <> 0` by simp[] >>
  `x MOD n = z MOD n` by metis_tac[MOD_MULT_RCANCEL] >>
  rfs[]
QED

(* Idea: for prime n, the mod_inv n x is unique. *)

(* Theorem: prime n /\ 0 < x /\ x < n ==> !y. 0 < y /\ y < n /\ (x * y) MOD n = 1 <=> y = mod_inv n x *)
(* Proof:
   If part: 0 < y /\ y < n /\ (x * y) MOD n = 1 ==> y = mod_inv n x
      Let z = mod_inv n x.
      Then (x * z) MOD n = 1       by mod_inv_def
        so (x * y) MOD n = (x * z) MOD n       by (x * y) MOD n = 1
        Now x MOD n = x <> 0       by LESS_MOD
        ==> y MOD n = z MOD n      by MOD_MULT_LCANCEL, prime n
        ==>       y = z            by LESS_MOD

   Only-if part: y = mod_inv n x ==> 0 < y /\ y < n /\ (x * y) MOD n = 1
      True                         by mod_inv_def
*)
Theorem mod_inv_unique:
  !n x. prime n /\ 0 < x /\ x < n ==> !y. 0 < y /\ y < n /\ (x * y) MOD n = 1 <=> y = mod_inv n x
Proof
  rw[mod_inv_def, EQ_IMP_THM] >>
  qabbrev_tac `z = mod_inv n x` >>
  `z < n /\ (x * z) MOD n = 1` by simp[mod_inv_def, Abbr`z`] >>
  `x MOD n <> 0` by simp[] >>
  `y MOD n = z MOD n` by metis_tac[MOD_MULT_LCANCEL] >>
  rfs[]
QED

(* Idea: for a prime n, (mod_inv n) maps over (natural (n-1)) *)

(* Theorem: prime n ==> (mod_inv n) endo (natural (n - 1)) *)
(* Proof:
   Let s = natural (n - 1)
   Then s = {j | 0 < j /\ j < n}   by natural_one_less
   This is to show: (mod_inv n) endo s <=>
      !x. 0 < x /\ x < n ==> 0 < mod_inv n /\ mod_inv n x < n
   which is true                   by mod_inv_def
*)
Theorem mod_inv_endo:
  !n. prime n ==> (mod_inv n) endo (natural (n - 1))
Proof
  rewrite_tac[natural_one_less] >>
  rw[mod_inv_def]
QED

(* Idea: for a prime n, (mod_inv n) is an involution over (natural (n-1)) *)

(* Theorem: prime n ==> (mod_inv n) involute (natural (n - 1)) *)
(* Proof:
   By natural_element, mod_inv_def, this is to show:
   (1) x <= n - 1 ==> 0 < mod_inv n x
       Note x < n, so 0 < mod_inv n x          by mod_inv_def
   (2) x <= n - 1 ==> mod_inv n x <= n - 1
       Note x < n, so mod_inv n x < n          by mod_inv_def
   (3) x <= n - 1 ==>
       mod_inv n (mod_inv n x) = x, true       by mod_inv_inv
*)
Theorem mod_inv_involute:
  !n. prime n ==> (mod_inv n) involute (natural (n - 1))
Proof
  rw[natural_element] >-
  fs[mod_inv_def] >-
 (`mod_inv n x < n` suffices_by simp[] >>
  fs[mod_inv_def]) >>
  fs[mod_inv_inv]
QED

(* Idea: for prime n, the self inverses are x = +1 or -1. *)

(* Theorem: prime n /\ 0 < x /\ x < n ==> (mod_inv n x = x <=> x = 1 \/ x = n - 1) *)
(* Proof:
   First, show that: mod_inv n x = x <=> (x ** 2) MOD n = 1.
      If part: mod_inv n x = x ==> (x ** 2) MOD n = 1
         Note (x * mod_inv n x) MOD n = 1      by mod_inv_def
           so           (x * x) MOD n = 1      by mod_inv n x = x
           or          (x ** 2) MOD n = 1      by EXP_2

      Only-if part: (x ** 2) MOD n = 1 ==> mod_inv n x = x
         Note        (x * x) MOD n = 1         by EXP_2
           so          mod_inv n x = x         by mod_inv_unique

   Thus the goal is: (x ** 2) MOD n = 1 <=> x = 1 \/ x = n - 1
        (x ** 2) MOD n = 1
    <=> x MOD n = 1 \/ x MOD n = n - 1         by MOD_SQ_EQ_1_prime
    <=>       x = 1 \/       x = n - 1         by LESS_MOD
*)
Theorem mod_inv_eq_self:
  !n x. prime n /\ 0 < x /\ x < n ==> (mod_inv n x = x <=> x = 1 \/ x = n - 1)
Proof
  rpt strip_tac >>
  `mod_inv n x = x <=> (x ** 2) MOD n = 1` by
  (rw[EQ_IMP_THM] >-
  metis_tac[mod_inv_def, EXP_2] >>
  metis_tac[EXP_2, mod_inv_unique]
  ) >>
  `(x ** 2) MOD n = 1 <=> x = 1 \/ x = n - 1` suffices_by fs[] >>
  simp[MOD_SQ_EQ_1_prime]
QED

(* Idea: the fixes of (mod_inv n) over (natural (n-1)) has only 1 and -1. *)

(* Theorem: prime n ==> fixes (mod_inv n) (natural (n - 1)) = {1; n - 1} *)
(* Proof:
   Note s = natural (n - 1).
   Then s = {j | 0 < j /\ j < n}               by natural_one_less
   By fixes_def, EXTENSION, this is to show:
      (0 < x /\ x < n) /\ mod_inv n x = x <=> x = 1 \/ x = n - 1
   But 0 < 1 /\ 1 < n                          by ONE_LT_PRIME
    so 0 < n - 1 /\ n - 1 < n                  by 1 < n
   This changes the goal to:
      0 < x /\ x < n ==> (mod_inv n x = x <=> x = 1 \/ x = n - 1)
   This is true                                by mod_inv_eq_self
*)
Theorem mod_inv_fixes:
  !n. prime n ==> fixes (mod_inv n) (natural (n - 1)) = {1; n - 1}
Proof
  simp[natural_one_less] >>
  rw[fixes_def, EXTENSION] >>
  `1 < n` by simp[ONE_LT_PRIME] >>
  `0 < x /\ x < n ==> (mod_inv n x = x <=> x = 1 \/ x = n - 1)` suffices_by simp[] >>
  simp[mod_inv_eq_self]
QED

(* Idea: the pairs of (mod_inv n) over (natural (n-1)) excludes 1 and -1. *)

(* Theorem: prime n ==> pairs (mod_inv n) (natural (n - 1)) = (natural (n - 1)) DIFF {1; n - 1} *)
(* Proof:
   Note s = natural (n - 1).
     pairs (mod_inv n) s
   = s DIFF fixes (mod_inv n) s    by pairs_by_diff
   = s DIFF {1; n - 1}             by mod_inv_fixes
*)
Theorem mod_inv_pairs:
  !n. prime n ==> pairs (mod_inv n) (natural (n - 1)) = (natural (n - 1)) DIFF {1; n - 1}
Proof
  simp[pairs_by_diff, mod_inv_fixes]
QED

(* Theorem: prime n ==> CARD (fixes (mod_inv n) (natural (n - 1))) = if n = 2 then 1 else 2 *)
(* Proof:
   Let s = natural (n - 1),
       f = mod_inv n.
   Note 1 < n                  by ONE_LT_PRIME
     CARD (fixes f s)
   = CARD {1; n - 1}           by mod_inv_fixes

   If n = 2, n - 1 = 1.
      CARD (fixes f s) = CARD {1} = 1
   Otherwise 2 < n, 1 <> n - 1.
      CARD (fixes f s) = CARD {1; n - 1} = 2
*)
Theorem mod_inv_fixes_card:
  !n. prime n ==> CARD (fixes (mod_inv n) (natural (n - 1))) = if n = 2 then 1 else 2
Proof
  simp[mod_inv_fixes, ONE_LT_PRIME]
QED

(* Theorem: prime n ==> CARD (pairs (mod_inv n) (natural (n - 1))) = n - 3 *)
(* Proof:
   Let s = natural (n - 1),
       f = mod_inv n.
   Note FINITE s                               by natural_finite
     so FINITE (fixes f s) /\                  by fixes_finite
        FINITE (pairs f s)                     by pairs_finite
    and s = fixes f s UNION pairs f s          by fixes_pairs_union
    and DISJOINT (fixes f s) (pairs f s)       by fixes_pairs_disjoint

       CARD (fixes f s) + CARD (pairs f s)
     = CARD s                                  by CARD_UNION_DISJOINT
     = n - 1                                   by natural_card

   Thus CARD (pairs f s)
      = (n - 1) - CARD (fixes f s)
      = (n - 1) - (if n = 2 then 1 else 2)     by mod_inv_fixes_card
      = if n = 2 then 0 else n - 3
      = n - 3                                  by integer arithmetic
*)
Theorem mod_inv_pairs_card:
  !n. prime n ==> CARD (pairs (mod_inv n) (natural (n - 1))) = n - 3
Proof
  rpt strip_tac >>
  qabbrev_tac `s = natural (n - 1)` >>
  qabbrev_tac `f = mod_inv n` >>
  qabbrev_tac `s1 = fixes f s` >>
  qabbrev_tac `s2 = pairs f s` >>
  `FINITE s /\ FINITE s1 /\ FINITE s2` by simp[natural_finite, fixes_finite, pairs_finite, Abbr`s`, Abbr`s1`, Abbr`s2`] >>
  `s = s1 UNION s2 /\ DISJOINT s1 s2` by simp[fixes_pairs_union, fixes_pairs_disjoint, Abbr`s1`, Abbr`s2`] >>
  `CARD s1 + CARD s2 = n - 1` by metis_tac[CARD_UNION_DISJOINT, natural_card, Abbr`s`] >>
  rfs[mod_inv_fixes_card, Abbr`s1`, Abbr`f`, Abbr`s`]
QED

(* Theorem: prime n ==> (pairs (mod_inv n) (natural (n - 1)) = {} <=> n <= 3) *)
(* Proof:
   Let s = natural (n - 1),
       f = mod_inv n.
   Note FINITE s                   by natural_finite
     so FINITE (pairs f s)         by pairs_finite

        pairs f s = {}
    <=> CARD (pairs f s) = 0       by CARD_EQ_0
    <=>            n - 3 = 0       by mod_inv_pairs_card
    <=>               n <= 3       by SUB_EQ_0
*)
Theorem mod_inv_pairs_empty:
  !n. prime n ==> (pairs (mod_inv n) (natural (n - 1)) = {} <=> n <= 3)
Proof
  rpt strip_tac >>
  qabbrev_tac `s = natural (n - 1)` >>
  qabbrev_tac `f = mod_inv n` >>
  `FINITE s /\ FINITE (pairs f s)` by simp[natural_finite, pairs_finite, Abbr`s`] >>
  metis_tac[CARD_EQ_0, mod_inv_pairs_card, SUB_EQ_0]
QED

(* Theorem: prime n ==> ONE_to_ONE (\x. if x IN (natural (n - 1)) then mod_inv n x else x) *)
(* Proof:
   Let s = natural (n - 1).
   Then s = {x | 0 < x /\ x < n}   by natural_one_less
   This is to show:
        (if x1 IN s then mod_inv n x1 else x1) = (if x2 IN s then mod_inv n x2 else x2)
     ==> x1 = x2
   If x1 IN s, x2 IN s,
          mod_inv n x1 = mod_inv n x2
      ==> mod_inv n (mod_inv n x1) = mod_inv n (mod_inv n x2)
      ==>                       x1 = x2        by mod_inv_inv
   If x1 NOTIN s, x2 NOTIN s,
          x1 = x2 ==> x1 = x2 is trivially true.
   If x1 IN s, x2 NOTIN s,
          mod_inv n x1 = x2 is impossible      by mod_inv_def
   If x1 NOTIN s, x2 IN s,
          x1 = mod_inv n x2 is impossible      by mod_inv_def
*)
Theorem mod_inv_11:
  !n. prime n ==> ONE_to_ONE (\x. if x IN (natural (n - 1)) then mod_inv n x else x)
Proof
  rpt strip_tac >>
  qabbrev_tac `s = natural (n - 1)` >>
  `s = {x | 0 < x /\ x < n}` by metis_tac[natural_one_less] >>
  `!x. x IN s <=> 0 < x /\ x < n` by fs[] >>
  Cases_on `x1 IN s /\ x2 IN s` >-
  metis_tac[mod_inv_inv] >>
  fs[] >| [
    Cases_on `x2 IN s` >-
    metis_tac[mod_inv_def] >>
    rfs[],
    Cases_on `x1 IN s` >-
    metis_tac[mod_inv_def] >>
    rfs[]
  ]
QED
(* But this is of no use, I need the doublet function (\x. {x; f x}) to be ONE_to_ONE, or INJ, which is not! *)

(* Theorem: prime n ==>
            EVERY ($= 1) (MAP (\s. PROD_SET s MOD n) (SET_TO_LIST (equiv_pairs (mod_inv n) (natural (n - 1))))) *)
(* Proof:
   Let s = natural (n - 1),
       f = mod_inv n,
       P = equiv_pairs f s,
       g = \s. PROD_SET s MOD n.
   The goal is to show: EVERY ($= 1) (MAP g (SET_TO_LIST P))

   Note FINITE t                               by natural_finite
     so FINITE P                               by equiv_pairs_finite
   also f endo s                               by mod_inv_endo, prime n
    and P = IMAGE (\x. {x; f x}) (pairs f s)   by equiv_pairs_by_image

       EVERY ($= 1) (MAP g (SET_TO_LIST P))
   <=> EVERY (\x. ($= 1) (g x)) (SET_TO_LIST P)                by EVERY_MAP
   <=> !e. MEM e (SET_TO_LIST P) ==> (\x. ($= 1) (g x)) e      by EVERY_MEM
   <=> !e. e IN P ==> g e = 1                                  by MEM_SET_TO_LIST, FINITE P
   <=> !e. ?x IN (pairs f s) /\ e = {x; f x} ==> g e = 1       by IN_IMAGE, or equiv_pairs_element_doublet

   Note x IN s /\ f x <> x                     by pairs_element
     so 0 < x /\ x < n                         by natural_one_less

        g e = PROD_SET {x; f x} MOD n
      = (x * f x) MOD n                        by PROD_SET_doublet
      = (x * mod_inv n x) MOD n                by function application
      = 1                                      by mod_inv_def, prime n
*)
Theorem mod_inv_equiv_pairs_thm:
  !n. prime n ==> EVERY ($= 1) (MAP (\s. PROD_SET s MOD n) (SET_TO_LIST (equiv_pairs (mod_inv n) (natural (n - 1)))))
Proof
  rpt strip_tac >>
  qabbrev_tac `s = natural (n - 1)` >>
  qabbrev_tac `f = mod_inv n` >>
  qabbrev_tac `P = equiv_pairs f s` >>
  qabbrev_tac `g = \s. PROD_SET s MOD n` >>
  rw[EVERY_MAP, EVERY_MEM] >>
  `FINITE s` by simp[natural_finite, Abbr`s`] >>
  `FINITE P` by simp[equiv_pairs_finite, Abbr`P`] >>
  `f endo s` by simp[mod_inv_endo, Abbr`f`, Abbr`s`] >>
  `P = IMAGE (\x. {x; f x}) (pairs f s)` by simp[equiv_pairs_by_image, Abbr`P`] >>
  `x IN P` by simp[GSYM MEM_SET_TO_LIST] >>
  qabbrev_tac `h = \x. {x; f x}` >>
  `?y. y IN (pairs f s) /\ x = h y` by metis_tac[IN_IMAGE] >>
  `_ = {y; mod_inv n y}` by fs[Abbr`h`, Abbr`f`] >>
  `0 < y /\ y < n /\ f y <> y` by fs[pairs_element, natural_one_less, Abbr`s`] >>
  `g x = PROD_SET {y; f y} MOD n` by rfs[Abbr`g`] >>
  `_ = (y * mod_inv n y) MOD n` by simp[GSYM PROD_SET_doublet, Abbr`f`] >>
  simp[mod_inv_def]
QED

(* Idea: the pairs of (mod_inv n) over (natural (n-1)) excludes +1 and -1, and PROD MOD n is 1. *)

(* Theorem: prime n ==> PROD_SET (pairs (mod_inv n) (natural (n - 1))) MOD n = 1 *)
(* Proof:
   Let s = natural (n - 1),
       f = mod_inv n.
   Then FINITE s                   by natural_finite
    and f involute s               by mod_inv_involute, prime n

   Let P = equiv_pairs f s,
       g = \s. PROD_SET s MOD n.
   Then FINITE P                   by equiv_pairs_finite
    and EVERY_FINITE P             by equiv_pairs_every_finite
    and PAIR_DISJOINT P            by equiv_pairs_pair_disjoint, f involute s

   Let ls = MAP g (SET_TO_LIST P).
   Then EVERY ($= 1) ls            by mod_inv_equiv_pairs_thm, prime n
    Now 1 < n                      by ONE_LT_PRIME

       (PROD_SET (pairs f s)) MOD n
     = (PROD_SET (BIGUNION P)) MOD n  by equiv_pairs_bigunion, f involute s
     = (PROD ls) MOD n             by PROD_SET_MOD_BIGUNION_eq_PROD_MAP_MOD
     = (1 ** LENGTH (ls)) MOD n    by PROD_K_LIST
     = 1 MOD n                     by EXP_1
     = 1                           by ONE_MOD, 1 < n
*)
Theorem mod_inv_pairs_prod_mod:
  !n. prime n ==> PROD_SET (pairs (mod_inv n) (natural (n - 1))) MOD n = 1
Proof
  rpt strip_tac >>
  qabbrev_tac `s = natural (n - 1)` >>
  qabbrev_tac `f = mod_inv n` >>
  qabbrev_tac `P = equiv_pairs f s` >>
  qabbrev_tac `g = \s. PROD_SET s MOD n` >>
  qabbrev_tac `ls = MAP g (SET_TO_LIST P)` >>
  `f involute s` by simp[mod_inv_involute, Abbr`f`, Abbr`s`] >>
  `FINITE s` by simp[natural_finite, Abbr`s`] >>
  `FINITE P` by simp[equiv_pairs_finite, Abbr`P`] >>
  `EVERY_FINITE P` by metis_tac[equiv_pairs_every_finite] >>
  `PAIR_DISJOINT P` by metis_tac[equiv_pairs_pair_disjoint] >>
  `EVERY ($= 1) ls` by simp[mod_inv_equiv_pairs_thm, Abbr`ls`, Abbr`g`, Abbr`P`, Abbr`f`, Abbr`s`] >>
  `PROD_SET (pairs f s) MOD n = PROD_SET (BIGUNION P) MOD n` by simp[equiv_pairs_bigunion, Abbr`P`] >>
  `_ = PROD ls MOD n` by simp[PROD_SET_MOD_BIGUNION_eq_PROD_MAP_MOD, Abbr`ls`, Abbr`g`] >>
  metis_tac[PROD_K_LIST, EXP_1, ONE_MOD, ONE_LT_PRIME]
QED

(* Idea: the fixes of (mod_inv n) over (natural (n-1)) has only +1 and -1, and PROD MOD n is n - 1. *)

(* Theorem: prime n ==> PROD_SET (fixes (mod_inv n) (natural (n - 1))) MOD n = n - 1 *)
(* Proof:
   Let s = natural (n - 1),
       f = mod_inv n.
   Note 1 < n                      by ONE_LT_PRIME
    and fixes f s = {1; n - 1}     by mod_inv_fixes

   If n = 2, n - 1 = 1.
        PROD_SET (fixes f s) MOD n
      = PROD_SET {1} MOD n         by above
      = 1 MOD n                    by PROD_SET_SING
      = 1 = n - 1                  by ONE_MOD, 1 < n
   Otherwise n <> 2, n - 1 <> 1.
        PROD_SET (fixes f s) MOD n
      = PROD_SET {1; n - 1} MOD n  by above
      = (1 * (n - 1)) MOD n        by PROD_SET_doublet
      = (n - 1) MOD n = n - 1      by LESS_MOD, n - 1 < n
*)
Theorem mod_inv_fixes_prod_mod:
  !n. prime n ==> PROD_SET (fixes (mod_inv n) (natural (n - 1))) MOD n = n - 1
Proof
  rpt strip_tac >>
  qabbrev_tac `s = natural (n - 1)` >>
  qabbrev_tac `f = mod_inv n` >>
  `1 < n` by simp[ONE_LT_PRIME] >>
  `fixes f s = {1; n - 1}` by simp[mod_inv_fixes, Abbr`f`, Abbr`s`] >>
  Cases_on `n = 2` >-
  fs[PROD_SET_SING] >>
  fs[PROD_SET_doublet]
QED

(* Idea: a prime number n has FACT (n - 1) MOD n = n - 1, always. *)

(* Theorem: prime n ==> FACT (n - 1) MOD n = n - 1 *)
(* Proof:
   Let f = mod_inv n,
       s = natural (n - 1),
       s1 = fixes f s,
       s2 = pairs f s.
   Then 1 < n                      by ONE_LT_PRIME
    and FINITE s                   by natural_finite
     so FINITE s1                  by fixes_finite
    and FINITE s2                  by pairs_finite
    and DISJOINT s1 s2             by fixes_pairs_disjoint

        FACT (n - 1) MOD n
      = PROD_SET s MOD n                       by FACT_BY_PROD_SET
      = PROD_SET (s1 UNION s2) MOD n           by fixes_pairs_union
      = (PROD_SET s1 * PROD_SET s2) MOD n      by PROD_SET_UNION_DISJOINT
      = (PROD_SET s1 MOD n * PROD_SET s2 MOD n) MOD n    by MOD_PRODUCT
      = (1 * (n - 1)) MOD n                    by mod_inv_pairs_prod_mod, mod_inv_fixes_prod_mod
      = (n - 1) MOD n = n - 1                  by LESS_MOD, 1 < n
*)
Theorem FACT_one_less_prime:
  !n. prime n ==> FACT (n - 1) MOD n = n - 1
Proof
  rpt strip_tac >>
  qabbrev_tac `s = natural (n - 1)` >>
  qabbrev_tac `f = mod_inv n` >>
  qabbrev_tac `s1 = fixes f s` >>
  qabbrev_tac `s2 = pairs f s` >>
  `1 < n` by simp[ONE_LT_PRIME] >>
  `FINITE s /\ FINITE s1 /\ FINITE s2` by simp[natural_finite, pairs_finite, fixes_finite, Abbr`s`, Abbr`s1`, Abbr`s2`] >>
  `s = s1 UNION s2 /\ DISJOINT s1 s2` by simp[fixes_pairs_union, fixes_pairs_disjoint, Abbr`s1`, Abbr`s2`] >>
  `FACT (n - 1) MOD n = PROD_SET (s1 UNION s2) MOD n` by simp[FACT_BY_PROD_SET] >>
  `_ = (PROD_SET s1 * PROD_SET s2) MOD n` by simp[PROD_SET_UNION_DISJOINT] >>
  `_ = (PROD_SET s1 MOD n * PROD_SET s2 MOD n) MOD n` by simp[GSYM MOD_PRODUCT] >>
  `_ = (1 * (n - 1)) MOD n` by simp[mod_inv_pairs_prod_mod, mod_inv_fixes_prod_mod, Abbr`s`, Abbr`f`, Abbr`s1`, Abbr`s2`] >>
  fs[]
QED

(* ------------------------------------------------------------------------- *)
(* Wilson's Theorem (1770)                                                   *)
(* ------------------------------------------------------------------------- *)

(* Wilson's Theorem: a number n is prime iff FACT (n - 1) MOD n = -1. *)

(* Theorem: prime n <=> 1 < n /\ FACT (n - 1) MOD n = n - 1 *)
(* Proof:
   If part: prime n ==> 1 < n /\ FACT (n - 1) MOD n = n - 1
      Note 1 < n                               by ONE_LT_PRIME
       and FACT (n - 1) MOD n = n - 1          by FACT_one_less_prime
   Only-if part: 1 < n /\ FACT (n - 1) MOD n = n - 1 ==> prime n
      By contradiction, suppose ~prime n.
      One way: by FACT_one_less_composite, ~prime n ==>
         FACT (n - 1) MOD n = if n = 0 then 1 else if n = 4 then 2 else 0
         which is never (n - 1) for 1 < n.
      Another way.
      Note 1 < n
       ==> ?a. 1 < a /\ a < n /\ a divides n   by PROPER_FACTOR
      Thus a divides FACT (n - 1)              by FACT_factor

           (FACT (n - 1) + 1) MOD n
         = (FACT (n - 1) MOD n + 1) MOD n      by MOD_SUM, ONE_MOD, 1 < n
         = (n - 1 + 1) MOD n                   by FACT (n - 1) MOD n = n - 1
         = 0 MOD n = 0                         by ZERO_MOD, 0 < n
      Thus n divides (FACT (n - 1) + 1)        by DIVIDES_MOD_0, 0 < n
        or a divides (FACT (n - 1) + 1)        by DIVIDES_TRANS, a divides n
       ==> a divides 1                         by DIVIDES_ADD_2
      This makes a = 1, contradicting 1 < a    by DIVIDES_ONE
*)
Theorem Wilson_thm:
  !n. prime n <=> 1 < n /\ FACT (n - 1) MOD n = n - 1
Proof
  rw[EQ_IMP_THM] >-
  simp[ONE_LT_PRIME] >-
  simp[FACT_one_less_prime] >>
  spose_not_then strip_assume_tac >>
  `?a. 1 < a /\ a < n /\ a divides n` by simp[PROPER_FACTOR] >>
  `(FACT (n - 1) + 1) MOD n = (n - 1 + 1) MOD n` by metis_tac[MOD_SUM, ONE_MOD] >>
  `_ = 0` by simp[DIVMOD_ID] >>
  `0 < n /\ 0 < a /\ a <= n - 1` by decide_tac >>
  `a divides (FACT (n - 1) + 1)` by metis_tac[DIVIDES_MOD_0, DIVIDES_TRANS] >>
  `a = 1` by metis_tac[FACT_factor, DIVIDES_ADD_2, DIVIDES_ONE] >>
  decide_tac
QED

(* Theorem alias *)
Theorem Wilson_congruence = FACT_one_less_prime;
(* val Wilson_congruence = |- !n. prime n ==> FACT (n - 1) MOD n = n - 1: thm *)

(*

Proving Wilson's theorem
https://math.stackexchange.com/questions/1850437
One has indeed an equivalence
p is prime ⇔ (p−1)! ≡ −1 (mod p)

(1) p is prime ⇒ (p−1)! ≡ −1 (mod p)
By Fermat's little theorem (a^(p−1) ≡ 1 (mod p)) and because each of 1,2,3,...(p−2),(p−1) is coprime with p, we have
   (p−2)! ≡ ((p−2)!)^(p−1) ≡ 1 (mod p)
 ⇒ (p−1)! ≡ (p−1)⋅1 ≡ −1 (mod p).

For p = 7, 1^6 ≡ 1, 2^6 = 64 ≡ 1, 3^6 = 729 ≡ 1, 4^6 ≡ (-3)^6 ≡ 1, 5^6 ≡ (-2)^6 ≡ 1, 6^6 ≡ (-1)^6 ≡ 1.
Yes, ((p−2)!)^(p−1) ≡ 1 (mod p), but so is ((p−1)!)^(p−1) ≡ 1 (mod p)
Why (p−2)! ≡ ((p−2)!)^(p−1), but not (p−1)! ≡ ((p−1)!)^(p−1) ??

(2) (p−1)! ≡ −1 (mod p) ⇒ p is prime.
Suppose p is composite then its (positive) divisors are in {1,2,3,...,(p−2),(p−1)}.
This implies that  gcd((p−1)!,p) > 1.
Now if (p−1)! ≡ −1 (mod p) then dividing by a (proper) divisor d of p and of (p−1)!
the equality (p−1)! = −1 + pm (equivalent to the congruence) one has d must divide −1, absurd.
Thus p is not composite.
*)

(* ------------------------------------------------------------------------- *)
(* Fermat's Little Theorem (induction proof)                                 *)
(* ------------------------------------------------------------------------- *)

(* Theorem: prime n ==> (a ** n) MOD n = a MOD n *)
(* Proof:
   By induction on a.
   Base: 0 ** n MOD n = 0 MOD n
        Note n <> 0                by NOT_PRIME_0
        0 ** n MOD n
      = 0 MOD n                    by ZERO_MOD, 0 < n
   Step: a ** n MOD n = a MOD n
     ==> SUC a ** n MOD n = SUC a MOD n

        Note n <> 0                by NOT_PRIME_0
        SUC a ** n MOD n
      = (a + 1) ** n MOD n         by ADD1
      = (a ** n + 1 ** n) MOD n    by binomial_thm_prime, prime n
      = (a ** n MOD n + 1 MOD n) MOD n   by MOD_SUM, EXP_1
      = (a + 1) MOD n              by induction hypothesis, ONE_MOD, 1 < n
      = SUC a MOD n                by ADD1
*)
Theorem Fermat_congruence:
  !n a. prime n ==> (a ** n) MOD n = a MOD n
Proof
  rpt strip_tac >>
  Induct_on `a` >-
  metis_tac[ZERO_EXP, NOT_PRIME_0] >>
  metis_tac[binomial_thm_prime, EXP_1, ADD1, MOD_SUM, ONE_LT_PRIME]
QED

(* ------------------------------------------------------------------------- *)
(* Moser Theorem (1956)                                                      *)
(* ------------------------------------------------------------------------- *)

(* Theorem: prime n ==> (FACT (n - 1) * a ** n + (n - 1) ** 2 * a) MOD n = 0 *)
(* Proof:
   Note 0 < n,                                 by PRIME_POS
    and n - 1 < n, and 1 + (n - 1) = n         by arithmetic

     (FACT (n - 1) * a ** n + (n - 1) ** 2 * a) MOD n
   = (FACT (n - 1) MOD n * (a ** n) MOD n +    by MOD_PRODUCT
      (n - 1) ** 2 MOD n * a MOD n) MOD n      by MOD_SUM
   = ((n - 1) * a MOD n +                      by Wilson_congruence, Fermat_congruence, prime n
      ((n - 1) * (n - 1)) MOD n * a MOD n) MOD n               by EXP_2
   = ((n - 1) * a MOD n + (n - 1) * (n - 1) * a MOD n) MOD n   by MOD_PRODUCT, LESS_MOD, n - 1 < n
   = ((1 + (n - 1)) * ((n - 1) * a MOD n)) MOD n               by RIGHT_ADD_DISTRIB
   = (n * ((n - 1) * a MOD n)) MOD n                           by 1 + (n - 1) = n
   = 0                                                         by MOD_EQ_0, 0 < n
*)
Theorem Moser_congruence:
  !n a. prime n ==> (FACT (n - 1) * a ** n + (n - 1) ** 2 * a) MOD n = 0
Proof
  rpt strip_tac >>
  `0 < n` by simp[PRIME_POS] >>
  `n - 1 < n /\ 1 + (n - 1) = n` by decide_tac >>
  `(FACT (n - 1) * a ** n + (n - 1) ** 2 * a) MOD n = (FACT (n - 1) MOD n * (a ** n) MOD n + (n - 1) ** 2 MOD n * a MOD n) MOD n` by simp[GSYM MOD_SUM, GSYM MOD_PRODUCT] >>
  `_ = ((n - 1) * a MOD n + ((n - 1) * (n - 1)) MOD n * a MOD n) MOD n` by simp[Wilson_congruence, Fermat_congruence, GSYM EXP_2] >>
  `_ = ((n - 1) * a MOD n + (n - 1) * (n - 1) * a MOD n) MOD n` by simp[GSYM MOD_PRODUCT, LESS_MOD] >>
  `_ = ((1 + (n - 1)) * ((n - 1) * a MOD n)) MOD n` by fs[RIGHT_ADD_DISTRIB] >>
  simp[MOD_EQ_0]
QED

(*

On theorems of Fermat, Wilson, and Gegenbauer
by Heng Huat Chan, Song Heng Chan, Teoh Guan Chua and Cheng Yeaw Ku.
Published online by Cambridge University Press:  12 September 2023
https://www.cambridge.org/core/journals/canadian-mathematical-bulletin/article/on-theorems-of-fermat-wilson-and-gegenbauer/C660F587216E3645CF8947A5A80940F6
In this article, we give generalizations of the well-known Fermat’s Little Theorem, Wilson’s theorem, and the little-known Gegenbauer’s theorem.
A nice article about generalisations, and proofs of congruence theorems by group action.

Around 1956, Moser [13] discovered the congruence
(p − 1)!a^p + (p − 1)^2 a ≡ 0 (mod p).(1.3)
By setting a = 1 in (1.3), one obtained Wilson's Theorem (1.2), and using (1.2) in (1.3), we deduced Fermat's Little Theorem (1.1).
On the other hand, (1.3) follows immediately from Wilson's Theorem (1.2) and Fermat's Little Theorem (1.1).

*)


(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
