(* ------------------------------------------------------------------------- *)
(* Windmills of the minds: Fermat's Two Squares Theorem (part 6)             *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "part6";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)

(* val _ = load "helperTwosqTheory"; *)
open helperTwosqTheory; (* for square_def, square_alt *)
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory gcdTheory;

(* val _ = load "involuteFixTheory"; *)
open involuteTheory;
open involuteFixTheory;

open listTheory;
open rich_listTheory;
open helperListTheory;
open listRangeTheory;

(* val _ = load "GaussTheory"; *)
open GaussTheory; (* for divisors_cofactor *)

(* val _ = load "groupActionTheory"; *)
open groupTheory; (* for group_inv_element *)
open groupActionTheory; (* for action_def *)

open logPowerTheory; (* for SQRT_SQ *)


(* ------------------------------------------------------------------------- *)
(* Windmills of the minds (part 6) Documentation                             *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Miscellaneous investigations.                                             *)
(* ------------------------------------------------------------------------- *)
(* Overloading (# is temporary):
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   Squares have odd number of divisors:
   cofactor_involute_divisors
                           |- !n. $DIV n involute divisors n
   cofactor_divisors_fixes_sing
                           |- !n. 0 < n ==> (square n <=> SING (fixes ($DIV n) (divisors n)))
   divisors_card_even      |- !n. EVEN (CARD (divisors n)) <=> n = 0 \/ ~square n
   divisors_card_odd       |- !n. ODD (CARD (divisors n)) <=> 0 < n /\ square n
   divisors_card_even_alt  |- !n. 0 < n ==> (EVEN (CARD (divisors n)) <=> ~square n)
   divisors_card_odd_alt   |- !n. 0 < n ==> (ODD (CARD (divisors n)) <=> square n)

   Inverse action of a group:
   group_inv_involute  |- !g. Group g ==> |/ involute G
   group_op_action     |- !g. Group g ==> (g act G) $*

*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)



(* ------------------------------------------------------------------------- *)
(* Squares have an odd number of divisors.                                   *)
(* ------------------------------------------------------------------------- *)

(* The oldest theorem in number theory:
   A number has an odd number of divisors iff it is a square.
*)

(* Idea: cofactor is an involution over divisors. *)

(* Theorem: ($DIV n) involute (divisors n) *)
(* Proof:
   Let f = (\j. n DIV j).
   For x IN divisors n,
   Then 0 < x /\ x <= n /\ x divides n         by divisors_def
     so 0 < n DIV x                            by DIV_POS, 0 < x
    and n DIV x <= n                           by DIV_LESS_EQ, 0 < x
    and (n DIV x) divides n                    by DIVIDES_COFACTOR, 0 < x
     so f x IN divisors n                      by divisors_element_alt, 0 < n
        f (f x)
      = n DIV (n DIV x)                        by notation
      = x                                      by divide_by_cofactor, 0 < x
   Thus f involute (divisors n)                by notation
*)
Theorem cofactor_involute_divisors:
  !n. ($DIV n) involute (divisors n)
Proof
  simp[divisors_def] >>
  ntac 3 strip_tac >>
  simp[DIV_POS, DIV_LESS_EQ, DIVIDES_COFACTOR, divide_by_cofactor]
QED

(* This is better than: divisors_divisors_bij. *)

(* Idea: only a square has (fixes cofactor divisors) a singleton. *)

(* Theorem: 0 < n ==> (square n <=> SING (fixes ($DIV n) (divisors n))) *)
(* Proof:
   Let s = divisors n,
       f = ($DIV n).
   If part: square n ==> SING (fixes f s)
      Note ?m. n = m * m         by square_def
        so m divides n           by divides_def
      Thus 0 < m                 by ZERO_DIVIDES, 0 < n
       ==> f m = m               by factor_eq_cofactor, 0 < m
       Now m IN s                by divisors_element_alt, 0 < n
        so m IN fixes f s        by fixes_element
      To show: fixes f s = {m}   by SING_DEF
      need to prove              by EXTENSION
           !x. x IN fixes f x ==> x = m
      Note x IN s /\ (f x = x)   by fixes_element
        so 0 < x /\ x divides n  by divisors_element
       and n DIV x = x           by notation
        so n = x ** 2            by factor_eq_cofactor, 0 < x
      Thus x ** 2 = n = m ** 2   by EXP_2
       ==> x = m                 by EXP_EXP_INJECTIVE

   Only-if part: SING (fixes f s) ==> square n
      Note ?m. m IN fixes f s    by SING_DEF, IN_SING
        so 0 < m /\ m divides n  by divisors_element
       and n DIV m = m           by fixes_element
       ==> n = m ** 2            by factor_eq_cofactor, 0 < m
        or square n              by square_alt
*)
Theorem cofactor_divisors_fixes_sing:
  !n. 0 < n ==> (square n <=> SING (fixes ($DIV n) (divisors n)))
Proof
  rpt strip_tac >>
  qabbrev_tac `s = divisors n` >>
  qabbrev_tac `f = ($DIV n)` >>
  `!j. f j = n DIV j` by rw[Abbr`f`] >>
  rw[EQ_IMP_THM] >| [
    `?m. n = m * m` by rw[GSYM square_def] >>
    `m divides n` by metis_tac[divides_def] >>
    `0 < m` by metis_tac[ZERO_DIVIDES, NOT_ZERO] >>
    `f m = m` by metis_tac[factor_eq_cofactor, EXP_2] >>
    `m IN fixes f s` by rw[divisors_element_alt, fixes_element, Abbr`s`] >>
    simp[SING_DEF] >>
    qexists_tac `m` >>
    rw[EXTENSION, EQ_IMP_THM] >>
    qabbrev_tac `n = m * m` >>
    `x IN s /\ (f x = x)` by fs[fixes_element] >>
    `0 < x /\ x divides n /\ (n DIV x = x)` by fs[divisors_element, Abbr`s`] >>
    `m ** 2 = x ** 2` by metis_tac[factor_eq_cofactor, EXP_2] >>
    metis_tac[EXP_EXP_INJECTIVE, DECIDE``2 <> 0``],
    `?m. m IN fixes f s` by metis_tac[SING_DEF, IN_SING] >>
    `0 < m /\ m divides n /\ (n DIV m = m)` by fs[divisors_element, fixes_element, Abbr`s`] >>
    metis_tac[factor_eq_cofactor, square_alt]
  ]
QED

(* Idea: a number has an even number of divisors iff it is not a square. *)

(* Theorem: EVEN (CARD (divisors n)) <=> n = 0 \/ ~square n *)
(* Proof:
   Let s = divisors n,
       f = ($DIV n).
   Note FINITE s                       by divisors_finite
   If part: EVEN (CARD s) /\ square n ==> n = 0
      By contradiction, suppose n <> 0.
      Then 0 < n                       by arithmetic
       ==> SING (fixes f s)            by cofactor_divisors_fixes_sing, 0 < n
        so CARD (fixes f s) = 1        by SING_CARD_1
       But f involute s                by cofactor_involute_divisors
       ==> EVEN (CARD (fixes f s))     by involute_set_fixes_both_even, EVEN (CARD s)
      which is a contradiction         by ODD_1, ODD_EVEN

   Only-if part: n = 0 \/ ~square n ==> EVEN (CARD s)
      If n = 0, s = divisors 0 = {}    by divisors_0
         so CARD s = 0, and EVEN 0     by CARD_EMPTY, EVEN
      If ~square n,
         Note f involute s             by cofactor_involute_divisors

      Claim: fixes f s = {}
      Proof: By EXTENSION, this is to show:
                !x. x IN fixes f s ==> F.
             Note x IN s /\ f x = x    by fixes_element
               so 0 < x /\ x divides n by divisors_element
              and n DIV x = x          by notation
             Thus n <> 0               by ZERO_DIV, 0 < x
               so n = x ** 2           by factor_eq_cofactor
               or square n             by square_alt
             which contradicts ~square n.

      Thus CARD (fixes f s) = 0        by CARD_EMPTY
       and EVEN 0                      by EVEN
        so EVEN (CARD s)               by involute_set_fixes_both_even
*)
Theorem divisors_card_even:
  !n. EVEN (CARD (divisors n)) <=> n = 0 \/ ~square n
Proof
  rpt strip_tac >>
  qabbrev_tac `s = divisors n` >>
  qabbrev_tac `f = ($DIV n)` >>
  `FINITE s` by rw[divisors_finite, Abbr`s`] >>
  rw[EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `0 < n` by decide_tac >>
    `SING (fixes f s)` by fs[cofactor_divisors_fixes_sing, Abbr`f`, Abbr`s`] >>
    `CARD (fixes f s) = 1` by rw[SING_CARD_1] >>
    `f involute s` by metis_tac[cofactor_involute_divisors] >>
    `~EVEN 1` by rw[ODD_1, ODD_EVEN] >>
    metis_tac[involute_set_fixes_both_even],
    fs[divisors_0, Abbr`s`],
    `f involute s` by fs[cofactor_involute_divisors, Abbr`f`, Abbr`s`] >>
    `fixes f s = {}` by
  (rw[fixes_def, EXTENSION] >>
    spose_not_then strip_assume_tac >>
    `0 < x /\ x divides n /\ (n DIV x = x)` by fs[divisors_element, Abbr`s`] >>
    `n <> 0` by metis_tac[ZERO_DIV, NOT_ZERO] >>
    metis_tac[factor_eq_cofactor, square_alt]) >>
    `CARD (fixes f s) = 0` by fs[] >>
    metis_tac[involute_set_fixes_both_even, EVEN]
  ]
QED

(* Idea: a number has an odd number of divisors iff it is a square. *)

(* Theorem: ODD (CARD (divisors n)) <=> 0 < n /\ square n *)
(* Proof:
       ODD (CARD (divisors n))
   <=> ~EVEN (CARD (divisors n))   by ODD_EVEN
   <=> ~(n = 0 \/ ~square n)       by divisors_card_even
   <=> n <> 0 /\ square n          by logic
   <=> 0 < n /\ square n           by NOT_ZERO
*)
Theorem divisors_card_odd:
  !n. ODD (CARD (divisors n)) <=> 0 < n /\ square n
Proof
  simp[divisors_card_even, ODD_EVEN] >>
  metis_tac[NOT_ZERO]
QED

(* Very good! *)

(* Theorem: 0 < n ==> (EVEN (CARD (divisors n)) <=> ~square n) *)
(* Proof: by divisors_card_even. *)
Theorem divisors_card_even_alt:
  !n. 0 < n ==> (EVEN (CARD (divisors n)) <=> ~square n)
Proof
  metis_tac[divisors_card_even, NOT_ZERO]
QED

(* Theorem: 0 < n ==> (ODD (CARD (divisors n)) <=> square n) *)
(* Proof: by divisors_card_odd. *)
Theorem divisors_card_odd_alt:
  !n. 0 < n ==> (ODD (CARD (divisors n)) <=> square n)
Proof
  simp[divisors_card_odd]
QED

(* ------------------------------------------------------------------------- *)
(* Inverse action of a group                                                 *)
(* ------------------------------------------------------------------------- *)

(* Idea: inverse is an involution for a group. *)

(* Theorem: Group g ==> |/ involute G *)
(* Proof: by group_inv_element, group_inv_inv. *)
Theorem group_inv_involute:
  !(g:'a group). Group g ==> |/ involute G
Proof
  simp[group_inv_element, group_inv_inv]
QED

(* Note: |/ is not an action, as it takes only one argument. *)

(* Idea: group operation is a group action. *)

(* Theorem: Group g ==> (g act G) g.op *)
(* Proof: by action_def,
> action_def |> ISPEC ``(g:'a group).op`` |> ISPEC ``g:'a group`` |> ISPEC ``(g:'a group).carrier``;
val it = |- (g act G) $* <=>
!x. x IN G ==>
    (!a. a IN G ==> a * x IN G) /\     by group_op_element
     #e * x = x /\                     by group_lid
    !a b. a IN G /\ b IN G ==>
          a * (b * x) = a * b * x      by group_assoc
    : thm
*)
Theorem group_op_action:
  !(g:'a group). Group g ==> (g act G) g.op
Proof
  rw[action_def, group_assoc]
QED

(* ------------------------------------------------------------------------- *)
(* Square free part of a number.                                             *)
(* ------------------------------------------------------------------------- *)

(* Define the square divisors of a number. *)
Definition square_divisors_def[nocompute]:
   square_divisors n = {x | x IN divisors n /\ square x}
End
(* use [nocompute] as this is not effective *)

(* Theorem: x IN square_divisors n <=> x IN divisors n /\ square x *)
(* Proof: by square_divisors_def. *)
Theorem square_divisors_element:
  !n x. x IN square_divisors n <=> x IN divisors n /\ square x
Proof
  simp[square_divisors_def]
QED

(* Theorem: square_divisors n SUBSET divisors n *)
(* Proof: by square_divisors_def, SUBSET_DEF. *)
Theorem square_divisors_subset:
  !n. square_divisors n SUBSET divisors n
Proof
  simp[square_divisors_def, SUBSET_DEF]
QED

(* Theorem: FINITE (square_divisors n) *)
(* Proof:
   Note (square_divisors n) SUBSET (divisors n)    by square_divisors_subset
    and FINITE (divisors n)                        by divisors_finite
     so FINITE (square_divisors n)                 by SUBSET_FINITE
*)
Theorem square_divisors_finite:
  !n. FINITE (square_divisors n)
Proof
  metis_tac[square_divisors_subset, divisors_finite, SUBSET_FINITE]
QED

(* Theorem: square_divisors n = IMAGE (\j. if j * j divides n then j * j else 1) (natural (SQRT n)) *)
(* Proof:
   By square_divisors_def, EXTENSION, this is to show:
   (1) x IN divisors n /\ square x ==> ?j. x = (if j ** 2 divides n then j ** 2 else 1) /\ ?z. j = SUC z /\ z < SQRT n
       Note x IN divisors n ==> n <> 0         by divisors_eq_empty, MEMBER_NOT_EMPTY
        Let j = SQRT x, then x = j ** 2        by square_eqn
       Note 0 < x /\ x <= n /\ x divides n     by divisors_element
       thus 0 < j                              by SQRT_EQ_0, 0 < x
         so ?z. j = SUC z                      by SUC_EXISTS
        Now x <= n ==> j <= SQRT n             by SQRT_LE
         so  z < j ==> z < SQRT n              by inequality
   (2) j < SQRT n /\ x = if SUC j ** 2 divides n then SUC j ** 2 else 1 ==> x IN divisors n /\ square x
       Note 0 < SQRT n, so 0 < n               by SQRT_EQ_0
       Let d = SUC j ** 2.
       If d divides n, then x = d
          and x divides n ==> x IN divisors n  by divisors_element_alt, 0 < n
          and x = SUC j ** 2 ==> square n      by square_alt
       If ~d divides n, then x = 1
          and 1 divides n                      by ONE_DIVIDES_ALL
          ==> 1 IN divisors n                  by divisors_element_alt, 0 < n
          and square 1                         by square_1
*)
Theorem square_divisors_eqn[compute]:
  !n. square_divisors n = IMAGE (\j. if j * j divides n then j * j else 1) (natural (SQRT n))
Proof
  rw[square_divisors_def, EXTENSION, EQ_IMP_THM] >| [
    `n <> 0` by metis_tac[divisors_eq_empty, MEMBER_NOT_EMPTY] >>
    qexists_tac `SQRT x` >>
    `(SQRT x) ** 2 = x` by fs[square_eqn] >>
    fs[divisors_element] >>
    `SQRT x <= SQRT n` by rw[SQRT_LE] >>
    `0 < SQRT x` by metis_tac[SQRT_EQ_0, NOT_ZERO] >>
    metis_tac[SUC_EXISTS, LESS_SUC, LESS_LESS_EQ_TRANS],
    `0 < SQRT n` by decide_tac >>
    `0 < n` by metis_tac[SQRT_EQ_0, NOT_ZERO] >>
    qabbrev_tac `d = SUC x' ** 2` >>
    (Cases_on `d divides n` >> simp[divisors_element_alt]),
    simp[divisors_element_alt]
  ] >>
  `0 < SQRT n` by decide_tac >>
  `0 < n` by metis_tac[SQRT_EQ_0, NOT_ZERO] >>
  qabbrev_tac `d = SUC x' ** 2` >>
  Cases_on `d divides n` >> simp[square_alt, Abbr`d`, square_1]
QED

(*
> EVAL ``square_divisors 10``; {1}
> EVAL ``square_divisors 25``; {25; 1}
> EVAL ``square_divisors 45``; {9; 1}
> EVAL ``square_divisors 100``; {100; 25; 4; 1}
*)

(* Note: the square-free part of 25 is 1, but 25 = 3(3) + 4(4), so cannot just look at square-free part! *)

(* Define the max square divisor of a number. *)
Definition max_square_divisor_def[nocompute]:
   max_square_divisor n = MAX_SET {x | x IN divisors n /\ square x}
End

(* Theorem: max_square_divisor 0 = 0 *)
(* Proof:
     max_square_divisor 0
   = MAX_SET {x | x IN divisors 0 /\ square x}     by max_square_divisor_def
   = MAX_SET {}                                    by divisors_0
   = 0                                             by MAX_SET_EMPTY
*)
Theorem max_square_divisor_0:
  max_square_divisor 0 = 0
Proof
  simp[max_square_divisor_def, divisors_0]
QED

(* Theorem: max_square_divisor 1 = 1 *)
(* Proof:
     max_square_divisor 1
   = MAX_SET {x | x IN divisors 1 /\ square x}     by max_square_divisor_def
   = MAX_SET {1}                                   by divisors_1, square_1
   = 1                                             by MAX_SET_SING
*)
Theorem max_square_divisor_1:
  max_square_divisor 1 = 1
Proof
  rw[max_square_divisor_def, divisors_1] >>
  (`{x | x = 1 /\ square x} = {1}` by (rw[EXTENSION] >> metis_tac[square_1])) >>
  simp[]
QED

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
