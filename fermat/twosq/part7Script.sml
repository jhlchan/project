(* ------------------------------------------------------------------------- *)
(* Node Hopping Algorithm (old version).                                     *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "part7";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)

(* val _ = load "quarityTheory"; *)
open helperTwosqTheory;
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory; (* for divides_def, prime_def *)
open logPowerTheory; (* for square_alt *)

open listTheory rich_listTheory;
open helperListTheory;
open listRangeTheory; (* for listRangeLHI_ALL_DISTINCT *)
open indexedListsTheory; (* for findi_EL and EL_findi *)

load "sublistTheory";
open sublistTheory;

open quarityTheory;
open pairTheory;

(* val _ = load "twoSquaresTheory"; *)
open windmillTheory;
open involuteTheory;
open involuteFixTheory;
open iterateComposeTheory;
open iterateComputeTheory; (* for iterate_while_thm *)
open twoSquaresTheory; (* for loop test found *)


(* ------------------------------------------------------------------------- *)
(* Node Hopping Algorithm Documentation (old version)                        *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   The zagier-flip permutation:
   ting_def            |- !x y z. ting (x,y,z) = (x + 2 * y,y,z - y - x)
   tong_def            |- !x y z. tong (x,y,z) = (2 * z - x,z,x + y - z)
   tung_def            |- !x y z. tung (x,y,z) = (x - 2 * z,x + y - z,z)
   zagier_flip_thm     |- !x y z. (zagier o flip) (x,y,z) =
                                  if x < z - y then ting (x,y,z)
                                  else if x < 2 * z then tong (x,y,z)
                                  else tung (x,y,z)

   The flip-zagier permutation:
   ping_def            |- !x y z. ping (x,y,z) = (x + 2 * z,y - z - x,z
   pong_def            |- !x y z. pong (x,y,z) = (2 * y - x,x + z - y,y)
   pung_def            |- !x y z. pung (x,y,z) = (x - 2 * y,y,x + z - y)
   flip_zagier_thm     |- !x y z. (flip o zagier) (x,y,z) =
                                  if x < y - z then ping (x,y,z)
                                  else if x < 2 * y then pong (x,y,z)
                                  else pung (x,y,z)

   Mind Fixed Points:
   windmill_condition_1    |- !x y z. x < y - z ==> x < y
   windmill_condition_2    |- !x y z. y < x + z /\ 0 < x ==> y - z < x
   windmill_condition_3    |- !x y z. x = y /\ 0 < y /\ 0 < z ==> y - z < x /\ x < 2 * y
   windmill_condition_4    |- !x y z. y < x ==> y - z < x
   windmill_condition_5    |- !x y z. ODD x /\ ~(x < 2 * y) ==> 2 * y < x

   Flip and Zagier composition:
   is_ping_def             |- !x y z. is_ping (x,y,z) <=> x < y - z
   is_pong_def             |- !x y z. is_pong (x,y,z) <=> ~(x < y - z) /\ x < 2 * y
   is_pung_def             |- !x y z. is_pung (x,y,z) <=> ~(x < y - z) /\ ~(x < 2 * y)
   triple_cases            |- !x y z. is_ping (x,y,z) \/ is_pong (x,y,z) \/ is_pung (x,y,z)
   triple_cases_alt        |- !t. is_ping t \/ is_pong t \/ is_pung t
   is_pong_x_x_z           |- !x z. 0 < x ==> is_pong (x,x,z)
   not_ping_x_y_y          |- !x y. ~is_ping (x,y,y)
   ping_not_pong           |- !t. is_ping t ==> ~is_pong t
   ping_not_pung           |- !t. is_ping t ==> ~is_pung t
   pong_not_ping           |- !t. is_pong t ==> ~is_ping t
   pong_not_pung           |- !t. is_pong t ==> ~is_pung t
   pung_not_ping           |- !t. is_pung t ==> ~is_ping t
   pung_not_pong           |- !t. is_pung t ==> ~is_pong t

   flip_zagier_rule        |- !x y z. (flip o zagier) (x,y,z) =
                                      if is_ping (x,y,z) then ping (x,y,z)
                                      else if is_pong (x,y,z) then pong (x,y,z)
                                      else pung (x,y,z)
   flip_zagier_alt         |- !t. (flip o zagier) t =
                                  if is_ping t then ping t else if is_pong t then pong t else pung t
   flip_zagier_ping        |- !t. is_ping t ==> (flip o zagier) t = ping t
   flip_zagier_pong        |- !t. is_pong t ==> (flip o zagier) t = pong t
   flip_zagier_pung        |- !t. is_pung t ==> (flip o zagier) t = pung t
   pung_next_not_ping      |- !x y z. 2 * y <= x ==> ~is_ping (pung (x,y,z))
   pung_next_not_ping_alt  |- !t. is_pung t ==> ~is_ping (pung t)
   ping_before_not_pung_alt|- !t. is_ping ((flip o zagier) t) ==> ~is_pung t
   flip_zagier_ping_funpow |- !m x y z. (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
                                        FUNPOW (flip o zagier) m (x,y,z) = FUNPOW ping m (x,y,z)
   flip_zagier_ping_funpow_alt
                           |- !m t. (!j. j < m ==> is_ping (FUNPOW ping j t)) ==>
                                    FUNPOW (flip o zagier) m t = FUNPOW ping m t
   flip_zagier_pung_funpow |- !m x y z. (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                        FUNPOW (flip o zagier) m (x,y,z) = FUNPOW pung m (x,y,z)
   flip_zagier_pung_funpow_alt
                           |- !m t. (!j. j < m ==> is_pung (FUNPOW pung j t)) ==>
                                    FUNPOW (flip o zagier) m t = FUNPOW pung m t

   mind_rise_by_ping       |- !x y z. mind (x,y,z) <= mind (ping (x,y,z))
   mind_rise_by_ping_alt   |- !t. mind t <= mind (ping t)
   mind_inc_by_ping        |- !x y z. is_ping (ping (x,y,z)) /\ 0 < z ==>
                                      mind (x,y,z) < mind (ping (x,y,z))
   mind_fall_by_pung       |- !x y z. 2 * y <= x ==> mind (pung (x,y,z)) <= mind (x,y,z)
   mind_fall_by_pung_alt   |- !t. is_pung t ==> mind (pung t) <= mind t
   mind_of_ping            |- !x y z. is_ping (x,y,z) ==> mind (x,y,z) = x + 2 * z
   mind_of_pung            |- !x y z. is_pung (x,y,z) ==> mind (x,y,z) = x

   Path on principal orbit of (flip o zagier) iteration:
   path_def                |- (!n. path n 0 = [(1,1,n DIV 4)]) /\
                               !n k. path n (SUC k) = SNOC ((flip o zagier) (LAST (path n k))) (path n k)
   path_0                  |- !p. path n 0 = [(1,1,n DIV 4)]
   path_suc                |- !n k. path n (SUC k) = SNOC ((flip o zagier) (LAST (path n k))) (path n k)
   path_1                  |- !n. path n 1 = [(1,1,n DIV 4); (1,n DIV 4,1)]
   path_not_nil            |- !n k. path n k <> []
   path_length             |- !n k. LENGTH (path n k) = k + 1
   path_head               |- !n k. HD (path n k) = (1,1,n DIV 4)
   path_last               |- !n k. LAST (path n k) = FUNPOW (flip o zagier) k (1,1,n DIV 4)
   path_head_alt           |- !n k. HD (path n k) = EL 0 (path n k)
   path_last_alt           |- !n k. LAST (path n k) = EL k (path n k)
   path_eq_sing            |- !n k. path n k = [(1,1,n DIV 4)] <=> k = 0
   path_front_length       |- !n k. LENGTH (FRONT (path n k)) = k
   path_front_head         |- !n k. 0 < k ==> HD (FRONT (path n k)) = (1,1,n DIV 4)
   path_element_eqn        |- !n k j. j <= k ==> EL j (path n k) = FUNPOW (flip o zagier) j (1,1,n DIV 4)
   path_element_suc        |- !n k j. j < k ==> EL (SUC j) (path n k) = (flip o zagier) (EL j (path n k))
   path_element_thm        |- !n k j h. j + h <= k ==> EL (j + h) (path n k) = FUNPOW (flip o zagier) h (EL j (path n k))
   path_element_in_mills   |- !n k j. tik n /\ j <= k ==> EL j (path n k) IN mills n
   path_element_windmill   |- !n k j x y z. tik n /\ j <= k /\ EL j (path n k) = (x,y,z) ==> n = windmill (x,y,z)
   path_element_windmill_alt
                           |- !n k j. tik n /\ j <= k ==> n = windmill (EL j (path n k))
   path_head_is_pong       |- !n k. is_pong (EL 0 (path n k))
   path_element_0          |- !n k. EL 0 (path n k) = (1,1,n DIV 4)
   path_element_1          |- !n k. 0 < k ==> EL 1 (path n k) = (1,n DIV 4,1)
   path_head_next_property |- !n k. 0 < k ==> (let t = EL 1 (path n k)
                                                in if n < 4 then is_pung t
                                                   else if n < 12 then is_pong t else is_ping t)
   path_last_not_ping      |- !n k. (let ls = path n k
                                      in flip (LAST ls) = LAST ls ==> ~is_ping (LAST ls))
   path_last_not_ping_thm  |- !n p. prime n /\ tik n /\ p = iterate_period (flip o zagier) (1,1,n DIV 4) ==>
                                    ~is_ping (LAST (path n (1 + HALF p)))
   path_element_ping_funpow|- !n k u m. (let ls = path n k; (x,y,z) = EL u ls
                                          in m + u <= k /\ (!j. j < m ==> is_ping (EL (u + j) ls)) ==>
                                             !j. j < m ==> EL (u + j) ls = FUNPOW ping j (x,y,z))
   path_element_pung_funpow|- !n k u m. (let ls = path n k; (x,y,z) = EL u ls
                                          in m + u <= k /\ (!j. j < m ==> is_pung (EL (u + j) ls)) ==>
                                             !j. j < m ==> EL (u + j) ls = FUNPOW pung j (x,y,z))

   Sections and blocks in a path:
   skip_ping_def       |- !ls k. skip_ping ls k = WHILE (\j. is_ping (EL j ls)) SUC k
   skip_pung_def       |- !ls k. skip_pung ls k = WHILE (\j. is_pung (EL j ls)) SUC k
   skip_ping_thm       |- !ls k h. k <= h /\ (!j. k <= j /\ j < h ==> is_ping (EL j ls)) /\ ~is_ping (EL h ls) ==>
                                   skip_ping ls k = h
   skip_ping_none      |- !ls k. ~is_ping (EL k ls) ==> skip_ping ls k = k
   skip_pung_thm       |- !ls k h. k <= h /\ (!j. k <= j /\ j < h ==> is_pung (EL j ls)) /\ ~is_pung (EL h ls) ==>
                                   skip_pung ls k =
   skip_pung_none      |- !ls k. ~is_pung (EL k ls) ==> skip_pung ls k = k

   pung_to_ping_has_pong   |- !n k j h. (let ls = path n k
                                          in j < h /\ h <= k /\ is_pung (EL j ls) /\ is_ping (EL h ls) ==>
                                             ?p. j < p /\ p < h /\ is_pong (EL p ls))
   pong_interval_ping_start        |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
                                                     !e. j < e /\ e < h /\ is_ping (EL e ls) ==>
                                                     !p. j < p /\ p <= e ==> is_ping (EL p ls))
   pong_interval_ping_start_alt    |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     EVERY (\p. ~is_pong (EL p ls)) [j + 1 ..< h] ==>
                                                     !e. j < e /\ e < h /\ is_ping (EL e ls) ==>
                                                     EVERY (\p. is_ping (EL p ls)) [j + 1 .. e])
   pong_interval_pung_stop         |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
                                                     !e. j < e /\ e < h /\ is_pung (EL e ls) ==>
                                                     !p. e <= p /\ p < h ==> is_pung (EL p ls))
   pong_interval_pung_stop_alt     |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     EVERY (\p. ~is_pong (EL p ls)) [j + 1 ..< h] ==>
                                                     !e. j < e /\ e < h /\ is_pung (EL e ls) ==>
                                                     EVERY (\p. is_pung (EL p ls)) [e ..< h])
   pong_interval_cut_exists        |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
                                                     ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
                                                         (!p. j < p /\ p < c ==> is_ping (EL p ls)) /\
                                                          !p. c <= p /\ p < h ==> is_pung (EL p ls))
   pong_interval_cut_exists_alt    |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     EVERY (\p. ~is_pong (EL p ls)) [j + 1 ..< h] ==>
                                                     ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
                                                     EVERY (\p. is_ping (EL p ls)) [j + 1 ..< c] /\
                                                     EVERY (\p. is_pung (EL p ls)) [c ..< h])

   pong_seed_pung_before       |- !n k e. (let ls = path n k
                                            in ~is_pung (HD ls) /\ e < k /\ is_pong (EL e ls) ==>
                                               ?j. j <= e /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\
                                                   ~is_pung (EL (PRE j) ls))
   pong_seed_ping_after        |- !n k e. (let ls = path n k
                                            in ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
                                               ?h. e <= h /\ h < k /\ (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\
                                                   ~is_ping (EL (SUC h) ls))
   pong_seed_pung_ping         |- !n k e. (let ls = path n k
                                            in ~is_pung (HD ls) /\ ~is_ping (LAST ls) /\
                                               e < k /\ is_pong (EL e ls) ==>
                                               ?j h. j <= e /\ e <= h /\ h < k /\
                                                     (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls) /\
                                                     (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls))
   pung_next_not_flip_fix      |- !x y z. 0 < z /\ is_pung (x,y,z) ==> (let t = pung (x,y,z) in flip t <> t)
   path_last_flip_fix_not_by_pung
                               |- !n k e. (let ls = path n k
                                            in tik n /\ ~square n /\ flip (LAST ls) = LAST ls ==> ~is_pung (EL (k - 1) ls))
   path_skip_pung_to_pong      |- !n k j. (let ls = path n k
                                            in tik n /\ ~square n /\ j < k /\ ~is_ping (EL j ls) /\
                                               flip (LAST ls) = LAST ls ==>
                                               (let e = skip_pung ls j
                                                 in j <= e /\ e < k /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ is_pong (EL e ls)))
   path_skip_ping_after_pong   |- !n k j. (let ls = path n k
                                            in j < k /\ is_pong (EL j ls) /\ flip (LAST ls) = LAST ls ==>
                                               (let e = skip_ping ls (j + 1)
                                                 in j < e /\ e <= k /\ (!p. j < p /\ p < e ==> is_ping (EL p ls)) /\ ~is_ping (EL e ls)))
   path_skip_from_pung_to_ping |- !n k u v w. (let ls = path n k
                                                in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                                   u < k /\ ~is_ping (EL u ls) /\
                                                   v = skip_pung ls u /\ w = skip_ping ls (v + 1) ==>
                                                   u <= v /\ v < k /\ is_pong (EL v ls) /\
                                                   (!p. u <= p /\ p < v ==> is_pung (EL p ls)) /\
                                                   v < w /\ w <= k /\  ~is_ping (EL w ls) /\
                                                   !p. v < p /\ p < w ==> is_ping (EL p ls))
   path_element_skip_ping      |- !n k j h. (let ls = path n k
                                              in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_ping (EL p ls)) ==>
                                                 EL (j + h) ls = FUNPOW ping h (EL j ls))
   path_element_skip_pung      |- !n k j h. (let ls = path n k
                                              in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_pung (EL p ls)) ==>
                                                 EL (j + h) ls = FUNPOW pung h (EL j ls))

   Hopping:
   hop_def             |- !m x y z. hop m (x,y,z) = (2 * m * y - x,z + m * x - m * m * y,y)
   hip_def             |- !m x y z. hip m (x,y,z) = (2 * m * y + x,z - m * x - m * m * y,y)
   hop_0               |- !x y z. hop 0 (x,y,z) = (0,z,y)
   hop_1               |- !x y z. hop 1 (x,y,z) = (2 * y - x,x + z - y,y)
   hop_1_eqn           |- hop 1 = pong
   hop_mind            |- !m x y. 0 < y /\ x DIV (2 * y) < m ==> 0 < 2 * m * y - x
   hop_arm             |- !m n x y z. ~square n /\ n = windmill (x,y,z) /\ 0 < y /\
                                      x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y) ==>
                                      0 < z + m * x - m * m * y
   hop_windmill        |- !m n x y z. n = windmill (x,y,z) /\
                                      0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y ==>
                                      n = windmill (hop m (x,y,z))
   hop_range           |- !m n x y z. n = windmill (x,y,z) /\
                                      0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y ==>
                                      x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y)
   hop_range_iff       |- !m n x y z. ~square n /\ n = windmill (x,y,z) ==>
                                     (0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y <=>
                                      0 < y /\ x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y))

   Matrices of ping, pong, pung and hop:
   ping_funpow         |- !m x y z. FUNPOW ping m (x,y,z) = (x + 2 * m * z,y - m * x - m * m * z,z)
   ping_windmill       |- !x y z. is_ping (x,y,z) ==> windmill (x,y,z) = windmill (ping (x,y,z))
   ping_windmill_alt   |- !t. is_ping t ==> windmill t = windmill (ping t)
   pong_windmill       |- !x y z. is_pong (x,y,z) ==> windmill (x,y,z) = windmill (pong (x,y,z))
   pong_windmill_alt   |- !t. is_pong t ==> windmill t = windmill (pong t)
   pung_windmill       |- !x y z. is_pung (x,y,z) ==> windmill (x,y,z) = windmill (pung (x,y,z))
   pung_windmill_alt   |- !t. is_pung t ==> windmill t = windmill (pung t)
   ping_funpow_windmill|- !m x y z. (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
                                    windmill (x,y,z) = windmill (FUNPOW ping m (x,y,z))
   ping_funpow_windmill_alt
                       |- !m t. (!j. j < m ==> is_ping (FUNPOW ping j t)) ==>
                                windmill t = windmill (FUNPOW ping m t)
   pung_funpow_windmill|- !m x y z. (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                    windmill (x,y,z) = windmill (FUNPOW pung m (x,y,z))
   pung_funpow_windmill_alt
                       |- !m t. (!j. j < m ==> is_pung (FUNPOW pung j t)) ==>
                                windmill t = windmill (FUNPOW pung m t)
   pung_funpow         |- !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\
                                      (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                      FUNPOW pung m (x,y,z) = (x - 2 * m * y,y,z + m * x - m * m * y)

   pong_by_hop         |- !x y z. pong (x,y,z) = hop 1 (x,y,z)
   pong_by_hop_alt     |- !t. pong t = hop 1 t
   pung_to_ping_by_hop |- !n x y z p q. tik n /\ ~square n /\ n = windmill (x,y,z) /\
                                        is_pong (FUNPOW pung q (x,y,z)) /\
                                        (!j. j < q ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                        (FUNPOW ping p o pong o FUNPOW pung q) (x,y,z) = hop (p + q + 1) (x,y,z)
   pung_to_ping_by_hop_alt
                       |- !n t p q. tik n /\ ~square n /\ n = windmill t /\ is_pong (FUNPOW pung q t) /\
                                    (!j. j < q ==> is_pung (FUNPOW pung j t)) ==>
                                    (FUNPOW ping p o pong o FUNPOW pung q) t = hop (p + q + 1) t

   Pong Indices along a Path:
   pong_indices_def    |- !ls. pong_indices ls = FILTER (\j. is_pong (EL j ls)) [0 ..< LENGTH ls]
   pong_indices_mem    |- !ls j. MEM j (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls)
   pong_indices_element|- !ls j. j IN set (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls)
   pong_indices_nil    |- pong_indices [] = []
   pong_indices_empty  |- set (pong_indices []) = {}:
   pong_indices_subset |- !ls. set (pong_indices ls) SUBSET count (LENGTH ls)
   pong_indices_finite |- !ls. FINITE (set (pong_indices ls))
   pong_indices_length |- !ls. LENGTH (pong_indices ls) <= LENGTH ls
   pong_indices_all_distinct
                       |- !ls. ALL_DISTINCT (pong_indices ls)
   pong_indices_path_cons      |- !n k. 0 < k ==> ?t. pong_indices (FRONT (path n k)) = 0::t
   pong_indices_path_not_nil   |- !n k. 0 < k ==> pong_indices (FRONT (path n k)) <> []
   pong_indices_path_head      |- !n k. 0 < k ==> HD (pong_indices (FRONT (path n k))) = 0
   pong_indices_path_eq_nil    |- !n k. pong_indices (FRONT (path n k)) = [] <=> k = 0
   pong_indices_path_element   |- !n k j. (let ls = path n k
                                            in MEM j (pong_indices (FRONT ls)) <=> j < k /\ is_pong (EL j ls))
   pong_indices_monotonic      |- !ls j. (let px = pong_indices ls
                                           in 0 < j /\ j < LENGTH px ==> EL (j - 1) px < EL j px)
   pong_indices_path_pong_gap  |- !n k j. (let ls = path n k; px = pong_indices (FRONT ls)
                                            in 0 < j /\ j < LENGTH px ==>
                                               !p. EL (j - 1) px < p /\ p < EL j px ==> ~is_pong (EL p ls))
   pong_indices_path_last      |- !n k. (let ls = path n k; px = pong_indices (FRONT ls); v = LAST px
                                          in 0 < k ==> v < k /\ is_pong (EL v ls) /\
                                             !p. v < p /\ p < k ==> ~is_pong (EL p ls))

   Define blocks based on pong indices:
   block_pairs_def     |- !ls. block_pairs ls =
                               MAP (\j. (j,skip_ping ls (j + 1))) (pong_indices (FRONT ls))
   blocks_def          |- !ls. blocks ls =
                                MAP (\j. (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls)) [0 ..< LENGTH ls]

   Block Pairs Theorems:
   block_pairs_nil             |- !x. block_pairs [x] = []
   block_pairs_length          |- !ls. LENGTH (block_pairs ls) = LENGTH (pong_indices (FRONT ls))
   block_pairs_mem             |- !ls v w. MEM (v,w) (block_pairs ls) <=>
                                           MEM v (pong_indices (FRONT ls)) /\ w = skip_ping ls (v + 1)
   block_pairs_element         |- !ls j v w. (let ps = block_pairs ls
                                               in j < LENGTH ps ==>
                                                  ((v,w) = EL j ps <=>
                                                  v = EL j (pong_indices (FRONT ls)) /\ w = skip_ping ls (v + 1)))
   block_pairs_path_0          |- !n. block_pairs (path n 0) = []
   block_pairs_path_cons       |- !n k. 0 < k ==> ?t. block_pairs (path n k) = (0,skip_ping (path n k) 1)::t
   block_pairs_path_not_nil    |- !n k. 0 < k ==> block_pairs (path n k) <> []
   block_pairs_path_head       |- !n k. 0 < k ==> HD (block_pairs (path n k)) = (0,skip_ping (path n k) 1)
   block_pairs_path_eq_nil     |- !n k. block_pairs (path n k) = [] <=> k = 0
   block_pairs_path_mem        |- !n k v w. (let ls = path n k
                                              in ~is_ping (LAST ls) /\ MEM (v,w) (block_pairs ls) ==>
                                                 v < w /\ w <= k /\ w = skip_ping ls (v + 1) /\
                                                 is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
                                                 !j. v < j /\ j < w ==> is_ping (EL j ls))
   block_pairs_path_pong       |- !n k v. (let ls = path n k
                                            in v < k /\ is_pong (EL v ls) <=>
                                               MEM (v,skip_ping ls (v + 1)) (block_pairs ls))
   block_pairs_path_next       |- !n k j a b c d. (let ls = path n k; ps = block_pairs ls
                                                    in ~is_ping (LAST ls) /\ 0 < j /\ j < LENGTH ps /\
                                                       (a,b) = EL (j - 1) ps /\ (c,d) = EL j ps ==>
                                                       b <= c /\ c = skip_pung ls b /\
                                                       !p. b <= p /\ p < c ==> is_pung (EL p ls))
   block_pairs_path_next_pong  |- !n k j a b c d. (let ls = path n k; ps = block_pairs ls
                                                    in ~is_ping (LAST ls) /\ 0 < j /\ j < LENGTH ps /\
                                                       (a,b) = EL (j - 1) ps /\ (c,d) = EL j ps /\
                                                       is_pong (EL b ls) ==> c = b)
   block_pairs_path_last       |- !n k. (let ls = path n k
                                          in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
                                             LAST (block_pairs ls) = (LAST (pong_indices (FRONT ls)),k))

   Blocks Theorems:
   blocks_nil          |- blocks [] = []
   blocks_length       |- !ls. LENGTH (blocks ls) = LENGTH ls
   blocks_path_0       |- !n. blocks (block_pairs (path n 0)) = []
   blocks_path_cons    |- !n k. 0 < k ==> ?t. blocks (block_pairs (path n k)) = (0,0,skip_ping (path n k) 1)::t
   blocks_path_not_nil |- !n k. 0 < k ==> blocks (block_pairs (path n k)) <> []
   blocks_path_head    |- !n k. 0 < k ==> HD (blocks (block_pairs (path n k))) = (0,0,skip_ping (path n k) 1)
   blocks_path_eq_nil  |- !n k. blocks (block_pairs (path n k)) = [] <=> k = 0
   blocks_mem          |- !ls u v w. MEM (u,v,w) (blocks ls) <=>
                                     ?j. j < LENGTH ls /\ (v,w) = EL j ls /\
                                         u = if j = 0 then 0 else SND (EL (j - 1) ls)
   blocks_path_mem     |- !n k u v w. (let ls = path n k
                                        in flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           u <= v /\ v < w /\ w <= k /\
                                           ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
                                           v = skip_pung ls u /\ w = skip_ping ls (v + 1) /\
                                           (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
                                            !j. v < j /\ j < w ==> is_ping (EL j ls))
   blocks_path_last    |- !n k. (let ls = path n k
                                  in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
                                     SND (LAST (blocks (block_pairs ls))) = (LAST (pong_indices (FRONT ls)),k))
   blocks_path_third_by_funpow
                       |- !n k u v w. (let ls = path n k
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                           MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           EL w ls = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls))
   blocks_path_third_by_hop
                       |- !n k u v w. (let ls = path n k
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                           MEM (u,v,w) (blocks (block_pairs ls)) ==> EL w ls = hop (w - u) (EL u ls))

   Hopping Algorithm:
   step_def            |- !k x y z. step k (x,y,z) = (x + k) DIV (2 * y)
   hopping_def         |- !k t. hopping k t = hop (step k t) t
   two_sq_hop_def      |- !n. two_sq_hop n = WHILE ($~ o found) (hopping (SQRT n)) (1,1,n DIV 4)

   blocks_triple_first_not_ping
                       |- !n k u v w. (let ls = path n k
                                        in flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           ~is_ping (EL u ls) /\ (let (x,y,z) = EL u ls in y <= x + z))
   step_0              |- !x y z. step 0 (x,y,z) = x DIV (2 * y)
   step_sqrt           |- !n x y z. step (SQRT n) (x,y,z) = (x + SQRT n) DIV (2 * y)


   node_def            |- (!n. node n 0 = (1,1,n DIV 4)) /\
                           !n j. node n (SUC j) = (let m = step (SQRT n) (node n j) in hop m (node n j))

*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* The zagier-flip permutation.                                              *)
(* ------------------------------------------------------------------------- *)

(* The three cases of Zagier map, or the Zagier-flip map.

zagier_def
|- !x y z. zagier (x,y,z) =
           if x < y - z then (x + 2 * z,z,y - z - x)
           else if x < 2 * y then (2 * y - x,y,x + z - y)
           else (x - 2 * y,x + z - y,y)
these cases are clean by checking (2 * y - x).
Note n = x² + 4yz, so z = (n - x²)/4y. Eliminating z:
           x < y - z
iff        x < y - (n - x²)/4y
iff      4xy < 4y² - n + x²
iff        n < 4y² - 4xy + x² = (2y - x)²
iff       √n < 2y - x

also       x < 2 * y
iff        0 < 2y - x

and the last case: 2y < x, or 2y - x < 0, or 2y - x = 0 in integer arithmetic.

zagier_flip_eqn
|- !x y z. (zagier o flip) (x,y,z) =
           if x < z - y then (x + 2 * y,y,z - y - x)
           else if x < 2 * z then (2 * z - x,z,x + y - z)
           else (x - 2 * z,x + y - z,z)
these cases are clean by checking (2 * z - x).
Note n = x² + 4yz, so y = (n - x²)/4z. Eliminating y:
             x < z - y
iff          x < z - (n - x²)/4z
iff        4xz < 4z² - n + x²
iff          n < 4z² - 4xz + x² = (2z - x)²
iff         √n < 2z - x

also         x < 2 * z
iff          0 < 2z - x

and the last case: 2z < x, or 2z - x < 0, or 2z - x = 0 in integer arithmetic.
*)

(* Define the map for each case of zagier_flip *)
Definition ting_def:
   ting (x,y,z) = (x + 2 * y, y, z - y - x)
End

Definition tong_def:
   tong (x,y,z) = (2 * z - x, z, x + y - z)
End

Definition tung_def:
   tung (x,y,z) = (x - 2 * z, x + y - z, z)
End

(* Matrices:
   ting   A = [[1,2,0],[0,1,0],[-1,-1,1]]  A^2 = [[1,4,0],[0,1,0],[-2,-4,1]]  same form A^m = [[1,2m,0],[0,1,0],[-m,-m*m,1]]
   tong   B = [[-1,0,2],[0,0,1],[1,1,-1]]  B^2 = [[3,2,-4],[1,1,-1],[-2,-1,4]]
   tung   C = [[1,0,-2],[1,1,-1],[0,0,1]]  C^2 = [[1,0,-4],[2,1,-4],[0,0,1]]  same form C^m = [[1,0,-2m],[m,1,-m*m],[0,0,1]]
*)

(* Theorem: (zagier o flip) (x, y, z) =
            if x < z - y then ting (x,y,z)
            else if x < 2 * z then tong (x,y,z)
            else tung (x,y,z) *)
(* Proof: by zagier_flip_eqn, ting_def, tong_def, tung_def. *)
Theorem zagier_flip_thm:
  !x y z. (zagier o flip) (x, y, z) =
           if x < z - y then ting (x,y,z)
           else if x < 2 * z then tong (x,y,z)
           else tung (x,y,z)
Proof
  simp[zagier_flip_eqn, ting_def, tong_def, tung_def]
QED

(* ------------------------------------------------------------------------- *)
(* The flip-zagier permutation.                                              *)
(* ------------------------------------------------------------------------- *)

(* Define the map for each case of flip_zagier *)
Definition ping_def:
   ping (x,y,z) = (x + 2 * z, y - z - x, z)
End

Definition pong_def:
   pong (x,y,z) = (2 * y - x, x + z - y, y)
End

Definition pung_def:
   pung (x,y,z) = (x - 2 * y, y, x + z - y)
End

(* Matrices:
   ping   D = [[1,0,2],[-1,1,-1],[0,0,1]]  D^2 = [[1,0,4],[-2,1,-4],[0,-0,1]]  same form D^m = [[1,0,2m],[-m,1,-m*m],[0,0,1]]
   pong   E = [[-1,2,0],[1,-1,1],[0,1,0]]  E^2 = [[3,-4,2],[-2,4,-1],[1,-1,1]]
   pung   F = [[1,-2,0],[0,1,0],[1,-1,1]]  F^2 = [[1,-4,0],[0,1,0],[2,-4,1]]  same form E^m = [[1,-2m,0],[0,1,0],[m,-m*m,1]]
*)

(* Theorem: (flip o zagier) (x, y, z) =
            if x < y - z then ping (x,y,z)
            else if x < 2 * y then pong (x,y,z)
            else pung (x,y,z) *)
(* Proof: by flip_zagier_eqn, ting_def, tong_def, tung_def. *)
Theorem flip_zagier_thm:
  !x y z. (flip o zagier) (x, y, z) =
           if x < y - z then ping (x,y,z)
           else if x < 2 * y then pong (x,y,z)
           else pung (x,y,z)
Proof
  simp[flip_zagier_eqn, ping_def, pong_def, pung_def]
QED

(* ------------------------------------------------------------------------- *)
(* Mind Fixed Points                                                         *)
(* ------------------------------------------------------------------------- *)

(*
> EVAL ``mind (x,y,z)``;
val it = |- mind (x,y,z) = if x < y - z then x + 2 * z else if x < y then 2 * y - x else x: thm
> EVAL ``mind (x,z,y)``;
val it = |- mind (x,z,y) = if x < z - y then x + 2 * y else if x < z then 2 * z - x else x: thm

What is the condition that:  mind (x,y,z) = mind (x,z,y)?


*)

(* Idea case 1: x < y and x + z < y *)

(* Theorem: x < y - z ==> x < y *)
(* Proof: by inequality. *)
Theorem windmill_condition_1:
  !x y z. x < y - z ==> x < y
Proof
  decide_tac
QED

(* Idea case 2: x < y and y < x + z *)

(* Theorem: y - z < x /\ 0 < x ==> x < y *)
(* Proof: by inequality. *)
Theorem windmill_condition_2:
  !x y z. y < x + z /\ 0 < x ==> y - z < x
Proof
  decide_tac
QED

(* Idea case 3: x = y *)

(* Theorem: x = y /\ 0 < y /\ 0 < z ==> y - z < x /\ x < 2 * y *)
(* Proof: by inequality. *)
Theorem windmill_condition_3:
  !x y z. x = y /\ 0 < y /\ 0 < z ==> y - z < x /\ x < 2 * y
Proof
  decide_tac
QED

(* Idea case 4: y < x and x < 2 * y *)

(* Theorem: y < x ==> y - z < x *)
(* Proof: by inequality. *)
Theorem windmill_condition_4:
  !x y z. y < x ==> y - z < x
Proof
  decide_tac
QED


(* Idea case 5: y < x and 2 * y < x *)

(* Theorem: ODD x /\ ~(x < 2 * y) ==> 2 * y < x *)
(* Proof: by inequality and x <> 2 * y. *)
Theorem windmill_condition_5:
  !x y z. ODD x /\ ~(x < 2 * y) ==> 2 * y < x
Proof
  rpt strip_tac >>
  `x <> 2 * y` by metis_tac[EVEN_EXISTS, ODD_EVEN] >>
  decide_tac
QED

(* ------------------------------------------------------------------------- *)
(* Flip and Zagier composition                                               *)
(* ------------------------------------------------------------------------- *)

(* Define the ping, pong, pung conditions *)
Definition is_ping_def:
   is_ping (x, y, z) <=> x < y - z
End
Definition is_pong_def:
   is_pong (x, y, z) <=> ~(x < y - z) /\ x < 2 * y
End
Definition is_pung_def:
   is_pung (x, y, z) <=> ~(x < y - z) /\ ~(x < 2 * y)
End

(* Theorem: is_ping (x,y,z) \/ is_pong (x,y,z) \/ is_pung (x,y,z) *)
(* Proof: by is_ping_def, is_pong_def, is_pung_def. *)
Theorem triple_cases:
  !x y z. is_ping (x,y,z) \/ is_pong (x,y,z) \/ is_pung (x,y,z)
Proof
  simp[is_ping_def, is_pong_def, is_pung_def]
QED

(* Theorem: is_ping t \/ is_pong t \/ is_pung t *)
(* Proof: by triple_cases, triple_parts. *)
Theorem triple_cases_alt:
  !t. is_ping t \/ is_pong t \/ is_pung t
Proof
  metis_tac[triple_cases, triple_parts]
QED

(* Theorem: 0 < x ==> is_pong (x,x,z) *)
(* Proof:
   Note x < x - z is false         by arithmetic, 0 < x
    but x < 2 * x is true          by arithmetic, 0 < x
     so is_pong (x,x,z)            by is_pong_def
*)
Theorem is_pong_x_x_z:
  !x z. 0 < x ==> is_pong (x,x,z)
Proof
  simp[is_pong_def]
QED

(* Theorem: ~is_ping (x,y,y) *)
(* Proof:
   Note x < y - y is false         by arithmetic
     so ~is_ping (x,y,y)           by is_ping_def
*)
Theorem not_ping_x_y_y:
  !x y. ~is_ping (x,y,y)
Proof
  simp[is_ping_def]
QED

(* Theorem: is_ping t ==> ~is_pong t *)
(* Proof: by is_ping_def, is_pong_def. *)
Theorem ping_not_pong:
  !t. is_ping t ==> ~is_pong t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_ping_def, is_pong_def]
QED

(* Theorem: is_ping t ==> ~is_pung t *)
(* Proof: by is_ping_def, is_pung_def. *)
Theorem ping_not_pung:
  !t. is_ping t ==> ~is_pung t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_ping_def, is_pung_def]
QED

(* Theorem: is_pong t ==> ~is_ping t *)
(* Proof: by is_pong_def, is_ping_def. *)
Theorem pong_not_ping:
  !t. is_pong t ==> ~is_ping t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_pong_def, is_ping_def]
QED

(* Theorem: is_pong t ==> ~is_pung t *)
(* Proof: by is_pong_def, is_pung_def. *)
Theorem pong_not_pung:
  !t. is_pong t ==> ~is_pung t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_pong_def, is_pung_def]
QED

(* Theorem: is_pung t ==> ~is_ping t *)
(* Proof: by is_pung_def, is_ping_def. *)
Theorem pung_not_ping:
  !t. is_pung t ==> ~is_ping t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_pung_def, is_ping_def]
QED

(* Theorem: is_pung t ==> ~is_pong t *)
(* Proof: by is_pung_def, is_pong_def. *)
Theorem pung_not_pong:
  !t. is_pung t ==> ~is_pong t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_pung_def, is_pong_def]
QED

(* Theorem: (flip o zagier) (x,y,z) =
                  if is_ping (x,y,z) then ping (x,y,z)
                  else if is_pong (x,y,z) then pong (x,y,z)
                  else pung (x,y,z) *)
(* Proof: by flip_zagier_thm and definitions. *)
Theorem flip_zagier_rule:
  !x y z. (flip o zagier) (x,y,z) =
           if is_ping (x,y,z) then ping (x,y,z)
           else if is_pong (x,y,z) then pong (x,y,z)
           else pung (x,y,z)
Proof
  simp[is_ping_def, is_pong_def, flip_zagier_thm]
QED

(* Theorem: (flip o zagier) t =
            if is_ping t then ping t
            else if is_pong t then pong t
            else pung t *)
(* Proof: by triple_parts, flip_zagier_rule. *)
Theorem flip_zagier_alt:
  !t. (flip o zagier) t =
      if is_ping t then ping t
      else if is_pong t then pong t
      else pung t
Proof
  metis_tac[triple_parts, flip_zagier_rule]
QED

(* Theorem: is_ping t ==> (flip o zagier) t = ping t *)
(* Proof: by flip_zagier_alt. *)
Theorem flip_zagier_ping:
  !t. is_ping t ==> (flip o zagier) t = ping t
Proof
  simp[flip_zagier_alt]
QED

(* Theorem: is_pong t ==> (flip o zagier) t = pong t *)
(* Proof: by flip_zagier_alt, pong_not_ping. *)
Theorem flip_zagier_pong:
  !t. is_pong t ==> (flip o zagier) t = pong t
Proof
  simp[flip_zagier_alt, pong_not_ping]
QED

(* Theorem: is_pung t ==> (flip o zagier) t = pung t *)
(* Proof: by flip_zagier_alt, pung_not_ping, pung_not_pong. *)
Theorem flip_zagier_pung:
  !t. is_pung t ==> (flip o zagier) t = pung t
Proof
  simp[flip_zagier_alt, pung_not_ping, pung_not_pong]
QED

(* Idea: after a pung, there is no ping. *)

(* Theorem: 2 * y <= x ==> ~is_ping (pung (x,y,z)) *)
(* Proof:
   Note pung (x,y,z) = (x - 2 * y,y,x + z - y)   by pung_def
   If this is is_ping,
      x - 2 * y < y - (x + z - y)                by is_ping_def
                = (2 * y - x) - z                by arithmetic
                = 0                              by 2 * y <= x
   This is impossible, a contradiction.
*)
Theorem pung_next_not_ping:
  !x y z. 2 * y <= x ==> ~is_ping (pung (x,y,z))
Proof
  simp[pung_def, is_ping_def]
QED

(* Theorem: is_pung t ==> ~is_ping (pung t) *)
(* Proof: by pung_next_not_ping, is_pung_def gives ~(x < 2 * y) *)
Theorem pung_next_not_ping_alt:
  !t. is_pung t ==> ~is_ping (pung t)
Proof
  metis_tac[pung_next_not_ping, is_pung_def, triple_parts, NOT_LESS]
QED

(* Theorem: is_ping ((flip o zagier) t) ==> ~is_pung t *)
(* Proof:
   By contradiction, suppose is_pung t.
   Then (flip o zagier) t = pung t             by flip_zagier_pung
    but is_ping (pung t) = F                   by pung_next_not_ping_alt
*)
Theorem ping_before_not_pung_alt:
  !t. is_ping ((flip o zagier) t) ==> ~is_pung t
Proof
  spose_not_then strip_assume_tac >>
  fs[flip_zagier_pung, pung_next_not_ping_alt]
QED

(* This means: after a pung, either a pong or a pung, no ping.  *)
(* This means: before a ping, either a ping or a pong, no pung. *)

(* Theorem: (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
            FUNPOW (flip o zagier) m (x,y,z) = FUNPOW ping m (x,y,z) *)
(* Proof:
   Let t = (x,y,z).
   By induction on m.
   Base: (!j. j < 0 ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (flip o zagier) 0 t = FUNPOW ping 0 t
      FUNPOW (flip o zagier) 0 t
    = t                            by FUNPOW_0
    = FUNPOW ping 0 t              by FUNPOW_0

   Step: (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (flip o zagier) m t = FUNPOW ping m t ==>
         (!j. j < SUC m ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (flip o zagier) (SUC m) t = FUNPOW ping (SUC m) t
      Note !j. j < SUC m ==> is_ping (FUNPOW ping j t)
       <=> !j. (j < m \/ j = m) ==> is_ping (FUNPOW ping j t)       by arithmetic
       <=> is_ping (FUNPOW ping m t) /\
           !j. j < m ==> is_ping (FUNPOW ping j t)                  by DISJ_IMP_THM

           is_ping (FUNPOW ping (SUC m) t)
       <=> is_ping (ping (FUNPOW ping m t))                          by FUNPOW_SUC
       <=> is_ping ((flip o zagier) (FUNPOW ping m t))               by flip_zagier_ping, [1]

        FUNPOW (flip o zagier) (SUC m) t
      = (flip o zagier) (FUNPOW (flip o zagier) m t)                 by FUNPOW_SUC
      = (flip o zagier) (FUNPOW ping m t)                            by induction hypothesis
      = ping (FUNPOW ping m t)                                       by flip_zagier_ping, [1]
      = FUNPOW ping (SUC m) t                                        by FUNPOW_SUC
*)
Theorem flip_zagier_ping_funpow:
  !m x y z. (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
            FUNPOW (flip o zagier) m (x,y,z) = FUNPOW ping m (x,y,z)
Proof
  rpt strip_tac >>
  qabbrev_tac `t = (x,y,z)` >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_ping (FUNPOW ping m t) /\ !j. j < m ==> is_ping (FUNPOW ping j t)` by metis_tac[] >>
  fs[FUNPOW_SUC, flip_zagier_ping]
QED

(* Theorem: (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (flip o zagier) m t = FUNPOW ping m t *)
(* Proof: by flip_zagier_ping_funpow, triple_parts. *)
Theorem flip_zagier_ping_funpow_alt:
  !m t. (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (flip o zagier) m t = FUNPOW ping m t
Proof
  metis_tac[flip_zagier_ping_funpow, triple_parts]
QED

(* Theorem: (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            FUNPOW (flip o zagier) m (x,y,z) = FUNPOW pung m (x,y,z) *)
(* Proof:
   Let t = (x,y,z).
   By induction on m.
   Base: (!j. j < 0 ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (flip o zagier) 0 t = FUNPOW pung 0 t
      FUNPOW (flip o zagier) 0 t
    = t                            by FUNPOW_0
    = FUNPOW pung 0 t              by FUNPOW_0

   Step: (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (flip o zagier) m t = FUNPOW pung m t ==>
         (!j. j < SUC m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (flip o zagier) (SUC m) t = FUNPOW pung (SUC m) t
      Note !j. j < SUC m ==> is_pung (FUNPOW pung j t)
       <=> !j. (j < m \/ j = m) ==> is_pung (FUNPOW pung j t)        by arithmetic
       <=> is_pung (FUNPOW pung m t) /\
           !j. j < m ==> is_pung (FUNPOW pung j t)                   by DISJ_IMP_THM

           is_pung (FUNPOW pung (SUC m) t)
       <=> is_pung (pung (FUNPOW pung m t))                          by FUNPOW_SUC
       <=> is_pung ((flip o zagier) (FUNPOW pung m t))               by flip_zagier_pung, [1]

        FUNPOW (flip o zagier) (SUC m) t
      = (flip o zagier) (FUNPOW (flip o zagier) m t)                 by FUNPOW_SUC
      = (flip o zagier) (FUNPOW pung m t)                            by induction hypothesis
      = pung (FUNPOW pung m t)                                       by flip_zagier_pung, [1]
      = FUNPOW pung (SUC m) t                                        by FUNPOW_SUC
*)
Theorem flip_zagier_pung_funpow:
  !m x y z. (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            FUNPOW (flip o zagier) m (x,y,z) = FUNPOW pung m (x,y,z)
Proof
  rpt strip_tac >>
  qabbrev_tac `t = (x,y,z)` >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_pung (FUNPOW pung m t) /\ !j. j < m ==> is_pung (FUNPOW pung j t)` by metis_tac[] >>
  fs[FUNPOW_SUC, flip_zagier_pung]
QED

(* Theorem: (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (flip o zagier) m t = FUNPOW pung m t *)
(* Proof: by flip_zagier_pung_funpow, triple_parts. *)
Theorem flip_zagier_pung_funpow_alt:
  !m t. (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (flip o zagier) m t = FUNPOW pung m t
Proof
  metis_tac[flip_zagier_pung_funpow, triple_parts]
QED

(* Idea: ping (x,y,z) will generally increase the mind, by first of 5 cases. *)

(* Theorem: mind (x,y,z) <= mind (ping (x,y,z)) *)
(* Proof:
   Let t = (x,y,z).
   Then ping t = (x + 2 * z,y - z - x,z)       by ping_def
   Case 1: x < y - z
   Note mind t = x + 2 * z                     by mind_def
   If x + 2 * z < (y - z - x) - z
        mind (ping t)
      = (x + 2 * z) + 2 * z                    by mind_def
      >= x + 2 * z = mind t                    by inequality
   Otherwise, if x + 2 * z < y - z - x         [1]
        mind (ping t)
      = 2 * (y - z - x) - (x + 2 * z)          by mind_def
      > 2 * (x + 2 * z) - (x + 2 * z)          by [1]
      = x + 2 * z = mind t                     by arithmetic
   Otherwise,
      mind (ping t) = x + 2 * z = mind t       by mind_def

   Case 2: ~(x < y - z) /\ x < y
   That is, y - z <= x, x < y.
        ==> y <= x + z ==> 2 * y <= 2 * x + 2 * z
        ==> 2 * y - x <= 2 * z + x             by inequality [1]
   Note mind t = 2 * y - x                     by mind_def
   If x + 2 * z < (y - z - x) - z
        mind (ping t)
      = (x + 2 * z) + 2 * z                    by mind_def
      >= x + 2 * z                             by inequality
      >= 2 * y - x = mind t                    by [1]
   Otherwise, if x + 2 * z < y - z - x         [2]
        mind (ping t)
      = 2 * (y - z - x) - (x + 2 * z)          by mind_def
      > 2 * (x + 2 * z) - (x + 2 * z)          by [2]
      = x + 2 * z                              by arithmetic
      >= 2 * y - x = mind t                    by [1]
   Otherwise,
        mind (ping t)
      = x + 2 * z                              by mind_def
      >= 2 * y - x = mind t                    by [1]

   Case 3: otherwise
   Note mind t = x                             by mind_def
   If x + 2 * z < (y - z - x) - z
        mind (ping t)
      = (x + 2 * z) + 2 * z                    by mind_def
      >= x = mind t                            by inequality
   Otherwise, if x + 2 * z < y - z - x         [3]
        mind (ping t)
      = 2 * (y - z - x) - (x + 2 * z)          by mind_def
      > 2 * (x + 2 * z) - (x + 2 * z)          by [3]
      = x + 2 * z                              by arithmetic
      >= x = mind t                            by inequality
   Otherwise,
        mind (ping t)
      = x + 2 * z                              by mind_def
      >= x = mind t                            by inequality

   Overall, mind t <= mind (ping t).
*)
Theorem mind_rise_by_ping:
  !x y z. mind (x,y,z) <= mind (ping (x,y,z))
Proof
  simp[ping_def, mind_def]
QED

(* Theorem: mind t <= mind t *)
(* Proof: by mind_rise_by_ping, triple_parts. *)
Theorem mind_rise_by_ping_alt:
  !t. mind t <= mind (ping t)
Proof
  metis_tac[mind_rise_by_ping, triple_parts]
QED

(* Theorem: is_ping (ping (x,y,z)) /\ 0 < z ==> mind (x,y,z) < mind (ping (x,y,z)) *)
(* Proof:
   Let t = (x,y,z).
   Then ping t = (x + 2 * z,y - z - x,z)       by ping_def
    and is_ping (ping (x,y,z))
    ==> x + 2 * z < (y - z - x) - z            by is_ping_def
   Case 1: x < y - z
   Note mind t = x + 2 * z                     by mind_def
        mind (ping t)
      = (x + 2 * z) + 2 * z                    by mind_def
      > x + 2 * z = mind t                     by inequality, 0 < z
   Case 2: ~(x < y - z) /\ x < y
   That is, y - z <= x, x < y.
        ==> y <= x + z ==> 2 * y <= 2 * x + 2 * z
        ==> 2 * y - x <= 2 * z + x             by inequality [1]
   Note mind t = 2 * y - x                     by mind_def
        mind (ping t)
      = (x + 2 * z) + 2 * z                    by mind_def
      >  x + 2 * z                             by inequality, 0 < z
      >= 2 * y - x = mind t                    by [1]

   Case 3: otherwise
   Note mind t = x                             by mind_def
        mind (ping t)
      = (x + 2 * z) + 2 * z                    by mind_def
      > x = mind t                             by inequality, 0 < z
*)
Theorem mind_inc_by_ping:
  !x y z. is_ping (ping (x,y,z)) /\ 0 < z ==> mind (x,y,z) < mind (ping (x,y,z))
Proof
  simp[ping_def, mind_def, is_ping_def]
QED
(* A counter-example when condition is is_ping (x,y,z) instead:

  For n = 73, (7,6,1) = ping (5,12,1), but ~is_ping (7,6,1).
> EVAL ``is_ping (5,12,1)``;
val it = |- is_ping (5,12,1) <=> T: thm
> EVAL ``ping (5,12,1)``;
val it = |- ping (5,12,1) = (7,6,1): thm
> EVAL ``(mind (5,12,1), mind (ping (5,12,1)))``;
val it = |- (mind (5,12,1),mind (ping (5,12,1))) = (7,7): thm
> EVAL ``is_ping (7,6,1)``;
val it = |- is_ping (7,6,1) <=> F: thm
*)

(* Idea: pung (x,y,z) will generally decrease the mind, by last of 5 cases. *)

(* Theorem: 2 * y <= x ==> mind (pung (x,y,z)) <= mind (x,y,z) *)
(* Proof:
   Let t = (x,y,z).
   Then pung t = (x - 2 * y,y,x + z - y)       by pung_def
   Case 1: x < y - z
   Note mind t = x + 2 * z                     by mind_def
   If x - 2 * y < y - (x + z - y)
        mind (pung t)
      = (x - 2 * y) + 2 * y                    by mind_def
      = x                                      by arithmetic, 2 * y <= x
      <= x + 2 * z = mind t
   Otherwise, if x - 2 * y < y
        mind (pung t)
      = 2 * y - (x - 2 * y)                    by mind_def
      <= 2 * y                                 by inequality
      <= x                                     by 2 * y <= x
      <= x + 2 * z = mind t
   Otherwise,
        mind (pung t)
      = x - 2 * y                              by mind_def
      <= x                                     by inequality
      <= x + 2 * z = mind t

   Case 2: ~(x < y - z) /\ x < y
   That is, 2 * x < 2 * y
        ==> x < 2 * y - x                      by inequality [1]
   Note mind t = 2 * y - x                     by mind_def
   If x - 2 * y < y - (x + z - y)
        mind (pung t)
      = (x - 2 * y) + 2 * y                    by mind_def
      = x                                      by arithmetic, 2 * y <= x
      < 2 * y - x = mind t                     by [1]
   Otherwise, if x - 2 * y < y
        mind (pung t)
      = 2 * y - (x - 2 * y)                    by mind_def
      = 2 * y - x + 4 * y                      by 2 * y <= x
      <= 2 * y - x = mind t                    by inequality
   Otherwise,
        mind (pung t)
      = x - 2 * y                              by mind_def
      <= x                                     by inequality
      < 2 * y - x = mind t                     by [1]

   Case 3: otherwise
   That is, ~(x < y)
        ==> y <= x ==> 2 * y <= 2 * x
        ==> 2 * y - x <= x                     by inequality [2]
   Note mind t = x                             by mind_def
   If x - 2 * y < y - (x + z - y)
        mind (pung t)
      = (x - 2 * y) + 2 * y                    by mind_def
      = x = mind t                             by arithmetic, 2 * y <= x
   Otherwise, if x - 2 * y < y
        mind (pung t)
      = 2 * y - (x - 2 * y)                    by mind_def
      = 2 * y - x + 4 * y                      by 2 * y <= x
      <= 2 * y - x                             by inequality
      <= x = mind t                            by [2]
   Otherwise,
        mind (pung t)
      = x - 2 * y                              by mind_def
      <= x = mind t                            by inequality

   Overall, mind (pung t) <= mind t.
*)
Theorem mind_fall_by_pung:
  !x y z. 2 * y <= x ==> mind (pung (x,y,z)) <= mind (x,y,z)
Proof
  simp[pung_def, mind_def]
QED

(* Theorem: is_pung t ==> mind (pung t) <= mind t *)
(* Proof: by mind_fall_by_pung, is_pung_def. *)
Theorem mind_fall_by_pung_alt:
  !t. is_pung t ==> mind (pung t) <= mind t
Proof
  metis_tac[mind_fall_by_pung, is_pung_def, triple_parts, NOT_LESS]
QED

(* Theorem: is_ping (x,y,z) ==> mind (x,y,z) = x + 2 * z *)
(* Proof:
   Note x < y - z                  by is_ping_def
     so mind (x,y,z) = x + 2 * z   by mind_def
*)
Theorem mind_of_ping:
  !x y z. is_ping (x,y,z) ==> mind (x,y,z) = x + 2 * z
Proof
  simp[is_ping_def, mind_def]
QED

(* Theorem: is_pung (x,y,z) ==> mind (x,y,z) = x *)
(* Proof:
   Note ~(x < y - z) /\
        ~(x < 2 * y)               by is_pung_def
    ==> ~(x < y - z) /\ 2 * y <= x by NOT_LESS
    ==> ~(x < y - z) /\ y <= x     by y <= 2 * y
    ==> ~(x < y - z) /\ ~(x < y)   by NOT_LESS
     so mind (x,y,z) = x           by mind_def
*)
Theorem mind_of_pung:
  !x y z. is_pung (x,y,z) ==> mind (x,y,z) = x
Proof
  simp[is_pung_def, mind_def]
QED

(* Example of decrease of mind:

  For n = 73, (1,2,9) = pung (5,2,6).
> EVAL ``is_pung (5,2,6)``;
val it = |- is_pung (5,2,6) <=> T: thm
> EVAL ``pung (5,2,6)``;
val it = |- pung (5,2,6) = (1,2,9): thm
> EVAL ``(mind (5,2,6), mind (pung (5,2,6)))``;
val it = |- (mind (5,2,6),mind (pung (5,2,6))) = (5,3): thm
> EVAL ``is_pung (1,2,9)``;
val it = |- is_pung (1,2,9) <=> F: thm

  For n = 89, (1,2,11) = pung o pung (9,2,1)
> EVAL ``is_pung (9,2,1)``;
val it = |- is_pung (9,2,1) <=> T: thm
> EVAL ``pung (9,2,1)``;
val it = |- pung (9,2,1) = (5,2,8): thm
> EVAL ``(mind (9,2,1), mind (pung (9,2,1)))``;
val it = |- (mind (9,2,1),mind (pung (9,2,1))) = (9,5): thm
> EVAL ``is_pung (5,2,8)``;
val it = |- is_pung (5,2,8) <=> T: thm
> EVAL ``(mind (5,2,8), mind (pung (5,2,8)))``;
val it = |- (mind (5,2,8),mind (pung (5,2,8))) = (5,3): thm
*)

(* ------------------------------------------------------------------------- *)
(* Path on principal orbit of (flip o zagier) iteration.                     *)
(* ------------------------------------------------------------------------- *)

(* Define the path between fixed points. *)
Definition path_def:
   path n 0 = [(1,1,n DIV 4)] /\
   path n (SUC k) = SNOC ((flip o zagier) (LAST (path n k))) (path n k)
End

(* Extract theorems *)
Theorem path_0 = path_def |> CONJUNCT1;
(* val path_0 = |- !n. path n 0 = [(1,1,n DIV 4)]: thm *)
Theorem path_suc = path_def |> CONJUNCT2;
(* val path_suc = |- !n k. path n (SUC k) = SNOC ((flip o zagier) (LAST (path n k))) (path n k): thm *)

(*
> EVAL ``path 61 6``;
val it = |- path 61 6 = [(1,1,15); (1,15,1); (3,13,1); (5,9,1); (7,3,1); (1,3,5); (5,3,3)]: thm
> EVAL ``path 89 11``;
val it = |- path 89 11 = [(1,1,22); (1,22,1); (3,20,1); (5,16,1); (7,10,1); (9,2,1); (5,2,8); (1,2,11); (3,10,2); (7,5,2); (3,4,5); (5,4,4)]: thm
*)


(* Theorem: path n 1 = [(1,1,n DIV 4); (1,n DIV 4,1)] *)
(* Proof:
   Let f = flip o zagier,
       t = (1,1,n DIV 4).
   Note path n 0 = [t]             by path_0
     path n 1
   = path n (SUC 0)                by ONE
   = SNOC (f (LAST [t]) [t]        by path_suc
   = SNOC (f t) [t]                by LAST_SING
   = [t; f t]                      by SNOC
   = [t; (1,n DIV 4 1)]            by flip_zagier_x_x_z
*)
Theorem path_1:
  !n. path n 1 = [(1,1,n DIV 4); (1,n DIV 4,1)]
Proof
  rpt strip_tac >>
  qabbrev_tac `f = flip o zagier` >>
  qabbrev_tac `t = (1,1,n DIV 4)` >>
  `path n (SUC 0) = [t; f t]` suffices_by metis_tac[flip_zagier_x_x_z, ONE, DECIDE``0 < 1``] >>
  simp[path_suc, path_0, Abbr`f`, Abbr`t`]
QED

(* Theorem: path n k <> [] *)
(* Proof:
   If k = 0,   path n 0
             = [(1,1,n DIV 4)]     by path_0
             <> []                 by NOT_NIL_CONS
   If k = SUC j, for some j.
               path n (SUC j)
             = SNOC h ls           by path_suc, where
                                   ls = path n j
                                   h = (flip o zagier) (LAST ls)
             <> []                 by NOT_SNOC_NIL
*)
Theorem path_not_nil:
  !n k. path n k <> []
Proof
  metis_tac[num_CASES, path_def, NOT_NIL_CONS, NOT_SNOC_NIL]
QED

(* Theorem: LENGTH (path n k) = k + 1 *)
(* Proof:
   Let f = flip o zagier.
   By induction on k.
   Base: LENGTH (path n 0) = 0 + 1
         LENGTH (path n 0)
       = LENGTH [(1,1,n DIV 4)]                by path_0
       = 1 = 0 + 1                             by LENGTH_SING
   Step: LENGTH (path n k) = k + 1 ==> LENGTH (path n (SUC k)) = SUC k + 1
         LENGTH (path n (SUC k))
       = LENGTH (SNOC (f (LAST ls)) ls)        by path_suc, where ls = path n k
       = SUC (LENGTH ls)                       by LENGTH_SNOC
       = SUC (n + 1)                           by induction hypothesis
       = SUC n + 1                             by arithmetic
*)
Theorem path_length:
  !n k. LENGTH (path n k) = k + 1
Proof
  strip_tac >>
  qabbrev_tac `f = flip o zagier` >>
  Induct >-
  simp[path_0] >>
  simp[path_suc, LENGTH_SNOC]
QED

(* Theorem: HD (path n k) = (1,1,n DIV 4) *)
(* Proof:
   Let f = flip o zagier,
       t = (1,1,n DIV 4).
   By induction on k.
   Base: HD (path n 0) = t
         HD (path n k)
       = HD [t]                                by path_0
       = t                                     by HD
   Step: HD (path n k) = t ==> HD (path n (SUC k)) = t
       Let ls = path n k.
       Note ls <> []                           by path_not_nil
         so ls = HD ls::TL ls                  by LIST_NOT_NIL
         HD (path n (SUC k))
       = HD (SNOC (f (LAST ls)) ls)            by path_suc
       = HD (HD ls::SNOC (f (LAST ls) (TL ls)) by SNOC
       = HD ls                                 by HD
       = t                                     by induction hypothesis
*)
Theorem path_head:
  !n k. HD (path n k) = (1,1,n DIV 4)
Proof
  strip_tac >>
  qabbrev_tac `f = flip o zagier` >>
  qabbrev_tac `t = (1,1,n DIV 4)` >>
  Induct >-
  simp[path_0, Abbr`t`] >>
  qabbrev_tac `ls = path n k` >>
  `ls <> []` by simp[path_not_nil, Abbr`ls`] >>
  `ls = HD ls :: TL ls` by simp[GSYM LIST_NOT_NIL] >>
  metis_tac[path_suc, SNOC, HD]
QED

(* Theorem: LAST (path n k) = FUNPOW (flip o zagier) k (1,1,n DIV 4) *)
(* Proof:
   Let f = flip o zagier,
       t = (1,1,n DIV 4).
   By induction on j.
   Base: LAST (path n 0) = FUNPOW f 0 t
         LAST (path n 0)
       = LAST [t]                              by path_0
       = t                                     by LAST_CONS
   Step: LAST (path n k) = FUNPOW f n t ==> LAST (path n (SUC k)) = FUNPOW f (SUC n) t
       Let ls = path n k.
         LAST (path n (SUC k))
       = LAST (SNOC (f (LAST ls)) ls)          by path_suc
       = f (LAST ls)                           by LAST_SNOC
       = f (FUNPOW f n t)                      by induction hypothesis
       = FUNPOW f (SUC n) t                    by FUNPOW_SUC
*)
Theorem path_last:
  !n k. LAST (path n k) = FUNPOW (flip o zagier) k (1,1,n DIV 4)
Proof
  strip_tac >>
  qabbrev_tac `f = flip o zagier` >>
  qabbrev_tac `t = (1,1,n DIV 4)` >>
  Induct >-
  simp[path_0, LAST_CONS, Abbr`t`] >>
  metis_tac[path_suc, LAST_SNOC, FUNPOW_SUC]
QED

(* Theorem: HD (path n k) = EL 0 (path n k) *)
(* Proof: by EL. *)
Theorem path_head_alt:
  !n k. HD (path n k) = EL 0 (path n k)
Proof
  simp[]
QED

(* Theorem: LAST (path n k) = EL k (path n k) *)
(* Proof:
   Note LENGTH (path n k) = k + 1              by path_length
     so path n k <> []                         by LENGTH_EQ_0
        LAST (path n k)
      = EL (PRE (k + 1)) (path n k)            by LAST_EL
      = EL k (path n k)                        by arithmetic
*)
Theorem path_last_alt:
  !n k. LAST (path n k) = EL k (path n k)
Proof
  rpt strip_tac >>
  `LENGTH (path n k) = k + 1 ` by simp[path_length] >>
  `k + 1 <> 0 /\ PRE (k + 1) = k` by decide_tac >>
  metis_tac[LAST_EL, LENGTH_EQ_0]
QED

(* Theorem: path n k = [(1,1,n DIV 4)] <=> k = 0 *)
(* Proof:
   Note path n k <> []             by path_not_nil
     so ?h t. path n k = h::t      by list_CASES
    and h = (1,1,n DIV 4)          by path_head
    and k + 1 = LENGTH (path n k)  by path_length
              = SUC (LENGTH t)     by LENGTH
    ==> LENGTH t = k               by arithmetic
        path n k = [(1,1,n DIV 4)]
    <=> t = []                     by above
    <=> LENGTH t = 0               by LENGTH_EQ_0
    <=> k = 0                      by above
*)
Theorem path_eq_sing:
  !n k. path n k = [(1,1,n DIV 4)] <=> k = 0
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `?h t. ls = h::t`  by metis_tac[path_not_nil, list_CASES] >>
  `h = (1,1,n DIV 4)` by metis_tac[path_head, HD] >>
  `k + 1 = LENGTH ls` by fs[path_length, Abbr`ls`] >>
  `_ = SUC (LENGTH t)` by simp[] >>
  `LENGTH t = k` by decide_tac >>
  `h::t = [h] <=> t = []` by simp[] >>
  metis_tac[LENGTH_EQ_0]
QED

(* Theorem: LENGTH (FRONT (path n k)) = k *)
(* Proof:
   Let ls = path n k.
   Then ls <> []               by path_not_nil
     so LENGTH (FRONT ls)
      = PRE (LENGTH ls)        by LENGTH_FRONT
      = PRE (k + 1)            by path_length
      = k                      by PRE, ADD1
*)
Theorem path_front_length:
  !n k. LENGTH (FRONT (path n k)) = k
Proof
  simp[path_not_nil, path_length, LENGTH_FRONT]
QED

(* Theorem: 0 < k ==> HD (FRONT (path n k)) = (1,1,n DIV 4) *)
(* Proof:
   Let ls = path n k.
   Note ls <> []                   by path_not_nil
    and LENGTH ls = k + 1          by path_length
     so LENGTH (FRONT ls) = k      by LENGTH_FRONT

      HD (FRONT ls)
    = EL 0 (FRONT ls)              by EL
    = EL 0 ls                      by FRONT_EL, 0 < k
    = HD ls                        by EL
    = (1,1,n DIV 4)                by path_head
*)
Theorem path_front_head:
  !n k. 0 < k ==> HD (FRONT (path n k)) = (1,1,n DIV 4)
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `ls <> []` by simp[path_not_nil, Abbr`ls`] >>
  `LENGTH (FRONT ls) = k` by fs[LENGTH_FRONT, path_length, Abbr`ls`] >>
  metis_tac[FRONT_EL, EL, path_head]
QED

(* Theorem: j <= k ==> EL j (path n k) = FUNPOW (flip o zagier) j (1,1,n DIV 4) *)
(* Proof:
   Let f = flip o zagier,
       t = (1,1,n DIV 4).
   By induction on k.
   Base: !j. j <= 0 ==> EL j (path n 0) = FUNPOW f j t
       That is, j = 0.
         EL 0 (path n 0)
       = HD (path n 0)                         by EL
       = HD [t]                                by path_0
       = t                                     by HD
       = FUNPOW f 0 t                          by FUNPOW_0
   Step: !j. j <= k ==> EL j (path n k) = FUNPOW f j t ==>
         !j. j <= SUC k ==> EL j (path n (SUC k)) = FUNPOW f j t
       If j <= k, then j < k + 1.
          Let ls = path n k.
          Then LENGTH ls = k + 1               by path_length
            EL j (path n (SUC k))
          = EL j (SNOC (f (LAST ls)) ls)       by path_suc
          = EL j ls                            by EL_SNOC, j < LENGTH ls
          = FUNPOW f j t                       by induction hypothesis
       Otherwise, j = SUC k.
          Let ls = path n (SUC k).
          Note LENGTH ls = (SUC k) + 1         by path_length
            so PRE (LENGTH ls) = SUC k         by arithmetic
           and ls <> []                        by path_not_nil
            EL (SUC k) (path n (SUC k))
          = LAST (path n (SUC k))              by LAST_EL
          = FUNPOW f (SUC k) t                 by path_last
*)
Theorem path_element_eqn:
  !n k j. j <= k ==> EL j (path n k) = FUNPOW (flip o zagier) j (1,1,n DIV 4)
Proof
  strip_tac >>
  qabbrev_tac `f = flip o zagier` >>
  qabbrev_tac `t = (1,1,n DIV 4)` >>
  Induct >-
  simp[path_0, Abbr`t`] >>
  rpt strip_tac >>
  Cases_on `j <= k` >| [
    `j < k + 1` by decide_tac >>
    `LENGTH (path n k) = k + 1` by simp[path_length] >>
    simp[path_suc, EL_SNOC],
    `j = SUC k /\ PRE (j + 1) = j` by decide_tac >>
    `LENGTH (path n j) = j + 1` by simp[path_length] >>
    `path n j <> []` by simp[path_not_nil] >>
    metis_tac[LAST_EL, path_last]
  ]
QED
(* This is a major result. *)

(* Theorem: j < k ==> EL (SUC j) (path n k) = (flip o zagier) (EL j (path n k)) *)
(* Proof:
   Let f = flip o zagier,
       t = (1,1,n DIV 4).
   Note SUC j <= k.
     EL (SUC j) (path n k)
   = FUNPOW f (SUC j) t            by path_element_eqn
   = f (FUNPOW f j t)              by FUNPOW_SUC
   = f (EL j (path n k))           by path_element_eqn
*)
Theorem path_element_suc:
  !n k j. j < k ==> EL (SUC j) (path n k) = (flip o zagier) (EL j (path n k))
Proof
  simp[path_element_eqn, FUNPOW_SUC]
QED
(* Note: this is the key relationship between path elements. *)

(* Theorem: j + h <= k ==> EL (j + h) (path n k) = FUNPOW (flip o zagier) h (EL j (path n k)) *)
(* Proof:
   Let ls = path n k,
       t = (1,1,n DIV 4),
       f = flip o zagier.
     EL (j + h) ls
   = FUNPOW f (j + h) t            by path_element_eqn
   = FUNPOW f (h + j) t            by ADD_COMM
   = FUNPOW f h (FUNPOW f j t)     by FUNPOW_ADD
   = FUNPOW f h (EL j ls)          by path_element_eqn
*)
Theorem path_element_thm:
  !n k j h. j + h <= k ==> EL (j + h) (path n k) = FUNPOW (flip o zagier) h (EL j (path n k))
Proof
  rw[path_element_eqn] >>
  simp[FUNPOW_ADD]
QED

(* Theorem: tik n /\ j < k ==> EL j (path n k) IN mills n *)
(* Proof:
   Let ls = path n k.
   By induction on j.
   Base: 0 <= k ==> EL 0 ls IN mills n
         EL 0 ls
       = HD ls                     by EL
       = (1,1,n DIV 4)             by path_head
     and (1,1,n DIV 4) IN mills n  by mills_element_trivial, tik n

   Step: j <= k ==> EL j ls IN mills n ==>
         SUC j <= k ==> EL (SUC j) ls IN mills n
       Note SUC j <= k ==> j < k, making j <= k.
         EL (SUC j) ls
       = (flip o zagier) (EL j ls)             by path_element_suc, j < k
       = flip (zagier (EL j ls))               by composition
     Note (EL j ls) IN mills n                 by inductive hypothesis, j <= k
      ==> zagier (EL j ls) IN mills n          by zagier_closure
      ==> flip (zagier (EL j ls)) IN mills n   by flip_closure
*)
Theorem path_element_in_mills:
  !n k j. tik n /\ j <= k ==> EL j (path n k) IN mills n
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  Induct_on `j` >-
  simp[path_head, mills_element_trivial, Abbr`ls`] >>
  rpt strip_tac >>
  `EL (SUC j) ls = (flip o zagier) (EL j ls)` by simp[path_element_suc, Abbr`ls`] >>
  simp[zagier_closure, flip_closure]
QED

(* Theorem: tik n /\ j <= k /\ EL j (path n k) = (x,y,z) ==> n = windmill (x,y,z) *)
(* Proof:
   Let ls = path n k.
   Note EL j ls = (x,y,z) IN (mills n)         by path_element_in_mills, j <= k
     so n = windmill (x,y,z)                   by mills_element_alt
*)
Theorem path_element_windmill:
  !n k j x y z. tik n /\ j <= k /\ EL j (path n k) = (x,y,z) ==> n = windmill (x,y,z)
Proof
  metis_tac[path_element_in_mills, mills_element_alt]
QED

(* Theorem: tik n /\ j <= k ==> n = windmill (EL j (path n k)) *)
(* Proof: by path_element_windmill, triple_parts. *)
Theorem path_element_windmill_alt:
  !n k j. tik n /\ j <= k ==> n = windmill (EL j (path n k))
Proof
  metis_tac[path_element_windmill, triple_parts]
QED

(* Theorem: is_pong (EL 0 (path n k)) *)
(* Proof:
       is_pong (EL 0 (path n k))
   <=> is_pong (HD (path n k))     by EL
   <=> is_pong (1,1,n DIV 4)       by path_head
   <=> T                           by is_pong_def, 1 < 2 * 1
*)
Theorem path_head_is_pong:
  !n k. is_pong (EL 0 (path n k))
Proof
  simp[path_head, is_pong_def]
QED

(* Theorem: EL 0 (path n k) = (1,1,n DIV 4) *)
(* Proof:
     EL 0 (path n k)
   = HD (path n k)             by path_head_alt
   = (1,1,n DIV 4)             by path_head
*)
Theorem path_element_0:
  !n k. EL 0 (path n k) = (1,1,n DIV 4)
Proof
  simp[GSYM path_head_alt, path_head]
QED

(* Theorem: 0 < k ==> EL 1 (path n k) = (1,n DIV 4,1) *)
(* Proof:
   Let t = EL 0 (path n k)
   Then t = (1,1,n DIV 4)      by path_element_0
   Note is_pong t              by path_head_is_pong
     EL 1 (path n k)
   = EL (SUC 0) (path n k)     by ONE
   = (flip o zagier) t         by path_element_suc, 0 < k
   = pong t                    by flip_zagier_pong
   = (1,n DIV 4,1)             by pong_def
*)
Theorem path_element_1:
  !n k. 0 < k ==> EL 1 (path n k) = (1,n DIV 4,1)
Proof
  rpt strip_tac >>
  qabbrev_tac `t = EL 0 (path n k)` >>
  `is_pong t` by simp[path_head_is_pong, Abbr`t`] >>
  `EL 1 (path n k) = EL (SUC 0) (path n k)` by simp[GSYM ONE] >>
  `_ = (flip o zagier) t` by simp[path_element_suc, Abbr`t`] >>
  `_ = pong t` by simp[flip_zagier_pong] >>
  simp[pong_def, path_element_0, Abbr`t`]
QED

(* Theorem: 0 < k ==> let t = EL 1 (path n k) in
            if n < 4 then is_pung t else if n < 12 then is_pong t else is_ping t *)
(* Proof:
   Let t = EL 1 (path n k).
   Then t = (1,n DIV 4,1)          by path_element_1
   Note n DIV 4 <= 2 <=> n < 3 * 4 by DIV_LE_X
     or n DIV 4 <= 2 <=> n < 12    by arithmetic
   Also n DIV 4 <= 0 <=> n < 1 * 4 by DIV_LE_X
     or n DIV 4 = 0  <=> n < 4     by arithmetic

   Thus, if n < 4,
       Then n DIV 4 = 0,
       ==> is_pung t               by is_pung_def
   If 4 <= n but n < 12,
       Then n DIV 4 <= 2,
       ==> is_pong t               by is_pong_def
   Otherwise, 12 <= n,
       Then 2 < n DIV 4,
       ==> is_ping t               by is_ping_def
*)
Theorem path_head_next_property:
  !n k. 0 < k ==> let t = EL 1 (path n k) in
        if n < 4 then is_pung t else if n < 12 then is_pong t else is_ping t
Proof
  rw_tac std_ss[] >>
  `t = (1, n DIV 4, 1)` by simp[path_element_1, Abbr`t`] >>
  `n DIV 4 <= 0 <=> n < 4` by simp[DIV_LE_X] >>
  `n DIV 4 <= 2 <=> n < 12` by simp[DIV_LE_X] >>
  (Cases_on `n < 4` >> simp[]) >-
  simp[is_pung_def] >>
  (Cases_on `n < 12` >> simp[]) >-
  simp[is_pong_def] >>
  simp[is_ping_def]
QED

(* Theorem: let ls = path n k in flip (LAST ls) = LAST ls ==> ~is_ping (LAST ls) *)
(* Proof:
   Let t = LAST ls.
   Then t = (x,y,z)                by triple_parts
    and flip t = t ==> y = z       by flip_fix
     so ~is_ping t                 by not_ping_x_y_y
*)
Theorem path_last_not_ping:
  !n k. let ls = path n k in flip (LAST ls) = LAST ls ==> ~is_ping (LAST ls)
Proof
  rw_tac std_ss[] >>
  metis_tac[triple_parts, flip_fix, not_ping_x_y_y]
QED

(* This is the short version. See the long version below. *)

(*
involute_involute_fix_orbit_fix_odd_inv |> ISPEC ``zagier`` |> ISPEC ``flip`` |> ISPEC ``mills n`` |> SPEC ``p:num`` |> ISPEC ``(1,1,n DIV 4)``;
val it = |- FINITE (mills n) /\ zagier involute mills n /\ flip involute mills n /\
      (1,1,n DIV 4) IN fixes zagier (mills n) /\
      p = iterate_period (flip o zagier) (1,1,n DIV 4) /\ ODD p ==>
      FUNPOW (flip o zagier) (1 + HALF p) (1,1,n DIV 4) IN
      fixes flip (mills n): thm

path_last  |- !n k. LAST (path n k) = FUNPOW (flip o zagier) k (1,1,n DIV 4)
path_last |> SPEC ``n:num`` |> SPEC ``1 + HALF p``;
val it = |- LAST (path n (1 + HALF p)) = FUNPOW (flip o zagier) (1 + HALF p) (1,1,n DIV 4): thm
*)

(* Idea: the last element of path is not a ping. *)

(* Theorem: prime n /\ tik n /\ p = iterate_period (flip o zagier) (1,1,n DIV 4) ==>
            ~is_ping (LAST (path n (1 + HALF p))) *)
(* Proof:
   Let u = (1,1,n DIV 4),
       k = 1 + HALF p,
       v = FUNPOW (flip o zagier) k u.
   Note prime n ==> ~square n            by prime_non_square
     so FINITE (mills n)                 by mills_finite_non_square
    and zagier involute (mills n)        by zagier_involute_mills_prime, prime n
    and flip involute (mills n)          by flip_involute_mills
    and fixes zagier (mills n) = {u}     by zagier_fixes_prime
     so u IN fixes zagier (mills n)      by IN_SING

    Now u IN s                           by mills_element_trivial, tik n
    and iterate_period (flip o zagier) u
      = iterate_period (zagier o flip) u by involute_involute_period_inv
     so ODD p                            by involute_involute_fix_sing_period_odd
    ==> v IN fixes flip (mills n)        by involute_involute_fix_orbit_fix_odd_inv
    But v = LAST (path n k)              by path_last
    and ~is_ping v                       by flip_fixes_element, not_ping_x_y_y
*)
Theorem path_last_not_ping_thm:
  !n p. prime n /\ tik n /\ p = iterate_period (flip o zagier) (1,1,n DIV 4) ==>
        ~is_ping (LAST (path n (1 + HALF p)))
Proof
  rpt strip_tac >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  qabbrev_tac `k = 1 + HALF p` >>
  qabbrev_tac `v = FUNPOW (flip o zagier) k u` >>
  qabbrev_tac `s = mills n` >>
  `~square n` by simp[prime_non_square] >>
  `FINITE s` by fs[mills_finite_non_square, Abbr`s`] >>
  `zagier involute s` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `fixes zagier s = {u}` by fs[zagier_fixes_prime, Abbr`s`, Abbr`u`] >>
  `u IN fixes zagier s` by simp[] >>
  `u IN s` by metis_tac[mills_element_trivial] >>
  qabbrev_tac `f = zagier` >>
  qabbrev_tac `g = flip` >>
  drule_then strip_assume_tac involute_involute_period_inv >>
  first_x_assum (qspecl_then [`f`, `g`, `u`] strip_assume_tac) >>
  `iterate_period (f o g) u = p` by metis_tac[] >>
  drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
  first_x_assum (qspecl_then [`f`, `g`, `p`, `u`] strip_assume_tac) >>
  `ODD p` by metis_tac[] >>
  drule_then strip_assume_tac involute_involute_fix_orbit_fix_odd_inv >>
  first_x_assum (qspecl_then [`f`, `g`, `p`, `u`] strip_assume_tac) >>
  `v IN fixes g s` by metis_tac[] >>
  `v = LAST (path n k)` by metis_tac[path_last] >>
  metis_tac[triple_parts, flip_fixes_element, not_ping_x_y_y]
QED

(* Theorem: let ls = path n k; (x,y,z) = EL u ls in m + u <= k /\
            (!j. j < m ==> is_ping (EL (u + j) ls)) ==> !j. j < m ==> EL (u + j) ls = FUNPOW ping j (x,y,z) *)
(* Proof:
   By induction on j.
   Base: 0 < m ==> EL (u + 0) ls = FUNPOW ping 0 (x,y,z)
      This gives m = 0.
          EL u ls
        = (x,y,z)                              by given
        = FUNPOW 0 (x,y,z)                     by FUNPOW_0
   Step: j < m ==> EL (u + j) ls = FUNPOW ping j (x,y,z) ==>
         SUC j < m ==> EL (u + SUC j) ls = FUNPOW ping (SUC j) (x,y,z)
      Note SUC j < m ==> j < m                 by inequality
      Thus is_ping (EL (u + j) ls)             by given condition
         EL (u + SUC j) ls
       = EL (SUC (u + j)) ls                   by ADD_CLAUSES
       = (flip o zagier) (EL (u + j) ls)       by path_element_suc, j <= m
       = ping (EL (u + j) ls)                  by flip_zagier_ping
       = ping (FUNPOW ping j (x,y,z))          by induction hypothesis
       = FUNPOW ping (SUC j) (x,y,z)           by FUNPOW_SUC
*)
Theorem path_element_ping_funpow:
  !n k u m. let ls = path n k; (x,y,z) = EL u ls in m + u <= k /\
            (!j. j < m ==> is_ping (EL (u + j) ls)) ==> !j. j < m ==> EL (u + j) ls = FUNPOW ping j (x,y,z)
Proof
  rw_tac std_ss[] >>
  Induct_on `j` >-
  simp[] >>
  strip_tac >>
  `j < m` by decide_tac >>
  `is_ping (EL (u + j) ls)` by metis_tac[] >>
  `EL (u + SUC j) ls = EL (SUC (u + j)) ls` by metis_tac[ADD_CLAUSES] >>
  `_ = (flip o zagier) (EL (u + j) ls)` by fs[path_element_suc, Abbr`ls`] >>
  `_ = ping (EL (u + j) ls)` by metis_tac[flip_zagier_ping] >>
  fs[FUNPOW_SUC]
QED

(* Theorem: let ls = path n k; (x,y,z) = EL u ls in m + u <= k /\
            (!j. j < m ==> is_pung (EL (u + j) ls)) ==> !j. j < m ==> EL (u + j) ls = FUNPOW pung j (x,y,z) *)
(* Proof:
   By induction on j.
   Base: 0 < m ==> EL (u + 0) ls = FUNPOW pung 0 (x,y,z)
      This gives m = 0.
          EL u ls
        = (x,y,z)                              by given
        = FUNPOW 0 (x,y,z)                     by FUNPOW_0
   Step: j < m ==> EL (u + j) ls = FUNPOW pung j (x,y,z) ==>
         SUC j < m ==> EL (u + SUC j) ls = FUNPOW pung (SUC j) (x,y,z)
      Note SUC j < m ==> j < m                 by inequality
      Thus is_pung (EL (u + j) ls)             by given condition
         EL (u + SUC j) ls
       = EL (SUC (u + j)) ls                   by ADD_CLAUSES
       = (flip o zagier) (EL (u + j) ls)       by path_element_suc, j <= m
       = pung (EL (u + j) ls)                  by flip_zagier_pung
       = pung (FUNPOW pung j (x,y,z))          by induction hypothesis
       = FUNPOW pung (SUC j) (x,y,z)           by FUNPOW_SUC
*)
Theorem path_element_pung_funpow:
  !n k u m. let ls = path n k; (x,y,z) = EL u ls in m + u <= k /\
            (!j. j < m ==> is_pung (EL (u + j) ls)) ==> !j. j < m ==> EL (u + j) ls = FUNPOW pung j (x,y,z)
Proof
  rw_tac std_ss[] >>
  Induct_on `j` >-
  simp[] >>
  strip_tac >>
  `j < m` by decide_tac >>
  `is_pung (EL (u + j) ls)` by metis_tac[] >>
  `EL (u + SUC j) ls = EL (SUC (u + j)) ls` by metis_tac[ADD_CLAUSES] >>
  `_ = (flip o zagier) (EL (u + j) ls)` by fs[path_element_suc, Abbr`ls`] >>
  `_ = pung (EL (u + j) ls)` by metis_tac[flip_zagier_pung] >>
  fs[FUNPOW_SUC]
QED

(* ------------------------------------------------------------------------- *)
(* Sections and blocks in a path.                                            *)
(* ------------------------------------------------------------------------- *)

(* Define the skip functions *)
Definition skip_ping_def:
   skip_ping ls k = WHILE (\j. is_ping (EL j ls)) SUC k
End

Definition skip_pung_def:
   skip_pung ls k = WHILE (\j. is_pung (EL j ls)) SUC k
End

(*
> EVAL  ``skip_ping (path 61 6) 1``;  = 4
> EVAL  ``skip_pung (path 61 6) 4``;  = 5
*)

(* Theorem: k <= h /\ (!j. k <= j /\ j < h ==> is_ping (EL j ls)) /\
            ~is_ping (EL h ls) ==> skip_ping ls k = h *)
(* Proof: by skip_idx_while_thm, skip_ping_def. *)
Theorem skip_ping_thm:
  !ls k h. k <= h /\ (!j. k <= j /\ j < h ==> is_ping (EL j ls)) /\
           ~is_ping (EL h ls) ==> skip_ping ls k = h
Proof
  simp[skip_ping_def, skip_idx_while_thm]
QED

(* Theorem: ~is_ping (EL k ls) ==> skip_ping ls k = k` *)
(* Proof: by skip_ping_thm, h = k. *)
Theorem skip_ping_none:
  !ls k. ~is_ping (EL k ls) ==> skip_ping ls k = k
Proof
  simp[skip_ping_thm]
QED

(* Theorem: k <= h /\ (!j. k <= j /\ j < h ==> is_pung (EL j ls)) /\
            ~is_pung (EL h ls) ==> skip_pung ls k = h *)
(* Proof: by skip_idx_while_thm, skip_pung_def. *)
Theorem skip_pung_thm:
  !ls k h. k <= h /\ (!j. k <= j /\ j < h ==> is_pung (EL j ls)) /\
           ~is_pung (EL h ls) ==> skip_pung ls k = h
Proof
  simp[skip_pung_def, skip_idx_while_thm]
QED

(* Theorem: ~is_pung (EL k ls) ==> skip_pung ls k = k` *)
(* Proof: by skip_pung_thm, h = k. *)
Theorem skip_pung_none:
  !ls k. ~is_pung (EL k ls) ==> skip_pung ls k = k
Proof
  simp[skip_pung_thm]
QED

(* Idea: between a pung and a ping, there must be a pong. *)

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pung (EL j ls) /\ is_ping (EL h ls) ==>
            ?p. j < p /\ p < h /\ is_pong (EL p ls) *)
(* Proof:
   Let ls = path n k.
   By contradiction, assume !p. j < p /\ p < h ==> ~is_pong (EL p ls).
   thus is_ping (EL p ls) \/ is_pung (EL p ls) by triple_cases
   Note j < h /\ is_pung (EL j ls)             by given
    and is_ping (EL h ls) ==>
        ~is_pung (EL h ls)                     by ping_not_pung
    ==> ?m. j <= m /\ m < h /\
           (!p. j <= p /\ p <= m ==> is_pung (EL p ls)) /\
            ~is_pung (EL (SUC m) ls)           by every_range_span_max
   Note ~is_pong (EL (SUC m) ls)               by every_range_less_ends, j < SUC m < h
    ==> is_ping (EL (SUC m) ls)                by triple_cases, [1]

    Now is_pung (EL m ls)                      by j <= m /\ m <= m
    But m < h <= k,
     so EL (SUC m) ls = pung (EL m ls)         by path_element_suc, flip_zagier_pung, m < k
    ==> ~is_ping (pung (EL m ls))              by pung_next_not_ping_alt
    This is a contradiction                    by [1]
*)
Theorem pung_to_ping_has_pong:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pung (EL j ls) /\ is_ping (EL h ls) ==>
            ?p. j < p /\ p < h /\ is_pong (EL p ls)
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  `?m. j <= m /\ m < h /\ is_pung (EL m ls) /\ ~is_pung (EL (SUC m) ls)` by
  (`~is_pung (EL h ls)` by simp[ping_not_pung] >>
  assume_tac every_range_span_max >>
  last_x_assum (qspecl_then [`\j. is_pung (EL j ls)`,`j`,`h`] strip_assume_tac) >>
  rfs[] >>
  `is_pung (EL m ls)` by fs[] >>
  metis_tac[]) >>
  `!p. j <= p ==> p <= h ==> ~is_pong (EL p ls)` by
    (`j <= h` by decide_tac >>
  assume_tac every_range_less_ends >>
  last_x_assum (qspecl_then [`\j. ~is_pong (EL j ls)`,`j`,`h`] strip_assume_tac) >>
  rfs[ping_not_pong, pung_not_pong]) >>
  `j <= SUC m /\ SUC m <= h` by decide_tac >>
  `~is_pong (EL (SUC m) ls)` by fs[] >>
  `is_ping (EL (SUC m) ls)` by metis_tac[triple_cases_alt] >>
  `EL (SUC m) ls = pung (EL m ls)` by fs[path_element_suc, flip_zagier_pung, Abbr`ls`] >>
  metis_tac[pung_next_not_ping_alt]
QED

(* Yes! This is the key result for the rest. *)

(* Idea: between two pongs, if there is a ping, then all pings after the starting pong. *)

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            !e. j < e /\ e < h /\ is_ping (EL e ls) ==> !p. j < p /\ p <= e ==> is_ping (EL p ls) *)
(* Proof:
   By contradiction, suppose there is (EL p ls) such that:
      j < p <= e /\ ~is_ping (EL p ls).
   Note !p. j < p < h ==> ~is_pong (EL p ls)   by given, [1]
     so for this p,
        j < p <= e < h ==> j < p < h ==> ~is_pong (EL p ls)
   Thus is_pung (EL p ls)                      by triple_cases_alt
    But is_ping (EL e ls)                      by given
   With p <= e, and p <> e, so p < e           by ping_not_pung
    and e < h <= k, so e <= k
    ==> ?q. p < q /\ q < e /\ is_pong (EL q ls)  by pung_to_ping_has_pong, p < e
    yet j < p < q < e < h,  ~is_pong (EL q ls)   by [1]
   This is a contradiction.
*)
Theorem pong_interval_ping_start:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            !e. j < e /\ e < h /\ is_ping (EL e ls) ==> !p. j < p /\ p <= e ==> is_ping (EL p ls)
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  `p < h` by decide_tac >>
  `~is_pong (EL p ls)` by fs[] >>
  `is_pung (EL p ls)` by metis_tac[triple_cases_alt] >>
  `p <> e` by metis_tac[ping_not_pung] >>
  `p < e /\ e <= k` by decide_tac >>
  assume_tac pung_to_ping_has_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `p`,`e`] strip_assume_tac) >>
  `?q. p < q /\ q < e /\ is_pong (EL q ls)` by metis_tac[] >>
  rfs[]
QED

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            !e. j < e /\ e < h /\ is_ping (EL e ls) ==> EVERY (\p. is_ping (EL p ls)) [j+1 .. e] *)
(* Proof:
   Note EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h]
    <=> !p. j + 1 <= p /\ p < h ==> ~is_pong (EL p ls)     by listRangeLHI_EVERY
    <=> !p. j< p /\ p < h ==> ~is_pong (EL p ls)
    and EVERY (\p. is_ping (EL p ls)) [j+1 .. e]
    <=> !p. j + 1 <= p /\ p <= e ==> is_ping (EL p ls)     by listRangeINC_EVERY
    <=> !p. j < p /\ p <= e ==> is_ping (EL p ls)
    The result follows from pong_interval_ping_start.
*)
Theorem pong_interval_ping_start_alt:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            !e. j < e /\ e < h /\ is_ping (EL e ls) ==> EVERY (\p. is_ping (EL p ls)) [j+1 .. e]
Proof
  rw_tac std_ss[] >>
  rfs[listRangeLHI_EVERY, listRangeINC_EVERY] >>
  `!p. j + 1 <= p <=> j < p` by decide_tac >>
  assume_tac pong_interval_ping_start >>
  last_x_assum (qspecl_then [`n`, `k`, `j`,`h`] strip_assume_tac) >>
  metis_tac[]
QED

(* Idea: between two pongs, if there is a pung, then all pungs to the ending pong. *)

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            !e. j < e /\ e < h /\ is_pung (EL e ls) ==> !p. e <= p /\ p < h ==> is_pung (EL p ls) *)
(* Proof:
   By contradiction, suppose there is (EL p ls) such that:
      e <= p < h /\ ~is_pung (EL p ls).
   Note !p. j < p < h ==> ~is_pong (EL p ls)   by given, [1]
     so for this p,
        j < e <= p < h ==> j < p < h ==> ~is_pong (EL p ls)
   Thus is_ping (EL p ls)                      by triple_cases_alt
    But is_pung (EL e ls)                      by given
   With e <= p, and e <> p, so e < p           by ping_not_pung
    and p < h <= k, so p <= k
    ==> ?q. e < q /\ q < p /\ is_pong (EL q ls)  by pung_to_ping_has_pong, e < p
    yet j < e <= p < q < h,  ~is_pong (EL q ls)  by [1]
   This is a contradiction.
*)
Theorem pong_interval_pung_stop:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            !e. j < e /\ e < h /\ is_pung (EL e ls) ==> !p. e <= p /\ p < h ==> is_pung (EL p ls)
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  `j < p` by decide_tac >>
  `~is_pong (EL p ls)` by fs[] >>
  `is_ping (EL p ls)` by metis_tac[triple_cases_alt] >>
  `e <> p` by metis_tac[ping_not_pung] >>
  `e < p /\ p <= k` by decide_tac >>
  assume_tac pung_to_ping_has_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `e`,`p`] strip_assume_tac) >>
  `?q. e < q /\ q < p /\ is_pong (EL q ls)` by metis_tac[] >>
  rfs[]
QED

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            !e. j < e /\ e < h /\ is_pung (EL e ls) ==> EVERY (\p. is_pung (EL p ls)) [e ..< h] *)
(* Proof:
   Note EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h]
    <=> !p. j + 1 <= p /\ p < h ==> ~is_pong (EL p ls)     by listRangeLHI_EVERY
    <=> !p. j< p /\ p < h ==> ~is_pong (EL p ls)
    and EVERY (\p. is_pung (EL p ls)) [e ..< h]
    <=> !p. e <= p /\ p < h ==> is_pung (EL p ls)          by listRangeINC_EVERY
    The result follows from pong_interval_pung_stop.
*)
Theorem pong_interval_pung_stop_alt:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            !e. j < e /\ e < h /\ is_pung (EL e ls) ==> EVERY (\p. is_pung (EL p ls)) [e ..< h]
Proof
  rw_tac std_ss[] >>
  rfs[listRangeLHI_EVERY] >>
  `!p. j + 1 <= p <=> j < p` by decide_tac >>
  assume_tac pong_interval_pung_stop >>
  last_x_assum (qspecl_then [`n`, `k`, `j`,`h`] strip_assume_tac) >>
  metis_tac[]
QED

(* Idea: the cut exists, in that between two pongs, there is a switch-over from ping to pung. *)

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            (!p. j < p /\ p < c ==> is_ping (EL p ls)) /\ (!p. c <= p /\ p < h ==> is_pung (EL p ls)) *)
(* Proof:
   Note j < h ==> SUC j <= h.
   If SUC j = h,
      pick c = h, that is c = SUC j.
      Then j < p < c = SUC j ==> no such p for is_ping (EL p ls)
       and h = c <= p < h ==> no such p for is_pung (EL p ls)
   Otherwise, SUC j < h.
      Let q = SUC j, then j < q < h.
      Note ~is_pong (EL q ls)                  by given
       so either is_ping or is_pung            by triple_cases

      If is_ping (EL q ls),
         Note ~is_ping (EL h ls)               by pong_not_ping
          ==> ?m. p <= m /\ m < h /\
                  (!j. p <= j /\ j <= m ==> is_ping (EL j ls)) /\
                  ~is_ping (EL (SUC m) ls)     by every_range_span_max
         Pick c = SUC m = m + 1.
         Note !p. SUC j <= p <=> j < p         by arithmetic
           so !p. j < p /\ p < c ==> is_ping (EL p ls)     by above

          Now ~is_ping (EL c ls)               by above
          and c <= h means c = h or c < h.
         If c = h, then trivially
            !p. c <= p /\ p < h ==> is_pung (EL p ls).
            and ~is_ping (EL c ls)             by pong_not_ping
         If c < h,
            Then ~is_pong (EL c ls)            by j < c < h
              so is_pung (EL c ls)             by triple_cases_alt
             ==> !p. c <= p /\ p < h ==> is_pung (EL p ls)
                                               by pong_interval_pung_stop
             and ~is_ping (EL c ls)            by pung_not_ping

      If is_pung (EL q ls),
         Pick c = SUC j.
         Then j < p < c = SUC j ==> no such p for is_ping (EL p ls)
          and !p. j < p <=> SUC j = c <= p     by arithmetic
           so !p. c <= p /\ p < h ==> is_pung (EL p ls)
                                               by pong_interval_pung_stop
          and if c < h, ~is_ping (EL c ls)     by pung_not_ping
           or if c = h, ~is_ping (EL c ls)     by pong_not_ping
*)
Theorem pong_interval_cut_exists:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            (!p. j < p /\ p < c ==> is_ping (EL p ls)) /\ (!p. c <= p /\ p < h ==> is_pung (EL p ls))
Proof
  rw_tac std_ss[] >>
  `SUC j = h \/ SUC j < h` by decide_tac >| [
    qexists_tac `h` >>
    simp[pong_not_ping],
    `j < SUC j` by decide_tac >>
    qabbrev_tac `q = SUC j` >>
    `~is_pong (EL q ls)` by fs[] >>
    `is_ping (EL q ls) \/ is_pung (EL q ls)` by metis_tac[triple_cases_alt] >| [
      `~is_ping (EL h ls)` by simp[pong_not_ping] >>
      assume_tac every_range_span_max >>
      last_x_assum (qspecl_then [`\p. is_ping (EL p ls)`, `q`, `h`] strip_assume_tac) >>
      rfs[] >>
      `q < SUC m /\ SUC m <= h` by decide_tac >>
      qabbrev_tac `c = SUC m` >>
      qexists_tac `c` >>
      `!p. q <= p /\ p <= m <=> j < p /\ p < c` by simp[Abbr`q`, Abbr`c`] >>
      rfs[] >>
      `c = h \/ c < h` by decide_tac >-
      simp[] >>
      `~is_pong (EL c ls)` by fs[] >>
      `is_pung (EL c ls)` by metis_tac[triple_cases_alt] >>
      assume_tac pong_interval_pung_stop >>
      last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
      rfs[] >>
      `j < c` by fs[Abbr`q`] >>
      metis_tac[],
      qexists_tac `q` >>
      rfs[pung_not_ping, Abbr`q`] >>
      assume_tac pong_interval_pung_stop >>
      last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
      rfs[] >>
      `j < SUC j` by decide_tac >>
      metis_tac[]
    ]
  ]
QED

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            EVERY (\p. is_ping (EL p ls)) [j+1 ..< c] /\ EVERY (\p. is_pung (EL p ls)) [c ..< h] *)
(* Proof:
       EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h]
   <=> !p. j + 1 <= p /\ p < h ==> ~is_pong (EL p ls)      by listRangeLHI_EVERY
   <=> !p. j < p /\ p < h ==> ~is_pong (EL p ls)           by arithmetic
   Thus ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            (!p. j < p /\ p < c ==> is_ping (EL p ls)) /\
            (!p. c <= p /\ p < h ==> is_pung (EL p ls))    by pong_interval_cut_exists
    ==> !p. j + 1 <= p /\ p < c ==> is_ping (EL p ls)      by arithmetic
   Thus EVERY (\p. is_ping (EL p ls)) [j+1 ..< c] /\
        EVERY (\p. is_pung (EL p ls)) [c ..< h]            by listRangeLHI_EVERY
*)
Theorem pong_interval_cut_exists_alt:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            EVERY (\p. is_ping (EL p ls)) [j+1 ..< c] /\ EVERY (\p. is_pung (EL p ls)) [c ..< h]
Proof
  rw[listRangeLHI_EVERY] >>
  `!p b. j + 1 <= p /\ p < b <=> j < p /\ p < b` by decide_tac >>
  assume_tac pong_interval_cut_exists >>
  last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
  metis_tac[]
QED

(* Yes! A major achievement! *)

(* Example:
ls = path 61 6
is_pong (EL 0 ls) = is_pong (1,1,15) = T
is_ping (EL 1 ls) = is_ping (1,15,1) = T
is_ping (EL 2 ls) = is_ping (3,13,1) = T
is_ping (EL 3 ls) = is_ping (5,9,1) = T
is_pung (EL 4 ls) = is_pung (7,3,1) = T
ls_pong (EL 5 ls) = is_pong (1,3,5) = T
is_pong (EL 6 ls) = is_pong (5,3,3) = T

cut c = 4.
*)

(* Idea: let e be a pong, then there are j and h with j < e a stretch of pungs, and e < h a strech of pings. *)

(* Theorem: let ls = path n k in ~is_pung (HD ls) /\ e < k /\ is_pong (EL e ls) ==>
             ?j. j <= e /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls) *)
(* Proof:
   If e = 0,
      Pick j = 0, then PRE j = 0 = e           by arithmetic
      Then is_pong (EL e ls)
       ==> ~is_pung (EL (PRE j) ls)            by pong_not_pung
      and the range 0 <= p /\ p < e is empty.

   If 0 < e,
      Let d = e - 1, then d < e.
      Consider the three cases of (EL d ls).

      Case: is_ping (EL d ls)
         Pick j = e, then PRE j = PRE e = d.
         Then ~is_pung (EL d ls)               by ping_not_pung
          and the range e <= p /\ p < e is empty.

      Case: is_pong (EL d ls)
         Pick j = e, then PRE j = PRE e = d.
         Then ~is_pung (EL d ls)               by pong_not_pung
          and the range e <= p /\ p < e is empty.

      Case: is_pung (EL d ls)
         Note HD ls = EL 0 ls                  by EL
          and ~is_pung (EL 0 ls)               by given
          ==> ?m. 0 < m /\ m <= e /\
                  (!j. m <= j /\ j <= e ==> is_pung (EL j ls)) /\
               ~is_pung (EL (PRE m) ls)        by every_range_span_min, 0 < e
         Pick j = m, and j <= e.
*)
Theorem pong_seed_pung_before:
  !n k e. let ls = path n k in ~is_pung (HD ls) /\ e < k /\ is_pong (EL e ls) ==>
          ?j. j <= e /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls)
Proof
  rw_tac std_ss[] >>
  `e = 0 \/ 0 < e` by decide_tac >| [
    qexists_tac `0` >>
    fs[pong_not_pung],
    `e - 1 < e /\ PRE e = e - 1` by decide_tac >>
    qabbrev_tac `d = e - 1` >>
    `is_ping (EL d ls) \/ is_pong (EL d ls) \/ is_pung (EL d ls)` by metis_tac[triple_cases_alt] >| [
      qexists_tac `e` >>
      fs[ping_not_pung],
      qexists_tac `e` >>
      fs[pong_not_pung],
      `HD ls = EL 0 ls` by simp[] >>
      `d <> 0` by metis_tac[] >>
      `0 < d` by decide_tac >>
      assume_tac every_range_span_min >>
      last_x_assum (qspecl_then [`\p. is_pung (EL p ls)`, `0`, `d`] strip_assume_tac) >>
      rfs[] >>
      qexists_tac `m` >>
      simp[]
    ]
  ]
QED

(* Theorem: let ls = path n k in ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
            ?h. e <= h /\ h < k /\ (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls) *)
(* Proof:
   Note LAST ls = EL k ls                      by path_last_alt
   Let d = SUC e, then e < d
   Then e < k ==> SUC e = d <= k               by arithmetic
   Consider the three cases of (EL d ls).

   Case: is_ping (EL d ls)
      Note ~is_ping (EL k ls) ==> d <> k       by given
        so d < k
       ==> ?m. d <= m /\ m < k /\
               (!j. d <= j /\ j <= m ==> is_ping (EL j ls)) /\
               ~is_ping (EL (SUC m) ls)        by every_range_span_max
      Pick h = m, and !j. d <= j <=> e < j.
       and d <= h, or SUC e <= h, or e < h, satisfying e <= h.
       and h = m < k.

   Case: is_pong (EL d ls)
      Pick h = e
        so ~is_ping (EL d ls)                  by pong_not_ping
       and the range e < p <= e is empty.

   Case: is_pung (EL d ls)
      Pick h = e
        so ~is_ping (EL d ls)                  by pung_not_ping
       and the range e < p <= e is empty.
*)
Theorem pong_seed_ping_after:
  !n k e. let ls = path n k in ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
          ?h. e <= h /\ h < k /\ (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls)
Proof
  rw_tac std_ss[] >>
  `~is_ping (EL k ls)` by fs[path_last_alt, Abbr`ls`] >>
  `SUC e <= k` by decide_tac >>
  qabbrev_tac `d = SUC e` >>
  `is_ping (EL d ls) \/ is_pong (EL d ls) \/ is_pung (EL d ls)` by metis_tac[triple_cases_alt] >| [
    `d <> k` by metis_tac[] >>
    `d < k` by decide_tac >>
    assume_tac every_range_span_max >>
    last_x_assum (qspecl_then [`\p. is_ping (EL p ls)`, `d`, `k`] strip_assume_tac) >>
    rfs[] >>
    `!j. d <= j <=> e < j` by simp[Abbr`d`] >>
    qexists_tac `m` >>
    simp[Abbr`d`],
    qexists_tac `e` >>
    simp[pong_not_ping],
    qexists_tac `e` >>
    simp[pung_not_ping]
  ]
QED

(* Theorem: let ls = path n k in ~is_pung (HD ls) /\ ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
            ?j h. j <= e /\ e <= h /\ h < k /\
                  (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls) /\
                  (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls) *)
(* Proof: by pong_seed_pung_before, pong_seed_ping_after. *)
Theorem pong_seed_pung_ping:
  !n k e. let ls = path n k in ~is_pung (HD ls) /\ ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
           ?j h. j <= e /\ e <= h /\ h < k /\
                 (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls) /\
                 (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls)
Proof
  metis_tac[pong_seed_pung_before, pong_seed_ping_after]
QED

(* Finally, this justifies the definition of blocks. *)

(*
The path starts with ~is_pung, end at flip fix a ~is_ping.
Path starts with is_pong, so also starts with ~is_ping and ends at ~is_ping.

patterns of blocks:
pong -> ping -> (block)
pung -> pong -> ping -> (block)
pung -> pong -> (block)

where (block) is pong or pung, i.e. ~is_ping.

n = 61 has 2 blocks:
block (path 61 6) 0 = (0,0,4)
      (1,1,15) -> (1,15,1) -> (3,13,1) -> (5,9,1) -> (7,3,1)
      is_pong     is_ping     is_ping     is_ping    is_pung
block (path 61 6) 1 = (4,5,6)
      (7,3,1) -> (1,3,5) -> (5,3,3)
      is_pung    is_pong    is_pong

n = 41 has 3 blocks:
block (path 41 6) 0 = (0,0,3)
      (1,1,10) -> (1,10,1) -> (3,8,1) -> (5,4,1)
      is_pong     is_ping     is_ping    is_pong
block (path 41 6) 1 = (3,3,4)
      (5,4,1) -> (3,2,4)
      is_pong    is_pong
block (path 41 6) 2 = (4,4,6)
      (3,2,4) -> (1,5,2) -> (5,2,2)
      is_pong     is_ping   is_pung

Thus:
* every block starts with pong or pung, a ~is_ping (EL j ls)
* every block ends with pong or pung, again ~is_ping (EL h ls), overlapping the start of next.
* prove that: from ~is_ping (EL j ls), either it is pong or pung. Use skip_pung to locate the next pong.
* prove that: from a pong, use skip_ping to locate the next ~is_ping, as end of block.
*)

(* Theorem: 0 < z /\ is_pung (x,y,z) ==>
            let t = pung (x,y,z) in flip t <> t *)
(* Proof:
   By contradiction, assume flip t = t.
   Note ~(x < y - z) /\ ~(x < 2 * y)           by is_pung_def
     or y - z <= x /\ 2 * y <= x               by NOT_LESS, [1]
    and t = pung (x,y,z)
          = (x - 2 * y,y,x + z - y)            by pung_def
   Then y = x + z - y                          by flip_fix
     or 2 * y = x + z
    ==> x + z <= x                             by [1]
   which is impossible when 0 < z.
*)
Theorem pung_next_not_flip_fix:
  !x y z. 0 < z /\ is_pung (x,y,z) ==>
          let t = pung (x,y,z) in flip t <> t
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  fs[is_pung_def, pung_def, Abbr`t`] >>
  fs[flip_fix]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls ==> ~is_pung (EL (k-1) ls) *)
(* Proof:
   Let t = EL (k-1) ls.
   By contradiction, assume is_pung t.

   If k = 0,
      Then t = EL 0 ls             by integer arithmetic
       and is_pong t               by path_head_is_pong
        so ~is_pung t              by pong_not_pung
      leading to contradiction.

   If 0 < k, SUC (k-1) = k         by arithmetic
   Note LAST ls
      = EL k ls                    by path_last_alt
      = (flip o zagier) t          by path_element_suc
      = pung t                     by flip_zagier_pung, is_pung t

   Now t = (x,y,z)                 by triple_parts
   and t IN (mills n)              by path_element_in_mills, tik n
    so 0 < z                       by mills_with_arms, ~square n
   ==> flip t <> t                 by pung_next_not_flip_fix
   This contradicts flip t = t.
*)
Theorem path_last_flip_fix_not_by_pung:
  !n k e. let ls = path n k in tik n /\ ~square n /\
          flip (LAST ls) = LAST ls ==> ~is_pung (EL (k-1) ls)
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  qabbrev_tac `t = EL (k - 1) ls` >>
  `k = 0 \/ 0 < k` by decide_tac >| [
    `k - 1 = 0` by decide_tac >>
    metis_tac[path_head_is_pong, pong_not_pung],
    `SUC (k - 1) = k /\ k - 1 < k` by decide_tac >>
    `LAST ls = pung t` by metis_tac[path_last_alt, path_element_suc, flip_zagier_pung] >>
    `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
    `t IN mills n` by simp[path_element_in_mills, Abbr`ls`,Abbr`t`] >>
    `0 < z` by metis_tac[mills_with_arms] >>
    metis_tac[pung_next_not_flip_fix]
  ]
QED

(* Idea: a block starts with non-ping, use skip_pung to locate next pong. *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ j < k /\ ~is_ping (EL j ls) /\
            flip (LAST ls) = LAST ls ==>
            let e = skip_pung ls j
             in j <= e /\ e < k /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ is_pong (EL e ls) *)
(* Proof:
   Let ls = path n k.
   If is_pong (EL j ls),
      Then ~is_pung (EL j ls),
      Thus skip_pung ls j = j = e < k          by skip_pung_thm
       and is_pong (EL j ls) is true           by assumption
       and !p. j <= p /\ p < j ==> is_pung (EL p ls)  is empty range

   If is_pung (EL j ls),
      Note 0 < k                               by j < k
       and LAST ls = EL k ls                   by path_last_alt
      Let d = k - 1, then SUC d = k            by 0 < k
      Note ~is_pung (EL d ls)                  by path_last_flip_fix_not_by_pung
       and j < k ==> j <= k - 1 = d            by arithmetic
       but j <> d, so j < d                    by ~is_pung (EL d ls)
      Thus ?m. j <= m /\ m < d /\
               (!p. j <= p /\ p <= m ==> is_pung (EL p ls)) /\
               ~is_pung (EL (SUC m) ls)        by every_range_span_max
        or SUC m = skip_pung ls j = e          by skip_pung_thm
       and SUC m <= d < k                      by d = k - 1
       and j < SUC m, satisfy j <= SUC m       by j <= m
       and !p. j <= p /\ p < SUC m ==> is_pung (EL p ls)

      Note is_pung (EL m ls)                   by above, j <= m /\ m <= m
       and   EL (SUC m) ls
           = (flip o zagier) (EL m ls)         by path_element_suc, m < d < k
           = pung (EL m ls)                    by flip_zagier_pung
        so ~is_ping (EL (SUC m) ls)            by pung_next_not_ping_alt
       ==> is_pong (EL (SUC m) ls)             by triple_cases_alt
*)
Theorem path_skip_pung_to_pong:
  !n k j. let ls = path n k in tik n /\ ~square n /\ j < k /\ ~is_ping (EL j ls) /\
          flip (LAST ls) = LAST ls ==>
          let e = skip_pung ls j
           in j <= e /\ e < k /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ is_pong (EL e ls)
Proof
  simp[] >>
  ntac 4 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `e = skip_pung ls j` >>
  `is_pong (EL j ls) \/ is_pung (EL j ls)` by metis_tac[triple_cases_alt] >| [
    `~is_pung (EL j ls)` by simp[pong_not_pung] >>
    `e = j` by simp[skip_pung_thm, Abbr`e`] >>
    simp[],
    `SUC (k - 1) = k /\ j <= k - 1 /\ k - 1 < k` by decide_tac >>
    qabbrev_tac `d = k - 1` >>
    `~is_pung (EL d ls)` by metis_tac[path_last_flip_fix_not_by_pung] >>
    `j <> d` by metis_tac[] >>
    `j < d` by decide_tac >>
    assume_tac every_range_span_max >>
    last_x_assum (qspecl_then [`\p. is_pung (EL p ls)`,`j`,`d`] strip_assume_tac) >>
    rfs[] >>
    `SUC m = e` by fs[skip_pung_thm, Abbr`e`] >>
    `is_pung (EL m ls)` by fs[] >>
    `EL (SUC m) ls = pung (EL m ls)` by fs[path_element_suc, flip_zagier_pung, Abbr`ls`] >>
    `~is_ping (EL (SUC m) ls)` by fs[pung_next_not_ping_alt] >>
    `j <= e /\ e < k /\ !p. p <= m <=> p < SUC m` by decide_tac >>
    metis_tac[triple_cases_alt]
  ]
QED

(* Idea: a block ends with non-ping, from pong use skip_pung to locate end, the next start. *)

(* Theorem: let ls = path n k in j < k /\ is_pong (EL j ls) /\ flip (LAST ls) = LAST ls ==>
            let e = skip_ping ls (j + 1) in j < e /\ e <= k /\ (!p. j < p /\ p < e ==> is_ping (EL p ls)) /\ ~is_ping (EL e ls) *)
(* Proof:
   Let ls = path n k.
   Let d = j + 1, so d <= k                   by j < k

   If is_ping (EL d ls)
      Note LAST ls = EL k ls                   by path_last_alt
       and ~is_ping (EL k ls)                  by path_last_not_ping
       and d <> k, so d < k                    by ~is_ping (EL k ls)
       ==> ?m. d <= m /\ m < k /\
               (!p. d <= p /\ p <= m ==> is_ping (EL p ls)) /\
               ~is_ping (EL (SUC m) ls)        by every_range_span_max
        or SUC m = skip_ping ls d = e          by skip_ping_thm
      with ~is_ping (EL (SUC m) ls)            by above
       and SUC m <= k                          by m < k
       and d < SUC m or j + 1 < SUC m, satisfy j < SUC m
       and !p. j+1 <= p /\ p <= m ==> is_ping (EL p ls)
        or !p. j < p /\ p < SUC m ==> is_ping (EL p ls)

   Otherwise, ~is_ping (EL d ls)
      Thus skip_ping ls d = d                  by skip_ping_thm
       and ~is_ping (EL d ls) is true          by assumption
       and d <= k, j + 1 = d, or j < d
       and !p. j < p /\ p < j+1 ==> is_ping (EL p ls) is empty range
*)
Theorem path_skip_ping_after_pong:
  !n k j. let ls = path n k in j < k /\ is_pong (EL j ls) /\
          flip (LAST ls) = LAST ls ==>
          let e = skip_ping ls (j + 1)
           in j < e /\ e <= k /\ (!p. j < p /\ p < e ==> is_ping (EL p ls)) /\ ~is_ping (EL e ls)
Proof
  simp[] >>
  ntac 4 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `j + 1 <= k` by decide_tac >>
  qabbrev_tac `d = j + 1` >>
  qabbrev_tac `e = skip_ping ls d` >>
  `is_ping (EL d ls) \/ ~is_ping (EL d ls)` by decide_tac >| [
    `~is_ping (EL k ls) /\ d <> k` by metis_tac[path_last_alt, path_last_not_ping] >>
    `d < k` by decide_tac >>
    assume_tac every_range_span_max >>
    last_x_assum (qspecl_then [`\p. is_ping (EL p ls)`,`d`,`k`] strip_assume_tac) >>
    rfs[] >>
    `SUC m = e` by fs[skip_ping_thm, Abbr`e`] >>
    `j < SUC m` by fs[Abbr`d`] >>
    `SUC m <= k /\ (!p. j < p <=> j + 1 <= p) /\ (!p. p <= m <=> p < SUC m)` by decide_tac >>
    metis_tac[],
    `e = d` by simp[skip_ping_thm, Abbr`e`] >>
    fs[Abbr`d`]
  ]
QED

(* Theorem: let ls = path n k in
            tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            u < k /\ ~is_ping (EL u ls) /\ v = skip_pung ls u /\ w = skip_ping ls (v + 1) ==>
            u <= v /\ v < k /\ is_pong (EL v ls) /\ (!p. u <= p /\ p < v ==> is_pung (EL p ls)) /\
            v < w /\ w <= k /\ ~is_ping (EL w ls) /\ (!p. v < p /\ p < w ==> is_ping (EL p ls)) *)
(* Proof:
   Let ls = path n k,
        v = skip_pung ls u,
        w = skip_ping ls (v + 1).
    Now ~is_ping (EL u ls)                         by given
    ==> is_pong (EL v ls) /\ u <= v /\ v < k /\
        !p. u <= p /\ p < v ==> is_pung (EL p ls)  by path_skip_pung_to_pong, u < k
    and is_pong (EL v ls)
    ==> ~is_ping (EL w ls) /\ v < w /\ w <= k /\
        !p. v < p /\ p < w ==> is_ping (EL p ls)   by path_skip_ping_after_pong, v < k
*)
Theorem path_skip_from_pung_to_ping:
  !n k u v w. let ls = path n k in
              tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              u < k /\ ~is_ping (EL u ls) /\ v = skip_pung ls u /\ w = skip_ping ls (v + 1) ==>
              u <= v /\ v < k /\ is_pong (EL v ls) /\ (!p. u <= p /\ p < v ==> is_pung (EL p ls)) /\
              v < w /\ w <= k /\ ~is_ping (EL w ls) /\ (!p. v < p /\ p < w ==> is_ping (EL p ls))
Proof
  simp[] >>
  ntac 4 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `v = skip_pung ls u` >>
  qabbrev_tac `w = skip_ping ls (v + 1)` >>
  assume_tac path_skip_pung_to_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `u`] strip_assume_tac) >>
  rfs[] >>
  assume_tac path_skip_ping_after_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `v`] strip_assume_tac) >>
  rfs[]
QED

(* Theorem: let ls = path n k in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_ping (EL p ls)) ==>
            EL (j + h) ls = FUNPOW ping h (EL j ls) *)
(* Proof:
   Let ls = path n k.
   By induction on h.
   Base: j + 0 <= k ==> (!p. j <= p /\ p < j + 0 ==> is_ping (EL p ls)) ==>
         EL (j + 0) ls = FUNPOW ping 0 (EL j ls)
           EL (j + 0) ls
         = EL j ls                             by arithmetic
         = FUNPOW ping 0 (EL j ls)             by FUNPOW_0
   Step: j + h <= k ==> (!p. j <= p /\ p < j + h ==> is_ping (EL p ls)) ==>
         EL (j + h) ls = FUNPOW ping h (EL j ls) ==>
         j + SUC h <= k ==> (!p. j <= p /\ p < j + SUC h ==> is_ping (EL p ls)) ==>
         EL (j + SUC h) ls = FUNPOW ping (SUC h) (EL j ls)
        Note j + SUC h <= k ==> j + h < k, satisying j + h <= k.
            !p. j <= p /\ p < j + SUC h ==> is_ping (EL p ls)
        <=> !p. j <= p /\ (p < j + h \/ p = j + h) ==> is_ping (EL p ls)                 by arithmetic
        <=> is_ping (EL (j + h) ls) /\ (!p. j <= p /\ p < j + h ==> is_ping (EL p ls))   by DISJ_IMP_THM

           EL (j + SUC h) ls
         = EL SUC (j + h) ls                   by arithmetic
         = (flip o zagier) (EL (j + h) ls)     by path_element_suc
         = ping (EL (j + h) ls)                by flip_zagier_ping
         = ping (FUNPOW ping h (EL j ls))      by induction hypothesis
         = FUNPOW ping (SUC h) (EL j ls)       by FUNPOW_SUC
*)
Theorem path_element_skip_ping:
  !n k j h. let ls = path n k in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_ping (EL p ls)) ==>
            EL (j + h) ls = FUNPOW ping h (EL j ls)
Proof
  rw_tac std_ss[] >>
  Induct_on `h` >-
  simp[] >>
  rpt strip_tac >>
  `!p. p < j + SUC h <=> p < j + h \/ p = j + h` by decide_tac >>
  `is_ping (EL (j + h) ls)` by simp[] >>
  `j + SUC h = SUC (j + h) /\ j + h < k /\ j + h <= k` by decide_tac >>
  `EL (j + SUC h) ls = (flip o zagier) (EL (j + h) ls)` by metis_tac[path_element_suc] >>
  `_ = ping (EL (j + h) ls)` by simp[flip_zagier_ping] >>
  `_ = ping (FUNPOW ping h (EL j ls))` by fs[] >>
  simp[FUNPOW_SUC]
QED

(* Theorem: let ls = path n k in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_pung (EL p ls)) ==>
            EL (j + h) ls = FUNPOW pung h (EL j ls) *)
(* Proof:
   Let ls = path n k.
   By induction on h.
   Base: j + 0 <= k ==> (!p. j <= p /\ p < j + 0 ==> is_pung (EL p ls)) ==>
         EL (j + 0) ls = FUNPOW pung 0 (EL j ls)
           EL (j + 0) ls
         = EL j ls                             by arithmetic
         = FUNPOW pung 0 (EL j ls)             by FUNPOW_0
   Step: j + h <= k ==> (!p. j <= p /\ p < j + h ==> is_pung (EL p ls)) ==>
         EL (j + h) ls = FUNPOW pung h (EL j ls) ==>
         j + SUC h <= k ==> (!p. j <= p /\ p < j + SUC h ==> is_pung (EL p ls)) ==>
         EL (j + SUC h) ls = FUNPOW pung (SUC h) (EL j ls)
        Note j + SUC h <= k ==> j + h < k, satisying j + h <= k.
            !p. j <= p /\ p < j + SUC h ==> is_pung (EL p ls)
        <=> !p. j <= p /\ (p < j + h \/ p = j + h) ==> is_pung (EL p ls)                 by arithmetic
        <=> is_pung (EL (j + h) ls) /\ (!p. j <= p /\ p < j + h ==> is_pung (EL p ls))   by DISJ_IMP_THM

           EL (j + SUC h) ls
         = EL SUC (j + h) ls                   by arithmetic
         = (flip o zagier) (EL (j + h) ls)     by path_element_suc
         = pung (EL (j + h) ls)                by flip_zagier_pung
         = pung (FUNPOW pung h (EL j ls))      by induction hypothesis
         = FUNPOW pung (SUC h) (EL j ls)       by FUNPOW_SUC
*)
Theorem path_element_skip_pung:
  !n k j h. let ls = path n k in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_pung (EL p ls)) ==>
            EL (j + h) ls = FUNPOW pung h (EL j ls)
Proof
  rw_tac std_ss[] >>
  Induct_on `h` >-
  simp[] >>
  rpt strip_tac >>
  `!p. p < j + SUC h <=> p < j + h \/ p = j + h` by decide_tac >>
  `is_pung (EL (j + h) ls)` by simp[] >>
  `j + SUC h = SUC (j + h) /\ j + h < k /\ j + h <= k` by decide_tac >>
  `EL (j + SUC h) ls = (flip o zagier) (EL (j + h) ls)` by metis_tac[path_element_suc] >>
  `_ = pung (EL (j + h) ls)` by simp[flip_zagier_pung] >>
  `_ = pung (FUNPOW pung h (EL j ls))` by fs[] >>
  simp[FUNPOW_SUC]
QED

(* ------------------------------------------------------------------------- *)
(* Hopping                                                                   *)
(* ------------------------------------------------------------------------- *)

(* Define the hop map *)
(* phi_m . [x,y,z] = [[-1,2m,0],[m,-m*m,1],[0,1,0]] . [x,y,z] = [2 * m * y - x, m * x - m * m * y + z, y] *)
Definition hop_def:
   hop m (x, y, z) = (2 * m * y - x, z + m * x - m * m * y, y)
End
(* Hop Matrix: H = [[-1,2*m,0],[m,-m*m,1],[0,1,0]]
This has a diagonalisation: H = P D P^(-1), where D is diagonal, but entries involves the golden ratio.
*)

(* Define the hip map *)
(* flip . A^m . [x,y,z] = [[1,2m,0],[-m,-m*m,1],[0,1,0]] . [x,y,z] = [2 * m * y + x, - m * x - m * m * y + z, y] *)
Definition hip_def:
   hip m (x, y, z) = (2 * m * y + x, z - m * x - m * m * y, y)
End
(* Hip Matrix: K = [[1,2*m,0],[-m,-m*m,1],[0,1,0]]
This has a diagonalisation: H = P D P^(-1), where D is diagonal, but entries involves sqrt(3) and imaginary unit.
*)

(* Extract theorems *)
Theorem hop_0 = hop_def |> SPEC ``0`` |> SIMP_RULE arith_ss [];
(* val hop_0 = |- !x y z. hop 0 (x,y,z) = (0,z,y): thm *)

Theorem hop_1 = hop_def |> SPEC ``1`` |> SIMP_RULE arith_ss [];
(* val hop_1 = |- !x y z. hop 1 (x,y,z) = (2 * y - x,x + z - y,y): thm *)

(* Theorem: hop 1 = pong *)
(* Proof: by hop_def, pong_def. *)
Theorem hop_1_eqn:
  hop 1 = pong
Proof
  rw[FUN_EQ_THM] >>
  `?a b c. x = (a, b, c)` by simp[triple_parts] >>
  fs[hop_def, pong_def]
QED

(* An important example of hop:
For n = 61,
pung (7,3,1) = (1,3,5)
pong (1,3,5) = (5,3,3)


> EVAL ``windmill (7,3,1)``; = 61
> EVAL ``pung (7,3,1)``; = (1,3,5)
> EVAL ``pong (1,3,5)``; = (5,3,3)
> EVAL ``hop 2 (7,3,1)``; = (5,3,3) , that is skipping pung.
> EVAL ``hop 1 (7,3,1)``; = (0,5,3) , when hop to pung.
> hop(2,[7,3,1]) gives [ 5, 3, 3 ]
> hop(1,[7,3,1]) gives [ -1, 5, 3 ]
This is actually H (1,3,5), or A (1,3,5):
[[-1,0,0],[0,0,1],[0,1,0]] . [1,3,5] = [-1,5,3]
with mind([-1,5,3]) = 5 = mind([1,3,5])

For hop(m,[9,2,7]) to be a valid windmill: x/2y < m <= (x + √n)/2y

For n = 89,
(9,2,1) -> pung -----> pung -----> pong -----> ping -> (7,5,2)
               (5,2,8)    (1,2,11)    (3,10,2)
> hop(0,[9,2,1]) = [ -9, 1, 2 ] mind = -5
> hop(1,[9,2,1]) = [ -5, 8, 2 ] mind = -1
> hop(2,[9,2,1]) = [ -1, 11, 2 ] mind = 3
> hop(3,[9,2,1]) = [ 3, 10, 2 ] mind = 7 <-- good m = 3
> hop(4,[9,2,1]) = [ 7, 5, 2 ]  mind = 7 <-- max  m = 4
x/2y = 9/4 = 2 integer, (x + √n)/y = (9 + 9)/4 = 4, so 2 < m <= 4.

For n = 137,
(9,2,7) -> pung -----> pung -----> pong -----> ping -----> ping -> (11,2,2)
               (5,2,14)    (1,2,17)    (3,16,2)    (7,11,2)
> hop(0,[9,2,7]) = [ -9, 7, 2 ] mind = -5
> hop(1,[9,2,7]) = [ -5, 14, 2 ] mind = -1
> hop(2,[9,2,7]) = [ -1, 17, 2 ] mind = 3
> hop(3,[9,2,7]) = [ 3, 16, 2 ]  mind = 7  <-- good m = 3
> hop(4,[9,2,7]) = [ 7, 11, 2 ]  mind = 11 <-- yes  m = 4
> hop(5,[9,2,7]) = [ 11, 2, 2 ]  mind = 11 <-- max  m = 5
x/2y = 9/4 = 2 integer, (x + √n)/y = (9 + 11)/4 = 5, so 2 < m <= 5
*)

(* Idea: condition for hop m (x,y,z) to have a mind. *)

(* Theorem: 0 < y /\ x DIV (2 * y) < m ==> 0 < 2 * m * y - x *)
(* Proof:
       x DIV (2 * y) < m
   ==> x < m * (2 * y)             by DIV_LT_X, 0 < y
   ==> 0 < 2 * m * y - x           by inequality
*)
Theorem hop_mind:
  !m x y. 0 < y /\ x DIV (2 * y) < m ==> 0 < 2 * m * y - x
Proof
  rpt strip_tac >>
  `0 < 2 * y` by decide_tac >>
  fs[DIV_LT_X]
QED

(* Idea: condition for hop m (x,y,z) to have arms. *)

(* Theorem: ~square n /\ n = windmill (x,y,z) /\ 0 < y /\
            x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y)
            ==> 0 < z + m * x - m * m * y *)
(* Proof:
   Note x DIV (2 * y) < m
    ==> 0 < 2 * m * y - x                      by hop_mind
   Also m <= (x + SQRT n) DIV (2 * y)
    ==> 2 * m * y <= x + SQRT n                by X_LE_DIV, 0 < y
    ==> 2 * m * y - x <= SQRT n                by x < m * (2 * y)
    ==> (2 * m * y - x) ** 2 <= (SQRT n) ** 2  by EXP_EXP_LE_MONO_IMP
    ==> (2 * m * y - x) ** 2 < n               by SQ_SQRT_LT_alt, ~square n
   Hence,
        (2 * m * y - x) ** 2 < x ** 2 + 4 * y * z                                  by windmill_def
    ==> (2 * m * y - x) ** 2 + 4 * m * x * y < x ** 2 + 4 * y * z + 4 * m * x * y  by inequality
    ==> (2 * m * y) ** 2 + x ** 2 < x ** 2 + 4 * y * z + 4 * m * x * y             by binomial_sub_sum
    ==>          (2 * m * y) ** 2 < 4 * y * z + 4 * m * x * y                      by inequality
    ==>             m * m * y * y < z * y + m * x * y                              by EXP_2
    ==>           (m * m * y) * y < (z + m * x) * y                                by LEFT_ADD_DISTRIB
    ==>                 m * m * y < z + m * x                                      by LT_MULT_LCANCEL, 0 < y
    ==>                         0 < z + m * x - m * m * y                          by inequality
*)
Theorem hop_arm:
  !m n x y z. ~square n /\ n = windmill (x,y,z) /\ 0 < y /\
               x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y)
          ==> 0 < z + m * x - m * m * y
Proof
  rw[windmill_def] >>
  qabbrev_tac `n = 4 * (y * z) + x ** 2` >>
  imp_res_tac hop_mind >>
  `(2 * m * y - x) ** 2 < n` by
  (`2 * m * y <= x + SQRT n` by rfs[X_LE_DIV] >>
  qabbrev_tac `yy = 2 * m * y` >>
  `yy - x <= SQRT n` by decide_tac >>
  `(yy - x) ** 2 <= (SQRT n) ** 2` by simp[EXP_EXP_LE_MONO_IMP] >>
  `(SQRT n) ** 2 < n` by simp[SQ_SQRT_LT_alt] >>
  decide_tac) >>
  `m ** 2 * y < z + m * x` by
    (qabbrev_tac `yy = 2 * m * y` >>
  `(yy - x) ** 2 + 2 * yy * x  < n + 2 * yy * x` by decide_tac >>
  `2 * yy * x = 4 * m * x * y` by simp[Abbr`yy`] >>
  `yy ** 2 + x ** 2 < 4 * (y * z) + x ** 2 + 4 * m * x * y` by simp[GSYM binomial_sub_sum, Abbr`n`] >>
  `yy ** 2 = 4 * y * (m * m * y)` by simp[Abbr`yy`, EXP_BASE_MULT] >>
  `4 * y * (m * m * y) < 4 * y * (z + m * x)` by decide_tac >>
  fs[]) >>
  decide_tac
QED

(* Idea: hop goes from windmill to windmill. *)

(* Theorem: n = windmill (x,y,z) /\
            0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y
            ==> n = windmill (hop m (x,y,z)) *)
(* Proof:
   Note hop m (x,y,z) = (2 * m * y - x,z + m * x - m * m * y,y)    by hop_def
    and 0 < 2 * m * y - x ==> 0 < y                                by MULT_0, SUB_0
    and 0 < z + m * x - m * m * y
    ==> m * m * y < z + m * x                                      by inequality
    ==> 4 * y * (m * m * y) < 4 * y * (z + m * x)                  by 0 < y
    ==> 4 * y * (m * m * y) < 4 * y * z + 4 * y * m * x            by LEFT_ADD_DISTRIB
    ==>   4 * y * m * m * y < 4 * y * z + 4 * m * x * y            by arithmetic, [1]
        windmill (hop m (x,y,z))
      = (2 * m * y - x) ** 2 + 4 * y * (z + m * x - m * m * y)     by windmill_def
      = (2 * m * y - x) ** 2 + (4 * y * (z + m * x) - 4 * y * m * m * y)        by LEFT_SUB_DISTRIB
      = (2 * m * y - x) ** 2 + (4 * y * z + 4 * m * y * x - 4 * y * m * m * y)  by LEFT_ADD_DISTRIB
      = (2 * m * y - x) ** 2 + 4 * y * z + 4 * m * y * x - 4 * y * m * m * y    by [1]
      = (2 * m * y - x) ** 2 + 2 * (2 * m * y) * x + 4 * y * z - 4 * y * m * m * y
      = (2 * m * y) ** 2 + x ** 2 + 4 * y * z - 4 * y * m * m * y  by binomial_sub_sum
      = 4 * m ** 2 * y ** 2 + x ** 2 + 4 * y * z - 4 * m ** 2 * y ** 2          by EXP_2, EXP_BASE_MULT
      = x ** 2 + 4 * y * z                                         by arithmetic
      = n                                                          by windmill_def
*)
Theorem hop_windmill:
  !m n x y z. n = windmill (x,y,z) /\
               0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y
           ==> n = windmill (hop m (x,y,z))
Proof
  rw[hop_def] >>
  `0 < y` by metis_tac[NOT_ZERO, MULT_0, SUB_0, DECIDE``~(0 < 0)``] >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  qabbrev_tac `yy = 2 * (m * y)` >>
  `4 * (m ** 2 * y ** 2) < 4 * (y * z) + 4 * (m * x * y)` by
  (`m ** 2 * y < z + m * x` by decide_tac >>
  `4 * y * (m ** 2 * y) < 4 * y * (z + m * x)` by fs[] >>
  `4 * y * (m ** 2 * y) = 4 * (m ** 2 * y ** 2)` by simp[] >>
  decide_tac) >>
  simp[windmill_def, LEFT_SUB_DISTRIB, LEFT_ADD_DISTRIB] >>
  `4 * (m * x * y) = 2 * yy * x` by simp[Abbr`yy`] >>
  `4 * (m * x * y) + (yy - x) ** 2 = yy ** 2 + x ** 2` by fs[GSYM binomial_sub_sum] >>
  `yy ** 2 = 4 * (m ** 2 * y ** 2)` by simp[EXP_BASE_MULT, Abbr`yy`] >>
  fs[windmill_def, Abbr`n`]
QED

(* Idea: windmill defines the range of hopping. *)

(* Theorem: n = windmill (x,y,z) /\
            0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y
            ==> x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y) *)
(* Proof:
   Note 0 < 2 * m * y - x
    ==> 0 < y                      by MULT_0, SUB_0
    and x < m * (2 * y)            by inequality
    ==> x DIV (2 * y) < m          by DIV_LT_X, 0 < y

   Also 0 < z + m * x - m * m * y
    ==> m * m * y < z + m * x                              by inequality
    ==> 4 * y * (m * m * y) < 4 * y * (z + m * x)          by LT_MULT_LCANCEL, 0 < y
    ==> 4 * y * (m * m * y) < 4 * y * z + 4 * y * m * x    by LEFT_ADD_DISTRIB
    ==>    (2 * m * y) ** 2 < 4 * y * z + 4 * y * m * x    by EXP_BASE_MULT
    ==> x ** 2 + (2 * m * y) ** 2 < x ** 2 + 4 * y * z + 4 * y * m * x   by inequality
    ==> (2 * m * y) ** 2 + x ** 2 < n + 4 * y * m * x      by windmill_def
    ==> (2 * m * y - x) ** 2 + 2 * (2 * m * y) * x < n + 4 * y * m * x   by binomial_sub_sum, x < 2 * m * y
    ==> (2 * m * y - x) ** 2 < n                           by inequality
    ==>       2 * m * y - x <= SQRT n                      by SQRT_LT, SQRT_OF_SQ
    ==>       2 * m * y <= x + SQRT n                      by inequality
    ==>               m <= (x + SQRT n) DIV (2 * y)        by X_LE_DIV, 0 < y
*)
Theorem hop_range:
  !m n x y z. n = windmill (x,y,z) /\
               0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y
           ==> x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y)
Proof
  ntac 6 strip_tac >>
  `0 < y` by metis_tac[NOT_ZERO, MULT_0, SUB_0, DECIDE``~(0 < 0)``] >>
  strip_tac >-
  fs[DIV_LT_X] >>
  `m * m * y < z + m * x` by decide_tac >>
  `4 * y * (m * m * y) < 4 * y * (z + m * x)` by fs[] >>
  `4 * y * (m * m * y) = (2 * m * y) ** 2` by simp[EXP_BASE_MULT] >>
  `4 * y * (z + m * x) = 4 * y * z + 4 * m * x * y` by simp[] >>
  `n = x ** 2 + 4 * y * z` by simp[windmill_def] >>
  `(2 * m * y) ** 2 + x ** 2 < n + 4 * m * x * y` by decide_tac >>
  `(2 * m * y) ** 2 + x ** 2 = (2 * m * y - x) ** 2 + 4 * m * x * y` by simp[GSYM binomial_sub_sum] >>
  `(2 * m * y - x) ** 2 < n` by decide_tac >>
  `(2 * m * y - x) <= SQRT n` by metis_tac[SQRT_LT, SQRT_OF_SQ] >>
  `m * (2 * y) <= x + SQRT n` by decide_tac >>
  simp[X_LE_DIV]
QED

(* Idea: the range of hopping is defined by being a windmill. *)

(* Theorem: ~square n /\ n = windmill (x,y,z) ==>
              (0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y
           <=> 0 < y /\ x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y)) *)
(* Proof:
   If part: x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y)
      Note 0 < 2 * m * y - x ==> 0 < y         by MULT_0, SUB_0
       and x DIV (2 * y) < m                   by hop_range
       and m <= (x + SQRT n) DIV (2 * y)       by hop_range

   Only-if part: 0 < y /\ 0 < 2 * (m * y) - x /\ 0 < z + m * x - m ** 2 * y
      Note 0 < y ==> 0 < 2 * (m * y) - x       by hop_mind
       and 0 < y ==> 0 < z + m * x - m * m * y by hop_arm
*)
Theorem hop_range_iff:
  !m n x y z. ~square n /\ n = windmill (x,y,z) ==>
              (0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y
           <=> 0 < y /\ x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y))
Proof
  ntac 6 strip_tac >>
  rewrite_tac [EQ_IMP_THM] >>
  ntac 2 strip_tac >| [
    `0 < y` by metis_tac[NOT_ZERO, MULT_0, SUB_0, DECIDE``~(0 < 0)``] >>
    metis_tac[hop_range],
    metis_tac[hop_mind, hop_arm]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Matrices of ping, pong, pung and hop.                                     *)
(* ------------------------------------------------------------------------- *)

(*
ping_def |- !x y z. ping (x,y,z) = (x + 2 * z,y - z - x,z)
pong_def |- !x y z. pong (x,y,z) = (2 * y - x,x + z - y,y)
pung_def |- !x y z. pung (x,y,z) = (x - 2 * y,y,x + z - y)

The matrices:
ping  D = [[1,0,2],[-1,1,-1],[0,0,1]]  D^2 = [[1,0,4],[-2,1,-4],[0,-0,1]]  same form D^m = [[1,0,2m],[-m,1,-m*m],[0,0,1]]
pong  E = [[-1,2,0],[1,-1,1],[0,1,0]]  E^2 = [[3,-4,2],[-2,4,-1],[1,-1,1]]
pung  F = [[1,-2,0],[0,1,0],[1,-1,1]]  F^2 = [[1,-4,0],[0,1,0],[2,-4,1]]   same form E^m = [[1,-2m,0],[0,1,0],[m,-m*m,1]]

Only D and F has diagonalisation in Jordan form (check in WolframAlpha), E has diagonal form, but not in Jordan form.
*)

(* Theorem: FUNPOW ping m (x,y,z) = (x + 2 * m * z, y - m * x - m * m * z, z) *)
(* Proof:
   By induction on m.
   Base: FUNPOW ping 0 (x,y,z) = (x + 2 * 0 * z,y - 0 * x - 0 * 0 * z,z)
        FUNPOW ping 0 (x,y,z)
      = (x,y,z)                                by FUNPOW_0
      = (x + 0, y - 0, z)                      by ADD_0, SUB_0
      = (x + 2 * 0 * z,y - 0 * x - 0 * 0 * z,z)
   Step: FUNPOW ping m (x,y,z) = (x + 2 * m * z,y - m * x - m * m * z,z) ==>
         FUNPOW ping (SUC m) (x,y,z) = (x + 2 * SUC m * z,y - SUC m * x - SUC m * SUC m * z,z)
        FUNPOW ping (SUC m) (x,y,z)
      = ping (FUNPOW ping m (x,y,z))                     by FUNPOW_SUC
      = ping (x + 2 * m * z,y - m * x - m * m * z,z)     by induction hypothesis
      = (x + 2 * m * z + 2 * z,
         y - m * x - m * m * z - z - (x + 2 * m * z),z)  by ping_def
      = (x + 2 * (m + 1) * z),                           by LEFT_ADD_DISTRIB
         y - (x * (m + 1) + z * (m + 1) ** 2), z)        by SUM_SQUARED
      = (x + 2 * SUC m * z,y - SUC m * x - SUC m * SUC m * z,z)    by ADD1, EXP_2
*)
Theorem ping_funpow:
  !m x y z. FUNPOW ping m (x,y,z) = (x + 2 * m * z, y - m * x - m * m * z, z)
Proof
  rpt strip_tac >>
  Induct_on `m` >-
  simp[] >>
  simp[FUNPOW_SUC, ping_def, ADD1] >>
  `(m + 1) ** 2 = m ** 2 + 2 * m + 1` by simp[SUM_SQUARED] >>
  fs[]
QED

(* Theorem: is_ping (x,y,z) ==> windmill (x,y,z) = windmill (ping (x,y,z)) *)
(* Proof:
     windmill (ping (x,y,z))
   = windmill ((flip o zagier) (x,y,z))        by flip_zagier_ping
   = windmill (x,y,z)                          by flip_zagier_windmill
*)
Theorem ping_windmill:
  !x y z. is_ping (x,y,z) ==> windmill (x,y,z) = windmill (ping (x,y,z))
Proof
  metis_tac[flip_zagier_ping, flip_zagier_windmill]
QED

(* Theorem: is_ping t ==> windmill t = windmill (ping t) *)
(* Proof: by ping_windmill, triple_parts. *)
Theorem ping_windmill_alt:
  !t. is_ping t ==> windmill t = windmill (ping t)
Proof
  metis_tac[ping_windmill, triple_parts]
QED

(* Theorem: is_pong (x,y,z) ==> windmill (x,y,z) = windmill (pong (x,y,z)) *)
(* Proof:
     windmill (pong (x,y,z))
   = windmill ((flip o zagier) (x,y,z))        by flip_zagier_pong
   = windmill (x,y,z)                          by flip_zagier_windmill
*)
Theorem pong_windmill:
  !x y z. is_pong (x,y,z) ==> windmill (x,y,z) = windmill (pong (x,y,z))
Proof
  metis_tac[flip_zagier_pong, flip_zagier_windmill]
QED

(* Theorem: is_pong t ==> windmill t = windmill (pong t) *)
(* Proof: by pong_windmill, triple_parts. *)
Theorem pong_windmill_alt:
  !t. is_pong t ==> windmill t = windmill (pong t)
Proof
  metis_tac[pong_windmill, triple_parts]
QED

(* Theorem: is_pung (x,y,z) ==> windmill (x,y,z) = windmill (pung (x,y,z)) *)
(* Proof:
     windmill (pung (x,y,z))
   = windmill ((flip o zagier) (x,y,z))        by flip_zagier_pung
   = windmill (x,y,z)                          by flip_zagier_windmill
*)
Theorem pung_windmill:
  !x y z. is_pung (x,y,z) ==> windmill (x,y,z) = windmill (pung (x,y,z))
Proof
  metis_tac[flip_zagier_pung, flip_zagier_windmill]
QED

(* Theorem: is_pung t ==> windmill t = windmill (pung t) *)
(* Proof: by pung_windmill, triple_parts. *)
Theorem pung_windmill_alt:
  !t. is_pung t ==> windmill t = windmill (pung t)
Proof
  metis_tac[pung_windmill, triple_parts]
QED

(* Theorem: (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
            windmill (x,y,z) = windmill (FUNPOW ping m (x,y,z)) *)
(* Proof:
   By induction on m.
   Base: (!j. j < 0 ==> is_ping (FUNPOW ping j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW ping 0 (x,y,z))
      Note FUNPOW ping 0 (x,y,z) = (x,y,z)     by FUNPOW_0
      Thus trivially true.
   Step: (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW ping m (x,y,z)) ==>
         (!j. j < SUC m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW ping (SUC m) (x,y,z))
      Note !j. j < SUC m ==> is_ping (FUNPOW ping j (x,y,z))
       <=> !j. (j < m \/ j = m) ==> is_ping (FUNPOW ping j (x,y,z))    by arithmetic
       <=> is_ping (FUNPOW ping m (x,y,z)) /\
           !j. j < m ==> is_ping (FUNPOW ping j (x,y,z))               by DISJ_IMP_THM
        windmill (FUNPOW ping (SUC m) (x,y,z))
      = windmill (ping (FUNPOW ping m (x,y,z)))                        by FUNPOW_SUC
      = windmill (FUNPOW ping m (x,y,z))                               by ping_windmill_alt
      = windmill (x,y,z)                                               by induction hypothesis
*)
Theorem ping_funpow_windmill:
  !m x y z. (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
             windmill (x,y,z) = windmill (FUNPOW ping m (x,y,z))
Proof
  rpt strip_tac >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_ping (FUNPOW ping m (x,y,z)) /\ !j. j < m ==> is_ping (FUNPOW ping j (x,y,z))` by metis_tac[] >>
  fs[FUNPOW_SUC, ping_windmill_alt]
QED

(* Theorem: (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> windmill t = windmill (FUNPOW ping m t) *)
(* Proof: by ping_funpow_windmill, triple_parts. *)
Theorem ping_funpow_windmill_alt:
  !m t. (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> windmill t = windmill (FUNPOW ping m t)
Proof
  metis_tac[ping_funpow_windmill, triple_parts]
QED

(* Theorem: (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            windmill (x,y,z) = windmill (FUNPOW pung m (x,y,z)) *)
(* Proof:
   By induction on m.
   Base: (!j. j < 0 ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW pung 0 (x,y,z))
      Note FUNPOW pung 0 (x,y,z) = (x,y,z)     by FUNPOW_0
      Thus trivially true.
   Step: (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW pung m (x,y,z)) ==>
         (!j. j < SUC m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW pung (SUC m) (x,y,z))
      Note !j. j < SUC m ==> is_pung (FUNPOW pung j (x,y,z))
       <=> !j. (j < m \/ j = m) ==> is_pung (FUNPOW pung j (x,y,z))    by arithmetic
       <=> is_pung (FUNPOW pung m (x,y,z)) /\
           !j. j < m ==> is_pung (FUNPOW pung j (x,y,z))               by DISJ_IMP_THM
        windmill (FUNPOW pung (SUC m) (x,y,z))
      = windmill (pung (FUNPOW pung m (x,y,z)))                        by FUNPOW_SUC
      = windmill (FUNPOW pung m (x,y,z))                               by pung_windmill_alt
      = windmill (x,y,z)                                               by induction hypothesis
*)
Theorem pung_funpow_windmill:
  !m x y z. (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
             windmill (x,y,z) = windmill (FUNPOW pung m (x,y,z))
Proof
  rpt strip_tac >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_pung (FUNPOW pung m (x,y,z)) /\ !j. j < m ==> is_pung (FUNPOW pung j (x,y,z))` by metis_tac[] >>
  fs[FUNPOW_SUC, pung_windmill_alt]
QED

(* Theorem: (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> windmill t = windmill (FUNPOW pung m t) *)
(* Proof: by pung_funpow_windmill, triple_parts. *)
Theorem pung_funpow_windmill_alt:
  !m t. (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> windmill t = windmill (FUNPOW pung m t)
Proof
  metis_tac[pung_funpow_windmill, triple_parts]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\
            (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            FUNPOW pung m (x,y,z) = (x - 2 * m * y, y, z + m * x - m * m * y) *)
(* Proof:
   By induction on m.
   Base: (!j. j < 0 ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         FUNPOW pung 0 (x,y,z) = (x - 2 * 0 * y,y,z + 0 * x - 0 * 0 * y)
        FUNPOW pung 0 (x,y,z)
      = (x,y,z)                                by FUNPOW_0
      = (x - 0, y, z + 0)                      by ADD_0, SUB_0
      = (x - 2 * 0 * y,y,z + 0 * x - 0 * 0 * y)
   Step: (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         FUNPOW pung m (x,y,z) = (x - 2 * m * y,y,z + m * x - m * m * y) ==>
         (!j. j < SUC m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         FUNPOW pung (SUC m) (x,y,z) = (x - 2 * SUC m * y,y,z + SUC m * x - SUC m * SUC m * y)
      Note !j. j < SUC m ==> is_pung (FUNPOW pung j (x,y,z))
       <=> !j. (j < m \/ j = m) ==> is_pung (FUNPOW pung j (x,y,z))    by arithmetic
       <=> is_pung (FUNPOW pung m (x,y,z)) /\
           !j. j < m ==> is_pung (FUNPOW pung j (x,y,z))               by DISJ_IMP_THM
      Thus n = windmill (FUNPOW pung m (x,y,z))          by pung_funpow_windmill
      giving the conditions:
           0 < x - 2 * m * y /\
           0 < z + m * x - m ** 2 * y                    by mills_element, mills_with_mind_and_arms, EXP_2

        FUNPOW pung (SUC m) (x,y,z)
      = pung (FUNPOW pung m (x,y,z))                     by FUNPOW_SUC
      = pung (x - 2 * m * y,y,z + m * x - m * m * y)     by induction hypothesis
      = (x - 2 * m * y - 2 * y, y,
         x - 2 * m * y + (z + m * x - m * m * y) - y)    by pung_def
      = (x - 2 * (m + 1) * y), y,                        by LEFT_ADD_DISTRIB, by conditions
         z + (m + 1) * x - (m + 1) ** 2 * y)             by SUM_SQUARED
      = (x - 2 * SUC m * y,y,z + SUC m * x - SUC m * SUC m * y)    by ADD1, EXP_2
*)
Theorem pung_funpow:
  !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\
              (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
              FUNPOW pung m (x,y,z) = (x - 2 * m * y, y, z + m * x - m * m * y)
Proof
  rpt strip_tac >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_pung (FUNPOW pung m (x,y,z)) /\ !j. j < m ==> is_pung (FUNPOW pung j (x,y,z))` by metis_tac[] >>
  `n = windmill (FUNPOW pung m (x,y,z))` by simp[GSYM pung_funpow_windmill] >>
  `0 < x - 2 * m * y /\ 0 < z + m * x - m ** 2 * y` by metis_tac[mills_element, mills_with_mind_and_arms, EXP_2, ONE_NOT_0] >>
  simp[FUNPOW_SUC, pung_def, ADD1] >>
  `x - 2 * (m * y) + (z + m * x - m ** 2 * y) - y = (z + m * x - m ** 2 * y) + (x - 2 * (m * y)) - y` by decide_tac >>
  `_ = z + m * x + x - (m ** 2 * y + 2 * m * y + y)` by fs[] >>
  `(m + 1) ** 2 = m ** 2 + 2 * m + 1` by simp[SUM_SQUARED] >>
  rfs[]
QED


(* Theorem: pong (x,y,z) = hop 1 (x,y,z) *)
(* Proof:
      pong (x,y,z)
    = (2 * y - x,x + z - y,y)                  by pong_def
    = (2 * 1 * y - x, z + 1 * x - 1 * 1 * y, y)
    = hop 1 (x,y,z)                            by hop_def
*)
Theorem pong_by_hop:
  !x y z. pong (x,y,z) = hop 1 (x,y,z)
Proof
  simp[pong_def, hop_def]
QED

(* Theorem: pong t = hop 1 t *)
(* Proof: by pong_by_hop, triple_parts. *)
Theorem pong_by_hop_alt:
  !t. pong t = hop 1 t
Proof
  metis_tac[pong_by_hop, triple_parts]
QED

(* Idea: the pung-to-ping through pong is equivalent to hop. *)

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\ is_pong (FUNPOW pung q (x,y,z)) /\
            (!j. j < q ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            (FUNPOW ping p o pong o FUNPOW pung q) (x,y,z) = hop (p + q + 1) (x,y,z) *)
(* Proof:
   Let a = FUNPOW pung q (x,y,z),
       b = pong a.
   The goal is to show: FUNPOW ping p b = hop (p + (q + 1)) (x,y,z)

   Note a = FUNPOW pung q (x,y,z)
          = (x - 2 * q * y, y,
             z + q * x - q ** 2 * y)           by pung_funpow, EXP_2
    and n = windmill a                         by pung_funpow_windmill
    ==> 0 < x - 2 * q * y /\
        0 < z + q * x - q ** 2 * y             by mills_element_alt, mills_with_mind_and_arms
     or 2 * q * y < x /\
        q ** 2 * y < z + q * x                 by inequality, [1]

   Note b = pong a
          = (2 * y - (x - 2 * q * y),
             x - 2 * q * y + (z + q * x - q ** 2 * y) - y,
             y)                                by pong_def
          = (2 * y + 2 * q * y - x,
             (z + q * x - q ** 2 * y) + (x - 2 * q * y) - y,
             y)                                by SUB_SUB, [1]
          = (2 * (q + 1) * y - x,
             z + (q + 1) * x - (q ** 2 + 2 * q + 1) * y,
             y)                                by RIGHT_ADD_DISTRIB
          = (2 * (q + 1) * y - x,
             z + (q + 1) * x - (q + 1) ** 2 * y,
             y)                                by SUM_SQUARED
    and n = windmill b                         by pong_windmill_alt
    ==> 0 < 2 * (q + 1) * y - x /\
        0 < z + (q + 1) * x - (q + 1) ** 2 * y by mills_element_alt, mills_with_mind_and_arms
     or x < 2 * (q + 1) * y /\
        (q + 1) ** 2 * y < z + (q + 1) * x     by inequality, [2]

     FUNPOW ping p o pong o FUNPOW pung q (x,y,z)
   = FUNPOW ping p (pong (FUNPOW pung q (x,y,z)))      by composition
   = FUNPOW ping p b                                   by above

   If p = 0,
     FUNPOW ping 0 b
   = b                                                 by FUNPOW_0
   = (2 * (q + 1) * y - x, z + (q + 1) * x - (q + 1) ** 2 * y, y)
   = hop (0 + q + 1) (x,y,z)                           by hop_def

   If 0 < p,
   Then p * x  < 2 * p * (q + 1) * y                   by LT_MULT_LCANCEL, [3]

     FUNPOW ping p b
   = (2 * (q + 1) * y - x + 2 * p * y,
      z + (q + 1) * x - (q + 1) ** 2 * y - p * (2 * (q + 1) * y - x) - p ** 2 * y,
      y)                                               by ping_funpow, EXP_2
   = (2 * (q + 1) * y + 2 * p * y - x,
      z + (q + 1) * x + p x - ((q + 1) ** 2 + 2 * p * (q + 1) + p ** 2) * y,
      y)                                               by arithmetic, [2][3]
   = (2 * (p + q + 1) * y - x,
      z + (p + q + 1) * x - ((q + 1) ** 2 + 2 * (q + 1) * p + p ** 2) * y,
      y)                                               by RIGHT_ADD_DISTRIB
   = (2 * (p + q + 1) * y - x,
      z + (p + q + 1) * x - (p + q + 1) ** 2 * y,
      y)                                               by SUM_SQUARED
   = hop (p + q + 1) (x,y,z)                           by hop_def
*)
Theorem pung_to_ping_by_hop:
  !n x y z p q. tik n /\ ~square n /\ n = windmill (x,y,z) /\ is_pong (FUNPOW pung q (x,y,z)) /\
                (!j. j < q ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                (FUNPOW ping p o pong o FUNPOW pung q) (x,y,z) = hop (p + q + 1) (x,y,z)
Proof
  rw[] >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  qabbrev_tac `a = FUNPOW pung q (x,y,z)` >>
  qabbrev_tac `b = pong a` >>
  `a = (x - 2 * q * y, y, z + q * x - q ** 2 * y)` by fs[pung_funpow, Abbr`a`] >>
  `n = windmill a` by simp[GSYM pung_funpow_windmill, Abbr`a`] >>
  `b = (2 * (q + 1) * y - x, z + (q + 1) * x - (q + 1) ** 2 * y, y)` by
  (rfs[pong_def, Abbr`b`] >>
  `0 < x - 2 * (q * y) /\ 0 < z + q * x - q ** 2 * y` by metis_tac[mills_element_alt, mills_with_mind_and_arms, ONE_NOT_0] >>
  simp[SUM_SQUARED]) >>
  `n = windmill b` by simp[pong_windmill_alt, Abbr`b`] >>
  `FUNPOW ping p b = (2 * (p + (q + 1)) * y - x, z + (p + (q + 1)) * x - (p + (q + 1)) ** 2 * y, y)` by
    (`p = 0 \/ 0 < p` by decide_tac >-
  simp[] >>
  qabbrev_tac `qq = q + 1` >>
  fs[ping_funpow] >>
  `0 < 2 * (qq * y) - x /\ 0 < z + qq * x - qq ** 2 * y` by metis_tac[mills_element_alt, mills_with_mind_and_arms, ONE_NOT_0] >>
  rfs[] >>
  `0 < p * (2 * (qq * y) - x)` by simp[] >>
  `0 < p * 2 * (qq * y) - p * x` by decide_tac >>
  `z + qq * x - (p * (2 * (qq * y) - x) + (p ** 2 * y + qq ** 2 * y)) = z + qq * x - (p ** 2 * y + qq ** 2 * y + p * (2 * (qq * y) - x))` by simp[] >>
  `_ = z + qq * x + p * x - (p ** 2 * y + qq ** 2 * y + 2 * p * qq * y)` by rfs[] >>
  `_ = z + (p + qq) * x - (p ** 2 + qq ** 2 + 2 * p * qq) * y` by rfs[] >>
  rfs[SUM_SQUARED]
  ) >>
  simp[hop_def]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ is_pong (FUNPOW pung q t) /\
            (!j. j < q ==> is_pung (FUNPOW pung j t)) ==>
            (FUNPOW ping p o pong o FUNPOW pung q) t = hop (p + q + 1) t *)
(* Proof: by pung_to_ping_by_hop, triple_parts. *)
Theorem pung_to_ping_by_hop_alt:
  !n t p q. tik n /\ ~square n /\ n = windmill t /\ is_pong (FUNPOW pung q t) /\
                (!j. j < q ==> is_pung (FUNPOW pung j t)) ==>
                (FUNPOW ping p o pong o FUNPOW pung q) t = hop (p + q + 1) t
Proof
  metis_tac[pung_to_ping_by_hop, triple_parts]
QED

(* ------------------------------------------------------------------------- *)
(* Pong Indices along a Path.                                                *)
(* ------------------------------------------------------------------------- *)

(*

The (ls = path n k) has a finite number of pongs.
Need to count them:
The first 0-th pong (at HD ls) gives (block ls 0)
The next 1-th pong gives (block ls 1), etc.
The last j-th pong gives (block ls j), with third at (LAST ls) such that flip (LAST ls) = LAST ls.

*)

(* Extract the indices of pong elements in a list. *)
Definition pong_indices_def:
    pong_indices ls = FILTER (\j. is_pong (EL j ls)) [0 ..< LENGTH ls]
End

(*
> EVAL ``pong_indices (path 41 6)``;
val it = |- pong_indices (path 41 6) = [0; 3; 4]: thm
> EVAL ``pong_indices (path 61 6)``;
val it = |- pong_indices (path 61 6) = [0; 5; 6]: thm
> EVAL ``pong_indices (path 97 14)``;
val it = |- pong_indices (path 97 14) = [0; 6; 8; 9; 11]: thm
*)


(* Theorem: MEM j (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls) *)
(* Proof:
   Let P = \j. is_pong (EL j ls).
       MEM j (pong_indices ls)
   <=> MEM j (FILTER P [0 ..< LENGTH ls])      by pong_indices_def
   <=> P j /\ MEM j [0 ..< LENGTH ls]          by MEM_FILTER
   <=> P j /\ j < LENGTH ls                    by MEM_listRangeLHI
   <=> j < LENGTH ls /\ is_pong (EL j ls)      by notation
*)
Theorem pong_indices_mem:
  !ls j. MEM j (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls)
Proof
  simp[pong_indices_def, MEM_FILTER] >>
  metis_tac[]
QED

(* Theorem: j IN set (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls) *)
(* Proof:
   Let P = \j. is_pong (EL j ls),
       k = LENGTH ls.
       j IN set (pong_indices ls)
   <=> j IN set (FILTER P [0 ..< k])           by pong_indices_def
   <=> j IN {j | P j} INTER set [0 ..< k]      by LIST_TO_SET_FILTER
   <=> j IN {j | P j} INTER (count k)          by listRangeLHI_SET
   <=> P j /\ j IN (count k)                   by IN_INTER
   <=> j < LENGTH ls /\ is_pong (EL j ls)      by IN_COUNT
*)
Theorem pong_indices_element:
  !ls j. j IN set (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls)
Proof
  simp[pong_indices_def, LIST_TO_SET_FILTER] >>
  metis_tac[]
QED

(* Theorem: pong_indices [] = [] *)
(* Proof:
   Let P = \j. is_pong (EL j []).
     pong_indices []
   = FILTER P [0 ..< LENGTH []]    by pong_indices_def
   = FILTER P [0 ..< 0]            by LENGTH
   = FILTER P []                   by listRangeLHI_EQ
   = []                            by FILTER
*)
Theorem pong_indices_nil:
  pong_indices [] = []
Proof
  simp[pong_indices_def]
QED

(* Theorem: set (pong_indices []) = {} *)
(* Proof:
     set (pong_indices [])
   = set []                        by pong_indices_nil
   = {}                            by LIST_TO_SET
*)
Theorem pong_indices_empty:
  set (pong_indices []) = {}
Proof
  simp[pong_indices_nil]
QED

(* Theorem: set (pong_indices ls) SUBSET count (LENGTH ls) *)
(* Proof:
       j IN set (pong_indices ls)
   <=> j < LENGTH ls /\ is_pong (EL j ls)      by pong_indices_element
   ==> j < LENGTH ls                           by implication
   <=> j IN count (LENGTH ls)                  by IN_COUNT
   Hence true                                  by SUBSET_DEF
*)
Theorem pong_indices_subset:
  !ls. set (pong_indices ls) SUBSET count (LENGTH ls)
Proof
  simp[pong_indices_element, SUBSET_DEF]
QED

(* Theorem: FINITE (set (pong_indices ls)) *)
(* Proof:
   Let s = count (LENGTH ls).
   Note set (pong_indices ls) SUBSET s         by pong_indices_subset
    and FINITE s                               by FINITE_COUNT
    ==> FINITE (pong_indices ls)               by SUBSET_FINITE
*)
Theorem pong_indices_finite:
  !ls. FINITE (set (pong_indices ls))
Proof
  metis_tac[pong_indices_subset, SUBSET_FINITE, FINITE_COUNT]
QED

(* Theorem: LENGTH (pong_indices ls) <= LENGTH ls *)
(* Proof:
   Let P = \j. is_pong (EL j ls).
      LENGTH (pong_indices ls)
    = LENGTH (FILTER P [0 ..< LENGTH ls])      by pong_indices_def
   <= LENGTH [0 ..< LENGTH ls]                 by LENGTH_FILTER_LEQ
    = LENGTH ls                                by listRangeLHI_LEN
*)
Theorem pong_indices_length:
  !ls. LENGTH (pong_indices ls) <= LENGTH ls
Proof
  rw[pong_indices_def] >>
  qabbrev_tac `P = \j. is_pong (EL j ls)` >>
  qabbrev_tac `k = LENGTH ls` >>
  `LENGTH (FILTER P [0 ..< k]) <= LENGTH [0 ..< k]` by simp[LENGTH_FILTER_LEQ] >>
  fs[listRangeLHI_LEN]
QED

(* Theorem: ALL_DISTINCT (pong_indices ls) *)
(* Proof:
   Let P = \j. is_pong (EL j ls).
       ALL_DISTINCT (pong_indices ls)
   <=> ALL_DISTINCT (FILTER P [0 ..< LENGTH ls])   by pong_indices_def
   <=> ALL_DISTINCT [0 ..< LENGTH ls]              by FILTER_ALL_DISTINCT
   <=> T                                           by listRangeLHI_ALL_DISTINCT
*)
Theorem pong_indices_all_distinct:
  !ls. ALL_DISTINCT (pong_indices ls)
Proof
  simp[pong_indices_def, FILTER_ALL_DISTINCT, listRangeLHI_ALL_DISTINCT]
QED

(*
n = 61 is an example with a pong at the end.

> EVAL ``pong_indices (FRONT (path 41 6))``;
val it = |- pong_indices (FRONT (path 41 6)) = [0; 3; 4]: thm
> EVAL ``pong_indices (FRONT (path 61 6))``;
val it = |- pong_indices (FRONT (path 61 6)) = [0; 5]: thm
> EVAL ``pong_indices (FRONT (path 97 14))``;
val it = |- pong_indices (FRONT (path 97 14)) = [0; 6; 8; 9; 11]: thm

Note:
> EVAL ``(path 5 0)``; = [(1,1,1)]
> EVAL ``block (path 5 0) 0``; non-terminating!
Rather than altering the definition to, say:
block ls 0 = (0,0, if 1 < LENGTH ls then skip_ping ls 1 else 0)
In view of:
> EVAL ``FRONT (path 5 0)``; = []
better to say: for n = 5, there is no block!

Note:
In order to prove that, for (u,v,w) = block ls j, u < k /\ ~is_ping (EL u ls),
the index j must be restricted, as (block ls j) is meaningful
only for j < LENGTH (pong_indices (FRONT (path n k))).
*)

(* Theorem: 0 < k ==> ?t. pong_indices (FRONT (path n k)) = 0 :: t *)
(* Proof:
   Let ls = path n k.
   Note ls <> []                   by path_not_nil
    and LENGTH ls = k + 1          by path_length
     so LENGTH (FRONT ls) = k      by LENGTH_FRONT

   Let fs = FRONT ls.
   Let P = \j. is_pong (EL j fs).
   Then P 0
      = is_pong (EL 0 fs)
      = is_pong (HD fs)            by EL
      = is_pong (1,1,n DIV 4)      by path_front_head
      = T                          by is_pong_x_x_z

      pong_indices (FRONT ls)
    = pong_indices fs              by notation
    = FILTER P [0 ..< k]           by pong_indices_def
    = FILTER P (0::[1 ..< k])      by listRangeLHI_CONS
    = 0 :: FILTER P [1 ..< k]      by FILTER, P 0

    Take t = FILTER P [1 ..< k],
      so pong_indices (FRONT ls) = 0 :: t
*)
Theorem pong_indices_path_cons:
  !n k. 0 < k ==> ?t. pong_indices (FRONT (path n k)) = 0 :: t
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `ls <> []` by simp[path_not_nil, Abbr`ls`] >>
  `LENGTH (FRONT ls) = k` by fs[LENGTH_FRONT, path_length, Abbr`ls`] >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `P = \j. is_pong (EL j fs)` >>
  `P 0` by simp[path_front_head, is_pong_x_x_z, Abbr`P`, Abbr`fs`, Abbr`ls`] >>
  `[0 ..< k] = 0::[1 ..< k]` by simp[listRangeLHI_CONS] >>
  `pong_indices fs = 0 :: FILTER P [1 ..< k]` by fs[pong_indices_def, Abbr`fs`] >>
  metis_tac[]
QED

(* Theorem: 0 < k ==> pong_indices (FRONT (path n k)) <> [] *)
(* Proof:
   Let ls = path n k.
   Note ?t. pong_indices (FRONT ls) = 0 :: t   by pong_indices_path_cons
     so pong_indices (FRONT ls) <> []          by NOT_NIL_CONS
*)
Theorem pong_indices_path_not_nil:
  !n k. 0 < k ==> pong_indices (FRONT (path n k)) <> []
Proof
  metis_tac[pong_indices_path_cons, NOT_NIL_CONS]
QED

(* Theorem: 0 < k ==> HD (pong_indices (FRONT (path n k))) = 0 *)
(* Proof:
   Let ls = path n k.
   Note ?t. pong_indices (FRONT ls) = 0 :: t   by pong_indices_path_cons
     so HD pong_indices (FRONT ls) = 0         by HD
*)
Theorem pong_indices_path_head:
  !n k. 0 < k ==> HD (pong_indices (FRONT (path n k))) = 0
Proof
  metis_tac[pong_indices_path_cons, HD]
QED

(* Theorem: pong_indices (FRONT (path n k)) = [] <=> k = 0 *)
(* Proof:
   If part: pong_indices (FRONT (path n k)) = [] ==> k = 0
      By contradiction, suppose k <> 0.
      Then 0 < k                                   by NOT_ZERO
        so pong_indices (FRONT (path n k)) <> []   by pong_indices_path_not_nil
      which is a contradiction.
   Only-if part: k = 0 ==> pong_indices (FRONT (path n k)) = []
        pong_indices (FRONT (path n 0))
      = pong_indices (FRONT [(1,1,n DIV 4)])       by path_0
      = pong_indices []                            by FRONT_CONS
      = []                                         by pong_indices_nil
*)
Theorem pong_indices_path_eq_nil:
  !n k. pong_indices (FRONT (path n k)) = [] <=> k = 0
Proof
  rw[EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `0 < k` by decide_tac >>
    fs[pong_indices_path_not_nil],
    simp[path_0, pong_indices_nil]
  ]
QED

(* Theorem: let ls = path n k in MEM j (pong_indices (FRONT ls)) <=> j < k /\ is_pong (EL j ls) *)
(* Proof:
   Let ls = path n k,
       fs = FRONT ls,
        P = \j. is_pong (EL j fs).
   Note ls <> []                       by path_not_nil
    and LENGTH fs = k                  by path_front_length
   Note !p. p < k ==>
            EL p fs = EL p ls          by FRONT_EL, ls <> [], [1]

       MEM j (pong_indices fs)
   <=> j < k /\ is_pong (EL j fs)      by pong_indices_mem
   <=> j < k /\ is_pong (EL j ls)      by [1], j < k
*)
Theorem pong_indices_path_element:
  !n k j. let ls = path n k in MEM j (pong_indices (FRONT ls)) <=> j < k /\ is_pong (EL j ls)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `P = \j. is_pong (EL j fs)` >>
  `ls <> [] /\ LENGTH fs = k` by simp[path_not_nil, path_front_length, Abbr`ls`, Abbr`fs`] >>
  metis_tac[pong_indices_mem, FRONT_EL]
QED

(* Theorem: let px = pong_indices ls in 0 < j /\ j < LENGTH px ==> EL (j-1) px < EL j px *)
(* Proof:
   Let px = pong_indices ls,
        k = LENGTH ls,
        P = \j. is_pong (EL j ls).
   Then px = FILTER P [0 ..< k]                by pong_indices_def

    Now MONO_INC [0 ..< k]                     by listRangeLHI_MONO_INC
    ==> MONO_INC px                            by FILTER_MONO_INC
    ==> EL (j - 1) px <= EL j px               by notation

    But ALL_DISTINCT [0 ..< k]                 by listRangeLHI_ALL_DISTINCT
    ==> ALL_DISTINCT px                        by FILTER_ALL_DISTINCT
     so EL (j - 1) px <> EL j px               by EL_ALL_DISTINCT_EL_EQ

   Thus EL (j-1) px < EL j px
*)
Theorem pong_indices_monotonic:
  !ls j. let px = pong_indices ls in 0 < j /\ j < LENGTH px ==> EL (j-1) px < EL j px
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `k = LENGTH ls` >>
  qabbrev_tac `P = \j. is_pong (EL j ls)` >>
  `px = FILTER P [0 ..< k]` by simp[pong_indices_def, Abbr`px`, Abbr`k`, Abbr`P`] >>
  `MONO_INC px` by fs[FILTER_MONO_INC, listRangeLHI_MONO_INC, Abbr`px`] >>
  `ALL_DISTINCT px` by fs[FILTER_ALL_DISTINCT, listRangeLHI_ALL_DISTINCT, Abbr`px`] >>
  `EL (j - 1) px <= EL j px` by fs[] >>
  `EL (j - 1) px <> EL j px` by fs[EL_ALL_DISTINCT_EL_EQ] >>
  decide_tac
QED

(* Theorem: let ls = path n k; px = pong_indices (FRONT ls) in
            0 < j /\ j < LENGTH px ==>
            !p. EL (j-1) px < p /\ p < EL j px ==> ~is_pong (EL p ls) *)
(* Proof:
   Let ls = path n k,
       fs = FRONT ls,
        P = \j. is_pong (EL j fs),
       px = pong_indices ls,
        h = LENGTH px,
        a = EL (j-1) px,
        b = EL j px.
   The goal is to show: 0 < j /\ j < h /\ a < p /\ p < b ==> ~is_pong (EL p ls)

   Note ls <> []                               by path_not_nil
    and LENGTH fs = k                          by path_front_length
    and px = FILTER P [0 ..< k]                by pong_indices_def
    and h <= k                                 by pong_indices_length

   Note MEM a px                               by MEM_EL, j - 1 < h
    and MEM b px                               by MEM_EL, j < h
    ==> a < k /\ is_pong (EL a fs)             by pong_indices_element
    and b < k /\ is_pong (EL b fs)             by pong_indices_element

   Note a < b                                  by a < p /\ p < b
        [0 ..< k]
      = [0 ..< b] ++ b::[b+1 ..< k]                    by listRangeLHI_SPLIT, b < k
      = [0 ..< a] ++ a::[a+1 ..< b] ++ b::[b+1 ..< k]  by listRangeLHI_SPLIT, a < b
    Now ALL_DISTINCT [0 ..< k]                 by listRangeLHI_ALL_DISTINCT
    ==> ALL_DISTINCT px                        by FILTER_ALL_DISTINCT
   Thus findi a px = j - 1                     by findi_EL
    and findi b px = j                         by findi_EL
     or findi b px = 1 + findi a px            by above

   Note P a /\ P b                             by application
   Thus FILTER P [a + 1 ..< b] = []            by FILTER_EL_NEXT_IDX
    <=> EVERY (\x. ~P x) [a + 1 ..< b]         by FILTER_EQ_NIL
    <=> !p. a + 1 <= p /\ p < b ==> ~P p       by listRangeLHI_EVERY
     so ~is_pong (EL p fs)                     by a < p, p < b
    ==> ~is_pong (EL p ls)                     by FRONT_EL, p < k
*)
Theorem pong_indices_path_pong_gap:
  !n k j. let ls = path n k; px = pong_indices (FRONT ls) in
          0 < j /\ j < LENGTH px ==>
          !p. EL (j-1) px < p /\ p < EL j px ==> ~is_pong (EL p ls)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `P = \j. is_pong (EL j fs)` >>
  qabbrev_tac `h = LENGTH px` >>
  qabbrev_tac `a = EL (j-1) px` >>
  qabbrev_tac `b = EL j px` >>
  `ls <> [] /\ LENGTH fs = k` by metis_tac[path_not_nil, path_front_length] >>
  `px = FILTER P [0 ..< k]` by fs[pong_indices_def, Abbr`px`, Abbr`P`] >>
  `h <= k` by metis_tac[pong_indices_length] >>
  `j - 1 < h` by decide_tac >>
  `MEM a px /\ MEM b px` by metis_tac[MEM_EL] >>
  `a < k /\ is_pong (EL a fs)` by fs[pong_indices_element, Abbr`px`] >>
  `b < k /\ is_pong (EL b fs)` by fs[pong_indices_element, Abbr`px`] >>
  `a < b` by decide_tac >>
  `[0 ..< k] = [0 ..< b] ++ b::[b+1 ..< k]` by fs[listRangeLHI_SPLIT] >>
  `_ = [0 ..< a] ++ a::[a+1 ..< b] ++ b::[b+1 ..< k]` by fs[listRangeLHI_SPLIT] >>
  `ALL_DISTINCT [0 ..< k]` by simp[listRangeLHI_ALL_DISTINCT] >>
  `ALL_DISTINCT px` by metis_tac[FILTER_ALL_DISTINCT] >>
  `findi b px = 1 + findi a px` by fs[findi_EL, Abbr`b`, Abbr`a`] >>
  `P a /\ P b` by fs[Abbr`P`] >>
  `FILTER P [a + 1 ..< b] = []` by metis_tac[FILTER_EL_NEXT_IDX] >>
  `EVERY (\x. ~P x) [a + 1 ..< b]` by fs[FILTER_EQ_NIL] >>
  `~is_pong (EL p fs)` by fs[listRangeLHI_EVERY, Abbr`P`] >>
  `p < k` by decide_tac >>
  metis_tac[FRONT_EL]
QED

(* A major key result! *)

(* Idea: after the LAST in pong_indices (FRONT (path n k)), there is no more pong. *)

(* Theorem: let ls = path n k; px = pong_indices (FRONT ls); v = LAST px in 0 < k ==>
            v < k /\ is_pong (EL v ls) /\ !p. v < p /\ p < k ==> ~is_pong (EL p ls) *)
(* Proof:
   Let ls = path n k,
       fs = FRONT ls,
       px = pong_indices fs,
        P = \j. is_pong (EL j fs),
        v = LAST px.
   The goal is to show: is_pong (EL v ls) /\ !p. v < p /\ p < k ==> ~is_pong (EL p ls)

   Note ls <> []                               by path_not_nil
    and LENGTH fs = k                          by path_front_length
    and px <> []                               by pong_indices_path_eq_nil, 0 < k
    and px = FILTER P [0 ..< k]                by pong_indices_def
   also !j. j < k ==> EL j fs = EL j ls        by FRONT_EL, ls <> [], [1]

   Note MEM v px                               by MEM_LAST, list_CASES, px <> []
    ==> v < k /\ is_pong (EL v ls)             by pong_indices_path_element
     so is_pong (EL v fs)                      by [1]
    and P v                                    by application

   Note ALL_DISTINCT [0 ..< k]                 by listRangeLHI_ALL_DISTINCT
    and [0 ..< k]
      = [0 ..< v] ++ v::[v+1 ..< k]            by listRangeLHI_SPLIT, v < k
   Thus FILTER P [v + 1 ..< k] = []            by FILTER_LAST_IFF
    <=> EVERY (\x. ~P x) [v + 1 ..< k]         by FILTER_EQ_NIL
    <=> !n. n < LENGTH [v + 1 ..< k] ==>
            (\x. ~P x) (EL n [v + 1 ..< k])    by EVERY_EL
    But LENGTH [v + 1 ..< k] = k - (v + 1)     by listRangeLHI_LEN
     so n < k - (v + 1) <=> n + (v + 1) < k    by SUB_LEFT_LESS
    ==> EL n [v + 1 ..< k] = n + (v + 1)       by listRangeLHI_EL, n + (v + 1) < k

   Note v < p /\ p < k
    <=> v + 1 <= p /\ p < k
    <=> 0 <= p - (v + 1) /\ p - (v + 1) < k - (v + 1)
    Put n = p - (v + 1),
   then ~is_pong (EL (p - (v + 1) + (v + 1)) fs)
     or ~is_pong (EL p fs)
     or ~is_pong (EL p ls)                     by FRONT_EL, [1]
*)
Theorem pong_indices_path_last:
  !n k. let ls = path n k; px = pong_indices (FRONT ls); v = LAST px in 0 < k ==>
        v < k /\ is_pong (EL v ls) /\ !p. v < p /\ p < k ==> ~is_pong (EL p ls)
Proof
  simp[] >>
  ntac 3 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `px = pong_indices fs` >>
  qabbrev_tac `P = \j. is_pong (EL j fs)` >>
  qabbrev_tac `v = LAST px` >>
  `ls <> [] /\ LENGTH fs = k` by simp[path_not_nil, path_front_length, Abbr`fs`, Abbr`ls`] >>
  `px <> []` by simp[pong_indices_path_eq_nil, Abbr`px`, Abbr`fs`, Abbr`ls`] >>
  `px = FILTER P [0 ..< k]` by fs[pong_indices_def, Abbr`px`] >>
  `MEM v px` by metis_tac[MEM_LAST, list_CASES] >>
  `v < k /\ is_pong (EL v ls)` by metis_tac[pong_indices_path_element] >>
  simp[] >>
  rpt strip_tac >>
  `!j. j < k ==> EL j fs = EL j ls` by metis_tac[FRONT_EL] >>
  `P v` by fs[Abbr`P`] >>
  `ALL_DISTINCT [0 ..< k]` by simp[listRangeLHI_ALL_DISTINCT] >>
  `[0 ..< k] = [0 ..< v] ++ v::[v+1 ..< k]` by simp[listRangeLHI_SPLIT] >>
  `FILTER P [v + 1 ..< k] = []` by metis_tac[FILTER_LAST_IFF] >>
  fs[FILTER_EQ_NIL, EVERY_EL, listRangeLHI_EL, Abbr`P`] >>
  first_x_assum (qspecl_then [`p - (v + 1)`] strip_assume_tac) >>
  rfs[]
QED

(* ------------------------------------------------------------------------- *)
(* Define blocks based on pong indices.                                      *)
(* ------------------------------------------------------------------------- *)

(*
> EVAL ``path 41 6``; = [(1,1,10); (1,10,1); (3,8,1); (5,4,1); (3,2,4); (1,5,2); (5,2,2)]: thm
> EVAL ``pong_indices (FRONT (path 41 6))``; = [0; 3; 4]: thm
> EVAL ``let ls = path 41 6 in MAP (\j. (j, skip_ping ls (j+1))) (pong_indices (FRONT ls))``; = [(0,3); (3,4); (4,6)]
> EVAL ``let ls = [(0,3); (3,4); (4,6)] in
          MAP (\j. (if j = 0 then 0 else SND(EL (j-1) ls), FST (EL j ls), SND (EL j ls))) [0 ..< LENGTH ls]``; = [(0,0,3); (3,3,4); (4,4,6)]
*)

(* Define a list of pairs from is_pong to skip_ping *)
Definition block_pairs_def:
   block_pairs ls = MAP (\j. (j, skip_ping ls (j+1))) (pong_indices (FRONT ls))
End

(* Define a list of triples starting from skip_ping of last pair. *)
Definition blocks_def:
   blocks ls = MAP (\j. (if j = 0 then 0 else SND (EL (j-1) ls), EL j ls)) [0 ..< LENGTH ls]
End

(*
> EVAL ``blocks (block_pairs (path 41 6))``; = [(0,0,3); (3,3,4); (4,4,6)]
> EVAL ``blocks (block_pairs (path 61 6))``; = [(0,0,4); (4,5,6)]
> EVAL ``blocks (block_pairs (path 97 14))``; = [(0,0,5); (5,6,7); (7,8,9); (9,9,10); (10,11,14)]
> EVAL ``blocks (block_pairs (path 5 0))``; = []: thm
*)

(* ------------------------------------------------------------------------- *)
(* Block Pairs Theorems.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: block_pairs [x] = [] *)
(* Proof:
   Let f = \j. (j, skip_ping ls (j+1)).
     block_pairs [x]
   = MAP f (pong_indices (FRONT [x]))          by block_pairs_def
   = MAP f (pong_indices [])                   by FRONT_CONS
   = MAP f []                                  by pong_indices_nil
   = []                                        by MAP
*)
Theorem block_pairs_nil:
  !x. block_pairs [x] = []
Proof
  simp[block_pairs_def, pong_indices_nil]
QED

(* Theorem: LENGTH (block_pairs ls) = LENGTH (pong_indices (FRONT ls)) *)
(* Proof:
   Let f = \j. (j,skip_ping ls (j + 1)).
     LENGTH (block_pairs ls)
   = LENGTH (MAP f (pong_indices (FRONT ls)))  by block_pairs_def
   = LENGTH (pong_indices (FRONT ls))          by LENGTH_MAP
*)
Theorem block_pairs_length:
  !ls. LENGTH (block_pairs ls) = LENGTH (pong_indices (FRONT ls))
Proof
  simp[block_pairs_def]
QED

(* Theorem: MEM (v,w) (block_pairs ls) <=>
            MEM v (pong_indices (FRONT ls)) /\ w = skip_ping ls (v+1) *)
(* Proof:
   Let ps = block_pairs ls,
       px = pong_indices (FRONT ls),
        f = \j. (j,skip_ping ls (j + 1)).
   Then ps =
       MEM (v,w) ps
   <=> MEM (v,w) (MAP f px)                    by block_pairs_def
   <=> ?j. (v,w) = f j /\ MEM j px             by MEM_MAP
   <=> ?j. v = j /\ w = skip_ping ls (j+1) /\ MEM j px
                                               by application, PAIR_EQ
   <=> MEM v px /\ w = skip_ping ls (v+1)      by v = j
*)
Theorem block_pairs_mem:
  !ls v w. MEM (v,w) (block_pairs ls) <=>
           MEM v (pong_indices (FRONT ls)) /\ w = skip_ping ls (v+1)
Proof
  rw[block_pairs_def, MEM_MAP] >>
  decide_tac
QED

(* Theorem: let ps = block_pairs ls in j < LENGTH ps ==>
            ((v, w) = EL j ps <=> v = EL j (pong_indices (FRONT ls)) /\ w = skip_ping ls (v+1)) *)
(* Proof:
   Let ps = block_pairs ls,
       px = pong_indices (FRONT ls),
        f = \j. (j,skip_ping ls (j + 1)).
   Then f v = (v, skip_ping ls (v+1))          by application
    and INJ f (set px) univ(:num # num)        by PAIR_EQ
    and ps = MAP f px                          by block_pairs_def
    and LENGTH px = LENGTH ps                  by LENGTH_MAP

        f v = EL j ps
            = EL j (MAP f px)                  by above
            = f (EL j px)                      by EL_MAP
     <=>  v = EL j px                          by INJ_IMP_11
    and   w = skip_ping ls (v+1)               by PAIR_EQ
*)
Theorem block_pairs_element:
  !ls j v w. let ps = block_pairs ls in j < LENGTH ps ==>
              ((v, w) = EL j ps <=> v = EL j (pong_indices (FRONT ls)) /\ w = skip_ping ls (v+1))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `h = LENGTH ps` >>
  qabbrev_tac `px = pong_indices (FRONT ls)` >>
  qabbrev_tac `f = \j. (j,skip_ping ls (j + 1))` >>
  `f v = (v, skip_ping ls (v+1))` by fs[Abbr`f`] >>
  `INJ f univ(:num) univ(:num # num)` by rw[INJ_DEF, Abbr`f`] >>
  `ps = MAP f px` by simp[block_pairs_def, Abbr`ps`, Abbr`f`, Abbr`px`] >>
  `LENGTH px = h` by simp[Abbr`px`, Abbr`h`] >>
  metis_tac[EL_MAP, INJ_IMP_11, PAIR_EQ]
QED

(* Theorem: block_pairs (path n 0) = [] *)
(* Proof:
     block_pairs (path n 0)
   = block_pairs [(1,1,n DIV 4)]   by path_0
   = []                            by block_pairs_nil
*)
Theorem block_pairs_path_0:
  !n. block_pairs (path n 0) = []
Proof
  simp[path_0, block_pairs_nil]
QED

(* Theorem: 0 < k ==> ?t. block_pairs (path n k) = (0, skip_ping (path n k) 1) :: t *)
(* Proof:
   Let ls = path n k,
        f = \j. (j, skip_ping ls (j+1)).
   Note ?tt. pong_indices (FRONT ls) = 0::tt   by pong_indices_path_cons, 0 < k
     block_pairs ls
   = MAP f (pong_indices (FRONT ls))           by block_pairs_def
   = MAP f (0 :: tt)                           by above
   = f 0 :: MAP f tt                           by MAP
   = (0, skip_ping ls 1) :: MAP f tt           by applying f

   Pick (MAP f tt) as the tail t.
*)
Theorem block_pairs_path_cons:
  !n k. 0 < k ==> ?t. block_pairs (path n k) = (0, skip_ping (path n k) 1) :: t
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `f = \j. (j, skip_ping ls (j+1))` >>
  `?t. pong_indices (FRONT ls) = 0 :: t` by metis_tac[pong_indices_path_cons] >>
  qexists_tac `MAP f t` >>
  simp[block_pairs_def, Abbr`ls`, Abbr`f`]
QED

(* Theorem: 0 < k ==> block_pairs (path n k) <> [] *)
(* Proof:
   Note ?t. block_pairs (path n k)
          = (0, skip_ping (path n k) 1) :: t   by block_pairs_path_cons
   Thus block_pairs (path n k) <> []           by NOT_NIL_CONS
*)
Theorem block_pairs_path_not_nil:
  !n k. 0 < k ==> block_pairs (path n k) <> []
Proof
  metis_tac[block_pairs_path_cons, NOT_NIL_CONS]
QED

(* Theorem: 0 < k ==> HD (block_pairs (path n k)) = (0, skip_ping (path n k) 1) *)
(* Proof:
   Note ?t. block_pairs (path n k)
          = (0, skip_ping (path n k) 1) :: t   by block_pairs_path_cons
     so HD (block_pairs (path n k))
      = (0, skip_ping (path n k) 1)            by HD
*)
Theorem block_pairs_path_head:
  !n k. 0 < k ==> HD (block_pairs (path n k)) = (0, skip_ping (path n k) 1)
Proof
  metis_tac[block_pairs_path_cons, HD]
QED

(* Theorem: block_pairs (path n k) = [] <=> k = 0 *)
(* Proof:
   If part: block_pairs (path n k) = [] ==> k = 0
      By contradiction, suppose k <> 0.
      Then 0 < k                               by NOT_ZERO
       and block_pairs (path n k) <> []        by block_pairs_path_not_nil
      which is a contradiction.

   Only-if part: k = 0 ==> block_pairs (path n k) = []
        block_pairs (path n 0)
      = []                                     by block_pairs_path_0
*)
Theorem block_pairs_path_eq_nil:
  !n k. block_pairs (path n k) = [] <=> k = 0
Proof
  rw[EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `0 < k` by decide_tac >>
    fs[block_pairs_path_not_nil],
    simp[block_pairs_path_0]
  ]
QED

(* Theorem: let ls = path n k in ~is_ping (LAST ls) /\ MEM (v,w) (block_pairs ls) ==>
            v < w /\ w <= k /\ w = skip_ping ls (v + 1) /\
            is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
            (!j. v < j /\ j < w ==> is_ping (EL j ls)) *)
(* Proof:
   Let ls = path n k,
        f = \j. (j,skip_ping ls (j + 1)).

       MEM (v,w) (block_pairs ls)
   <=> MEM (v,w) (MAP f (pong_indices (FRONT ls)))             by block_pairs_def
   ==> ?j. (v,w) = f j /\ MEM j (pong_indices (FRONT ls))      by MEM_MAP
   ==> ?j. (v,w) = f j /\ j < k /\ is_pong (EL j ls)           by pong_indices_path_element
   ==> ?h. j <= h /\ h < k /\
           (!p. j < p /\ p <= h ==> is_ping (EL p ls)) /\
           ~is_ping (EL (SUC h) ls)                            by pong_seed_ping_after
    or (!p. j + 1 <= p /\ p < SUC h ==> is_ping (EL p ls))     by above
   ==> skip_ping ls (j + 1) = SUC h                            by skip_ping_thm
   Now (v,w) = f j = (j, skip_ping ls (j + 1))
    so v = j, w = skip_ping ls (j + 1) = SUC h                 by PAIR_EQ
   Thus v = j < SUC h = w,
        w = SUC h <= k,
        !j. v < j /\ j <= h <=> v < j /\ j < SUC h = w.
*)
Theorem block_pairs_path_mem:
  !n k v w. let ls = path n k in ~is_ping (LAST ls) /\ MEM (v,w) (block_pairs ls) ==>
            v < w /\ w <= k /\ w = skip_ping ls (v + 1) /\
            is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
            (!j. v < j /\ j < w ==> is_ping (EL j ls))
Proof
  simp[] >>
  ntac 5 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `f = \j. (j,skip_ping ls (j + 1))` >>
  `MEM (v,w) (MAP f (pong_indices (FRONT ls)))` by simp[GSYM block_pairs_def, Abbr`f`] >>
  fs[MEM_MAP] >>
  `y < k /\ is_pong (EL y ls)` by metis_tac[pong_indices_path_element] >>
  `f y = (y,skip_ping ls (y + 1))` by simp[Abbr`f`] >>
  assume_tac pong_seed_ping_after >>
  last_x_assum (qspecl_then [`n`, `k`, `y`] strip_assume_tac) >>
  rfs[] >>
  `skip_ping ls (y + 1) = SUC h` by fs[skip_ping_thm] >>
  `v = y /\ w = SUC h` by metis_tac[PAIR_EQ] >>
  fs[]
QED

(* Theorem: let ls = path n k in
            (v < k /\ is_pong (EL v ls) <=> MEM (v, skip_ping ls (v + 1)) (block_pairs ls)) *)
(* Proof:
   Let ls = path n k,
       fs = FRONT ls,
        f = \j. (j,skip_ping ls (j + 1)).
   Thus block_pairs ls
      = MAP f (pong_indices fs)                by block_pairs_def
   Note ls <> []                               by path_not_nil
    and LENGTH fs = k                          by path_front_length
    and f v = (v, skip_ping ls (v + 1))        by application

        v < k /\ is_pong (EL v ls)
    <=> MEM v (pong_indices fs)                by pong_indices_path_element, v < k
    <=> MEM (f v) (MAP f (pong_indices fs))    by MEM_MAP
*)
Theorem block_pairs_path_pong:
  !n k v. let ls = path n k in
          (v < k /\ is_pong (EL v ls) <=> MEM (v, skip_ping ls (v + 1)) (block_pairs ls))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `f = \j. (j,skip_ping ls (j + 1))` >>
  `block_pairs ls = MAP f (pong_indices fs)` by simp[block_pairs_def, Abbr`ls`, Abbr`fs`] >>
  `ls <> [] /\ LENGTH fs = k` by simp[path_not_nil, path_front_length, Abbr`ls`, Abbr`fs`] >>
  `v < k /\ is_pong (EL v ls) <=> MEM v (pong_indices fs)` by metis_tac[pong_indices_path_element] >>
  simp[MEM_MAP, Abbr`f`]
QED

(* Theorem: let ls = path n k; ps = block_pairs ls in ~is_ping (LAST ls) /\
             0 < j /\ j < LENGTH ps /\ (a,b) = EL (j - 1) ps /\ (c,d) = EL j ps ==>
             b <= c /\ c = skip_pung ls b /\
             !p. b <= p /\ p < c ==> is_pung (EL p ls) *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
        h = LENGTH ps.
   Then j < h ==> j - 1 < h                    by arithmetic
     so MEM (a,b) ps /\ MEM (c,d) ps           by MEM_EL
    and a < k /\ is_pong (EL a ls)             by block_pairs_path_mem, LESS_LESS_EQ_TRANS
    and c < k /\ is_pong (EL c ls)             by block_pairs_path_mem, LESS_LESS_EQ_TRANS

   Claim: a < c /\ !p. a < p /\ p < c ==> ~is_pong (EL p ls)
   Proof: Note a = EL (j - 1) px               by block_pairs_element
           and c = EL j px                     by block_pairs_element
           Now LENGTH px = h                   by block_pairs_length
           ==> a < c                           by pong_indices_monotonic
           and !p. a < p /\ p < c ==> ~is_pong (EL p ls)
                                               by pong_indices_path_pong_gap

  Thus ?cut. a < cut /\ cut <= c /\ ~is_ping (EL cut ls) /\
       !p. cut <= p /\ p < c ==> is_pung (EL p ls)
                                               by pong_interval_cut_exists, claim.
   Now ~is_pung (EL c ls)                      by pong_not_pung
    so c = skip_pung ls cut                    by skip_pung_thm
   But cut = skip_ping ls (a + 1)              by skip_ping_thm
           = b                                 by block_pairs_path_mem
   Thus b <= c /\ c = skip_pung ls b           by above
*)
Theorem block_pairs_path_next:
  !n k j a b c d. let ls = path n k; ps = block_pairs ls in ~is_ping (LAST ls) /\
                  0 < j /\ j < LENGTH ps /\ (a,b) = EL (j - 1) ps /\ (c,d) = EL j ps ==>
                  b <= c /\ c = skip_pung ls b /\
                  !p. b <= p /\ p < c ==> is_pung (EL p ls)
Proof
  simp[] >>
  ntac 8 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `h = LENGTH ps` >>
  `j - 1 < h` by decide_tac >>
  `MEM (a,b) ps /\ MEM (c,d) ps` by metis_tac[MEM_EL] >>
  `a < k /\ is_pong (EL a ls) /\ c < k /\ is_pong (EL c ls)` by metis_tac[block_pairs_path_mem, LESS_LESS_EQ_TRANS] >>
  `a < c /\ !p. a < p /\ p < c ==> ~is_pong (EL p ls)` by
  (qabbrev_tac `px = pong_indices (FRONT ls)` >>
  `a = EL (j - 1) px /\ c = EL j px` by metis_tac[block_pairs_element] >>
  `LENGTH px = h` by fs[block_pairs_length, Abbr`px`, Abbr`ps`] >>
  `a < c` by metis_tac[pong_indices_monotonic] >>
  metis_tac[pong_indices_path_pong_gap]) >>
  assume_tac pong_interval_cut_exists >>
  last_x_assum (qspecl_then [`n`, `k`, `a`, `c`] strip_assume_tac) >>
  rfs[] >>
  rename1 `cut <= _` >>
  `c = skip_pung ls cut` by fs[skip_pung_thm, pong_not_pung] >>
  `cut = skip_ping ls (a + 1)` by fs[skip_ping_thm] >>
  metis_tac[block_pairs_path_mem]
QED

(* A significant achievement! *)

(* Theorem: let ls = path n k; ps = block_pairs ls in ~is_ping (LAST ls) /\
            0 < j /\ j < LENGTH ps /\ (a,b) = EL (j - 1) ps /\ (c,d) = EL j ps /\
            is_pong (EL b ls) ==> c = b *)
(* Proof:
   Note c = skip_pung ls b         by block_pairs_path_next
    now ~is_pung (EL b ls)         by pong_not_pung
    ==> skip_pung ls b = b         by skip_pung_none
     so c = b
*)
Theorem block_pairs_path_next_pong:
  !n k j a b c d. let ls = path n k; ps = block_pairs ls in ~is_ping (LAST ls) /\
                  0 < j /\ j < LENGTH ps /\ (a,b) = EL (j - 1) ps /\ (c,d) = EL j ps /\
                  is_pong (EL b ls) ==> c = b
Proof
  rw_tac std_ss[] >>
  `skip_pung ls b = b` by simp[skip_pung_none, pong_not_pung] >>
  metis_tac[block_pairs_path_next]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
            LAST (block_pairs ls) = (LAST (pong_indices (FRONT ls)), k) *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
        f = \j. (j,skip_ping ls (j + 1)),
       px = pong_indices (FRONT ls),
        v = LAST px.
   The goal is to show: LAST ps = (v, k).

   Note px <> []                               by pong_indices_path_not_nil, 0 < k
        LAST ps
      = LAST (MAP f px)                        by block_pairs_def
      = f (LAST px) = f v                      by LAST_MAP
      = (v, skip_ping ls (v + 1))              by application

   It remains to show: skip_ping ls (v + 1) = k.

   Note v < k /\ is_pong (EL v ls) /\
        !p. v < p /\ p < k ==> ~is_pong (EL p ls)
                                               by pong_indices_path_last
    Now v < k ==> v + 1 <= k                   by arithmetic, [1]
    and ~is_ping (LAST ls)                     by path_last_not_ping
     or ~is_ping (EL k ls)                     by path_last_alt, [2]

  Claim: !j. v < j /\ j < k ==> is_ping (EL j ls)
  Proof: By contradiction, suppose ~is_ping (EL j ls).
         Note ~is_pong (EL j ls)               by v < j /\ j < k
           so is_pung (EL j ls)                by triple_cases_alt
         Let h = k - 1.
         Then ~is_pung (EL h ls)               by path_last_flip_fix_not_by_pung
           so j <> h, or j < h /\ h < k
          and ~is_pong (EL h ls)               by v < h /\ h < k
          ==> is_ping (EL h ls)                by triple_cases_alt
         Thus ?p. j < p /\ p < h /\ is_pong (EL p ls))
                                               by pung_to_ping_has_pong
          but ~is_pong (EL p ls)               by v < p /\ p < k
         This is a contradiction.

  Therefore, !j. v + 1 <= j /\ j < k ==> is_ping (EL j ls)
                                               by arithmetic, claim, [3]
  Hence skip_ping ls (v + 1) = k               by skip_ping_thm, [1],[2],[3].
*)
Theorem block_pairs_path_last:
  !n k. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
        LAST (block_pairs ls) = (LAST (pong_indices (FRONT ls)), k)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `f = \j. (j,skip_ping ls (j + 1))` >>
  qabbrev_tac `px = pong_indices (FRONT ls)` >>
  qabbrev_tac `v = LAST px` >>
  `ps = MAP f px` by simp[block_pairs_def, Abbr`ps`, Abbr`f`, Abbr`px`] >>
  `px <> []` by simp[pong_indices_path_not_nil, Abbr`px`, Abbr`ls`] >>
  `LAST ps = (v, skip_ping ls (v + 1))` by simp[LAST_MAP, Abbr`v`, Abbr`f`] >>
  `skip_ping ls (v + 1) = k` suffices_by metis_tac[PAIR_EQ] >>
  assume_tac pong_indices_path_last >>
  last_x_assum (qspecl_then [`n`, `k`] strip_assume_tac) >>
  rfs[] >>
  `~is_ping (EL k ls)` by metis_tac[path_last_alt, path_last_not_ping] >>
  irule skip_ping_thm >>
  rw[] >>
  spose_not_then strip_assume_tac >>
  `v < j /\ j <= k - 1 /\ v < k - 1 /\ k - 1 < k` by decide_tac >>
  qabbrev_tac `h = k - 1` >>
  `is_pung (EL j ls)` by metis_tac[triple_cases_alt] >>
  `~is_pung (EL h ls) /\ j <> h` by metis_tac[path_last_flip_fix_not_by_pung] >>
  `is_ping (EL h ls)` by metis_tac[triple_cases_alt] >>
  assume_tac pung_to_ping_has_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
  rfs[] >>
  `v < p /\ p < k` by decide_tac >>
  metis_tac[]
QED

(* A remarkable result showing the LAST block_pairs touches the k, the last of (path n k). *)

(* ------------------------------------------------------------------------- *)
(* Blocks Theorems.                                                          *)
(* ------------------------------------------------------------------------- *)

(* Theorem: blocks [] = [] *)
(* Proof:
   Let f = \j. (if j = 0 then 0 else SND (EL (j-1) ls), EL j ls).
     blocks []
   = MAP f [0 ..< LENGTH []]       by blocks_def
   = MAP f [0 ..< 0]               by LENGTH
   = MAP f []                      by listRangeLHI_NIL
   = []                            by MAP
*)
Theorem blocks_nil:
  blocks [] = []
Proof
  simp[blocks_def]
QED

(* Theorem: LENGTH (blocks ls) = LENGTH ls *)
(* Proof:
   Let f = \j. (if j = 0 then 0 else SND (EL (j-1) ls), EL j ls).
     LENGTH (blocks ls)
   = LENGTH (MAP f [0 ..< LENGTH ls])          by blocks_def
   = LENGTH [0 ..< LENGTH ls]                  by LENGTH_MAP
   = LENGTH ls                                 by listRangeLHI_LEN
*)
Theorem blocks_length:
  !ls. LENGTH (blocks ls) = LENGTH ls
Proof
  simp[blocks_def]
QED

(* Theorem: blocks (block_pairs (path n 0)) = [] *)
(* Proof:
     blocks (block_pairs (path n 0))
   = blocks []                     by block_pairs_path_0
   = []                            by blocks_nil
*)
Theorem blocks_path_0:
  !n. blocks (block_pairs (path n 0)) = []
Proof
  simp[block_pairs_path_0, blocks_nil]
QED

(* Theorem: 0 < k ==> ?t. blocks (block_pairs (path n k)) = (0, 0, skip_ping (path n k) 1) :: t *)
(* Proof:
   Let ls = block_pairs (path n k),
       sp = skip_ping (path n k) 1,
        f = \j. (if j = 0 then 0 else SND (EL (j - 1) ls), EL j ls).
   Note ?tt. ls = (0, sp) :: tt                by block_pairs_path_cons
     so LENGTH ls = SUC (LENGTH tt) > 0        by LENGTH
    and EL 0 ls = (0, sp)                      by EL, HD

     blocks (block_pairs (path n k))
   = MAP f [0 ..< LENGTH ls]                   by blocks_def
   = MAP f (0 :: [1 ..< LENGTH ls])            by listRangeLHI_CONS
   = f 0 :: MAP f [1 ..< LENGTH ls]            by MAP
   = (0, 0, sp):: MAP f [1 ..< LENGTH ls]      by applying f

   Pick MAP f [1 ..< LENGTH ls] as tail t.
*)
Theorem blocks_path_cons:
  !n k. 0 < k ==> ?t. blocks (block_pairs (path n k)) = (0, 0, skip_ping (path n k) 1) :: t
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = block_pairs (path n k)` >>
  qabbrev_tac `sp = skip_ping (path n k) 1` >>
  qabbrev_tac `f = \j. (if j = 0 then 0 else SND (EL (j - 1) ls), EL j ls)` >>
  `?t. ls = (0, sp) :: t` by metis_tac[block_pairs_path_cons] >>
  `0 < LENGTH ls /\ EL 0 ls = (0, sp)` by fs[] >>
  `[0 ..< LENGTH ls] = 0::[1 ..< LENGTH ls]` by simp[listRangeLHI_CONS] >>
  qexists_tac `MAP f [1 ..< LENGTH ls]` >>
  simp[blocks_def, Abbr`f`]
QED

(* Theorem: 0 < k ==> blocks (block_pairs (path n k)) <> [] *)
(* Proof:
   Let sp = skip_ping (path n k) 1.
   ?t. blocks (block_pairs (path n k))
     = (0, 0, sp) :: t                         by blocks_path_cons
   Thus blocks (block_pairs (path n k)) <> []  by NOT_NIL_CONS
*)
Theorem blocks_path_not_nil:
  !n k. 0 < k ==> blocks (block_pairs (path n k)) <> []
Proof
  metis_tac[blocks_path_cons, NOT_NIL_CONS]
QED

(* Theorem: 0 < k ==> HD (blocks (block_pairs (path n k))) = (0, 0, skip_ping (path n k) 1) *)
(* Proof:
   Let sp = skip_ping (path n k) 1.
   ?t. blocks (block_pairs (path n k))
     = (0, 0, sp) :: t                         by blocks_path_cons
   Thus HD (blocks (block_pairs (path n k)))
      = (0, 0, sp)                             by HD
*)
Theorem blocks_path_head:
  !n k. 0 < k ==> HD (blocks (block_pairs (path n k))) = (0, 0, skip_ping (path n k) 1)
Proof
  metis_tac[blocks_path_cons, HD]
QED

(* Theorem: blocks (block_pairs (path n k)) = [] <=> k = 0 *)
(* Proof:
   If part: blocks (block_pairs (path n k)) = [] <=> k = 0
      By contradiction, suppose k <> 0.
      Then 0 < k                   by NOT_ZERO
        so blocks (block_pairs (path n k)) <> []
                                   by blocks_path_not_nil
      which is a contradiction.

   Only-if part: k = 0 ==> blocks (block_pairs (path n k))
         blocks (block_pairs (path n 0))
       = []                        by blocks_path_0
*)
Theorem blocks_path_eq_nil:
  !n k. blocks (block_pairs (path n k)) = [] <=> k = 0
Proof
  rw[EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `0 < k` by decide_tac >>
    fs[blocks_path_not_nil],
    simp[blocks_path_0]
  ]
QED

(* Theorem: MEM (u,v,w) (blocks ls) <=>
            ?j. j < LENGTH ls /\ (v,w) = EL j ls /\ u = if j = 0 then 0 else SND (EL (j - 1) ls) *)
(* Proof:
   Let \j. (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls),
       h = LENGTH ls.
       MEM (u,v,w) (block ls)
   <=> MEM (u,v,w) (MAP f [0 ..< h])           by blocks_def
   <=> ?j. (u,v,w) = f j /\ MEM j [0 ..< h]    by MEM_MAP
   <=> ?j. (u,v,w) = (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls) /\
           j < h                               by listRangeLHI_MEM
   <=> ?j. j < h /\ u = if j = 0 then 0 else SND (EL (j - 1) ls) /\ (v,w) = EL j ls
                                               by PAIR_EQ
*)
Theorem blocks_mem:
  !ls u v w. MEM (u,v,w) (blocks ls) <=>
             ?j. j < LENGTH ls /\ (v,w) = EL j ls /\
                 u = if j = 0 then 0 else SND (EL (j - 1) ls)
Proof
  rw[blocks_def, MEM_MAP] >>
  metis_tac[]
QED

(* Theorem: let ls = path n k in flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==>
            u <= v /\ v < w /\ w <= k /\
            ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
            v = skip_pung ls u /\ w = skip_ping ls (v + 1) /\
            (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
            (!j. v < j /\ j < w ==> is_ping (EL j ls))  *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
       px = pong_indices (FRONT ls),
        h = LENGTH ps.

   Note MEM (u,v,w) (blocks ps)
    ==> ?j. j < h /\ (v,w) = EL j ps
    and u = if j = 0 then 0 else SND (EL (j - 1) ps)
                                               by blocks_mem, PAIR_EQ
   Note h = LENGTH px                          by block_pairs_length
     so h <= k                                 by pong_indices_length, path_front_length
     or 0 < h /\ 0 < k                         by j < h, h <= k

    Now ~is_ping (LAST ls)                     by path_last_not_ping
    and MEM (v,w) ps                           by MEM_EL, j < h
    ==> v < w /\ w <= k /\ w = skip_ping ls (v + 1) /\
        is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
        !j. v < j /\ j < w ==> is_ping (EL j ls)
                                               by block_pairs_path_mem
   It remains to show:
        u <= v /\ ~is_ping (EL u ls) /\ v = skip_pung ls u /\
        !j. u <= j /\ j < v ==> is_pung (EL j ls)

   If j = 0,
      Then u = 0, and 0 <= v is trivial.
       Now EL j ps = HD ps                     by EL
                   = (0, skip_ping ls 1)       by block_pairs_path_head
      Thus v = 0                               by PAIR_EQ
      Note is_pong (EL 0 ls)                   by path_head_is_pong
        so ~is_ping (EL 0 ls)                  by pong_not_ping
       and ~is_pung (EL 0 ls)                  by pong_not_pung
       ==> skip_pung ls 0 = 0 = v              by skip_pung_none
       and the range !j. u <= j /\ j < v is empty.

   Otherwise j <> 0,
      Then u = SND (EL (j - 1) ps
      Thus ?a. EL (j - 1) ps = (a, u)          by PAIR, SND
       and MEM (a,u) ps                        by MEM_EL, j - 1 < h
        so ~is_ping (EL u ls)                  by block_pairs_path_mem
      Also u <= v /\ v = skip_pung ls u /\
           !j. u <= j /\ j < v ==> is_pung (EL j ls)
                                               by block_pairs_path_next, 0 < j
*)
Theorem blocks_path_mem:
  !n k u v w. let ls = path n k in flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==>
              u <= v /\ v < w /\ w <= k /\
              ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
              v = skip_pung ls u /\ w = skip_ping ls (v + 1) /\
              (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
              (!j. v < j /\ j < w ==> is_ping (EL j ls))
Proof
  simp[blocks_mem] >>
  ntac 6 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `px = pong_indices (FRONT ls)` >>
  qabbrev_tac `h = LENGTH ps` >>
  `LENGTH px = h /\ h <= k` by metis_tac[block_pairs_length, pong_indices_length, path_front_length] >>
  `0 < h /\ 0 < k` by decide_tac >>
  `~is_ping (LAST ls)` by metis_tac[path_last_not_ping] >>
  `MEM (v,w) ps` by metis_tac[MEM_EL] >>
  qabbrev_tac `X = (u = if j = 0 then 0 else SND (EL (j - 1) ps))` >>
  assume_tac block_pairs_path_mem >>
  last_x_assum (qspecl_then [`n`, `k`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  qunabbrev_tac `X` >>
  Cases_on `j = 0` >| [
    `u = 0 /\ v = 0` by fs[block_pairs_path_head, Abbr`ps`, Abbr`ls`] >>
    `is_pong (EL 0 ls)` by metis_tac[path_head_is_pong] >>
    fs[pong_not_ping, pong_not_pung, skip_pung_none],
    `?a. EL (j - 1) ps = (a, u)` by metis_tac[PAIR, SND] >>
    `0 < j /\ j - 1 < h` by decide_tac >>
    `MEM (a,u) ps` by metis_tac[MEM_EL] >>
    `~is_ping (EL u ls)` by metis_tac[block_pairs_path_mem] >>
    assume_tac block_pairs_path_next >>
    last_x_assum (qspecl_then [`n`, `k`, `j`, `a`, `u`, `v`, `w`] strip_assume_tac) >>
    rfs[]
  ]
QED

(* A very good result! *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
            SND (LAST (blocks (block_pairs ls))) = (LAST (pong_indices (FRONT ls)),k) *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
       px = pong_indices (FRONT ls),
        f = \j. (if j = 0 then 0 else SND (EL (j - 1) ps),EL j ps),
        h = LENGTH ps.
   The goal is to show: SND (LAST (blocks ps)) = (LAST px, k)

   Note ps <> []                   by block_pairs_path_eq_nil, 0 < k
     so h <> 0                     by LENGTH_EQ_0
    and [0 ..< h] <> []            by listRangeLHI_NIL, 0 < h
     LAST (blocks ps)
   = LAST (MAP f [0 ..< h])        by blocks_def
   = f (LAST [0 ..< h])            by LAST_MAP
   = f (h - 1)                     by listRangeLHI_LAST
   = (if h - 1 = 0 then 0 else SND (EL (h - 2) ps),EL (h - 1) ps)

   Thus SND (LAST (blocks ps))
      = EL (h - 1) ps              by SND
      = LAST ps                    by LAST_EL
      = (LAST px, k)               by block_pairs_path_last
*)
Theorem blocks_path_last:
  !n k. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
        SND (LAST (blocks (block_pairs ls))) = (LAST (pong_indices (FRONT ls)),k)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `px = pong_indices (FRONT ls)` >>
  qabbrev_tac `h = LENGTH ps` >>
  qabbrev_tac `f = \j. (if j = 0 then 0 else SND (EL (j - 1) ps),EL j ps)` >>
  `ps <> []` by simp[block_pairs_path_eq_nil, Abbr`ps`, Abbr`ls`] >>
  `h <> 0` by metis_tac[LENGTH_EQ_0] >>
  `[0 ..< h] <> []` by simp[listRangeLHI_NIL] >>
  `LAST (blocks ps) = LAST (MAP f [0 ..< h])` by fs[blocks_def, Abbr`ps`, Abbr`f`] >>
  `_ = f (LAST [0 ..< h])` by simp[LAST_MAP] >>
  `0 <= h - 1 /\ (h - 1) + 1 = h /\ PRE h = h - 1` by decide_tac >>
  `LAST [0 ..< h] = h - 1` by metis_tac[listRangeLHI_LAST] >>
  simp[Abbr`f`] >>
  metis_tac[LAST_EL, block_pairs_path_last]
QED

(* Idea: the third of a block can be reached by the first through steps of pung-pong-ping. *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==>
            EL w ls = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls) *)
(* Proof:
   Let ls = path n k.
   Note u <= v /\ v < w /\ w <= k /\
        ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
        v = skip_pung ls u /\ w = skip_ping ls (v + 1)           by blocks_path_mem
   Thus v < k /\ !p. u <= p /\ p < v ==> is_pung (EL p ls) /\
        w <= k /\ !p. v < p /\ p < w ==> is_ping (EL p ls)       by path_skip_from_pung_to_ping, u < k
     or      !p. v + 1 <= p /\ p < w ==> is_ping (EL p ls)       by inequality

     EL w ls
   = EL (v + 1 + (w - (v + 1))) ls                               by v < w ==> v + 1 <= w
   = (FUNPOW ping (w - (v + 1))) (EL (v + 1) ls)                 by path_element_skip_ping, w <= k
   = (FUNPOW ping (w - (v + 1))) (EL (SUC v) ls)                 by ADD1
   = (FUNPOW ping (w - (v + 1))) ((flip o zagier) (EL v ls))     by path_element_suc, v < k
   = (FUNPOW ping (w - (v + 1))) (pong (EL v ls))                by flip_zagier_pong, is_pong (EL v ls)
   = (FUNPOW ping (w - (v + 1))) (pong (EL (u + (v - u)) ls))    by u <= v
   = (FUNPOW ping (w - (v + 1))) (pong (FUNPOW pung (v - u) (EL u ls)))  by path_element_skip_pung, v <= k
   = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls)  by composition
*)
Theorem blocks_path_third_by_funpow:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==>
              EL w ls = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls)
Proof
  rw_tac std_ss[] >>
  assume_tac blocks_path_mem >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  `EL w ls = FUNPOW ping (w - (v + 1)) (pong (FUNPOW pung (v - u) (EL u ls)))` suffices_by simp[] >>
  qabbrev_tac `X = (w = skip_ping ls (v + 1))` >>
  qabbrev_tac `Y = (v = skip_pung ls u)` >>
  assume_tac path_skip_from_pung_to_ping >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  qunabbrev_tac `X` >>
  qunabbrev_tac `Y` >>
  `EL (v + 1) ls = pong (EL v ls)` by
  (`v < k` by decide_tac >>
  `EL (v + 1) ls = (flip o zagier) (EL v ls)` by metis_tac[path_element_suc, ADD1] >>
  simp[flip_zagier_pong]) >>
  `EL v ls = FUNPOW pung (v - u) (EL u ls)` by
    (`v = u + (v - u)` by decide_tac >>
  `EL v ls = EL (u + (v - u)) ls` by simp[] >>
  assume_tac path_element_skip_pung >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v - u`] strip_assume_tac) >>
  rfs[]) >>
  `EL w ls = (FUNPOW ping (w - (v + 1))) (EL (v + 1) ls)` by
      (`w = v + 1 + (w - (v + 1))` by decide_tac >>
  `EL w ls = EL (v + 1 + (w - (v + 1))) ls` by fs[] >>
  assume_tac path_element_skip_ping >>
  last_x_assum (qspecl_then [`n`, `k`, `v + 1`, `w - (v + 1)`] strip_assume_tac) >>
  rfs[]) >>
  fs[]
QED

(* Idea: the third of a block can be reached by the first through hop. *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==>
            EL w ls = hop (w - u) (EL u ls) *)
(* Proof:
   Let ls = path n k,
        t = EL u ls.
   The goal is to show: EL w ls = hop (w - u) t.

   Note u <= v /\ v < w /\ w <= k /\
        ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
        v = skip_pung ls u /\ w = skip_ping ls (v + 1)
                                               by blocks_path_mem
    and !p. u <= p /\ p < v ==> is_pung (EL p ls)
                                               by path_skip_from_pung_to_ping, u < k, [*]

   Note n = windmill t                         by path_element_windmill_alt, u <= k, [1]

   Claim: is_pong (FUNPOW pung (v - u) t)
   Proof:    FUNPOW pung (v - u) t
           = FUNPOW pung (v - u) (EL u ls)     by notation
           = EL (u + (v - u)) ls               by path_element_skip_pung, [*]
           = EL v ls                           by u <= v
          Thus is_pong (FUNPOW pung (v - u) t) by is_pong (EL v ls), [2]

   Claim: !h. h < v - u ==> is_pung (FUNPOW pung h t)
   Proof: Note h < v - u
           <=> 0 <= h /\ h < v - u
           <=> u <= h + u /\ h + u < v
             FUNPOW pung h t
           = FUNPOW pung h (EL u ls)           by notation
           = EL (u + h) ls                     by path_element_skip_pung
           Thus is_pung (EL (u + h) ls)        by [*], [3]

   Thus EL w ls
      = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) t
                                               by blocks_path_third_by_funpow
      = hop (w - (v + 1) + (v - u) + 1) t      by pung_to_ping_by_hop_alt, [1],[2],[3]
      = hop (w - (v + 1) + (v + 1) - u) t      by u <= v
      = hop (w - u) t
*)
Theorem blocks_path_third_by_hop:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==>
              EL w ls = hop (w - u) (EL u ls)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `t = EL u ls` >>
  assume_tac blocks_path_mem >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  `EL w ls = hop (w - u) t` suffices_by simp[] >>
  qabbrev_tac `X = (w = skip_ping ls (v + 1))` >>
  qabbrev_tac `Y = (v = skip_pung ls u)` >>
  `n = windmill t` by fs[path_element_windmill_alt, Abbr`t`, Abbr`ls`] >>
  `u < k` by decide_tac >>
  `!p. u <= p /\ p < v ==> is_pung (EL p ls)` by metis_tac[path_skip_from_pung_to_ping] >>
  `is_pong (FUNPOW pung (v - u) t)` by
  (assume_tac path_element_skip_pung >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v - u`] strip_assume_tac) >>
  rfs[] >>
  fs[]) >>
  `!h. h < v - u ==> is_pung (FUNPOW pung h t)` by
    (rpt strip_tac >>
  assume_tac path_element_skip_pung >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `h`] strip_assume_tac) >>
  rfs[] >>
  `u <= h + u /\ h + u < v` by decide_tac >>
  metis_tac[]) >>
  assume_tac blocks_path_third_by_funpow >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  assume_tac pung_to_ping_by_hop_alt >>
  last_x_assum (qspecl_then [`n`, `t`, `w - (v + 1)`, `v - u`] strip_assume_tac) >>
  rfs[]
QED

(* ------------------------------------------------------------------------- *)
(* Hopping Algorithm                                                         *)
(* ------------------------------------------------------------------------- *)

(* To ensure hop with stay as a windmill, the conditions from hop_range are:
   x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y)
   which can be taken as (x + k) DIV (2 * y)
   with 0 < k <= SQRT n, seems integer 0 <= k <= SQRT n is good.
*)

(* Define the step function *)
Definition step_def:
   step k (x,y,z) = (x + k) DIV (2 * y)  (* depends on only x, y, not z *)
End
(* the parameter k is taken from the caller, the hopping. *)

(* Define the hopping function *)
Definition hopping_def:
   hopping k t = hop (step k t) t
End
(* the parameter k = SQRT n is taken from the caller, two_sq_hop. *)

(* Define two_squares algorithm with hopping *)
Definition two_sq_hop_def:
   two_sq_hop n =  WHILE ($~ o found) (hopping (SQRT n)) (1,1,n DIV 4)
End

(* The map: mind (x,y,z) gives the mind of a windmill.

mind_def  |- !x y z. mind (x,y,z) = if x < y - z then x + 2 * z else if x < y then 2 * y - x else x

Zagier map preserves the mind;
mind_zagier_eqn |- !x y z. mind (zagier (x,y,z)) = mind (x,y,z)

Generally, flip map changes the mind:
EVAL ``mind (flip (x,y,z))``;
|- mind (flip (x,y,z)) = if x < z - y then x + 2 * y else if x < z then 2 * z - x else x

But  mind (7,1,3) = 7 = mind (7,3,1), so sometimes flip does not change the mind.
Thus, when  mind t = mind (flip t), the triple t is a node, and the pair (t, flip t) share the same mind.
These nodes in (mill n) are close to the hopping points of the improved algorithm.

Part of the reason is due to the map   phi_m = F_1^{m} H = H F_3^{m}, with m = (x + SQRT n) DIV (2 * y) when applied to t = (x,y,z).
hop_def         |- !m x y z. hop m (x,y,z) = (2 * m * y - x,z + m * x - m * m * y,y)
step_def        |- !k x y. step k (x,y) = (x + k) DIV (2 * y)
hopping_def     |- !k x y z. hopping k (x,y,z) = hop (step k (x,y)) (x,y,z)
two_sq_hop_def  |- !n. two_sq_hop n = WHILE ($~ o found) (hopping (SQRT n)) (1,1,n DIV 4)

> EVAL ``two_sq_hop 5``;
val it = |- two_sq_hop 5 = (1,1,1): thm
> EVAL ``two_sq_hop 13``;
val it = |- two_sq_hop 13 = (3,1,1): thm
> EVAL ``two_sq_hop 17``;
val it = |- two_sq_hop 17 = (1,2,2): thm
> EVAL ``two_sq_hop 29``;
val it = |- two_sq_hop 29 = (5,1,1): thm
> EVAL ``two_sq_hop 37``;
val it = |- two_sq_hop 37 = (1,3,3): thm
> EVAL ``two_sq_hop 41``;
val it = |- two_sq_hop 41 = (5,2,2): thm
> EVAL ``two_sq_hop 53``;
val it = |- two_sq_hop 53 = (7,1,1): thm
> EVAL ``two_sq_hop 61``;
val it = |- two_sq_hop 61 = (5,3,3): thm
> EVAL ``two_sq_hop 73``;
val it = |- two_sq_hop 73 = (3,4,4): thm
> EVAL ``two_sq_hop 89``;
val it = |- two_sq_hop 89 = (5,4,4): thm
> EVAL ``two_sq_hop 97``;
val it = |- two_sq_hop 97 = (9,2,2): thm
> EVAL ``two_sq_hop 1277``;
val it = |- two_sq_hop 1277 = (11,17,17): thm (reduces from 23 steps to 5 hops)

> EVAL ``two_sq_hop 773``;
val it = |- two_sq_hop 773 = (17,11,11): thm
> EVAL ``two_sq_hop 797``;
val it = |- two_sq_hop 797 = (11,13,13): thm
> EVAL ``two_sq_hop 977``;
val it = |- two_sq_hop 977 = (31,2,2): thm
> EVAL ``two_sq_hop 997``;
val it = |- two_sq_hop 997 = (31,3,3): thm
> EVAL ``two_sq_hop 1801``;
val it = |- two_sq_hop 1801 = (35,12,12): thm   (reduces from 132 steps to 33 hops)
> EVAL ``two_sq_hop 1933``;
val it = |- two_sq_hop 1933 = (13,21,21): thm

*)

(* Theorem: let ls = path n k in
            flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
            ~is_ping (EL u ls) /\ let (x,y,z) = EL u ls in y <= x + z *)
(* Proof:
   Note ~is_ping (EL u ls)         by blocks_path_mem
     so ~(x < y - z)               by is_ping_def
     or y - z <= x
     or y <= x + z
*)
Theorem blocks_triple_first_not_ping:
  !n k u v w. let ls = path n k in
              flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
              ~is_ping (EL u ls) /\ let (x,y,z) = EL u ls in y <= x + z
Proof
  simp[] >>
  ntac 6 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `~is_ping (EL u ls)` by metis_tac[blocks_path_mem] >>
  `?x y z. EL u ls = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_ping_def]
QED

(* Theorem: step 0 (x,y,z) = x DIV (2 * y) *)
(* Proof:
     step 0 (x,y,z)
   = (x + 0) DIV (2 * y)       by step_def
   = x DIV (2 * y)             by ADD_0
*)
Theorem step_0:
  !x y z. step 0 (x,y,z) = x DIV (2 * y)
Proof
  simp[step_def]
QED

(* Theorem: step (SQRT n) (x,y,z) = (x + SQRT n) DIV (2 * y) *)
(* Proof: by step_def. *)
Theorem step_sqrt:
  !n x y z. step (SQRT n) (x,y,z) = (x + SQRT n) DIV (2 * y)
Proof
  simp[step_def]
QED

(* ------------------------------------------------------------------------- *)
(* Stepping and Hopping.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Note:
   Given the principal ls = path n k, the successive elements are linear:

   j <= k ==> EL (SUC j) ls = (flip o zagier) (EL j ls)

   The ping, pong and pung are just specific cases of (flip o zagier),
   in fact the same. Although for a block triple (u,v,w), we have:
   v = skip_pung ls u, and w = skip_ping ls (v + 1)

   As skip_pung and skip_ping are repeated versions of pung and ping,
   such algorithms are not better than original iteration.

   But hop is not linear, jut hops over triples from a starting one (x,y,z).
   hop 0   jumps to a phantom: (-x,z,y)
   hop 1   is pong, suitable only if is_pong (x,y,z)
   hop 2   jumps to (2y(2) - x, z + x(2) - y(2)(2), y)
   hop 3   jumps to (2y(3) - x, z + x(3) - y(3)(3), y)

   We have proved:   El w ls = hop (w - u) (EL u ls)
   which is a hint between hopping and iteration, but this hint is useful
   only when (w - u) is known without skip_pung or skip_ping.

   Luckily, due the definition of hop:
        hop m (x,y,z) = (2 * m * y - x,z + m * x - m * m * y,y)
   The first component keeps inceasing linearly,
   the second component decreases quadratically,
   the third component keeps feeding from previous second.

   There seems to be formulae for v and w, for EL u ls = (x,y,z):
         v = u + step 0 (x,y,z)
         w = u + step (SQRT n) (x,y,z)

   Once we know ~is_ping (EL u ls), whether is_pong or is_pung
   depends only on x < 2 * y or not. This gives the value
          x DIV (2 * y) = step 0 (x,y,z)
   and this seems to determine how many pung's have skipped,
   plus 1 to pass the is_pong (EL v ls). After that, it is skip_ping,
   and (x + SQRT n) DIV (2 * y) = step (SQRT) (x,y,z)
   determines how many skips across a block to arrive at the next block.

*)

(*
hop_windmill
|- !m n x y z. n = windmill (x,y,z) /\ 0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y ==>
               n = windmill (hop m (x,y,z))
hop_range_iff
|- !m n x y z. ~square n /\ n = windmill (x,y,z) ==>
           (0 < 2 * m * y - x /\ 0 < z + m * x - m * m * y <=>
            0 < y /\ x DIV (2 * y) < m /\ m <= (x + SQRT n) DIV (2 * y))

> EVAL ``let u = (1,1,24) in MAP (\j. hop j u) [0 ..< 9]``; =
      [(0,24,1); (1,24,1); (3,22,1); (5,18,1); (7,12,1); (9,4,1); (11,0,1); (13,0,1); (15,0,1)]
> EVAL ``skip_ping (let u = (1,1,24) in MAP (\j. hop j u) [0 ..< 9]) 0``; = 5
*)



(* Define the hopping nodes on the principal orbit *)
Definition node_def:
   node n 0 = (1, 1, n DIV 4) /\
   node n (SUC j) = let m = step (SQRT n) (node n j) in hop m (node n j)
End

(*
> EVAL ``node 61 0``;
val it = |- node 61 0 = (1,1,15): thm
> EVAL ``node 61 1``;
val it = |- node 61 1 = (7,3,1): thm
> EVAL ``node 61 2``;
val it = |- node 61 2 = (5,3,3): thm
> EVAL ``node 61 3``;
val it = |- node 61 3 = (7,1,3): thm
> EVAL ``node 61 4``;
val it = |- node 61 4 = (7,3,1): thm
> EVAL ``step (SQRT 61) (5,3,3)``;
val it = |- step (SQRT 61) (5,3,3) = 2: thm
> EVAL ``step (SQRT 61) (7,1,3)``;
val it = |- step (SQRT 61) (7,1,3) = 7: thm  (skipping the zagier-fix!)

> EVAL ``node 97 0``;
val it = |- node 97 0 = (1,1,24): thm
> EVAL ``node 97 1``;
val it = |- node 97 1 = (9,4,1): thm
> EVAL ``node 97 2``;
val it = |- node 97 2 = (7,3,4): thm
> EVAL ``node 97 3``;
val it = |- node 97 3 = (5,6,3): thm
> EVAL ``node 97 4``;
val it = |- node 97 4 = (7,2,6): thm
> EVAL ``node 97 5``;
val it = |- node 97 5 = (9,2,2): thm
*)

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
