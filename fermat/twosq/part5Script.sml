(* ------------------------------------------------------------------------- *)
(* Fermat's Two Squares Theorem: a proof using corner shapes.                *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "part5";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)

(* val _ = load "quarityTheory"; *)
open helperTwosqTheory;
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory; (* for divides_def, prime_def *)
open EulerTheory; (* for natural_finite, natural_card *)
open logPowerTheory; (* for prime_non_square *)

open quarityTheory;
open pairTheory;

(* val _ = load "involuteFixTheory"; *)
open involuteTheory;
open involuteFixTheory;


(* ------------------------------------------------------------------------- *)
(* Windmills of the minds (part 5) Documentation                             *)
(* ------------------------------------------------------------------------- *)
(* ------------------------------------------------------------------------- *)
(* Fermat Two Squares by Corners Documentation                               *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   Helper Theorems:
   quadruples_parts    |- !q. ?a x b y. q = (a,x,b,y)

   The set of corner pieces:
   corners_def         |- !n. corners n = {(a,x,b,y) | n = a * x + b * y /\ 0 < x /\ 0 < y /\ 0 < a /\ a < b}
   corners_subset      |- !n. corners n SUBSET (let s = count (n + 1) in s CROSS s CROSS s CROSS s)
   corners_finite      |- !n. FINITE (corners n)
   corners_empty       |- !n. corners n = {} <=> n <= 2

   Involution on corners:
   conjugate_def       |- !a x b y. conjugate (a,x,b,y) = (y,b - a,y + x,a)
   conjugate_involute  |- !a b x y. a < b ==> conjugate (conjugate (a,x,b,y)) = (a,x,b,y)
   conjugate_involute_corners
                       |- !n. conjugate involute (corners n)
   conjugate_fix       |- !a b x y. conjugate (a,x,b,y) = (a,x,b,y) <=> a = y /\ b = x + y
   conjugate_fix_lt    |- !a b x y. a < b ==> (conjugate (a,x,b,y) = (a,x,b,y) <=> x = b - a /\ y = a)
   corners_conjugate_fixes
                       |- !n a b. fixes conjugate (corners n) =
                                  {(a,b - a,b,a) | n = a * (2 * b - a) /\ 0 < a /\ a < b}

   Sets of corners partition:
   corners_less_def    |- !n. corners_less n = {(a,x,b,y) | (a,x,b,y) IN corners n /\ x < y}
   corners_eq_def      |- !n. corners_eq n = {(a,x,b,y) | (a,x,b,y) IN corners n /\ x = y}
   corners_more_def    |- !n. corners_more n = {(a,x,b,y) | (a,x,b,y) IN corners n /\ y < x}
   corners_less_element|- !n a x b y. (a,x,b,y) IN corners_less n <=> (a,x,b,y) IN corners n /\ x < y
   corners_less_subset |- !n. corners_less n SUBSET corners n
   corners_less_finite |- !n. FINITE (corners_less n)
   corners_eq_element  |- !n a x b y. (a,x,b,y) IN corners_eq n <=> (a,x,b,y) IN corners n /\ x = y
   corners_eq_subset   |- !n. corners_eq n SUBSET corners n
   corners_eq_finite   |- !n. FINITE (corners_eq n)
   corners_more_element|- !n a x b y. (a,x,b,y) IN corners_more n <=> (a,x,b,y) IN corners n /\ y < x
   corners_more_subset |- !n. corners_more n SUBSET corners n
   corners_more_finite |- !n. FINITE (corners_more n)
   corners_eq_alt      |- !n a b x. corners_eq n =
                                    {(a,x,b,x) | n = (a + b) * x /\ 0 < a /\ a < b /\ 0 < x}
   corners_less_alt    |- !n a b x y. corners_less n =
                                      {(a,x,b,y) | n = a * x + b * y /\ 0 < a /\ a < b /\ 0 < x /\ x < y}
   corners_more_alt    |- !n a b x y. corners_more n =
                                      {(a,x,b,y) | n = a * x + b * y /\ 0 < a /\ a < b /\ 0 < y /\ y < x}
   corners_partiton_thm|- !n. corners n = BIGUNION {corners_less n; corners_eq n; corners_more n}
   corners_card_eqn    |- !n. CARD (corners n) =
                              CARD (corners_less n) + CARD (corners_eq n) + CARD (corners_more n)

   Involutions on corner partitions:
   pair_invert_def         |- !a x b y. pair_invert (a,x,b,y) = (x,a,y,b)
   pair_swap_def           |- !a x b y. pair_swap (a,x,b,y) = (y,b,x,a)
   pair_invert_involute    |- !a x b y. pair_invert (pair_invert (a,x,b,y)) = (a,x,b,y)
   pair_swap_involute      |- !a x b y. pair_swap (pair_swap (a,x,b,y)) = (a,x,b,y)
   pair_invert_involute_corners_less
                           |- !n. pair_invert involute (corners_less n)
   pair_swap_involute_corners_more
                           |- !n. pair_swap involute (corners_more n)
   pair_invert_fix         |- !a x b y. pair_invert (a,x,b,y) = (a,x,b,y) <=> x = a /\ y = b
   pair_swap_fix           |- !a x b y. pair_swap (a,x,b,y) = (a,x,b,y) <=> x = b /\ y = a
   corners_less_pair_invert_fixes
                           |- !n a b. fixes pair_invert (corners_less n) =
                                      {(a,a,b,b) | n = a ** 2 + b ** 2 /\ 0 < a /\ a < b}
   corners_more_pair_swap_fixes
                           |- !n a b. fixes pair_swap (corners_more n) =
                                      {(a,b,b,a) | n = 2 * a * b /\ 0 < a /\ a < b}

   Fermat's Two Squares Theorem:
   prime_corners_eq        |- !p. prime p ==>
                                  corners_eq p = {(a,1,b,1) | p = a + b /\ 0 < a /\ a < b}
   prime_corners_eq_eqn    |- !p. prime p ==>
                                  corners_eq p = IMAGE (\a. (a,1,p - a,1)) (natural (HALF (p - 1)))
   prime_corners_eq_card   |- !p. prime p ==> CARD (corners_eq p) = HALF (p - 1)
   prime_corners_conjugate_fixes
                           |- !p. prime p ==>
                                  fixes conjugate (corners p) =
                                  if p = 2 then {} else {(1,HALF (p - 1),HALF (p + 1),1)}
   prime_corners_more_pair_swap_fixes
                           |- !p. prime p ==> fixes pair_swap (corners_more p) = {}
   tik_prime_corners_less_pair_invert_fixes
                           |- !p. tik p /\ prime p ==> fixes pair_invert (corners_less p) <> {}
   fermat_two_squares_by_corners
                           |- !p. tik p /\ prime p ==> ?(a,b). p = a ** 2 + b ** 2 /\ a < b
*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: ?a x b y. q = (a,x,b,y) *)
(* Proof: by a = FST q, x = FST (SND q), etc. *)
Theorem quadruples_parts:
  !q: num # num # num # num. ?a x b y. q = (a,x,b,y)
Proof
  metis_tac[PAIR]
QED

(* ------------------------------------------------------------------------- *)
(* The set of corner pieces.                                                 *)
(* ------------------------------------------------------------------------- *)

(*
Geometrically a corner piece looks like the Greek captial letter Γ:

               b                                    y       x
       +-----------------+                      +-------+----------+
       |                 |                      |       |          |
       |                 | y                    |       |          | a
       +-----------+-----+      conjugation   b |       |          |
       |           |                            |       +----------+
       |           | x                          |       |
       |           |                            |       |
       +-----------+                            +-------+
             a

So that the area corresponds to a number n = a * x + b * y.
The conditions 0 < x, 0 < y, 0 < a < b makes a valid shape, with a corner near the top-left.

The conjugation η : (a,x,b,y) → (y,b-a,y+x,a), a reflection about the slanting diagonal,
is an involution.

*)

(* Define the set of corner pieces for n *)
Definition corners_def[nocompute]:
   corners n = {(a,x,b,y) | n = a * x + b * y /\ 0 < x /\ 0 < y /\ 0 < a /\ a < b }
End
(* use [nocompute] as this is not effective. *)

(* Theorem: (a,x,b,y) IN corners n <=> n = a * x + b * y /\ 0 < x /\ 0 < y /\ 0 < a /\ a < b *)
(* Proof: by corners_def. *)
Theorem corners_element:
  !n a b x y. (a,x,b,y) IN corners n <=> n = a * x + b * y /\ 0 < x /\ 0 < y /\ 0 < a /\ a < b
Proof
  simp[corners_def]
QED

(* Theorem: corners n SUBSET (let s = count (n + 1) in s CROSS s CROSS s CROSS s) *)
(* Proof:
   By SUBSET_DEF, IN_COUNT, IN_CROSS, this is to show:
      (a,x,b,y) IN corners n ==> a <= n /\ b <= n /\ x <= n /\ y <= n
   Note n = a * x + b * y          by corners_def
     so a * x <= n                 by inequality
    and b * y <= n                 by inequality
    ==> x <= n /\ y <= n           by MULT_LE_IMP_LE
    and a <= n /\ b <= n           by MULT_LE_IMP_LE, MULT_COMM
*)
Theorem corners_subset:
  !n. corners n SUBSET (let s = count (n + 1) in s CROSS s CROSS s CROSS s)
Proof
  simp[SUBSET_DEF, FORALL_PROD] >>
  ntac 6 strip_tac >>
  rename1 `(a,x,b,y) IN _` >>
  fs[corners_def] >>
  `0 < b` by decide_tac >>
  `0 < n` by metis_tac[ADD_EQ_0, MULT_EQ_0, NOT_ZERO] >>
  `a <= n /\ x <= n /\ b <= n /\ y <= n` suffices_by simp[] >>
  `a * x <= n /\ b * y <= n` by decide_tac >>
  metis_tac[MULT_LE_IMP_LE, MULT_COMM]
QED

(* Theorem: FINITE (corners n) *)
(* Proof:
   Let s = count (n + 1),
       t = s CROSS s CROSS s CROSS s.
   Note (corners n) SUBSET t       by corners_subset
    and FINITE s                   by FINITE_COUNT
     so FINITE t                   by FINITE_CROSS
    ==> FINITE (corners n)         by SUBSET_FINITE
*)
Theorem corners_finite:
  !n. FINITE (corners n)
Proof
  metis_tac[corners_subset, FINITE_COUNT, FINITE_CROSS, SUBSET_FINITE]
QED

(* Theorem: corners n = {} <=> n <= 2 *)
(* Proof:
   If part: corners n = {} ==> n <= 2
      By contradiction, suppose ~(n <= 2).
      Then 2 < n                               by inequality
        so n = 1 * 1 + (n - 1) * 1             by arithmetic
       and 1 < n - 1                           by inequality
       ==> (1,1,n - 1,1) IN corners n          by corners_def
      This contradicts corners n = {}          by MEMBER_NOT_EMPTY
   Only-if part: n <= 2 ==> corners n = {}
      By contradiction, suppose corners n <> {}.
      Then ?q. q = (a,x,b,y) IN corners n      by MEMBER_NOT_EMPTY
       and n = a * x + b * y /\ 0 < a /\ a < b by corners_def
       Now 0 < b, given 0 < x /\ 0 < y         by inequality, 0 < a < b
        so 0 < a * x /\ 0 < b * y              by LESS_MULT2
       ==> 2 <= a * x + b * y                  by arithmetic
       This makes n = 2                        by 2 <= n <= 2
       ==> a * x = 1 /\ b * y = 1              by ADD_EQ_2
       ==> a = 1     /\ b = 1                  by MULT_EQ_1
       This contradicts a < b                  by LESS_REFL
*)
Theorem corners_empty:
  !n. corners n = {} <=> n <= 2
Proof
  rw[EXTENSION, EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `2 < n` by decide_tac >>
    `n = 1 * 1 + (n - 1) * 1 /\ 1 < n - 1` by decide_tac >>
    `(1,1,n - 1,1) IN corners n` by fs[corners_def] >>
    metis_tac[],
    spose_not_then strip_assume_tac >>
    rename1 `q IN _` >>
    fs[corners_def] >>
    `0 < b` by decide_tac >>
    `0 < a * x /\ 0 < b * y` by simp[] >>
    `n = 2` by decide_tac >>
    metis_tac[ADD_EQ_2, MULT_EQ_1, DECIDE``~(1 < 1)``]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Involution on corners.                                                    *)
(* ------------------------------------------------------------------------- *)

(* Define the conjugate of a corner piece. *)
Definition conjugate_def:
   conjugate (a, x, b, y) = (y, b - a, y + x, a)
End

(* Theorem: a < b ==> conjugate (conjugate (a, x, b, y)) = (a, x, b, y) *)
(* Proof:
     conjugate (conjugate (a, x, b, y))
   = conjugate (y, b - a, y + x, a)            by conjugate_def
   = (a, (y + x) - y, a + (b - a), y)          by conjugate_def
   = (a, x, b, y)                              by arithmetic
*)
Theorem conjugate_involute:
  !a b x y. a < b ==> conjugate (conjugate (a, x, b, y)) = (a, x, b, y)
Proof
  simp[conjugate_def]
QED

(* Theorem: conjugate involute (corners n) *)
(* Proof:
   Note q = (a,x,b,y) with 0 < a /\ a < b /\ 0 < x /\ 0 < y /\
        n = a * x + b * y                      by corners_def
    and conjugate q = (y,b-a,y+x,a)            by conjugate_def
        y * (b - a) + (y + x) * a
      = y * b - y * a + y * a + x * a          by arithmetic
      = y * b + x * a                          by arithmetic
      = a * x + b * y = n
    and 0 < b - a                              by a < b
    and 0 < y + x                              by 0 < x, 0 < y
     so conjuate q IN corners n                by corners_def
    and conjugate (conjugate q) = q            by conjugate_involute, a < b
*)
Theorem conjugate_involute_corners:
  !n. conjugate involute (corners n)
Proof
  ntac 3 strip_tac >>
  rename1 `q IN _` >>
  fs[corners_def] >>
  strip_tac >| [
    map_every qexists_tac [`y`, `b-a`, `y+x`, `a`] >>
    simp[conjugate_def] >>
    `y * a < y * b` by fs[] >>
    `a * (x + y) + y * (b - a) = a * x + a * y + (y * b - y * a)` by simp[] >>
    decide_tac,
    simp[conjugate_involute]
  ]
QED

(* Theorem: conjugate (a,x,b,y) = (a,x,b,y) <=> (a = y /\ b = x + y) *)
(* Proof:
     (a,x,b,y)
   = conjugate (a,x,b,y)           by assumption
   = (y,b - a,y + x,a)             by conjugate_def
   ==> a = y,
       x = b = a,
       b = y + x,
       y = a                       by equating components
   ==> a = y, b = x + y            by arithmetic
*)
Theorem conjugate_fix:
  !a b x y. conjugate (a,x,b,y) = (a,x,b,y) <=> (a = y /\ b = x + y)
Proof
  simp[conjugate_def]
QED

(* Theorem: a < b ==> (conjugate (a,x,b,y) = (a,x,b,y) <=> (x = b - a /\ y = a)) *)
(* Proof:
       conjugate (a,x,b,y) = (a,x,b,y)
   <=> a = y /\ b = x + y                      by conjugate_fix
   <=> y = a /\ x + a = b                      by y = a
   <=> y = a /\ x = b - a                      by arithmetic, a < b
*)
Theorem conjugate_fix_lt:
  !a b x y. a < b ==> (conjugate (a,x,b,y) = (a,x,b,y) <=> (x = b - a /\ y = a))
Proof
  simp[conjugate_def]
QED

(* Theorem: fixes conjugate (corners n) =
            {(a,b - a,b,a) | n = a * (2 * b - a) /\ 0 < a /\ a < b} *)
(* Proof:
   By fixes_def, EXTENSION, this is to show:
   (1) q IN corners n /\ conjugate q = q ==>
       ?a b. q = (a,b - a,b,a) /\ n = a * (2 * b - a) /\ 0 < a /\ a < b
       Note q = (a,x,b,y) /\ n = a * x + b * y /\ a < b  by corners_def
        and conjugate q = q ==> x = b - a /\ y = a       by conjugate_fix_lt
            n = a * x + b * y
              = a * (b - a) + b * a                      by above
              = a * b + a * (b - a)                      by ADD_COMM, MULT_COMM
              = a * (b + (b - a))                        by LEFT_ADD_DISTRIB
              = a * (2 * b - a)                          by arithmetic
   (2) n = a * (2 * b - a) /\ q = (a,b - a,b,a) /\ 0 < a /\ a < b ==>
       q IN corners n /\ conjugate q = q
       Note n = a * (2 * b - a)
              = a * (b + (b - a))                        by arithmetic, a < b
              = a * b + a * (b - a)                      by LEFT_ADD_DISTRIB
              = a * (b - a) + b * a                      by MULT_COMM, ADD_COMM
         so q = (a, b - a, b, a) IN corners n            by corners_def
        and conjugate q = q                              by conjugate_fix_lt
*)
Theorem corners_conjugate_fixes:
  !n a b. fixes conjugate (corners n) =
            {(a,b - a,b,a) | n = a * (2 * b - a) /\ 0 < a /\ a < b}
Proof
  rw[fixes_def, EXTENSION, EQ_IMP_THM] >| [
    rename1 `q IN _` >>
    fs[corners_def] >>
    `x = b - a /\ y = a` by rfs[conjugate_fix_lt] >>
    simp[] >>
    simp[GSYM LEFT_ADD_DISTRIB],
    simp[corners_def] >>
    simp[GSYM LEFT_ADD_DISTRIB],
    simp[conjugate_fix_lt]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Sets of corners partition.                                                *)
(* ------------------------------------------------------------------------- *)

(* Define the partitions *)
Definition corners_less_def[nocompute]:
   corners_less n = {(a,x,b,y) | (a,x,b,y) IN corners n /\ x < y}
End

Definition corners_eq_def[nocompute]:
   corners_eq n = {(a,x,b,y) | (a,x,b,y) IN corners n /\ x = y}
End

Definition corners_more_def[nocompute]:
   corners_more n = {(a,x,b,y) | (a,x,b,y) IN corners n /\ y < x}
End

(* Theorem: (a,x,b,y) IN corners_less n <=> (a,x,b,y) IN corners n /\ x < y *)
(* Proof: by corners_less_def. *)
Theorem corners_less_element:
  !n a x b y. (a,x,b,y) IN corners_less n <=> (a,x,b,y) IN corners n /\ x < y
Proof
  simp[corners_less_def]
QED

(* Theorem: corners_less n SUBSET corners n *)
(* Proof: true by corners_less_def, SUBSET_DEF. *)
Theorem corners_less_subset:
  !n. corners_less n SUBSET corners n
Proof
  rw[corners_less_def, SUBSET_DEF] >>
  fs[]
QED

(* Theorem: FINITE (corners_less n) *)
(* Proof:
   Note corners_less n SUBSET corners n        by corners_less_subset
    and FINITE (corners n)                     by corners_finite
    ==> FINITE (corners_less n)                by SUBSET_FINITE
*)
Theorem corners_less_finite:
  !n. FINITE (corners_less n)
Proof
  metis_tac[corners_less_subset, corners_finite, SUBSET_FINITE]
QED

(* Theorem: (a,x,b,y) IN corners_eq n <=> (a,x,b,y) IN corners n /\ x = y *)
(* Proof: by corners_eq_def. *)
Theorem corners_eq_element:
  !n a x b y. (a,x,b,y) IN corners_eq n <=> (a,x,b,y) IN corners n /\ x = y
Proof
  simp[corners_eq_def] >>
  metis_tac[]
QED

(* Theorem: corners_eq n SUBSET corners n *)
(* Proof: true by corners_eq_def, SUBSET_DEF. *)
Theorem corners_eq_subset:
  !n. corners_eq n SUBSET corners n
Proof
  rw[corners_eq_def, SUBSET_DEF] >>
  fs[]
QED

(* Theorem: FINITE (corners_eq n) *)
(* Proof:
   Note corners_eq n SUBSET corners n          by corners_eq_subset
    and FINITE (corners n)                     by corners_finite
    ==> FINITE (corners_eq n)                  by SUBSET_FINITE
*)
Theorem corners_eq_finite:
  !n. FINITE (corners_eq n)
Proof
  metis_tac[corners_eq_subset, corners_finite, SUBSET_FINITE]
QED

(* Theorem: (a,x,b,y) IN corners_more n <=> (a,x,b,y) IN corners n /\ y < x *)
(* Proof: by corners_more_def. *)
Theorem corners_more_element:
  !n a x b y. (a,x,b,y) IN corners_more n <=> (a,x,b,y) IN corners n /\ y < x
Proof
  simp[corners_more_def]
QED

(* Theorem: corners_more n SUBSET corners n *)
(* Proof: true by corners_more_def, SUBSET_DEF. *)
Theorem corners_more_subset:
  !n. corners_more n SUBSET corners n
Proof
  rw[corners_more_def, SUBSET_DEF] >>
  fs[]
QED

(* Theorem: FINITE (corners_more n) *)
(* Proof:
   Note corners_more n SUBSET corners n        by corners_more_subset
    and FINITE (corners n)                     by corners_finite
    ==> FINITE (corners_more n)                by SUBSET_FINITE
*)
Theorem corners_more_finite:
  !n. FINITE (corners_more n)
Proof
  metis_tac[corners_more_subset, corners_finite, SUBSET_FINITE]
QED

(* Theorem: corners_eq n = {(a,x,b,x) | n = (a + b) * x /\ 0 < a /\ a < b /\ 0 < x} *)
(* Proof:
   By EXTENSION, this is to show:
   (1) q IN corners_eq n ==> ?a b x. q = (a,x,b,x) /\ n = (a + b) * x /\ 0 < a /\ a < b /\ 0 < x
       Note ?a b x. q = (a,x,b,y) /\
                    q IN corners n /\ y = x    by corners_eq_def
        and n = a * x + b * x, 0 < a, a < b    by corners_def
              = (a + b) * x                    by RIGHT_ADD_DISTRIB
   (2) n = (a + b) * x ==> (a,x,b,x) IN corners_eq n
       Note n = (a + b) * x = a * x + b * x    by RIGHT_ADD_DISTRIB
        ==> (a,x,b,x) IN corners n             by corners_def
        ==> (a,x,b,x) IN corners_eq n          by corners_eq_def
*)
Theorem corners_eq_alt:
  !n a b x. corners_eq n = {(a,x,b,x) | n = (a + b) * x /\ 0 < a /\ a < b /\ 0 < x}
Proof
  rw[EXTENSION, EQ_IMP_THM] >-
  fs[corners_eq_def, corners_def] >>
  simp[corners_eq_def, corners_def]
QED

(* Theorem: corners_less n =
            {(a,x,b,y) | n = a * x + b * y /\ 0 < a /\ a < b /\ 0 < x /\ x < y} *)
(* Proof:
   By EXTENSION,
       (a,x,b,y) IN corners_less n
   <=> (a,x,b,y) IN corners n /\ x < y         by corners_less_def
   <=> n = a * x + b * y /\ 0 < a /\ a < b /\
       0 < x /\ 0 < y /\ x < y                 by corners_def
   <=> n = a * x + b * y /\ 0 < a /\ a < b /\
       0 < x /\ x < y                          by LESS_TRANS
*)
Theorem corners_less_alt:
  !n a b x y. corners_less n =
      {(a,x,b,y) | n = a * x + b * y /\ 0 < a /\ a < b /\ 0 < x /\ x < y}
Proof
  rw[corners_less_def, corners_def, EXTENSION, EQ_IMP_THM]
QED

(* Theorem: corners_more n =
            {(a,x,b,y) | n = a * x + b * y /\ 0 < a /\ a < b /\ 0 < y /\ y < x} *)
(* Proof:
   By EXTENSION,
       (a,x,b,y) IN corners_more n
   <=> (a,x,b,y) IN corners n /\ y < x         by corners_more_def
   <=> n = a * x + b * y /\ 0 < a /\ a < b /\
       0 < x /\ 0 < y /\ y < x                 by corners_def
   <=> n = a * x + b * y /\ 0 < a /\ a < b /\
       0 < y /\ y < x                          by LESS_TRANS
*)
Theorem corners_more_alt:
  !n a b x y. corners_more n =
      {(a,x,b,y) | n = a * x + b * y /\ 0 < a /\ a < b /\ 0 < y /\ y < x}
Proof
  rw[corners_more_def, corners_def, EXTENSION, EQ_IMP_THM]
QED

(* Theorem: corners n = BIGUNION {(corners_less n); (corners_eq n); (corners_more n)} *)
(* Proof:
   By EXTENSION, IN_BIGUNION, this is to show:
   (1) q IN corners n ==> q IN (corners_less n) \/ q IN (corners_eq n) \/ q IN (corners_more n)
       Note ?a x b y. q = (a,x,b,y)            by quadruples_parts
       By num_nchotomy, there are 3 cases:
       (a) x < y ==> q IN (corners_less n)     by corners_less_def
       (b) x = y ==> q IN (corners_eq n)       by corners_eq_def
       (c) y < x ==> q IN (corners_more n)     by corners_more_def
   (2) q IN corners_less n \/ q IN corners_eq n \/ q IN corners_more n ==> q IN corners n
       Note ?a x b y. q = (a,x,b,y)            by quadruples_parts
       Hence true                              by corners_less_element,
                                                  corners_eq_element,
                                                  corners_more_element.
*)
Theorem corners_partiton_thm:
  !n. corners n = BIGUNION {(corners_less n); (corners_eq n); (corners_more n)}
Proof
  simp[] >>
  simp[Once EXTENSION, Once EQ_IMP_THM] >>
  ntac 3 strip_tac >| [
    strip_tac >>
    rename1 `q IN _` >>
    `?a x b y. q = (a,x,b,y)` by simp[quadruples_parts] >>
    `x < y \/ x = y \/ y < x` by simp[num_nchotomy] >-
    fs[corners_less_def] >-
    fs[corners_eq_def] >>
    fs[corners_more_def],
    metis_tac[quadruples_parts, corners_less_element, corners_eq_element, corners_more_element]
  ]
QED

(* Theorem: CARD (corners n) =
            CARD (corners_less n) + CARD (corners_eq n) + CARD (corners_more n) *)
(* Proof:
   Let P = {(corners_less n); (corners_eq n); (corners_more n)}.
   Then corners n = BIGUNION P                 by corners_partiton_thm
   Note FINITE (corners n)                     by corners_finite
    ==> FINITE P /\ EVERY_FINITE P             by FINITE_BIGUNION_EQ
    and PAIR_DISJOINT P                        by DISJOINT_ALT, definitions
     so   CARD (corners n)
        = CARD (BIGUNION P)                    by corners_partiton_thm
        = SIGMA CARD P                         by disjoint_bigunion_card
   Let a = corners_less n,
       b = corners_eq n,
       c = corners_more n.
   To show: SIGMA CARD P = CARD a + CARD b + CARD c.
   If a = b,
      Then a = {}                              by corners_less_def, corners_eq_def
       and CARD a = CARD b = 0                 by CARD_EMPTY
        so corners n = c                       by BIGUNION P = {} UNION {} UNION c
       and the sum follows.
   If b = c,
      Then b = {}                              by corners_more_def, corners_eq_def
       and CARD b = CARD c = 0                 by CARD_EMPTY
        so corners n = a                       by BIGUNION P = a UNION {} UNION {}
       and the sum follows.
   If c = a,
      Then c = {}                              by corners_more_def, corners_less_def
       and CARD c = CARD a = 0                 by CARD_EMPTY
        so corners n = b                       by BIGUNION P = {} UNION b UNION {}
       and the sum follows.
   Otherwise, a <> b, b <> c, c <> a.
       Then the sum follows                    by SUM_IMAGE_TRIPLET
*)
Theorem corners_card_eqn:
  !n. CARD (corners n) = CARD (corners_less n) + CARD (corners_eq n) + CARD (corners_more n)
Proof
  rpt strip_tac >>
  qabbrev_tac `P = {(corners_less n); (corners_eq n); (corners_more n)}` >>
  `corners n = BIGUNION P` by simp[corners_partiton_thm, Abbr`P`] >>
  `FINITE P /\ EVERY_FINITE P` by metis_tac[corners_finite, FINITE_BIGUNION_EQ] >>
  `PAIR_DISJOINT P` by
  ((rw[DISJOINT_ALT, Abbr`P`] >> fs[corners_less_def, corners_eq_def, corners_more_def])) >>
  imp_res_tac disjoint_bigunion_card >>
  qabbrev_tac `a = corners_less n` >>
  qabbrev_tac `b = corners_eq n` >>
  qabbrev_tac `c = corners_more n` >>
  Cases_on `a = b` >| [
    `a = {}` by metis_tac[quadruples_parts, corners_less_element, corners_eq_element, MEMBER_NOT_EMPTY, DECIDE``x < y /\ x = y ==> F``] >>
    `corners n = c` by fs[Abbr`P`] >>
    fs[],
    Cases_on `b = c` >| [
      `b = {}` by metis_tac[quadruples_parts, corners_more_element, corners_eq_element, MEMBER_NOT_EMPTY, DECIDE``y < x /\ x = y ==> F``] >>
      `corners n = a` by fs[Abbr`P`] >>
      fs[],
      Cases_on `c = a` >| [
        `c = {}` by metis_tac[quadruples_parts, corners_more_element, corners_less_element, MEMBER_NOT_EMPTY, DECIDE``y < x /\ x < y ==> F``] >>
        `corners n = b` by fs[Abbr`P`] >>
        fs[],
        fs[SUM_IMAGE_TRIPLET, Abbr`P`]
      ]
    ]
  ]
QED
(* This is really good! *)

(* ------------------------------------------------------------------------- *)
(* Involutions on corner partitions.                                         *)
(* ------------------------------------------------------------------------- *)

(* Define two involutions. *)
Definition pair_invert_def:
   pair_invert (a:num,x:num,b:num,y:num) = (x,a,y,b)
End

Definition pair_swap_def:
   pair_swap (a:num,x:num,b:num,y:num) = (y,b,x,a)
End

(* Theorem: pair_invert (pair_invert (a,x,b,y)) = (a,x,b,y) *)
(* Proof:
     pair_invert (pair_invert (a,x,b,y))
   = pair_invert (x,a,y,b)         by pair_invert_def
   = (a,x,b,y)                     by pair_invert_def
*)
Theorem pair_invert_involute:
  !a x b y.pair_invert (pair_invert (a,x,b,y)) = (a,x,b,y)
Proof
  simp[pair_invert_def]
QED

(* Theorem: pair_swap (pair_swap (a,x,b,y)) = (a,x,b,y) *)
(* Proof:
     pair_swap (pair_swap (a,x,b,y))
   = pair_swap (y,b,x,a)           by pair_swap_def
   = (a,x,b,y)                     by pair_swap_def
*)
Theorem pair_swap_involute:
  !a x b y. pair_swap (pair_swap (a,x,b,y)) = (a,x,b,y)
Proof
  simp[pair_swap_def]
QED

(* Theorem: pair_invert involute (corners_less n) *)
(* Proof:
   Note q = (a,x,b,y) with a < b, x < y        by corners_less_alt
     so pair_invert q = (x,a,y,b)              by pair_invert_def
    and x * a + y * b
      = a * x + b * y                          by MULT_COMM
     so pair_invert q IN corners_less n        by corners_less_alt
    and pair_invert (pair_invert q) = q        by pair_invert_involute

*)
Theorem pair_invert_involute_corners_less:
  !n. pair_invert involute (corners_less n)
Proof
  ntac 3 strip_tac >>
  rename1 `q IN _` >>
  fs[corners_less_alt] >>
  rpt strip_tac >| [
    map_every qexists_tac [`x`, `a`, `y`, `b`] >>
    simp[pair_invert_def],
    simp[pair_invert_involute]
  ]
QED

(* Theorem: pair_swap involute (corners_more n) *)
(* Proof:
   Note q = (a,x,b,y) with a < b, y < x        by corners_more_alt
     so pair_swap q = (y,b,x,a)                by pair_swap_def
    and y * b + x * a
      = x * a + y * b                          by ADD_COMM
      = a * x + b * y                          by MULT_COMM
     so pair_swap q IN corners_more n          by corners_more_alt
    and pair_swap (pair_swap q) = q            by pair_swap_involute

*)
Theorem pair_swap_involute_corners_more:
  !n. pair_swap involute (corners_more n)
Proof
  ntac 3 strip_tac >>
  rename1 `q IN _` >>
  fs[corners_more_alt] >>
  rpt strip_tac >| [
    map_every qexists_tac [`y`, `b`, `x`, `a`] >>
    simp[pair_swap_def],
    simp[pair_swap_involute]
  ]
QED

(* Theorem: pair_invert (a,x,b,y) = (a,x,b,y) <=> x = a /\ y = b *)
(* Proof: by pair_invert_def. *)
Theorem pair_invert_fix:
  !a x b y. pair_invert (a,x,b,y) = (a,x,b,y) <=> x = a /\ y = b
Proof
  simp[pair_invert_def]
QED

(* Theorem: pair_swap (a,x,b,y) = (a,x,b,y) <=> x = b /\ y = a *)
(* Proof: by pair_swap_def. *)
Theorem pair_swap_fix:
  !a x b y. pair_swap (a,x,b,y) = (a,x,b,y) <=> x = b /\ y = a
Proof
  simp[pair_swap_def]
QED

(* Theorem: fixes pair_invert (corners_less n) = {(a,a,b,b) | n = a ** 2 + b ** 2 /\ 0 < a /\ a < b} *)
(* Proof:
   By fixes_def, EXTENSION, this is to show:
   (1) q IN corners_less n /\ pair_invert q = q ==>
       ?a b. q = (a,a,b,b) /\ n = a ** 2 + b ** 2 /\ 0 < a /\ a < b
       Note q = (a,x,b,y) /\ n = a * x + b * y /\ x < y      by corners_less_alt
        and x = a /\ y = b                                   by pair_invert_fix
         so n = a * a + b * b = a ** 2 + b ** 2              by EXP_2
   (2) n = a ** 2 + b ** 2 /\ 0 < a /\ a < b /\ q = (a,a,b,b) ==>
       q IN corners_less n /\ pair_invert q = q
       Note n = a ** 2 + b ** 2 = a * a + b * b              by EXP_2
         so q = (a,a,b,b) IN corners_less n                  by corners_less_alt
        and pair_invert q = q                                by pair_invert_fix
*)
Theorem corners_less_pair_invert_fixes:
  !n a b. fixes pair_invert (corners_less n) = {(a,a,b,b) | n = a ** 2 + b ** 2 /\ 0 < a /\ a < b}
Proof
  rw[fixes_def, EXTENSION, EQ_IMP_THM] >| [
    rename1 `q IN _` >>
    fs[corners_less_alt] >>
    rfs[pair_invert_fix],
    rw[corners_less_alt],
    simp[pair_invert_fix]
  ]
QED

(* Theorem: fixes pair_swap (corners_more n) = {(a,b,b,a) | n = 2 * a * b /\ 0 < a /\ a < b} *)
(* Proof:
   By fixes_def, EXTENSION, this is to show:
   (1) q IN corners_more n /\ pair_swap q = q ==>
       ?a b. q = (a,b,b,a) /\ n = 2 * a * b /\ 0 < a /\ a < b
       Note q = (a,x,b,y) /\ n = a * x + b * y /\ y < x      by corners_more_alt
        and x = b /\ y = a                                   by pair_swap_fix
         so n = a * b + b * a = 2 * a * b                    by arithmetic
   (2) n = 2 * a * b /\ 0 < a /\ a < b /\ q = (a,b,b,a) ==>
       q IN corners_more n /\ pair_swap q = q
       Note n = 2 * a * b = a * b + b * a                    by arithmetic
         so q = (a,b,b,a) IN corners_more n                  by corners_more_alt
        and pair_swap q = q                                  by pair_swap_fix
*)
Theorem corners_more_pair_swap_fixes:
  !n a b. fixes pair_swap (corners_more n) = {(a,b,b,a) | n = 2 * a * b /\ 0 < a /\ a < b}
Proof
  rw[fixes_def, EXTENSION, EQ_IMP_THM] >| [
    rename1 `q IN _` >>
    fs[corners_more_alt] >>
    rfs[pair_swap_fix],
    rw[corners_more_alt],
    simp[pair_swap_fix]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Fermat's Two Squares Theorem.                                             *)
(* ------------------------------------------------------------------------- *)

(* Theorem: prime p ==> corners_eq p = {(a,1,b,1) | p = a + b /\ 0 < a /\ a < b} *)
(* Proof:
   By corners_eq_alt, EXTENSION, this is to show:
      p = (a + b) * x ==> x = 1
   By contradiction, suppose x <> 1.
   Then x divides p                by divides_def
    ==> x = p                      by prime_def, x <> 1
   Note 0 < p                      by PRIME_POS
     so a + b = 1                  by MULT_EQ_SELF
   This contradicts 0 < a, 0 < b   by arithmetic
*)
Theorem prime_corners_eq:
  !p. prime p ==> corners_eq p = {(a,1,b,1) | p = a + b /\ 0 < a /\ a < b}
Proof
  rw[corners_eq_alt, EXTENSION, EQ_IMP_THM] >>
  rename1 `x = _` >>
  qabbrev_tac `p = x * (a + b)` >>
  spose_not_then strip_assume_tac >>
  `x divides p` by fs[divides_def, Abbr`p`] >>
  `x = p` by metis_tac[prime_def] >>
  `0 < p` by simp[PRIME_POS] >>
  `a + b = 1` by fs[Abbr`p`] >>
  decide_tac
QED

(* Theorem: prime p ==>
            corners_eq p = IMAGE (\a. (a,1,p - a, 1)) (natural (HALF (p - 1))) *)
(* Proof:
   Let s = {(a,1,b,1) | p = a + b /\ 0 < a /\ a < b},
       t = IMAGE (\a. (a,1,p - a, 1)) (natural (HALF (p - 1)))
   Note corners_eq p = s                       by prime_corners_eq
   Thus only need to show: s = t
   By EXTENSION, this is to show:
   (1) (a,1,b,1) IN s ==> (a,1,b,1) IN t
       By IN_IMAGE, this is to show:
          p = a + b /\ 0 < a /\ a < b ==> ?x. a = SUC x /\ x < HALF (p - 1)
       Note ?k. a = SUC k                      by SUC_EXISTS, 0 < a
                  = k + 1                      by ADD1
        Now 0 < p                              by PRIME_POS
        and a + a < a + b = p                  by LT_ADD_LCANCEL
         or (k + 1) + (k + 1) < p              by above
         or   2 * k + 1 < p - 1                by arithmetic
        ==>           k < HALF (p - 1)         by HALF_ODD_LT
        Let x = k, then x < HALF (p - 1).
   (2) (a,1,b,1) IN t ==> (a,1,b,1) IN s
       By IN_IMAGE, this is to show:
          ?a. 0 < a /\ a <= HALF (p - 1) ==> p = a + (p - a) /\ a < p - a
       Note 1 < p                              by ONE_LT_PRIME
         so 0 < p - 1                          by inequality
        and ?q. p - 1 = SUC q                  by SUC_EXISTS, 0 < p - 1
        and ?k. a = SUC k                      by SUC_EXISTS, 0 < a
        Now     k < HALF (SUC q)               by SUC k <= HALF (p - 1)
        ==> 2 * k + 1 <= q                     by HALF_SUC_LE
         or 2 * k + 2 <= q + 1 = p - 1         by ADD1
         or      2 * a < p                     by a = SUC k
         or      a + a < p                     by arithmetic
         so a < p - a, and p = a + (p - a).
*)
Theorem prime_corners_eq_eqn:
  !p. prime p ==> corners_eq p = IMAGE (\a. (a,1,p - a, 1)) (natural (HALF (p - 1)))
Proof
  rpt strip_tac >>
  `{(a,1,b,1) | p = a + b /\ 0 < a /\ a < b} = IMAGE (\a. (a,1,p - a, 1)) (natural (HALF (p - 1)))` suffices_by simp[prime_corners_eq] >>
  rw[EXTENSION, EQ_IMP_THM] >| [
    `?k. a = SUC k` by simp[SUC_EXISTS] >>
    `a + a < a + b` by fs[] >>
    qabbrev_tac `p = a + b` >>
    `0 < p` by fs[PRIME_POS] >>
    `2 * k + 1 < p - 1` by decide_tac >>
    metis_tac[HALF_ODD_LT],
    rename1 `k < _` >>
    `1 < p` by simp[ONE_LT_PRIME] >>
    `0 < p - 1` by decide_tac >>
    `?q. p - 1 = SUC q` by simp[SUC_EXISTS] >>
    `2 * k + 1 <= q` by fs[HALF_SUC_LE] >>
    `2 * SUC k < p` by decide_tac >>
    qabbrev_tac `a = SUC k` >>
    decide_tac
  ]
QED

(* Theorem: prime p ==> CARD (corners_eq p) = HALF (p - 1) *)
(* Proof:
   Let s = natural (HALF (p - 1)),
       t = univ(:num # num # num # num),
       f = (\a. (a,1,p - a,1)).
    Note FINITE s                  by natural_finite
     and INJ f s t                 by INJ_DEF, equating components
         CARD (corners_eq p)
       = CARD (IMAGE f s)          by prime_corners_eq_eqn
       = CARD s                    by INJ_CARD_IMAGE
       = HALF (p - 1)              by natural_card
*)
Theorem prime_corners_eq_card:
  !p. prime p ==> CARD (corners_eq p) = HALF (p - 1)
Proof
  rpt strip_tac >>
  imp_res_tac prime_corners_eq_eqn >>
  qabbrev_tac `s = natural (HALF (p - 1))` >>
  qabbrev_tac `f = \a. (a,1,p - a,1)` >>
  `FINITE s` by simp[natural_finite, Abbr`s`] >>
  `INJ f s univ(:num # num # num # num)` by rw[INJ_DEF, Abbr`f`] >>
  metis_tac[INJ_CARD_IMAGE, natural_card]
QED

(* Theorem: prime p ==> fixes conjugate (corners p) =
            if p = 2 then {} else {(1, HALF (p - 1), HALF (p + 1),1)} *)
(* Proof:
   If p = 2,
      Then corners 2 = {}                      by corners_empty, 2 <= p
        so fixes conjugate {} = {}             by fixes_empty
   Otherwise p <> 2.
   Let s = {(a,b - a,b,a) | p = a * (2 * b - a) /\ 0 < a /\ a < b}.
   Then fixes conjugate (corners n) = s        by corners_conjugate_fixes
   The goal is to show:
       s = {(1, HALF (p - 1), HALF (p + 1),1)}
   By EXTENSION, this is to show:
   (1) p = a * (2 * b - a) /\ 0 < a /\ a < b ==>
       a = 1, b - 1 = HALF (p - 1), b = HALF (p + 1)
       Note 2 * b - a = b + (b - a)            by arithmetic
         so 2 * b - a <> 1                     by 0 < a, a < b
        But (2 * b - a) divides p              by divides_def
         so 2 * b - a = p                      by prime_def
        ==> a = 1                              by MULT_EQ_SELF
       Thus  2 * b = p + 1                     by arithmetic
        and  2 * (b - 1) = p - 1               by LEFT_SUB_DISTRIB
        ==> b = HALF (p + 1) /\                by HALF_TWICE
            b - 1 = HALF (p - 1)               by HALF_TWICE
   (2) a = 1 /\ b = HALF (p + 1) ==> p = a * (2 * b - a) /\ a < b
       This is to show:
            p = 2 * HALF (p + 1) - 1           by putting a = 1
        and 1 < HALF (p + 1)                   by putting b = HALF (p + 1)
       Note ~EVEN p                            by EVEN_PRIME, p <> 2
         so ODD p                              by ODD_EVEN
       thus ?k. p = 2 * k + 1                  by ODD_EXISTS
       and   HALF (p + 1)
           = HALF (2 * k + 2)                  by arithmetic
           = HALF (2 * (k + 1))                by LEFT_ADD_DISTRIB
           = k + 1                             by HALF_TWICE
        so   2 * HALF (p + 1) - 1
           = 2 * (k + 1) - 1                   by above
           = 2 * k + 1                         by LEFT_ADD_DISTRIB
           = p
      Also p <> 1                              by NOT_PRIME_1
        so k <> 0                              by p = 2 * k + 1
       ==> 1 < k + 1 = HALF (p + 1)            by inequality
*)
Theorem prime_corners_conjugate_fixes:
  !p. prime p ==> fixes conjugate (corners p) =
                    if p = 2 then {} else {(1, HALF (p - 1), HALF (p + 1),1)}
Proof
  rpt strip_tac >>
  Cases_on `p = 2` >-
  metis_tac[corners_empty, fixes_empty, LESS_EQ_REFL] >>
  qabbrev_tac `s = {(a,b - a,b,a) | p = a * (2 * b - a) /\ 0 < a /\ a < b}` >>
  `s = {(1,HALF (p - 1),HALF (p + 1),1)}` suffices_by simp[corners_conjugate_fixes, Abbr`s`] >>
  simp[Abbr`s`, EXTENSION, EQ_IMP_THM] >>
  ntac 3 strip_tac >| [
    simp[] >>
    `2 * b - a <> 1` by decide_tac >>
    `2 * b - a = p` by metis_tac[divides_def, prime_def] >>
    `a = 1` by fs[] >>
    `2 * b = p + 1` by decide_tac >>
    `2 * (b - 1) = p - 1` by decide_tac >>
    metis_tac[HALF_TWICE],
    `ODD p` by metis_tac[EVEN_PRIME, ODD_EVEN] >>
    `?k. p = 2 * k + 1` by metis_tac[ODD_EXISTS, ADD1] >>
    `p - 1 = 2 * k /\ p + 1 = 2 * (k + 1)` by decide_tac >>
    `k <> 0` by metis_tac[MULT_0, ADD, NOT_PRIME_1] >>
    simp[HALF_TWICE]
  ]
QED

(* Theorem: prime p ==> fixes pair_swap (corners_more p) = {} *)
(* Proof:
   Let s = {(a,b,b,a) | p = 2 * a * b /\ 0 < a /\ a < b}.
   Then fixes pair_swap (corners_more p) = s   by corners_more_pair_swap_fixes
   Goal is to show: s = {}
   By contradiction, suppose s <> {}.
   Then ?q. q = (a,b,b,a) /\
            p = 2 * a * b /\ 0 < a /\ a < b    by MEMBER_NOT_EMPTY
    ==> EVEN p                                 by EVEN_DOUBLE
     so p = 2                                  by EVEN_PRIME
    ==> a * b = 1                              by MULT_EQ_SELF
     so a = 1 /\ b = 1                         by MULT_EQ_1
    This contradicts a < b                     by LESS_REFL
*)
Theorem prime_corners_more_pair_swap_fixes:
  !p. prime p ==> fixes pair_swap (corners_more p) = {}
Proof
  rpt strip_tac >>
  `{(a,b,b,a) | p = 2 * a * b /\ 0 < a /\ a < b} = {}` suffices_by simp[corners_more_pair_swap_fixes] >>
  spose_not_then strip_assume_tac >>
  fs[EXTENSION] >>
  `p = 2` by fs[EVEN_DOUBLE, GSYM EVEN_PRIME] >>
  `a = 1 /\ b = 1` by fs[] >>
  decide_tac
QED

(* Theorem: tik p /\ prime p ==> fixes pair_invert (corners_less p) <> {} *)
(* Proof:
   Let s = corners p,
       u = corners_less p,
       v = corners_eq p,
       w = corners_more p.
   By contradiction, assume fixes pair_invert u = {}.

   Claim: ODD (CARD s)
   Proof: Note ODD p                           by odd_tik_tok
            so p <> 2                          by EVEN_2, ODD_EVEN
           ==> CARD (fixes conjugate s) = 1    by prime_corners_conjugate_fixes, CARD_SING
            or ODD (CARD (fixes conjugate s))  by ODD_1
           Now FINITE s                        by corners_finite
           and conjugate involute s            by conjugate_involute_corners
           ==> ODD (CARD s)                    by involute_set_fixes_both_odd, [1]

   Claim: EVEN (CARD w)
   Proof: Note CARD (fixes pair_swap w) = 0    by prime_corners_more_pair_swap_fixes, CARD_EMPTY
            so EVEN (CARD (fixes pair_swap w)) by EVEN_0
           Now FINITE w                        by corners_more_finite
           and pair_swap involute w            by pair_swap_involute_corners_more
           ==> EVEN (CARD w)                   by involute_set_fixes_both_even, [2]

   Claim: EVEN (CARD v)
   Proof: Note ?k. p = 4 * k + 1               by tik_exists
           and CARD v = HALF (p - 1)           by prime_corners_eq_card, prime p
                      = HALF (4 * k)           by above
                      = HALF (2 * (2 * k))     by arithmetic
                      = 2 * k                  by HALF_TWICE
          ==> EVEN (CARD v)                    by EVEN_DOUBLE, [3]

   Note CARD s = CARD u + CARD v + CARD w      by corners_card_eqn
   With ODD (CARD s)                           by [1]
    and EVEN (CARD v + CARD w)                 by EVEN_ADD, [2],[3]
    ==> ODD (CARD u)                           by ODD_ADD, ODD_EVEN
    Now FINITE u                               by corners_less_finite
    and pair_invert involute u                 by pair_invert_involute_corners_less
    ==> ODD (CARD (fixes pair_invert u))       by involute_set_fixes_both_odd
    But CARD (fixes pair_invert u) = 0         by CARD_EMPTY, assumption
    This is a contradiction, as ODD 0 = F      by ODD
*)
Theorem tik_prime_corners_less_pair_invert_fixes:
  !p. tik p /\ prime p ==> fixes pair_invert (corners_less p) <> {}
Proof
  rpt strip_tac >>
  qabbrev_tac `s = corners p` >>
  qabbrev_tac `u = corners_less p` >>
  qabbrev_tac `v = corners_eq p` >>
  qabbrev_tac `w = corners_more p` >>
  `ODD (CARD s)` by
  (`p <> 2` by metis_tac[odd_tik_tok, EVEN_2, ODD_EVEN] >>
  `CARD (fixes conjugate s) = 1` by fs[prime_corners_conjugate_fixes, Abbr`s`] >>
  `ODD (CARD (fixes conjugate s))` by simp[ODD_1] >>
  metis_tac[corners_finite, conjugate_involute_corners, involute_set_fixes_both_odd]) >>
  `EVEN (CARD w)` by
    (`CARD (fixes pair_swap w) = 0` by rw[prime_corners_more_pair_swap_fixes, Abbr`w`] >>
  `EVEN (CARD (fixes pair_swap w))` by simp[EVEN_0] >>
  metis_tac[corners_more_finite, pair_swap_involute_corners_more, involute_set_fixes_both_even]) >>
  ` EVEN (CARD v)` by
      (`?k. p = 4 * k + 1` by simp[tik_exists] >>
  `p - 1 = 2 * (2 * k)` by decide_tac >>
  `HALF (p - 1) = 2 * k` by metis_tac[HALF_TWICE] >>
  fs[prime_corners_eq_card, EVEN_DOUBLE, Abbr`v`]) >>
  `EVEN (CARD v + CARD w)` by simp[EVEN_ADD] >>
  `CARD s = CARD u + (CARD v + CARD w)` by simp[corners_card_eqn, Abbr`s`, Abbr`u`, Abbr`v`, Abbr`w`] >>
  `ODD (CARD u)` by metis_tac[ODD_ADD, ODD_EVEN] >>
  `ODD (CARD (fixes pair_invert u))` by metis_tac[corners_less_finite, pair_invert_involute_corners_less, involute_set_fixes_both_odd] >>
  rfs[ODD]
QED

(* Theorem: tik p /\ prime p ==> ?(a, b). p = a ** 2 + b ** 2 /\ a < b *)
(* Proof:
   Let s = fixes pair_invert (corners_less p).
   Note s <> {}                                by tik_prime_corners_less_pair_invert_fixes
    ==> ?q. q IN s                             by MEMBER_NOT_EMPTY
    ==> ?a b. q = (a,a,b,b) /\
              p = a ** 2 + b ** 2 /\ a < b     by corners_less_pair_invert_fixes
   Take them and form a pair (a,b).
*)
Theorem fermat_two_squares_by_corners:
  !p. tik p /\ prime p ==> ?(a, b). p = a ** 2 + b ** 2 /\ a < b
Proof
  rpt strip_tac >>
  imp_res_tac tik_prime_corners_less_pair_invert_fixes >>
  fs[corners_less_pair_invert_fixes, EXTENSION] >>
  rw[ELIM_UNCURRY] >>
  qexists_tac `(a,b)` >>
  simp[]
QED


(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
