(* ------------------------------------------------------------------------- *)
(* Node Hopping Algorithm (new version).                                     *)
(* ------------------------------------------------------------------------- *)

(*===========================================================================*)

(* add all dependent libraries for script *)
open HolKernel boolLib bossLib Parse;

(* declare new theory at start *)
val _ = new_theory "part8";

(* ------------------------------------------------------------------------- *)


(* open dependent theories *)
(* arithmeticTheory -- load by default *)

(* val _ = load "quarityTheory"; *)
open helperTwosqTheory;
open helperNumTheory;
open helperSetTheory;
open helperFunctionTheory;
open arithmeticTheory pred_setTheory;
open dividesTheory; (* for divides_def, prime_def *)
open logPowerTheory; (* for square_alt *)

open listTheory rich_listTheory;
open helperListTheory;
open listRangeTheory; (* for listRangeLHI_ALL_DISTINCT *)
open indexedListsTheory; (* for findi_EL and EL_findi *)

load "sublistTheory";
open sublistTheory;

open quarityTheory;
open pairTheory;

(* val _ = load "twoSquaresTheory"; *)
open windmillTheory;
open involuteTheory;
open involuteFixTheory;
open iterationTheory; (* for iterate_period_pos *)
open iterateComposeTheory;
open iterateComputeTheory; (* for iterate_while_thm *)
open twoSquaresTheory; (* for loop test found *)


(* ------------------------------------------------------------------------- *)
(* Node Hopping Algorithm Documentation                                      *)
(* ------------------------------------------------------------------------- *)
(* Overloading:
*)
(* Definitions and Theorems (# are exported, ! are in compute):

   The zagier-flip permutation:
   ping_def            |- !x y z. ping (x,y,z) = (x + 2 * y,y,z - y - x)
   pong_def            |- !x y z. pong (x,y,z) = (2 * z - x,z,x + y - z)
   pung_def            |- !x y z. pung (x,y,z) = (x - 2 * z,x + y - z,z)
   ping_alt            |- !x y z. ping (x,y,z) = (x + 2 * y,y,z - (x + y))
   is_ping_def         |- !x y z. is_ping (x,y,z) <=> x < z - y
   is_pong_def         |- !x y z. is_pong (x,y,z) <=> ~(x < z - y) /\ x < 2 * z
   is_pung_def         |- !x y z. is_pung (x,y,z) <=> ~(x < z - y) /\ ~(x < 2 * z)
   triple_cases        |- !x y z. is_ping (x,y,z) \/ is_pong (x,y,z) \/ is_pung (x,y,z)
   triple_cases_alt    |- !t. is_ping t \/ is_pong t \/ is_pung t
   ping_not_pong       |- !t. is_ping t ==> ~is_pong t
   ping_not_pung       |- !t. is_ping t ==> ~is_pung t
   pong_not_ping       |- !t. is_pong t ==> ~is_ping t
   pong_not_pung       |- !t. is_pong t ==> ~is_pung t
   pung_not_ping       |- !t. is_pung t ==> ~is_ping t
   pung_not_pong       |- !t. is_pung t ==> ~is_pong t
   is_ping_alt         |- !x y z. is_ping (x,y,z) <=> SQRT (windmill (x,y,z)) < 2 * z - x
   is_pong_alt         |- !x y z. is_pong (x,y,z) <=> 0 < 2 * z - x /\ 2 * z - x <= SQRT (windmill (x,y,z))
   is_pung_alt         |- !x y z. is_pung (x,y,z) <=> 2 * z - x = 0
   is_ping_x_x_z       |- !x z. 2 * x < z ==> is_ping (x,x,z)
   is_ping_1_1_k       |- !k. 2 < k ==> is_ping (1,1,k)
   not_ping_x_y_y      |- !x y. ~is_ping (x,y,y)
   is_pong_x_y_x       |- !x y. 0 < x ==> is_pong (x,y,x)
   is_ping_by_ping     |- !x y z. (let (xx,yy,zz) = ping (x,y,z) in is_ping (x,y,z) <=> 0 < zz)
   is_pong_by_pong     |- !x y z. (let (xx,yy,zz) = pong (x,y,z) in 0 < xx /\ 0 < zz ==> is_pong (x,y,z))
   is_pung_by_pung     |- !x y z. (let (xx,yy,zz) = pung (x,y,z) in 0 < xx ==> is_pung (x,y,z))
   is_ping_by_ping_alt |- !t. (let (x,y,z) = ping t in is_ping t <=> 0 < z)
   is_pong_by_pong_alt |- !t. (let (x,y,z) = pong t in 0 < x /\ 0 < z ==> is_pong t)
   is_pung_by_pung_alt |- !t. (let (x,y,z) = pung t in 0 < x ==> is_pung t)

   Zagier and Flip composition:
   zagier_flip_thm         |- !x y z. (zagier o flip) (x,y,z) =
                                      if is_ping (x,y,z) then ping (x,y,z)
                                      else if is_pong (x,y,z) then pong (x,y,z)
                                      else pung (x,y,z)
   zagier_flip_alt         |- !t. (zagier o flip) t =
                                  if is_ping t then ping t else if is_pong t then pong t else pung t
   zagier_flip_ping        |- !t. is_ping t ==> (zagier o flip) t = ping t
   zagier_flip_pong        |- !t. is_pong t ==> (zagier o flip) t = pong t
   zagier_flip_pung        |- !t. is_pung t ==> (zagier o flip) t = pung t
   zagier_flip_ping_funpow |- !m x y z. (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
                                        FUNPOW (zagier o flip) m (x,y,z) = FUNPOW ping m (x,y,z)
   zagier_flip_ping_funpow_alt
                           |- !m t. (!j. j < m ==> is_ping (FUNPOW ping j t)) ==>
                                    FUNPOW (zagier o flip) m t = FUNPOW ping m t
   zagier_flip_pung_funpow |- !m x y z. (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                        FUNPOW (zagier o flip) m (x,y,z) = FUNPOW pung m (x,y,z)
   zagier_flip_pung_funpow_alt
                           |- !m t. (!j. j < m ==> is_pung (FUNPOW pung j t)) ==>
                                    FUNPOW (zagier o flip) m t = FUNPOW pung m t

   pung_next_not_ping      |- !x y z. 2 * z <= x ==> ~is_ping (pung (x,y,z))
   pung_next_not_ping_alt  |- !t. is_pung t ==> ~is_ping (pung t)
   ping_before_not_pung_alt|- !t. is_ping ((zagier o flip) t) ==> ~is_pung t

   mind_of_ping            |- !x y z. mind (ping (x,y,z)) = x + 2 * y
   mind_rise_by_ping       |- !x y z. mind (x,y,z) <= mind (ping (x,y,z))
   mind_rise_by_ping_alt   |- !t. mind t <= mind (ping t)
   mind_inc_by_ping        |- !x y z. 0 < x /\ 0 < y ==> mind (x,y,z) < mind (ping (x,y,z))
   mind_of_pung            |- !x y z. 2 * z <= x ==> mind (pung (x,y,z)) = x
   mind_fall_by_pung       |- !x y z. 2 * y <= x ==> mind (pung (x,y,z)) <= mind (x,y,z)
   mind_fall_by_pung_alt   |- !t. is_pung t ==> mind (pung t) <= mind t

   Path on principal orbit of (zagier o flip) iteration:
   path_def                |- (!n. path n 0 = [(1,n DIV 4,1)]) /\
                              !n k. path n (SUC k) = SNOC ((zagier o flip) (LAST (path n k))) (path n k)
   path_0                  |- !n. path n 0 = [(1,n DIV 4,1)]
   path_suc                |- !n k. path n (SUC k) = SNOC ((zagier o flip) (LAST (path n k))) (path n k)
   path_1                  |- !n. path n 1 = [(1,n DIV 4,1); (1,1,n DIV 4)]
   path_not_nil            |- !n k. path n k <> []
   path_length             |- !n k. LENGTH (path n k) = k + 1
   path_head               |- !n k. HD (path n k) = (1,n DIV 4,1)
   path_last               |- !n k. LAST (path n k) = FUNPOW (zagier o flip) k (1,n DIV 4,1)
   path_head_alt           |- !n k. HD (path n k) = EL 0 (path n k)
   path_last_alt           |- !n k. LAST (path n k) = EL k (path n k)
   path_tail_alt           |- !n m. TL (path n (1 + m)) = iterate_trace (1,1,n DIV 4) (zagier o flip) m
   path_tail_all_distinct  |- !n k. tik n /\ ~square n /\
                                    k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) ==>
                                    ALL_DISTINCT (TL (path n k))
   path_eq_sing            |- !n k. path n k = [(1,n DIV 4,1)] <=> k = 0
   path_front_length       |- !n k. LENGTH (FRONT (path n k)) = k
   path_front_head         |- !n k. 0 < k ==> HD (FRONT (path n k)) = (1,n DIV 4,1)
   path_element_eqn        |- !n k j. j <= k ==> EL j (path n k) = FUNPOW (zagier o flip) j (1,n DIV 4,1)
   path_element_suc        |- !n k j. j < k ==> EL (SUC j) (path n k) = (zagier o flip) (EL j (path n k))
   path_element_thm        |- !n k j h. j + h <= k ==> EL (j + h) (path n k) = FUNPOW (zagier o flip) h (EL j (path n k))
   path_element_in_mills   |- !n k j. tik n /\ j <= k ==> EL j (path n k) IN mills n
   path_element_windmill   |- !n k j x y z. tik n /\ j <= k /\ EL j (path n k) = (x,y,z) ==> n = windmill (x,y,z)
   path_element_windmill_alt
                           |- !n k j. tik n /\ j <= k ==> n = windmill (EL j (path n k))
   path_head_is_pong       |- !n k. is_pong (EL 0 (path n k))
   path_element_0          |- !n k. EL 0 (path n k) = (1,n DIV 4,1)
   path_element_1          |- !n k. 0 < k ==> EL 1 (path n k) = (1,1,n DIV 4)
   path_tail_element       |- !n k j. j < k ==> EL j (TL (path n k)) = FUNPOW (zagier o flip) j (1,1,n DIV 4)
   path_head_next_property |- !n k. 0 < k ==> (let t = EL 1 (path n k)
                                                in if n < 4 then is_pung t
                                                   else if n < 12 then is_pong t
                                                   else is_ping t)
   path_last_not_ping      |- !n k. (let ls = path n k
                                      in flip (LAST ls) = LAST ls ==> ~is_ping (LAST ls))
   path_last_not_ping_thm  |- !n p. prime n /\ tik n /\ p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
                                    ~is_ping (LAST (path n (1 + HALF p)))
   path_element_ping_funpow|- !n k u m. (let ls = path n k
                                          in m + u <= k /\ (!j. j < m ==> is_ping (EL (u + j) ls)) ==>
                                             !j. j <= m ==> EL (u + j) ls = FUNPOW ping j (EL u ls))
   path_element_pung_funpow|- !n k u m. (let ls = path n k
                                          in m + u <= k /\ (!j. j < m ==> is_pung (EL (u + j) ls)) ==>
                                             !j. j <= m ==> EL (u + j) ls = FUNPOW pung j (EL u ls))
   path_element_ping_funpow_alt
                           |- !n k j h. (let ls = path n k
                                          in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_ping (EL p ls)) ==>
                                             EL (j + h) ls = FUNPOW ping h (EL j ls))
   path_element_pung_funpow_alt
                           |- !n k j h. (let ls = path n k
                                          in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_pung (EL p ls)) ==>
                                             EL (j + h) ls = FUNPOW pung h (EL j ls))

   Sections and blocks in a path:
   skip_idx_ping_def       |- !ls k. skip_idx_ping ls k = WHILE (\j. is_ping (EL j ls)) SUC k
   skip_idx_pung_def       |- !ls k. skip_idx_pung ls k = WHILE (\j. is_pung (EL j ls)) SUC k
   skip_idx_ping_thm       |- !ls k h. k <= h /\ (!j. k <= j /\ j < h ==> is_ping (EL j ls)) /\ ~is_ping (EL h ls) ==>
                                       skip_idx_ping ls k = h
   skip_idx_ping_none      |- !ls k. ~is_ping (EL k ls) ==> skip_idx_ping ls k = k
   skip_idx_pung_thm       |- !ls k h. k <= h /\ (!j. k <= j /\ j < h ==> is_pung (EL j ls)) /\ ~is_pung (EL h ls) ==>
                                       skip_idx_pung ls k =
   skip_idx_pung_none      |- !ls k. ~is_pung (EL k ls) ==> skip_idx_pung ls k = k

   pung_to_ping_has_pong   |- !n k j h. (let ls = path n k
                                          in j < h /\ h <= k /\ is_pung (EL j ls) /\ is_ping (EL h ls) ==>
                                             ?p. j < p /\ p < h /\ is_pong (EL p ls))
   pong_interval_ping_start        |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
                                                     !e. j < e /\ e < h /\ is_ping (EL e ls) ==>
                                                     !p. j < p /\ p <= e ==> is_ping (EL p ls))
   pong_interval_ping_start_alt    |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     EVERY (\p. ~is_pong (EL p ls)) [j + 1 ..< h] ==>
                                                     !e. j < e /\ e < h /\ is_ping (EL e ls) ==>
                                                     EVERY (\p. is_ping (EL p ls)) [j + 1 .. e])
   pong_interval_pung_stop         |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
                                                     !e. j < e /\ e < h /\ is_pung (EL e ls) ==>
                                                     !p. e <= p /\ p < h ==> is_pung (EL p ls))
   pong_interval_pung_stop_alt     |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     EVERY (\p. ~is_pong (EL p ls)) [j + 1 ..< h] ==>
                                                     !e. j < e /\ e < h /\ is_pung (EL e ls) ==>
                                                     EVERY (\p. is_pung (EL p ls)) [e ..< h])
   pong_interval_cut_exists        |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
                                                     ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
                                                         (!p. j < p /\ p < c ==> is_ping (EL p ls)) /\
                                                          !p. c <= p /\ p < h ==> is_pung (EL p ls))
   pong_interval_cut_exists_alt    |- !n k j h. (let ls = path n k
                                                  in j < h /\ h <= k /\ is_pong (EL j ls) /\ is_pong (EL h ls) /\
                                                     EVERY (\p. ~is_pong (EL p ls)) [j + 1 ..< h] ==>
                                                     ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
                                                     EVERY (\p. is_ping (EL p ls)) [j + 1 ..< c] /\
                                                     EVERY (\p. is_pung (EL p ls)) [c ..< h])

   pong_seed_pung_before       |- !n k e. (let ls = path n k
                                            in ~is_pung (HD ls) /\ e < k /\ is_pong (EL e ls) ==>
                                               ?j. j <= e /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\
                                                   ~is_pung (EL (PRE j) ls))
   pong_seed_ping_after        |- !n k e. (let ls = path n k
                                            in ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
                                               ?h. e <= h /\ h < k /\ (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\
                                                   ~is_ping (EL (SUC h) ls))
   pong_seed_pung_ping         |- !n k e. (let ls = path n k
                                            in ~is_pung (HD ls) /\ ~is_ping (LAST ls) /\
                                               e < k /\ is_pong (EL e ls) ==>
                                               ?j h. j <= e /\ e <= h /\ h < k /\
                                                     (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls) /\
                                                     (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls))
   pung_next_not_flip_fix      |- !x y z. 0 < y /\ is_pung (x,y,z) ==> (let t = pung (x,y,z) in flip t <> t)
   path_last_flip_fix_not_by_pung
                               |- !n k e. (let ls = path n k
                                            in tik n /\ ~square n /\ flip (LAST ls) = LAST ls ==> ~is_pung (EL (k - 1) ls))
   path_skip_idx_pung_to_pong  |- !n k j. (let ls = path n k
                                            in tik n /\ ~square n /\ j < k /\ ~is_ping (EL j ls) /\
                                               flip (LAST ls) = LAST ls ==>
                                               (let e = skip_idx_pung ls j
                                                 in j <= e /\ e < k /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ is_pong (EL e ls)))
   path_skip_idx_ping_after_pong
                               |- !n k j. (let ls = path n k
                                            in j < k /\ is_pong (EL j ls) /\ flip (LAST ls) = LAST ls ==>
                                               (let e = skip_idx_ping ls (j + 1)
                                                 in j < e /\ e <= k /\ (!p. j < p /\ p < e ==> is_ping (EL p ls)) /\ ~is_ping (EL e ls)))
   path_skip_idx_from_pung_to_ping
                               |- !n k u v w. (let ls = path n k
                                                in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                                   u < k /\ ~is_ping (EL u ls) /\
                                                   v = skip_idx_pung ls u /\ w = skip_idx_ping ls (v + 1) ==>
                                                   u <= v /\ v < k /\ is_pong (EL v ls) /\
                                                   (!p. u <= p /\ p < v ==> is_pung (EL p ls)) /\
                                                   v < w /\ w <= k /\  ~is_ping (EL w ls) /\
                                                   !p. v < p /\ p < w ==> is_ping (EL p ls))

   Hopping:
   hop_def             |- !m x y z. hop m (x,y,z) = if x < 2 * m * z
                                                    then (2 * m * z - x,z,y + m * x - m * m * z)
                                                    else (x - 2 * m * z,y + m * x - m * m * z,z)
   hop_alt             |- !m x y z. 0 < z ==>
                                    hop m (x,y,z) =
                                    if x DIV (2 * z) < m
                                    then (2 * m * z - x,z,y + m * x - m * m * z)
                                    else (x - 2 * m * z,y + m * x - m * m * z,z)
   hop_0               |- !x y z. hop 0 (x,y,z) = (x,y,z)
   hop_0_alt           |- !t. hop 0 t = t
   hop_1               |- !x y z. ~is_ping (x,y,z) ==> hop 1 (x,y,z) = (zagier o flip) (x,y,z)
   hop_1_alt           |- !t. ~is_ping t ==> hop 1 t = (zagier o flip) t
   hop_mind            |- !m x z. 0 < z ==> (x DIV (2 * z) < m <=> 0 < 2 * m * z - x)
   hop_arm             |- !m n x y z. ~square n /\ n = windmill (x,y,z) ==>
                                      (m <= (x + SQRT n) DIV (2 * z) <=> 0 < y + m * x - m * m * z)
   hop_triple_first    |- !n m x y z. tik n /\ n = windmill (x,y,z) ==> 0 < FST (hop m (x,y,z))
   hop_windmill        |- !m n x y z. ~square n /\ n = windmill (x,y,z) /\ m <= (x + SQRT n) DIV (2 * z) ==>
                                      n = windmill (hop m (x,y,z))
   hop_range           |- !m n x y z. n = windmill (x,y,z) /\ 0 < 2 * m * z - x /\
                                      0 < y + m * x - m * m * z ==>
                                      x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z)
   hop_range_iff       |- !m n x y z. ~square n /\ n = windmill (x,y,z) ==>
                                      (0 < 2 * m * z - x /\ 0 < y + m * x - m * m * z <=>
                                       0 < z /\ x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z))

   Matrices of ping, pong, pung and hop:
   ping_funpow         |- !m x y z. FUNPOW ping m (x,y,z) = (x + 2 * m * y,y,z - m * x - m * m * y)
   ping_windmill       |- !x y z. is_ping (x,y,z) ==> windmill (x,y,z) = windmill (ping (x,y,z))
   ping_windmill_alt   |- !t. is_ping t ==> windmill t = windmill (ping t)
   pong_windmill       |- !x y z. is_pong (x,y,z) ==> windmill (x,y,z) = windmill (pong (x,y,z))
   pong_windmill_alt   |- !t. is_pong t ==> windmill t = windmill (pong t)
   pung_windmill       |- !x y z. is_pung (x,y,z) ==> windmill (x,y,z) = windmill (pung (x,y,z))
   pung_windmill_alt   |- !t. is_pung t ==> windmill t = windmill (pung t)
   ping_funpow_windmill|- !m x y z. (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
                                    windmill (x,y,z) = windmill (FUNPOW ping m (x,y,z))
   ping_funpow_windmill_alt
                       |- !m t. (!j. j < m ==> is_ping (FUNPOW ping j t)) ==>
                                windmill t = windmill (FUNPOW ping m t)
   pung_funpow_windmill|- !m x y z. (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                    windmill (x,y,z) = windmill (FUNPOW pung m (x,y,z))
   pung_funpow_windmill_alt
                       |- !m t. (!j. j < m ==> is_pung (FUNPOW pung j t)) ==>
                                windmill t = windmill (FUNPOW pung m t)
   pung_funpow         |- !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\
                                      (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                      FUNPOW pung m (x,y,z) = (x - 2 * m * z,y + m * x - m * m * z,z)
   pung_funpow_by_hop  |- !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\
                                      (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                      FUNPOW pung m (x,y,z) = hop m (x,y,z)
   pung_funpow_by_hop_alt
                       |- !n m t. tik n /\ ~square n /\ n = windmill t /\
                                  (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW pung m t = hop m t
   pung_to_ping_by_hop |- !n x y z p q. tik n /\ ~square n /\ n = windmill (x,y,z) /\
                                        is_pong (FUNPOW pung q (x,y,z)) /\
                                        (!j. j < q ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                                        (FUNPOW ping p o pong o FUNPOW pung q) (x,y,z) = hop (p + q + 1) (x,y,z)
   pung_to_ping_by_hop_alt
                       |- !n t p q. tik n /\ ~square n /\ n = windmill t /\ is_pong (FUNPOW pung q t) /\
                                    (!j. j < q ==> is_pung (FUNPOW pung j t)) ==>
                                    (FUNPOW ping p o pong o FUNPOW pung q) t = hop (p + q + 1) t
   hop_over_pung       |- !m x y z. 0 < z /\ m < x DIV (2 * z) ==> is_pung (hop m (x,y,z))
   hop_beyond_pung     |- !m x y z. 0 < z /\ m = x DIV (2 * z) ==> ~is_pung (hop m (x,y,z))

   Pong Indices along a Path:
   pong_indices_def    |- !ls. pong_indices ls = FILTER (\j. is_pong (EL j ls)) [0 ..< LENGTH ls]
   pong_indices_mem    |- !ls j. MEM j (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls)
   pong_indices_element|- !ls j. j IN set (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls)
   pong_indices_nil    |- pong_indices [] = []
   pong_indices_empty  |- set (pong_indices []) = {}:
   pong_indices_subset |- !ls. set (pong_indices ls) SUBSET count (LENGTH ls)
   pong_indices_finite |- !ls. FINITE (set (pong_indices ls))
   pong_indices_length |- !ls. LENGTH (pong_indices ls) <= LENGTH ls
   pong_indices_all_distinct
                       |- !ls. ALL_DISTINCT (pong_indices ls)
   pong_indices_path_cons      |- !n k. 0 < k ==> ?t. pong_indices (FRONT (path n k)) = 0::t
   pong_indices_path_not_nil   |- !n k. 0 < k ==> pong_indices (FRONT (path n k)) <> []
   pong_indices_path_head      |- !n k. 0 < k ==> HD (pong_indices (FRONT (path n k))) = 0
   pong_indices_path_eq_nil    |- !n k. pong_indices (FRONT (path n k)) = [] <=> k = 0
   pong_indices_path_element   |- !n k j. (let ls = path n k
                                            in MEM j (pong_indices (FRONT ls)) <=> j < k /\ is_pong (EL j ls))
   pong_indices_monotonic      |- !ls j. (let px = pong_indices ls
                                           in j + 1 < LENGTH px ==> EL j px < EL (j + 1) px)
   pong_indices_path_pong_gap  |- !n k j. (let ls = path n k; px = pong_indices (FRONT ls)
                                            in j + 1 < LENGTH px ==>
                                               !p. EL j px < p /\ p < EL (j + 1) px ==> ~is_pong (EL p ls))
   pong_indices_path_last      |- !n k. (let ls = path n k; px = pong_indices (FRONT ls); v = LAST px
                                          in 0 < k ==> v < k /\ is_pong (EL v ls) /\
                                             !p. v < p /\ p < k ==> ~is_pong (EL p ls))

   Define blocks based on pong indices:
   block_pairs_def     |- !ls. block_pairs ls =
                               MAP (\j. (j,skip_idx_ping ls (j + 1))) (pong_indices (FRONT ls))
   blocks_def          |- !ls. blocks ls =
                                MAP (\j. (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls)) [0 ..< LENGTH ls]

   Block Pairs Theorems:
   block_pairs_nil             |- !x. block_pairs [x] = []
   block_pairs_length          |- !ls. LENGTH (block_pairs ls) = LENGTH (pong_indices (FRONT ls))
   block_pairs_mem             |- !ls v w. MEM (v,w) (block_pairs ls) <=>
                                           MEM v (pong_indices (FRONT ls)) /\ w = skip_idx_ping ls (v + 1)
   block_pairs_element         |- !ls j v w. (let ps = block_pairs ls
                                               in j < LENGTH ps ==>
                                                  ((v,w) = EL j ps <=>
                                                  v = EL j (pong_indices (FRONT ls)) /\ w = skip_idx_ping ls (v + 1)))
   block_pairs_path_0          |- !n. block_pairs (path n 0) = []
   block_pairs_path_cons       |- !n k. 0 < k ==> ?t. block_pairs (path n k) = (0,skip_idx_ping (path n k) 1)::t
   block_pairs_path_not_nil    |- !n k. 0 < k ==> block_pairs (path n k) <> []
   block_pairs_path_head       |- !n k. 0 < k ==> HD (block_pairs (path n k)) = (0,skip_idx_ping (path n k) 1)
   block_pairs_path_eq_nil     |- !n k. block_pairs (path n k) = [] <=> k = 0
   block_pairs_path_mem        |- !n k v w. (let ls = path n k
                                              in ~is_ping (LAST ls) /\ MEM (v,w) (block_pairs ls) ==>
                                                 v < w /\ w <= k /\ w = skip_idx_ping ls (v + 1) /\
                                                 is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
                                                 !j. v < j /\ j < w ==> is_ping (EL j ls))
   block_pairs_path_pong       |- !n k v. (let ls = path n k
                                            in v < k /\ is_pong (EL v ls) <=>
                                               MEM (v,skip_idx_ping ls (v + 1)) (block_pairs ls))
   block_pairs_path_next       |- !n k j a b c d. (let ls = path n k;
                                                       ps = block_pairs ls
                                                    in ~is_ping (LAST ls) /\ j + 1 < LENGTH ps /\
                                                       EL j ps = (a,b) /\ EL (j + 1) ps = (c,d) ==>
                                                       b <= c /\ c = skip_idx_pung ls b /\
                                                       !p. b <= p /\ p < c ==> is_pung (EL p ls))
   block_pairs_path_next_pong  |- !n k j a b c d. (let ls = path n k;
                                                       ps = block_pairs ls
                                                    in ~is_ping (LAST ls) /\ j + 1 < LENGTH ps /\
                                                       EL j ps = (a,b) /\ EL (j + 1) ps = (c,d) /\
                                                       is_pong (EL b ls) ==> c = b)
   block_pairs_path_last       |- !n k. (let ls = path n k
                                          in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
                                             LAST (block_pairs ls) = (LAST (pong_indices (FRONT ls)),k))

   Blocks Theorems:
   blocks_nil          |- blocks [] = []
   blocks_length       |- !ls. LENGTH (blocks ls) = LENGTH ls
   blocks_eq_nil       |- !ls. blocks ls = [] <=> ls = []
   blocks_head         |- !ls. ls <> [] ==> HD (blocks ls) = (0,HD ls)
   blocks_mem          |- !ls u v w. MEM (u,v,w) (blocks ls) <=>
                                     ?j. j < LENGTH ls /\ (v,w) = EL j ls /\
                                         u = if j = 0 then 0 else SND (EL (j - 1) ls)
   blocks_next_element |- !ls j. j + 1 < LENGTH (blocks ls) ==>
                                 FST (EL (j + 1) (blocks ls)) = SND (SND (EL j (blocks ls)))
   blocks_path_0       |- !n. blocks (block_pairs (path n 0)) = []
   blocks_path_cons    |- !n k. 0 < k ==> ?t. blocks (block_pairs (path n k)) = (0,0,skip_idx_ping (path n k) 1)::t
   blocks_path_not_nil |- !n k. 0 < k ==> blocks (block_pairs (path n k)) <> []
   blocks_path_head    |- !n k. 0 < k ==> HD (blocks (block_pairs (path n k))) = (0,0,skip_idx_ping (path n k) 1)
   blocks_path_eq_nil  |- !n k. blocks (block_pairs (path n k)) = [] <=> k = 0
   blocks_path_mem     |- !n k u v w. (let ls = path n k
                                        in flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           u <= v /\ v < w /\ w <= k /\
                                           ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
                                           v = skip_idx_pung ls u /\ w = skip_idx_ping ls (v + 1) /\
                                           (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
                                            !j. v < j /\ j < w ==> is_ping (EL j ls))
   blocks_path_last    |- !n k. (let ls = path n k
                                  in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
                                     SND (SND (LAST (blocks (block_pairs ls)))) = k)
   blocks_path_indices_by_funpow
                       |- !n k u v w. (let ls = path n k
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                           MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           EL v ls = FUNPOW pung (v - u) (EL u ls) /\
                                           EL (v + 1) ls = pong (EL v ls) /\
                                           EL w ls = FUNPOW ping (w - (v + 1)) (EL (v + 1) ls))
   blocks_path_third_by_funpow
                       |- !n k u v w. (let ls = path n k
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                           MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           EL w ls = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls))
   blocks_path_indices_by_hop
                       |- !n k u v w. (let ls = path n k
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                           MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           EL v ls = hop (v - u) (EL u ls) /\
                                           EL w ls = hop (w - u) (EL u ls))
   blocks_path_second_by_hop
                       |- !n k u v w. (let ls = path n k
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                           MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           EL v ls = hop (v - u) (EL u ls))
   blocks_path_third_by_hop
                       |- !n k u v w. (let ls = path n k
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                           MEM (u,v,w) (blocks (block_pairs ls)) ==> EL w ls = hop (w - u) (EL u ls))

   Hopping Algorithm:
   step_def            |- !k x y z. step k (x,y,z) = (x + k) DIV (2 * z)
   hopping_def         |- !k t. hopping k t = hop (step k t) t
   two_sq_hop_def      |- !n. two_sq_hop n = WHILE ($~ o found) (hopping (SQRT n)) (1,n DIV 4,1)

   blocks_triple_first_not_ping
                       |- !n k u v w. (let ls = path n k
                                        in flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                           ~is_ping (EL u ls) /\ (let (x,y,z) = EL u ls in z <= x + y))
   step_0              |- !x y z. step 0 (x,y,z) = x DIV (2 * z)
   step_sqrt           |- !n x y z. step (SQRT n) (x,y,z) = (x + SQRT n) DIV (2 * z)
   path_start_over_ping    |- !n k. (let h = HALF (1 + SQRT n)
                                      in tik n /\ ~square n /\ h <= k ==>
                                         !j. 0 < j /\ j < h ==> is_ping (EL j (path n k)))
   path_start_beyond_ping  |- !n k. (let h = HALF (1 + SQRT n)
                                      in tik n /\ ~square n /\ h <= k ==> ~is_ping (EL h (path n k)))
   path_start_skip_idx_ping    |- !n k. (let h = HALF (1 + SQRT n)
                                      in tik n /\ ~square n /\ h <= k ==>
                                         skip_idx_ping (path n k) 1 = step (SQRT n) (1,n DIV 4,1))

   Hopping by step 0:
   hop_step_0_over_pung    |- !m x y z. 0 < z /\ m < step 0 (x,y,z) ==> is_pung (hop m (x,y,z))
   hop_step_0_beyond_pung  |- !m x y z. 0 < z /\ m = step 0 (x,y,z) ==> ~is_pung (hop m (x,y,z))
   step_0_eq_0             |- !x y z. 0 < z ==> (step 0 (x,y,z) = 0 <=> ~is_pung (x,y,z))
   step_0_of_ping          |- !x y z. 0 < z /\ is_ping (x,y,z) ==> step 0 (x,y,z) = 0
   step_0_of_pong          |- !x y z. 0 < z /\ is_pong (x,y,z) ==> step 0 (x,y,z) = 0
   step_0_of_pung          |- !x y z. 0 < z /\ is_pung (x,y,z) ==> 0 < step 0 (x,y,z)
   hop_identity_1          |- !m x z. (let xx = x - 2 * m * z in xx - 2 * z = x - 2 * (m + 1) * z)
   hop_identity_2          |- !m x z. (let xx = x - 2 * m * z in 0 < xx ==> 2 * z - xx = 2 * (m + 1) * z - x)
   hop_identity_3          |- !m x y z. (let xx = x - 2 * m * z;
                                             yy = y + m * x - m ** 2 * z
                                          in 0 < xx /\ 0 < yy ==>
                                             xx + yy - z = y + (m + 1) * x - (m + 1) ** 2 * z)
   hop_identity_4          |- !m x y z. (let xx = 2 * m * z - x;
                                             yy = y + m * x - m ** 2 * z
                                          in 0 < xx /\ 0 < yy ==>
                                             yy - z - xx = y + (m + 1) * x - (m + 1) ** 2 * z)
   hop_step_0_before_pong  |- !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m <= step 0 (x,y,z) ==>
                                          hop m (x,y,z) = FUNPOW pung m (x,y,z)
   hop_step_0_before_pong_alt
                           |- !n m t. tik n /\ ~square n /\ n = windmill t /\ m <= step 0 t ==>
                                      hop m t = FUNPOW pung m t
   hop_step_0_at_pong      |- !n m x y z. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) ==>
                                          hop (m + 1) (x,y,z) = pong (hop m (x,y,z))
   hop_step_0_at_pong_alt  |- !n m t. tik n /\ ~square n /\ n = windmill t /\ m = step 0 t ==>
                                      hop (m + 1) t = pong (hop m t)
   hop_step_0_beyond_pong  |- !n x y z m j. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\
                                            m + j < step (SQRT n) (x,y,z) ==>
                                            hop (m + 1 + j) (x,y,z) = FUNPOW ping j (hop (m + 1) (x,y,z))
   hop_step_0_beyond_pong_alt
                           |- !n m j t. tik n /\ ~square n /\ n = windmill t /\
                                        m = step 0 t /\ m + j < step (SQRT n) t ==>
                                        hop (m + 1 + j) t = FUNPOW ping j (hop (m + 1) t)
   hop_step_0_around_pong  |- !n x y z m j. tik n /\ ~square n /\ n = windmill (x,y,z) /\
                                            m = step 0 (x,y,z) /\ m + j < step (SQRT n) (x,y,z) ==>
                                            hop (m + 1 + j) (x,y,z) =
                                            (FUNPOW ping j o pong o FUNPOW pung m) (x,y,z)
   hop_step_0_around_pong_alt
                           |- !n m j t. tik n /\ ~square n /\ n = windmill t /\
                                        m = step 0 t /\ m + j < step (SQRT n) t ==>
                                        hop (m + 1 + j) t = (FUNPOW ping j o pong o FUNPOW pung m) t
   hop_step_0_before_pong_is_pung
                           |- !n x y z m. ~square n /\ n = windmill (x,y,z) /\ m < step 0 (x,y,z) ==>
                                          is_pung (hop m (x,y,z))
   hop_step_0_before_pong_is_pung_alt
                           |- !n m t. ~square n /\ n = windmill t /\ m < step 0 t ==> is_pung (hop m t)
   hop_step_0_at_pong_is_pong
                           |- !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\
                                          ~is_ping (x,y,z) ==> is_pong (hop m (x,y,z))
   hop_step_0_at_pong_is_pong_alt
                           |- !n m t. tik n /\ ~square n /\ n = windmill t /\ m = step 0 t /\ ~is_ping t ==>
                                      is_pong (hop m t)
   hop_step_0_path_element_before_pong
                           |- !n k u m. (let ls = path n k
                                          in tik n /\ ~square n /\ u <= k /\ m <= step 0 (EL u ls) ==>
                                             hop m (EL u ls) = FUNPOW pung m (EL u ls))
   hop_step_0_path_element_at_pong
                           |- !n k u m. (let ls = path n k
                                          in tik n /\ ~square n /\ u <= k /\ m = step 0 (EL u ls) ==>
                                             hop (m + 1) (EL u ls) = pong (hop m (EL u ls)))
   hop_step_0_path_element_beyond_pong
                           |- !n k u m j. (let ls = path n k
                                            in tik n /\ ~square n /\ u <= k /\
                                               m = step 0 (EL u ls) /\ m + j < step (SQRT n) (EL u ls) ==>
                                               hop (m + 1 + j) (EL u ls) = FUNPOW ping j (hop (m + 1) (EL u ls)))
   hop_step_0_path_element_around_pong
                           |- !n k u m j. (let ls = path n k
                                            in tik n /\ ~square n /\ u <= k /\
                                               m = step 0 (EL u ls) /\ m + j < step (SQRT n) (EL u ls) ==>
                                               hop (m + 1 + j) (EL u ls) = (FUNPOW ping j o pong o FUNPOW pung m) (EL u ls))
   hop_step_0_path_element |- !n k u m. (let ls = path n k
                                          in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                             u < k /\ m <= step 0 (EL u ls) ==>
                                             u + m < k /\ hop m (EL u ls) = EL (u + m) ls)
   hop_step_0_suc          |- !n k u j. (let ls = path n k
                                          in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                             u < k /\ j < step 0 (EL u ls) ==>
                                             hop (SUC j) (EL u ls) = (zagier o flip) (hop j (EL u ls)))
   hopping_path_step_0     |- !n k u. (let ls = path n k
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                           u < k ==> hopping 0 (EL u ls) = EL (skip_idx_pung ls u) ls)
   blocks_path_second_by_hopping
                           |- !n k u v w. (let ls = path n k
                                            in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                               MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                               EL v ls = hopping 0 (EL u ls))

   Hopping by step (SQRT n):
   step_sqrt_eq_0          |- !x y z. 0 < z ==>
                                      (step (SQRT (windmill (x,y,z))) (x,y,z) = 0 <=> is_ping (x,y,z))
   step_sqrt_of_ping       |- !x y z. 0 < z /\ is_ping (x,y,z) ==>
                                      step (SQRT (windmill (x,y,z))) (x,y,z) = 0
   step_sqrt_of_pong       |- !x y z. 0 < z /\ is_pong (x,y,z) ==>
                                      0 < step (SQRT (windmill (x,y,z))) (x,y,z)
   step_sqrt_of_pung       |- !x y z. 0 < z /\ is_pung (x,y,z) ==>
                                      0 < step (SQRT (windmill (x,y,z))) (x,y,z)
   step_0_le_step_sqrt     |- !x y z. 0 < z ==> step 0 (x,y,z) <= step (SQRT (windmill (x,y,z))) (x,y,z)
   step_0_lt_step_sqrt     |- !n x y z. ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
                                        step 0 (x,y,z) < step (SQRT n) (x,y,z)
   step_0_lt_step_sqrt_alt |- !n t. ~square n /\ n = windmill t /\ ~is_ping t ==> step 0 t < step (SQRT n) t
   hop_step_0_beyond_pong_is_ping
                           |- !n x y z m j. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\
                                            m + 1 + j < step (SQRT n) (x,y,z) ==> is_ping (hop (m + 1 + j) (x,y,z))
   hop_step_0_beyond_pong_is_ping_alt
                           |- !n m j t. tik n /\ ~square n /\ n = windmill t /\ m = step 0 t /\
                                        m + 1 + j < step (SQRT n) t ==> is_ping (hop (m + 1 + j) t)
   hop_step_sqrt_not_ping  |- !n x y z. tik n /\ ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
                                        ~is_ping (hop (step (SQRT n) (x,y,z)) (x,y,z))
   hop_step_sqrt_not_ping_alt
                           |- !n t. tik n /\ ~square n /\ n = windmill t /\ ~is_ping t ==>
                                    ~is_ping (hop (step (SQRT n) t) t)

   Skipping by Triples:
   skip_ping_def           |- !t. skip_ping t = WHILE is_ping ping t
   skip_pung_def           |- !t. skip_pung t = WHILE is_pung pung t
   skip_ping_thm           |- !t k. ~is_ping (FUNPOW ping k t) /\ (!j. j < k ==> is_ping (FUNPOW ping j t)) ==>
                                    skip_ping t = FUNPOW ping k t
   skip_ping_none          |- !t. ~is_ping t ==> skip_ping t = t
   skip_pung_thm           |- !t k. ~is_pung (FUNPOW pung k t) /\ (!j. j < k ==> is_pung (FUNPOW pung j t)) ==>
                                    skip_pung t = FUNPOW pung k t
   skip_pung_none          |- !t. ~is_pung t ==> skip_pung t = t
   hopping_0               |- !n x y z. tik n /\ ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
                                        hopping 0 (x,y,z) = skip_pung (x,y,z)
   hopping_0_alt           |- !n t. tik n /\ ~square n /\ n = windmill t ==>
                                    hopping 0 t = skip_pung t
   hopping_sqrt            |- !n x y z. tik n /\ ~square n /\ n = windmill (x,y,z) ==>
                                        hopping (SQRT n) (x,y,z) = skip_ping (pong (hopping 0 (x,y,z)))
   hopping_sqrt_alt        |- !n t. tik n /\ ~square n /\ n = windmill t /\ ~is_ping t ==>
                                    hopping (SQRT n) t = skip_ping (pong (hopping 0 t))
   blocks_path_element     |- !n k u v w. (let ls = path n k
                                            in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                               MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                               u <= v /\ v < w /\ w <= k /\ ~is_ping (EL u ls) /\
                                               is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
                                               EL v ls = skip_pung (EL u ls) /\
                                               EL (v + 1) ls = pong (EL v ls) /\
                                               EL w ls = skip_ping (EL (v + 1) ls) /\
                                               (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
                                                !j. v < j /\ j < w ==> is_ping (EL j ls))
   blocks_path_third_by_hopping
                           |- !n k u v w. (let ls = path n k
                                            in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                               MEM (u,v,w) (blocks (block_pairs ls)) ==>
                                               EL w ls = hopping (SQRT n) (EL u ls))
   blocks_path_triple_by_hopping
                           |- !n k j. (let ls = path n k;
                                           bs = blocks (block_pairs ls);
                                            t = EL j bs
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ j < LENGTH bs ==>
                                           EL (SND (SND t)) ls = hopping (SQRT n) (EL (FST t) ls))
   hopping_sqrt_funpow     |- !n k j. (let ls = path n k;
                                           bs = blocks (block_pairs ls)
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ j < LENGTH bs ==>
                                           FUNPOW (hopping (SQRT n)) (j + 1) (HD ls) =
                                           hopping (SQRT n) (EL (FST (EL j bs)) ls))
   hopping_sqrt_funpow_eqn |- !n k j. (let ls = path n k;
                                           bs = blocks (block_pairs ls)
                                        in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ j < LENGTH bs ==>
                                           FUNPOW (hopping (SQRT n)) (j + 1) (HD ls) = EL (SND (SND (EL j bs))) ls)
   hopping_sqrt_funpow_max |- !n k. (let ls = path n k
                                      in tik n /\ ~square n /\ flip (LAST ls) = LAST ls ==>
                                      FUNPOW (hopping (SQRT n)) (LENGTH (blocks (block_pairs ls))) (HD ls) = LAST ls)

   Hopping for tik primes:
   tik_prime_property  |- !n. (let s = mills n;
                                   u = (1,1,n DIV 4)
                                in tik n /\ prime n ==>
                                   ~square n /\ FINITE s /\ zagier involute s /\ flip involute s /\
                                   fixes zagier s = {u} /\ ODD (iterate_period (zagier o flip) u))
   tik_prime_iterate_period_odd
                       |- !n. tik n /\ prime n ==> ODD (iterate_period (zagier o flip) (1,1,n DIV 4))
   tik_iterate_period_eq_1
                       |- !n. tik n ==> (iterate_period (zagier o flip) (1,1,n DIV 4) = 1 <=> n = 5)
   tik_path_head_flip_fix
                       |- !n k. (let ls = path n k in tik n ==> (flip (HD ls) = HD ls <=> n = 5))
   path_last_at_half_period
                       |- !n k. (let ls = path n k;
                                      u = (1,1,n DIV 4);
                                      p = iterate_period (zagier o flip) u
                                  in k = 1 + HALF p ==> LAST ls = FUNPOW (zagier o flip) (HALF p) u)
   tik_prime_path_last_flip_fix
                       |- !n k. (let ls = path n k
                                  in tik n /\ prime n /\
                                     k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) ==>
                                     flip (LAST ls) = LAST ls)
   tik_prime_path_not_flip_fix
                       |- !n k j. (let ls = path n k
                                    in tik n /\ prime n /\
                                       k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
                                       0 < j /\ j < k ==> flip (EL j ls) <> EL j ls)
   tik_prime_path_flip_fix_iff
                       |- !n k j. (let ls = path n k
                                    in tik n /\ prime n /\
                                       k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
                                       0 < j /\ j <= k ==> (flip (EL j ls) = EL j ls <=> j = k))
   hopping_sqrt_funpow_at_blocks_third
                       |- !n k j. (let ls = path n k;
                                        bs = blocks (block_pairs ls)
                                    in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ j <= LENGTH bs ==>
                                       ?w. w <= k /\ FUNPOW (hopping (SQRT n)) j (HD ls) = EL w ls)
   blocks_path_third_less
                       |- !n k j. (let ls = path n k;
                                       bs = blocks (block_pairs ls)
                                    in flip (LAST ls) = LAST ls /\ j + 1 < LENGTH bs ==>
                                       SND (SND (EL j bs)) < SND (SND (EL (j + 1) bs)))
   blocks_path_third_monotonic
                       |- !n k j h. (let ls = path n k;
                                         bs = blocks (block_pairs ls)
                                      in flip (LAST ls) = LAST ls /\ j < h /\ h < LENGTH bs ==>
                                         SND (SND (EL j bs)) < SND (SND (EL h bs)))
   blocks_path_third_pos
                       |- !n k j. (let ls = path n k;
                                        bs = blocks (block_pairs ls)
                                    in flip (LAST ls) = LAST ls /\ 0 < k /\ j < LENGTH bs ==>
                                       0 < SND (SND (EL j bs)))
   hopping_sqrt_funpow_less
                       |- !n k j. (let ls = path n k;
                                       bs = blocks (block_pairs ls)
                                    in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
                                       0 < j /\ j < LENGTH bs /\ ALL_DISTINCT (TL ls) ==>
                                       FUNPOW (hopping (SQRT n)) j (HD ls) <> LAST ls)
   tik_prime_path_hopping_not_flip_fix
                       |- !n k j t. (let ls = path n k;
                                         bs = blocks (block_pairs ls)
                                      in tik n /\ prime n /\
                                         k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
                                         0 < j /\ j < LENGTH bs /\ t = FUNPOW (hopping (SQRT n)) j (HD ls) ==>
                                         flip t <> t)
   tik_prime_path_hopping_flip_fix_iff
                       |- !n k j t. (let ls = path n k;
                                         bs = blocks (block_pairs ls)
                                      in tik n /\ prime n /\
                                         k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
                                         0 < j /\ j <= LENGTH bs /\ t = FUNPOW (hopping (SQRT n)) j (HD ls) ==>
                                         (flip t = t <=> j = LENGTH bs))
   tik_prime_path_hopping_while_thm
                       |- !n k. (let ls = path n k
                                  in tik n /\ prime n /\
                                     k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) ==>
                                     WHILE (\t. flip t <> t) (hopping (SQRT n)) (HD ls) = LAST ls)
   two_sq_hop_thm      |- !n k. tik n /\ prime n ==> two_sq_hop n IN fixes flip (mills n)
   two_squares_hop_def |- !n. two_squares_hop n = (let (x,y,z) = two_sq_hop n in (x,y + z))
   two_squares_hop_thm |- !n. tik n /\ prime n ==> (let (u,v) = two_squares_hop n in n = u ** 2 + v ** 2)

*)

(* ------------------------------------------------------------------------- *)
(* Helper Theorems                                                           *)
(* ------------------------------------------------------------------------- *)

(* ------------------------------------------------------------------------- *)
(* Windmill Helper                                                           *)
(* ------------------------------------------------------------------------- *)

(* Theorem: n = windmill (x,y,z) ==> x <= SQRT n *)
(* Proof:
   Note n = x ** 2 + 4 * y * z     by windmill_def
     so x ** 2 <= n                by inequality
    ==> SQRT (x ** 2) <= SQRT n    by SQRT_LE
     or             x <= SQRT n    by SQRT_OF_SQ
*)
Theorem windmill_x_upper:
  !n x y z. n = windmill (x,y,z) ==> x <= SQRT n
Proof
  rw[windmill_def] >>
  qabbrev_tac `n = 4 * (y * z) + x ** 2` >>
  `x ** 2 <= n` by simp[Abbr`n`] >>
  metis_tac[SQRT_LE, SQRT_OF_SQ]
QED

(* Theorem: n = windmill (x,y,z) ==> y * z <= n DIV 4 *)
(* Proof:
   Note n = x ** 2 + 4 * y * z     by windmill_def
     so 4 y * z <= n               by inequality
    ==>   y * z <= n DIV 4         by DIV_LE_MONOTONE
*)
Theorem windmill_yz_upper:
  !n x y z. n = windmill (x,y,z) ==> y * z <= n DIV 4
Proof
  simp[windmill_def]
QED

(* Theorem: n = windmill (x,y,z) /\ 0 < z ==> y <= n DIV 4 *)
(* Proof:
   Note y <= y * z             by LE_MULT_CANCEL_LBARE, 0 < z
    and y * z <= n DIV 4       by windmill_yz_upper
     so y <= n DIV 4           by LESS_EQ_TRANS
*)
Theorem windmill_y_upper:
  !n x y z. n = windmill (x,y,z) /\ 0 < z ==> y <= n DIV 4
Proof
  metis_tac[windmill_yz_upper, LE_MULT_CANCEL_LBARE, LESS_EQ_TRANS]
QED

(* Theorem: n = windmill (x,y,z) /\ 0 < y ==> z <= n DIV 4 *)
(* Proof:
   Note windmill (x,y,z) = windmill (x,z,y)    by windmill_comm
     so z <= n DIV 4                           by windmill_y_upper
*)
Theorem windmill_z_upper:
  !n x y z. n = windmill (x,y,z) /\ 0 < y ==> z <= n DIV 4
Proof
  metis_tac[windmill_y_upper, windmill_comm]
QED

(* Theorem: n MOD 4 <> 0 /\ n = windmill (x,y,z) ==> 0 < x *)
(* Proof:
   Note (x,y,z) IN (mills n)       by mills_element
     so 0 < x                      by mills_with_all_mind
*)
Theorem windmill_with_mind:
  !n x y z. n MOD 4 <> 0 /\ n = windmill (x,y,z) ==> 0 < x
Proof
  metis_tac[mills_element, mills_with_all_mind, NOT_ZERO]
QED

(* Theorem: ~square n /\ n = windmill (x,y,z) ==> 0 < y /\ 0 < z *)
(* Proof:
   Note (x,y,z) IN (mills n)       by mills_element
     so 0 < y /\ 0 < z             by mills_with_no_arms
*)
Theorem windmill_with_arms:
  !n x y z. ~square n /\ n = windmill (x,y,z) ==> 0 < y /\ 0 < z
Proof
  metis_tac[mills_element, mills_with_no_arms, NOT_ZERO]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) ==> 0 < x /\ 0 < y /\ 0 < z *)
(* Proof:
   Note tik n ==> n MOD 4 <> 0     by 1 <> 0
     so 0 < x                      by windmill_with_mind
     so 0 < y /\ 0 < z             by windmill_with_arms,  ~square n
*)
Theorem windmill_mind_and_arms:
  !n x y z. tik n /\ ~square n /\ n = windmill (x,y,z) ==> 0 < x /\ 0 < y /\ 0 < z
Proof
  metis_tac[windmill_with_mind, windmill_with_arms, ONE_NOT_0]
QED

(* Note: windmill.hol only has flip_closure and zagier_closure. *)

(* Theorem: t IN mills n <=> n = windmill t *)
(* Proof: by mills_element, triple_parts. *)
Theorem mills_element_alt:
  !n t. t IN mills n <=> n = windmill t
Proof
  metis_tac[mills_element, triple_parts]
QED

(* Theorem: tik n ==> (1,n DIV 4,1) IN mills n *)
(* Proof:
   Note (1,1,n DIV 4) in mills n               by mills_element_trivial
     so flip (1,1,n DIV 4) in mills n          by flip_closure
     or (1,n DIV 4, 1) in mills n              by flip_def
*)
Theorem mills_element_trivial_flip:
  !n. tik n ==> (1,n DIV 4,1) IN mills n
Proof
  metis_tac[mills_element_trivial, flip_def, flip_closure]
QED

(* Theorem: n MOD 4 <> 0 <=> !x y z. (x,y,z) IN mills n ==> 0 < x *)
(* Proof: by mills_with_all_mind. *)
Theorem mills_with_mind:
  !n. n MOD 4 <> 0 <=> !x y z. (x,y,z) IN mills n ==> 0 < x
Proof
  metis_tac[mills_with_all_mind, NOT_ZERO]
QED

(* Theorem: ~square n <=> !x y z. (x,y,z) IN mills n ==> 0 < y /\ 0 < z *)
(* Proof: by mills_with_no_arms. *)
Theorem mills_with_arms:
  !n. ~square n <=> !x y z. (x,y,z) IN mills n ==> 0 < y /\ 0 < z
Proof
  metis_tac[mills_with_no_arms, NOT_ZERO]
QED

(* Theorem: ~square n /\ n MOD 4 <> 0 <=> !x y z. (x,y,z) IN mills n ==> 0 < x /\ 0 < y /\ 0 < z *)
(* Proof: by mills_with_mind, mills_with_arms. *)
Theorem mills_with_mind_and_arms:
  !n. ~square n /\ n MOD 4 <> 0 <=> !x y z. (x,y,z) IN mills n ==> 0 < x /\ 0 < y /\ 0 < z
Proof
  metis_tac[mills_with_mind, mills_with_arms]
QED

(* Theorem: windmill (x,y,z) = windmill (flip (x,y,z)) *)
(* Proof: by flip_closure, mills_element_triple. *)
(* Note: can extract flip_closure proof, then simplify it. *)
Theorem flip_windmill:
  !x y z. windmill (x,y,z) = windmill (flip (x,y,z))
Proof
  metis_tac[flip_closure, mills_element_triple]
QED

(* Use this to simplify flip_closure. *)

(* Theorem: windmill (x,y,z) = windmill (flip (x,y,z)) *)
(* Proof:
     windmill (flip (x,y,z))
   = windmill (x,z,y)          by flip_def
   = x ** 2 + 4 * z * y        by windmill_def
   = x ** 2 + 4 * y * z        by MULT_COMM
   = windmill (x,y,z)          by windmill_def
*)
Theorem flip_windmill[allow_rebind]:
  !x y z. windmill (x,y,z) = windmill (flip (x,y,z))
Proof
  simp[flip_def, windmill_def]
QED

(* Theorem: windmill t = windmill (flip t) *)
(* Proof: by flip_windmill, triple_parts. *)
Theorem flip_windmill_alt:
  !t. windmill t = windmill (flip t)
Proof
  metis_tac[flip_windmill, triple_parts]
QED

(* Theorem: (x, y, z) IN mills n <=> flip (x, y, z) IN mills n *)
(* Proof:
       (x, y, z) IN mills n
   <=> n = windmill (x,y,z)                    by mills_element
   <=> n = windmill (flip (x,y,z))             by flip_windmill
   <=> flip (x,y,z) IN mills n                 by mills_element_alt
*)
Theorem flip_closure_iff:
  !n x y z. (x, y, z) IN mills n <=> flip (x, y, z) IN mills n
Proof
  metis_tac[mills_element, mills_element_alt, flip_windmill]
QED

(* Theorem: t IN mills n <=> flip t IN mills n *)
(* Proof: by flip_closure_iff, triple_parts. *)
Theorem flip_closure_iff_alt:
  !n t. t IN mills n <=> flip t IN mills n
Proof
  metis_tac[flip_closure_iff, triple_parts]
QED

(* Theorem: windmill (x,y,z) = windmill (zagier (x,y,z)) *)
(* Proof: by zagier_closure, mills_element_triple. *)
(* Note: can extract zagier_closure proof, then simplify it. *)
Theorem zagier_windmill:
  !x y z. windmill (x,y,z) = windmill (zagier (x,y,z))
Proof
  metis_tac[zagier_closure, mills_element_triple]
QED

(* Use this to simplify zagier_closure. *)

(* Theorem: windmill (x,y,z) = windmill (zagier (x,y,z)) *)
(* Proof:
   By windmill_def, zagier_def, this is to show:
   (1) x < y - z ==>
       4 * (y * z) + x ** 2 = 4 * (z * (y - (x + z))) + (x + 2 * z) ** 2
       or windmill (x, y, z) = windmill (x + 2 * z) z (y - (x + z))
       From      x < y - z
         so  x + z < y               by LESS_SUB_ADD_LESS, or in detail:
       Note x < 0 is impossible, so ~(y <= z), or z < y, implies z <= y.
         so  x + z < y - z + z = y   by SUB_ADD, z <= y
       Thus  x + z <= y              by x + z < y

         windmill (x + 2 * z, z ,y - (x + z))
       = (x + 2 * z) ** 2 + 4 * z * (y - (x + z))
       = x ** 2 + (2 * z) ** 2 + 4 * x * z + 4 * z * (y - (x + z))   by SUM_SQUARED
       = x ** 2 + 4 * z * z + 4 * z * x + 4 * z (y - (x + z))        by EXP_2
       = x ** 2 + 4 * z * (x + z) + 4 * z (y - (x + z)) by LEFT_ADD_DISTRIB
       = x ** 2 + 4 * z * ((x + z) + (y - (x + z)))     by LEFT_ADD_DISTRIB
       = x ** 2 + 4 * z * ((x + z) + y - (x + z))       by LESS_EQ_ADD_SUB, x + z <= y
       = x ** 2 + 4 * z * (y + (x + z) - (x + z))
       = x ** 2 + 4 * z * y                             by ADD_SUB
       = x ** 2 + 4 * y * z
       = windmill (x, y, z)

   (2) ~(x < y - z) /\ x < 2 * y ==>
        4 * (y * z) + x ** 2 = 4 * (y * (x + z - y)) + (2 * y - x) ** 2
       or windmill (x, y, z) = windmill (2 * y - x) y (x + z - y)
       Note y - z <= x             by ~(x < y - z)
         so y <= x + z             by SUB_RIGHT_LESS_EQ

         windmill (2 * y - x, y, x + z - y)
       = (2 * y - x) ** 2 + 4 * y * (x + z - y)
       = (2 * y - x) ** 2 + 8 * y * x - 8 * y * x + 4 * y * (x + z - y)  by ADD_SUB
       = (2 * y - x) ** 2 + 8 * y * x + 4 * y * (x + z - y) - 8 * y * x  by SUB_RIGHT_ADD,
                        since 8 * y * x <= (2 * y - x) ** 2 + 8 * y * x
       = (2 * y + x) ** 2 + 4 * y * (x + z - y) - 8 * y * x   by binomial_sub_add, x < 2 * y
       = (2 * y) ** 2 + x ** 2 + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x
                                                              by SUM_SQUARED
       = x ** 2 + 4 * y * y + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x
                                                              by EXP_2
       = x ** 2 + 4 * y * (y + x) + 4 * y * (x + z - y) - 8 * y * x
                                                              by LEFT_ADD_DISTRIB
       = x ** 2 + 4 * y * (y + x + (x + z - y)) - 8 * y * x   by LEFT_ADD_DISTRIB
       = x ** 2 + 4 * y * (y + x + x + z - y) - 8 * y * x     by LESS_EQ_ADD_SUB, y <= x + z
       = x ** 2 + 4 * y * (2 * x + z) - 4 * y * (2 * x)       by arithmetic
       = x ** 2 + 4 * y * (2 * x + z - 2 * x)                 by LEFT_SUB_DISTRIB
       = x ** 2 + 4 * y * z                                   by ADD_SUB
       = windmill (x, y, z)

   (3) ~(x < y - z) /\ ~(x < 2 * y) ==>
       4 * (y * z) + x ** 2 = 4 * (y * (x + z - y)) + (x - 2 * y) ** 2
       or windmill (x, y, z) = windmill (x - 2 * y) y (x + z - y)
       Note y - z <= x             by ~(x < y - z)
         so y <= x + z             by SUB_RIGHT_LESS_EQ
       Also 2 * y <= x             by ~(x < 2 * y)

         windmill (x - 2 * y, y, x + z - y)
       = (x - 2 * y) ** 2 + 4 * y * (x + z - y)
       = (x - 2 * y) ** 2 + 8 * y * x - 8 * y * x + 4 * y * (x + z - y)  by ADD_SUB
       = (x - 2 * y) ** 2 + 8 * y * x + 4 * y * (x + z - y) - 8 * y * x  by SUB_RIGHT_ADD,
                        since 8 * y * x <= (2 * y - x) ** 2 + 8 * y * x
       = (x + 2 * y) ** 2 + 4 * y * (x + z - y) - 8 * y * x   by binomial_sub_add, 2 * y <= x
       = x ** 2 + (2 * y) ** 2 + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x
                                                              by SUM_SQUARED
       = x ** 2 + 4 * y * y + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x
                                                              by EXP_2
       = x ** 2 + 4 * y * (y + x) + 4 * y * (x + z - y) - 8 * y * x
                                                              by LEFT_ADD_DISTRIB
       = x ** 2 + 4 * y * (y + x + (x + z - y)) - 8 * y * x   by LEFT_ADD_DISTRIB
       = x ** 2 + 4 * y * (y + x + x + z - y) - 8 * y * x     by LESS_EQ_ADD_SUB, y <= x + z
       = x ** 2 + 4 * y * (2 * x + z) - 4 * y * (2 * x)       by arithmetic
       = x ** 2 + 4 * y * (2 * x + z - 2 * x)                 by LEFT_SUB_DISTRIB
       = x ** 2 + 4 * y * z                                   by ADD_SUB
       = windmill (x, y, z)
*)
Theorem zagier_windmill[allow_rebind]:
  !x y z. windmill (x,y,z) = windmill (zagier (x,y,z))
Proof
  rw[windmill_def, zagier_def] >| [
    `x + z < y` by decide_tac >>
    `(x + 2 * z) ** 2 + 4 * (z * (y - (x + z))) =
    x ** 2 + (2 * z) ** 2 + 4 * x * z + 4 * z * (y - (x + z))` by simp[SUM_SQUARED] >>
    `_ = x ** 2 + (2 * z) ** 2 + 4 * z * x + 4 * z * (y - (x + z))` by decide_tac >>
    `_ = x ** 2 + (2 * z) * (2 * z) + 4 * z * x + 4 * z * (y - (x + z))` by metis_tac[EXP_2] >>
    `_ = x ** 2 + 4 * z * z + 4 * z * x + 4 * z * (y - (x + z))` by decide_tac >>
    `_ = x ** 2 + 4 * z * (z + x) + 4 * z * (y - (x + z))` by decide_tac >>
    `_ = x ** 2 + 4 * z * ((x + z) + (y - (x + z)))` by rw[LEFT_ADD_DISTRIB] >>
    `_ = x ** 2 + 4 * z * ((x + z) + y - (x + z))` by rw[] >>
    `_ = x ** 2 + 4 * z * y` by decide_tac >>
    simp[],
    `y <= x + z` by decide_tac >>
    `(2 * y - x) ** 2 + 4 * (y * (x + z - y)) =
    (2 * y - x) ** 2 + 4 * y * (x + z - y)` by decide_tac >>
    `_ = (2 * y - x) ** 2 + 8 * y * x - 8 * y * x + 4 * y * (x + z - y)` by decide_tac >>
    `_ = (2 * y - x) ** 2 + 4 * (2 * y) * x + 4 * y * (x + z - y) - 8 * y * x` by fs[] >>
    `_ = (2 * y + x) ** 2 + 4 * y * (x + z - y) - 8 * y * x` by simp[binomial_sub_add] >>
    `_ = (2 * y) ** 2 + x ** 2 + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x` by simp[SUM_SQUARED] >>
    `_ = (2 * y) * (2 * y) + x ** 2 + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x` by metis_tac[EXP_2] >>
    `_ = x ** 2 + 4 * y * y + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x` by fs[] >>
    `_ = x ** 2 + 4 * y * (y + x) + 4 * y * (x + z - y) - 8 * y * x` by rw[LEFT_ADD_DISTRIB] >>
    `_ = x ** 2 + 4 * y * (y + x + (x + z - y)) - 8 * y * x` by rw[LEFT_ADD_DISTRIB] >>
    `_ = x ** 2 + 4 * y * (y + x + x + z - y) - 8 * y * x` by rw[LESS_EQ_ADD_SUB] >>
    `_ = x ** 2 + 4 * y * (x + x + z) - 4 * y * (x + x)` by decide_tac >>
    `_ = x ** 2 + 4 * y * (x + x + z - (x + x))` by decide_tac >>
    `_ = x ** 2 + 4 * y * z` by decide_tac >>
    simp[],
    `y <= x + z` by decide_tac >>
    `2 * y <= x` by decide_tac >>
    `(x - 2 * y) ** 2 + 4 * (y * (x + z - y)) =
    (x - 2 * y) ** 2 + 4 * y * (x + z - y)` by decide_tac >>
    `_ = (x - 2 * y) ** 2 + 8 * y * x - 8 * y * x + 4 * y * (x + z - y)` by decide_tac >>
    `_ = (x - 2 * y) ** 2 + 4 * x * (2 * y) + 4 * y * (x + z - y) - 8 * y * x` by fs[] >>
    `_ = (x + 2 * y) ** 2 + 4 * y * (x + z - y) - 8 * y * x` by simp[binomial_sub_add] >>
    `_ = x ** 2 + (2 * y) ** 2 + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x` by simp[SUM_SQUARED] >>
    `_ = x ** 2 + (2 * y) * (2 * y) + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x` by metis_tac[EXP_2] >>
    `_ = x ** 2 + 4 * y * y + 4 * y * x + 4 * y * (x + z - y) - 8 * y * x` by fs[] >>
    `_ = x ** 2 + 4 * y * (y + x) + 4 * y * (x + z - y) - 8 * y * x` by rw[LEFT_ADD_DISTRIB] >>
    `_ = x ** 2 + 4 * y * (y + x + (x + z - y)) - 8 * y * x` by rw[LEFT_ADD_DISTRIB] >>
    `_ = x ** 2 + 4 * y * (y + x + x + z - y) - 8 * y * x` by rw[LESS_EQ_ADD_SUB] >>
    `_ = x ** 2 + 4 * y * (x + x + z) - 4 * y * (x + x)` by decide_tac >>
    `_ = x ** 2 + 4 * y * (x + x + z - (x + x))` by decide_tac >>
    `_ = x ** 2 + 4 * y * z` by decide_tac >>
    simp[]
  ]
QED

(* Theorem: windmill t = windmill (zagier t) *)
(* Proof: by zagier_windmill, triple_parts. *)
Theorem zagier_windmill_alt:
  !t. windmill t = windmill (zagier t)
Proof
  metis_tac[zagier_windmill, triple_parts]
QED

(* Theorem: (x, y, z) IN mills n <=> zagier (x, y, z) IN mills n *)
(* Proof:
       (x, y, z) IN mills n
   <=> n = windmill (x,y,z)                    by mills_element
   <=> n = windmill (zagier (x,y,z))           by zagier_windmill
   <=> zagier (x,y,z) IN mills n               by mills_element_alt
*)
Theorem zagier_closure_iff:
  !n x y z. (x, y, z) IN mills n <=> zagier (x, y, z) IN mills n
Proof
  metis_tac[mills_element, mills_element_alt, zagier_windmill]
QED

(* Theorem: t IN mills n <=> zagier t IN mills n *)
(* Proof: by zagier_closure_iff, triple_parts. *)
Theorem zagier_closure_iff_alt:
  !n t. t IN mills n <=> zagier t IN mills n
Proof
  metis_tac[zagier_closure_iff, triple_parts]
QED

(* Theorem: windmill (x,y,z) = windmill ((zagier o flip) (x,y,z)) *)
(* Proof:
     windmill ((zagier o flip) (x,y,z))
   = windmill (zagier (flip (x,y,z)))          by o_THM
   = windmill (flip (x,y,z))                   by zagier_windmill_alt
   = windmill (x,y,z)                          by flip_windmill
*)
Theorem zagier_flip_windmill:
  !x y z. windmill (x,y,z) = windmill ((zagier o flip) (x,y,z))
Proof
  simp[GSYM zagier_windmill_alt, flip_windmill]
QED

(* Theorem: windmill t = windmill ((zagier o flip) t) *)
(* Proof: by zagier_flip_windmill, triple_parts. *)
Theorem zagier_flip_windmill_alt:
  !t. windmill t = windmill ((zagier o flip) t)
Proof
  metis_tac[zagier_flip_windmill, triple_parts]
QED

(* Theorem: fixes flip (mills n) = {(x,y,y) | n = windmill (x,y,y)} *)
(* Proof:
     fixes flip (mills n)
   = {t | t IN (mills n) /\ flip t = t}        by fixes_def
   = {(x,y,z) | (x,y,z) IN (mills n) /\ flip (x,y,z) = (x,y,z)}    by triple_parts
   = {(x,y,z) | (x,y,z) IN (mills n) /\ y = z} by flip_fix
   = {(x,y,y) | (x,y,y) IN (mills n)}          by simplification
   = {(x,y,y) | n = windmill (x,y,y)}          by mills_def
*)
Theorem flip_fixes:
  !n. fixes flip (mills n) = {(x,y,y) | n = windmill (x,y,y)}
Proof
  rw[fixes_def, mills_def, EXTENSION] >>
  metis_tac[flip_fix]
QED

(* Theorem: (x,y,z) IN fixes flip (mills n) <=> (n = windmill (x,y,z) /\ y = z) *)
(* Proof: by flip_fixes. *)
Theorem flip_fixes_element:
  !n x y z. (x,y,z) IN fixes flip (mills n) <=> (n = windmill (x,y,z) /\ y = z)
Proof
  simp[flip_fixes] >>
  metis_tac[]
QED
(* move to windmill.hol *)

(* ------------------------------------------------------------------------- *)
(* The zagier-flip permutation.                                              *)
(* ------------------------------------------------------------------------- *)

(* The three cases of Zagier map, or the Zagier-flip map.

zagier_def
|- !x y z. zagier (x,y,z) =
           if x < y - z then (x + 2 * z,z,y - z - x)
           else if x < 2 * y then (2 * y - x,y,x + z - y)
           else (x - 2 * y,x + z - y,y)
these cases are clean by checking (2 * y - x).
Note n = x² + 4yz, so z = (n - x²)/4y. Eliminating z:
           x < y - z
iff        x < y - (n - x²)/4y
iff      4xy < 4y² - n + x²
iff        n < 4y² - 4xy + x² = (2y - x)²
iff       √n < 2y - x

also       x < 2 * y
iff        0 < 2y - x

and the last case: 2y < x, or 2y - x < 0, or 2y - x = 0 in integer arithmetic.

zagier_flip_eqn
|- !x y z. (zagier o flip) (x,y,z) =
           if x < z - y then (x + 2 * y,y,z - y - x)
           else if x < 2 * z then (2 * z - x,z,x + y - z)
           else (x - 2 * z,x + y - z,z)
these cases are clean by checking (2 * z - x).
Note n = x² + 4yz, so y = (n - x²)/4z. Eliminating y:
             x < z - y
iff          x < z - (n - x²)/4z
iff        4xz < 4z² - n + x²
iff          n < 4z² - 4xz + x² = (2z - x)²
iff         √n < 2z - x

also         x < 2 * z
iff          0 < 2z - x

and the last case: 2z < x, or 2z - x < 0, or 2z - x = 0 in integer arithmetic.
*)

(* Define the map for each case of zagier_flip *)
Definition ping_def:
   ping (x,y,z) = (x + 2 * y, y, z - y - x)
End

Definition pong_def:
   pong (x,y,z) = (2 * z - x, z, x + y - z)
End

Definition pung_def:
   pung (x,y,z) = (x - 2 * z, x + y - z, z)
End

(* Theorem: ping (x,y,z) = (x + 2 * y,y,z - (x + y)) *)
(* Proof:
        ping (x,y,z)
      = (x + 2 * y,y,z - y - x)    by ping_def
      = (x + 2 * y,y,z - (x + y))  by SUB_RIGHT_SUB
*)
Theorem ping_alt:
  !x y z. ping (x,y,z) = (x + 2 * y,y,z - (x + y))
Proof
  simp[ping_def]
QED

(* Matrices:
   ping   A = [[1,2,0],[0,1,0],[-1,-1,1]]  A^2 = [[1,4,0],[0,1,0],[-2,-4,1]]  same form A^m = [[1,2m,0],[0,1,0],[-m,-m*m,1]]
   pong   B = [[-1,0,2],[0,0,1],[1,1,-1]]  B^2 = [[3,2,-4],[1,1,-1],[-2,-1,4]]
   pung   C = [[1,0,-2],[1,1,-1],[0,0,1]]  C^2 = [[1,0,-4],[2,1,-4],[0,0,1]]  same form C^m = [[1,0,-2m],[m,1,-m*m],[0,0,1]]
*)

(* Define the ping, pong, pung conditions *)
Definition is_ping_def:
   is_ping (x, y, z) <=> x < z - y
End
Definition is_pong_def:
   is_pong (x, y, z) <=> ~(x < z - y) /\ x < 2 * z
End
Definition is_pung_def:
   is_pung (x, y, z) <=> ~(x < z - y) /\ ~(x < 2 * z)
End

(* Theorem: is_ping (x,y,z) \/ is_pong (x,y,z) \/ is_pung (x,y,z) *)
(* Proof: by is_ping_def, is_pong_def, is_pung_def. *)
Theorem triple_cases:
  !x y z. is_ping (x,y,z) \/ is_pong (x,y,z) \/ is_pung (x,y,z)
Proof
  simp[is_ping_def, is_pong_def, is_pung_def]
QED

(* Theorem: is_ping t \/ is_pong t \/ is_pung t *)
(* Proof: by triple_cases, triple_parts. *)
Theorem triple_cases_alt:
  !t. is_ping t \/ is_pong t \/ is_pung t
Proof
  metis_tac[triple_cases, triple_parts]
QED

(* Theorem: is_ping t ==> ~is_pong t *)
(* Proof: by is_ping_def, is_pong_def. *)
Theorem ping_not_pong:
  !t. is_ping t ==> ~is_pong t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_ping_def, is_pong_def]
QED

(* Theorem: is_ping t ==> ~is_pung t *)
(* Proof: by is_ping_def, is_pung_def. *)
Theorem ping_not_pung:
  !t. is_ping t ==> ~is_pung t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_ping_def, is_pung_def]
QED

(* Theorem: is_pong t ==> ~is_ping t *)
(* Proof: by is_pong_def, is_ping_def. *)
Theorem pong_not_ping:
  !t. is_pong t ==> ~is_ping t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_pong_def, is_ping_def]
QED

(* Theorem: is_pong t ==> ~is_pung t *)
(* Proof: by is_pong_def, is_pung_def. *)
Theorem pong_not_pung:
  !t. is_pong t ==> ~is_pung t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_pong_def, is_pung_def]
QED

(* Theorem: is_pung t ==> ~is_ping t *)
(* Proof: by is_pung_def, is_ping_def. *)
Theorem pung_not_ping:
  !t. is_pung t ==> ~is_ping t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_pung_def, is_ping_def]
QED

(* Theorem: is_pung t ==> ~is_pong t *)
(* Proof: by is_pung_def, is_pong_def. *)
Theorem pung_not_pong:
  !t. is_pung t ==> ~is_pong t
Proof
  rpt strip_tac >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_pung_def, is_pong_def]
QED

(* Theorem: is_ping (x,y,z) <=> SQRT (windmill (x,y,z)) < 2 * z - x *)
(* Proof:
   Let n = windmill (x,y,z).
   Then n = x ** 2 + 4 * y * z                 by windmill_def
    and is_ping (x,y,z) <=> x < z - y          by is_ping_def

   If part: x < z - y ==> SQRT n < 2 * z - x
      By contradiction, suppose 2 * z - x <= SQRT n.
      Note x < 2 * z, 0 < z                    by x < z - y
          2 * z - x <= SQRT n
      ==> (2 * z - x) ** 2 <= (SQRT n) ** 2    by EXP_EXP_LE_MONO_IMP
      ==> (2 * z - x) ** 2 <= n                by SQ_SQRT_LE_alt, LESS_EQ_TRANS
      ==> 4 * z ** 2 + x ** 2 - 4 * z * x <= n                     by binomial_sub, x < 2 * z
      ==> x ** 2 + 4 * z * z - 4 * z * x <= x ** 2 + 4 * y * z     by EXP_2
      ==> 4 * z * (z - x) <= 4 * z * y         by LEFT_SUB_DISTRIB
      ==> z - x <= y                           by LT_MULT_LCANCEL, 0 < z
      ==> z - y <= x                           by inequaltiy
      This contradicts x < z - y.

   Only-if part: SQRT n < 2 * z - x ==> x < z - y
      Note 0 < 2 * z - x                       by SQRT n < 2 * z - x
        so x < 2 * z and 0 < z                 by inequality
           SQRT n < 2 * z - x
       <=> SQRT n < SQRT ((2 * z - x) ** 2)    by SQRT_OF_SQ
       ==> n < (2 * z - x) ** 2                by SQRT_LT_SQRT
       <=> x ** 2 + 4 * y * z < 4 * z ** 2 + x ** 2 - 4 * z * x
                                               by binomial_sub, x < 2 * z
       <=> 4 * y * z  < 4 * z * z - 4 * z * x  by EXP_2
       <=> (4 * z) * y < 4 * z * (z - x)       by LEFT_SUB_DISTRIB
       <=> y < z - x                           by LT_MULT_LCANCEL, 0 < z
       <=> y + x < z                           by inequality
       <=> x < z - y                           by inequality
*)
Theorem is_ping_alt:
  !x y z. is_ping (x,y,z) <=> SQRT (windmill (x,y,z)) < 2 * z - x
Proof
  rpt strip_tac >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  rw[is_ping_def, EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `2 * z - x <= SQRT n` by decide_tac >>
    `0 < z /\ x < 2 * z` by decide_tac >>
    `4 * z * x < 4 * z * z` by simp[] >>
    `(2 * z - x) ** 2 <= (SQRT n) ** 2` by fs[EXP_EXP_LE_MONO_IMP] >>
    `(SQRT n) ** 2 <= n` by simp[SQ_SQRT_LE_alt] >>
    `(2 * z - x) ** 2 = x ** 2 + 4 * z * z - 4 * z * x` by simp[binomial_sub, Once EXP_2] >>
    `_ = x ** 2 + 4 * z * (z - x)` by decide_tac >>
    `n = x ** 2 + 4 * z * y` by simp[windmill_def, Abbr`n`] >>
    `4 * z * (z - x) <= 4 * z * y` by decide_tac >>
    fs[],
    `n = x ** 2 + 4 * z * y` by simp[windmill_def, Abbr`n`] >>
    `n < (2 * z - x) ** 2` by metis_tac[SQRT_OF_SQ, SQRT_LT_SQRT] >>
    `(2 * z - x) ** 2 = x ** 2 + 4 * z * z - 4 * z * x` by fs[binomial_sub, Once EXP_2] >>
    `_ = x ** 2 + 4 * z * (z - x)` by decide_tac >>
    `4 * z * y < 4 * z * (z - x)` by decide_tac >>
    fs[]
  ]
QED

(* Theorem: is_pong (x,y,z) <=> 0 < 2 * z - x /\ 2 * z - x <= SQRT (windmill (x,y,z)) *)
(* Proof:
   Let n = windmill (x,y,z).
   If part: is_pong (x,y,z) <=> 0 < 2 * z - x /\ 2 * z - x <= SQRT n
      Note x < 2 * z               by is_pong_def
        so 0 < 2 * z - x           by inequaltiy
      Also ~is_ping (x,y,z)        by pong_not_ping
        so ~(SQRT n < 2 * z - x)   by is_ping_alt
        or 2 * z - x <= SQRT n     by NOT_LESS

   Only-if part: 0 < 2 * z - x /\ 2 * z - x <= SQRT n ==> is_pong (x,y,z)
      Note 2 * z - x <= SQRT n
       <=> ~(SQRT n < 2 * z - x)   by NOT_LESS
       <=> ~is_ping (x,y,z)        by is_ping_alt
       <=> ~(x < z - y)            by is_ping_def
       and x < 2 * z               by 0 < 2 * z - x
       ==> is_pong (x,y,z)         by is_pong_def
*)
Theorem is_pong_alt:
  !x y z. is_pong (x,y,z) <=> 0 < 2 * z - x /\ 2 * z - x <= SQRT (windmill (x,y,z))
Proof
  rw_tac bool_ss [EQ_IMP_THM] >-
  fs[is_pong_def] >-
  metis_tac[pong_not_ping, is_ping_alt, NOT_LESS] >>
  `~is_ping (x,y,z)` by fs[is_ping_alt] >>
  fs[is_ping_def, is_pong_def]
QED

(* Theorem: is_pung (x,y,z) <=> 2 * z - x = 0 *)
(* Proof:
   Let n = windmill (x,y,z).
   If part: is_pung (x,y,z) ==> 2 * z - x = 0
      Note ~is_ping (x,y,z)        by pung_not_ping
        so ~(SQRT n < 2 * z - x)   by is_ping_alt
       and ~is_pong (x,y,z)        by pung_not_pong
        so ~(0 < 2 * z - x)        by is_pong_alt
        or 2 * z - x = 0           by arithmetic

   Only-if part: 2 * z - x = 0 ==> is_pung (x,y,z)
      Note ~((SQRT n) < 2 * z - x) by 2 * z - x = 0
        so ~is_ping (x,y,z)        by is_ping_alt
      Also ~(0 < 2 * z - x)        by 2 * z - x = 0
        so ~is_pong (x,y,z)        by is_pong_alt
      Thus is_pung (x,y,z)         by triple_cases
*)
Theorem is_pung_alt:
  !x y z. is_pung (x,y,z) <=> 2 * z - x = 0
Proof
  rpt strip_tac >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  rw[EQ_IMP_THM] >| [
    `~(0 < 2 * z - x /\ 2 * z - x <= SQRT n)` by metis_tac[pung_not_pong, is_pong_alt] >>
    `~(SQRT n < 2 * z - x)` by metis_tac[pung_not_ping, is_ping_alt] >>
    decide_tac,
    `~(0 < 2 * z - x) /\ ~((SQRT n) < 2 * z - x)` by decide_tac >>
    metis_tac[is_ping_alt, is_pong_alt, triple_cases]
  ]
QED

(* Theorem: 2 * x < z ==> is_ping (x,x,z) *)
(* Proof:
      is_ping (x,x,z)
   <=> x < z - x               by is_ping_def
   <=> x + x < z               by SUB_LEFT_LESS
   <=> 2 * x < z               by arithmetic
   <=> T                       by given
*)
Theorem is_ping_x_x_z:
  !x z. 2 * x < z ==> is_ping (x,x,z)
Proof
  simp[is_ping_def]
QED

(*
> EVAL ``is_ping (1,1,0)``; <=> F: thm
> EVAL ``is_ping (1,1,1)``; <=> F: thm
> EVAL ``is_ping (1,1,2)``; <=> F: thm
> EVAL ``is_ping (1,1,3)``; <=> T: thm
*)

(* Theorem: 2 < k ==> is_ping (1,1,k) *)
(* Proof: by is_ping_x_x_z. *)
Theorem is_ping_1_1_k:
  !k. 2 < k ==> is_ping (1,1,k)
Proof
  simp[is_ping_x_x_z]
QED

(* Theorem: ~is_ping (x,y,y) *)
(* Proof:
   Note x < y - y is false         by arithmetic
     so ~is_ping (x,y,y)           by is_ping_def
*)
Theorem not_ping_x_y_y:
  !x y. ~is_ping (x,y,y)
Proof
  simp[is_ping_def]
QED

(* Theorem: 0 < x ==> is_pong (x,y,x) *)
(* Proof:
   Note x < x - y is false         by arithmetic, 0 < x
    but x < 2 * x is true          by arithmetic, 0 < x
     so is_pong (x,y,x)            by is_pong_def
*)
Theorem is_pong_x_y_x:
  !x y. 0 < x ==> is_pong (x,y,x)
Proof
  simp[is_pong_def]
QED

(* Idea: A triple (x,y,z) is a ping if, and only if, ping (x,y,z) has third positive. *)

(* Theorem: let (xx,yy,zz) = ping (x,y,z) in is_ping (x,y,z) <=> 0 < zz *)
(* Proof:
   Let xx = x + 2 * y,
       zz = z - x - y.
   Then ping (x,y,z) = (xx,y,zz)               by ping_def
        0 < zz
    <=> 0 < z - x - y
          = z - (x + y)                        by SUB_RIGHT_SUB
    <=> x + y < z                              by SUB_LESS_0
    <=>     x < z - y                          by SUB_LEFT_LESS
    <=> is_ping (x,y,z)                        by is_ping_def

    Or simply,
        is_ping (x,y,z)
    <=> x < z - y                              by is_ping_def
    <=> 0 < z - y - x                          by SUB_LESS_0
    <=> 0 < z - (y + x)                        by SUB_RIGHT_SUB
    <=> 0 < z - (x + y)                        by ADD_COMM
    <=> 0 < z - x - y                          by SUB_RIGHT_SUB
    <=> 0 < zz                                 by notation
*)
Theorem is_ping_by_ping:
  !x y z. let (xx,yy,zz) = ping (x,y,z) in is_ping (x,y,z) <=> 0 < zz
Proof
  rw[ping_def, is_ping_def]
QED

(* Idea: A triple (x,y,z) is a pung if pong (x,y,z) has first and third positive. *)

(* Theorem: let (xx,yy,zz) = pung (x,y,z) in 0 < xx ==> is_pung (x,y,z) *)
(* Proof:
   Let xx = 2 * z - x,
       zz = x + y - z.
   Then pong (x,y,z) = (xx,z,zz)               by pong_def
        0 < xx
    <=> 0 < 2 * z - x
    <=> x < 2 * z                              by SUB_LESS_0, [1]
        0 < zz
    <=> 0 < x + y - z
    <=> z < x + y                              by SUB_EQ_0
    ==> z - y < x                              by SUB_RIGHT_LESS, [2]
   Thus is_pung (x,y,z)                        by is_pung_def, [1][2]
*)
Theorem is_pong_by_pong:
  !x y z. let (xx,yy,zz) = pong (x,y,z) in 0 < xx /\ 0 < zz ==> is_pong (x,y,z)
Proof
  rw[pong_def, is_pong_def]
QED

(* Idea: A triple (x,y,z) is a pung if pong (x,y,z) has first positive. *)

(* Theorem: let (xx,yy,zz) = pung (x,y,z) in 0 < xx ==> is_pung (x,y,z) *)
(* Proof:
   Let xx = x - 2 * z,
       yy = x + y - z.
   Then pung (x,y,z) = (xx,yy,z)               by pung_def
        0 < xx
    <=> 0 < x - 2 * z
    <=> 2 * z < x                              by SUB_LESS_0
    ==> 2 * z - x = 0                          by SUB_EQ_0
    <=> is_pung (x,y,z)                        by is_pung_alt
*)
Theorem is_pung_by_pung:
  !x y z. let (xx,yy,zz) = pung (x,y,z) in 0 < xx ==> is_pung (x,y,z)
Proof
  rw[pung_def, is_pung_alt]
QED

(* Theorem: let (x,y,z) = ping t in is_ping t <=> 0 < z *)
(* Proof: by is_ping_by_ping, triple_parts. *)
Theorem is_ping_by_ping_alt:
  !t. let (x,y,z) = ping t in is_ping t <=> 0 < z
Proof
  metis_tac[is_ping_by_ping, triple_parts]
QED

(* Theorem: let (x,y,z) = pong t in 0 < x /\ 0 < z ==> is_pong t *)
(* Proof: by is_pong_by_pong, triple_parts. *)
Theorem is_pong_by_pong_alt:
  !t. let (x,y,z) = pong t in 0 < x /\ 0 < z ==> is_pong t
Proof
  metis_tac[is_pong_by_pong, triple_parts]
QED

(* Theorem: let (x,y,z) = pung t in 0 < x ==> is_pung t *)
(* Proof: by is_pung_by_pung, triple_parts. *)
Theorem is_pung_by_pung_alt:
  !t. let (x,y,z) = pung t in 0 < x ==> is_pung t
Proof
  metis_tac[is_pung_by_pung, triple_parts]
QED

(* ------------------------------------------------------------------------- *)
(* Zagier and Flip composition                                               *)
(* ------------------------------------------------------------------------- *)

(* Theorem: (zagier o flip) (x, y, z) =
            if is_ping (x,y,z) then ping (x,y,z)
            else if is_pong (x,y,z) then pong (x,y,z)
            else pung (x,y,z) *)
(* Proof: by zagier_flip_eqn, ping_def, pong_def, pung_def, is_ping_def, is_pong_def. *)
Theorem zagier_flip_thm:
  !x y z. (zagier o flip) (x, y, z) =
          if is_ping (x,y,z) then ping (x,y,z)
          else if is_pong (x,y,z) then pong (x,y,z)
          else pung (x,y,z)
Proof
  simp[zagier_flip_eqn, ping_def, pong_def, pung_def, is_ping_def, is_pong_def]
QED

(* Theorem: (zagier o flip) t = if is_ping t then ping t else if is_pong t then pong t else pung t *)
(* Proof: by zagier_flip_thm, triple_parts. *)
Theorem zagier_flip_alt:
  !t. (zagier o flip) t = if is_ping t then ping t
                          else if is_pong t then pong t else pung t
Proof
  metis_tac[zagier_flip_thm, triple_parts]
QED

(* Theorem: is_ping t ==> (zagier o flip) t = ping t *)
(* Proof: by zagier_flip_alt. *)
Theorem zagier_flip_ping:
  !t. is_ping t ==> (zagier o flip) t = ping t
Proof
  simp[zagier_flip_alt]
QED

(* Theorem: is_pong t ==> (zagier o flip) t = pong t *)
(* Proof: by zagier_flip_alt, pong_not_ping. *)
Theorem zagier_flip_pong:
  !t. is_pong t ==> (zagier o flip) t = pong t
Proof
  simp[zagier_flip_alt, pong_not_ping]
QED

(* Theorem: is_pung t ==> (zagier o flip) t = pung t *)
(* Proof: by zagier_flip_alt, pung_not_ping, pung_not_pong. *)
Theorem zagier_flip_pung:
  !t. is_pung t ==> (zagier o flip) t = pung t
Proof
  simp[zagier_flip_alt, pung_not_ping, pung_not_pong]
QED

(* Theorem: (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
            FUNPOW (zagier o flip) m (x,y,z) = FUNPOW ping m (x,y,z) *)
(* Proof:
   Let t = (x,y,z).
   By induction on m.
   Base: (!j. j < 0 ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (zagier o flip) 0 t = FUNPOW ping 0 t
      FUNPOW (zagier o flip) 0 t
    = t                            by FUNPOW_0
    = FUNPOW ping 0 t              by FUNPOW_0

   Step: (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (zagier o flip) m t = FUNPOW ping m t ==>
         (!j. j < SUC m ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (zagier o flip) (SUC m) t = FUNPOW ping (SUC m) t
      Note !j. j < SUC m ==> is_ping (FUNPOW ping j t)
       <=> !j. (j < m \/ j = m) ==> is_ping (FUNPOW ping j t)      by arithmetic
       <=> is_ping (FUNPOW ping m t) /\
           !j. j < m ==> is_ping (FUNPOW ping j t)                 by DISJ_IMP_THM

           is_ping (FUNPOW ping (SUC m) t)
       <=> is_ping (ping (FUNPOW ping m t))                        by FUNPOW_SUC
       <=> is_ping ((zagier o flip) (FUNPOW ping m t))             by zagier_flip_ping, [1]

        FUNPOW (zagier o flip) (SUC m) t
      = (zagier o flip) (FUNPOW (zagier o flip) m t)               by FUNPOW_SUC
      = (zagier o flip) (FUNPOW ping m t)                          by induction hypothesis
      = ping (FUNPOW ping m t)                                     by zagier_flip_ping, [1]
      = FUNPOW ping (SUC m) t                                      by FUNPOW_SUC
*)
Theorem zagier_flip_ping_funpow:
  !m x y z. (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
            FUNPOW (zagier o flip) m (x,y,z) = FUNPOW ping m (x,y,z)
Proof
  rpt strip_tac >>
  qabbrev_tac `t = (x,y,z)` >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_ping (FUNPOW ping m t) /\ !j. j < m ==> is_ping (FUNPOW ping j t)` by metis_tac[] >>
  fs[FUNPOW_SUC, zagier_flip_ping]
QED

(* Theorem: (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (zagier o flip) m t = FUNPOW ping m t *)
(* Proof: by zagier_flip_ping_funpow, triple_parts. *)
Theorem zagier_flip_ping_funpow_alt:
  !m t. (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> FUNPOW (zagier o flip) m t = FUNPOW ping m t
Proof
  metis_tac[zagier_flip_ping_funpow, triple_parts]
QED

(* Theorem: (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            FUNPOW (zagier o flip) m (x,y,z) = FUNPOW pung m (x,y,z) *)
(* Proof:
   Let t = (x,y,z).
   By induction on m.
   Base: (!j. j < 0 ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (zagier o flip) 0 t = FUNPOW pung 0 t
      FUNPOW (zagier o flip) 0 t
    = t                            by FUNPOW_0
    = FUNPOW pung 0 t              by FUNPOW_0

   Step: (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (zagier o flip) m t = FUNPOW pung m t ==>
         (!j. j < SUC m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (zagier o flip) (SUC m) t = FUNPOW pung (SUC m) t
      Note !j. j < SUC m ==> is_pung (FUNPOW pung j t)
       <=> !j. (j < m \/ j = m) ==> is_pung (FUNPOW pung j t)        by arithmetic
       <=> is_pung (FUNPOW pung m t) /\
           !j. j < m ==> is_pung (FUNPOW pung j t)                   by DISJ_IMP_THM

           is_pung (FUNPOW pung (SUC m) t)
       <=> is_pung (pung (FUNPOW pung m t))                          by FUNPOW_SUC
       <=> is_pung ((zagier o flip) (FUNPOW pung m t))               by zagier_flip_pung, [1]

        FUNPOW (zagier o flip) (SUC m) t
      = (zagier o flip) (FUNPOW (zagier o flip) m t)                 by FUNPOW_SUC
      = (zagier o flip) (FUNPOW pung m t)                            by induction hypothesis
      = pung (FUNPOW pung m t)                                       by zagier_flip_pung, [1]
      = FUNPOW pung (SUC m) t                                        by FUNPOW_SUC
*)
Theorem zagier_flip_pung_funpow:
  !m x y z. (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            FUNPOW (zagier o flip) m (x,y,z) = FUNPOW pung m (x,y,z)
Proof
  rpt strip_tac >>
  qabbrev_tac `t = (x,y,z)` >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_pung (FUNPOW pung m t) /\ !j. j < m ==> is_pung (FUNPOW pung j t)` by metis_tac[] >>
  fs[FUNPOW_SUC, zagier_flip_pung]
QED

(* Idea: after a pung, there is no ping. *)

(* Theorem: 2 * z <= x ==> ~is_ping (pung (x,y,z)) *)
(* Proof:
   Note pung (x,y,z)
      = (x - 2 * z,x + y - z,z)                by pung_def
   If this is is_ping,
      x - 2 * z < z - (x + y - z)              by is_ping_def
                = (2 * z - x) - y              by arithmetic
                = 0                            by 2 * z <= x
   This is impossible, a contradiction.
*)
Theorem pung_next_not_ping:
  !x y z. 2 * z <= x ==> ~is_ping (pung (x,y,z))
Proof
  simp[pung_def, is_ping_def]
QED

(* Theorem: is_pung t ==> ~is_ping (pung t) *)
(* Proof: by pung_next_not_ping, is_pung_def gives ~(x < 2 * z) *)
Theorem pung_next_not_ping_alt:
  !t. is_pung t ==> ~is_ping (pung t)
Proof
  metis_tac[pung_next_not_ping, is_pung_def, triple_parts, NOT_LESS]
QED

(* Theorem: is_ping ((zagier o flip) t) ==> ~is_pung t *)
(* Proof:
   By contradiction, suppose is_pung t.
   Then (zagier o flip) t = pung t             by zagier_flip_pung
    but is_ping (pung t) = F                   by pung_next_not_ping_alt
*)
Theorem ping_before_not_pung_alt:
  !t. is_ping ((zagier o flip) t) ==> ~is_pung t
Proof
  spose_not_then strip_assume_tac >>
  fs[zagier_flip_pung, pung_next_not_ping_alt]
QED

(* This means: after a pung, either a pong or a pung, no ping.  *)
(* This means: before a ping, either a ping or a pong, no pung. *)

(* Theorem: (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (zagier o flip) m t = FUNPOW pung m t *)
(* Proof: by zagier_flip_pung_funpow, triple_parts. *)
Theorem zagier_flip_pung_funpow_alt:
  !m t. (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW (zagier o flip) m t = FUNPOW pung m t
Proof
  metis_tac[zagier_flip_pung_funpow, triple_parts]
QED

(* Idea: ping (x,y,z) will generally increase the mind, by first of 5 cases. *)

(* Theorem: mind (ping (x,y,z)) = x + 2 * y *)
(* Proof:
   Let t = (x,y,z).
   Then ping t = (x + 2 * y,y,z - y - x)       by ping_def

   For mind (ping t),
   If x + 2 * y < y - (z - y - x)
   or x + 2 * y < x + 2 * y - z, impossible.
   Otherwise, if x + 2 * y < y, also impossible.
   Otherwise,
        mind (ping t)
      = x + 2 * y                              by mind_def
*)
Theorem mind_of_ping:
  !x y z. mind (ping (x,y,z)) = x + 2 * y
Proof
  simp[ping_def, mind_def]
QED

(* Theorem: mind (x,y,z) <= mind (ping (x,y,z)) *)
(* Proof:
   Let t = (x,y,z).
   Then ping t = (x + 2 * y,y,z - y - x)       by ping_def

   Case 1: x < y - z
   Note mind t = x + 2 * z                     by mind_def
        mind (ping t)
      = x + 2 * y                              by mind_of_ping
      > x + 2 * (x + z)                        by x + z < y
      >= x + 2 * z = mind t

   Case 2: ~(x < y - z) /\ x < y
   Note mind t = 2 * y - x                     by mind_def
        mind (ping t)
      = x + 2 * y                              by mind_of_ping
      >= 2 * y
      >= 2 * y - x = mind t

   Case 3: otherwise, ~(x < y - z) /\ ~(x < y)
   Note mind t = x                             by mind_def
        mind (ping t)
      = x + 2 * y                              by mind_of_ping
      >= x = mind t                            by inequality

   Overall, mind t <= mind (ping t).
*)
Theorem mind_rise_by_ping:
  !x y z. mind (x,y,z) <= mind (ping (x,y,z))
Proof
  simp[mind_of_ping, mind_def]
QED

(* Theorem: mind t <= mind (ping t) *)
(* Proof: by mind_rise_by_ping, triple_parts. *)
Theorem mind_rise_by_ping_alt:
  !t. mind t <= mind (ping t)
Proof
  metis_tac[mind_rise_by_ping, triple_parts]
QED

(* Theorem: 0 < x /\ 0 < y ==> mind (x,y,z) < mind (ping (x,y,z)) *)
(* Proof:
   Let t = (x,y,z).
   Case 1: x < y - z
   Note mind t = x + 2 * z                     by mind_def
        mind (ping t)
      = x + 2 * y                              by mind_of_ping
      > x + 2 * (x + z)                        by x + z < y
      > x + 2 * z = mind t                     by 0 < x
   Case 2: ~(x < y - z) /\ x < y
   Note mind t = 2 * y - x                     by mind_def
        mind (ping t)
      = x + 2 * y                              by mind_of_ping
      > 2 * y                                  by 0 < x
      > 2 * y - x = mind t                     by 0 < x
   Case 3: otherwise
   Note mind t = x                             by mind_def
        mind (ping t)
      = x + 2 * y                              by mind_of_ping
      > x = mind t                             by 0 < y
*)
Theorem mind_inc_by_ping:
  !x y z. 0 < x /\ 0 < y ==> mind (x,y,z) < mind (ping (x,y,z))
Proof
  simp[mind_def, mind_of_ping]
QED

(* Idea: pung (x,y,z) will generally decrease the mind, by last of 5 cases. *)

(* Theorem: 2 * z <= x ==> mind (pung (x,y,z)) = x*)
(* Proof:
   Let t = (x,y,z).
   Then pung t = (x - 2 * z,x + y - z,z)       by pung_def
   For mind definition,
   If x - 2 * z < (x + y - z) - z
   or x - 2 * z < y + (x - 2 * z)              by 2 * z <= x
   If 0 < y, this is true,
   Thus mind (pung t)
      = (x - 2 * z) + 2 * z                    by mind_def
      = x
   When y = 0, this is false, so check:
   If x - 2 * z < x + 0 - z = z + (x - 2 * z)
   If 0 < z, this is true,
   Thus mind (pung t)
      = 2 * (x + 0 - z) - (x - 2 * z)          by mind_def
      = x                                      by 2 * z <= x
   When z = 0, this is false,
   Thus mind (pung t)
      = x - 2 * 0                              by mind_def
      = x
*)
Theorem mind_of_pung:
  !x y z. 2 * z <= x ==> mind (pung (x,y,z)) = x
Proof
  rw[pung_def] >>
  `0 < y \/ y = 0` by decide_tac >-
  simp[mind_def] >>
  `0 < z \/ z = 0` by decide_tac >-
  simp[mind_def] >>
  simp[mind_def]
QED

(* Theorem: 2 * z <= x ==> mind (pung (x,y,z)) <= mind (x,y,z) *)
(* Proof:
   Let t = (x,y,z).

   Case 1: x < y - z
   Note mind t = x + 2 * z                     by mind_def
        mind (pung t)
      = x                                      by mind_of_pung
      <= x + 2 * z = mind t

   Case 2: ~(x < y - z) /\ x < y
   That is, 2 * x < 2 * y
        ==> x < 2 * y - x                      by inequality [1]
   Note mind t = 2 * y - x                     by mind_def
        mind (pung t)
      = x                                      by mind_of_pung
      < 2 * y - x = mind t                     by [1]

   Case 3: otherwise, ~(x < y - z) /\ ~(x < y)
   Note mind t = x                             by mind_def
        mind (pung t)
      = x = mind t                             by mind_of_pung

   Overall, mind (pung t) <= mind t.
*)
Theorem mind_fall_by_pung:
  !x y z. 2 * z <= x ==> mind (pung (x,y,z)) <= mind (x,y,z)
Proof
  simp[mind_def, mind_of_pung]
QED

(* Theorem: is_pung t ==> mind (pung t) <= mind t *)
(* Proof: by mind_fall_by_pung, is_pung_def. *)
Theorem mind_fall_by_pung_alt:
  !t. is_pung t ==> mind (pung t) <= mind t
Proof
  metis_tac[mind_fall_by_pung, is_pung_def, triple_parts, NOT_LESS]
QED

(* ------------------------------------------------------------------------- *)
(* Path on principal orbit of (zagier o flip) iteration.                     *)
(* ------------------------------------------------------------------------- *)

(* Define the path from flip of zagier-fix to flip-fix. *)
Definition path_def:
   path n 0 = [(1,n DIV 4,1)] /\ (* flip of (1,1,n DIV 4) *)
   path n (SUC k) = SNOC ((zagier o flip) (LAST (path n k))) (path n k)
End

(* Extract theorems *)
Theorem path_0 = path_def |> CONJUNCT1;
(* val path_0 = |- !n. path n 0 = [(1,n DIV 4,1)]: thm *)
Theorem path_suc = path_def |> CONJUNCT2;
(* val path_suc = |- !n k. path n (SUC k) = SNOC ((zagier o flip) (LAST (path n k))) (path n k): thm *)

(*
> EVAL ``path 61 6``;
val it = |- path 61 6 = [(1,15,1); (1,1,15); (3,1,13); (5,1,9); (7,1,3); (1,5,3); (5,3,3)]: thm
> EVAL ``path 89 11``;
val it = |- path 89 11 = [(1,22,1); (1,1,22); (3,1,20); (5,1,16); (7,1,10); (9,1,2); (5,8,2);
                          (1,11,2); (3,2,10); (7,2,5); (3,5,4); (5,4,4)]: thm
*)


(* Theorem: path n 1 = [(1,n DIV 4,1); (1,1,n DIV 4)] *)
(* Proof:
   Let f = zagier o flip,
       t = (1,n DIV 4,1).
   Note path n 0 = [t]             by path_0
     path n 1
   = path n (SUC 0)                by ONE
   = SNOC (f (LAST [t]) [t]        by path_suc
   = SNOC (f t) [t]                by LAST_SING
   = [t; f t]                      by SNOC
   = [t; (1,1,n DIV 4)]            by zagier_flip_x_y_x
*)
Theorem path_1:
  !n. path n 1 = [(1,n DIV 4,1); (1,1,n DIV 4)]
Proof
  rpt strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `t = (1,n DIV 4,1)` >>
  `path n (SUC 0) = [t; f t]` suffices_by metis_tac[zagier_flip_x_y_x, ONE, DECIDE``0 < 1``] >>
  simp[path_suc, path_0, Abbr`f`, Abbr`t`]
QED

(* Theorem: path n k <> [] *)
(* Proof:
   If k = 0,   path n 0
             = [(1,n DIV 4,1)]     by path_0
             <> []                 by NOT_NIL_CONS
   If k = SUC j, for some j.
               path n (SUC j)
             = SNOC h ls           by path_suc, where
                                   ls = path n j
                                   h = (zagier o flip) (LAST ls)
             <> []                 by NOT_SNOC_NIL
*)
Theorem path_not_nil:
  !n k. path n k <> []
Proof
  metis_tac[num_CASES, path_def, NOT_NIL_CONS, NOT_SNOC_NIL]
QED

(* Theorem: LENGTH (path n k) = k + 1 *)
(* Proof:
   Let f = zagier o flip.
   By induction on k.
   Base: LENGTH (path n 0) = 0 + 1
         LENGTH (path n 0)
       = LENGTH [(1,n DIV 4,1)]                by path_0
       = 1 = 0 + 1                             by LENGTH_SING
   Step: LENGTH (path n k) = k + 1 ==> LENGTH (path n (SUC k)) = SUC k + 1
         LENGTH (path n (SUC k))
       = LENGTH (SNOC (f (LAST ls)) ls)        by path_suc, where ls = path n k
       = SUC (LENGTH ls)                       by LENGTH_SNOC
       = SUC (n + 1)                           by induction hypothesis
       = SUC n + 1                             by arithmetic
*)
Theorem path_length:
  !n k. LENGTH (path n k) = k + 1
Proof
  strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  Induct >-
  simp[path_0] >>
  simp[path_suc, LENGTH_SNOC]
QED

(* Theorem: HD (path n k) = (1,n DIV 4,1) *)
(* Proof:
   Let f = zagier o flip,
       t = (1,n DIV 4,1).
   By induction on k.
   Base: HD (path n 0) = t
         HD (path n k)
       = HD [t]                                by path_0
       = t                                     by HD
   Step: HD (path n k) = t ==> HD (path n (SUC k)) = t
       Let ls = path n k.
       Note ls <> []                           by path_not_nil
         so ls = HD ls::TL ls                  by LIST_NOT_NIL
         HD (path n (SUC k))
       = HD (SNOC (f (LAST ls)) ls)            by path_suc
       = HD (HD ls::SNOC (f (LAST ls) (TL ls)) by SNOC
       = HD ls                                 by HD
       = t                                     by induction hypothesis
*)
Theorem path_head:
  !n k. HD (path n k) = (1,n DIV 4,1)
Proof
  strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `t = (1,n DIV 4,1)` >>
  Induct >-
  simp[path_0, Abbr`t`] >>
  qabbrev_tac `ls = path n k` >>
  `ls <> []` by simp[path_not_nil, Abbr`ls`] >>
  `ls = HD ls :: TL ls` by simp[GSYM LIST_NOT_NIL] >>
  metis_tac[path_suc, SNOC, HD]
QED

(* Theorem: LAST (path n k) = FUNPOW (zagier o flip) k (1,n DIV 4,1) *)
(* Proof:
   Let f = zagier o flip,
       t = (1,n DIV 4,1).
   By induction on j.
   Base: LAST (path n 0) = FUNPOW f 0 t
         LAST (path n 0)
       = LAST [t]                              by path_0
       = t                                     by LAST_CONS
   Step: LAST (path n k) = FUNPOW f n t ==> LAST (path n (SUC k)) = FUNPOW f (SUC n) t
       Let ls = path n k.
         LAST (path n (SUC k))
       = LAST (SNOC (f (LAST ls)) ls)          by path_suc
       = f (LAST ls)                           by LAST_SNOC
       = f (FUNPOW f n t)                      by induction hypothesis
       = FUNPOW f (SUC n) t                    by FUNPOW_SUC
*)
Theorem path_last:
  !n k. LAST (path n k) = FUNPOW (zagier o flip) k (1,n DIV 4,1)
Proof
  strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `t = (1,n DIV 4,1)` >>
  Induct >-
  simp[path_0, LAST_CONS, Abbr`t`] >>
  metis_tac[path_suc, LAST_SNOC, FUNPOW_SUC]
QED

(* Theorem: HD (path n k) = EL 0 (path n k) *)
(* Proof: by EL. *)
Theorem path_head_alt:
  !n k. HD (path n k) = EL 0 (path n k)
Proof
  simp[]
QED

(* Theorem: LAST (path n k) = EL k (path n k) *)
(* Proof:
   Note LENGTH (path n k) = k + 1              by path_length
     so path n k <> []                         by LENGTH_EQ_0
        LAST (path n k)
      = EL (PRE (k + 1)) (path n k)            by LAST_EL
      = EL k (path n k)                        by arithmetic
*)
Theorem path_last_alt:
  !n k. LAST (path n k) = EL k (path n k)
Proof
  rpt strip_tac >>
  `LENGTH (path n k) = k + 1 ` by simp[path_length] >>
  `k + 1 <> 0 /\ PRE (k + 1) = k` by decide_tac >>
  metis_tac[LAST_EL, LENGTH_EQ_0]
QED

(* Idea: the tail of (path n (1 + m)) is an iterate trace up to m. *)

(* Theorem: TL (path n (1 + m)) = iterate_trace (1,1,n DIV 4) (zagier o flip) m *)
(* Proof:
   Let f = zagier o flip,
       u = (1,1,n DIV 4),
       v = (1,n DIV 4,1).
   The goal is to show: TL (path n (1 + m)) = iterate_trace u f m

   By induction on m.
   Base: TL (path n (1 + 0)) = iterate_trace u f 0
         TL (path n (1 + 0))
       = TL (path n 1)
       = TL [(1,n DIV 4,1); u]                 by path_1
       = [u]                                   by TL
       = iterate_trace u f 0                   by iterate_trace_0

   Step: TL (path n (1 + m)) = iterate_trace u f m ==>
         TL (path n (1 + SUC m)) = iterate_trace u f (SUC m)
         TL (path n (1 + SUC m))
       = TL (path n (SUC (1 + m)))             by ADD
       = TL (SNOC (f (LAST (path n (1 + m)))) (path n (1 + m)))     by path_suc
       = SNOC (f (LAST (path n (1 + m)))) (TL (path n (1 + m)))     by TL_SNOC, path n (1 + m) <> []
       = SNOC (f (LAST (path n (1 + m)))) (iterate_trace u f m)     by induction hypothesis
       = SNOC (f (FUNPOW f (1 + m) v)) (iterate_trace u f m)        by path_last
       = SNOC (FUNPOW f (1 + m) (f v)) (iterate_trace u f m)        by FUNPOW_SUC, FUNPOW
       = SNOC (FUNPOW f (SUC m) u) (iterate_trace u f m)            by zagier_flip_x_y_x
       = iterate_trace u f (SUC m)                                  by iterate_trace_suc
*)
Theorem path_tail_alt:
  !n m. TL (path n (1 + m)) = iterate_trace (1,1,n DIV 4) (zagier o flip) m
Proof
  rpt strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  Induct_on `m` >-
  simp[path_1, iterate_trace_0] >>
  qabbrev_tac `v = (1,n DIV 4,1)` >>
  `f (LAST (path n (1 + m))) = f (FUNPOW f (1 + m) v)` by metis_tac[path_last] >>
  `_ = FUNPOW f (1 + m) (f v)` by simp[GSYM FUNPOW_SUC, FUNPOW] >>
  `_ = FUNPOW f (1 + m) u` by simp[zagier_flip_x_y_x, Abbr`u`, Abbr`v`, Abbr`f`] >>
  `TL (path n (1 + SUC m)) = TL (SNOC (f (LAST (path n (1 + m)))) (path n (1 + m)))` by fs[path_suc, GSYM ADD1, Abbr`f`] >>
  `_ = SNOC (FUNPOW f (1 + m) u) (TL (path n (1 + m)))` by metis_tac[TL_SNOC, NULL_EQ, path_not_nil] >>
  `_ = SNOC (FUNPOW f (1 + m) u) (iterate_trace u f m)` by fs[] >>
  simp[iterate_trace_suc, ADD1]
QED

(* Idea: the tail of (path n k) has elements all distinct. *)

(* Theorem: tik n /\ ~square n /\ k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) ==>
            ALL_DISTINCT (TL (path n k)) *)
(* Proof:
   Let s = mills n,
       f = zagier o flip,
       u = (1,1,n DIV 4),
       p = iterate_period f u.
   Note FINITE s                               by mills_finite_non_square, ~square n
    and zagier involute s                      by zagier_involute_mills, tik n
    and flip involute s                        by flip_involute_mills
     so f PERMUTES s                           by involute_involute_permutes
   Also u IN s                                 by mills_element_trivial, tik n
   Thus 0 < p                                  by iterate_period_pos
    and HALF p < p                             by HALF_LT, 0 < p

     TL (path n k)
   = TL (path n (1 + HALF p))                  by notation
   = iterate_trace u f (HALF p)                by path_tail_alt

   Thus ALL_DISTINCT (TL (path n k))           by iterate_trace_all_distinct
*)
Theorem path_tail_all_distinct:
  !n k. tik n /\ ~square n /\ k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) ==>
        ALL_DISTINCT (TL (path n k))
Proof
  rpt strip_tac >>
  qabbrev_tac `s = mills n` >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  qabbrev_tac `p = iterate_period f u` >>
  `FINITE s` by simp[mills_finite_non_square, Abbr`s`] >>
  `zagier involute s` by metis_tac[zagier_involute_mills, ONE_NOT_0] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `f PERMUTES s` by fs[involute_involute_permutes, Abbr`f`] >>
  `u IN s` by simp[mills_element_trivial, Abbr`s`, Abbr`u`] >>
  `0 < p` by metis_tac[iterate_period_pos] >>
  `HALF p < p` by simp[HALF_LT] >>
  metis_tac[path_tail_alt, iterate_trace_all_distinct]
QED

(* Theorem: path n k = [(1,n DIV 4,1)] <=> k = 0 *)
(* Proof:
   Note path n k <> []             by path_not_nil
     so ?h t. path n k = h::t      by list_CASES
    and h = (1,n DIV 4,1)          by path_head
    and k + 1 = LENGTH (path n k)  by path_length
              = SUC (LENGTH t)     by LENGTH
    ==> LENGTH t = k               by arithmetic
        path n k = [(1,n DIV 4,1)]
    <=> t = []                     by above
    <=> LENGTH t = 0               by LENGTH_EQ_0
    <=> k = 0                      by above
*)
Theorem path_eq_sing:
  !n k. path n k = [(1,n DIV 4,1)] <=> k = 0
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `?h t. ls = h::t`  by metis_tac[path_not_nil, list_CASES] >>
  `h = (1,n DIV 4,1)` by metis_tac[path_head, HD] >>
  `k + 1 = LENGTH ls` by fs[path_length, Abbr`ls`] >>
  `_ = SUC (LENGTH t)` by simp[] >>
  `LENGTH t = k` by decide_tac >>
  `h::t = [h] <=> t = []` by simp[] >>
  metis_tac[LENGTH_EQ_0]
QED

(* Theorem: LENGTH (FRONT (path n k)) = k *)
(* Proof:
   Let ls = path n k.
   Then ls <> []               by path_not_nil
     so LENGTH (FRONT ls)
      = PRE (LENGTH ls)        by LENGTH_FRONT
      = PRE (k + 1)            by path_length
      = k                      by PRE, ADD1
*)
Theorem path_front_length:
  !n k. LENGTH (FRONT (path n k)) = k
Proof
  simp[path_not_nil, path_length, LENGTH_FRONT]
QED

(* Theorem: 0 < k ==> HD (FRONT (path n k)) = (1,n DIV 4,1) *)
(* Proof:
   Let ls = path n k.
   Note ls <> []                   by path_not_nil
    and LENGTH ls = k + 1          by path_length
     so LENGTH (FRONT ls) = k      by LENGTH_FRONT

      HD (FRONT ls)
    = EL 0 (FRONT ls)              by EL
    = EL 0 ls                      by FRONT_EL, 0 < k
    = HD ls                        by EL
    = (1,1,n DIV 4)                by path_head
*)
Theorem path_front_head:
  !n k. 0 < k ==> HD (FRONT (path n k)) = (1,n DIV 4,1)
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `ls <> []` by simp[path_not_nil, Abbr`ls`] >>
  `LENGTH (FRONT ls) = k` by fs[LENGTH_FRONT, path_length, Abbr`ls`] >>
  metis_tac[FRONT_EL, EL, path_head]
QED

(* Theorem: j <= k ==> EL j (path n k) = FUNPOW (zagier o flip) j (1,1,n DIV 4) *)
(* Proof:
   Let f = zagier o flip,
       t = (1,n DIV 4,1).
   By induction on k.
   Base: !j. j <= 0 ==> EL j (path n 0) = FUNPOW f j t
       That is, j = 0.
         EL 0 (path n 0)
       = HD (path n 0)                         by EL
       = HD [t]                                by path_0
       = t                                     by HD
       = FUNPOW f 0 t                          by FUNPOW_0
   Step: !j. j <= k ==> EL j (path n k) = FUNPOW f j t ==>
         !j. j <= SUC k ==> EL j (path n (SUC k)) = FUNPOW f j t
       If j <= k, then j < k + 1.
          Let ls = path n k.
          Then LENGTH ls = k + 1               by path_length
            EL j (path n (SUC k))
          = EL j (SNOC (f (LAST ls)) ls)       by path_suc
          = EL j ls                            by EL_SNOC, j < LENGTH ls
          = FUNPOW f j t                       by induction hypothesis
       Otherwise, j = SUC k.
          Let ls = path n (SUC k).
          Note LENGTH ls = (SUC k) + 1         by path_length
            so PRE (LENGTH ls) = SUC k         by arithmetic
           and ls <> []                        by path_not_nil
            EL (SUC k) (path n (SUC k))
          = LAST (path n (SUC k))              by LAST_EL
          = FUNPOW f (SUC k) t                 by path_last
*)
Theorem path_element_eqn:
  !n k j. j <= k ==> EL j (path n k) = FUNPOW (zagier o flip) j (1,n DIV 4,1)
Proof
  strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  qabbrev_tac `t = (1,n DIV 4,1)` >>
  Induct >-
  simp[path_0, Abbr`t`] >>
  rpt strip_tac >>
  Cases_on `j <= k` >| [
    `j < k + 1` by decide_tac >>
    `LENGTH (path n k) = k + 1` by simp[path_length] >>
    simp[path_suc, EL_SNOC],
    `j = SUC k /\ PRE (j + 1) = j` by decide_tac >>
    `LENGTH (path n j) = j + 1` by simp[path_length] >>
    `path n j <> []` by simp[path_not_nil] >>
    metis_tac[LAST_EL, path_last]
  ]
QED
(* This is a major result. *)

(* Theorem: j < k ==> EL (SUC j) (path n k) = (zagier o flip) (EL j (path n k)) *)
(* Proof:
   Let f = zagier o flip,
       t = (1,n DIV 4,t).
   Note SUC j <= k.
     EL (SUC j) (path n k)
   = FUNPOW f (SUC j) t            by path_element_eqn
   = f (FUNPOW f j t)              by FUNPOW_SUC
   = f (EL j (path n k))           by path_element_eqn
*)
Theorem path_element_suc:
  !n k j. j < k ==> EL (SUC j) (path n k) = (zagier o flip) (EL j (path n k))
Proof
  simp[path_element_eqn, FUNPOW_SUC]
QED
(* Note: this is the key relationship between path elements. *)

(* Theorem: j + h <= k ==> EL (j + h) (path n k) = FUNPOW (zagier o flip) h (EL j (path n k)) *)
(* Proof:
   Let ls = path n k,
       t = (1,n DIV 4,1),
       f = zagier o flip.
     EL (j + h) ls
   = FUNPOW f (j + h) t            by path_element_eqn
   = FUNPOW f (h + j) t            by ADD_COMM
   = FUNPOW f h (FUNPOW f j t)     by FUNPOW_ADD
   = FUNPOW f h (EL j ls)          by path_element_eqn
*)
Theorem path_element_thm:
  !n k j h. j + h <= k ==> EL (j + h) (path n k) = FUNPOW (zagier o flip) h (EL j (path n k))
Proof
  rw[path_element_eqn] >>
  simp[FUNPOW_ADD]
QED

(* Theorem: tik n /\ j < k ==> EL j (path n k) IN mills n *)
(* Proof:
   Let ls = path n k.
   By induction on j.
   Base: 0 <= k ==> EL 0 ls IN mills n
         EL 0 ls
       = HD ls                     by EL
       = (1,n DIV 4,1)             by path_head
     and (1,n DIV 4,1) IN mills n  by mills_element_trivial_flip, tik n

   Step: j <= k ==> EL j ls IN mills n ==>
         SUC j <= k ==> EL (SUC j) ls IN mills n
       Note SUC j <= k ==> j < k, making j <= k.
         EL (SUC j) ls
       = (zagier o flip) (EL j ls)             by path_element_suc, j < k
       = flip (zagier (EL j ls))               by composition
     Note (EL j ls) IN mills n                 by inductive hypothesis, j <= k
      ==> zagier (EL j ls) IN mills n          by zagier_closure
      ==> flip (zagier (EL j ls)) IN mills n   by flip_closure
*)
Theorem path_element_in_mills:
  !n k j. tik n /\ j <= k ==> EL j (path n k) IN mills n
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  Induct_on `j` >-
  simp[path_head, mills_element_trivial_flip, Abbr`ls`] >>
  rpt strip_tac >>
  `EL (SUC j) ls = (zagier o flip) (EL j ls)` by simp[path_element_suc, Abbr`ls`] >>
  simp[zagier_closure, flip_closure]
QED

(* Theorem: tik n /\ j <= k /\ EL j (path n k) = (x,y,z) ==> n = windmill (x,y,z) *)
(* Proof:
   Let ls = path n k.
   Note EL j ls = (x,y,z) IN (mills n)         by path_element_in_mills, j <= k
     so n = windmill (x,y,z)                   by mills_element_alt
*)
Theorem path_element_windmill:
  !n k j x y z. tik n /\ j <= k /\ EL j (path n k) = (x,y,z) ==> n = windmill (x,y,z)
Proof
  metis_tac[path_element_in_mills, mills_element_alt]
QED

(* Theorem: tik n /\ j <= k ==> n = windmill (EL j (path n k)) *)
(* Proof: by path_element_windmill, triple_parts. *)
Theorem path_element_windmill_alt:
  !n k j. tik n /\ j <= k ==> n = windmill (EL j (path n k))
Proof
  metis_tac[path_element_windmill, triple_parts]
QED

(* Theorem: is_pong (EL 0 (path n k)) *)
(* Proof:
       is_pong (EL 0 (path n k))
   <=> is_pong (HD (path n k))     by EL
   <=> is_pong (1,n DIV 4,1)       by path_head
   <=> T                           by is_pong_def, 1 < 2 * 1
*)
Theorem path_head_is_pong:
  !n k. is_pong (EL 0 (path n k))
Proof
  simp[path_head, is_pong_def]
QED

(* Theorem: EL 0 (path n k) = (1,n DIV 4,1) *)
(* Proof:
     EL 0 (path n k)
   = HD (path n k)             by path_head_alt
   = (1,1,n DIV 4)             by path_head
*)
Theorem path_element_0:
  !n k. EL 0 (path n k) = (1,n DIV 4,1)
Proof
  simp[GSYM path_head_alt, path_head]
QED

(* Theorem: 0 < k ==> EL 1 (path n k) = (1,1,n DIV 4) *)
(* Proof:
   Let t = EL 0 (path n k)
   Then t = (1,n DIV 4,1)      by path_element_0
   Note is_pong t              by path_head_is_pong
     EL 1 (path n k)
   = EL (SUC 0) (path n k)     by ONE
   = (zagier o flip) t         by path_element_suc, 0 < k
   = pong t                    by zagier_flip_pong
   = (1,1,n DIV 4)             by pong_def
*)
Theorem path_element_1:
  !n k. 0 < k ==> EL 1 (path n k) = (1,1,n DIV 4)
Proof
  rpt strip_tac >>
  qabbrev_tac `t = EL 0 (path n k)` >>
  `is_pong t` by simp[path_head_is_pong, Abbr`t`] >>
  `EL 1 (path n k) = EL (SUC 0) (path n k)` by simp[GSYM ONE] >>
  `_ = (zagier o flip) t` by simp[path_element_suc, Abbr`t`] >>
  `_ = pong t` by simp[zagier_flip_pong] >>
  simp[pong_def, path_element_0, Abbr`t`]
QED

(* Theorem: j < k ==> EL j (TL (path n k)) = FUNPOW (zagier o flip) j (1,1,n DIV 4) *)
(* Proof:
   Let zagier o flip = f.
   Note 0 < k /\ 1 + j <= k        by j < k
     EL j (TL (path n k))
   = EL (SUC j) (path n k)         by EL
   = EL (1 + j) (path n k)         by ADD1
   = FUNPOW f j (EL 1 (path n k))  by path_element_thm, 1 + j <= k
   = FUNPOW f j (1,1,n DIV 4)      by path_element_1, 0 < k
*)
Theorem path_tail_element:
  !n k j. j < k ==> EL j (TL (path n k)) = FUNPOW (zagier o flip) j (1,1,n DIV 4)
Proof
  rpt strip_tac >>
  qabbrev_tac `f = zagier o flip` >>
  `EL j (TL (path n k)) = EL (SUC j) (path n k)` by simp[EL] >>
  `_ = EL (1 + j) (path n k)` by simp[ADD1] >>
  `_ = FUNPOW f j (EL 1 (path n k))` by fs[path_element_thm, Abbr`f`] >>
  simp[path_element_1]
QED

(* Theorem: 0 < k ==> let t = EL 1 (path n k) in
            if n < 4 then is_pung t else if n < 12 then is_pong t else is_ping t *)
(* Proof:
   Let t = EL 1 (path n k).
   Then t = (1,1,n DIV 4)          by path_element_1
   Note 3 <= n DIV 4 <=> 3 * 4 <= n   by X_LE_DIV
      or 2 < n DIV 4 <=> 12 <= n
   Also 1 <= n DIV 4 <=> 1 * 4 <= n   by X_LE_DIV
      or 0 < n DIV 4 <=> 4 <= n

   Thus, if n < 4,
       Then n DIV 4 = 0,
       ==> is_pung t               by is_pung_def
   If 4 <= n but n < 8,
       Then n DIV 4 = 1,
       ==> is_pong t               by is_pong_def
   Otherwise, 8 <= n,
       Then 2 < n DIV 4,
       ==> is_ping t               by is_ping_def
*)
Theorem path_head_next_property:
  !n k. 0 < k ==> let t = EL 1 (path n k) in
        if n < 4 then is_pung t else if n < 12 then is_pong t else is_ping t
Proof
  rw_tac std_ss[] >>
  `t = (1,1,n DIV 4)` by simp[path_element_1, Abbr`t`] >>
  `3 <= n DIV 4 <=> 12 <= n` by simp[X_LE_DIV] >>
  `1 <= n DIV 4 <=> 4 <= n` by simp[X_LE_DIV] >>
  (Cases_on `n < 4` >> simp[]) >-
  simp[is_pung_def] >>
  (Cases_on `n < 12` >> simp[]) >-
  simp[is_pong_def] >>
  fs[is_ping_def]
QED

(* Theorem: let ls = path n k in flip (LAST ls) = LAST ls ==> ~is_ping (LAST ls) *)
(* Proof:
   Let t = LAST ls.
   Then t = (x,y,z)                by triple_parts
    and flip t = t ==> y = z       by flip_fix
     so ~is_ping t                 by not_ping_x_y_y
*)
Theorem path_last_not_ping:
  !n k. let ls = path n k in flip (LAST ls) = LAST ls ==> ~is_ping (LAST ls)
Proof
  rw_tac std_ss[] >>
  metis_tac[triple_parts, flip_fix, not_ping_x_y_y]
QED

(* This is the short version. See the long version below. *)

(*
involute_involute_fix_orbit_fix_odd_inv |> ISPEC ``zagier`` |> ISPEC ``flip`` |> ISPEC ``mills n`` |> SPEC ``p:num`` |> ISPEC ``(1,1,n DIV 4)``;
val it = |- FINITE (mills n) /\ zagier involute mills n /\ flip involute mills n /\
      (1,1,n DIV 4) IN fixes zagier (mills n) /\
      p = iterate_period (zagier o flip) (1,1,n DIV 4) /\ ODD p ==>
      FUNPOW (zagier o flip) (1 + HALF p) (1,1,n DIV 4) IN
      fixes flip (mills n): thm

path_last  |- !n k. LAST (path n k) = FUNPOW (zagier o flip) k (1,1,n DIV 4)
path_last |> SPEC ``n:num`` |> SPEC ``1 + HALF p``;
val it = |- LAST (path n (1 + HALF p)) = FUNPOW (zagier o flip) (1 + HALF p) (1,1,n DIV 4): thm
*)

(* Idea: the last element of path is not a ping. *)

(* Theorem: prime n /\ tik n /\ p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
            ~is_ping (LAST (path n (1 + HALF p))) *)
(* Proof:
   Let u = (1,1,n DIV 4),
       k = 1 + HALF p,
       v = FUNPOW (zagier o flip) k u.
   Note prime n ==> ~square n                  by prime_non_square
     so FINITE (mills n)                       by mills_finite_non_square
    and zagier involute (mills n)              by zagier_involute_mills_prime, prime n
    and flip involute (mills n)                by flip_involute_mills
    and fixes zagier (mills n) = {u}           by zagier_fixes_prime
     so u IN fixes zagier (mills n)            by IN_SING

    Now u IN s                                 by mills_element_trivial, tik n
     so ODD p                                  by involute_involute_fix_sing_period_odd
    ==> v IN fixes flip (mills n)              by involute_involute_fix_orbit_fix_odd

    Claim: v = LAST (path n (1 + k))
    Proof: Let ls = path n (1 + k).
           Then ls <> []                       by path_not_nil
            and LENGTH ls = k + 2              by path_length
                LAST ls
              = EL (PRE (LENGTH ls)) ls        by LAST_EL
              = EL (1 + k) ls                  by above
              = FUNPOW (f o g) k (EL 1 ls)     by path_element_thm
              = FUNPOW (f o g) k u             by path_element_1
              = v

    Thus v = LAST (path n k)                   by path_last
     and ~is_ping v                            by flip_fixes_element, not_ping_x_y_y
*)
Theorem path_last_not_ping_thm:
  !n p. prime n /\ tik n /\ p = iterate_period (zagier o flip) (1,1,n DIV 4) ==>
        ~is_ping (LAST (path n (1 + HALF p)))
Proof
  rpt strip_tac >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  qabbrev_tac `k = HALF p` >>
  qabbrev_tac `v = FUNPOW (zagier o flip) k u` >>
  qabbrev_tac `s = mills n` >>
  `~square n` by simp[prime_non_square] >>
  `FINITE s` by fs[mills_finite_non_square, Abbr`s`] >>
  `zagier involute s` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `fixes zagier s = {u}` by fs[zagier_fixes_prime, Abbr`s`, Abbr`u`] >>
  `u IN fixes zagier s` by simp[] >>
  `u IN s` by metis_tac[mills_element_trivial] >>
  qabbrev_tac `f = zagier` >>
  qabbrev_tac `g = flip` >>
  drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
  first_x_assum (qspecl_then [`f`, `g`, `p`, `u`] strip_assume_tac) >>
  `ODD p` by metis_tac[] >>
  drule_then strip_assume_tac involute_involute_fix_orbit_fix_odd >>
  first_x_assum (qspecl_then [`f`, `g`, `p`, `u`] strip_assume_tac) >>
  `v IN fixes g s` by metis_tac[] >>
  `v = LAST (path n (1 + k))` by
  (qabbrev_tac `ls = path n (1 + k)` >>
  `ls <> [] /\ LENGTH ls = k + 2` by simp[path_not_nil, path_length, Abbr`ls`] >>
  `PRE (k + 2) = k + 1` by decide_tac >>
  `LAST ls = EL (1 + k) ls` by simp[LAST_EL] >>
  `_ = FUNPOW (f o g) k (EL 1 ls)` by fs[path_element_thm, Abbr`ls`] >>
  simp[path_element_1, Abbr`ls`, Abbr`u`, Abbr`v`]) >>
  metis_tac[triple_parts, flip_fixes_element, not_ping_x_y_y]
QED

(* Theorem: let ls = path n k in m + u <= k /\
            (!j. j < m ==> is_ping (EL (u + j) ls)) ==>
             !j. j <= m ==> EL (u + j) ls = FUNPOW ping j (EL u ls) *)
(* Proof:
   By induction on j.
   Base: 0 <= m ==> EL (u + 0) ls = FUNPOW ping 0 (EL u ls)
          EL u ls
        = FUNPOW ping 0 (EL u ls)              by FUNPOW_0
   Step: j <= m ==> EL (u + j) ls = FUNPOW ping j (EL u ls) ==>
         SUC j <= m ==> EL (u + SUC j) ls = FUNPOW ping (SUC j) (EL u ls)
      Note SUC j <= m ==> j <= m               by inequality
      Thus is_ping (EL (u + j) ls)             by j < m
         EL (u + SUC j) ls
       = EL (SUC (u + j)) ls                   by ADD_CLAUSES
       = (zagier o flip) (EL (u + j) ls)       by path_element_suc, SUC j <= m
       = ping (EL (u + j) ls)                  by zagier_flip_ping
       = ping (FUNPOW ping j (EL u ls))        by induction hypothesis
       = FUNPOW ping (SUC j) (EL u ls)         by FUNPOW_SUC
*)
Theorem path_element_ping_funpow:
  !n k u m. let ls = path n k in m + u <= k /\
            (!j. j < m ==> is_ping (EL (u + j) ls)) ==>
             !j. j <= m ==> EL (u + j) ls = FUNPOW ping j (EL u ls)
Proof
  rw_tac std_ss[] >>
  Induct_on `j` >-
  simp[] >>
  strip_tac >>
  `j < m` by decide_tac >>
  `is_ping (EL (u + j) ls)` by metis_tac[] >>
  `EL (u + SUC j) ls = EL (SUC (u + j)) ls` by metis_tac[ADD_CLAUSES] >>
  `_ = (zagier o flip) (EL (u + j) ls)` by fs[path_element_suc, Abbr`ls`] >>
  `_ = ping (EL (u + j) ls)` by metis_tac[zagier_flip_ping] >>
  fs[FUNPOW_SUC]
QED

(* Theorem: let ls = path n k in m + u <= k /\
            (!j. j < m ==> is_pung (EL (u + j) ls)) ==>
            !j. j <= m ==> EL (u + j) ls = FUNPOW pung j (EL u ls) *)
(* Proof:
   By induction on j.
   Base: 0 <= m ==> EL (u + 0) ls = FUNPOW pung 0 (EL u ls)
          EL u ls
        = FUNPOW pung 0 (EL u ls)              by FUNPOW_0
   Step: j <= m ==> EL (u + j) ls = FUNPOW pung j (EL u ls) ==>
         SUC j <= m ==> EL (u + SUC j) ls = FUNPOW pung (SUC j) (EL u ls)
      Note SUC j < m ==> j < m                 by inequality
      Thus is_pung (EL (u + j) ls)             by j < m
         EL (u + SUC j) ls
       = EL (SUC (u + j)) ls                   by ADD_CLAUSES
       = (zagier o flip) (EL (u + j) ls)       by path_element_suc, SUC j <= m
       = pung (EL (u + j) ls)                  by zagier_flip_pung
       = pung (FUNPOW pung j (EL u ls))        by induction hypothesis
       = FUNPOW pung (SUC j) (EL u ls)         by FUNPOW_SUC
*)
Theorem path_element_pung_funpow:
  !n k u m. let ls = path n k in m + u <= k /\
            (!j. j < m ==> is_pung (EL (u + j) ls)) ==>
             !j. j <= m ==> EL (u + j) ls = FUNPOW pung j (EL u ls)
Proof
  rw_tac std_ss[] >>
  Induct_on `j` >-
  simp[] >>
  strip_tac >>
  `j < m` by decide_tac >>
  `is_pung (EL (u + j) ls)` by metis_tac[] >>
  `EL (u + SUC j) ls = EL (SUC (u + j)) ls` by metis_tac[ADD_CLAUSES] >>
  `_ = (zagier o flip) (EL (u + j) ls)` by fs[path_element_suc, Abbr`ls`] >>
  `_ = pung (EL (u + j) ls)` by metis_tac[zagier_flip_pung] >>
  fs[FUNPOW_SUC]
QED

(* Theorem: let ls = path n k in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_ping (EL p ls)) ==>
            EL (j + h) ls = FUNPOW ping h (EL j ls) *)
(* Proof:
   Let ls = path n k.
   By induction on h.
   Base: j + 0 <= k ==> (!p. j <= p /\ p < j + 0 ==> is_ping (EL p ls)) ==>
         EL (j + 0) ls = FUNPOW ping 0 (EL j ls)
           EL (j + 0) ls
         = EL j ls                             by arithmetic
         = FUNPOW ping 0 (EL j ls)             by FUNPOW_0
   Step: j + h <= k ==> (!p. j <= p /\ p < j + h ==> is_ping (EL p ls)) ==>
         EL (j + h) ls = FUNPOW ping h (EL j ls) ==>
         j + SUC h <= k ==> (!p. j <= p /\ p < j + SUC h ==> is_ping (EL p ls)) ==>
         EL (j + SUC h) ls = FUNPOW ping (SUC h) (EL j ls)
        Note j + SUC h <= k ==> j + h < k, satisying j + h <= k.
            !p. j <= p /\ p < j + SUC h ==> is_ping (EL p ls)
        <=> !p. j <= p /\ (p < j + h \/ p = j + h) ==> is_ping (EL p ls)                 by arithmetic
        <=> is_ping (EL (j + h) ls) /\ (!p. j <= p /\ p < j + h ==> is_ping (EL p ls))   by DISJ_IMP_THM

           EL (j + SUC h) ls
         = EL SUC (j + h) ls                   by arithmetic
         = (zagier o flip) (EL (j + h) ls)     by path_element_suc
         = ping (EL (j + h) ls)                by zagier_flip_ping
         = ping (FUNPOW ping h (EL j ls))      by induction hypothesis
         = FUNPOW ping (SUC h) (EL j ls)       by FUNPOW_SUC

   Another proof:
   By path_element_ping_funpow with u = j, m = h.
*)
Theorem path_element_ping_funpow_alt:
  !n k j h. let ls = path n k in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_ping (EL p ls)) ==>
            EL (j + h) ls = FUNPOW ping h (EL j ls)
Proof
  rw_tac std_ss[] >>
  assume_tac path_element_ping_funpow >>
  last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
  rfs[]
QED

(* Theorem: let ls = path n k in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_pung (EL p ls)) ==>
            EL (j + h) ls = FUNPOW pung h (EL j ls) *)
(* Proof:
   Let ls = path n k.
   By induction on h.
   Base: j + 0 <= k ==> (!p. j <= p /\ p < j + 0 ==> is_pung (EL p ls)) ==>
         EL (j + 0) ls = FUNPOW pung 0 (EL j ls)
           EL (j + 0) ls
         = EL j ls                             by arithmetic
         = FUNPOW pung 0 (EL j ls)             by FUNPOW_0
   Step: j + h <= k ==> (!p. j <= p /\ p < j + h ==> is_pung (EL p ls)) ==>
         EL (j + h) ls = FUNPOW pung h (EL j ls) ==>
         j + SUC h <= k ==> (!p. j <= p /\ p < j + SUC h ==> is_pung (EL p ls)) ==>
         EL (j + SUC h) ls = FUNPOW pung (SUC h) (EL j ls)
        Note j + SUC h <= k ==> j + h < k, satisying j + h <= k.
            !p. j <= p /\ p < j + SUC h ==> is_pung (EL p ls)
        <=> !p. j <= p /\ (p < j + h \/ p = j + h) ==> is_pung (EL p ls)                 by arithmetic
        <=> is_pung (EL (j + h) ls) /\ (!p. j <= p /\ p < j + h ==> is_pung (EL p ls))   by DISJ_IMP_THM

           EL (j + SUC h) ls
         = EL SUC (j + h) ls                   by arithmetic
         = (zagier o flip) (EL (j + h) ls)     by path_element_suc
         = pung (EL (j + h) ls)                by zagier_flip_pung
         = pung (FUNPOW pung h (EL j ls))      by induction hypothesis
         = FUNPOW pung (SUC h) (EL j ls)       by FUNPOW_SUC

   Another proof:
   By path_element_pung_funpow with u = j, m = h.
*)
Theorem path_element_pung_funpow_alt:
  !n k j h. let ls = path n k in j + h <= k /\ (!p. j <= p /\ p < j + h ==> is_pung (EL p ls)) ==>
            EL (j + h) ls = FUNPOW pung h (EL j ls)
Proof
  rw_tac std_ss[] >>
  assume_tac path_element_pung_funpow >>
  last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
  rfs[]
QED

(* ------------------------------------------------------------------------- *)
(* Sections and blocks in a path.                                            *)
(* ------------------------------------------------------------------------- *)

(* Define the skip functions *)
Definition skip_idx_ping_def:
   skip_idx_ping ls k = WHILE (\j. is_ping (EL j ls)) SUC k
End

Definition skip_idx_pung_def:
   skip_idx_pung ls k = WHILE (\j. is_pung (EL j ls)) SUC k
End

(*
> EVAL  ``skip_idx_ping (path 61 6) 1``;  = 4
> EVAL  ``skip_idx_pung (path 61 6) 4``;  = 5
*)

(* Theorem: k <= h /\ (!j. k <= j /\ j < h ==> is_ping (EL j ls)) /\
            ~is_ping (EL h ls) ==> skip_idx_ping ls k = h *)
(* Proof: by skip_idx_while_thm, skip_idx_ping_def. *)
Theorem skip_idx_ping_thm:
  !ls k h. k <= h /\ (!j. k <= j /\ j < h ==> is_ping (EL j ls)) /\
           ~is_ping (EL h ls) ==> skip_idx_ping ls k = h
Proof
  simp[skip_idx_ping_def, skip_idx_while_thm]
QED

(* Theorem: ~is_ping (EL k ls) ==> skip_idx_ping ls k = k` *)
(* Proof: by skip_idx_ping_thm, h = k. *)
Theorem skip_idx_ping_none:
  !ls k. ~is_ping (EL k ls) ==> skip_idx_ping ls k = k
Proof
  simp[skip_idx_ping_thm]
QED

(* Theorem: k <= h /\ (!j. k <= j /\ j < h ==> is_pung (EL j ls)) /\
            ~is_pung (EL h ls) ==> skip_idx_pung ls k = h *)
(* Proof: by skip_idx_while_thm, skip_idx_pung_def. *)
Theorem skip_idx_pung_thm:
  !ls k h. k <= h /\ (!j. k <= j /\ j < h ==> is_pung (EL j ls)) /\
           ~is_pung (EL h ls) ==> skip_idx_pung ls k = h
Proof
  simp[skip_idx_pung_def, skip_idx_while_thm]
QED

(* Theorem: ~is_pung (EL k ls) ==> skip_idx_pung ls k = k` *)
(* Proof: by skip_idx_pung_thm, h = k. *)
Theorem skip_idx_pung_none:
  !ls k. ~is_pung (EL k ls) ==> skip_idx_pung ls k = k
Proof
  simp[skip_idx_pung_thm]
QED

(* Idea: between a pung and a ping, there must be a pong. *)

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pung (EL j ls) /\ is_ping (EL h ls) ==>
            ?p. j < p /\ p < h /\ is_pong (EL p ls) *)
(* Proof:
   Let ls = path n k.
   By contradiction, assume !p. j < p /\ p < h ==> ~is_pong (EL p ls).
   thus is_ping (EL p ls) \/ is_pung (EL p ls) by triple_cases
   Note j < h /\ is_pung (EL j ls)             by given
    and is_ping (EL h ls) ==>
        ~is_pung (EL h ls)                     by ping_not_pung
    ==> ?m. j <= m /\ m < h /\
           (!p. j <= p /\ p <= m ==> is_pung (EL p ls)) /\
            ~is_pung (EL (SUC m) ls)           by every_range_span_max
   Note ~is_pong (EL (SUC m) ls)               by every_range_less_ends, j < SUC m < h
    ==> is_ping (EL (SUC m) ls)                by triple_cases, [1]

    Now is_pung (EL m ls)                      by j <= m /\ m <= m
    But m < h <= k,
     so EL (SUC m) ls = pung (EL m ls)         by path_element_suc, zagier_flip_pung, m < k
    ==> ~is_ping (pung (EL m ls))              by pung_next_not_ping_alt
    This is a contradiction                    by [1]
*)
Theorem pung_to_ping_has_pong:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pung (EL j ls) /\ is_ping (EL h ls) ==>
            ?p. j < p /\ p < h /\ is_pong (EL p ls)
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  `?m. j <= m /\ m < h /\ is_pung (EL m ls) /\ ~is_pung (EL (SUC m) ls)` by
  (`~is_pung (EL h ls)` by simp[ping_not_pung] >>
  assume_tac every_range_span_max >>
  last_x_assum (qspecl_then [`\j. is_pung (EL j ls)`,`j`,`h`] strip_assume_tac) >>
  rfs[] >>
  `is_pung (EL m ls)` by fs[] >>
  metis_tac[]) >>
  `!p. j <= p ==> p <= h ==> ~is_pong (EL p ls)` by
    (`j <= h` by decide_tac >>
  assume_tac every_range_less_ends >>
  last_x_assum (qspecl_then [`\j. ~is_pong (EL j ls)`,`j`,`h`] strip_assume_tac) >>
  rfs[ping_not_pong, pung_not_pong]) >>
  `j <= SUC m /\ SUC m <= h` by decide_tac >>
  `~is_pong (EL (SUC m) ls)` by fs[] >>
  `is_ping (EL (SUC m) ls)` by metis_tac[triple_cases_alt] >>
  `EL (SUC m) ls = pung (EL m ls)` by fs[path_element_suc, zagier_flip_pung, Abbr`ls`] >>
  metis_tac[pung_next_not_ping_alt]
QED

(* Yes! This is the key result for the rest. *)

(* Idea: between two pongs, if there is a ping, then all pings after the starting pong. *)

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            !e. j < e /\ e < h /\ is_ping (EL e ls) ==> !p. j < p /\ p <= e ==> is_ping (EL p ls) *)
(* Proof:
   By contradiction, suppose there is (EL p ls) such that:
      j < p <= e /\ ~is_ping (EL p ls).
   Note !p. j < p < h ==> ~is_pong (EL p ls)   by given, [1]
     so for this p,
        j < p <= e < h ==> j < p < h ==> ~is_pong (EL p ls)
   Thus is_pung (EL p ls)                      by triple_cases_alt
    But is_ping (EL e ls)                      by given
   With p <= e, and p <> e, so p < e           by ping_not_pung
    and e < h <= k, so e <= k
    ==> ?q. p < q /\ q < e /\ is_pong (EL q ls)  by pung_to_ping_has_pong, p < e
    yet j < p < q < e < h,  ~is_pong (EL q ls)   by [1]
   This is a contradiction.
*)
Theorem pong_interval_ping_start:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            !e. j < e /\ e < h /\ is_ping (EL e ls) ==> !p. j < p /\ p <= e ==> is_ping (EL p ls)
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  `p < h` by decide_tac >>
  `~is_pong (EL p ls)` by fs[] >>
  `is_pung (EL p ls)` by metis_tac[triple_cases_alt] >>
  `p <> e` by metis_tac[ping_not_pung] >>
  `p < e /\ e <= k` by decide_tac >>
  assume_tac pung_to_ping_has_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `p`,`e`] strip_assume_tac) >>
  `?q. p < q /\ q < e /\ is_pong (EL q ls)` by metis_tac[] >>
  rfs[]
QED

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            !e. j < e /\ e < h /\ is_ping (EL e ls) ==> EVERY (\p. is_ping (EL p ls)) [j+1 .. e] *)
(* Proof:
   Note EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h]
    <=> !p. j + 1 <= p /\ p < h ==> ~is_pong (EL p ls)     by listRangeLHI_EVERY
    <=> !p. j< p /\ p < h ==> ~is_pong (EL p ls)
    and EVERY (\p. is_ping (EL p ls)) [j+1 .. e]
    <=> !p. j + 1 <= p /\ p <= e ==> is_ping (EL p ls)     by listRangeINC_EVERY
    <=> !p. j < p /\ p <= e ==> is_ping (EL p ls)
    The result follows from pong_interval_ping_start.
*)
Theorem pong_interval_ping_start_alt:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            !e. j < e /\ e < h /\ is_ping (EL e ls) ==> EVERY (\p. is_ping (EL p ls)) [j+1 .. e]
Proof
  rw_tac std_ss[] >>
  rfs[listRangeLHI_EVERY, listRangeINC_EVERY] >>
  `!p. j + 1 <= p <=> j < p` by decide_tac >>
  assume_tac pong_interval_ping_start >>
  last_x_assum (qspecl_then [`n`, `k`, `j`,`h`] strip_assume_tac) >>
  metis_tac[]
QED

(* Idea: between two pongs, if there is a pung, then all pungs to the ending pong. *)

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            !e. j < e /\ e < h /\ is_pung (EL e ls) ==> !p. e <= p /\ p < h ==> is_pung (EL p ls) *)
(* Proof:
   By contradiction, suppose there is (EL p ls) such that:
      e <= p < h /\ ~is_pung (EL p ls).
   Note !p. j < p < h ==> ~is_pong (EL p ls)   by given, [1]
     so for this p,
        j < e <= p < h ==> j < p < h ==> ~is_pong (EL p ls)
   Thus is_ping (EL p ls)                      by triple_cases_alt
    But is_pung (EL e ls)                      by given
   With e <= p, and e <> p, so e < p           by ping_not_pung
    and p < h <= k, so p <= k
    ==> ?q. e < q /\ q < p /\ is_pong (EL q ls)  by pung_to_ping_has_pong, e < p
    yet j < e <= p < q < h,  ~is_pong (EL q ls)  by [1]
   This is a contradiction.
*)
Theorem pong_interval_pung_stop:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            !e. j < e /\ e < h /\ is_pung (EL e ls) ==> !p. e <= p /\ p < h ==> is_pung (EL p ls)
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  `j < p` by decide_tac >>
  `~is_pong (EL p ls)` by fs[] >>
  `is_ping (EL p ls)` by metis_tac[triple_cases_alt] >>
  `e <> p` by metis_tac[ping_not_pung] >>
  `e < p /\ p <= k` by decide_tac >>
  assume_tac pung_to_ping_has_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `e`,`p`] strip_assume_tac) >>
  `?q. e < q /\ q < p /\ is_pong (EL q ls)` by metis_tac[] >>
  rfs[]
QED

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            !e. j < e /\ e < h /\ is_pung (EL e ls) ==> EVERY (\p. is_pung (EL p ls)) [e ..< h] *)
(* Proof:
   Note EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h]
    <=> !p. j + 1 <= p /\ p < h ==> ~is_pong (EL p ls)     by listRangeLHI_EVERY
    <=> !p. j< p /\ p < h ==> ~is_pong (EL p ls)
    and EVERY (\p. is_pung (EL p ls)) [e ..< h]
    <=> !p. e <= p /\ p < h ==> is_pung (EL p ls)          by listRangeINC_EVERY
    The result follows from pong_interval_pung_stop.
*)
Theorem pong_interval_pung_stop_alt:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            !e. j < e /\ e < h /\ is_pung (EL e ls) ==> EVERY (\p. is_pung (EL p ls)) [e ..< h]
Proof
  rw_tac std_ss[] >>
  rfs[listRangeLHI_EVERY] >>
  `!p. j + 1 <= p <=> j < p` by decide_tac >>
  assume_tac pong_interval_pung_stop >>
  last_x_assum (qspecl_then [`n`, `k`, `j`,`h`] strip_assume_tac) >>
  metis_tac[]
QED

(* Idea: the cut exists, in that between two pongs, there is a switch-over from ping to pung. *)

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            (!p. j < p /\ p < c ==> is_ping (EL p ls)) /\ (!p. c <= p /\ p < h ==> is_pung (EL p ls)) *)
(* Proof:
   Note j < h ==> SUC j <= h.
   If SUC j = h,
      pick c = h, that is c = SUC j.
      Then j < p < c = SUC j ==> no such p for is_ping (EL p ls)
       and h = c <= p < h ==> no such p for is_pung (EL p ls)
   Otherwise, SUC j < h.
      Let q = SUC j, then j < q < h.
      Note ~is_pong (EL q ls)                  by given
       so either is_ping or is_pung            by triple_cases

      If is_ping (EL q ls),
         Note ~is_ping (EL h ls)               by pong_not_ping
          ==> ?m. p <= m /\ m < h /\
                  (!j. p <= j /\ j <= m ==> is_ping (EL j ls)) /\
                  ~is_ping (EL (SUC m) ls)     by every_range_span_max
         Pick c = SUC m = m + 1.
         Note !p. SUC j <= p <=> j < p         by arithmetic
           so !p. j < p /\ p < c ==> is_ping (EL p ls)     by above

          Now ~is_ping (EL c ls)               by above
          and c <= h means c = h or c < h.
         If c = h, then trivially
            !p. c <= p /\ p < h ==> is_pung (EL p ls).
            and ~is_ping (EL c ls)             by pong_not_ping
         If c < h,
            Then ~is_pong (EL c ls)            by j < c < h
              so is_pung (EL c ls)             by triple_cases_alt
             ==> !p. c <= p /\ p < h ==> is_pung (EL p ls)
                                               by pong_interval_pung_stop
             and ~is_ping (EL c ls)            by pung_not_ping

      If is_pung (EL q ls),
         Pick c = SUC j.
         Then j < p < c = SUC j ==> no such p for is_ping (EL p ls)
          and !p. j < p <=> SUC j = c <= p     by arithmetic
           so !p. c <= p /\ p < h ==> is_pung (EL p ls)
                                               by pong_interval_pung_stop
          and if c < h, ~is_ping (EL c ls)     by pung_not_ping
           or if c = h, ~is_ping (EL c ls)     by pong_not_ping
*)
Theorem pong_interval_cut_exists:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ (!p. j < p /\ p < h ==> ~is_pong (EL p ls)) ==>
            ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            (!p. j < p /\ p < c ==> is_ping (EL p ls)) /\ (!p. c <= p /\ p < h ==> is_pung (EL p ls))
Proof
  rw_tac std_ss[] >>
  `SUC j = h \/ SUC j < h` by decide_tac >| [
    qexists_tac `h` >>
    simp[pong_not_ping],
    `j < SUC j` by decide_tac >>
    qabbrev_tac `q = SUC j` >>
    `~is_pong (EL q ls)` by fs[] >>
    `is_ping (EL q ls) \/ is_pung (EL q ls)` by metis_tac[triple_cases_alt] >| [
      `~is_ping (EL h ls)` by simp[pong_not_ping] >>
      assume_tac every_range_span_max >>
      last_x_assum (qspecl_then [`\p. is_ping (EL p ls)`, `q`, `h`] strip_assume_tac) >>
      rfs[] >>
      `q < SUC m /\ SUC m <= h` by decide_tac >>
      qabbrev_tac `c = SUC m` >>
      qexists_tac `c` >>
      `!p. q <= p /\ p <= m <=> j < p /\ p < c` by simp[Abbr`q`, Abbr`c`] >>
      rfs[] >>
      `c = h \/ c < h` by decide_tac >-
      simp[] >>
      `~is_pong (EL c ls)` by fs[] >>
      `is_pung (EL c ls)` by metis_tac[triple_cases_alt] >>
      assume_tac pong_interval_pung_stop >>
      last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
      rfs[] >>
      `j < c` by fs[Abbr`q`] >>
      metis_tac[],
      qexists_tac `q` >>
      rfs[pung_not_ping, Abbr`q`] >>
      assume_tac pong_interval_pung_stop >>
      last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
      rfs[] >>
      `j < SUC j` by decide_tac >>
      metis_tac[]
    ]
  ]
QED

(* Theorem: let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            EVERY (\p. is_ping (EL p ls)) [j+1 ..< c] /\ EVERY (\p. is_pung (EL p ls)) [c ..< h] *)
(* Proof:
       EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h]
   <=> !p. j + 1 <= p /\ p < h ==> ~is_pong (EL p ls)      by listRangeLHI_EVERY
   <=> !p. j < p /\ p < h ==> ~is_pong (EL p ls)           by arithmetic
   Thus ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            (!p. j < p /\ p < c ==> is_ping (EL p ls)) /\
            (!p. c <= p /\ p < h ==> is_pung (EL p ls))    by pong_interval_cut_exists
    ==> !p. j + 1 <= p /\ p < c ==> is_ping (EL p ls)      by arithmetic
   Thus EVERY (\p. is_ping (EL p ls)) [j+1 ..< c] /\
        EVERY (\p. is_pung (EL p ls)) [c ..< h]            by listRangeLHI_EVERY
*)
Theorem pong_interval_cut_exists_alt:
  !n k j h. let ls = path n k in j < h /\ h <= k /\
            is_pong (EL j ls) /\ is_pong (EL h ls) /\ EVERY (\p. ~is_pong (EL p ls)) [j+1 ..< h] ==>
            ?c. j < c /\ c <= h /\ ~is_ping (EL c ls) /\
            EVERY (\p. is_ping (EL p ls)) [j+1 ..< c] /\ EVERY (\p. is_pung (EL p ls)) [c ..< h]
Proof
  rw[listRangeLHI_EVERY] >>
  `!p b. j + 1 <= p /\ p < b <=> j < p /\ p < b` by decide_tac >>
  assume_tac pong_interval_cut_exists >>
  last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
  metis_tac[]
QED

(* Yes! A major achievement! *)

(* Example:
ls = path 61 6
is_pong (EL 0 ls) = is_pong (1,1,15) = T
is_ping (EL 1 ls) = is_ping (1,15,1) = T
is_ping (EL 2 ls) = is_ping (3,13,1) = T
is_ping (EL 3 ls) = is_ping (5,9,1) = T
is_pung (EL 4 ls) = is_pung (7,3,1) = T
ls_pong (EL 5 ls) = is_pong (1,3,5) = T
is_pong (EL 6 ls) = is_pong (5,3,3) = T

cut c = 4.
*)

(* Idea: let e be a pong, then there are j and h with j < e a stretch of pungs, and e < h a strech of pings. *)

(* Theorem: let ls = path n k in ~is_pung (HD ls) /\ e < k /\ is_pong (EL e ls) ==>
             ?j. j <= e /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls) *)
(* Proof:
   If e = 0,
      Pick j = 0, then PRE j = 0 = e           by arithmetic
      Then is_pong (EL e ls)
       ==> ~is_pung (EL (PRE j) ls)            by pong_not_pung
      and the range 0 <= p /\ p < e is empty.

   If 0 < e,
      Let d = e - 1, then d < e.
      Consider the three cases of (EL d ls).

      Case: is_ping (EL d ls)
         Pick j = e, then PRE j = PRE e = d.
         Then ~is_pung (EL d ls)               by ping_not_pung
          and the range e <= p /\ p < e is empty.

      Case: is_pong (EL d ls)
         Pick j = e, then PRE j = PRE e = d.
         Then ~is_pung (EL d ls)               by pong_not_pung
          and the range e <= p /\ p < e is empty.

      Case: is_pung (EL d ls)
         Note HD ls = EL 0 ls                  by EL
          and ~is_pung (EL 0 ls)               by given
          ==> ?m. 0 < m /\ m <= e /\
                  (!j. m <= j /\ j <= e ==> is_pung (EL j ls)) /\
               ~is_pung (EL (PRE m) ls)        by every_range_span_min, 0 < e
         Pick j = m, and j <= e.
*)
Theorem pong_seed_pung_before:
  !n k e. let ls = path n k in ~is_pung (HD ls) /\ e < k /\ is_pong (EL e ls) ==>
          ?j. j <= e /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls)
Proof
  rw_tac std_ss[] >>
  `e = 0 \/ 0 < e` by decide_tac >| [
    qexists_tac `0` >>
    fs[pong_not_pung],
    `e - 1 < e /\ PRE e = e - 1` by decide_tac >>
    qabbrev_tac `d = e - 1` >>
    `is_ping (EL d ls) \/ is_pong (EL d ls) \/ is_pung (EL d ls)` by metis_tac[triple_cases_alt] >| [
      qexists_tac `e` >>
      fs[ping_not_pung],
      qexists_tac `e` >>
      fs[pong_not_pung],
      `HD ls = EL 0 ls` by simp[] >>
      `d <> 0` by metis_tac[] >>
      `0 < d` by decide_tac >>
      assume_tac every_range_span_min >>
      last_x_assum (qspecl_then [`\p. is_pung (EL p ls)`, `0`, `d`] strip_assume_tac) >>
      rfs[] >>
      qexists_tac `m` >>
      simp[]
    ]
  ]
QED

(* Theorem: let ls = path n k in ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
            ?h. e <= h /\ h < k /\ (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls) *)
(* Proof:
   Note LAST ls = EL k ls                      by path_last_alt
   Let d = SUC e, then e < d
   Then e < k ==> SUC e = d <= k               by arithmetic
   Consider the three cases of (EL d ls).

   Case: is_ping (EL d ls)
      Note ~is_ping (EL k ls) ==> d <> k       by given
        so d < k
       ==> ?m. d <= m /\ m < k /\
               (!j. d <= j /\ j <= m ==> is_ping (EL j ls)) /\
               ~is_ping (EL (SUC m) ls)        by every_range_span_max
      Pick h = m, and !j. d <= j <=> e < j.
       and d <= h, or SUC e <= h, or e < h, satisfying e <= h.
       and h = m < k.

   Case: is_pong (EL d ls)
      Pick h = e
        so ~is_ping (EL d ls)                  by pong_not_ping
       and the range e < p <= e is empty.

   Case: is_pung (EL d ls)
      Pick h = e
        so ~is_ping (EL d ls)                  by pung_not_ping
       and the range e < p <= e is empty.
*)
Theorem pong_seed_ping_after:
  !n k e. let ls = path n k in ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
          ?h. e <= h /\ h < k /\ (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls)
Proof
  rw_tac std_ss[] >>
  `~is_ping (EL k ls)` by fs[path_last_alt, Abbr`ls`] >>
  `SUC e <= k` by decide_tac >>
  qabbrev_tac `d = SUC e` >>
  `is_ping (EL d ls) \/ is_pong (EL d ls) \/ is_pung (EL d ls)` by metis_tac[triple_cases_alt] >| [
    `d <> k` by metis_tac[] >>
    `d < k` by decide_tac >>
    assume_tac every_range_span_max >>
    last_x_assum (qspecl_then [`\p. is_ping (EL p ls)`, `d`, `k`] strip_assume_tac) >>
    rfs[] >>
    `!j. d <= j <=> e < j` by simp[Abbr`d`] >>
    qexists_tac `m` >>
    simp[Abbr`d`],
    qexists_tac `e` >>
    simp[pong_not_ping],
    qexists_tac `e` >>
    simp[pung_not_ping]
  ]
QED

(* Theorem: let ls = path n k in ~is_pung (HD ls) /\ ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
            ?j h. j <= e /\ e <= h /\ h < k /\
                  (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls) /\
                  (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls) *)
(* Proof: by pong_seed_pung_before, pong_seed_ping_after. *)
Theorem pong_seed_pung_ping:
  !n k e. let ls = path n k in ~is_pung (HD ls) /\ ~is_ping (LAST ls) /\ e < k /\ is_pong (EL e ls) ==>
           ?j h. j <= e /\ e <= h /\ h < k /\
                 (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ ~is_pung (EL (PRE j) ls) /\
                 (!p. e < p /\ p <= h ==> is_ping (EL p ls)) /\ ~is_ping (EL (SUC h) ls)
Proof
  metis_tac[pong_seed_pung_before, pong_seed_ping_after]
QED

(* Finally, this justifies the definition of blocks. *)

(*
The path starts with ~is_pung, end at flip fix a ~is_ping.
Path starts with is_pong, so also starts with ~is_ping and ends at ~is_ping.

patterns of blocks:
pong -> ping -> (block)
pung -> pong -> ping -> (block)
pung -> pong -> (block)

where (block) is pong or pung, i.e. ~is_ping.

n = 61 has 2 blocks:
block (path 61 6) 0 = (0,0,4)
      (1,1,15) -> (1,15,1) -> (3,13,1) -> (5,9,1) -> (7,3,1)
      is_pong     is_ping     is_ping     is_ping    is_pung
block (path 61 6) 1 = (4,5,6)
      (7,3,1) -> (1,3,5) -> (5,3,3)
      is_pung    is_pong    is_pong

n = 41 has 3 blocks:
block (path 41 6) 0 = (0,0,3)
      (1,1,10) -> (1,10,1) -> (3,8,1) -> (5,4,1)
      is_pong     is_ping     is_ping    is_pong
block (path 41 6) 1 = (3,3,4)
      (5,4,1) -> (3,2,4)
      is_pong    is_pong
block (path 41 6) 2 = (4,4,6)
      (3,2,4) -> (1,5,2) -> (5,2,2)
      is_pong     is_ping   is_pung

Thus:
* every block starts with pong or pung, a ~is_ping (EL j ls)
* every block ends with pong or pung, again ~is_ping (EL h ls), overlapping the start of next.
* prove that: from ~is_ping (EL j ls), either it is pong or pung. Use skip_idx_pung to locate the next pong.
* prove that: from a pong, use skip_idx_ping to locate the next ~is_ping, as end of block.
*)

(* Theorem: 0 < y /\ is_pung (x,y,z) ==>
            let t = pung (x,y,z) in flip t <> t *)
(* Proof:
   By contradiction, assume flip t = t.
   Note ~(x < z - y) /\ ~(x < 2 * z)           by is_pung_def
     or z - y <= x /\ 2 * z <= x               by NOT_LESS, [1]
    and t = pung (x,y,z)
          = (x - 2 * z,x + y - z,z)            by pung_def
   Then z = x + y - z                          by flip_fix
     or 2 * z = x + y                          by arithmetic
    ==> x + y <= x                             by [1]
   which is impossible when 0 < y.
*)
Theorem pung_next_not_flip_fix:
  !x y z. 0 < y /\ is_pung (x,y,z) ==>
          let t = pung (x,y,z) in flip t <> t
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  fs[is_pung_def, pung_def, Abbr`t`] >>
  fs[flip_fix]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls ==> ~is_pung (EL (k-1) ls) *)
(* Proof:
   Let t = EL (k-1) ls.
   By contradiction, assume is_pung t.

   If k = 0,
      Then t = EL 0 ls             by integer arithmetic
       and is_pong t               by path_head_is_pong
        so ~is_pung t              by pong_not_pung
      leading to contradiction.

   If 0 < k, SUC (k-1) = k         by arithmetic
   Note LAST ls
      = EL k ls                    by path_last_alt
      = (zagier o flip) t          by path_element_suc
      = pung t                     by zagier_flip_pung, is_pung t

   Now t = (x,y,z)                 by triple_parts
   and t IN (mills n)              by path_element_in_mills, tik n
    so 0 < y                       by mills_with_arms, ~square n
   ==> flip t <> t                 by pung_next_not_flip_fix
   This contradicts flip t = t.
*)
Theorem path_last_flip_fix_not_by_pung:
  !n k e. let ls = path n k in tik n /\ ~square n /\
          flip (LAST ls) = LAST ls ==> ~is_pung (EL (k-1) ls)
Proof
  rw_tac std_ss[] >>
  spose_not_then strip_assume_tac >>
  qabbrev_tac `t = EL (k - 1) ls` >>
  `k = 0 \/ 0 < k` by decide_tac >| [
    `k - 1 = 0` by decide_tac >>
    metis_tac[path_head_is_pong, pong_not_pung],
    `SUC (k - 1) = k /\ k - 1 < k` by decide_tac >>
    `LAST ls = pung t` by metis_tac[path_last_alt, path_element_suc, zagier_flip_pung] >>
    `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
    `t IN mills n` by simp[path_element_in_mills, Abbr`ls`,Abbr`t`] >>
    `0 < y` by metis_tac[mills_with_arms] >>
    metis_tac[pung_next_not_flip_fix]
  ]
QED

(* Idea: a block starts with non-ping, use skip_idx_pung to locate next pong. *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ j < k /\ ~is_ping (EL j ls) /\
            flip (LAST ls) = LAST ls ==>
            let e = skip_idx_pung ls j
             in j <= e /\ e < k /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ is_pong (EL e ls) *)
(* Proof:
   Let ls = path n k.
   If is_pong (EL j ls),
      Then ~is_pung (EL j ls),
      Thus skip_idx_pung ls j = j = e < k      by skip_idx_pung_thm
       and is_pong (EL j ls) is true           by assumption
       and !p. j <= p /\ p < j ==> is_pung (EL p ls)  is empty range

   If is_pung (EL j ls),
      Note 0 < k                               by j < k
       and LAST ls = EL k ls                   by path_last_alt
      Let d = k - 1, then SUC d = k            by 0 < k
      Note ~is_pung (EL d ls)                  by path_last_flip_fix_not_by_pung
       and j < k ==> j <= k - 1 = d            by arithmetic
       but j <> d, so j < d                    by ~is_pung (EL d ls)
      Thus ?m. j <= m /\ m < d /\
               (!p. j <= p /\ p <= m ==> is_pung (EL p ls)) /\
               ~is_pung (EL (SUC m) ls)        by every_range_span_max
        or SUC m = skip_idx_pung ls j = e      by skip_idx_pung_thm
       and SUC m <= d < k                      by d = k - 1
       and j < SUC m, satisfy j <= SUC m       by j <= m
       and !p. j <= p /\ p < SUC m ==> is_pung (EL p ls)

      Note is_pung (EL m ls)                   by above, j <= m /\ m <= m
       and   EL (SUC m) ls
           = (zagier o flip) (EL m ls)         by path_element_suc, m < d < k
           = pung (EL m ls)                    by zagier_flip_pung
        so ~is_ping (EL (SUC m) ls)            by pung_next_not_ping_alt
       ==> is_pong (EL (SUC m) ls)             by triple_cases_alt
*)
Theorem path_skip_idx_pung_to_pong:
  !n k j. let ls = path n k in tik n /\ ~square n /\ j < k /\ ~is_ping (EL j ls) /\
          flip (LAST ls) = LAST ls ==>
          let e = skip_idx_pung ls j
           in j <= e /\ e < k /\ (!p. j <= p /\ p < e ==> is_pung (EL p ls)) /\ is_pong (EL e ls)
Proof
  simp[] >>
  ntac 4 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `e = skip_idx_pung ls j` >>
  `is_pong (EL j ls) \/ is_pung (EL j ls)` by metis_tac[triple_cases_alt] >| [
    `~is_pung (EL j ls)` by simp[pong_not_pung] >>
    `e = j` by simp[skip_idx_pung_thm, Abbr`e`] >>
    simp[],
    `SUC (k - 1) = k /\ j <= k - 1 /\ k - 1 < k` by decide_tac >>
    qabbrev_tac `d = k - 1` >>
    `~is_pung (EL d ls)` by metis_tac[path_last_flip_fix_not_by_pung] >>
    `j <> d` by metis_tac[] >>
    `j < d` by decide_tac >>
    assume_tac every_range_span_max >>
    last_x_assum (qspecl_then [`\p. is_pung (EL p ls)`,`j`,`d`] strip_assume_tac) >>
    rfs[] >>
    `SUC m = e` by fs[skip_idx_pung_thm, Abbr`e`] >>
    `is_pung (EL m ls)` by fs[] >>
    `EL (SUC m) ls = pung (EL m ls)` by fs[path_element_suc, zagier_flip_pung, Abbr`ls`] >>
    `~is_ping (EL (SUC m) ls)` by fs[pung_next_not_ping_alt] >>
    `j <= e /\ e < k /\ !p. p <= m <=> p < SUC m` by decide_tac >>
    metis_tac[triple_cases_alt]
  ]
QED

(* Idea: a block ends with non-ping, from pong use skip_idx_pung to locate end, the next start. *)

(* Theorem: let ls = path n k in j < k /\ is_pong (EL j ls) /\ flip (LAST ls) = LAST ls ==>
            let e = skip_idx_ping ls (j + 1) in j < e /\ e <= k /\ (!p. j < p /\ p < e ==> is_ping (EL p ls)) /\ ~is_ping (EL e ls) *)
(* Proof:
   Let ls = path n k.
   Let d = j + 1, so d <= k                   by j < k

   If is_ping (EL d ls)
      Note LAST ls = EL k ls                   by path_last_alt
       and ~is_ping (EL k ls)                  by path_last_not_ping
       and d <> k, so d < k                    by ~is_ping (EL k ls)
       ==> ?m. d <= m /\ m < k /\
               (!p. d <= p /\ p <= m ==> is_ping (EL p ls)) /\
               ~is_ping (EL (SUC m) ls)        by every_range_span_max
        or SUC m = skip_idx_ping ls d = e      by skip_idx_ping_thm
      with ~is_ping (EL (SUC m) ls)            by above
       and SUC m <= k                          by m < k
       and d < SUC m or j + 1 < SUC m, satisfy j < SUC m
       and !p. j+1 <= p /\ p <= m ==> is_ping (EL p ls)
        or !p. j < p /\ p < SUC m ==> is_ping (EL p ls)

   Otherwise, ~is_ping (EL d ls)
      Thus skip_idx_ping ls d = d              by skip_idx_ping_thm
       and ~is_ping (EL d ls) is true          by assumption
       and d <= k, j + 1 = d, or j < d
       and !p. j < p /\ p < j+1 ==> is_ping (EL p ls) is empty range
*)
Theorem path_skip_idx_ping_after_pong:
  !n k j. let ls = path n k in j < k /\ is_pong (EL j ls) /\
          flip (LAST ls) = LAST ls ==>
          let e = skip_idx_ping ls (j + 1)
           in j < e /\ e <= k /\ (!p. j < p /\ p < e ==> is_ping (EL p ls)) /\ ~is_ping (EL e ls)
Proof
  simp[] >>
  ntac 4 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `j + 1 <= k` by decide_tac >>
  qabbrev_tac `d = j + 1` >>
  qabbrev_tac `e = skip_idx_ping ls d` >>
  `is_ping (EL d ls) \/ ~is_ping (EL d ls)` by decide_tac >| [
    `~is_ping (EL k ls) /\ d <> k` by metis_tac[path_last_alt, path_last_not_ping] >>
    `d < k` by decide_tac >>
    assume_tac every_range_span_max >>
    last_x_assum (qspecl_then [`\p. is_ping (EL p ls)`,`d`,`k`] strip_assume_tac) >>
    rfs[] >>
    `SUC m = e` by fs[skip_idx_ping_thm, Abbr`e`] >>
    `j < SUC m` by fs[Abbr`d`] >>
    `SUC m <= k /\ (!p. j < p <=> j + 1 <= p) /\ (!p. p <= m <=> p < SUC m)` by decide_tac >>
    metis_tac[],
    `e = d` by simp[skip_idx_ping_thm, Abbr`e`] >>
    fs[Abbr`d`]
  ]
QED

(* Theorem: let ls = path n k in
            tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            u < k /\ ~is_ping (EL u ls) /\ v = skip_idx_pung ls u /\ w = skip_idx_ping ls (v + 1) ==>
            u <= v /\ v < k /\ is_pong (EL v ls) /\ (!p. u <= p /\ p < v ==> is_pung (EL p ls)) /\
            v < w /\ w <= k /\ ~is_ping (EL w ls) /\ (!p. v < p /\ p < w ==> is_ping (EL p ls)) *)
(* Proof:
   Let ls = path n k,
        v = skip_idx_pung ls u,
        w = skip_idx_ping ls (v + 1).
    Now ~is_ping (EL u ls)                         by given
    ==> is_pong (EL v ls) /\ u <= v /\ v < k /\
        !p. u <= p /\ p < v ==> is_pung (EL p ls)  by path_skip_idx_pung_to_pong, u < k
    and is_pong (EL v ls)
    ==> ~is_ping (EL w ls) /\ v < w /\ w <= k /\
        !p. v < p /\ p < w ==> is_ping (EL p ls)   by path_skip_idx_ping_after_pong, v < k
*)
Theorem path_skip_idx_from_pung_to_ping:
  !n k u v w. let ls = path n k in
              tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              u < k /\ ~is_ping (EL u ls) /\ v = skip_idx_pung ls u /\ w = skip_idx_ping ls (v + 1) ==>
              u <= v /\ v < k /\ is_pong (EL v ls) /\ (!p. u <= p /\ p < v ==> is_pung (EL p ls)) /\
              v < w /\ w <= k /\ ~is_ping (EL w ls) /\ (!p. v < p /\ p < w ==> is_ping (EL p ls))
Proof
  simp[] >>
  ntac 4 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `v = skip_idx_pung ls u` >>
  qabbrev_tac `w = skip_idx_ping ls (v + 1)` >>
  assume_tac path_skip_idx_pung_to_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `u`] strip_assume_tac) >>
  rfs[] >>
  assume_tac path_skip_idx_ping_after_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `v`] strip_assume_tac) >>
  rfs[]
QED

(* ------------------------------------------------------------------------- *)
(* Hopping                                                                   *)
(* ------------------------------------------------------------------------- *)

(* Define the hop map *)
Definition hop_def:
   hop m (x,y,z) = if x < 2 * m * z
                   then (2 * m * z - x, z, y + m * x - m * m * z)
                   else (x - 2 * m * z, y + m * x - m * m * z, z)
End
(* Note: x <> 2 * m * z for a windmill, see windmill_mind_odd. *)
(* The condition x < 2 * m * z matches is_pong when m = 1. *)
(* first Hop Matrix: H = [[-1,0,2*m],[0,0,1],[m,1,-m*m]]
This has a diagonalisation: H = P D P^(-1), where D is diagonal. *)
(* second Hop Matrix: H = [[1,0,-2*m],[m,1,-m*m],[0,0,1]]
This has a Jordan form: H = S J S^(-1), where J is not diagonal. *)

(* Example for n = 61:
> EVAL ``hop 0 (1,15,1)``; = (1,15,1)
> EVAL ``hop 1 (1,15,1)``; = (1,1,15)
> EVAL ``hop 2 (1,15,1)``; = (3,1,13)
> EVAL ``hop 3 (1,15,1)``; = (5,1,9)
> EVAL ``hop 4 (1,15,1)``; = (7,1,3)
> EVAL ``hop 5 (1,15,1)``; = (9,1,0) <-- hop too much!
> EVAL ``hop 0 (7,1,3)``; = (7,1,3)
> EVAL ``hop 1 (7,1,3)``; = (1,5,3)
> EVAL ``hop 2 (7,1,3)``; = (5,3,3)
*)

(*
DIV_LE_X  |- !x y z. 0 < z ==> (y DIV z <= x <=> y < (x + 1) * z)
DIV_LT_X  |- !x y z. 0 < z ==> (y DIV z < x <=> y < x * z)
X_LE_DIV  |- !x y z. 0 < z ==> (x <= y DIV z <=> x * z <= y)
X_LT_DIV  |- !x y z. 0 < z ==> (x < y DIV z <=> (x + 1) * z <= y)
*)

(* Theorem: 0 < z ==>
            hop m (x,y,z) = if x DIV (2 * z) < m
                            then (2 * m * z - x, z, y + m * x - m * m * z)
                            else (x - 2 * m * z, y + m * x - m * m * z, z) *)
(* Proof:
   Note x DIV (2 * z) < m
    <=> x < 2 * m * z              by DIV_LT_X, 0 < z
   The result follows              by hop_def
*)
Theorem hop_alt:
  !m x y z. 0 < z ==>
            hop m (x,y,z) = if x DIV (2 * z) < m
                            then (2 * m * z - x, z, y + m * x - m * m * z)
                            else (x - 2 * m * z, y + m * x - m * m * z, z)
Proof
  rpt strip_tac >>
  `x DIV (2 * z) < m <=> x < 2 * m * z` by simp[DIV_LT_X] >>
  simp[hop_def]
QED

(* Theorem: hop 0 (x,y,z) = (x,y,z) *)
(* Proof: by hop_def, as x < 0 is false. *)
Theorem hop_0:
  !x y z. hop 0 (x,y,z) = (x,y,z)
Proof
  simp[hop_def]
QED

(* Theorem: hop 0 t = t *)
(* Proof: by hop_0, triple_parts. *)
Theorem hop_0_alt:
  !t. hop 0 t = t
Proof
  metis_tac[hop_0, triple_parts]
QED

(* Theorem: ~is_ping (x,y,z) ==> hop 1 (x,y,z) = (zagier o flip) (x,y,z) *)
(* Proof:
   Let f = zagier o flip.
   Note ~is_ping (x,y,z) means ~(x < z - y)    by is_ping_def
    and hop 1 (x,y,z)
      = if x < 2 * z
        then (2 * z - x, z, y + x - z)
        else (x - 2 * z, y + x - z, z)         by hop_def
      = if x < 2 * z
        then pong (x,y,z)                      by pong_def
        else pung (x,y,z)                      by pung_def
   If x < 2 * z, then is_pong (x,y,z)          by is_pong_def, ~(x < z - y)
      so hop 1 (x,y,z) = f (x,y,z)             by zagier_flip_pong
   Otherwise, is_pung (x,y,z)                  by is_pung_def, ~(x < z - y)
      so hop 1 (x,y,z) = f (x,y,z)             by zagier_flip_pung
*)
Theorem hop_1:
  !x y z. ~is_ping (x,y,z) ==> hop 1 (x,y,z) = (zagier o flip) (x,y,z)
Proof
  simp[is_ping_def, hop_def, zagier_flip_eqn]
QED

(* Theorem: ~is_ping t ==> hop 1 t = (zagier o flip) t *)
(* Proof: by hop_1, triple_parts. *)
Theorem hop_1_alt:
  !t. ~is_ping t ==> hop 1 t = (zagier o flip) t
Proof
  metis_tac[hop_1, triple_parts]
QED

(*
max hopping from (x,y,z) = (x + √n)/(2 * z)

For n = 89, start with (1,22,1), SQRT n = 9.
> EVAL ``let (x,y,z) = (1,22,1) in hop ((x + 9) DIV (2 * z)) (x,y,z)``; = (9,1,2)
> EVAL ``let (x,y,z) = (9,1,2) in hop ((x + 9) DIV (2 * z)) (x,y,z)``; = (7,2,5)
> EVAL ``let (x,y,z) = (7,2,5) in hop ((x + 9) DIV (2 * z)) (x,y,z)``; = (3,5,4)
> EVAL ``let (x,y,z) = (3,5,4) in hop ((x + 9) DIV (2 * z)) (x,y,z)``; = (5,4,4)

For n = 137, start with (1,34,1), SQRT n = 11.
> EVAL ``let (x,y,z) = (1,34,1) in hop ((x + 11) DIV (2 * z)) (x,y,z)``; = (11,1,4)
> EVAL ``let (x,y,z) = (11,1,4) in hop ((x + 11) DIV (2 * z)) (x,y,z)``; = (5,4,7)
> EVAL ``let (x,y,z) = (5,4,7) in hop ((x + 11) DIV (2 * z)) (x,y,z)``; = (9,7,2)
> EVAL ``let (x,y,z) = (9,7,2) in hop ((x + 11) DIV (2 * z)) (x,y,z)``; = (11,2,2)
*)


(* Idea: condition for hop m (x,y,z) to have a mind. *)

(* Theorem: 0 < z ==> (x DIV (2 * z) < m <=> 0 < 2 * m * z - x) *)
(* Proof:
       x DIV (2 * z) < m
   <=> x < m * (2 * z)             by DIV_LT_X, 0 < z
   <=> 0 < 2 * m * z - x           by inequality
*)
Theorem hop_mind:
  !m x z. 0 < z ==> (x DIV (2 * z) < m <=> 0 < 2 * m * z - x)
Proof
  rpt strip_tac >>
  `0 < 2 * z` by decide_tac >>
  fs[DIV_LT_X]
QED

(* Idea: condition for hop m (x,y,z) to have arms. *)

(* Theorem: ~square n /\ n = windmill (x,y,z) ==>
            (m <= (x + SQRT n) DIV (2 * z) <=> 0 < y + m * x - m * m * z) *)
(* Proof:
   Note 0 < y /\ 0 < z                         by windmill_with_arms, ~square n
   For the expression (y + m * x - m * m * z),
   with a quadratic dependence on m, surely m cannot be too large.

   If part: m <= (x + SQRT n) DIV (2 * z) ==> 0 < y + m * x - m ** 2 * z
      If x < 2 * m * z,
      Note m <= (x + SQRT n) DIV (2 * z)
       <=> 2 * m * z <= x + SQRT n                 by X_LE_DIV, 0 < z
       <=> 2 * m * z - x <= SQRT n                 by inequality
       ==> (2 * m * z - x) ** 2 <= (SQRT n) ** 2   by EXP_EXP_LE_MONO_IMP
       ==> (2 * m * z - x) ** 2 < n                by SQ_SQRT_LT_alt, ~square n
       ==> (2 * m * z - x) ** 2 < x ** 2 + 4 * y * z                               by windmill_def
       ==> (2 * m * z - x) ** 2 + 4 * m * x * z < x ** 2 + 4 * y * z + 4 * m * x * z
       ==> (2 * m * z) ** 2 + x ** 2 < x ** 2 + 4 * z * y + 4 * m * x * z          by binomial_sub_sum, x < 2 * m * z
       <=>          (2 * m * z) ** 2 < 4 * z * y + 4 * m * x * z                   by inequality
       <=>             m * m * z * z < y * z + m * x * z                           by EXP_2
       <=>           (m * m * z) * z < (y + m * x) * z                             by LEFT_ADD_DISTRIB
       <=>                 m * m * z < y + m * x                                   by LT_MULT_LCANCEL, 0 < z
       <=>                         0 < y + m * x - m * m * z                       by inequality

   Otherwise, 2 * m * z <= x.
       Thus   m * (2 * m * z) <= m * x             by LE_MULT_LCANCEL
       ==>     2 * m ** 2 * z <= m * x             by EXP_2
      2 * m ** 2 * z + y - m ** 2 * z <= m * x + y - m ** 2 * z
      y + m ** 2 * z <= y + m * x - m ** 2 * z
      LHS is > 0.

   Only-if part: 0 < y + m * x - m ** 2 * z ==> m <= (x + SQRT n) DIV (2 * z)
      If x < 2 * m * z
               0 < y + m * x - m ** 2 * z
         <=>   m ** 2 * z < y + m * x                                        by inequality
         <=> 4 * z * (m ** 2 * z) < 4 * z * (y + m * x)                      by LT_MULT_LCANCEL, 0 < z
         <=> 4 * z * m ** 2 + x ** 2 < x ** 2 + 4 * y * z + 4 * m * z * x    by LEFT_ADD_DISTRIB
         <=> 4 * z * m ** 2 + x ** 2 < n + 4 * m * z * x                     by windmill_def
         <=>                    (2 * z * m - x) ** 2 < n                     by binomial_sub_add, x < 2 * m * z
         ==>                     2 * z * m - x <= SQRT n                     by SQRT_LT, SQRT_OF_SQ
         ==>                         2 * z * m <= x + SQRT n                 by inequality
         ==>                                 m <= (x + SQRT n) DIV (2 * z)   by X_LE_DIV, 0 < z

      Otherwise, 2 * m * z <= x
              m <= x DIV (2 * z)               by X_LE_DIV, 0 < z
         ==>  m <= (x + SQRT n) DIV (2 * z)    by DIV_LE_MONOTONE, 0 < z
*)
Theorem hop_arm:
  !m n x y z. ~square n /\ n = windmill (x,y,z) ==>
              (m <= (x + SQRT n) DIV (2 * z) <=> 0 < y + m * x - m * m * z)
Proof
  rpt strip_tac >>
  `0 < y /\ 0 < z` by metis_tac[windmill_with_arms] >>
  qabbrev_tac `X = (n = windmill (x,y,z))` >>
  rw[EQ_IMP_THM] >| [
    `x < 2 * m * z \/ 2 * m * z <= x` by decide_tac >| [
      qabbrev_tac `zz = 2 * m * z` >>
      `(zz - x) ** 2 < n` by
  (`zz <= x + SQRT n` by rfs[X_LE_DIV, Abbr`zz`] >>
      `(zz - x) ** 2 <= (SQRT n) ** 2` by simp[EXP_EXP_LE_MONO_IMP] >>
      `(SQRT n) ** 2 < n` by simp[SQ_SQRT_LT_alt] >>
      decide_tac) >>
      `m ** 2 * z < y + m * x` by
    (`n = x ** 2 + 4 * y * z` by fs[windmill_def] >>
      `2 * zz * x = 4 * m * x * z` by simp[Abbr`zz`] >>
      `zz ** 2 + x ** 2 < 4 * (z * y) + x ** 2 + 4 * m * x * z` by simp[GSYM binomial_sub_sum] >>
      `zz ** 2 = 4 * z * (m * m * z)` by simp[Abbr`zz`, EXP_BASE_MULT] >>
      `4 * z * (m * m * z) < 4 * z * (y + m * x)` by decide_tac >>
      fs[]) >>
      decide_tac,
      `m * (2 * m * z) <= m * x` by simp[] >>
      `m * (2 * m * z) = 2 * m ** 2 * z` by fs[] >>
      decide_tac
    ],
    `x < 2 * m * z \/ 2 * m * z <= x` by decide_tac >| [
      `4 * z * (m * m * z) < 4 * z * (y + m * x)` by fs[] >>
      `4 * z * (m * m * z) = (2 * m * z) ** 2` by simp[EXP_BASE_MULT] >>
      `4 * z * (y + m * x) = 4 * z * y + 4 * m * x * z` by simp[] >>
      `n = x ** 2 + 4 * y * z` by fs[windmill_def] >>
      `(2 * m * z) ** 2 + x ** 2 < n + 4 * m * x * z` by decide_tac >>
      `(2 * m * z) ** 2 + x ** 2 = (2 * m * z - x) ** 2 + 4 * m * x * z` by simp[GSYM binomial_sub_sum] >>
      `(2 * m * z - x) ** 2 < n` by decide_tac >>
      `(2 * m * z - x) <= SQRT n` by metis_tac[SQRT_LT, SQRT_OF_SQ] >>
      `m * (2 * z) <= x + SQRT n` by decide_tac >>
      simp[X_LE_DIV],
      `m <= x DIV (2 * z)` by simp[X_LE_DIV] >>
      `x <= x + SQRT n /\ 0 < 2 * z` by decide_tac >>
      metis_tac[DIV_LE_MONOTONE, LESS_EQ_TRANS]
    ]
  ]
QED

(* Idea: the first of a hop triple is always positive. *)

(* Theorem: tik n /\ n = windmill (x,y,z) ==> 0 < FST (hop m (x,y,z)) *)
(* Proof:
   If x < 2 * (m * z),
   Then hop m (x,y,z) = (2 * m * z - x,z,y + m * x - m * m * z)    by hop_def
     so FST (hop m (x,y,z)) = 2 * m * z - x                        by FST
    and 0 < 2 * m * z - x                                          by x < 2 * (m * z)

   If ~(x < 2 * (m * z)),
   Then hop m (x,y,z) = (x - 2 * m * z,y + m * x - m * m * z,z)    by hop_def
     so FST (hop m (x,y,z)) = x - 2 * m * z                        by FST
    but ODD x                                                      by windmill_mind_odd, tik n
     so x <> 2 * (m * z)                                           by EVEN_DOUBLE, EVEN_ODD
    ==> 0 < x - 2 * m * z                                          by arithmetic
*)
Theorem hop_triple_first:
  !n m x y z. tik n /\ n = windmill (x,y,z) ==> 0 < FST (hop m (x,y,z))
Proof
  rw[hop_def] >>
  `2 * (m * z) <= x` by decide_tac >>
  `ODD x` by metis_tac[windmill_mind_odd] >>
  `x <> 2 * (m * z)` by metis_tac[EVEN_DOUBLE, EVEN_ODD] >>
  decide_tac
QED

(* Idea: hop goes from windmill to windmill. *)

(* Theorem: ~square n /\ n = windmill (x,y,z) /\ m <= (x + SQRT n) DIV (2 * z) ==>
            n = windmill (hop m (x,y,z)) *)
(* Proof:
   Note 0 < y /\ 0 < z                         by windmill_with_arms, ~square n
    and 0 < y + m * x - m * m * z              by hop_arm, ~square n
    ==> m * m * z < y + m * x                                      by inequality
    ==> 4 * z * (m * m * z) < 4 * z * (y + m * x)                  by 0 < z
    ==> 4 * z * (m * m * z) < 4 * z * y + 4 * z * m * x            by LEFT_ADD_DISTRIB
    ==>   4 * z * m * m * z < 4 * z * y + 4 * m * x * z            by arithmetic, [1]

   If x < 2 * (m * z),
   Then hop m (x,y,z) = (2 * m * z - x,z,y + m * x - m * m * z)    by hop_def
        windmill (hop m (x,y,z))
      = (2 * m * z - x) ** 2 + 4 * z * (y + m * x - m * m * z)     by windmill_def
      = (2 * m * z - x) ** 2 + (4 * z * (y + m * x) - 4 * z * m * m * z)        by LEFT_SUB_DISTRIB
      = (2 * m * z - x) ** 2 + (4 * z * y + 4 * m * z * x - 4 * z * m * m * z)  by LEFT_ADD_DISTRIB
      = (2 * m * z - x) ** 2 + 4 * z * y + 4 * m * z * x - 4 * z * m * m * z    by [1]
      = (2 * m * z - x) ** 2 + 2 * (2 * m * z) * x + 4 * y * z - 4 * z * m * m * z
      = (2 * m * z) ** 2 + x ** 2 + 4 * z * y - 4 * z * m * m * z  by binomial_sub_sum, x < 2 * (m * z)
      = 4 * m ** 2 * z ** 2 + x ** 2 + 4 * z * y - 4 * m ** 2 * z ** 2          by EXP_2, EXP_BASE_MULT
      = x ** 2 + 4 * z * y                                         by arithmetic
      = n                                                          by windmill_def

   If ~(x < 2 * (m * z)),
   Then hop m (x,y,z) = (x - 2 * m * z,y + m * x - m * m * z,z)    by hop_def
        windmill (hop m (x,y,z))
      = (x - 2 * m * z) ** 2 + 4 * (y + m * x - m * m * z) * z     by windmill_def
      = (x - 2 * m * z) ** 2 + (4 * z * (y + m * x) - 4 * z * m * m * z)        by LEFT_SUB_DISTRIB
      = (x - 2 * m * z) ** 2 + (4 * z * y + 4 * m * z * x - 4 * z * m * m * z)  by LEFT_ADD_DISTRIB
      = (x - 2 * m * z) ** 2 + 4 * z * y + 4 * m * z * x - 4 * z * m * m * z    by [1]
      = (x - 2 * m * z) ** 2 + 2 * (2 * m * z) * x + 4 * y * z - 4 * z * m * m * z
      = (2 * m * z) ** 2 + x ** 2 + 4 * z * y - 4 * z * m * m * z  by binomial_sub_sum, 2 * (m * z) <= x
      = 4 * m ** 2 * z ** 2 + x ** 2 + 4 * z * y - 4 * m ** 2 * z ** 2          by EXP_2, EXP_BASE_MULT
      = x ** 2 + 4 * z * y                                         by arithmetic
      = n                                                          by windmill_def
*)
Theorem hop_windmill:
  !m n x y z. ~square n /\ n = windmill (x,y,z) /\ m <= (x + SQRT n) DIV (2 * z) ==>
              n = windmill (hop m (x,y,z))
Proof
  rpt strip_tac >>
  `0 < y /\ 0 < z` by metis_tac[windmill_with_arms] >>
  `0 < y + m * x - m * m * z` by metis_tac[hop_arm] >>
  `4 * (m ** 2 * z ** 2) < 4 * (z * y) + 4 * (m * x * z)` by
  (`m * m * z < y + m * x` by decide_tac >>
  `4 * z * (m ** 2 * z) < 4 * z * (y + m * x)` by fs[] >>
  `4 * z * (m ** 2 * z) = 4 * (m ** 2 * z ** 2)` by simp[] >>
  decide_tac) >>
  rw[hop_def] >| [
    qabbrev_tac `n = windmill (x,y,z)` >>
    qabbrev_tac `zz = 2 * (m * z)` >>
    simp[windmill_def, LEFT_SUB_DISTRIB, LEFT_ADD_DISTRIB] >>
    `4 * (m * x * z) = 2 * zz * x` by simp[Abbr`zz`] >>
    `4 * (m * x * z) + (zz - x) ** 2 = zz ** 2 + x ** 2` by fs[GSYM binomial_sub_sum] >>
    `zz ** 2 = 4 * (m ** 2 * z ** 2)` by simp[EXP_BASE_MULT, Abbr`zz`] >>
    fs[windmill_def, Abbr`n`],
    qabbrev_tac `n = windmill (x,y,z)` >>
    qabbrev_tac `zz = 2 * (m * z)` >>
    simp[windmill_def, LEFT_SUB_DISTRIB, LEFT_ADD_DISTRIB] >>
    `4 * (m * x * z) = 2 * zz * x` by simp[Abbr`zz`] >>
    `4 * (m * x * z) + (x - zz) ** 2 = zz ** 2 + x ** 2` by fs[GSYM binomial_sub_sum] >>
    `zz ** 2 = 4 * (m ** 2 * z ** 2)` by simp[EXP_BASE_MULT, Abbr`zz`] >>
    fs[windmill_def, Abbr`n`]
  ]
QED

(* Idea: windmill defines the range of hopping. *)

(* Theorem: n = windmill (x,y,z) /\
            0 < 2 * m * z - x /\ 0 < y + m * x - m * m * z ==>
            x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z) *)
(* Proof:
   Note 0 < 2 * m * z - x
    ==> 0 < z                      by MULT_0, SUB_0
    and x < m * (2 * z)            by inequality
    ==> x DIV (2 * z) < m          by DIV_LT_X, 0 < z

   Also 0 < y + m * x - m * m * z  (without ~square n)
    ==> m * m * z < y + m * x                              by inequality
    ==> 4 * z * (m * m * z) < 4 * z * (y + m * x)          by LT_MULT_LCANCEL, 0 < z
    ==> 4 * z * (m * m * z) < 4 * z * y + 4 * z * m * x    by LEFT_ADD_DISTRIB
    ==>    (2 * m * z) ** 2 < 4 * z * y + 4 * z * m * x    by EXP_BASE_MULT
    ==> x ** 2 + (2 * m * z) ** 2 < x ** 2 + 4 * z * y + 4 * z * m * x   by inequality
    ==> (2 * m * z) ** 2 + x ** 2 < n + 4 * z * m * x      by windmill_def
    ==> (2 * m * z - x) ** 2 + 2 * (2 * m * z) * x < n + 4 * z * m * x   by binomial_sub_sum, x < 2 * m * y
    ==> (2 * m * z - x) ** 2 < n                           by inequality
    ==>       2 * m * z - x <= SQRT n                      by SQRT_LT, SQRT_OF_SQ
    ==>       2 * m * z <= x + SQRT n                      by inequality
    ==>               m <= (x + SQRT n) DIV (2 * z)        by X_LE_DIV, 0 < z
*)
Theorem hop_range:
  !m n x y z. n = windmill (x,y,z) /\
              0 < 2 * m * z - x /\ 0 < y + m * x - m * m * z ==>
              x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z)
Proof
  ntac 6 strip_tac >>
  `0 < z` by metis_tac[NOT_ZERO, MULT_0, SUB_0, DECIDE``~(0 < 0)``] >>
  strip_tac >-
  fs[DIV_LT_X] >>
  `m * m * z < y + m * x` by decide_tac >>
  `4 * z * (m * m * z) < 4 * z * (y + m * x)` by fs[] >>
  `4 * z * (m * m * z) = (2 * m * z) ** 2` by simp[EXP_BASE_MULT] >>
  `4 * z * (y + m * x) = 4 * z * y + 4 * m * x * z` by simp[] >>
  `n = x ** 2 + 4 * y * z` by simp[windmill_def] >>
  `(2 * m * z) ** 2 + x ** 2 < n + 4 * m * x * z` by decide_tac >>
  `(2 * m * z) ** 2 + x ** 2 = (2 * m * z - x) ** 2 + 4 * m * x * z` by simp[GSYM binomial_sub_sum] >>
  `(2 * m * z - x) ** 2 < n` by decide_tac >>
  `(2 * m * z - x) <= SQRT n` by metis_tac[SQRT_LT, SQRT_OF_SQ] >>
  `m * (2 * z) <= x + SQRT n` by decide_tac >>
  simp[X_LE_DIV]
QED

(* Idea: the range of hopping is defined by being a windmill. *)

(* Theorem: ~square n /\ n = windmill (x,y,z) ==>
            (0 < 2 * m * z - x /\ 0 < y + m * x - m * m * z <=>
             0 < z /\ x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z)) *)
(* Proof:
   If part: 0 < 2 * m * z - x /\ 0 < y + m * x - m * m * z ==>
            0 < z /\ x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z)
      Note 0 < 2 * m * z - x ==> 0 < z         by MULT_0, SUB_0
       and x DIV (2 * z) < m                   by hop_range
       and m <= (x + SQRT n) DIV (2 * z)       by hop_range

   Only-if part: 0 < z /\ x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z) ==>
                 0 < 2 * m * z - x /\ 0 < y + m * x - m * m * z
      Note x DIV (2 * z) < m
       ==> 0 < 2 * (m * z) - x                 by hop_mind
       and m <= (x + SQRT n) DIV (2 * z)
       ==> 0 < y + m * x - m * m * z           by hop_arm
*)
Theorem hop_range_iff:
  !m n x y z. ~square n /\ n = windmill (x,y,z) ==>
              (0 < 2 * m * z - x /\ 0 < y + m * x - m * m * z <=>
              0 < z /\ x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z))
Proof
  ntac 6 strip_tac >>
  rewrite_tac [EQ_IMP_THM] >>
  ntac 2 strip_tac >| [
    `0 < z` by metis_tac[NOT_ZERO, MULT_0, SUB_0, DECIDE``~(0 < 0)``] >>
    metis_tac[hop_range],
    metis_tac[hop_mind, hop_arm]
  ]
QED

(* ------------------------------------------------------------------------- *)
(* Matrices of ping, pong, pung and hop.                                     *)
(* ------------------------------------------------------------------------- *)

(*
ping_def |- !x y z. ping (x,y,z) = (x + 2 * y,y,z - y - x)
pong_def |- !x y z. pong (x,y,z) = (2 * z - x,z,x + y - z)
pung_def |- !x y z. pung (x,y,z) = (x - 2 * z,x + y - z,z)

The matrices:
ping  P = [[1,2,0],[0,1,0],[-1,-1,1]]  P^2 = [[1,4,0],[0,1,0],[-2,-4,1]]  same form P^m = [[1,2m,0],[0,1,0],[-m,-m*m,1]]
pong  Q = [[-1,0,2],[0,0,1],[1,1,-1]]  Q^2 = [[3,2,-4],[1,1,-1],[-2,-1,4]]
pung  R = [[1,0,-2],[1,1,-1],[0,0,1]]  R^2 = [[1,0,-4],[2,1,-4],[0,0,1]]  same form R^m = [[1,0,-2m],[m,1,-m*m],[0,0,1]]

Both P and R have Jordan forms (check in WolframAlpha), Q has diagonal form.
*)

(* Theorem: FUNPOW ping m (x,y,z) = (x + 2 * m * y, y, z - m * x - m * m * y) *)
(* Proof:
   By induction on m.
   Base: FUNPOW ping 0 (x,y,z) = (x + 2 * 0 * y,y,z - 0 * x - 0 * 0 * y)
        FUNPOW ping 0 (x,y,z)
      = (x,y,z)                                by FUNPOW_0
      = (x + 0, y, z - 0)                      by ADD_0, SUB_0
      = (x + 2 * 0 * y, y, z - 0 * x - 0 * 0 * y)
   Step: FUNPOW ping m (x,y,z) = (x + 2 * m * y,y,z - m * x - m * m * y) ==>
         FUNPOW ping (SUC m) (x,y,z) = (x + 2 * SUC m * y,y,z - SUC m * x - SUC m * SUC m * y)

        FUNPOW ping (SUC m) (x,y,z)
      = ping (FUNPOW ping m (x,y,z))                     by FUNPOW_SUC
      = ping (x + 2 * m * y,y,z - m * x - m * m * y)     by induction hypothesis
      = (x + 2 * m * y + 2 * y,y,z - m * x - m * m * y - y - (x + 2 * m * y))
                                                         by ping_def
      = (x + 2 * (m + 1) * y), y,                        by LEFT_ADD_DISTRIB
         z - (x * (m + 1) + y * (m + 1) ** 2))           by SUM_SQUARED
      = (x + 2 * SUC m * y, y, z - SUC m * x - SUC m * SUC m * y)
                                                         by ADD1, EXP_2
*)
Theorem ping_funpow:
  !m x y z. FUNPOW ping m (x,y,z) = (x + 2 * m * y, y, z - m * x - m * m * y)
Proof
  rpt strip_tac >>
  Induct_on `m` >-
  simp[] >>
  simp[FUNPOW_SUC, ping_def, ADD1] >>
  `(m + 1) ** 2 = m ** 2 + 2 * m + 1` by simp[SUM_SQUARED] >>
  fs[]
QED

(* Theorem: is_ping (x,y,z) ==> windmill (x,y,z) = windmill (ping (x,y,z)) *)
(* Proof:
     windmill (ping (x,y,z))
   = windmill ((zagier o flip) (x,y,z))        by zagier_flip_ping
   = windmill (x,y,z)                          by zagier_flip_windmill
*)
Theorem ping_windmill:
  !x y z. is_ping (x,y,z) ==> windmill (x,y,z) = windmill (ping (x,y,z))
Proof
  simp[zagier_flip_ping, zagier_flip_windmill]
QED

(* Theorem: is_ping t ==> windmill t = windmill (ping t) *)
(* Proof: by ping_windmill, triple_parts. *)
Theorem ping_windmill_alt:
  !t. is_ping t ==> windmill t = windmill (ping t)
Proof
  metis_tac[ping_windmill, triple_parts]
QED

(* Theorem: is_pong (x,y,z) ==> windmill (x,y,z) = windmill (pong (x,y,z)) *)
(* Proof:
     windmill (pong (x,y,z))
   = windmill ((zagier o flip) (x,y,z))        by zagier_flip_pong
   = windmill (x,y,z)                          by zagier_flip_windmill
*)
Theorem pong_windmill:
  !x y z. is_pong (x,y,z) ==> windmill (x,y,z) = windmill (pong (x,y,z))
Proof
  simp[zagier_flip_pong, zagier_flip_windmill]
QED

(* Theorem: is_pong t ==> windmill t = windmill (pong t) *)
(* Proof: by pong_windmill, triple_parts. *)
Theorem pong_windmill_alt:
  !t. is_pong t ==> windmill t = windmill (pong t)
Proof
  metis_tac[pong_windmill, triple_parts]
QED

(* Theorem: is_pung (x,y,z) ==> windmill (x,y,z) = windmill (pung (x,y,z)) *)
(* Proof:
     windmill (pung (x,y,z))
   = windmill ((zagier o flip) (x,y,z))        by zagier_flip_pung
   = windmill (x,y,z)                          by zagier_flip_windmill
*)
Theorem pung_windmill:
  !x y z. is_pung (x,y,z) ==> windmill (x,y,z) = windmill (pung (x,y,z))
Proof
  simp[zagier_flip_pung, zagier_flip_windmill]
QED

(* Theorem: is_pung t ==> windmill t = windmill (pung t) *)
(* Proof: by pung_windmill, triple_parts. *)
Theorem pung_windmill_alt:
  !t. is_pung t ==> windmill t = windmill (pung t)
Proof
  metis_tac[pung_windmill, triple_parts]
QED

(* Theorem: (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
            windmill (x,y,z) = windmill (FUNPOW ping m (x,y,z)) *)
(* Proof:
   By induction on m.
   Base: (!j. j < 0 ==> is_ping (FUNPOW ping j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW ping 0 (x,y,z))
      Note FUNPOW ping 0 (x,y,z) = (x,y,z)     by FUNPOW_0
      Thus trivially true.
   Step: (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW ping m (x,y,z)) ==>
         (!j. j < SUC m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW ping (SUC m) (x,y,z))
      Note !j. j < SUC m ==> is_ping (FUNPOW ping j (x,y,z))
       <=> !j. (j < m \/ j = m) ==> is_ping (FUNPOW ping j (x,y,z))    by arithmetic
       <=> is_ping (FUNPOW ping m (x,y,z)) /\
           !j. j < m ==> is_ping (FUNPOW ping j (x,y,z))               by DISJ_IMP_THM
        windmill (FUNPOW ping (SUC m) (x,y,z))
      = windmill (ping (FUNPOW ping m (x,y,z)))                        by FUNPOW_SUC
      = windmill (FUNPOW ping m (x,y,z))                               by ping_windmill_alt
      = windmill (x,y,z)                                               by induction hypothesis
*)
Theorem ping_funpow_windmill:
  !m x y z. (!j. j < m ==> is_ping (FUNPOW ping j (x,y,z))) ==>
             windmill (x,y,z) = windmill (FUNPOW ping m (x,y,z))
Proof
  rpt strip_tac >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_ping (FUNPOW ping m (x,y,z)) /\ !j. j < m ==> is_ping (FUNPOW ping j (x,y,z))` by metis_tac[] >>
  fs[FUNPOW_SUC, ping_windmill_alt]
QED

(* Theorem: (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> windmill t = windmill (FUNPOW ping m t) *)
(* Proof: by ping_funpow_windmill, triple_parts. *)
Theorem ping_funpow_windmill_alt:
  !m t. (!j. j < m ==> is_ping (FUNPOW ping j t)) ==> windmill t = windmill (FUNPOW ping m t)
Proof
  metis_tac[ping_funpow_windmill, triple_parts]
QED

(* Theorem: (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            windmill (x,y,z) = windmill (FUNPOW pung m (x,y,z)) *)
(* Proof:
   By induction on m.
   Base: (!j. j < 0 ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW pung 0 (x,y,z))
      Note FUNPOW pung 0 (x,y,z) = (x,y,z)     by FUNPOW_0
      Thus trivially true.
   Step: (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW pung m (x,y,z)) ==>
         (!j. j < SUC m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         windmill (x,y,z) = windmill (FUNPOW pung (SUC m) (x,y,z))
      Note !j. j < SUC m ==> is_pung (FUNPOW pung j (x,y,z))
       <=> !j. (j < m \/ j = m) ==> is_pung (FUNPOW pung j (x,y,z))    by arithmetic
       <=> is_pung (FUNPOW pung m (x,y,z)) /\
           !j. j < m ==> is_pung (FUNPOW pung j (x,y,z))               by DISJ_IMP_THM
        windmill (FUNPOW pung (SUC m) (x,y,z))
      = windmill (pung (FUNPOW pung m (x,y,z)))                        by FUNPOW_SUC
      = windmill (FUNPOW pung m (x,y,z))                               by pung_windmill_alt
      = windmill (x,y,z)                                               by induction hypothesis
*)
Theorem pung_funpow_windmill:
  !m x y z. (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
             windmill (x,y,z) = windmill (FUNPOW pung m (x,y,z))
Proof
  rpt strip_tac >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_pung (FUNPOW pung m (x,y,z)) /\ !j. j < m ==> is_pung (FUNPOW pung j (x,y,z))` by metis_tac[] >>
  fs[FUNPOW_SUC, pung_windmill_alt]
QED

(* Theorem: (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> windmill t = windmill (FUNPOW pung m t) *)
(* Proof: by pung_funpow_windmill, triple_parts. *)
Theorem pung_funpow_windmill_alt:
  !m t. (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> windmill t = windmill (FUNPOW pung m t)
Proof
  metis_tac[pung_funpow_windmill, triple_parts]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\
            (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            FUNPOW pung m (x,y,z) = (x - 2 * m * z, y + m * x - m * m * z, z) *)
(* Proof:
   By induction on m.
   Base: (!j. j < 0 ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         FUNPOW pung 0 (x,y,z) = (x - 2 * 0 * z,y + 0 * x - 0 * 0 * z,z)
        FUNPOW pung 0 (x,y,z)
      = (x,y,z)                                by FUNPOW_0
      = (x - 0, y + 0, z)                      by ADD_0, SUB_0
      = (x - 2 * 0 * z,y + 0 * x - 0 * 0 * z, z)
   Step: (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         FUNPOW pung m (x,y,z) = (x - 2 * m * z,y + m * x - m * m * z,z) ==>
         (!j. j < SUC m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
         FUNPOW pung (SUC m) (x,y,z) = (x - 2 * SUC m * z,y + SUC m * x - SUC m * SUC m * z,z)
      Note !j. j < SUC m ==> is_pung (FUNPOW pung j (x,y,z))
       <=> !j. (j < m \/ j = m) ==> is_pung (FUNPOW pung j (x,y,z))    by arithmetic
       <=> is_pung (FUNPOW pung m (x,y,z)) /\
           !j. j < m ==> is_pung (FUNPOW pung j (x,y,z))               by DISJ_IMP_THM
      Thus n = windmill (FUNPOW pung m (x,y,z))          by pung_funpow_windmill
      giving the conditions:
           0 < x - 2 * m * z /\
           0 < y + m * x - m ** 2 * z                    by windmill_mind_and_arms, EXP_2

        FUNPOW pung (SUC m) (x,y,z)
      = pung (FUNPOW pung m (x,y,z))                     by FUNPOW_SUC
      = pung (x - 2 * m * z,y + m * x - m * m * z,z)     by induction hypothesis
      = (x - 2 * m * z - 2 * z,x - 2 * m * z + (y + m * x - m * m * z) - z,z)
                                                         by pung_def
      = (x - 2 * (m + 1) * z),                           by LEFT_ADD_DISTRIB, by conditions
         y + (m + 1) * x - (m + 1) ** 2 * z, z)          by SUM_SQUARED
      = (x - 2 * SUC m * z,y + SUC m * x - SUC m * SUC m * z,z)    by ADD1, EXP_2
*)
Theorem pung_funpow:
  !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\
              (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
              FUNPOW pung m (x,y,z) = (x - 2 * m * z, y + m * x - m * m * z, z)
Proof
  rpt strip_tac >>
  Induct_on `m` >-
  simp[] >>
  strip_tac >>
  `!j. j < SUC m <=> (j < m \/ j = m)` by decide_tac >>
  `is_pung (FUNPOW pung m (x,y,z)) /\ !j. j < m ==> is_pung (FUNPOW pung j (x,y,z))` by metis_tac[] >>
  `n = windmill (FUNPOW pung m (x,y,z))` by simp[GSYM pung_funpow_windmill] >>
  `0 < x - 2 * m * z /\ 0 < y + m * x - m ** 2 * z` by metis_tac[windmill_mind_and_arms, EXP_2] >>
  simp[FUNPOW_SUC, pung_def, ADD1] >>
  `x - 2 * (m * z) + (y + m * x - m ** 2 * z) - z = (y + m * x - m ** 2 * z) + (x - 2 * (m * z)) - z` by decide_tac >>
  `_ = y + m * x + x - (m ** 2 * z + 2 * m * z + z)` by fs[] >>
  `(m + 1) ** 2 = m ** 2 + 2 * m + 1` by simp[SUM_SQUARED] >>
  rfs[]
QED

(* Idea: the pungs is equivalent to hop. *)

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\
            (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            FUNPOW pung m (x,y,z) = hop m (x,y,z) *)
(* Proof:
   Let xx = x - 2 * m * z,
       yy = y + m * x - m * m * z.
   Then FUNPOW pung m (x,y,z)
      = (xx, yy, z)                            by pung_funpow
    and n = windmill (xx,yy,z)                 by pung_funpow_windmill
   Thus 0 < xx                                 by windmill_with_mind, tik n
     or 2 * m * z < x                          by SUB_LESS_0

        hop m (x,y,z)
      = (xx, yy, z)                            by hop_def, 2 * m * z < x
      = FUNPOW pung m (x,y,z)                  by above
*)
Theorem pung_funpow_by_hop:
  !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\
              (!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))) ==>
              FUNPOW pung m (x,y,z) = hop m (x,y,z)
Proof
  rpt strip_tac >>
  `FUNPOW pung m (x,y,z) = (x - 2 * m * z,y + m * x - m * m * z,z)` by fs[pung_funpow] >>
  `n = windmill (FUNPOW pung m (x,y,z))` by metis_tac[pung_funpow_windmill_alt] >>
  `0 < x - 2 * m * z` by metis_tac[windmill_with_mind, ONE_NOT_0] >>
  simp[hop_def]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\
          (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW pung m t = hop m t *)
(* Proof: by pung_funpow_by_hop. *)
Theorem pung_funpow_by_hop_alt:
  !n m t. tik n /\ ~square n /\ n = windmill t /\
          (!j. j < m ==> is_pung (FUNPOW pung j t)) ==> FUNPOW pung m t = hop m t
Proof
  metis_tac[pung_funpow_by_hop, triple_parts]
QED

(* Idea: the pung-to-ping through pong is equivalent to hop. *)

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\ is_pong (FUNPOW pung q (x,y,z)) /\
            (!j. j < q ==> is_pung (FUNPOW pung j (x,y,z))) ==>
            (FUNPOW ping p o pong o FUNPOW pung q) (x,y,z) = hop (p + q + 1) (x,y,z) *)
(* Proof:
   Let a = FUNPOW pung q (x,y,z),
       b = pong a,
       c = FUNPOW ping p b.
   The goal is to show: c = hop (p + (q + 1)) (x,y,z)

   Note a = FUNPOW pung q (x,y,z)
          = (x - 2 * q * z,
             y + q * x - q ** 2 * z, z)        by pung_funpow, EXP_2
    and n = windmill a                         by pung_funpow_windmill
    ==> 0 < x - 2 * q * z /\
        0 < y + q * x - q ** 2 * z             by windmill_mind_and_arms
     or 2 * q * z < x /\
        q ** 2 * z < y + q * x                 by inequality, [1]

   Note b = pong a
          = (2 * z - (x - 2 * q * z), z,
             x - 2 * q * z + (y + q * x - q ** 2 * z) - z,
                                               by pong_def
          = (2 * z + 2 * q * z - x, z,
             (y + q * x - q ** 2 * z) + (x - 2 * q * z) - z)
                                               by SUB_SUB, [1]
          = (2 * (q + 1) * z - x, z,
             y + (q + 1) * x - (q ** 2 + 2 * q + 1) * z)
                                               by RIGHT_ADD_DISTRIB
          = (2 * (q + 1) * z - x, z,
             y + (q + 1) * x - (q + 1) ** 2 * z)
                                               by SUM_SQUARED
    and n = windmill b                         by pong_windmill_alt
    ==> 0 < 2 * (q + 1) * z - x /\
        0 < y + (q + 1) * x - (q + 1) ** 2 * z by windmill_mind_and_arms
     or x < 2 * (q + 1) * z /\
        (q + 1) ** 2 * z < y + (q + 1) * x     by inequality, [2]

     FUNPOW ping p o pong o FUNPOW pung q (x,y,z)
   = FUNPOW ping p (pong (FUNPOW pung q (x,y,z)))      by composition
   = FUNPOW ping p b                                   by above

   If p = 0,
     FUNPOW ping 0 b
   = b                                                 by FUNPOW_0
   = (2 * (q + 1) * z - x, z, y + (q + 1) * x - (q + 1) ** 2 * z)
   = hop (0 + q + 1) (x,y,z)                           by hop_def

   If 0 < p,
   Then p * x  < 2 * p * (q + 1) * z                   by LT_MULT_LCANCEL, [3]

     c
   = FUNPOW ping p b
   = (2 * (q + 1) * z - x + 2 * p * z, z,
      y + (q + 1) * x - (q + 1) ** 2 * z - p * (2 * (q + 1) * z - x) - p ** 2 * z)
                                                       by ping_funpow, EXP_2
   = (2 * (q + 1) * z + 2 * p * z - x, z,
      y + (q + 1) * x + p x - ((q + 1) ** 2 + 2 * p * (q + 1) + p ** 2) * z,
                                                       by arithmetic, [2][3]
   = (2 * (p + q + 1) * z - x, z,
      y + (p + q + 1) * x - ((q + 1) ** 2 + 2 * (q + 1) * p + p ** 2) * z,
                                                       by RIGHT_ADD_DISTRIB
   = (2 * (p + q + 1) * z - x, z,
      y + (p + q + 1) * x - (p + q + 1) ** 2 * z)      by SUM_SQUARED

    Now 0 < 2 * (q + 1) * z - x                by windmill_mind_and_arms
    and 2 * (p + (q + 1)) * z - x = 2 * p * z + (2 * (q + 1) * z - x)
                                               by 0 < 2 * (q + 1) * z - x
   Thus 0 < 2 * (p + (q + 1)) * z - x          by inequality
   Thus c = hop (p + q + 1) (x,y,z)            by hop_def
*)
Theorem pung_to_ping_by_hop:
  !n x y z p q. tik n /\ ~square n /\ n = windmill (x,y,z) /\ is_pong (FUNPOW pung q (x,y,z)) /\
                (!j. j < q ==> is_pung (FUNPOW pung j (x,y,z))) ==>
                (FUNPOW ping p o pong o FUNPOW pung q) (x,y,z) = hop (p + q + 1) (x,y,z)
Proof
  rw[] >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  qabbrev_tac `a = FUNPOW pung q (x,y,z)` >>
  qabbrev_tac `b = pong a` >>
  qabbrev_tac `c = FUNPOW ping p b` >>
  `a = (x - 2 * q * z, y + q * x - q ** 2 * z, z)` by fs[pung_funpow, Abbr`a`] >>
  `n = windmill a` by simp[GSYM pung_funpow_windmill, Abbr`a`] >>
  `b = (2 * (q + 1) * z - x, z, y + (q + 1) * x - (q + 1) ** 2 * z)` by
  (fs[pong_def, Abbr`b`] >>
  `0 < x - 2 * (q * z) /\ 0 < y + q * x - q ** 2 * z` by metis_tac[windmill_mind_and_arms] >>
  simp[SUM_SQUARED]) >>
  `n = windmill b` by simp[pong_windmill_alt, Abbr`b`] >>
  `c = (2 * (p + (q + 1)) * z - x, z, y + (p + (q + 1)) * x - (p + (q + 1)) ** 2 * z)` by
    (`p = 0 \/ 0 < p` by decide_tac >-
  simp[Abbr`c`] >>
  qabbrev_tac `qq = q + 1` >>
  fs[ping_funpow, Abbr`c`] >>
  `0 < 2 * (qq * z) - x /\ 0 < y + qq * x - qq ** 2 * z` by metis_tac[windmill_mind_and_arms] >>
  rfs[] >>
  `0 < p * (2 * (qq * z) - x)` by simp[] >>
  `0 < p * 2 * (qq * z) - p * x` by decide_tac >>
  `y + qq * x - (p * (2 * (qq * z) - x) + (p ** 2 * z + qq ** 2 * z)) = y + qq * x - (p ** 2 * z + qq ** 2 * z + p * (2 * (qq * z) - x))` by simp[] >>
  `_ = y + qq * x + p * x - (p ** 2 * z + qq ** 2 * z + 2 * p * qq * z)` by rfs[] >>
  `_ = y + (p + qq) * x - (p ** 2 + qq ** 2 + 2 * p * qq) * z` by rfs[] >>
  rfs[SUM_SQUARED]
  ) >>
  `0 < 2 * (q + 1) * z - x` by metis_tac[windmill_mind_and_arms] >>
  `0 < 2 * (p + (q + 1)) * z - x` by decide_tac >>
  simp[hop_def]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ is_pong (FUNPOW pung q t) /\
            (!j. j < q ==> is_pung (FUNPOW pung j t)) ==>
            (FUNPOW ping p o pong o FUNPOW pung q) t = hop (p + q + 1) t *)
(* Proof: by pung_to_ping_by_hop, triple_parts. *)
Theorem pung_to_ping_by_hop_alt:
  !n t p q. tik n /\ ~square n /\ n = windmill t /\ is_pong (FUNPOW pung q t) /\
                (!j. j < q ==> is_pung (FUNPOW pung j t)) ==>
                (FUNPOW ping p o pong o FUNPOW pung q) t = hop (p + q + 1) t
Proof
  metis_tac[pung_to_ping_by_hop, triple_parts]
QED

(* Theorem: 0 < z /\ m < x DIV (2 * z) ==> is_pung (hop m (x,y,z)) *)
(* Proof:
   With m < x DIV (2 * z),
        hop m (x,y,z) = (x - 2 * m * z,y + m * x - m * m * z,z)
                                               by hop_alt
   Note m < x DIV (2 * z)
    <=> 2 * (m + 1) * z <= x                   by X_LT_DIV, 0 < z
    ==> 2 * m * z <= x                         by inequality
    and 2 * (m + 1) * z - x = 0                by arithmetic
   Now 2 * z - (x - 2 * m * z)
     = 2 * (m + 1) * z - x = 0                 by above
   Thus is_pung (hop m (x,y,z))                by is_pung_alt
*)
Theorem hop_over_pung:
  !m x y z. 0 < z /\ m < x DIV (2 * z) ==> is_pung (hop m (x,y,z))
Proof
  rpt strip_tac >>
  `0 < 2 * z` by decide_tac >>
  `2 * (m + 1) * z <= x` by fs[X_LT_DIV] >>
  fs[hop_alt, is_pung_alt]
QED

(* Theorem: 0 < z /\ m = x DIV (2 * z) ==> ~is_pung (hop m (x,y,z) *)
(* Proof:
   With m = x DIV (2 * z),
        hop m (x,y,z) = (x - 2 * m * z,y + m * x - m * m * z,z)
                                               by hop_alt
   Note 2 * m * z
      = (x DIV (2 * z)) * (2 * z) <= x         by DIV_MULT_LE
     so 2 * z - (x - 2 * m * z)
      = 2 * (m + 1) * z - x                    by SUB_SUB, 2 * m * z <= x

    Now x DIV (2 * z) <= m
    ==> x DIV (2 * z) < m + 1
    <=> x < 2 * (m + 1) * z                    by DIV_LT_X, 0 < z
    <=> 0 < 2 * (m + 1) * z - x                by inequality
   Thus 2 * (m + 1) * z - x <> 0               by NOT_ZERO
    ==> ~is_pung (hop m (x,y,z))               by is_pung_alt
*)
Theorem hop_beyond_pung:
  !m x y z. 0 < z /\ m = x DIV (2 * z) ==> ~is_pung (hop m (x,y,z))
Proof
  rpt strip_tac >>
  `~(x DIV (2 * z) < m)` by decide_tac >>
  qabbrev_tac `X = (m = x DIV (2 * z))` >>
  rfs[hop_alt] >>
  fs[] >>
  qunabbrev_tac `X` >>
  `0 < 2 * z` by decide_tac >>
  `m * (2 * z) <= x` by metis_tac[DIV_MULT_LE] >>
  `2 * z - (x - 2 * (m * z)) = 2 * (m + 1) * z - x` by decide_tac >>
  `x DIV (2 * z) < m + 1` by decide_tac >>
  `x < 2 * (m + 1) * z` by rfs[DIV_LT_X] >>
  fs[is_pung_alt]
QED


(* ------------------------------------------------------------------------- *)
(* Pong Indices along a Path.                                                *)
(* ------------------------------------------------------------------------- *)

(*

The (ls = path n k) has a finite number of pongs.
Need to count them:
The first 0-th pong (at HD ls) gives (block ls 0)
The next 1-th pong gives (block ls 1), etc.
The last j-th pong gives (block ls j), with third at (LAST ls) such that flip (LAST ls) = LAST ls.

*)

(* Extract the indices of pong elements in a list. *)
Definition pong_indices_def:
    pong_indices ls = FILTER (\j. is_pong (EL j ls)) [0 ..< LENGTH ls]
End

(*
> EVAL ``pong_indices (path 41 6)``; = [0; 3; 4]: thm
> EVAL ``pong_indices (path 61 6)``; = [0; 5; 6]: thm
> EVAL ``pong_indices (path 97 14)``; = [0; 6; 8; 9; 11]: thm
*)


(* Theorem: MEM j (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls) *)
(* Proof:
   Let P = \j. is_pong (EL j ls).
       MEM j (pong_indices ls)
   <=> MEM j (FILTER P [0 ..< LENGTH ls])      by pong_indices_def
   <=> P j /\ MEM j [0 ..< LENGTH ls]          by MEM_FILTER
   <=> P j /\ j < LENGTH ls                    by MEM_listRangeLHI
   <=> j < LENGTH ls /\ is_pong (EL j ls)      by notation
*)
Theorem pong_indices_mem:
  !ls j. MEM j (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls)
Proof
  simp[pong_indices_def, MEM_FILTER] >>
  metis_tac[]
QED

(* Theorem: j IN set (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls) *)
(* Proof:
   Let P = \j. is_pong (EL j ls),
       k = LENGTH ls.
       j IN set (pong_indices ls)
   <=> j IN set (FILTER P [0 ..< k])           by pong_indices_def
   <=> j IN {j | P j} INTER set [0 ..< k]      by LIST_TO_SET_FILTER
   <=> j IN {j | P j} INTER (count k)          by listRangeLHI_SET
   <=> P j /\ j IN (count k)                   by IN_INTER
   <=> j < LENGTH ls /\ is_pong (EL j ls)      by IN_COUNT
*)
Theorem pong_indices_element:
  !ls j. j IN set (pong_indices ls) <=> j < LENGTH ls /\ is_pong (EL j ls)
Proof
  simp[pong_indices_def, LIST_TO_SET_FILTER] >>
  metis_tac[]
QED

(* Theorem: pong_indices [] = [] *)
(* Proof:
   Let P = \j. is_pong (EL j []).
     pong_indices []
   = FILTER P [0 ..< LENGTH []]    by pong_indices_def
   = FILTER P [0 ..< 0]            by LENGTH
   = FILTER P []                   by listRangeLHI_EQ
   = []                            by FILTER
*)
Theorem pong_indices_nil:
  pong_indices [] = []
Proof
  simp[pong_indices_def]
QED

(* Theorem: set (pong_indices []) = {} *)
(* Proof:
     set (pong_indices [])
   = set []                        by pong_indices_nil
   = {}                            by LIST_TO_SET
*)
Theorem pong_indices_empty:
  set (pong_indices []) = {}
Proof
  simp[pong_indices_nil]
QED

(* Theorem: set (pong_indices ls) SUBSET count (LENGTH ls) *)
(* Proof:
       j IN set (pong_indices ls)
   <=> j < LENGTH ls /\ is_pong (EL j ls)      by pong_indices_element
   ==> j < LENGTH ls                           by implication
   <=> j IN count (LENGTH ls)                  by IN_COUNT
   Hence true                                  by SUBSET_DEF
*)
Theorem pong_indices_subset:
  !ls. set (pong_indices ls) SUBSET count (LENGTH ls)
Proof
  simp[pong_indices_element, SUBSET_DEF]
QED

(* Theorem: FINITE (set (pong_indices ls)) *)
(* Proof:
   Let s = count (LENGTH ls).
   Note set (pong_indices ls) SUBSET s         by pong_indices_subset
    and FINITE s                               by FINITE_COUNT
    ==> FINITE (pong_indices ls)               by SUBSET_FINITE
*)
Theorem pong_indices_finite:
  !ls. FINITE (set (pong_indices ls))
Proof
  metis_tac[pong_indices_subset, SUBSET_FINITE, FINITE_COUNT]
QED

(* Theorem: LENGTH (pong_indices ls) <= LENGTH ls *)
(* Proof:
   Let P = \j. is_pong (EL j ls).
      LENGTH (pong_indices ls)
    = LENGTH (FILTER P [0 ..< LENGTH ls])      by pong_indices_def
   <= LENGTH [0 ..< LENGTH ls]                 by LENGTH_FILTER_LEQ
    = LENGTH ls                                by listRangeLHI_LEN
*)
Theorem pong_indices_length:
  !ls. LENGTH (pong_indices ls) <= LENGTH ls
Proof
  rw[pong_indices_def] >>
  qabbrev_tac `P = \j. is_pong (EL j ls)` >>
  qabbrev_tac `k = LENGTH ls` >>
  `LENGTH (FILTER P [0 ..< k]) <= LENGTH [0 ..< k]` by simp[LENGTH_FILTER_LEQ] >>
  fs[listRangeLHI_LEN]
QED

(* Theorem: ALL_DISTINCT (pong_indices ls) *)
(* Proof:
   Let P = \j. is_pong (EL j ls).
       ALL_DISTINCT (pong_indices ls)
   <=> ALL_DISTINCT (FILTER P [0 ..< LENGTH ls])   by pong_indices_def
   <=> ALL_DISTINCT [0 ..< LENGTH ls]              by FILTER_ALL_DISTINCT
   <=> T                                           by listRangeLHI_ALL_DISTINCT
*)
Theorem pong_indices_all_distinct:
  !ls. ALL_DISTINCT (pong_indices ls)
Proof
  simp[pong_indices_def, FILTER_ALL_DISTINCT, listRangeLHI_ALL_DISTINCT]
QED

(*
n = 61 is an example with a pong at the end.

> EVAL ``pong_indices (FRONT (path 41 6))``; = [0; 3; 4]: thm
> EVAL ``pong_indices (FRONT (path 61 6))``; = [0; 5]: thm
> EVAL ``pong_indices (FRONT (path 97 14))``; = [0; 6; 8; 9; 11]: thm

Note:
> EVAL ``(path 5 0)``; = [(1,1,1)]
> EVAL ``block (path 5 0) 0``; non-terminating!
Rather than altering the definition to, say:
block ls 0 = (0,0, if 1 < LENGTH ls then skip_idx_ping ls 1 else 0)
In view of:
> EVAL ``FRONT (path 5 0)``; = []
better to say: for n = 5, there is no block!

Note:
In order to prove that, for (u,v,w) = block ls j, u < k /\ ~is_ping (EL u ls),
the index j must be restricted, as (block ls j) is meaningful
only for j < LENGTH (pong_indices (FRONT (path n k))).
*)

(* Theorem: 0 < k ==> ?t. pong_indices (FRONT (path n k)) = 0 :: t *)
(* Proof:
   Let ls = path n k.
   Note ls <> []                   by path_not_nil
    and LENGTH ls = k + 1          by path_length
     so LENGTH (FRONT ls) = k      by LENGTH_FRONT

   Let fs = FRONT ls.
   Let P = \j. is_pong (EL j fs).
   Then P 0
      = is_pong (EL 0 fs)
      = is_pong (HD fs)            by EL
      = is_pong (1,n DIV 4,1)      by path_front_head
      = T                          by is_pong_x_y_x

      pong_indices (FRONT ls)
    = pong_indices fs              by notation
    = FILTER P [0 ..< k]           by pong_indices_def
    = FILTER P (0::[1 ..< k])      by listRangeLHI_CONS
    = 0 :: FILTER P [1 ..< k]      by FILTER, P 0

    Take t = FILTER P [1 ..< k],
      so pong_indices (FRONT ls) = 0 :: t
*)
Theorem pong_indices_path_cons:
  !n k. 0 < k ==> ?t. pong_indices (FRONT (path n k)) = 0 :: t
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `ls <> []` by simp[path_not_nil, Abbr`ls`] >>
  `LENGTH (FRONT ls) = k` by fs[LENGTH_FRONT, path_length, Abbr`ls`] >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `P = \j. is_pong (EL j fs)` >>
  `P 0` by simp[path_front_head, is_pong_x_y_x, Abbr`P`, Abbr`fs`, Abbr`ls`] >>
  `[0 ..< k] = 0::[1 ..< k]` by simp[listRangeLHI_CONS] >>
  `pong_indices fs = 0 :: FILTER P [1 ..< k]` by fs[pong_indices_def, Abbr`fs`] >>
  metis_tac[]
QED

(* Theorem: 0 < k ==> pong_indices (FRONT (path n k)) <> [] *)
(* Proof:
   Let ls = path n k.
   Note ?t. pong_indices (FRONT ls) = 0 :: t   by pong_indices_path_cons
     so pong_indices (FRONT ls) <> []          by NOT_NIL_CONS
*)
Theorem pong_indices_path_not_nil:
  !n k. 0 < k ==> pong_indices (FRONT (path n k)) <> []
Proof
  metis_tac[pong_indices_path_cons, NOT_NIL_CONS]
QED

(* Theorem: 0 < k ==> HD (pong_indices (FRONT (path n k))) = 0 *)
(* Proof:
   Let ls = path n k.
   Note ?t. pong_indices (FRONT ls) = 0 :: t   by pong_indices_path_cons
     so HD pong_indices (FRONT ls) = 0         by HD
*)
Theorem pong_indices_path_head:
  !n k. 0 < k ==> HD (pong_indices (FRONT (path n k))) = 0
Proof
  metis_tac[pong_indices_path_cons, HD]
QED

(* Theorem: pong_indices (FRONT (path n k)) = [] <=> k = 0 *)
(* Proof:
   If part: pong_indices (FRONT (path n k)) = [] ==> k = 0
      By contradiction, suppose k <> 0.
      Then 0 < k                                   by NOT_ZERO
        so pong_indices (FRONT (path n k)) <> []   by pong_indices_path_not_nil
      which is a contradiction.
   Only-if part: k = 0 ==> pong_indices (FRONT (path n k)) = []
        pong_indices (FRONT (path n 0))
      = pong_indices (FRONT [(1,n DIV 4,1)])       by path_0
      = pong_indices []                            by FRONT_CONS
      = []                                         by pong_indices_nil
*)
Theorem pong_indices_path_eq_nil:
  !n k. pong_indices (FRONT (path n k)) = [] <=> k = 0
Proof
  rw[EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `0 < k` by decide_tac >>
    fs[pong_indices_path_not_nil],
    simp[path_0, pong_indices_nil]
  ]
QED

(* Theorem: let ls = path n k in MEM j (pong_indices (FRONT ls)) <=> j < k /\ is_pong (EL j ls) *)
(* Proof:
   Let ls = path n k,
       fs = FRONT ls,
        P = \j. is_pong (EL j fs).
   Note ls <> []                       by path_not_nil
    and LENGTH fs = k                  by path_front_length
   Note !p. p < k ==>
            EL p fs = EL p ls          by FRONT_EL, ls <> [], [1]

       MEM j (pong_indices fs)
   <=> j < k /\ is_pong (EL j fs)      by pong_indices_mem
   <=> j < k /\ is_pong (EL j ls)      by [1], j < k
*)
Theorem pong_indices_path_element:
  !n k j. let ls = path n k in MEM j (pong_indices (FRONT ls)) <=> j < k /\ is_pong (EL j ls)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `P = \j. is_pong (EL j fs)` >>
  `ls <> [] /\ LENGTH fs = k` by simp[path_not_nil, path_front_length, Abbr`ls`, Abbr`fs`] >>
  metis_tac[pong_indices_mem, FRONT_EL]
QED

(* Theorem: let px = pong_indices ls in j + 1 < LENGTH px ==> EL j px < EL (j + 1) px *)
(* Proof:
   Let px = pong_indices ls,
        k = LENGTH ls,
        P = \j. is_pong (EL j ls).
   Then px = FILTER P [0 ..< k]                by pong_indices_def

    Now MONO_INC [0 ..< k]                     by listRangeLHI_MONO_INC
    ==> MONO_INC px                            by FILTER_MONO_INC
    ==> EL j px <= EL (j + 1) px               by notation

    But ALL_DISTINCT [0 ..< k]                 by listRangeLHI_ALL_DISTINCT
    ==> ALL_DISTINCT px                        by FILTER_ALL_DISTINCT
     so EL j px <> EL (j + 1) px               by EL_ALL_DISTINCT_EL_EQ

   Thus EL j px < EL (j + 1) px
*)
Theorem pong_indices_monotonic:
  !ls j. let px = pong_indices ls in j + 1 < LENGTH px ==> EL j px < EL (j + 1) px
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `k = LENGTH ls` >>
  qabbrev_tac `P = \j. is_pong (EL j ls)` >>
  `px = FILTER P [0 ..< k]` by simp[pong_indices_def, Abbr`px`, Abbr`k`, Abbr`P`] >>
  `MONO_INC px` by fs[FILTER_MONO_INC, listRangeLHI_MONO_INC, Abbr`px`] >>
  `ALL_DISTINCT px` by fs[FILTER_ALL_DISTINCT, listRangeLHI_ALL_DISTINCT, Abbr`px`] >>
  `EL j px <= EL (j + 1) px` by fs[] >>
  `EL j px <> EL (j + 1) px` by fs[EL_ALL_DISTINCT_EL_EQ] >>
  decide_tac
QED

(* Theorem: let ls = path n k; px = pong_indices (FRONT ls) in j + 1 < LENGTH px ==>
            !p. EL j px < p /\ p < EL (j + 1) px ==> ~is_pong (EL p ls) *)
(* Proof:
   Let ls = path n k,
       fs = FRONT ls,
        P = \j. is_pong (EL j fs),
       px = pong_indices ls,
        h = LENGTH px,
        a = EL j px,
        b = EL (j + 1) px.
   The goal is to show: j + 1 < h /\ a < p /\ p < b ==> ~is_pong (EL p ls)

   Note ls <> []                               by path_not_nil
    and LENGTH fs = k                          by path_front_length
    and px = FILTER P [0 ..< k]                by pong_indices_def
    and h <= k                                 by pong_indices_length

   Note MEM a px                               by MEM_EL, j - 1 < h
    and MEM b px                               by MEM_EL, j < h
    ==> a < k /\ is_pong (EL a fs)             by pong_indices_element
    and b < k /\ is_pong (EL b fs)             by pong_indices_element

   Note a < b                                  by a < p /\ p < b
        [0 ..< k]
      = [0 ..< b] ++ b::[b+1 ..< k]                    by listRangeLHI_SPLIT, b < k
      = [0 ..< a] ++ a::[a+1 ..< b] ++ b::[b+1 ..< k]  by listRangeLHI_SPLIT, a < b
    Now ALL_DISTINCT [0 ..< k]                 by listRangeLHI_ALL_DISTINCT
    ==> ALL_DISTINCT px                        by FILTER_ALL_DISTINCT
   Thus findi a px = j                         by findi_EL
    and findi b px = j + 1                     by findi_EL
     or findi b px = 1 + findi a px            by above

   Note P a /\ P b                             by application
   Thus FILTER P [a + 1 ..< b] = []            by FILTER_EL_NEXT_IDX
    <=> EVERY (\x. ~P x) [a + 1 ..< b]         by FILTER_EQ_NIL
    <=> !p. a + 1 <= p /\ p < b ==> ~P p       by listRangeLHI_EVERY
     so ~is_pong (EL p fs)                     by a < p, p < b
    ==> ~is_pong (EL p ls)                     by FRONT_EL, p < k
*)
Theorem pong_indices_path_pong_gap:
  !n k j. let ls = path n k; px = pong_indices (FRONT ls) in j + 1 < LENGTH px ==>
          !p. EL j px < p /\ p < EL (j + 1) px ==> ~is_pong (EL p ls)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `P = \j. is_pong (EL j fs)` >>
  qabbrev_tac `h = LENGTH px` >>
  qabbrev_tac `a = EL j px` >>
  qabbrev_tac `b = EL (j + 1) px` >>
  `ls <> [] /\ LENGTH fs = k` by metis_tac[path_not_nil, path_front_length] >>
  `px = FILTER P [0 ..< k]` by fs[pong_indices_def, Abbr`px`, Abbr`P`] >>
  `h <= k` by metis_tac[pong_indices_length] >>
  `j < h` by decide_tac >>
  `MEM a px /\ MEM b px` by metis_tac[MEM_EL] >>
  `a < k /\ is_pong (EL a fs)` by fs[pong_indices_element, Abbr`px`] >>
  `b < k /\ is_pong (EL b fs)` by fs[pong_indices_element, Abbr`px`] >>
  `a < b` by decide_tac >>
  `[0 ..< k] = [0 ..< b] ++ b::[b+1 ..< k]` by fs[listRangeLHI_SPLIT] >>
  `_ = [0 ..< a] ++ a::[a+1 ..< b] ++ b::[b+1 ..< k]` by fs[listRangeLHI_SPLIT] >>
  `ALL_DISTINCT [0 ..< k]` by simp[listRangeLHI_ALL_DISTINCT] >>
  `ALL_DISTINCT px` by metis_tac[FILTER_ALL_DISTINCT] >>
  `findi b px = 1 + findi a px` by fs[findi_EL, Abbr`b`, Abbr`a`] >>
  `P a /\ P b` by fs[Abbr`P`] >>
  `FILTER P [a + 1 ..< b] = []` by metis_tac[FILTER_EL_NEXT_IDX] >>
  `EVERY (\x. ~P x) [a + 1 ..< b]` by fs[FILTER_EQ_NIL] >>
  `~is_pong (EL p fs)` by fs[listRangeLHI_EVERY, Abbr`P`] >>
  `p < k` by decide_tac >>
  metis_tac[FRONT_EL]
QED

(* A major key result! *)

(* Idea: after the LAST in pong_indices (FRONT (path n k)), there is no more pong. *)

(* Theorem: let ls = path n k; px = pong_indices (FRONT ls); v = LAST px in 0 < k ==>
            v < k /\ is_pong (EL v ls) /\ !p. v < p /\ p < k ==> ~is_pong (EL p ls) *)
(* Proof:
   Let ls = path n k,
       fs = FRONT ls,
       px = pong_indices fs,
        P = \j. is_pong (EL j fs),
        v = LAST px.
   The goal is to show: is_pong (EL v ls) /\ !p. v < p /\ p < k ==> ~is_pong (EL p ls)

   Note ls <> []                               by path_not_nil
    and LENGTH fs = k                          by path_front_length
    and px <> []                               by pong_indices_path_eq_nil, 0 < k
    and px = FILTER P [0 ..< k]                by pong_indices_def
   also !j. j < k ==> EL j fs = EL j ls        by FRONT_EL, ls <> [], [1]

   Note MEM v px                               by MEM_LAST, list_CASES, px <> []
    ==> v < k /\ is_pong (EL v ls)             by pong_indices_path_element
     so is_pong (EL v fs)                      by [1]
    and P v                                    by application

   Note ALL_DISTINCT [0 ..< k]                 by listRangeLHI_ALL_DISTINCT
    and [0 ..< k]
      = [0 ..< v] ++ v::[v+1 ..< k]            by listRangeLHI_SPLIT, v < k
   Thus FILTER P [v + 1 ..< k] = []            by FILTER_LAST_IFF
    <=> EVERY (\x. ~P x) [v + 1 ..< k]         by FILTER_EQ_NIL
    <=> !n. n < LENGTH [v + 1 ..< k] ==>
            (\x. ~P x) (EL n [v + 1 ..< k])    by EVERY_EL
    But LENGTH [v + 1 ..< k] = k - (v + 1)     by listRangeLHI_LEN
     so n < k - (v + 1) <=> n + (v + 1) < k    by SUB_LEFT_LESS
    ==> EL n [v + 1 ..< k] = n + (v + 1)       by listRangeLHI_EL, n + (v + 1) < k

   Note v < p /\ p < k
    <=> v + 1 <= p /\ p < k
    <=> 0 <= p - (v + 1) /\ p - (v + 1) < k - (v + 1)
    Put n = p - (v + 1),
   then ~is_pong (EL (p - (v + 1) + (v + 1)) fs)
     or ~is_pong (EL p fs)
     or ~is_pong (EL p ls)                     by FRONT_EL, [1]
*)
Theorem pong_indices_path_last:
  !n k. let ls = path n k; px = pong_indices (FRONT ls); v = LAST px in 0 < k ==>
        v < k /\ is_pong (EL v ls) /\ !p. v < p /\ p < k ==> ~is_pong (EL p ls)
Proof
  simp[] >>
  ntac 3 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `px = pong_indices fs` >>
  qabbrev_tac `P = \j. is_pong (EL j fs)` >>
  qabbrev_tac `v = LAST px` >>
  `ls <> [] /\ LENGTH fs = k` by simp[path_not_nil, path_front_length, Abbr`fs`, Abbr`ls`] >>
  `px <> []` by simp[pong_indices_path_eq_nil, Abbr`px`, Abbr`fs`, Abbr`ls`] >>
  `px = FILTER P [0 ..< k]` by fs[pong_indices_def, Abbr`px`] >>
  `MEM v px` by metis_tac[MEM_LAST, list_CASES] >>
  `v < k /\ is_pong (EL v ls)` by metis_tac[pong_indices_path_element] >>
  simp[] >>
  rpt strip_tac >>
  `!j. j < k ==> EL j fs = EL j ls` by metis_tac[FRONT_EL] >>
  `P v` by fs[Abbr`P`] >>
  `ALL_DISTINCT [0 ..< k]` by simp[listRangeLHI_ALL_DISTINCT] >>
  `[0 ..< k] = [0 ..< v] ++ v::[v+1 ..< k]` by simp[listRangeLHI_SPLIT] >>
  `FILTER P [v + 1 ..< k] = []` by metis_tac[FILTER_LAST_IFF] >>
  fs[FILTER_EQ_NIL, EVERY_EL, listRangeLHI_EL, Abbr`P`] >>
  first_x_assum (qspecl_then [`p - (v + 1)`] strip_assume_tac) >>
  rfs[]
QED

(* ------------------------------------------------------------------------- *)
(* Define blocks based on pong indices.                                      *)
(* ------------------------------------------------------------------------- *)

(*
> EVAL ``path 41 6``; = [(1,10,1); (1,1,10); (3,1,8); (5,1,4); (3,4,2); (1,2,5); (5,2,2)]: thm
> EVAL ``pong_indices (FRONT (path 41 6))``; = [0; 3; 4]: thm
> EVAL ``let ls = path 41 6 in MAP (\j. (j, skip_idx_ping ls (j+1))) (pong_indices (FRONT ls))``; = [(0,3); (3,4); (4,6)]
> EVAL ``let ls = [(0,3); (3,4); (4,6)] in
          MAP (\j. (if j = 0 then 0 else SND(EL (j-1) ls), (EL j ls))) [0 ..< LENGTH ls]``; = [(0,0,3); (3,3,4); (4,4,6)]
*)

(* Define a list of pairs from is_pong to skip_idx_ping *)
Definition block_pairs_def:
   block_pairs ls = MAP (\j. (j, skip_idx_ping ls (j+1))) (pong_indices (FRONT ls))
End

(* Define a list of triples starting from skip_idx_ping of last pair. *)
Definition blocks_def:
   blocks ls = MAP (\j. (if j = 0 then 0 else SND (EL (j-1) ls), EL j ls)) [0 ..< LENGTH ls]
End

(*
> EVAL ``blocks (block_pairs (path 41 6))``; = [(0,0,3); (3,3,4); (4,4,6)]
> EVAL ``blocks (block_pairs (path 61 6))``; = [(0,0,4); (4,5,6)]
> EVAL ``blocks (block_pairs (path 97 14))``; = [(0,0,5); (5,6,7); (7,8,9); (9,9,10); (10,11,14)]
> EVAL ``blocks (block_pairs (path 5 0))``; = []: thm
*)

(* ------------------------------------------------------------------------- *)
(* Block Pairs Theorems.                                                     *)
(* ------------------------------------------------------------------------- *)

(* Theorem: block_pairs [x] = [] *)
(* Proof:
   Let f = \j. (j, skip_idx_ping ls (j+1)).
     block_pairs [x]
   = MAP f (pong_indices (FRONT [x]))          by block_pairs_def
   = MAP f (pong_indices [])                   by FRONT_CONS
   = MAP f []                                  by pong_indices_nil
   = []                                        by MAP
*)
Theorem block_pairs_nil:
  !x. block_pairs [x] = []
Proof
  simp[block_pairs_def, pong_indices_nil]
QED

(* Theorem: LENGTH (block_pairs ls) = LENGTH (pong_indices (FRONT ls)) *)
(* Proof:
   Let f = \j. (j,skip_idx_ping ls (j + 1)).
     LENGTH (block_pairs ls)
   = LENGTH (MAP f (pong_indices (FRONT ls)))  by block_pairs_def
   = LENGTH (pong_indices (FRONT ls))          by LENGTH_MAP
*)
Theorem block_pairs_length:
  !ls. LENGTH (block_pairs ls) = LENGTH (pong_indices (FRONT ls))
Proof
  simp[block_pairs_def]
QED

(* Theorem: MEM (v,w) (block_pairs ls) <=>
            MEM v (pong_indices (FRONT ls)) /\ w = skip_idx_ping ls (v+1) *)
(* Proof:
   Let ps = block_pairs ls,
       px = pong_indices (FRONT ls),
        f = \j. (j,skip_idx_ping ls (j + 1)).
   Then ps =
       MEM (v,w) ps
   <=> MEM (v,w) (MAP f px)                    by block_pairs_def
   <=> ?j. (v,w) = f j /\ MEM j px             by MEM_MAP
   <=> ?j. v = j /\ w = skip_idx_ping ls (j+1) /\ MEM j px
                                               by application, PAIR_EQ
   <=> MEM v px /\ w = skip_idx_ping ls (v+1)      by v = j
*)
Theorem block_pairs_mem:
  !ls v w. MEM (v,w) (block_pairs ls) <=>
           MEM v (pong_indices (FRONT ls)) /\ w = skip_idx_ping ls (v+1)
Proof
  rw[block_pairs_def, MEM_MAP] >>
  decide_tac
QED

(* Theorem: let ps = block_pairs ls in j < LENGTH ps ==>
            ((v, w) = EL j ps <=> v = EL j (pong_indices (FRONT ls)) /\ w = skip_idx_ping ls (v+1)) *)
(* Proof:
   Let ps = block_pairs ls,
       px = pong_indices (FRONT ls),
        f = \j. (j,skip_idx_ping ls (j + 1)).
   Then f v = (v, skip_idx_ping ls (v+1))      by application
    and INJ f (set px) univ(:num # num)        by PAIR_EQ
    and ps = MAP f px                          by block_pairs_def
    and LENGTH px = LENGTH ps                  by LENGTH_MAP

        f v = EL j ps
            = EL j (MAP f px)                  by above
            = f (EL j px)                      by EL_MAP
     <=>  v = EL j px                          by INJ_IMP_11
    and   w = skip_idx_ping ls (v+1)           by PAIR_EQ
*)
Theorem block_pairs_element:
  !ls j v w. let ps = block_pairs ls in j < LENGTH ps ==>
              ((v, w) = EL j ps <=> v = EL j (pong_indices (FRONT ls)) /\ w = skip_idx_ping ls (v+1))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `h = LENGTH ps` >>
  qabbrev_tac `px = pong_indices (FRONT ls)` >>
  qabbrev_tac `f = \j. (j,skip_idx_ping ls (j + 1))` >>
  `f v = (v, skip_idx_ping ls (v+1))` by fs[Abbr`f`] >>
  `INJ f univ(:num) univ(:num # num)` by rw[INJ_DEF, Abbr`f`] >>
  `ps = MAP f px` by simp[block_pairs_def, Abbr`ps`, Abbr`f`, Abbr`px`] >>
  `LENGTH px = h` by simp[Abbr`px`, Abbr`h`] >>
  metis_tac[EL_MAP, INJ_IMP_11, PAIR_EQ]
QED

(* Theorem: block_pairs (path n 0) = [] *)
(* Proof:
     block_pairs (path n 0)
   = block_pairs [(1,1,n DIV 4)]   by path_0
   = []                            by block_pairs_nil
*)
Theorem block_pairs_path_0:
  !n. block_pairs (path n 0) = []
Proof
  simp[path_0, block_pairs_nil]
QED

(* Theorem: 0 < k ==> ?t. block_pairs (path n k) = (0, skip_idx_ping (path n k) 1) :: t *)
(* Proof:
   Let ls = path n k,
        f = \j. (j, skip_idx_ping ls (j+1)).
   Note ?tt. pong_indices (FRONT ls) = 0::tt   by pong_indices_path_cons, 0 < k
     block_pairs ls
   = MAP f (pong_indices (FRONT ls))           by block_pairs_def
   = MAP f (0 :: tt)                           by above
   = f 0 :: MAP f tt                           by MAP
   = (0, skip_idx_ping ls 1) :: MAP f tt       by applying f

   Pick (MAP f tt) as the tail t.
*)
Theorem block_pairs_path_cons:
  !n k. 0 < k ==> ?t. block_pairs (path n k) = (0, skip_idx_ping (path n k) 1) :: t
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `f = \j. (j, skip_idx_ping ls (j+1))` >>
  `?t. pong_indices (FRONT ls) = 0 :: t` by metis_tac[pong_indices_path_cons] >>
  qexists_tac `MAP f t` >>
  simp[block_pairs_def, Abbr`ls`, Abbr`f`]
QED

(* Theorem: 0 < k ==> block_pairs (path n k) <> [] *)
(* Proof:
   Note ?t. block_pairs (path n k)
          = (0, skip_idx_ping (path n k) 1) :: t   by block_pairs_path_cons
   Thus block_pairs (path n k) <> []               by NOT_NIL_CONS
*)
Theorem block_pairs_path_not_nil:
  !n k. 0 < k ==> block_pairs (path n k) <> []
Proof
  metis_tac[block_pairs_path_cons, NOT_NIL_CONS]
QED

(* Theorem: 0 < k ==> HD (block_pairs (path n k)) = (0, skip_idx_ping (path n k) 1) *)
(* Proof:
   Note ?t. block_pairs (path n k)
          = (0, skip_idx_ping (path n k) 1) :: t   by block_pairs_path_cons
     so HD (block_pairs (path n k))
      = (0, skip_idx_ping (path n k) 1)            by HD
*)
Theorem block_pairs_path_head:
  !n k. 0 < k ==> HD (block_pairs (path n k)) = (0, skip_idx_ping (path n k) 1)
Proof
  metis_tac[block_pairs_path_cons, HD]
QED

(* Theorem: block_pairs (path n k) = [] <=> k = 0 *)
(* Proof:
   If part: block_pairs (path n k) = [] ==> k = 0
      By contradiction, suppose k <> 0.
      Then 0 < k                               by NOT_ZERO
       and block_pairs (path n k) <> []        by block_pairs_path_not_nil
      which is a contradiction.

   Only-if part: k = 0 ==> block_pairs (path n k) = []
        block_pairs (path n 0)
      = []                                     by block_pairs_path_0
*)
Theorem block_pairs_path_eq_nil:
  !n k. block_pairs (path n k) = [] <=> k = 0
Proof
  rw[EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `0 < k` by decide_tac >>
    fs[block_pairs_path_not_nil],
    simp[block_pairs_path_0]
  ]
QED

(* Theorem: let ls = path n k in ~is_ping (LAST ls) /\ MEM (v,w) (block_pairs ls) ==>
            v < w /\ w <= k /\ w = skip_idx_ping ls (v + 1) /\
            is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
            (!j. v < j /\ j < w ==> is_ping (EL j ls)) *)
(* Proof:
   Let ls = path n k,
        f = \j. (j,skip_idx_ping ls (j + 1)).

       MEM (v,w) (block_pairs ls)
   <=> MEM (v,w) (MAP f (pong_indices (FRONT ls)))             by block_pairs_def
   ==> ?j. (v,w) = f j /\ MEM j (pong_indices (FRONT ls))      by MEM_MAP
   ==> ?j. (v,w) = f j /\ j < k /\ is_pong (EL j ls)           by pong_indices_path_element
   ==> ?h. j <= h /\ h < k /\
           (!p. j < p /\ p <= h ==> is_ping (EL p ls)) /\
           ~is_ping (EL (SUC h) ls)                            by pong_seed_ping_after
    or (!p. j + 1 <= p /\ p < SUC h ==> is_ping (EL p ls))     by above
   ==> skip_idx_ping ls (j + 1) = SUC h                        by skip_idx_ping_thm
   Now (v,w) = f j = (j, skip_idx_ping ls (j + 1))
    so v = j, w = skip_idx_ping ls (j + 1) = SUC h             by PAIR_EQ
   Thus v = j < SUC h = w,
        w = SUC h <= k,
        !j. v < j /\ j <= h <=> v < j /\ j < SUC h = w.
*)
Theorem block_pairs_path_mem:
  !n k v w. let ls = path n k in ~is_ping (LAST ls) /\ MEM (v,w) (block_pairs ls) ==>
            v < w /\ w <= k /\ w = skip_idx_ping ls (v + 1) /\
            is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
            (!j. v < j /\ j < w ==> is_ping (EL j ls))
Proof
  simp[] >>
  ntac 5 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `f = \j. (j,skip_idx_ping ls (j + 1))` >>
  `MEM (v,w) (MAP f (pong_indices (FRONT ls)))` by simp[GSYM block_pairs_def, Abbr`f`] >>
  fs[MEM_MAP] >>
  `y < k /\ is_pong (EL y ls)` by metis_tac[pong_indices_path_element] >>
  `f y = (y,skip_idx_ping ls (y + 1))` by simp[Abbr`f`] >>
  assume_tac pong_seed_ping_after >>
  last_x_assum (qspecl_then [`n`, `k`, `y`] strip_assume_tac) >>
  rfs[] >>
  `skip_idx_ping ls (y + 1) = SUC h` by fs[skip_idx_ping_thm] >>
  `v = y /\ w = SUC h` by metis_tac[PAIR_EQ] >>
  fs[]
QED

(* Theorem: let ls = path n k in
            (v < k /\ is_pong (EL v ls) <=> MEM (v, skip_idx_ping ls (v + 1)) (block_pairs ls)) *)
(* Proof:
   Let ls = path n k,
       fs = FRONT ls,
        f = \j. (j,skip_idx_ping ls (j + 1)).
   Thus block_pairs ls
      = MAP f (pong_indices fs)                by block_pairs_def
   Note ls <> []                               by path_not_nil
    and LENGTH fs = k                          by path_front_length
    and f v = (v, skip_idx_ping ls (v + 1))    by application

        v < k /\ is_pong (EL v ls)
    <=> MEM v (pong_indices fs)                by pong_indices_path_element, v < k
    <=> MEM (f v) (MAP f (pong_indices fs))    by MEM_MAP
*)
Theorem block_pairs_path_pong:
  !n k v. let ls = path n k in
          (v < k /\ is_pong (EL v ls) <=> MEM (v, skip_idx_ping ls (v + 1)) (block_pairs ls))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `fs = FRONT ls` >>
  qabbrev_tac `f = \j. (j,skip_idx_ping ls (j + 1))` >>
  `block_pairs ls = MAP f (pong_indices fs)` by simp[block_pairs_def, Abbr`ls`, Abbr`fs`] >>
  `ls <> [] /\ LENGTH fs = k` by simp[path_not_nil, path_front_length, Abbr`ls`, Abbr`fs`] >>
  `v < k /\ is_pong (EL v ls) <=> MEM v (pong_indices fs)` by metis_tac[pong_indices_path_element] >>
  simp[MEM_MAP, Abbr`f`]
QED

(* Theorem: let ls = path n k; ps = block_pairs ls in ~is_ping (LAST ls) /\
            j + 1 < LENGTH ps /\ EL j ps = (a,b) /\ EL (j + 1) ps = (c,d) ==>
            b <= c /\ c = skip_idx_pung ls b /\
            !p. b <= p /\ p < c ==> is_pung (EL p ls) *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
        h = LENGTH ps.
   Then j < h ==> j - 1 < h                    by arithmetic
     so MEM (a,b) ps /\ MEM (c,d) ps           by MEM_EL
    and a < k /\ is_pong (EL a ls)             by block_pairs_path_mem, LESS_LESS_EQ_TRANS
    and c < k /\ is_pong (EL c ls)             by block_pairs_path_mem, LESS_LESS_EQ_TRANS

   Claim: a < c /\ !p. a < p /\ p < c ==> ~is_pong (EL p ls)
   Proof: Note a = EL (j - 1) px               by block_pairs_element
           and c = EL j px                     by block_pairs_element
           Now LENGTH px = h                   by block_pairs_length
           ==> a < c                           by pong_indices_monotonic
           and !p. a < p /\ p < c ==> ~is_pong (EL p ls)
                                               by pong_indices_path_pong_gap

  Thus ?cut. a < cut /\ cut <= c /\ ~is_ping (EL cut ls) /\
       !p. cut <= p /\ p < c ==> is_pung (EL p ls)
                                               by pong_interval_cut_exists, claim.
   Now ~is_pung (EL c ls)                      by pong_not_pung
    so c = skip_idx_pung ls cut                by skip_idx_pung_thm
   But cut = skip_idx_ping ls (a + 1)          by skip_idx_ping_thm
           = b                                 by block_pairs_path_mem
   Thus b <= c /\ c = skip_idx_pung ls b       by above
*)
Theorem block_pairs_path_next:
  !n k j a b c d. let ls = path n k; ps = block_pairs ls in ~is_ping (LAST ls) /\
                  j + 1 < LENGTH ps /\ EL j ps = (a,b) /\ EL (j + 1) ps = (c,d) ==>
                  b <= c /\ c = skip_idx_pung ls b /\
                  !p. b <= p /\ p < c ==> is_pung (EL p ls)
Proof
  simp[] >>
  ntac 8 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `h = LENGTH ps` >>
  `j < h` by decide_tac >>
  `MEM (a,b) ps /\ MEM (c,d) ps` by metis_tac[MEM_EL] >>
  `a < k /\ is_pong (EL a ls) /\ c < k /\ is_pong (EL c ls)` by metis_tac[block_pairs_path_mem, LESS_LESS_EQ_TRANS] >>
  `a < c /\ !p. a < p /\ p < c ==> ~is_pong (EL p ls)` by
  (qabbrev_tac `px = pong_indices (FRONT ls)` >>
  `a = EL j px /\ c = EL (j + 1) px` by metis_tac[block_pairs_element] >>
  `LENGTH px = h` by fs[block_pairs_length, Abbr`px`, Abbr`ps`] >>
  `a < c` by metis_tac[pong_indices_monotonic] >>
  metis_tac[pong_indices_path_pong_gap]) >>
  assume_tac pong_interval_cut_exists >>
  last_x_assum (qspecl_then [`n`, `k`, `a`, `c`] strip_assume_tac) >>
  rfs[] >>
  rename1 `cut <= _` >>
  `c = skip_idx_pung ls cut` by fs[skip_idx_pung_thm, pong_not_pung] >>
  `cut = skip_idx_ping ls (a + 1)` by fs[skip_idx_ping_thm] >>
  metis_tac[block_pairs_path_mem]
QED

(* A significant achievement! *)

(* Theorem: let ls = path n k; ps = block_pairs ls in ~is_ping (LAST ls) /\
            j + 1 < LENGTH ps /\ EL j ps = (a,b) /\ EL (j + 1) ps = (c,d) /\
            is_pong (EL b ls) ==> c = b *)
(* Proof:
   Note c = skip_idx_pung ls b     by block_pairs_path_next
    now ~is_pung (EL b ls)         by pong_not_pung
    ==> skip_idx_pung ls b = b     by skip_idx_pung_none
     so c = b
*)
Theorem block_pairs_path_next_pong:
  !n k j a b c d. let ls = path n k; ps = block_pairs ls in ~is_ping (LAST ls) /\
                  j + 1 < LENGTH ps /\ EL j ps = (a,b) /\ EL (j + 1) ps = (c,d) /\
                  is_pong (EL b ls) ==> c = b
Proof
  rw_tac std_ss[] >>
  `skip_idx_pung ls b = b` by simp[skip_idx_pung_none, pong_not_pung] >>
  metis_tac[block_pairs_path_next]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
            LAST (block_pairs ls) = (LAST (pong_indices (FRONT ls)), k) *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
        f = \j. (j,skip_idx_ping ls (j + 1)),
       px = pong_indices (FRONT ls),
        v = LAST px.
   The goal is to show: LAST ps = (v, k).

   Note px <> []                               by pong_indices_path_not_nil, 0 < k
        LAST ps
      = LAST (MAP f px)                        by block_pairs_def
      = f (LAST px) = f v                      by LAST_MAP
      = (v, skip_idx_ping ls (v + 1))          by application

   It remains to show: skip_idx_ping ls (v + 1) = k.

   Note v < k /\ is_pong (EL v ls) /\
        !p. v < p /\ p < k ==> ~is_pong (EL p ls)
                                               by pong_indices_path_last
    Now v < k ==> v + 1 <= k                   by arithmetic, [1]
    and ~is_ping (LAST ls)                     by path_last_not_ping
     or ~is_ping (EL k ls)                     by path_last_alt, [2]

  Claim: !j. v < j /\ j < k ==> is_ping (EL j ls)
  Proof: By contradiction, suppose ~is_ping (EL j ls).
         Note ~is_pong (EL j ls)               by v < j /\ j < k
           so is_pung (EL j ls)                by triple_cases_alt
         Let h = k - 1.
         Then ~is_pung (EL h ls)               by path_last_flip_fix_not_by_pung
           so j <> h, or j < h /\ h < k
          and ~is_pong (EL h ls)               by v < h /\ h < k
          ==> is_ping (EL h ls)                by triple_cases_alt
         Thus ?p. j < p /\ p < h /\ is_pong (EL p ls))
                                               by pung_to_ping_has_pong
          but ~is_pong (EL p ls)               by v < p /\ p < k
         This is a contradiction.

  Therefore, !j. v + 1 <= j /\ j < k ==> is_ping (EL j ls)
                                               by arithmetic, claim, [3]
  Hence skip_idx_ping ls (v + 1) = k           by skip_idx_ping_thm, [1],[2],[3].
*)
Theorem block_pairs_path_last:
  !n k. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
        LAST (block_pairs ls) = (LAST (pong_indices (FRONT ls)), k)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `f = \j. (j,skip_idx_ping ls (j + 1))` >>
  qabbrev_tac `px = pong_indices (FRONT ls)` >>
  qabbrev_tac `v = LAST px` >>
  `ps = MAP f px` by simp[block_pairs_def, Abbr`ps`, Abbr`f`, Abbr`px`] >>
  `px <> []` by simp[pong_indices_path_not_nil, Abbr`px`, Abbr`ls`] >>
  `LAST ps = (v, skip_idx_ping ls (v + 1))` by simp[LAST_MAP, Abbr`v`, Abbr`f`] >>
  `skip_idx_ping ls (v + 1) = k` suffices_by metis_tac[PAIR_EQ] >>
  assume_tac pong_indices_path_last >>
  last_x_assum (qspecl_then [`n`, `k`] strip_assume_tac) >>
  rfs[] >>
  `~is_ping (EL k ls)` by metis_tac[path_last_alt, path_last_not_ping] >>
  irule skip_idx_ping_thm >>
  rw[] >>
  spose_not_then strip_assume_tac >>
  `v < j /\ j <= k - 1 /\ v < k - 1 /\ k - 1 < k` by decide_tac >>
  qabbrev_tac `h = k - 1` >>
  `is_pung (EL j ls)` by metis_tac[triple_cases_alt] >>
  `~is_pung (EL h ls) /\ j <> h` by metis_tac[path_last_flip_fix_not_by_pung] >>
  `is_ping (EL h ls)` by metis_tac[triple_cases_alt] >>
  assume_tac pung_to_ping_has_pong >>
  last_x_assum (qspecl_then [`n`, `k`, `j`, `h`] strip_assume_tac) >>
  rfs[] >>
  `v < p /\ p < k` by decide_tac >>
  metis_tac[]
QED

(* A remarkable result showing the LAST block_pairs touches the k, the last of (path n k). *)

(* ------------------------------------------------------------------------- *)
(* Blocks Theorems.                                                          *)
(* ------------------------------------------------------------------------- *)

(* Theorem: blocks [] = [] *)
(* Proof:
   Let f = \j. (if j = 0 then 0 else SND (EL (j-1) ls), EL j ls).
     blocks []
   = MAP f [0 ..< LENGTH []]       by blocks_def
   = MAP f [0 ..< 0]               by LENGTH
   = MAP f []                      by listRangeLHI_NIL
   = []                            by MAP
*)
Theorem blocks_nil:
  blocks [] = []
Proof
  simp[blocks_def]
QED

(* Theorem: LENGTH (blocks ls) = LENGTH ls *)
(* Proof:
   Let f = \j. (if j = 0 then 0 else SND (EL (j-1) ls), EL j ls).
     LENGTH (blocks ls)
   = LENGTH (MAP f [0 ..< LENGTH ls])          by blocks_def
   = LENGTH [0 ..< LENGTH ls]                  by LENGTH_MAP
   = LENGTH ls                                 by listRangeLHI_LEN
*)
Theorem blocks_length:
  !ls. LENGTH (blocks ls) = LENGTH ls
Proof
  simp[blocks_def]
QED

(* Theorem: blocks ls = [] <=> ls = [] *)
(* Proof:
   Let f = \j. (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls).
       blocks ls = []
   <=> MAP f [0 ..< LENGTH ls] = []            by blocks_def
   <=> [0 ..< LENGTH ls] = []                  by MAP_EQ_NIL
   <=> LENGTH ls <= 0                          by listRangeLHI_NIL
   <=> ls = []                                 by LENGTH_NIL
*)
Theorem blocks_eq_nil:
  !ls. blocks ls = [] <=> ls = []
Proof
  simp[blocks_def, listRangeLHI_NIL]
QED

(* Theorem: ls <> [] ==> HD (blocks ls) = (0, HD ls) *)
(* Proof:
   Note 0 < LENGTH ls                          by LENGTH_NON_NIL
     so [0 ..< LENGTH ls] <> []                by listRangeLHI_NIL
   Let f = \j. (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls).
   Then HD (blocks ls)
      = HD (MAP f [0 ..< LENGTH ls])           by blocks_def
      = f (HD [0 ..< LENGTH ls])               by MAP_HD
      = f 0                                    by listRangeLHI_CONS
      = (0, EL 0 ls)                           by application
      = (0, HD ls)                             by EL
*)
Theorem blocks_head:
  !ls. ls <> [] ==> HD (blocks ls) = (0, HD ls)
Proof
  rw[blocks_def] >>
  qabbrev_tac `f = \j. (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls)` >>
  `0 < LENGTH ls` by simp[LENGTH_NON_NIL] >>
  `[0 ..< LENGTH ls] <> []` by simp[listRangeLHI_NIL] >>
  `HD [0 ..< LENGTH ls] = 0` by simp[listRangeLHI_CONS] >>
  simp[MAP_HD, Abbr`f`]
QED

(* Theorem: MEM (u,v,w) (blocks ls) <=>
            ?j. j < LENGTH ls /\ (v,w) = EL j ls /\ u = if j = 0 then 0 else SND (EL (j - 1) ls) *)
(* Proof:
   Let \j. (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls),
       h = LENGTH ls.
       MEM (u,v,w) (block ls)
   <=> MEM (u,v,w) (MAP f [0 ..< h])           by blocks_def
   <=> ?j. (u,v,w) = f j /\ MEM j [0 ..< h]    by MEM_MAP
   <=> ?j. (u,v,w) = (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls) /\
           j < h                               by listRangeLHI_MEM
   <=> ?j. j < h /\ u = if j = 0 then 0 else SND (EL (j - 1) ls) /\ (v,w) = EL j ls
                                               by PAIR_EQ
*)
Theorem blocks_mem:
  !ls u v w. MEM (u,v,w) (blocks ls) <=>
             ?j. j < LENGTH ls /\ (v,w) = EL j ls /\
                 u = if j = 0 then 0 else SND (EL (j - 1) ls)
Proof
  rw[blocks_def, MEM_MAP] >>
  metis_tac[]
QED

(* Theorem: j + 1 < LENGTH (blocks ls) ==>
            FST (EL (j + 1) (blocks ls)) = SND (SND (EL j (blocks ls))) *)
(* Proof:
   Let f = \j. (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls).
   Note LENGTH (blocks ls) = LENGTH ls         by blocks_length
     EL (j + 1) (blocks ls)
   = EL (j + 1) (MAP f [0 ..< LENGTH ls])      by blocks_def
   = f (EL (j + 1)[0 ..< LENGTH ls])           by EL_MAP, j + 1 < LENGTH ls
   = f (j + 1)                                 by listRangeLHI_EL
   = (SND (EL j ls),EL (j + 1) ls)             by applying f, [1]

     EL j (blocks ls)
   = EL j (MAP f [0 ..< LENGTH ls])            by blocks_def
   = f (EL j [0 ..< LENGTH ls])                by EL_MAP, j < LENGTH ls
   = f j                                       by listRangeLHI_EL
   = (if j = 0 then 0 else SND (EL (j - 1) ls),EL j ls)    by applying f, [2]

     FST (EL (j + 1) (blocks ls))
   = SND (EL j ls)                             by above, [1]
   = SND (SND (EL j (blocks ls)))              by above, [2]
*)
Theorem blocks_next_element:
  !ls j. j + 1 < LENGTH (blocks ls) ==>
         FST (EL (j + 1) (blocks ls)) = SND (SND (EL j (blocks ls)))
Proof
  simp[blocks_def, EL_MAP, listRangeLHI_EL]
QED

(* Theorem: blocks (block_pairs (path n 0)) = [] *)
(* Proof:
     blocks (block_pairs (path n 0))
   = blocks []                     by block_pairs_path_0
   = []                            by blocks_nil
*)
Theorem blocks_path_0:
  !n. blocks (block_pairs (path n 0)) = []
Proof
  simp[block_pairs_path_0, blocks_nil]
QED

(* Theorem: 0 < k ==> ?t. blocks (block_pairs (path n k)) = (0, 0, skip_idx_ping (path n k) 1) :: t *)
(* Proof:
   Let ls = block_pairs (path n k),
       sp = skip_idx_ping (path n k) 1,
        f = \j. (if j = 0 then 0 else SND (EL (j - 1) ls), EL j ls).
   Note ?tt. ls = (0, sp) :: tt                by block_pairs_path_cons
     so LENGTH ls = SUC (LENGTH tt) > 0        by LENGTH
    and EL 0 ls = (0, sp)                      by EL, HD

     blocks (block_pairs (path n k))
   = MAP f [0 ..< LENGTH ls]                   by blocks_def
   = MAP f (0 :: [1 ..< LENGTH ls])            by listRangeLHI_CONS
   = f 0 :: MAP f [1 ..< LENGTH ls]            by MAP
   = (0, 0, sp):: MAP f [1 ..< LENGTH ls]      by applying f

   Pick MAP f [1 ..< LENGTH ls] as tail t.
*)
Theorem blocks_path_cons:
  !n k. 0 < k ==> ?t. blocks (block_pairs (path n k)) = (0, 0, skip_idx_ping (path n k) 1) :: t
Proof
  rpt strip_tac >>
  qabbrev_tac `ls = block_pairs (path n k)` >>
  qabbrev_tac `sp = skip_idx_ping (path n k) 1` >>
  qabbrev_tac `f = \j. (if j = 0 then 0 else SND (EL (j - 1) ls), EL j ls)` >>
  `?t. ls = (0, sp) :: t` by metis_tac[block_pairs_path_cons] >>
  `0 < LENGTH ls /\ EL 0 ls = (0, sp)` by fs[] >>
  `[0 ..< LENGTH ls] = 0::[1 ..< LENGTH ls]` by simp[listRangeLHI_CONS] >>
  qexists_tac `MAP f [1 ..< LENGTH ls]` >>
  simp[blocks_def, Abbr`f`]
QED

(* Theorem: 0 < k ==> blocks (block_pairs (path n k)) <> [] *)
(* Proof:
   Let sp = skip_idx_ping (path n k) 1.
   ?t. blocks (block_pairs (path n k))
     = (0, 0, sp) :: t                         by blocks_path_cons
   Thus blocks (block_pairs (path n k)) <> []  by NOT_NIL_CONS
*)
Theorem blocks_path_not_nil:
  !n k. 0 < k ==> blocks (block_pairs (path n k)) <> []
Proof
  metis_tac[blocks_path_cons, NOT_NIL_CONS]
QED

(* Theorem: 0 < k ==> HD (blocks (block_pairs (path n k))) = (0, 0, skip_idx_ping (path n k) 1) *)
(* Proof:
   Let sp = skip_idx_ping (path n k) 1.
   ?t. blocks (block_pairs (path n k))
     = (0, 0, sp) :: t                         by blocks_path_cons
   Thus HD (blocks (block_pairs (path n k)))
      = (0, 0, sp)                             by HD
*)
Theorem blocks_path_head:
  !n k. 0 < k ==> HD (blocks (block_pairs (path n k))) = (0, 0, skip_idx_ping (path n k) 1)
Proof
  metis_tac[blocks_path_cons, HD]
QED

(* Theorem: blocks (block_pairs (path n k)) = [] <=> k = 0 *)
(* Proof:
   If part: blocks (block_pairs (path n k)) = [] <=> k = 0
      By contradiction, suppose k <> 0.
      Then 0 < k                   by NOT_ZERO
        so blocks (block_pairs (path n k)) <> []
                                   by blocks_path_not_nil
      which is a contradiction.

   Only-if part: k = 0 ==> blocks (block_pairs (path n k))
         blocks (block_pairs (path n 0))
       = []                        by blocks_path_0
*)
Theorem blocks_path_eq_nil:
  !n k. blocks (block_pairs (path n k)) = [] <=> k = 0
Proof
  rw[EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `0 < k` by decide_tac >>
    fs[blocks_path_not_nil],
    simp[blocks_path_0]
  ]
QED

(* Theorem: let ls = path n k in flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==>
            u <= v /\ v < w /\ w <= k /\
            ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
            v = skip_idx_pung ls u /\ w = skip_idx_ping ls (v + 1) /\
            (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
            (!j. v < j /\ j < w ==> is_ping (EL j ls))  *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
       px = pong_indices (FRONT ls),
        h = LENGTH ps.

   Note MEM (u,v,w) (blocks ps)
    ==> ?j. j < h /\ (v,w) = EL j ps
    and u = if j = 0 then 0 else SND (EL (j - 1) ps)
                                               by blocks_mem, PAIR_EQ
   Note h = LENGTH px                          by block_pairs_length
     so h <= k                                 by pong_indices_length, path_front_length
     or 0 < h /\ 0 < k                         by j < h, h <= k

    Now ~is_ping (LAST ls)                     by path_last_not_ping
    and MEM (v,w) ps                           by MEM_EL, j < h
    ==> v < w /\ w <= k /\ w = skip_idx_ping ls (v + 1) /\
        is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
        !j. v < j /\ j < w ==> is_ping (EL j ls)
                                               by block_pairs_path_mem
   It remains to show:
        u <= v /\ ~is_ping (EL u ls) /\ v = skip_idx_pung ls u /\
        !j. u <= j /\ j < v ==> is_pung (EL j ls)

   If j = 0,
      Then u = 0, and 0 <= v is trivial.
       Now EL j ps = HD ps                     by EL
                   = (0, skip_idx_ping ls 1)   by block_pairs_path_head
      Thus v = 0                               by PAIR_EQ
      Note is_pong (EL 0 ls)                   by path_head_is_pong
        so ~is_ping (EL 0 ls)                  by pong_not_ping
       and ~is_pung (EL 0 ls)                  by pong_not_pung
       ==> skip_idx_pung ls 0 = 0 = v          by skip_idx_pung_none
       and the range !j. u <= j /\ j < v is empty.

   Otherwise j <> 0,
      Then u = SND (EL (j - 1) ps
      Thus ?a. EL (j - 1) ps = (a,u)           by PAIR, SND
       and MEM (a,u) ps                        by MEM_EL, j - 1 < h
        so ~is_ping (EL u ls)                  by block_pairs_path_mem
      Also u <= v /\ v = skip_idx_pung ls u /\
           !j. u <= j /\ j < v ==> is_pung (EL j ls)
                                               by block_pairs_path_next, 0 < j
*)
Theorem blocks_path_mem:
  !n k u v w. let ls = path n k in flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==>
              u <= v /\ v < w /\ w <= k /\
              ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
              v = skip_idx_pung ls u /\ w = skip_idx_ping ls (v + 1) /\
              (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
              (!j. v < j /\ j < w ==> is_ping (EL j ls))
Proof
  simp[blocks_mem] >>
  ntac 6 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `px = pong_indices (FRONT ls)` >>
  qabbrev_tac `h = LENGTH ps` >>
  `LENGTH px = h /\ h <= k` by metis_tac[block_pairs_length, pong_indices_length, path_front_length] >>
  `0 < h /\ 0 < k` by decide_tac >>
  `~is_ping (LAST ls)` by metis_tac[path_last_not_ping] >>
  `MEM (v,w) ps` by metis_tac[MEM_EL] >>
  qabbrev_tac `X = (u = if j = 0 then 0 else SND (EL (j - 1) ps))` >>
  assume_tac block_pairs_path_mem >>
  last_x_assum (qspecl_then [`n`, `k`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  qunabbrev_tac `X` >>
  Cases_on `j = 0` >| [
    `u = 0 /\ v = 0` by fs[block_pairs_path_head, Abbr`ps`, Abbr`ls`] >>
    `is_pong (EL 0 ls)` by metis_tac[path_head_is_pong] >>
    fs[pong_not_ping, pong_not_pung, skip_idx_pung_none],
    `?a. EL (j - 1) ps = (a,u)` by metis_tac[PAIR, SND] >>
    `0 < j /\ j - 1 < h` by decide_tac >>
    `MEM (a,u) ps` by metis_tac[MEM_EL] >>
    `~is_ping (EL u ls)` by metis_tac[block_pairs_path_mem] >>
    assume_tac block_pairs_path_next >>
    last_x_assum (qspecl_then [`n`, `k`, `j-1`, `a`, `u`, `v`, `w`] strip_assume_tac) >>
    rfs[]
  ]
QED

(* A very good result! *)

(* Idea: the last of blocks for a path has the third component at last index. *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
            SND (SND (LAST (blocks (block_pairs ls)))) = k *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
       px = pong_indices (FRONT ls),
        f = \j. (if j = 0 then 0 else SND (EL (j - 1) ps),EL j ps),
        h = LENGTH ps.
   The goal is to show: SND (LAST (blocks ps)) = (LAST px, k)

   Note ps <> []                   by block_pairs_path_eq_nil, 0 < k
     so h <> 0                     by LENGTH_EQ_0
    and [0 ..< h] <> []            by listRangeLHI_NIL, 0 < h
     LAST (blocks ps)
   = LAST (MAP f [0 ..< h])        by blocks_def
   = f (LAST [0 ..< h])            by LAST_MAP
   = f (h - 1)                     by listRangeLHI_LAST
   = (if h - 1 = 0 then 0 else SND (EL (h - 2) ps),EL (h - 1) ps)

   Thus SND (SND (LAST (blocks ps)))
      = SND (EL (h - 1) ps)        by SND
      = SND (LAST ps)              by LAST_EL
      = SND (LAST px, k)           by block_pairs_path_last
      = k                          by SND
*)
Theorem blocks_path_last:
  !n k. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ 0 < k ==>
        SND (SND (LAST (blocks (block_pairs ls)))) = k
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `px = pong_indices (FRONT ls)` >>
  qabbrev_tac `h = LENGTH ps` >>
  qabbrev_tac `f = \j. (if j = 0 then 0 else SND (EL (j - 1) ps),EL j ps)` >>
  `ps <> []` by simp[block_pairs_path_eq_nil, Abbr`ps`, Abbr`ls`] >>
  `h <> 0` by metis_tac[LENGTH_EQ_0] >>
  `[0 ..< h] <> []` by simp[listRangeLHI_NIL] >>
  `LAST (blocks ps) = LAST (MAP f [0 ..< h])` by fs[blocks_def, Abbr`ps`, Abbr`f`] >>
  `_ = f (LAST [0 ..< h])` by simp[LAST_MAP] >>
  `0 <= h - 1 /\ (h - 1) + 1 = h /\ PRE h = h - 1` by decide_tac >>
  `LAST [0 ..< h] = h - 1` by metis_tac[listRangeLHI_LAST] >>
  simp[Abbr`f`] >>
  metis_tac[LAST_EL, block_pairs_path_last, SND]
QED

(* Idea: the indices of a block can be reached by the first through steps of pung-pong-ping. *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==>
            EL v ls = FUNPOW pung (v - u) (EL u ls) /\
            EL (v + 1) ls = pong (EL v ls) /\
            EL w ls = (FUNPOW ping (w - (v + 1))) (EL (v + 1) ls) *)
(* Proof:
   Let ls = path n k.
   Note u <= v /\ v < w /\ w <= k /\
        ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
        v = skip_idx_pung ls u /\ w = skip_idx_ping ls (v + 1) /\
        (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
        (!j. v < j /\ j < w ==> is_ping (EL j ls))               by blocks_path_mem
   Thus !j. v + 1 <= j /\ j < w ==> is_ping (EL j ls)            by inequality

     EL v ls
   = EL (u + (v - u)) ls)                      by u <= v
   = FUNPOW pung (v - u) (EL u ls)             by path_element_pung_funpow_alt, v <= k

     EL (v + 1) ls
   = EL (SUC v) ls                             by ADD1
   = (zagier o flip) (EL v ls)                 by path_element_suc, v < k
   = pong (EL v ls)                            by zagier_flip_pong, is_pong (EL v ls)

     EL w ls
   = EL (v + 1 + (w - (v + 1))) ls                               by v < w ==> v + 1 <= w
   = (FUNPOW ping (w - (v + 1))) (EL (v + 1) ls)                 by path_element_ping_funpow_alt, w <= k
   = (FUNPOW ping (w - (v + 1))) (pong (EL v ls))                        by above
   = (FUNPOW ping (w - (v + 1))) (pong (FUNPOW pung (v - u) (EL u ls)))  by above
   = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls)  by composition
*)
Theorem blocks_path_indices_by_funpow:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==>
              EL v ls = FUNPOW pung (v - u) (EL u ls) /\
              EL (v + 1) ls = pong (EL v ls) /\
              EL w ls = (FUNPOW ping (w - (v + 1))) (EL (v + 1) ls)
Proof
  simp[] >>
  ntac 6 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  assume_tac blocks_path_mem >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  `EL v ls = FUNPOW pung (v - u) (EL u ls) /\ EL (v + 1) ls = pong (EL v ls) /\ EL w ls = (FUNPOW ping (w - (v + 1))) (EL (v + 1) ls)` suffices_by simp[] >>
  rpt strip_tac >| [
    `v = u + (v - u)` by decide_tac >>
    `EL v ls = EL (u + (v - u)) ls` by simp[] >>
    assume_tac path_element_pung_funpow_alt >>
    last_x_assum (qspecl_then [`n`, `k`, `u`, `v - u`] strip_assume_tac) >>
    rfs[],
    `v < k` by decide_tac >>
    `EL (v + 1) ls = (zagier o flip) (EL v ls)` by metis_tac[path_element_suc, ADD1] >>
    simp[zagier_flip_pong],
    `w = v + 1 + (w - (v + 1))` by decide_tac >>
    `EL w ls = EL (v + 1 + (w - (v + 1))) ls` by fs[] >>
    assume_tac path_element_ping_funpow_alt >>
    last_x_assum (qspecl_then [`n`, `k`, `v + 1`, `w - (v + 1)`] strip_assume_tac) >>
    rfs[]
  ]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==>
            EL w ls = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls) *)
(* Proof:
   Note EL v ls = FUNPOW pung (v - u) (EL u ls) /\
        EL (v + 1) ls = pong (EL v ls) /\
        EL w ls = (FUNPOW ping (w - (v + 1))) (EL (v + 1) ls)  by blocks_path_indices_by_funpow
   Thus EL w ls
      = (FUNPOW ping (w - (v + 1))) (EL (v + 1) ls)                         by above
      = (FUNPOW ping (w - (v + 1))) (pong (EL v ls))                        by above
      = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls)  by above
*)
Theorem blocks_path_third_by_funpow:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==>
              EL w ls = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) (EL u ls)
Proof
  rw_tac std_ss[] >>
  metis_tac[blocks_path_indices_by_funpow]
QED

(* Idea: the indices of a block can be reached by the first through hop. *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==>
            EL v ls = hop (v - u) (EL u ls) /\
            EL w ls = hop (w - u) (EL u ls) *)
(* Proof:
   Let ls = path n k,
        t = EL u ls.
   The goal is to show: EL v ls = hop (v - u) t /\ EL w ls = hop (w - u) t.

   Note u <= v /\ v < w /\ w <= k /\
        ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
        v = skip_idx_pung ls u /\ w = skip_idx_ping ls (v + 1) /\
        (!j. u <= j /\ j < v ==> is_pung (EL j ls))
                                               by blocks_path_mem, [*]

   Note n = windmill t                         by path_element_windmill_alt, u <= k, [1]

   Claim: EL v ls = FUNPOW pung (v - u) t
   Proof:    FUNPOW pung (v - u) t
           = FUNPOW pung (v - u) (EL u ls)     by notation
           = EL (u + (v - u)) ls               by path_element_pung_funpow_alt, [*]
           = EL v ls                           by u <= v

   Thus is_pong (FUNPOW pung (v - u) t)        by is_pong (EL v ls), [2]

   Claim: !h. h < v - u ==> is_pung (FUNPOW pung h t)
   Proof: Note h < v - u
           <=> 0 <= h /\ h < v - u
           <=> u <= h + u /\ h + u < v
             FUNPOW pung h t
           = FUNPOW pung h (EL u ls)           by notation
           = EL (u + h) ls                     by path_element_pung_funpow_alt
           Thus is_pung (EL (u + h) ls)        by [*], [3]

        EL v ls
      = FUNPOW pung (v - u) t                  by claim above
      = hop (v - u) t                          by pung_funpow_by_hop_alt

        EL w ls
      = (FUNPOW ping (w - (v + 1)) o pong o FUNPOW pung (v - u)) t
                                               by blocks_path_third_by_funpow
      = hop (w - (v + 1) + (v - u) + 1) t      by pung_to_ping_by_hop_alt, [1],[2],[3]
      = hop (w - (v + 1) + (v + 1) - u) t      by u <= v
      = hop (w - u) t                          by arithmetic
*)
Theorem blocks_path_indices_by_hop:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==>
              EL v ls = hop (v - u) (EL u ls) /\
              EL w ls = hop (w - u) (EL u ls)
Proof
  simp[] >>
  ntac 6 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `t = EL u ls` >>
  assume_tac blocks_path_mem >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  `EL v ls = hop (v - u) t /\ EL w ls = hop (w - u) t` suffices_by simp[] >>
  qabbrev_tac `X = (w = skip_idx_ping ls (v + 1))` >>
  qabbrev_tac `Y = (v = skip_idx_pung ls u)` >>
  `u < k` by decide_tac >>
  `n = windmill t` by fs[path_element_windmill_alt, Abbr`t`, Abbr`ls`] >>
  `EL v ls = FUNPOW pung (v - u) t` by
  (assume_tac path_element_pung_funpow_alt >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v - u`] strip_assume_tac) >>
  rfs[]) >>
  `!h. h < v - u ==> is_pung (FUNPOW pung h t)` by
    (rpt strip_tac >>
  assume_tac path_element_pung_funpow_alt >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `h`] strip_assume_tac) >>
  rfs[] >>
  `u <= h + u /\ h + u < v` by decide_tac >>
  metis_tac[]) >>
  strip_tac >-
  fs[pung_funpow_by_hop_alt] >>
  assume_tac blocks_path_third_by_funpow >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  assume_tac pung_to_ping_by_hop_alt >>
  last_x_assum (qspecl_then [`n`, `t`, `w - (v + 1)`, `v - u`] strip_assume_tac) >>
  fs[]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==> EL v ls = hop (v - u) (EL u ls) *)
(* Proof: by blocks_path_indices_by_hop. *)
Theorem blocks_path_second_by_hop:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==> EL v ls = hop (v - u) (EL u ls)
Proof
  metis_tac[blocks_path_indices_by_hop]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==> EL w ls = hop (w - u) (EL u ls) *)
(* Proof: by blocks_path_indices_by_hop. *)
Theorem blocks_path_third_by_hop:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==> EL w ls = hop (w - u) (EL u ls)
Proof
  metis_tac[blocks_path_indices_by_hop]
QED

(* ------------------------------------------------------------------------- *)
(* Hopping Algorithm                                                         *)
(* ------------------------------------------------------------------------- *)

(* To ensure hop with stay as a windmill, the conditions from hop_range are:
   x DIV (2 * z) < m /\ m <= (x + SQRT n) DIV (2 * z)
   which can be taken as (x + k) DIV (2 * z)
   with 0 < k <= SQRT n, seems integer 0 <= k <= SQRT n is good.
*)

(* Define the step function *)
Definition step_def:
   step k (x,y,z) = (x + k) DIV (2 * z)  (* depends on only x, z, not y *)
End
(* the parameter k is taken from the caller, the hopping. *)

(* Define the hopping function *)
Definition hopping_def:
   hopping k t = hop (step k t) t
End
(* the parameter k = SQRT n is taken from the caller, two_sq_hop. *)

(* Define two_squares algorithm with hopping *)
Definition two_sq_hop_def:
   two_sq_hop n =  WHILE ($~ o found) (hopping (SQRT n)) (1,n DIV 4,1)
End
(* Use (1,n DIV 4,1) rather than (1,1,n DIV 4), so path starts with a non-ping. *)

(* The map: mind (x,y,z) gives the mind of a windmill.

mind_def  |- !x y z. mind (x,y,z) = if x < y - z then x + 2 * z else if x < y then 2 * y - x else x

Zagier map preserves the mind;
mind_zagier_eqn |- !x y z. mind (zagier (x,y,z)) = mind (x,y,z)

Generally, flip map changes the mind:
EVAL ``mind (flip (x,y,z))``;
|- mind (flip (x,y,z)) = if x < z - y then x + 2 * y else if x < z then 2 * z - x else x

But  mind (7,1,3) = 7 = mind (7,3,1), so sometimes flip does not change the mind.
Thus, when  mind t = mind (flip t), the triple t is a node, and the pair (t, flip t) share the same mind.
These nodes in (mill n) are close to the hopping points of the improved algorithm.

Part of the reason is due to the map   phi_m = F_1^{m} H = H F_3^{m}, with m = (x + SQRT n) DIV (2 * y) when applied to t = (x,y,z).
hop_def         |- !m x y z. hop m (x,y,z) = (2 * m * y - x,z + m * x - m * m * y,y)
step_def        |- !k x y. step k (x,y) = (x + k) DIV (2 * y)
hopping_def     |- !k x y z. hopping k (x,y,z) = hop (step k (x,y)) (x,y,z)
two_sq_hop_def  |- !n. two_sq_hop n = WHILE ($~ o found) (hopping (SQRT n)) (1,1,n DIV 4)

> EVAL ``two_sq_hop 5``; = (1,1,1): thm
> EVAL ``two_sq_hop 13``; = (3,1,1): thm
> EVAL ``two_sq_hop 17``; = (1,2,2): thm
> EVAL ``two_sq_hop 29``; = (5,1,1): thm
> EVAL ``two_sq_hop 37``; = (1,3,3): thm
> EVAL ``two_sq_hop 41``; = (5,2,2): thm
> EVAL ``two_sq_hop 53``; = (7,1,1): thm
> EVAL ``two_sq_hop 61``; = (5,3,3): thm
> EVAL ``two_sq_hop 73``; = (3,4,4): thm
> EVAL ``two_sq_hop 89``; = (5,4,4): thm
> EVAL ``two_sq_hop 97``; = (9,2,2): thm
> EVAL ``two_sq_hop 1277``; = (11,17,17): thm (reduces from 23 steps to 5 hops)

> EVAL ``two_sq_hop 773``; = (17,11,11): thm
> EVAL ``two_sq_hop 797``; = (11,13,13): thm
> EVAL ``two_sq_hop 977``; = (31,2,2): thm
> EVAL ``two_sq_hop 997``; = (31,3,3): thm
> EVAL ``two_sq_hop 1801``; = (35,12,12): thm   (reduces from 132 steps to 33 hops)
> EVAL ``two_sq_hop 1933``; = (13,21,21): thm

*)

(* Theorem: let ls = path n k in
            flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
            ~is_ping (EL u ls) /\ let (x,y,z) = EL u ls in y <= x + z *)
(* Proof:
   Note ~is_ping (EL u ls)         by blocks_path_mem
     so ~(x < z - y)               by is_ping_def
     or z - y <= x
     or z <= x + y
*)
Theorem blocks_triple_first_not_ping:
  !n k u v w. let ls = path n k in
              flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
              ~is_ping (EL u ls) /\ let (x,y,z) = EL u ls in z <= x + y
Proof
  simp[] >>
  ntac 6 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  `~is_ping (EL u ls)` by metis_tac[blocks_path_mem] >>
  `?x y z. EL u ls = (x,y,z)` by metis_tac[triple_parts] >>
  fs[is_ping_def]
QED

(* Theorem: step 0 (x,y,z) = x DIV (2 * z) *)
(* Proof:
     step 0 (x,y,z)
   = (x + 0) DIV (2 * z)       by step_def
   = x DIV (2 * z)             by ADD_0
*)
Theorem step_0:
  !x y z. step 0 (x,y,z) = x DIV (2 * z)
Proof
  simp[step_def]
QED

(* Theorem: step (SQRT n) (x,y,z) = (x + SQRT n) DIV (2 * z) *)
(* Proof: by step_def. *)
Theorem step_sqrt:
  !n x y z. step (SQRT n) (x,y,z) = (x + SQRT n) DIV (2 * z)
Proof
  simp[step_def]
QED

(*

EVAL ``let n = 61 in (SQRT n, (1,n DIV 4,1))``; = (7,1,15,1)
EVAL ``hopping 7 (1,15,1)``; = (7,1,3)
EVAL ``hopping 7 (7,1,3)``; = (5,3,3)
EVAL ``FUNPOW (hopping 7) 1 (1,15,1)``; = (7,1,3)
EVAL ``FUNPOW (hopping 7) 2 (1,15,1)``; = (5,3,3)

A quick way to find k of (path n k), without proof:

Definition path_count_def:
   path_count u k = if (flip u = u) then k else path_count ((zagier o flip) u) (k+1)
End
-- need a Termination proof.

Definition path_count_def:
   path_count n = WHILE (\(j,t). flip t <> t) (\(j,t). (SUC j, (zagier o flip) t)) (0,(1,n DIV 4,1))
End

> EVAL ``path_count 61``; = (6,5,3,3)  which means k = 6, (5,3,3) a flip-fix is LAST.
> EVAL ``block_pairs (path 61 6)``; = [(0,4); (5,6)]
> EVAL ``blocks (block_pairs (path 61 6))``; = [(0,0,4); (4,5,6)]
> EVAL ``EL 4 (path 61 6)``; = (7,1,3)
> EVAL ``EL 6 (path 61 6)``; = (5,3,3)

*)

(* Theorem: let h = HALF (1 + SQRT n) in tik n /\ ~square n /\ h <= k ==>
            !j. 0 < j /\ j < h ==> is_ping (EL j (path n k)) *)
(* Proof:
   Let ls = path n k,
        h = HALF (1 + SQRT n).
   By complete induction on j.
   This is to show: !m. m < j ==> 0 < m ==> m < h ==> is_ping (EL m ls) ==>
                    !j. 0 < j /\ j < h ==> is_ping (EL j ls)

   Note the given condition, with j < h, is equivalent to:
        !m. 1 <= m /\ m < j ==> is_ping (EL m ls)      [1]
   Let jj = j - 1,
       q = n DIV 4.
   Then n = 4 * q + 1                          by DIVISION, tik n
    and 0 < k                                  by 0 < j < h <= k

        EL j ls
      = FUNPOW ping jj (EL 1 ls)               by path_element_ping_funpow_alt, [1]
      = FUNPOW ping jj (1,1,q)                 by path_element_1, 0 < k
      = (1 + 2 * jj, 1, q - jj - jj ** 2)      by ping_funpow

        is_ping (EL j ls)
    <=> 1 + 2 * jj + 1 < q - jj - jj ** 2      by is_ping_def
    <=> (jj ** 2 + 2 * jj + 1) + (jj + 1) < q  by arithmetic
    <=>          (jj + 1) ** 2 + (jj + 1) < q  by SUM_SQUARED
    <=>                 j ** 2 + j < q         by jj = j - 1
    <=> 4 * j ** 2 + 4 * j + 1 < 4 * q + 1     by multiplying 4, add 1
    <=>       (2 * j + 1) ** 2 < n             by SUM_SQUARED, n = 4 * q + 1

    Now j < h
    <=> j < (1 + SQRT n) DIV 2                 by notation
    <=> (j + 1) * 2 <= 1 + SQRT n              by X_LT_DIV
    <=>   2 * j + 1 <= SQRT n                  by arithmetic
    <=>  (2 * j + 1) ** 2 <= (SQRT n) ** 2     by EXP_EXP_LE_MONO

    But  (SQRT n) ** 2 < n                     by SQ_SQRT_LT_alt, ~square n
    Thus (2 * j + 1) ** 2 < n                  by inequality
      or is_ping (EL j ls).
*)
Theorem path_start_over_ping:
  !n k. let h = HALF (1 + SQRT n) in tik n /\ ~square n /\ h <= k ==>
        !j. 0 < j /\ j < h ==> is_ping (EL j (path n k))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `ls = path n k` >>
  completeInduct_on `j` >>
  rpt strip_tac >>
  assume_tac path_element_ping_funpow_alt >>
  last_x_assum (qspecl_then [`n`, `k`, `1`, `j - 1`] strip_assume_tac) >>
  rfs[] >>
  `is_ping (EL j ls)` suffices_by simp[] >>
  `_ = FUNPOW ping (j - 1) (1,1,n DIV 4)` by fs[path_element_1, Abbr`ls`] >>
  qabbrev_tac `jj = j - 1` >>
  qabbrev_tac `q = n DIV 4` >>
  qabbrev_tac `X = is_ping (EL j ls)` >>
  fs[ping_funpow] >>
  qunabbrev_tac `X` >>
  simp[is_ping_def] >>
  `(jj ** 2 + 2 * jj + 1) + (jj + 1) < q` suffices_by decide_tac >>
  `(2 * j + 1) ** 2 < n` by
  (`(j + 1) * 2 <= 1 + SQRT n` by fs[X_LT_DIV, Abbr`h`] >>
  `2 * j + 1 <= SQRT n` by decide_tac >>
  `(2 * j + 1) ** 2 <= (SQRT n) ** 2` by simp[] >>
  `(SQRT n) ** 2 < n` by simp[SQ_SQRT_LT_alt] >>
  decide_tac) >>
  `(2 * j + 1) ** 2 = 4 * j ** 2 + 4 * j + 1` by simp[SUM_SQUARED, EXP_BASE_MULT] >>
  `n = q * 4 + 1` by metis_tac[DIVISION, DECIDE``0 < 4``] >>
  `_ = 4 * q + 1` by decide_tac >>
  `j ** 2 + j < q` by decide_tac >>
  `j ** 2 + j = (jj + 1) ** 2 + jj + 1` by simp[Abbr`jj`] >>
  fs[SUM_SQUARED]
QED

(* Theorem: let h = HALF (1 + SQRT n) in tik n /\ ~square n /\ h <= k ==>
            ~is_ping (EL h (path n k)) *)
(* Proof:
   Let ls = path n k,
        h = HALF (1 + SQRT n).
   By contradiction, suppose is_ping (EL h ls).
   Note n <> 0                     by square_0
     so SQRT n <> 0                by SQRT_EQ_0
    ==> 0 < h                      by HALF_EQ_0, NOT_ZERO
    Now !j. 0 < j /\ j < h ==> is_ping (EL j ls)
                                   by path_start_over_ping
     or !j. 1 <= j /\ j < h ==> is_ping (EL j ls)      [1]
   Let hh = h - 1,
       q = n DIV 4.
   Then n = 4 * q + 1                          by DIVISION, tik n
    and 0 < k                                  by 0 < h <= k

        EL h ls
      = FUNPOW ping hh (EL 1 ls)               by path_element_ping_funpow_alt, [1]
      = FUNPOW ping hh (1,1,q)                 by path_element_1, 0 < k
      = (1 + 2 * hh, 1, q - hh - hh ** 2)      by ping_funpow

   Thus 1 + 2 * hh < q - hh - hh ** 2 - 1      by is_ping_def, is_ping (EL h ls)
    <=> hh ** 2 + 2 * hh + 1 + (hh + 1) < q    by arithmetic
    <=> (hh + 1) ** 2 + (hh + 1) < q           by SUM_SQUARED
    <=>               h ** 2 + h < q           by hh = h - 1
    ==>   4 * h ** 2 + 4 * h + 1 < 4 * q + 1   by arithmetic
    <=>         (2 * h + 1) ** 2 < 4 * q + 1   by SUM_SQUARED
    <=>         (2 * h + 1) ** 2 < n           by n = 4 * q + 1
    ==>          2 * h + 1 <= SQRT n           by SQRT_LT, SQRT_OF_SQ
    <=>        2 * (h + 1) <= 1 + SQRT n       by arithmetic
    <=>             h < (1 + SQRT n) DIV 2     by X_LT_DIV
    <=>             h < h                      by notation
   which is false, a contradiction.
*)
Theorem path_start_beyond_ping:
  !n k. let h = HALF (1 + SQRT n) in tik n /\ ~square n /\ h <= k ==>
        ~is_ping (EL h (path n k))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `ls = path n k` >>
  assume_tac path_start_over_ping >>
  last_x_assum (qspecl_then [`n`, `k`] strip_assume_tac) >>
  rfs[] >>
  spose_not_then strip_assume_tac >>
  `0 < n /\ 0 < SQRT n` by metis_tac[square_0, SQRT_EQ_0, NOT_ZERO] >>
  `0 < h` by metis_tac[HALF_EQ_0, ADD_EQ_0, ADD_EQ_1, NOT_ZERO] >>
  assume_tac path_element_ping_funpow_alt >>
  last_x_assum (qspecl_then [`n`, `k`, `1`, `h - 1`] strip_assume_tac) >>
  rfs[] >>
  `_ = FUNPOW ping (h - 1) (1,1,n DIV 4)` by fs[path_element_1, Abbr`ls`] >>
  qabbrev_tac `hh = h - 1` >>
  qabbrev_tac `q = n DIV 4` >>
  fs[ping_funpow] >>
  fs[is_ping_def] >>
  `hh ** 2 + 2 * hh + 1 + (hh + 1) < q` by decide_tac >>
  `hh ** 2 + 2 * hh + 1 + (hh + 1) = (hh + 1) ** 2 + (hh + 1)` by simp[SUM_SQUARED, EXP_BASE_MULT] >>
  `_ = h ** 2 + h` by simp[Abbr`hh`] >>
  `n = q * 4 + 1` by metis_tac[DIVISION, DECIDE``0 < 4``] >>
  `4 * h ** 2 + 4 * h + 1 < n` by decide_tac >>
  `4 * h ** 2 + 4 * h + 1 = (2 * h + 1) ** 2` by simp[SUM_SQUARED, EXP_BASE_MULT] >>
  `2 * h + 1 <= SQRT n` by metis_tac[SQRT_LT, SQRT_OF_SQ] >>
  `(h + 1) * 2 <= 1 + SQRT n` by decide_tac >>
  `h < HALF (1 + SQRT n)` by simp[X_LT_DIV] >>
  fs[Abbr`h`]
QED

(* A good exercise! *)


(* Theorem: let h = HALF (1 + SQRT n) in tik n /\ ~square n /\ h <= k ==>
            skip_idx_ping (path n k) 1 = step (SQRT n) (1,n DIV 4,1) *)
(* Proof:
   Let ls = path n k,
       h = HALF (1 + SQRT n),
       u = (1,n DIV 4,1).
   The goal is to show: skip_idx_ping ls 1 = step (SQRT n) u.

   Note n <> 0                     by square_0, ~square n
     so 0 < SQRT n                 by SQRT_EQ_0, 0 < n
    and 0 < h                      by HALF_EQ_0, NOT_ZERO
     so 0 < k                      by h <= k
   Also !j. 0 < j /\ j < h ==> is_ping (EL j ls)
                                   by path_start_over_ping
    and ~is_ping (EL h ls)         by path_start_beyond_ping
   Thus skip_idx_ping ls 1
      = h                          by skip_idx_ping_thm
      = step (SQRT n) u            by step_sqrt
*)
Theorem path_start_skip_idx_ping:
  !n k. let h = HALF (1 + SQRT n) in tik n /\ ~square n /\ h <= k ==>
        skip_idx_ping (path n k) 1 = step (SQRT n) (1,n DIV 4,1)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `u = (1,n DIV 4,1)` >>
  `0 < n /\ 0 < SQRT n` by metis_tac[square_0, SQRT_EQ_0, NOT_ZERO] >>
  `0 < h` by metis_tac[HALF_EQ_0, ADD_EQ_0, ADD_EQ_1, NOT_ZERO] >>
  `~is_ping (EL h ls)` by metis_tac[path_start_beyond_ping] >>
  assume_tac path_start_over_ping >>
  last_x_assum (qspecl_then [`n`, `k`] strip_assume_tac) >>
  rfs[] >>
  simp[skip_idx_ping_thm, step_sqrt, Abbr`u`]
QED

(* First good result. *)

(* ------------------------------------------------------------------------- *)
(* Hopping by step 0.                                                        *)
(* ------------------------------------------------------------------------- *)

(*
Note:
step_0  |- !x y z. step 0 (x,y,z) = x DIV (2 * z)

hopping 0 (x,y,z) = hop (step 0 (x,y,z)) (x,y,z)

This corresponds to the hopping over pungs, reaching pong.
*)

(* Theorem: 0 < z /\ m < step 0 (x,y,z) ==> is_pung (hop m (x,y,z)) *)
(* Proof: by step_0, hop_over_pung. *)
Theorem hop_step_0_over_pung:
  !m x y z. 0 < z /\ m < step 0 (x,y,z) ==> is_pung (hop m (x,y,z))
Proof
  simp[step_0, hop_over_pung]
QED

(* Theorem: 0 < z /\ m = step 0 (x,y,z) ==> ~is_pung (hop m (x,y,z)) *)
(* Proof: by step_0, hop_beyond_pung. *)
Theorem hop_step_0_beyond_pung:
  !m x y z. 0 < z /\ m = step 0 (x,y,z) ==> ~is_pung (hop m (x,y,z))
Proof
  simp[step_0, hop_beyond_pung]
QED

(* Therefore,
If is_ping (x,y,z), step 0 (x,y,z) = 0, as there is no pung to skip.
If is_pong (x,y,z), step 0 (x,y,z) = 0, no pung to skip.
If is_pung (x,y,z), step 0 (x,y,z) <> 0, can be 1, 2, 3, .... at least one to skip.
*)

(* Theorem: 0 < z ==> (step 0 (x,y,z) = 0 <=> ~is_pung (x,y,z)) *)
(* Proof:
       is_pung (x,y,z)
   <=> 2 * z - x = 0           by is_pung_alt
   <=> 2 * z <= x              by SUB_EQ_0
   <=>     1 <= x DIV (2 * z)  by X_LE_DIV
   <=>     0 < x DIV (2 * z)   by LESS_EQ
   <=>     0 < step 0 (x,y,z)  by step_0
   <=>    step 0 (x,y,z) <> 0  by NOT_ZERO_LT_ZERO
   Thus step 0 (x,y,z) = 0 <=> ~is_pung (x,y,z)    by contrapositive
*)
Theorem step_0_eq_0:
  !x y z. 0 < z ==> (step 0 (x,y,z) = 0 <=> ~is_pung (x,y,z))
Proof
  rpt strip_tac >>
  `0 < 2 * z` by decide_tac >>
  `is_pung (x,y,z) <=> 2 * z <= x` by simp[is_pung_alt] >>
  `_ = (1 <= x DIV (2 * z))` by fs[X_LE_DIV] >>
  `_ = (1 <= step 0 (x,y,z))` by simp[step_0] >>
  `_ = (step 0 (x,y,z) <> 0)` by decide_tac >>
  metis_tac[]
QED

(* Theorem: 0 < z /\ is_ping (x,y,z) ==> step 0 (x,y,z) = 0 *)
(* Proof: by ping_not_pung, step_0_eq_0. *)
Theorem step_0_of_ping:
  !x y z. 0 < z /\ is_ping (x,y,z) ==> step 0 (x,y,z) = 0
Proof
  simp[ping_not_pung, step_0_eq_0]
QED

(* Theorem: 0 < z /\ is_pong (x,y,z) ==> step 0 (x,y,z) = 0 *)
(* Proof: by pong_not_pung, step_0_eq_0. *)
Theorem step_0_of_pong:
  !x y z. 0 < z /\ is_pong (x,y,z) ==> step 0 (x,y,z) = 0
Proof
  simp[pong_not_pung, step_0_eq_0]
QED

(* Theorem: 0 < z /\ is_pung (x,y,z) ==> 0 < step 0 (x,y,z) *)
(* Proof: by step_0_eq_0, NOT_ZERO. *)
Theorem step_0_of_pung:
  !x y z. 0 < z /\ is_pung (x,y,z) ==> 0 < step 0 (x,y,z)
Proof
  metis_tac[step_0_eq_0, NOT_ZERO]
QED

(*
For this range, m <= step 0 (x,y,z) = x DIV (2 * z),
which is ~(x DIV (2 * z) < m),
hop m (x,y,z) = (x - 2 * m * z,y + m * x - m * m * z,z) = (xx,yy,zz)
that is,
xx decreases with m linearly, starting from x.
yy varies with m quadratically, first increases, then decreases, starting from y.
zz is always kept at original z, which is the focus.

Therefore, the discriminant (2 * zz - xx) for subsequent ping/pong/pung
only depends on xx, and increases linearly with m:
2 * zz - xx = 2 * z - (x - 2 * m * z) = 2 * (m + 1) * z - x
Initially, 2 * zz - xx = 2 * z - x is negative for pung,
but eventually this switches to positive, when pung becomes pong at m = step 0 (x,y,z).
*)

(* Theorem: let xx = x - 2 * m * z in xx - 2 * z = x - 2 * (m + 1) * z *)
(* Proof:
   Let xx = x - 2 * m * z.
     xx - 2 * z
   = x - 2 * m * z - 2 * z               by notation
   = x - (2 * m * z + 2 * z)             by SUB_RIGHT_SUB
   = x - 2 * z * (m + 1)                 by LEFT_ADD_DISTRIB
*)
Theorem hop_identity_1:
  !m x z. let xx = x - 2 * m * z in xx - 2 * z = x - 2 * (m + 1) * z
Proof
  simp[]
QED

(* Theorem: let xx = x - 2 * m * z in xx - 2 * z = x - 2 * (m + 1) * z *)
(* Proof:
   Let xx = x - 2 * m * z.
     2 * z - xx
   = 2 * z - (x - 2 * m * z)             by notation
   = 2 * z + 2 * m * z - x               by SUB_SUB, 0 < xx
   = 2 * z * (m + 1) - x                 by LEFT_ADD_DISTRIB
*)
Theorem hop_identity_2:
  !m x z. let xx = x - 2 * m * z in 0 < xx ==> 2 * z - xx = 2 * (m + 1) * z - x
Proof
  simp[]
QED

(* Theorem: let xx = x - 2 * m * z; yy = y + m * x - m ** 2 * z in 0 < xx /\ 0 < yy ==>
            xx + yy - z = y + (m + 1) * x - (m + 1) ** 2 * z *)
(* Proof:
   Let xx = x - 2 * m * z,
       yy = y + m * x - m ** 2 * z.
     xx + yy - z
   = x - 2 * m * z + (y + m * x - m ** 2 * z) - z      by notation
   = (y + m * x - m ** 2 * z) + x - 2 * m * z - z      by SUB_LEFT_ADD, 0 < xx
   = y + (m * x + x) - (m ** 2 * z + 2 * m * z + z)    by SUB_RIGHT_SUB, 0 < yy
   = y + (m + 1) * x - (m ** 2 + 2 * m + 1) * z        by RIGHT_ADD_DISTRIB
   = y + m * x - (m + 1) ** 2 * z                      by SUM_SQUARED
*)
Theorem hop_identity_3:
  !m x y z. let xx = x - 2 * m * z; yy = y + m * x - m ** 2 * z in 0 < xx /\ 0 < yy ==>
            xx + yy - z = y + (m + 1) * x - (m + 1) ** 2 * z
Proof
  rw_tac std_ss[] >>
  `xx + yy - z = y + m * x - m ** 2 * z + x - 2 * (m * z) - z` by simp[Abbr`xx`, Abbr`yy`] >>
  `_ = y + m * x + x - m ** 2 * z - 2 * (m * z) - z` by rfs[Abbr`yy`] >>
  `_ = y + (m + 1) * x - (m ** 2 + 2 * m + 1) * z` by decide_tac >>
  simp[SUM_SQUARED]
QED

(* Theorem: let xx = 2 * m * z - x; yy = y + m * x - m ** 2 * z in 0 < xx /\ 0 < yy ==>
            yy - z - xx = y + (m + 1) * x - (m + 1) ** 2 * z *)
(* Proof:
   Let xx = 2 * m * z - x,
       yy = y + m * x - m ** 2 * z.
     yy - z - xx
   = (y + m * x - m ** 2 * z) - z - (2 * m * z - x)    by notation
   = (y + m * x - m ** 2 * z) + x - 2 * m * z - z      by SUB_LEFT_ADD, 0 < xx
   = y + (m * x + x) - (m ** 2 * z + 2 * m * z + z)    by SUB_RIGHT_SUB, 0 < yy
   = y + (m + 1) * x - (m ** 2 + 2 * m + 1) * z        by RIGHT_ADD_DISTRIB
   = y + m * x - (m + 1) ** 2 * z                      by SUM_SQUARED
*)
Theorem hop_identity_4:
  !m x y z. let xx = 2 * m * z - x; yy = y + m * x - m ** 2 * z in 0 < xx /\ 0 < yy ==>
            yy - z - xx = y + (m + 1) * x - (m + 1) ** 2 * z
Proof
  rw_tac std_ss[] >>
  `yy - z - xx = y + m * x - m ** 2 * z + x - 2 * (m * z) - z` by simp[Abbr`xx`, Abbr`yy`] >>
  `_ = y + m * x + x - m ** 2 * z - 2 * (m * z) - z` by rfs[Abbr`yy`] >>
  `_ = y + (m + 1) * x - (m ** 2 + 2 * m + 1) * z` by decide_tac >>
  simp[SUM_SQUARED]
QED

(* Idea: before hopping to a pong, hopping is just (FUNPOW pung j). *)

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\
            m <= step 0 (x,y,z) ==> hop m (x,y,z) = FUNPOW pung m (x,y,z) *)
(* Proof:
   Note m <= x DIV (2 * z)                     by step_0

   By complete induction on m.
   This is to show: !j. j < m /\ j <= x DIV (2 * z) ==> hop j (x,y,z) = FUNPOW pung j (x,y,z)
                ==> m <= x DIV (2 * z) ==> hop m (x,y,z) = FUNPOW pung m (x,y,z)

   If m = 0,
        FUNPOW pung 0 (x,y,z)
      = (x,y,z)                                by FUNPOW_0
      = hop 0 (x,y,z)                          by hop_0

   Otherwise, 0 < m.
      Let n = windmill (x,y,z).
      Then 0 < z                               by windmill_with_arms, ~square n
      Let j = m - 1,
          xx = x - 2 * j * z,
          yy = y + j * x - j ** 2 * z.
      Then m = SUC j, j < m <= x DIV (2 * z)   by ADD1

      Note hop j (x,y,z) = (xx,yy,z)           by hop_alt, j < x DIV (2 * z)
       and m <= (x + SQRT n) DIV (2 * z)       by DIV_LE_MONOTONE
       ==> n = windmill (xx,yy,z)              by hop_windmill
        so 0 < xx                              by windmill_with_mind, tik n
       and 0 < yy                              by windmill_with_arms, ~square n

           FUNPOW pung m (x,y,z)
         = pung (FUNPOW pung j (x,y,z))        by FUNPOW_SUC
         = pung (hop j (x,y,z))                by induction hypothesis
         = pung (xx,yy,z)                      by above
         = (xx - 2 * z,xx + yy - z,z)          by pung_def
         = (x - 2 * (j + 1) * z,                         by hop_identity_1
            y + (j + 1) * x - (j + 1) ** 2 * z, z)       by hop_identity_3
         = (x - 2 * m * z, y + m * x - m ** 2 * z, z)    by m = j + 1
         = hop m (x,y,z)                       by hop_alt, m <= x DIV (2 * z)
*)
Theorem hop_step_0_before_pong:
  !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\
              m <= step 0 (x,y,z) ==> hop m (x,y,z) = FUNPOW pung m (x,y,z)
Proof
  rw[step_0] >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  completeInduct_on `m` >>
  strip_tac >>
  `m = 0 \/ 0 < m` by decide_tac >-
  simp[hop_0] >>
  last_x_assum (qspecl_then [`m-1`] strip_assume_tac) >>
  rfs[] >>
  `SUC (m - 1) = m /\ m - 1 < m /\ m = m - 1 + 1` by decide_tac >>
  qabbrev_tac `j = m - 1` >>
  qabbrev_tac `xx = x - 2 * j * z` >>
  qabbrev_tac `yy = y + j * x - j ** 2 * z` >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  `hop j (x,y,z) = (xx,yy,z)` by fs[hop_alt, Abbr`xx`, Abbr`yy`] >>
  `0 < 2 * z` by decide_tac >>
  `x DIV (2 * z) <= (x + SQRT n) DIV (2 * z)` by simp[DIV_LE_MONOTONE] >>
  `j <= (x + SQRT n) DIV (2 * z)` by decide_tac >>
  `n = windmill (xx,yy,z)` by metis_tac[hop_windmill] >>
  `0 < xx` by metis_tac[windmill_with_mind, ONE_NOT_0] >>
  `0 < yy` by metis_tac[windmill_with_arms] >>
  `FUNPOW pung m (x,y,z) = pung (FUNPOW pung j (x,y,z))` by simp[GSYM FUNPOW_SUC] >>
  `_ = pung (hop j (x,y,z))` by fs[] >>
  `_ = pung (xx,yy,z)` by fs[] >>
  `_ = (xx - 2 * z,xx + yy - z,z)` by simp[pung_def] >>
  `_ = (x - 2 * m * z, y + m * x - m ** 2 * z, z)` by metis_tac[hop_identity_1, hop_identity_3] >>
  fs[hop_alt]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ m <= step 0 t ==> hop m t = FUNPOW pung m t *)
(* Proof: by hop_step_0_before_pong, triple_parts. *)
Theorem hop_step_0_before_pong_alt:
  !n m t. tik n /\ ~square n /\ n = windmill t /\ m <= step 0 t ==> hop m t = FUNPOW pung m t
Proof
  metis_tac[hop_step_0_before_pong, triple_parts]
QED

(*
For n = 61,
u = (1,15,1), is_pong u.
step 0 u = 1 DIV (2 * 1) = 0 = m.
hop m u = hop 0 (1,15,1) = (1,15,1)
hop (m + 1) = hop 1 (1,15,1) = (1,1,15)
pong (1,15,1) = (1,1,15)

*)

(* Idea: at pong, the hop definition changes from one form to another.*)

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) ==>
            hop (m + 1) (x,y,z) = pong (hop m (x,y,z)) *)
(* Proof:
   Note m = x DIV (2 * z)                      by step_0
   Let n = windmill (x,y,z).
   Then 0 < z                                  by windmill_with_arms, ~square n
    and m <= (x + SQRT n) DIV (2 * z)          by DIV_LE_MONOTONE

   Let xx = x - 2 * m * z,
       yy = y + m * x - m ** 2 * z.
   Then hop m (x,y,z) = (xx,yy,z)              by hop_alt, x DIV (2 * z) = m

   Note n = windmill (xx,yy,z)                 by hop_windmill, m <= step (SQRT n) (x,y,z)
     so 0 < xx                                 by windmill_with_mind, tik n
    and 0 < yy                                 by windmill_with_arms, ~square n

        pong (hop m (x,y,z))
      = pong (xx,yy,z)                         by above
      = (2 * z - xx,z,xx + yy - z)             by pong_def
      = (2 * (m + 1) * z - x, z,               by hop_identity_2, 0 < xx
         y + (m + 1) * x - (m + 1) ** 2 * z)   by hop_identity_3, 0 < yy
      = hop (m + 1) (x,y,z)                    by hop_alt, x DIV (2 * z) < m + 1
*)
Theorem hop_step_0_at_pong:
  !n m x y z. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) ==>
              hop (m + 1) (x,y,z) = pong (hop m (x,y,z))
Proof
  rpt strip_tac >>
  `m = x DIV (2 * z)` by simp[step_0] >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  `0 < 2 * z` by decide_tac >>
  `m <= (x + SQRT n) DIV (2 * z)` by rfs[DIV_LE_MONOTONE] >>
  qabbrev_tac `xx = x - 2 * m * z` >>
  qabbrev_tac `yy = y + m * x - m ** 2 * z` >>
  `hop m (x,y,z) = (xx,yy,z)` by fs[hop_alt, Abbr`xx`, Abbr`yy`] >>
  `n = windmill (xx,yy,z)` by metis_tac[hop_windmill] >>
  `0 < xx` by metis_tac[windmill_with_mind, ONE_NOT_0] >>
  `0 < yy` by metis_tac[windmill_with_arms] >>
  `pong (hop m (x,y,z)) = pong (xx,yy,z)` by simp[] >>
  `_ = (2 * z - xx,z,xx + yy - z)` by simp[pong_def] >>
  `_ = (2 * (m + 1) * z - x,z,xx + yy - z)` by rfs[hop_identity_2, Abbr`xx`] >>
  `_ = (2 * (m + 1) * z - x,z,y + (m + 1) * x - (m + 1) ** 2 * z)` by metis_tac[hop_identity_3] >>
  `_ = (2 * (m + 1) * z - x,z,y + (m + 1) * x - (m + 1) * (m + 1) * z)` by metis_tac[EXP_2] >>
  fs[hop_alt]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ m = step 0 t ==> hop (m + 1) t = pong (hop m t) *)
(* Proof: by hop_step_0_at_pong, triple_parts. *)
Theorem hop_step_0_at_pong_alt:
  !n m t. tik n /\ ~square n /\ n = windmill t /\ m = step 0 t ==> hop (m + 1) t = pong (hop m t)
Proof
  metis_tac[hop_step_0_at_pong, triple_parts]
QED

(* Idea: after hopping to a pong, further hopping is just (FUNPOW ping j). *)

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\ m + j < step (SQRT n) (x,y,z) ==>
            hop (m + 1 + j) (x,y,z) = FUNPOW ping j (hop (m + 1) (x,y,z)) *)
(* Proof:
   Let n = windmill (x,y,z),
       k = m + 1.
   Note m = x DIV (2 * z)                      by step_0
    and m + j < (x + SQRT n) DIV (2 * z)       by step_sqrt [1]
    and 0 < z                                  by windmill_with_arms, ~square n
   The goal is to show: hop (k + j) (x,y,z) = FUNPOW ping j (hop k (x,y,z))

   By complete induction on j.
   This is to show: !h. h < j /\ h + m < (x + SQRT n) DIV (2 * z) ==>
                        hop (k + h) (x,y,z) = FUNPOW ping h (hop k (x,y,z))
                 ==> j + m < (x + SQRT n) DIV (2 * z) ==>
                     hop (k + j) (x,y,z) = FUNPOW ping j (hop k (x,y,z))

   If j = 0,
        FUNPOW ping 0 (hop k (x,y,z))
      = hop k (x,y,z)                          by FUNPOW_0
      = hop (k + 0) (x,y,z)                    by ADD_0

   Otherwise, 0 < j.
      Let h = j - 1, then j = SUC h.
       so h < j /\ h + m < (x + SQRT n) DIV (2 * z)

      Let xx = 2 * (m + j) * z - x,
          yy = y + (m + j) * x - (m + j) ** 2 * z.
      Then hop (m + j) (x,y,z) = (xx,z,yy)     by hop_alt, m < m + j
       and m + j <= (x + SQRT n) DIV (2 * z)   by [1]
       ==> n = windmill (xx,z,yy)              by hop_windmill
      Thus 0 < xx                              by windmill_with_mind, tik n
       and 0 < yy                              by windmill_with_arms, ~square n

        FUNPOW ping j (hop k (x,y,z))
      = FUNPOW ping (SUC h) (hop k (x,y,z))    by j = SUC h
      = ping (FUNPOW ping h (hop k (x,y,z)))   by FUNPOW_SUC
      = ping (hop (k + h) (x,y,z))             by induction hypothesis
      = ping (hop (m + j) (x,y,z))             by k + h = m + 1 + j - 1 = m + j
      = ping (xx, z, yy)                       by above
      = (xx + 2 * z,z,yy - z - xx)             by ping_def
      = (2 * z * (m + j) - x + 2 * z, z, yy - z - xx)
      = (2 * z * (m + j + 1) - x, z,           by LEFT_ADD_DISTRIB
         y + (m + j + 1) * x - (m + j + 1) ** 2 * z)
                                               by hop_identity_4, 0 < xx, 0 < yy
      = hop (m + j + 1) (x,y,z)                by hop_alt, m + j + 1 <= (x + SQRT n) DIV (2 * z)
      = hop (k + j) (x,y,z)                    by k = m + 1
*)
Theorem hop_step_0_beyond_pong:
  !n x y z m j. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\ m + j < step (SQRT n) (x,y,z) ==>
                hop (m + 1 + j) (x,y,z) = FUNPOW ping j (hop (m + 1) (x,y,z))
Proof
  rw[step_0, step_sqrt] >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  qabbrev_tac `m = x DIV (2 * z)` >>
  completeInduct_on `j` >>
  strip_tac >>
  `j = 0 \/ 0 < j` by decide_tac >-
  simp[] >>
  last_x_assum (qspecl_then [`j-1`] strip_assume_tac) >>
  rfs[] >>
  `SUC (j - 1) = j` by decide_tac >>
  qabbrev_tac `h = j - 1` >>
  qabbrev_tac `k = m + 1` >>
  qabbrev_tac `xx = 2 * (j + m) * z - x` >>
  qabbrev_tac `yy = y + (j + m) * x - (j + m) ** 2 * z` >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  `hop (j + m) (x,y,z) = (xx,z,yy)` by fs[hop_alt, Abbr`m`, Abbr`xx`, Abbr`yy`] >>
  `n = windmill (xx,z,yy)` by metis_tac[hop_windmill, LESS_IMP_LESS_OR_EQ] >>
  `0 < xx` by metis_tac[windmill_with_mind, ONE_NOT_0] >>
  `0 < yy` by metis_tac[windmill_with_arms] >>
  `FUNPOW ping j (hop k (x,y,z)) = ping (FUNPOW ping h (hop k (x,y,z)))` by metis_tac[FUNPOW_SUC] >>
  `_ = ping (xx,z,yy)` by fs[] >>
  `_ = (xx + 2 * z,z,yy - z - xx)` by simp[ping_def] >>
  `_ = (2 * z * (j + m) + 2 * z - x, z, yy - z - xx)` by simp[Abbr`xx`] >>
  `_ = (2 * (j + k) * z - x, z, yy - z - xx)` by simp[Abbr`k`] >>
  `_ = (2 * (j + k) * z - x, z, y + (j + m + 1) * x - (j + m + 1) ** 2 * z)` by metis_tac[hop_identity_4] >>
  fs[hop_alt, Abbr`k`]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ m = step 0 t /\ m + j < step (SQRT n) t ==>
            hop (m + 1 + j) t = FUNPOW ping j (hop (m + 1) t) *)
(* Proof: by hop_step_0_beyond_pong, triple_parts. *)
Theorem hop_step_0_beyond_pong_alt:
  !n m j t. tik n /\ ~square n /\ n = windmill t /\ m = step 0 t /\ m + j < step (SQRT n) t ==>
            hop (m + 1 + j) t = FUNPOW ping j (hop (m + 1) t)
Proof
  metis_tac[hop_step_0_beyond_pong, triple_parts]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\ m + j < step (SQRT n) (x,y,z) ==>
            hop (m + 1 + j) (x,y,z) = (FUNPOW ping j o pong o FUNPOW pung m) (x,y,z) *)
(* Proof:
     hop (m + 1 + j) (x,y,z)
   = FUNPOW ping j (hop (m + 1) (x,y,z))       by hop_step_0_beyond_pong
   = FUNPOW ping j (pong (hop m (x,y,z)))      by hop_step_0_at_pong
   = FUNPOW ping j (pong (FUNPOW pung m (x,y,z)))      by hop_step_0_before_pong
   = (FUNPOW ping j o pong o FUNPOW pung m) (x,y,z)    by o_THM
*)
Theorem hop_step_0_around_pong:
  !n x y z m j. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\ m + j < step (SQRT n) (x,y,z) ==>
                hop (m + 1 + j) (x,y,z) = (FUNPOW ping j o pong o FUNPOW pung m) (x,y,z)
Proof
  simp[hop_step_0_beyond_pong, hop_step_0_at_pong, hop_step_0_before_pong]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ m = step 0 t /\ m + j < step (SQRT n) t ==>
            hop (m + 1 + j) t = (FUNPOW ping j o pong o FUNPOW pung m) t *)
(* Proof: by hop_step_0_around_pong, triple_parts. *)
Theorem hop_step_0_around_pong_alt:
  !n m j t. tik n /\ ~square n /\ n = windmill t /\ m = step 0 t /\ m + j < step (SQRT n) t ==>
            hop (m + 1 + j) t = (FUNPOW ping j o pong o FUNPOW pung m) t
Proof
  metis_tac[hop_step_0_around_pong, triple_parts]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ u <= k /\ m <= step 0 (EL u ls) ==>
            hop m (EL u ls) = FUNPOW pung m (EL u ls) *)
(* Proof:
   Let ls = path n k,
       t = EL u ls.
   Note n = windmill t                         by path_element_windmill
   Thus hop m t = FUNPOW pung m t              by hop_step_0_before_pong_alt
*)
Theorem hop_step_0_path_element_before_pong:
  !n k u m. let ls = path n k in tik n /\ ~square n /\ u <= k /\ m <= step 0 (EL u ls) ==>
            hop m (EL u ls) = FUNPOW pung m (EL u ls)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `t = EL u ls` >>
  `n = windmill t` by fs[path_element_windmill_alt, Abbr`ls`, Abbr`t`] >>
  fs[hop_step_0_before_pong_alt]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ u <= k /\ m = step 0 (EL u ls) ==>
            hop (m + 1) (EL u ls) = pong (hop m (EL u ls)) *)
(* Proof:
   Let ls = path n k,
       t = EL u ls.
   Note n = windmill t                         by path_element_windmill
   Thus hop (m + 1) t = pong (hop m t)         by hop_step_0_at_pong_alt
*)
Theorem hop_step_0_path_element_at_pong:
  !n k u m. let ls = path n k in tik n /\ ~square n /\ u <= k /\ m = step 0 (EL u ls) ==>
            hop (m + 1) (EL u ls) = pong (hop m (EL u ls))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `t = EL u ls` >>
  `n = windmill t` by fs[path_element_windmill_alt, Abbr`ls`, Abbr`t`] >>
  fs[hop_step_0_at_pong_alt]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ u <= k /\
            m = step 0 (EL u ls) /\ m + j < step (SQRT n) (EL u ls) ==>
            hop (m + 1 + j) (EL u ls) = FUNPOW ping j (hop (m + 1) (EL u ls)) *)
(* Proof:
   Let ls = path n k,
       t = EL u ls.
   Note n = windmill t                         by path_element_windmill
   Thus hop (m + 1 + j) t = FUNPOW ping j (hop (m + 1) t)
                                               by hop_step_0_beyond_pong_alt
*)
Theorem hop_step_0_path_element_beyond_pong:
  !n k u m j. let ls = path n k in tik n /\ ~square n /\ u <= k /\
              m = step 0 (EL u ls) /\ m + j < step (SQRT n) (EL u ls) ==>
              hop (m + 1 + j) (EL u ls) = FUNPOW ping j (hop (m + 1) (EL u ls))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `t = EL u ls` >>
  `n = windmill t` by fs[path_element_windmill_alt, Abbr`ls`, Abbr`t`] >>
  fs[hop_step_0_beyond_pong_alt]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ u <= k /\
            m = step 0 (EL u ls) /\ m + j < step (SQRT n) (EL u ls) ==>
            hop (m + 1 + j) (EL u ls) = (FUNPOW ping j o pong o FUNPOW pung m) (EL u ls) *)
(* Proof:
   Let ls = path n k,
       t = EL u ls.
   Note n = windmill t                         by path_element_windmill
   Thus hop (m + 1 + j) t = (FUNPOW ping j o pong o FUNPOW pung m) t
                                               by hop_step_0_around_pong_alt
*)
Theorem hop_step_0_path_element_around_pong:
  !n k u m j. let ls = path n k in tik n /\ ~square n /\ u <= k /\
              m = step 0 (EL u ls) /\ m + j < step (SQRT n) (EL u ls) ==>
              hop (m + 1 + j) (EL u ls) = (FUNPOW ping j o pong o FUNPOW pung m) (EL u ls)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `t = EL u ls` >>
  `n = windmill t` by fs[path_element_windmill_alt, Abbr`ls`, Abbr`t`] >>
  fs[hop_step_0_around_pong_alt]
QED

(* Theorem: ~square n /\ n = windmill (x,y,z) /\ m < step 0 (x,y,z) ==>
            is_pung (hop m (x,y,z)) *)
(* Proof:
   Note 0 < z                      by windmill_with_arms, ~square n
    and m < x DIV (2 * z)          by step_0
     so is_pung (hop m (x,y,z))    by hop_over_pung
*)
Theorem hop_step_0_before_pong_is_pung:
  !n x y z m. ~square n /\ n = windmill (x,y,z) /\ m < step 0 (x,y,z) ==>
              is_pung (hop m (x,y,z))
Proof
  rw[step_0] >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  simp[hop_over_pung]
QED

(* Theorem: ~square n /\ n = windmill t /\ m < step 0 t ==> is_pung (hop m t) *)
(* Proof: by hop_step_0_before_pong_is_pung, triple_parts. *)
Theorem hop_step_0_before_pong_is_pung_alt:
  !n m t. ~square n /\ n = windmill t /\ m < step 0 t ==> is_pung (hop m t)
Proof
  metis_tac[hop_step_0_before_pong_is_pung, triple_parts]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\
            ~is_ping (x,y,z) ==> is_pong (hop m (x,y,z)) *)
(* Proof:
   Note 0 < z                      by windmill_with_arms, ~square n
    and m = x DIV (2 * z)          by step_0
   Let t = hop m (x,y,z).
   Note ~is_pung t                 by hop_beyond_pung

   If m = 0,
      Then t = hop 0 (x,y,z)
             = (x,y,z)             by hop_0
      Thus ~is_pung (x,y,z)        by above, t = (x,y,z)
       and ~is_ping (x,y,z)        by given
       ==> is_pong (x,y,z)         by triple_cases

   Otherwise, 0 < m.
      Let u = hop (m-1) (x,y,z).
      Then is_pung u               by hop_over_pung
       ==> ~is_ping (pung u)
           hop m (x,y,z)
         = FUNPOW pung m (x,y,z)   by hop_step_0_before_pong
         = pung (FUNPOW pung (m-1) (x,y,z))    by FUNPOW_SUC
         = pung (hop (m - 1) (x,y,z))          by hop_step_0_before_pong
         = pung u

      Thus ~is_ping t              by pung_next_not_ping_alt
      With ~is_pung t              by given
       ==> is_pong t               by triple_cases_alt
*)
Theorem hop_step_0_at_pong_is_pong:
  !n x y z m. tik n /\ ~square n /\ n = windmill (x,y,z) /\ m = step 0 (x,y,z) /\
            ~is_ping (x,y,z) ==> is_pong (hop m (x,y,z))
Proof
  rpt strip_tac >>
  `m = x DIV (2 * z)` by simp[step_0] >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  qabbrev_tac `t = hop m (x,y,z)` >>
  `~is_pung t` by fs[hop_beyond_pung, Abbr`t`] >>
  `m = 0 \/ 0 < m` by decide_tac >| [
    `t = (x,y,z)` by simp[hop_0, Abbr`t`] >>
    metis_tac[triple_cases],
    qabbrev_tac `u = hop (m - 1) (x,y,z)` >>
    `t = FUNPOW pung m (x,y,z)` by fs[hop_step_0_before_pong, Abbr`t`] >>
    `u = FUNPOW pung (m-1) (x,y,z)` by fs[hop_step_0_before_pong, Abbr`u`] >>
    `m = SUC (m - 1)` by decide_tac >>
    `t = pung u` by metis_tac[FUNPOW_SUC] >>
    `is_pung u` by fs[hop_over_pung, Abbr`u`] >>
    `~is_ping t` by rfs[pung_next_not_ping_alt] >>
    metis_tac[triple_cases_alt]
  ]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ m = step 0 t /\ ~is_ping t ==> is_pong (hop m t) *)
(* Proof: by hop_step_0_at_pong_is_pong, triple_parts. *)
Theorem hop_step_0_at_pong_is_pong_alt:
  !n m t. tik n /\ ~square n /\ n = windmill t /\ m = step 0 t /\ ~is_ping t ==> is_pong (hop m t)
Proof
  metis_tac[hop_step_0_at_pong_is_pong, triple_parts]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            u < k /\ m <= step 0 (EL u ls) ==> u + m < k /\ hop m (EL u ls) = EL (u + m) ls *)
(* Proof:
   Let ls = path n k,
       t = EL u ls.
   The goal is to show: m + u < k /\ hop m t = EL (m + u) ls

   If ~is_pung t,
      Then step 0 t = 0                        by step_0_eq_0
        so m = 0                               by m <= 0
      Thus u + 0 < k                           by u < k
       and hop 0 t = t = EL (u + 0) ls         by hop_0_alt

   If is_pung t,
      By complete induction on m.
      This is to show: !j. j < m /\ u + j <= k /\ j <= step 0 t ==> j + u < k /\ hop j t = EL (u + j) ls
                   ==> m <= step 0 t ==> m + u < k /\ hop m t = EL (u + m) ls
      If m = 0,
         Then u + 0 < k                        by u < k
          and hop 0 t = EL (u + 0) ls          by hop_0_alt
      Otherwise, 0 < m.
         Put j = m - 1.
         Then j < m /\ u + j <= k /\ j <= step 0 t,
          ==> hop j t = EL (u + j) ls          by induction hypothesis
          and is_pung (hop j t)                by hop_step_0_over_pung, j < m

         Claim: m + u < k
         Proof: By contradiction, suppose k <= m + u.
                Then m + u = k                 by m + u - 1 < k, or m + u < k + 1
                  so u + j = u + (m - 1) = k - 1
                 But ~is_pung (EL (k - 1) ls)  by path_last_flip_fix_not_by_pung
                 This contradicts is_pung (hop j t).

         For the remaining goal: hop m t = EL (u + m) ls.
         Note m <= step 0 (x,y,z)
           so m <= x DIV (2 * z)               by step_0
          and j < x DIV (2 * z)                by j < m

          Let xx = x - 2 * j * z,
              yy = y + j * x - j ** 2 * z
          Then hop j t = (xx,yy,z)             by hop_alt, j < x DIV (2 * z)
           and n = windmill (EL (u + j) ls)    by path_element_windmill_alt
                 = hop j t = (xx,yy,z)         by above
            so 0 < xx                          by windmill_with_mind, tik n
           and 0 < yy                          by windmill_with_arms, ~square n

             EL (u + m) ls
           = (zagier o flip) EL (u + j) ls     by path_element_suc
           = pung (hop j t)                    by zagier_flip_pung
           = pung (xx,yy,z)                    by above
           = (xx - 2 * z,xx + yy - z,z)        by pung_def
           = (x - 2 * z * (j + 1),             by hop_identity_1
              y + (j + 1) * x - (j + 1) ** 2 * z, z)  by hop_identity_3, 0 < xx, 0 < yy
           = (x - 2 * m * z,y + m * x - m * m * z,z)  by m = j + 1
           = hop m (x,y,z)                     by hop_alt, m <= x DIV (2 * z)
*)
Theorem hop_step_0_path_element:
  !n k u m. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            u < k /\ m <= step 0 (EL u ls) ==> u + m < k /\ hop m (EL u ls) = EL (u + m) ls
Proof
  simp[] >>
  ntac 5 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  qabbrev_tac `t = EL u ls` >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  `n = windmill (x,y,z)` by metis_tac[path_element_windmill, LESS_IMP_LESS_OR_EQ] >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  `~is_pung t \/ is_pung t` by decide_tac >| [
    `step 0 t = 0` by rfs[step_0_eq_0] >>
    `m = 0` by decide_tac >>
    simp[hop_0_alt],
    completeInduct_on `m` >>
    strip_tac >>
    `m = 0 \/ 0 < m` by decide_tac >-
    simp[hop_0_alt, Abbr`t`] >>
    last_x_assum (qspecl_then [`m-1`] strip_assume_tac) >>
    rfs[] >>
    `m - 1 < m /\ (m - 1) + 1 = m /\ SUC (m + u - 1) = m + u /\ m + u - 1 < k` by decide_tac >>
    `is_pung (EL (m + u - 1) ls)` by metis_tac[hop_step_0_over_pung, LESS_LESS_EQ_TRANS] >>
    `EL (m + u) ls = (zagier o flip) (EL (m + u - 1) ls)` by metis_tac[path_element_suc] >>
    `_ = pung (hop (m - 1) (x,y,z))` by fs[zagier_flip_pung] >>
    strip_tac >| [
      spose_not_then strip_assume_tac >>
      `m + u = k` by decide_tac >>
      metis_tac[path_last_flip_fix_not_by_pung],
      qabbrev_tac `j = m - 1` >>
      `0 < 2 * z` by decide_tac >>
      `m <= x DIV (2 * z)` by fs[step_0] >>
      qabbrev_tac `xx = x - 2 * j * z` >>
      qabbrev_tac `yy = y + j * x - j ** 2 * z` >>
      `hop j (x,y,z) = (xx,yy,z)` by fs[hop_alt, Abbr`xx`, Abbr`yy`] >>
      `n = windmill (EL (m + u - 1) ls)` by fs[path_element_windmill_alt, Abbr`ls`] >>
      `0 < xx` by metis_tac[windmill_with_mind, ONE_NOT_0] >>
      `0 < yy` by metis_tac[windmill_with_arms] >>
      `pung (hop j (x,y,z)) = (xx - 2 * z,xx + yy - z,z)` by rfs[pung_def] >>
      simp[hop_alt] >>
      rpt strip_tac >| [
        `xx - 2 * z = x - 2 * (j + 1) * z` by metis_tac[hop_identity_1] >>
        simp[Abbr`j`],
        `xx + yy - z = y + (j + 1) * x - (j + 1) ** 2 * z` by metis_tac[hop_identity_3] >>
        simp[Abbr`j`]
      ]
    ]
  ]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            u < k /\ j < step 0 (EL u ls) ==>
            hop (SUC j) (EL u ls) = (zagier o flip) (hop j (EL u ls)) *)
(* Proof:
   Let t = EL u ls.
   Note SUC j <= step 0 t                     by j < step 0 t
   Thus u + j < k /\ hop j t = EL (j + u) ls  by hop_step_0_path_element
   Also u + SUC j < k /\ hop (SUC j) t = EL (u + SUC j) ls
                                              by hop_step_0_path_element
      hop (SUC j) (EL u ls)
    = EL (SUC (u + j)) ls                     by SUC (u + j) = u + SUC j
    = (zagier o flip) (EL (u + j) ls)         by path_element_suc, u + j < k
    = (zagier o flip) hop j t                 by above
*)
Theorem hop_step_0_suc:
  !n k u j. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            u < k /\ j < step 0 (EL u ls) ==>
            hop (SUC j) (EL u ls) = (zagier o flip) (hop j (EL u ls))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `t = EL u ls` >>
  assume_tac hop_step_0_path_element >>
  last_x_assum (qspecl_then [`n`,`k`,`u`,`j`] strip_assume_tac) >>
  rfs[] >>
  assume_tac hop_step_0_path_element >>
  last_x_assum (qspecl_then [`n`,`k`,`u`,`SUC j`] strip_assume_tac) >>
  rfs[] >>
  `u + SUC j = SUC (j + u)` by decide_tac >>
  simp[path_element_suc, Abbr`ls`, Abbr`t`]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ u < k ==>
            hopping 0 (EL u ls) = EL (skip_idx_pung ls u) ls *)
(* Proof:
   Let ls = path n k,
        t = EL u ls,
   Note ?x y z. t = (x,y,z)                    by triple_parts
    and n = windmill (x,y,z)                   by path_element_windmill, tik n
     so 0 < z                                  by windmill_with_arms, ~square n

   Let m = step 0 t.
   Then !j. j < m ==> is_pung (hop j (x,y,z))  by hop_step_0_over_pung, 0 < z [1]
    and ~is_pung (hop m (x,y,z))               by hop_step_0_beyond_pung, 0 < z [2]

    Now !j. j <= m ==> u + j < k /\ hop j (x,y,z) = EL (u + j) ls)
                                               by hop_step_0_path_element [3]

   Thus ~is_pung (EL (u + m) ls)               by putting j = m, [2]
    and !j. j < m ==> is_pung (EL (u + j) ls)  by keeping j, [1]
     or !i. u + 0 <= i /\ i < u + m ==> is_pung (EL i ls)
                                               by i = u + j
   Thus skip_idx_pung ls u = m + u             by skip_idx_pung_thm

        hopping 0 t
      = hop (step 0 t) t                       by hopping_def
      = hop m (x,y,z)                          by m = step 0 t
      = EL (u + m) ls                          by putting j = m, [3]
      = EL (skip_idx_pung ls u) ls             by above
*)
Theorem hopping_path_step_0:
  !n k u. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ u < k ==>
          hopping 0 (EL u ls) = EL (skip_idx_pung ls u) ls
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `t = EL u ls` >>
  `?x y z. t = (x,y,z)` by metis_tac[triple_parts] >>
  `n = windmill (x,y,z)` by metis_tac[path_element_windmill, LESS_IMP_LESS_OR_EQ] >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  qabbrev_tac `m = x DIV (2 * z)` >>
  `step 0 (x,y,z) = m` by fs[step_0, Abbr`m`] >>
  `!j. j < m ==> is_pung (hop j (x,y,z))` by simp[hop_over_pung, Abbr`m`] >>
  `~is_pung (hop m (x,y,z))` by simp[hop_beyond_pung, Abbr`m`] >>
  assume_tac hop_step_0_path_element >>
  last_x_assum (qspecl_then [`n`,`k`,`u`] strip_assume_tac) >>
  rfs[] >>
  `~is_pung (EL (u + m) ls) /\ !j. j < m ==> is_pung (EL (u + j) ls)` by fs[] >>
  `!j. u <= j /\ j < u + m ==> is_pung (EL j ls)` by
  (rpt strip_tac >>
  `0 <= j - u /\ j - u < m /\ j = u + (j - u)` by decide_tac >>
  qabbrev_tac `i = j - u` >>
  fs[]) >>
  assume_tac skip_idx_pung_thm >>
  last_x_assum (qspecl_then [`ls`,`u`,`u + m`] strip_assume_tac) >>
  rfs[] >>
  `hopping 0 (x,y,z) = hop m (x,y,z)` suffices_by fs[] >>
  simp[hopping_def]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==> EL v ls = hopping 0 (EL u ls) *)
(* Proof:
   Note u <= v /\ v < w /\ w <= k /\
        v = skip_idx_pung ls u     by blocks_path_mem
     EL v ls
   = EL (skip_idx_pung ls u) ls    by above
   = hopping 0 (EL u ls)           by hopping_path_step_0
*)
Theorem blocks_path_second_by_hopping:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==> EL v ls = hopping 0 (EL u ls)
Proof
  rw_tac std_ss[] >>
  `u <= v /\ v < w /\ w <= k /\ v = skip_idx_pung ls u` by metis_tac[blocks_path_mem] >>
  `u < k` by decide_tac >>
  metis_tac[hopping_path_step_0]
QED

(* ------------------------------------------------------------------------- *)
(* Hopping by step (SQRT n).                                                 *)
(* ------------------------------------------------------------------------- *)

(*
Note:
step_sqrt  |- !n x y z. step (SQRT n) (x,y,z) = (x + SQRT n) DIV (2 * z)

hopping (SQRT n) (x,y,z) = hop (step (SQRT n) (x,y,z)) (x,y,z)

This corresponds to the hopping after pong, over pings.

If is_ping (x,y,z), step (SQRT n) (x,y,z) = 0, there is no pungs to skip, no pong to stay, cannot see pings beyond.
If is_pong (x,y,z), step (SQRT n) (x,y,z) <> 0, itself is pong, hope to find some pings to skip.
If is_pung (x,y,z), step (SQRT n) (x,y,z) <> 9, there are pungs to skip, stay at a pong, maybe pings to skip.
*)

(* Theorem: 0 < z ==> (step (SQRT (windmill (x,y,z))) (x,y,z) = 0 <=> is_ping (x,y,z)) *)
(* Proof:
   Let n = windmill (x,y,z).
       is_ping (x,y,z)
   <=> SQRT n < 2 * z - x                      by is_ping_alt
   <=> x + SQRT n < 2 * z                      by SUB_LEFT_LESS
   <=> (x + SQRT n) DIV (2 * z) < 1            by DIV_LT_X
   <=>    step (SQRT n) (x,y,z) < 1            by step_sqrt
   <=>    step (SQRT n) (x,y,z) = 0            by inequality
*)
Theorem step_sqrt_eq_0:
  !x y z. 0 < z ==> (step (SQRT (windmill (x,y,z))) (x,y,z) = 0 <=> is_ping (x,y,z))
Proof
  rpt strip_tac >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  `0 < 2 * z` by decide_tac >>
  `is_ping (x,y,z) <=> x + SQRT n < 2 * z` by simp[is_ping_alt, Abbr`n`] >>
  `_ = ((x + SQRT n) DIV (2 * z) < 1)` by fs[DIV_LT_X] >>
  `_ = (step (SQRT n) (x,y,z) < 1)` by simp[step_sqrt] >>
  `_ = (step (SQRT n) (x,y,z) = 0)` by decide_tac >>
  simp[]
QED

(* Theorem: 0 < z /\ is_ping (x,y,z) ==> step (SQRT (windmill (x,y,z))) (x,y,z) = 0 *)
(* Proof: by step_sqrt_eq_0. *)
Theorem step_sqrt_of_ping:
  !x y z. 0 < z /\ is_ping (x,y,z) ==> step (SQRT (windmill (x,y,z))) (x,y,z) = 0
Proof
  simp[step_sqrt_eq_0]
QED

(* Theorem: 0 < z /\ is_pong (x,y,z) ==> 0 < step (SQRT (windmill (x,y,z))) (x,y,z) *)
(* Proof:
   Let n = windmill (x,y,z).
       is_pong (x,y,z)
   ==> 2 * z - x <= SQRT n                     by is_pong_alt
   <=> 2 * z <= x + SQRT n                     by SUB_RIGHT_LESS_EQ
   <=> 1 <= (x + SQRT n) DIV (2 * z)           by X_LE_DIV
   <=> 1 <= step (SQRT n) (x,y,z)              by step_sqrt
   <=> 0 < step (SQRT n) (x,y,z)               by inequality
   or simply by step_sqrt_eq_0, pong_not_ping.
*)
Theorem step_sqrt_of_pong:
  !x y z. 0 < z /\ is_pong (x,y,z) ==> 0 < step (SQRT (windmill (x,y,z))) (x,y,z)
Proof
  metis_tac[pong_not_ping, step_sqrt_eq_0, NOT_ZERO]
QED

(* Theorem: 0 < z /\ is_pung (x,y,z) ==> 0 < step (SQRT (windmill (x,y,z))) (x,y,z) *)
(* Proof: by pung_not_ping, step_sqrt_eq_0. *)
Theorem step_sqrt_of_pung:
  !x y z. 0 < z /\ is_pung (x,y,z) ==> 0 < step (SQRT (windmill (x,y,z))) (x,y,z)
Proof
  metis_tac[pung_not_ping, step_sqrt_eq_0, NOT_ZERO]
QED

(*
That's why hopping m (x,y,z) always starts with ~is_ping (x,y,z),

                   step 0 (x,y,z)            step (SQRT n) (x,y,z)
is_ping (x,y,z)       = 0                       = 0
is_pong (x,y,z)       = 0                       > 0
is_pung (x,y,z)       > 0                       > 0
*)

(* Idea: for n = windmill (x,y,z), step 0 (x,y,z) <= step (SQRT n) (x,y,z). *)

(* Theorem: 0 < z ==> step 0 (x,y,z) <= step (SQRT (windmill (x,y,z))) (x,y,z) *)
(* Proof:
   Let n = windmill (x,y,z).
   Note x <= x + SQRT n                        by inequality
   Thus step 0 (x,y,z)
      = x DIV (2 * z)                          by step_0
     <= (x + SQRT n) DIV (2 * z)               by DIV_LE_MONOTONE
      = step (SQRT n) (x,y,z)                  by step_sqrt
*)
Theorem step_0_le_step_sqrt:
  !x y z. 0 < z ==> step 0 (x,y,z) <= step (SQRT (windmill (x,y,z))) (x,y,z)
Proof
  rpt strip_tac >>
  `0 < 2 * z` by decide_tac >>
  simp[step_0, step_sqrt, DIV_LE_MONOTONE]
QED

(*
Q: For a pung (x,y,z), looks like x DIV (2 * z) < (x + SQRT n) DIV (2 * z) always, why?

For example, when n = 97, SQRT n = 9. The two will be equal if (2 * z) > SQRT n = 9; e.g. when z = 5.
However, for a windmill, 97 = n = x ** 2 + 2 * 4 * y * z.
The smallest x = 1, so 4 * y * z = 96, or y * z = 16. Max z is only 4, not 5!

Let h = SQRT n.

          x DIV (2 * z) < (x + h) DIV (2 * z)
      <=> (1 + x DIV (2 * z)) * 2 * z <= x + h       by X_LT_DIV
      <=> 2 * z + 2 * z * (x DIV (2 * z)) <= x + h   by RIGHT_ADD_DISTRIB
      <=> 2 * z + x <= x + h + x MOD (2 * z)         by DIVISION
      <=>     2 * z <= h + x MOD (2 * z)

          x DIV (2 * z) < (x + h) DIV (2 * z)
      <=> x < 2 * z * (x + h) DIV (2 * z)            by DIV_LT_X
      <=> x + (x + h) MOD (2 * z) < (x + h)          by DIVISION
      <=>     (x + h) MOD (2 * z) < h
      This is true if    2 * z <= h   since (x + h) MOD (2 * z) < 2 * z   by MOD_LESS
      or  2 * z <= SQRT n             which is what I suspected.

      For windmill, n = x ** 2 + 4 * y * z
      Thus            4 * y * z <= n         in fact < n as x is ODD
      If only         4 * z * z <= n         this is true when y <= z
      Then                2 * z <= SQRT n    by SQRT_LE

      Now is_pung_alt |- !x y z. is_pung (x,y,z) <=> 2 * z - x = 0
      That is, 2 * z <= x                    by SUB_EQ_0
      which is not  2 * z <= SQRT n,
      But x ** 2 <= n, so x <= SQRT n        by windmill_x_upper!

This gives the following proof.
*)

(* Theorem: ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
            step 0 (x,y,z) < step (SQRT n) (x,y,z) *)
(* Proof:
   Let n = windmill (x,y,z),
       m = step 0 (x,y,z),
       k = step (SQRT n) (x,y,z).
   The goal is to show: m < k.
   Note 0 < z                                       by windmill_with_arms, ~square n

   If is_pong (x,y,z),
      Then m = 0                                    by step_0_of_pong
       but 0 < k                                    by step_sqrt_of_pong
      Hence m < k.

   Otherwise, is_pung (x,y,z)                       by triple_cases
      Then 2 * z - x = 0                            by is_pung_alt
        so 2 * z <= x                               by SUB_EQ_0
      also x <= SQRT n                              by windmill_x_upper
      Thus 2 * z <= SQRT n                          by LESS_EQ_TRANS, [1]

      Let h = SQRT n.
      Note m = x DIV (2 * z)                        by step_0
       and k = (x + h) DIV (2 * z)                  by step_sqrt
       Now (x + h) MOD (2 * z) < (2 * z)            by MOD_LESS
        so (x + h) MOD (2 * z) < h                  by [1]
       <=> ((x + h) DIV (2 * z)) * (2 * z) + (x + h) MOD (2 * z)
         < ((x + h) DIV (2 * z)) * (2 * z) + h      by LESS_MONO_ADD_EQ
       <=> x + h < k * (2 * z) + h                  by DIVISION
       <=>     x < k * (2 * z)                      by LESS_MONO_ADD_EQ
       <=> x DIV (2 * z) < k                        by DIV_LT_X
       <=>             m < k                        by notation
*)
Theorem step_0_lt_step_sqrt:
  !n x y z. ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
            step 0 (x,y,z) < step (SQRT n) (x,y,z)
Proof
  rpt strip_tac >>
  qabbrev_tac `m = step 0 (x,y,z)` >>
  qabbrev_tac `k = step (SQRT n) (x,y,z)` >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  `is_pong (x,y,z) \/ is_pung (x,y,z)` by metis_tac[triple_cases] >| [
    `m = 0` by fs[step_0_of_pong, Abbr`m`] >>
    `0 < k` by metis_tac[step_sqrt_of_pong] >>
    decide_tac,
    qabbrev_tac `h = SQRT n` >>
    `2 * z <= x` by fs[is_pung_alt] >>
    `x <= h` by metis_tac[windmill_x_upper] >>
    `k = (x + h) DIV (2 * z)` by simp[step_def, Abbr`k`] >>
    `0 < 2 * z` by decide_tac >>
    `x + h = k * (2 * z) + (x + h) MOD (2 * z)` by metis_tac[DIVISION] >>
    `(x + h) MOD (2 * z) < 2 * z` by simp[] >>
    `x < k * (2 * z)` by decide_tac >>
    `x DIV (2 * z) < k` by simp[DIV_LT_X] >>
    fs[step_0, Abbr`m`]
  ]
QED

(* Theorem: ~square n /\ n = windmill t /\ ~is_ping t ==> step 0 t < step (SQRT n) t *)
(* Proof: by step_0_lt_step_sqrt, triple_parts. *)
Theorem step_0_lt_step_sqrt_alt:
  !n t. ~square n /\ n = windmill t /\ ~is_ping t ==> step 0 t < step (SQRT n) t
Proof
  metis_tac[step_0_lt_step_sqrt, triple_parts]
QED

(*
For this range, x DIV (2 * z) = step 0 (x,y,z) < m <= step (SQRT n) (x,y,z) = (x + SQRT n) DIV (2 * z),
which is DIV (2 * z) < m,
hop m (x,y,z) = (2 * m * z - x,z,y + m * x - m * m * z) = (xx,yy,zz)
that is,
xx increases with m linearly, starting from x - 2 * (step 0 (x,y,z)) * z, the "x" of the pong from ~is_ping.
yy is always kept at original z, which is the focus.
zz varies with m quadratically, first increases, then decreases, starting from "y" of the pong from ~is_ping.

Therefore, the discriminant (2 * zz - xx) for subsequent ping/pong/pung
has two variations:
2 * zz - xx = 2 * (y + m * x - m * m * z) - (2 * m * z - x)
= x + 2 * y + 2 * m * x - 2 * m * m * z - 2 * m * z
= (x + 2 * y) + 2 * m * (x - z) - 2 * m * m * z

This is hard to handle, from the initial ~is_ping (x,y,z).
This is handled by:
hop_alt |- 0 < z ==> hop m (x,y,z) = if x DIV (2 * z) < m then (normal hop) else (invert hop)
This allows to decide which version simply by checking m: if (step 0 (x,y,z) < m) ....

Most likely to proceed by:
(1) show that hopping 0 (x,y,z) will start from ~is_ping (x,y,z) and reaches a pong.
(2) show that hopping (SQRT n) (x,y,z) is equivalent to starting from pong and do skip_idx_ping.
*)

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\
            m = step 0 (x,y,z) /\ m + 1 + j < step (SQRT n) (x,y,z) ==> is_ping (hop (m + 1 + j) (x,y,z)) *)
(* Proof:
   Let s = step (SQRT n) (x,y,z),
       t = hop (m + 1) (x,y,z).
       ping (hop (m + 1 + j) (x,y,z))
     = ping (FUNPOW ping j t)                  by hop_step_0_beyond_pong, m + j < s
     = FUNPOW ping (SUC j) t                   by FUNPOW_SUC
     = hop (m + 1 + SUC j) (x,y,z)             by m + SUC j = m + 1 + j < s

   Let (xx,yy,zz) = hop (m + 1 + SUC j) t.
   Note s = (x + SQRT n) DIV (2 * z)           by step_sqrt
   ==> n = windmill (xx,yy,zz)                 by hop_windmill, ~square n
     so 0 < zz                                 by windmill_with_arms, ~square n

   Thus is_ping hop (m + 1 + j) (x,y,z)        by is_ping_by_ping
*)
Theorem hop_step_0_beyond_pong_is_ping:
  !n x y z m j. tik n /\ ~square n /\ n = windmill (x,y,z) /\
                m = step 0 (x,y,z) /\ m + 1 + j < step (SQRT n) (x,y,z) ==> is_ping (hop (m + 1 + j) (x,y,z))
Proof
  rpt strip_tac >>
  qabbrev_tac `s = step (SQRT n) (x,y,z)` >>
  qabbrev_tac `t = hop (m + 1) (x,y,z)` >>
  `m + j < s /\ m + SUC j < s /\ m + 1 + SUC j <= s` by decide_tac >>
  `ping (hop (m + 1 + j) (x,y,z)) = ping (FUNPOW ping j t)` by metis_tac[hop_step_0_beyond_pong] >>
  `_ = FUNPOW ping (SUC j) t` by simp[FUNPOW_SUC] >>
  `_ = hop (m + 1 + SUC j) (x,y,z)` by metis_tac[hop_step_0_beyond_pong] >>
  `?xx yy zz. hop (m + 1 + SUC j) (x,y,z) = (xx,yy,zz)` by metis_tac[triple_parts] >>
  `n = windmill (xx,yy,zz)` by metis_tac[hop_windmill, step_sqrt] >>
  `0 < zz` by metis_tac[windmill_with_arms] >>
  qabbrev_tac `u = hop (m + 1 + j) (x,y,z)` >>
  assume_tac is_ping_by_ping_alt >>
  last_x_assum (qspecl_then [`u`] strip_assume_tac) >>
  rfs[]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\
            m = step 0 t /\ m + 1 + j < step (SQRT n) t ==> is_ping (hop (m + 1 + j) t) *)
(* Proof: by hop_step_0_beyond_pong_is_ping, triple_parts. *)
Theorem hop_step_0_beyond_pong_is_ping_alt:
  !n m j t. tik n /\ ~square n /\ n = windmill t /\
            m = step 0 t /\ m + 1 + j < step (SQRT n) t ==> is_ping (hop (m + 1 + j) t)
Proof
  metis_tac[hop_step_0_beyond_pong_is_ping, triple_parts]
QED

(* Very good! *)

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
            ~is_ping (hop (step (SQRT n) (x,y,z)) (x,y,z)) *)
(* Proof:
   Let n = windmill (x,y,z),
       m = step 0 (x,y,z),
       k = step (SQRT n) (x,y,z).
   By contradiction, suppose is_ping (hop k (x,y,z)).
   Note is_pong (hop m (x,y,z))                by hop_step_0_at_pong_is_pong [*]
    and 0 < z                                  by windmill_with_arms, ~square n
     so m <= k                                 by step_0_le_step_sqrt

   If m = k,
      Then is_pong (hop k (x,y,z))             by m = k
      This contradicts is_ping (hop k (x,y,z)) by pong_not_ping

   Otherwise, m < k, or m + 1 <= k

     Claim: hop (k + 1) (x,y,z) = ping (hop k (x,y,z))
     Proof: Let j = k - (m + 1).
            Then k = m + 1 + j                                 by arithmetic
            Note is_pong (FUNPOW pung m (x,y,z))               by hop_step_0_before_pong, [*]
             and !j. j < m ==> is_pung (hop j (x,y,z))         by hop_step_0_before_pong_is_pung
              or !j. j < m ==> is_pung (FUNPOW pung j (x,y,z)) by hop_step_0_before_pong
                 hop (k + 1) (x,y,z)
               = (FUNPOW ping (SUC j) o pong o FUNPOW pung m) (x,y,z)     by pung_to_ping_by_hop
               = ping ((FUNPOW ping j o pong  o FUNPOW pung m) (x,y,z))   by FUNPOW_SUC
               = ping (hop k (x,y,z))                          by pung_to_ping_by_hop]);


      Note m = x DIV (2 * z)                   by step_0
       and k = (x + SQRT n) DIV (2 * z)        by step_sqrt
      Let xx = 2 * (k + 1) * z - x,
          yy = y + (k + 1) * x - (k + 1) * (k + 1) * z.
      Then hop (k + 1) (x,y,z) = (xx,z,yy)     by hop_alt, x DIV (2 * z) = m < k + 1
       and n = windmill (hop k (x,y,z))        by hop_windmill k <= (x + SQRT n) DIV (2 * z)
             = windmill (xx,z,yy)              by ping_windmill_alt], is_ping (hop k (x,y,z))
      Thus ~(0 < yy)                           by hop_arm, k < k + 1
       ==> ~is_ping (hop k (x,y,z))            by is_ping_by_ping_alt, claim.
      This contradicts is_ping (hop k (x,y,z)).
*)
Theorem hop_step_sqrt_not_ping:
  !n x y z. tik n /\ ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
            ~is_ping (hop (step (SQRT n) (x,y,z)) (x,y,z))
Proof
  rpt strip_tac >>
  qabbrev_tac `m = step 0 (x,y,z)` >>
  qabbrev_tac `k = step (SQRT n) (x,y,z)` >>
  `is_pong (hop m (x,y,z))` by fs[hop_step_0_at_pong_is_pong] >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  `m <= k` by simp[step_0_le_step_sqrt, Abbr`m`, Abbr`k`] >>
  `m = k \/ m < k` by decide_tac >-
  metis_tac[ping_not_pong] >>
  `hop (k + 1) (x,y,z) = ping (hop k (x,y,z))` by
  (`is_pong (FUNPOW pung m (x,y,z))` by metis_tac[hop_step_0_before_pong, LESS_EQ_REFL] >>
  `!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))` by metis_tac[hop_step_0_before_pong_is_pung, hop_step_0_before_pong, LESS_IMP_LESS_OR_EQ] >>
  `k = (k - m - 1) + m + 1 /\ k + 1 = SUC (k - m - 1) + m + 1` by decide_tac >>
  qabbrev_tac `j = k - m - 1` >>
  `hop (k + 1) (x,y,z) = (FUNPOW ping (SUC j) o pong o FUNPOW pung m) (x,y,z)` by fs[pung_to_ping_by_hop] >>
  `_ = FUNPOW ping (SUC j) (pong (FUNPOW pung m (x,y,z)))` by fs[] >>
  `_ = ping (FUNPOW ping j (pong (FUNPOW pung m (x,y,z))))` by simp[FUNPOW_SUC] >>
  `_ = ping ((FUNPOW ping j o pong  o FUNPOW pung m) (x,y,z))` by fs[] >>
  metis_tac[pung_to_ping_by_hop]) >>
  `k = (x + SQRT n) DIV (2 * z)` by fs[step_sqrt, Abbr`k`] >>
  `~(k + 1 <= (x + SQRT n) DIV (2 * z)) /\ m < k + 1` by decide_tac >>
  qabbrev_tac `xx = 2 * (k + 1) * z - x` >>
  qabbrev_tac `yy = y + (k + 1) * x - (k + 1) * (k + 1) * z` >>
  `hop (k + 1) (x,y,z) = (xx,z,yy)` by rfs[hop_alt, step_0, Abbr`m`] >>
  `n = windmill (hop k (x,y,z))` by metis_tac[hop_windmill, LESS_OR_EQ] >>
  `_ = windmill (xx,z,yy)` by metis_tac[ping_windmill_alt] >>
  `~(0 < yy)` by metis_tac[hop_arm] >>
  assume_tac is_ping_by_ping_alt >>
  last_x_assum (qspecl_then [`hop k (x,y,z)`] strip_assume_tac) >>
  rgs[]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ ~is_ping t ==> ~is_ping (hop (step (SQRT n) t) t) *)
(* Proof: by hop_step_sqrt_not_ping, triple_parts. *)
Theorem hop_step_sqrt_not_ping_alt:
  !n t. tik n /\ ~square n /\ n = windmill t /\ ~is_ping t ==> ~is_ping (hop (step (SQRT n) t) t)
Proof
  metis_tac[hop_step_sqrt_not_ping, triple_parts]
QED

(* Excellent! *)

(* ------------------------------------------------------------------------- *)
(* Skipping by Triples.                                                      *)
(* ------------------------------------------------------------------------- *)

(* Define skips by triple *)
Definition skip_ping_def:
   skip_ping t = WHILE (is_ping) ping t
End

Definition skip_pung_def:
   skip_pung t = WHILE (is_pung) pung t
End

(* Theorem: ~is_ping (FUNPOW ping k t) /\ (!j. j < k ==> is_ping (FUNPOW ping j t)) ==>
            skip_ping t = FUNPOW ping k t *)
(* Proof: by skip_ping_def, iterate_while_thm. *)
Theorem skip_ping_thm:
  !t k. ~is_ping (FUNPOW ping k t) /\ (!j. j < k ==> is_ping (FUNPOW ping j t)) ==>
        skip_ping t = FUNPOW ping k t
Proof
  simp[skip_ping_def, iterate_while_thm]
QED

(* Theorem: ~is_ping t ==> skip_ping t = t *)
(* Proof: put k = 0 in skip_ping_thm. *)
Theorem skip_ping_none:
  !t. ~is_ping t ==> skip_ping t = t
Proof
  metis_tac[skip_ping_thm, FUNPOW_0, DECIDE``~(j < 0)``]
QED

(* Theorem: ~is_pung (FUNPOW pung k t) /\ (!j. j < k ==> is_pung (FUNPOW pung j t)) ==>
            skip_pung t = FUNPOW pung k t *)
(* Proof: by skip_pung_def, iterate_while_thm. *)
Theorem skip_pung_thm:
  !t k. ~is_pung (FUNPOW pung k t) /\ (!j. j < k ==> is_pung (FUNPOW pung j t)) ==>
        skip_pung t = FUNPOW pung k t
Proof
  simp[skip_pung_def, iterate_while_thm]
QED

(* Theorem: ~is_pung t ==> skip_pung t = t *)
(* Proof: put k = 0 in skip_pung_thm. *)
Theorem skip_pung_none:
  !t. ~is_pung t ==> skip_pung t = t
Proof
  metis_tac[skip_pung_thm, FUNPOW_0, DECIDE``~(j < 0)``]
QED


(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) ==>
            hopping 0 (x,y,z) = skip_pung (x,y,z) *)
(* Proof:
   Let m = step 0 (x,y,z).

   If is_ping (x,y,z),
      Npte 0 < z                               by windmill_with_arms, ~square n
      Then m = 0                               by step_0_of_ping, 0 < z
       and ~is_pung (x,y,z)                    by ping_not_pung
        hopping 0 (x,y,z)
      = hop (step 0 (x,y,z)) (x,y,z)           by hopping_def
      = hop m (x,y,z)                          by notation
      = (x,y,z)                                by hop_0, m = 0
      = skip_pung (x,y,z)                      by skip_pung_none

   Otherwise, ~is_ping (x,y,z).
   Then is_pong (hop m (x,y,z))                by hop_step_0_at_pong_is_pong
     and hop m (x,y,z) = FUNPOW pung m (x,y,z) by hop_step_0_before_pong
     so ~is_pung (hop m (x,y,z))               by pong_not_pung
   Also !j. j < m ==> is_pung (hop j (x,y,z))  by hop_step_0_before_pong_is_pung

        hopping 0 (x,y,z)
      = hop (step 0 (x,y,z)) (x,y,z)           by hopping_def
      = hop m (x,y,z)                          by notation
      = FUNPOW pung m (x,y,z)                  by above
      = skip_pung (x,y,z)                      by skip_pung_thm
*)
Theorem hopping_0:
  !n x y z. tik n /\ ~square n /\ n = windmill (x,y,z) ==>
            hopping 0 (x,y,z) = skip_pung (x,y,z)
Proof
  rpt strip_tac >>
  qabbrev_tac `m = step 0 (x,y,z)` >>
  `is_ping (x,y,z) \/ ~is_ping (x,y,z)` by decide_tac >| [
    `0 < z` by metis_tac[windmill_with_arms] >>
    `m = 0` by fs[step_0_of_ping, Abbr`m`] >>
    fs[skip_pung_none, hopping_def, hop_0, ping_not_pung, Abbr`m`],
    `is_pong (hop m (x,y,z))` by fs[hop_step_0_at_pong_is_pong, Abbr`m`] >>
    `hop m (x,y,z) = FUNPOW pung m (x,y,z)` by fs[hop_step_0_before_pong, Abbr`m`] >>
    `!j. j < m ==> is_pung (FUNPOW pung j (x,y,z))` by fs[hop_step_0_before_pong_is_pung, GSYM hop_step_0_before_pong, Abbr`m`] >>
    fs[skip_pung_thm, hopping_def, pong_not_pung, Abbr`m`]
  ]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t ==> hopping 0 t = skip_pung t *)
(* Proof: by hopping_0, triple_parts. *)
Theorem hopping_0_alt:
  !n t. tik n /\ ~square n /\ n = windmill t ==> hopping 0 t = skip_pung t
Proof
  metis_tac[hopping_0, triple_parts]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
            hopping (SQRT n) (x,y,z) = skip_ping (pong (hopping 0 (x,y,z))) *)
(* Proof:
   Let m = step 0 (x,y,z),
       k = step (SQRT n) (x,y,z).

   Note hopping (SQRT n) (x,y,z)
      = hop (step (SQRT n) (x,y,z)) (x,y,z)    by hopping_def
      = hop k (x,y,z)                          by notation [1]
    and hopping 0 (x,y,z)
      = hop (step 0 (x,y,z)) (x,y,z)           by hopping_def
      = hop m (x,y,z)                          by notation [2]
   The goal is to show: hop k (x,y,z) = skip_ping (pong (hop m (x,y,z))).

   Note 0 < z                                  by windmill_with_arms, ~square n
    and m < k                                  by step_0_lt_step_sqrt, ~is_ping (x,y,z)
   Let k = m + 1 + h,
       t = hop (m + 1) (x,y,z),
        hop k (x,y,z)
      = FUNPOW ping h t                        by hop_step_0_beyond_pong, m + h < k

   Note !j. j < h ==> is_ping (hop (m + 1 + j) (x,y,z))
                                               by hop_step_0_beyond_pong_is_ping
     or !j. j < h ==> is_ping (FUNPOW ping j t)
                                               by hop_step_0_beyond_pong
   Note ~is_ping (hop k (x,y,z))               by hop_step_sqrt_not_ping
     or ~is_ping (FUNPOW ping h t)             by above

        hopping (SQRT n) (x,y,z)
      = hop k (x,y,z)                          by [1]
      = FUNPOW ping h t                        by above
      = skip_ping (FUNPOW ping 0 t)            by skip_ping_thm
      = skip_ping (hop (m + 1) (x,y,z))        by FUNPOW_0
      = skip_ping (pong (hop m (x,y,z)))       by hop_step_0_at_pong
      = skip_ping (pong (hopping 0 (x,y,z)))   by [2]
*)
Theorem hopping_sqrt:
  !n x y z. tik n /\ ~square n /\ n = windmill (x,y,z) /\ ~is_ping (x,y,z) ==>
            hopping (SQRT n) (x,y,z) = skip_ping (pong (hopping 0 (x,y,z)))
Proof
  rw[hopping_def] >>
  qabbrev_tac `n = windmill (x,y,z)` >>
  qabbrev_tac `m = step 0 (x,y,z)` >>
  qabbrev_tac `k = step (SQRT n) (x,y,z)` >>
  `0 < z` by metis_tac[windmill_with_arms] >>
  `m < k` by fs[step_0_lt_step_sqrt, Abbr`m`, Abbr`k`, Abbr`n`] >>
  `k = m + 1 + (k - m - 1)` by decide_tac >>
  qabbrev_tac `h = k - m - 1` >>
  qabbrev_tac `t = hop (m + 1) (x,y,z)` >>
  `t = pong (hop m (x,y,z))` by fs[hop_step_0_at_pong, Abbr`m`, Abbr`n`, Abbr`t`] >>
  `m + h < k` by decide_tac >>
  `hop k (x,y,z) = FUNPOW ping h t` by metis_tac[hop_step_0_beyond_pong] >>
  `~is_ping (FUNPOW ping h t)` by metis_tac[hop_step_sqrt_not_ping] >>
  `!j. j < h ==> is_ping (FUNPOW ping j t)` by
  (rpt strip_tac >>
  `m + 1 + j < k` by decide_tac >>
  `is_ping (hop (m + 1 + j) (x,y,z))` by fs[hop_step_0_beyond_pong_is_ping, Abbr`n`, Abbr`m`] >>
  rfs[hop_step_0_beyond_pong, Abbr`n`, Abbr`m`, Abbr`t`]) >>
  `skip_ping t = FUNPOW ping h t` by fs[skip_ping_thm] >>
  rfs[]
QED

(* Theorem: tik n /\ ~square n /\ n = windmill t /\ ~is_ping t ==>
            hopping (SQRT n) t = skip_ping (pong (hopping 0 t)) *)
(* Proof: by hopping_sqrt, triple_parts. *)
Theorem hopping_sqrt_alt:
  !n t. tik n /\ ~square n /\ n = windmill t /\ ~is_ping t ==>
        hopping (SQRT n) t = skip_ping (pong (hopping 0 t))
Proof
  metis_tac[hopping_sqrt, triple_parts]
QED


(*
For n = 97, SQRT n = 9, k = n DIV 4 = 24.
> EVAL ``hopping 9 (1,24,1)``; = (9,1,4)
> EVAL ``hopping 9 (9,1,4)``; = (7,4,3)
> EVAL ``hopping 9 (7,4,3)``; = (5,3,6)
> EVAL ``hopping 9 (5,3,6)``; = (7,6,2)
> EVAL ``hopping 9 (7,6,2)``; = (9,2,2)
*)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
            u <= v /\ v < w /\ w <= k /\ ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
            EL v ls = skip_pung (EL u ls) /\
            EL (v + 1) ls = pong (EL v ls) /\
            EL w ls = skip_ping (EL (v + 1) ls) /\
            (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
            (!j. v < j /\ j < w ==> is_ping (EL j ls)) *)
(* Proof:
   Let ls = path n k.
   By blocks_path_mem, we have the following:
      u <= v /\ v < w /\ w <= k /\
      ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
      (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
      (!j. v < j /\ j < w ==> is_ping (EL j ls))
   with the remaining goals:
   (1) EL v ls = skip_pung (EL u ls) /\
       Note EL v ls = FUNPOW pung (v - u) (EL u ls)                by blocks_path_indices_by_funpow
        and ~is_pung (EL v ls)                                     by pong_not_pung
       Also !j. j < v - u ==> is_pung (FUNPOW pung j (EL u ls))    by path_element_pung_funpow
       Thus EL v ls = skip_pung (EL u ls)                          by skip_pung_thm

   (2) EL (v + 1) ls = pong (EL v ls), true                        by blocks_path_indices_by_funpow

   (3) EL w ls = skip_ping (EL (v + 1) ls) /\
       Note EL w ls = FUNPOW ping (w - (v + 1)) (EL (v + 1) ls)             by blocks_path_indices_by_funpow
        and !j. j < w - (v + 1) ==> is_ping (FUNPOW ping j (EL (v + 1) ls)) by path_element_ping_funpow
       Thus EL w ls = skip_ping (EL (v + 1) ls)                    by skip_ping_thm
*)
Theorem blocks_path_element:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ MEM (u,v,w) (blocks (block_pairs ls)) ==>
              u <= v /\ v < w /\ w <= k /\ ~is_ping (EL u ls) /\ is_pong (EL v ls) /\ ~is_ping (EL w ls) /\
              EL v ls = skip_pung (EL u ls) /\
              EL (v + 1) ls = pong (EL v ls) /\
              EL w ls = skip_ping (EL (v + 1) ls) /\
              (!j. u <= j /\ j < v ==> is_pung (EL j ls)) /\
              (!j. v < j /\ j < w ==> is_ping (EL j ls))
Proof
  simp[] >>
  ntac 6 strip_tac >>
  qabbrev_tac `ls = path n k` >>
  assume_tac blocks_path_mem >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  qabbrev_tac `X = (w = skip_idx_ping ls (v + 1))` >>
  qabbrev_tac `Y = (v = skip_idx_pung ls u)` >>
  assume_tac blocks_path_indices_by_funpow >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  simp[] >>
  `EL v ls = skip_pung (EL u ls) /\ EL w ls = skip_ping (EL (v + 1) ls)` suffices_by metis_tac[] >>
  rpt strip_tac >| [
    `!j. j < v - u ==> is_pung (FUNPOW pung j (EL u ls))` by
  (rpt strip_tac >>
    assume_tac path_element_pung_funpow >>
    last_x_assum (qspecl_then [`n`, `k`, `u`, `v - u`] strip_assume_tac) >>
    rfs[] >>
    `u <= j + u /\ j + u < v /\ j <= v - u` by decide_tac >>
    metis_tac[]) >>
    metis_tac[skip_pung_thm, pong_not_pung],
    `!j. j < w - (v + 1) ==> is_ping (FUNPOW ping j (EL (v + 1) ls))` by
  (rpt strip_tac >>
    assume_tac path_element_ping_funpow >>
    last_x_assum (qspecl_then [`n`, `k`, `v + 1`, `w - (v + 1) `] strip_assume_tac) >>
    rfs[] >>
    `v < j + (v + 1) /\ j + (v + 1) < w` by decide_tac >>
    metis_tac[LESS_IMP_LESS_OR_EQ]) >>
    metis_tac[skip_ping_thm]
  ]
QED

(* Idea: the third of indices can be reached by hopping. *)

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
            MEM (u,v,w) (blocks (block_pairs ls)) ==> EL w ls = hopping (SQRT n) (EL u ls) *)
(* Proof:
   Let ls = path n k,
       t = EL u ls.
   Note n = windmill t                         by path_element_windmill_alt, u <= k

       EL w ls
     = skip_ping (EL (v + 1) ls)               by blocks_path_element
     = skip_ping (pong (EL v ls))              by blocks_path_element
     = skip_ping (pong (skip_pung t))
                                               by blocks_path_element
     = skip_ping (pong (hopping 0 t))          by hopping_0_alt
     = hopping (SQRT n) t                      by hopping_sqrt_alt
*)
Theorem blocks_path_third_by_hopping:
  !n k u v w. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\
              MEM (u,v,w) (blocks (block_pairs ls)) ==> EL w ls = hopping (SQRT n) (EL u ls)
Proof
  rw_tac std_ss[] >>
  assume_tac blocks_path_element >>
  last_x_assum (qspecl_then [`n`, `k`, `u`, `v`, `w`] strip_assume_tac) >>
  rfs[] >>
  `n = windmill (EL u ls)` by fs[path_element_windmill_alt, Abbr`ls`] >>
  fs[hopping_sqrt_alt, hopping_0_alt]
QED

(* This is a spectacular result! *)

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls); t = EL j bs in
            tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ j <  *)
(* Proof:
   Let (x,y,z) = t.                            by triple_parts
   Then MEM (x,y,z) bs                         by EL_MEM, j < LENGTH bs
        EL (SND (SND t)) ls
      = EL w ls                                by SND
      = hopping (SQRT n) (EL u ls)             by blocks_path_third_by_hopping
      = hopping (SQRT n) (EL (FST t) ls)       by FST
*)
Theorem blocks_path_triple_by_hopping:
  !n k j. let ls = path n k; bs = blocks (block_pairs ls); t = EL j bs in
          tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ j < LENGTH bs ==>
          EL (SND (SND t)) ls = hopping (SQRT n) (EL (FST t) ls)
Proof
  rw_tac std_ss[] >>
  `?u v w. t = (u,v,w)` by metis_tac[triple_parts] >>
  `MEM (u,v,w) bs` by metis_tac[EL_MEM] >>
  metis_tac[blocks_path_third_by_hopping, FST, SND]
QED

(* Idea: for ls = path n k, iterate of hopping (SQRT n) from (HD ls) is equivalent to hopping from some (EL u ls) of a block (u,v,w). *)

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ ~square n /\
            flip (LAST ls) = LAST ls /\ j < LENGTH bs ==>
            FUNPOW (hopping (SQRT n)) (j + 1) (HD ls) = hopping (SQRT n) (EL (FST (EL j bs)) ls) *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
       bs = blocks ps.
   The goal is to show: FUNPOW f (j + 1) (HD ls) = f (EL (FST (EL j bs)) ls)

   By induction on j.
   Base: 0 < LENGTH bs ==> FUNPOW f (0 + 1) (HD ls) = f (EL (FST (EL 0 bs)) ls)
         LHS = FUNPOW f 1 (HD ls)
             = f (HD ls)                       by FUNPOW_1
         Note ls <> []                         by path_not_nil
           so HD bs = (0, HD ps)               by blocks_head, [1]
         RHS = f (EL (FST (EL 0 bs)) ls)
             = f (EL (FST (HD bs)) ls)         by EL
             = f (EL 0 ls)                     by [1]
             = f (HD ls) = LHS                 by EL

   Step: j < LENGTH bs ==> FUNPOW f (j + 1) (HD ls) = f (EL (FST (EL j bs)) ls)
     ==> SUC j < LENGTH bs ==> FUNPOW f (SUC j + 1) (HD ls) = f (EL (FST (EL (SUC j) bs)) ls)
      Note j < LENGTH bs.
       and MEM (EL j bs) bs                    by EL_MEM, j < LENGTH bs
       Let (u,v,w) = EL j bs.
           FUNPOW f (SUC j + 1) (HD ls)
         = f (FUNPOW f (j + 1) (HD ls))        by FUNPOW_SUC, SUC (j + 1) = SUC j + 1
         = f (f (EL (FST (EL j bs)) ls))       by induction hypothesis
         = f (f (EL u ls))                     by (u,v,w) = EL j bs
         = f (EL w ls)                         by blocks_path_third_by_hopping
         = f (EL (SND (SND (EL j bs))) ls)     by (u,v,w) = EL j bs
         = f (EL (FST (EL (j + 1) bs)) ls)     by blocks_next_element
         = f (EL (FST (EL (SUC j) bs)) ls)     by ADD1
*)
Theorem hopping_sqrt_funpow:
  !n k j. let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ ~square n /\
          flip (LAST ls) = LAST ls /\ j < LENGTH bs ==>
          FUNPOW (hopping (SQRT n)) (j + 1) (HD ls) = hopping (SQRT n) (EL (FST (EL j bs)) ls)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `f = hopping (SQRT n)` >>
  qabbrev_tac `ps = block_pairs ls` >>
  Induct_on `j` >| [
    rw[] >>
    `LENGTH bs <> 0` by decide_tac >>
    `ps <> []` by metis_tac[blocks_length, LENGTH_EQ_0] >>
    simp[blocks_head, Abbr`bs`],
    rpt strip_tac >>
    `j <= LENGTH bs /\ j < LENGTH bs` by decide_tac >>
    `MEM (EL j bs) bs` by metis_tac[EL_MEM] >>
    `?u v w. EL j bs = (u,v,w)` by metis_tac[triple_parts] >>
    `FUNPOW f (j + 1) (HD ls) = f (EL u ls)` by fs[] >>
    `_ = EL w ls` by metis_tac[blocks_path_third_by_hopping] >>
    `_ = EL (FST (EL (j + 1) bs)) ls` by fs[blocks_next_element, Abbr`bs`] >>
    fs[FUNPOW_SUC, GSYM ADD1]
  ]
QED

(* Idea: for ls = path n k, iterate of hopping (SQRT n) from (HD ls) ends at some (EL w ls) of a block (u,v,w). *)

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ ~square n /\
            flip (LAST ls) = LAST ls /\ j < LENGTH bs ==>
            FUNPOW (hopping (SQRT n)) (j + 1) (HD ls) = EL (SND (SND (EL j bs))) ls *)
(* Proof:
   Let ls = path n k,
       bs = blocks (block_pairs ls).
     FUNPOW (hopping (SQRT n)) (j + 1) (HD ls)
   = hopping (SQRT n) (EL (FST (EL j bs)) ls)          by hopping_sqrt_funpow, j < LENGTH bs
   = EL (SND (SND (EL j bs))) ls                       by blocks_path_triple_by_hopping, j < LENGTH bs
*)
Theorem hopping_sqrt_funpow_eqn:
  !n k j. let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ ~square n /\
          flip (LAST ls) = LAST ls /\ j < LENGTH bs ==>
          FUNPOW (hopping (SQRT n)) (j + 1) (HD ls) = EL (SND (SND (EL j bs))) ls
Proof
  rw_tac std_ss[] >>
  assume_tac hopping_sqrt_funpow >>
  last_x_assum (qspecl_then [`n`, `k`, `j`] strip_assume_tac) >>
  rfs[] >>
  assume_tac blocks_path_triple_by_hopping >>
  last_x_assum (qspecl_then [`n`, `k`, `j`] strip_assume_tac) >>
  rfs[]
QED

(* Theorem: let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls ==>
            FUNPOW (hopping (SQRT n)) (LENGTH (blocks (block_pairs ls))) (HD ls) = LAST ls *)
(* Proof:
   Let ls = path n k,
       ps = block_pairs ls,
       bs = blocks ps,
       h = LENGTH bs.
   The goal is to show: FUNPOW (hopping (SQRT n)) h (HD ls) = LAST ls

   If h = 0,
      Then bs = []                             by LENGTH_EQ_0
       and k = 0                               by blocks_path_eq_nil
      Thus ls = [(1,n DIV 4,1)]                by path_0
           FUNPOW (hopping (SQRT n)) 0 (HD ls)
         = HD ls                               by FUNPOW_0
         = LAST ls                             by ls = [(1,n DIV 4,1)]

   Otherwise 0 < h.
      Then bs <> []                            by LENGTH_EQ_0
       and 0 < k                               by blocks_path_eq_nil
       Let (u,v,w) = LAST bs.
       Then MEM (u,v,w) bs                     by LAST_MEM
        and SND (SND (LAST bs)) = w = k        by blocks_path_last

           FUNPOW (hopping (SQRT n)) h (HD ls)
         = hopping (SQRT n) (EL (FST (EL (h-1) bs)) ls)    by hopping_sqrt_funpow
         = hopping (SQRT n) (EL (FST (LAST bs)) ls)        by LAST_EL
         = hopping (SQRT n) (EL u ls)                      by (u,v,w) = LAST bs
         = EL w ls                                         by blocks_path_third_by_hopping
         = LAST ls                                         by path_last_alt, w = k
*)
Theorem hopping_sqrt_funpow_max:
  !n k. let ls = path n k in tik n /\ ~square n /\ flip (LAST ls) = LAST ls ==>
        FUNPOW (hopping (SQRT n)) (LENGTH (blocks (block_pairs ls))) (HD ls) = LAST ls
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `ps = block_pairs ls` >>
  qabbrev_tac `bs = blocks ps` >>
  qabbrev_tac `h = LENGTH bs` >>
  `h = 0 \/ 0 < h` by decide_tac >| [
    `bs = [] /\ k = 0` by metis_tac[blocks_path_eq_nil, LENGTH_EQ_0] >>
    rfs[path_0, Abbr`ls`],
    `bs <> [] /\ 0 < k` by metis_tac[blocks_path_eq_nil, LENGTH_EQ_0, NOT_ZERO] >>
    `?u v w. LAST bs = (u,v,w)` by metis_tac[triple_parts] >>
    `MEM (u,v,w) bs` by metis_tac[LAST_MEM] >>
    `LAST bs = EL (h - 1) bs` by metis_tac[LAST_EL, PRE_SUB1] >>
    qabbrev_tac `X = (FUNPOW (hopping (SQRT n)) h (HD ls) = LAST ls)` >>
    assume_tac hopping_sqrt_funpow >>
    last_x_assum (qspecl_then [`n`,`k`,`h-1`] strip_assume_tac) >>
    rfs[Abbr`ps`] >>
    `_ = hopping (SQRT n) (EL u ls)` by metis_tac[FST] >>
    `_ = EL w ls` by metis_tac[blocks_path_third_by_hopping] >>
    `_ = EL k ls` by metis_tac[blocks_path_last, SND] >>
    fs[path_last_alt, Abbr`ls`]
  ]
QED

(*
Example of a path with two flip-fixes.
For n = 65 = 1² + 8² = 7² + 4²
This one has k = n DIV 4 = 65 DIV 4 = 16.
However, each path (almost) from a zagier-fix leads to only one flip-fix:
> EVAL ``path 65 5``; = [(1,16,1); (1,1,16); (3,1,14); (5,1,10); (7,1,4); (1,4,4)]
> EVAL ``MAP (\j. FUNPOW (zagier o flip) j (1,1,16)) [0 ..< 5]``; = [(1,1,16); (3,1,14); (5,1,10); (7,1,4); (1,4,4)]
> EVAL ``MAP (\j. FUNPOW (zagier o flip) j (5,5,2)) [0 ..< 4]``; = [(5,5,2); (1,8,2); (3,2,7); (7,2,2)]

Do I know that, for tik n /\ ~square n, path starting from (1,n DIV 4,1) leads to only one flip-fix?
Or we can just say: hopping (SQRT n) will lead to the first flip-fix, and later let tik n /\ prime n to resolve the issue.

involute_involute_fix_odd_period_fix
|- !f g s p x. FINITE s /\ f involute s /\ g involute s /\ x IN fixes f s /\
               p = iterate_period (f o g) x /\ ODD p ==>
               !j. 0 < j /\ j < p ==> (FUNPOW (f o g) j x IN fixes g s <=> j = HALF p)

involute_involute_fix_orbit_fix_odd
|- !f g s p x. FINITE s /\ f involute s /\ g involute s /\ x IN fixes f s /\
               p = iterate_period (f o g) x /\ ODD p ==> FUNPOW (f o g) (HALF p) x IN fixes g s

involute_involute_fix_sing_period_odd
|- !f g s p x. FINITE s /\ f involute s /\ g involute s /\ fixes f s = {x} /\
               p = iterate_period (f o g) x ==> ODD p
zagier_fixes_prime
|- !p. prime p /\ tik p ==> fixes zagier (mills p) = {(1,1,p DIV 4)}

A quick way to find iterate_period of triple x, without proof, by iterate from u to fixed v:

Definition path_period_def:
   path_period u v k = if (u = v) then k else path_period ((zagier o flip) u) v (k+1)
End
-- need a Termination proof.

Definition path_period_def:
   path_period n = let s = (1,1,n DIV 4) in WHILE (\(j,t). t <> s) (\(j,t). (SUC j, (zagier o flip) t)) (1, (zagier o flip) s)
End

> EVAL ``path_period 61``; = (11,1,1,15)  which means p = 11, back to (1,1,5) a zagier-fix at LAST.
> EVAL ``path 61 (1 + HALF 11)``; = [(1,15,1); (1,1,15); (3,1,13); (5,1,9); (7,1,3); (1,5,3); (5,3,3)]
> EVAL ``path 61 (HALF (1 + 11))``; = [(1,15,1); (1,1,15); (3,1,13); (5,1,9); (7,1,3); (1,5,3); (5,3,3)

> EVAL ``path_period 97``; = (27,1,1,24)  which means p = 27, back to (1,1,24) a zagier-fix at LAST.
> EVAL ``path 97 (1 + HALF 27)``; =
         [(1,24,1); (1,1,24); (3,1,22); (5,1,18); (7,1,12); (9,1,4); (1,6,4);
          (7,4,3); (1,8,3); (5,3,6); (7,6,2); (3,11,2); (1,2,12); (5,2,9); (9,2,2)]
> EVAL ``path 97 (HALF (1 + 27))``; =
         [(1,24,1); (1,1,24); (3,1,22); (5,1,18); (7,1,12); (9,1,4); (1,6,4);
          (7,4,3); (1,8,3); (5,3,6); (7,6,2); (3,11,2); (1,2,12); (5,2,9); (9,2,2)]
For ODD period p, (1 + p) is even, and HALF (1 + p) is an exact half.
But ODD_SUC_HALF  |- !n. ODD n ==> HALF (SUC n) = SUC (HALF n)
*)

(* ------------------------------------------------------------------------- *)
(* Hopping for tik primes.                                                   *)
(* ------------------------------------------------------------------------- *)

(* Theorem: let s = mills n; u = (1,1,n DIV 4) in tik n /\ prime n ==>
            ~square n /\ FINITE s /\ zagier involute s /\ flip involute s /\
            fixes zagier s = {u} /\ ODD (iterate_period (zagier o flip) u) *)
(* Proof:
   Let u = (1,1,n DIV 4),
       p = iterate_period (zagier o flip) u,
       s = mills n.
   Note ~square n                  by prime_non_square
     so FINITE s                   by mills_finite_non_square
    and zagier involute s          by zagier_involute_mills_prime
    and flip involute s            by flip_involute_mills
   Also fixes zagier s = {u}       by zagier_fixes_prime, tik n /\ prime n
   Thus ODD p                      by involute_involute_fix_sing_period_odd
*)
Theorem tik_prime_property:
  !n. let s = mills n; u = (1,1,n DIV 4) in tik n /\ prime n ==>
      ~square n /\ FINITE s /\ zagier involute s /\ flip involute s /\
      fixes zagier s = {u} /\ ODD (iterate_period (zagier o flip) u)
Proof
  simp[] >>
  ntac 2 strip_tac >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  qabbrev_tac `p = iterate_period (zagier o flip) u` >>
  qabbrev_tac `s = mills n` >>
  `~square n` by simp[prime_non_square] >>
  `FINITE s` by fs[mills_finite_non_square, Abbr`s`] >>
  `zagier involute s` by metis_tac[zagier_involute_mills_prime] >>
  `flip involute s` by metis_tac[flip_involute_mills] >>
  `fixes zagier s = {u}` by fs[zagier_fixes_prime, Abbr`s`, Abbr`u`] >>
  drule_then strip_assume_tac involute_involute_fix_sing_period_odd >>
  last_x_assum (qspecl_then [`zagier`, `flip`, `p`, `u`] strip_assume_tac) >>
  rfs[]
QED

(* Theorem: tik n /\ prime n ==> ODD (iterate_period (zagier o flip) (1,1,n DIV 4)) *)
(* Proof: by tik_prime_property. *)
Theorem tik_prime_iterate_period_odd:
  !n. tik n /\ prime n ==> ODD (iterate_period (zagier o flip) (1,1,n DIV 4))
Proof
  metis_tac[tik_prime_property]
QED

(* Theorem: tik n ==> (iterate_period (zagier o flip) (1,1,n DIV 4) = 1 <=> n = 5) *)
(* Proof:
   Note iterate_period f u = 1 <=> n DIV 4 = 1 by zagier_flip_1_1_z_period
   Then n = (n DIV 4) * 4 + (n MOD 4)          by DIVISION
          = 1 * 4 + 1 = 5                      by tik n
*)
Theorem tik_iterate_period_eq_1:
  !n. tik n ==> (iterate_period (zagier o flip) (1,1,n DIV 4) = 1 <=> n = 5)
Proof
  rpt strip_tac >>
  `n = (n DIV 4) * 4 + 1` by metis_tac[DIVISION, DECIDE``0 < 4``] >>
  simp[zagier_flip_1_1_z_period]
QED

(* Theorem: let ls = path n k in tik n ==> (flip (HD ls) = HD ls <=> n = 5) *)
(* Proof:
   Let ls = path n k.
   Then HD ls = (1,n DIV 4,1)                  by path_head
       flip (HD ls) = HD ls
   <=> n DIV 4 = 1                             by flip_fix
   <=> n = 1 * 4 + n MOD 1                     by DIVISION
   <=> n = 1 * 4 + 1                           by tik n
   <=> n = 5
*)
Theorem tik_path_head_flip_fix:
  !n k. let ls = path n k in tik n ==> (flip (HD ls) = HD ls <=> n = 5)
Proof
  rw_tac std_ss[] >>
  `HD ls = (1, n DIV 4, 1)` by fs[path_head, Abbr`ls`] >>
  `flip (HD ls) = HD ls <=> n DIV 4 = 1` by simp[flip_fix] >>
  `n = (n DIV 4) * 4 + 1` by metis_tac[DIVISION, DECIDE``0 < 4``] >>
  fs[]
QED

(* Idea: for ls = path n k, LAST ls is iterate (zagier o flip) of zagier-fix to half period. *)

(* Theorem: let ls = path n k; u = (1,1,n DIV 4); p = iterate_period (zagier o flip) u in
            k = 1 + HALF p ==> LAST ls = FUNPOW (zagier o flip) (HALF p) u *)
(* Proof:
   Note ls <> []                   by path_not_nil
    and 0 < k                      by k = 1 + HALF p

     LAST ls
   = EL k ls                       by path_last_alt
   = EL (1 + HALF p) ls            by k = 1 + HALF p
   = EL (SUC (HALF p)) ls          by ADD1
   = EL (HALF p) (TL ls)           by EL
   = FUNPOW (zagier o flip) (HALF p) u
                                   by path_tail_element, HALF p < k
*)
Theorem path_last_at_half_period:
  !n k. let ls = path n k; u = (1,1,n DIV 4); p = iterate_period (zagier o flip) u in
        k = 1 + HALF p ==> LAST ls = FUNPOW (zagier o flip) (HALF p) u
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `k = 1 + HALF p` >>
  `ls <> []` by fs[path_not_nil, Abbr`ls`] >>
  `0 < k /\ HALF p < k` by fs[Abbr`k`] >>
  `LAST ls = EL (SUC (HALF p)) ls` by fs[path_last_alt, ADD1, Abbr`ls`, Abbr`k`] >>
  `_ = EL (HALF p) (TL ls)` by simp[EL] >>
  fs[path_tail_element, Abbr`ls`, Abbr`u`]
QED

(* Idea: for ls = path n k of a tik prime, flip (LAST ls) = (LAST ls) when k = 1 + HALF (iterate period). *)

(* Theorem: let ls = path n k in tik n /\ prime n /\
            k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) ==> flip (LAST ls) = LAST ls *)
(* Proof:
   Let u = (1,1,n DIV 4),
       p = iterate_period (zagier o flip) u,
       k = 1 + HALF p,
       s = mills n,
       t = FUNPOW (zagier o flip) (HALF p) u.
   Note FINITE s /\ zagier involute s /\ flip involute s /\
        fixes zagier s = {u} /\ ODD p          by tik_prime_property

   Thus u IN fixes zagier s                    by IN_SING
    ==> t IN fixes flip s                      by involute_involute_fix_orbit_fix_odd
    But t = LAST ls                            by path_last_at_half_period
     so flip (LAST ls) = LAST ls               by flip_fixes_alt
*)
Theorem tik_prime_path_last_flip_fix:
  !n k. let ls = path n k in tik n /\ prime n /\
        k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) ==> flip (LAST ls) = LAST ls
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  qabbrev_tac `p = iterate_period (zagier o flip) u` >>
  qabbrev_tac `k = 1 + HALF p` >>
  qabbrev_tac `s = mills n` >>
  qabbrev_tac `t = FUNPOW (zagier o flip) (HALF p) u` >>
  `LAST ls = t` by metis_tac[path_last_at_half_period] >>
  assume_tac tik_prime_property >>
  last_x_assum (qspecl_then [`n`] strip_assume_tac) >>
  rfs[] >>
  `t IN fixes flip s` by fs[involute_involute_fix_orbit_fix_odd, Abbr`t`, Abbr`p`, Abbr`u`] >>
  fs[flip_fixes_alt]
QED

(* Idea: for ls = path n k of a tik prime, flip (EL j ls) <> (EL j ls) when j < k = 1 + HALF (iterate period). *)

(* Theorem: let ls = path n k in tik n /\ prime n /\
            k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
            0 < j /\ j < k ==> flip (EL j ls) <> EL j ls *)
(* Proof:
   Let u = (1,1,n DIV 4),
       p = iterate_period (zagier o flip) u,
       k = 1 + HALF p,
       s = mills n,
       t = FUNPOW (zagier o flip) (HALF p) u.
   Note FINITE s /\ zagier involute s /\ flip involute s /\
        fixes zagier s = {u} /\ ODD p          by tik_prime_property

   By contradiction, suppose flip (EL j ls) = EL j ls.
   Then EL j ls IN fixes flip s                by flip_fixes_alt
   Note 0 < p                                  by ODD_POS
     so HALF p < p                             by HALF_LT
     or k = 1 + HALF p <= p

   Let h = j - 1                               by 0 < j
   Then j = SUC h, and h < j
        EL j ls
      = EL h (TL ls)                           by EL
      = FUNPOW (zagier o flip) h u             by path_tail_element, h < k
    ==> h = HALF p                             by involute_involute_fix_odd_period_fix, h < p
     or j - 1 = HALF p,
     so j = 1 + HALF p = k.
    This contradicts j < k.
*)
Theorem tik_prime_path_not_flip_fix:
  !n k j. let ls = path n k in tik n /\ prime n /\
          k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
          0 < j /\ j < k ==> flip (EL j ls) <> EL j ls
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  qabbrev_tac `p = iterate_period (zagier o flip) u` >>
  qabbrev_tac `k = 1 + HALF p` >>
  qabbrev_tac `s = mills n` >>
  assume_tac tik_prime_property >>
  last_x_assum (qspecl_then [`n`] strip_assume_tac) >>
  rfs[] >>
  spose_not_then strip_assume_tac >>
  `EL j ls IN s` by fs[path_element_in_mills, Abbr`s`, Abbr`ls`] >>
  `EL j ls IN fixes flip s` by fs[flip_fixes_alt, Abbr`s`] >>
  `j = SUC (j - 1)` by decide_tac >>
  qabbrev_tac `h = j - 1` >>
  `EL j ls = EL h (TL ls)` by metis_tac[EL] >>
  `_ = FUNPOW (zagier o flip) h u` by fs[path_tail_element, Abbr`h`, Abbr`u`, Abbr`ls`] >>
  `FUNPOW (zagier o flip) h u IN fixes flip s` by metis_tac[] >>
  `0 < p` by simp[ODD_POS] >>
  `k <= p` by metis_tac[HALF_LT, LESS_EQ, ADD1] >>
  drule_then strip_assume_tac involute_involute_fix_odd_period_fix >>
  last_x_assum (qspecl_then [`zagier`, `flip`, `p`, `u`, `h`] strip_assume_tac) >>
  rfs[] >>
  `j = 1 + HALF p` by decide_tac >>
  fs[Abbr`k`]
QED

(* Idea: for ls = path n k of a tik prime, flip (EL j ls) = (EL j ls) iff j = k = 1 + HALF (iterate period). *)

(* Theorem: let ls = path n k in tik n /\ prime n /\
          k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
          0 < j /\ j <= k ==> (flip (EL j ls) = EL j ls <=> j = k) *)
(* Proof:
   If part: flip (EL j ls) = EL j ls ==> j = k
      By contradiction, assume j <> k.
      Then j < k                               by j <= k
      Thus flip (EL j ls) <> EL j ls           by tik_prime_path_not_flip_fix
      This contradicts flip (EL j ls) = EL j ls.

   Only-if part: j = k ==> flip (EL j ls) = EL j ls
      Note LAST ls = EL k ls                   by path_last_alt
      Thus flip (EL j ls) = EL j ls            by tik_prime_path_last_flip_fix
*)
Theorem tik_prime_path_flip_fix_iff:
  !n k j. let ls = path n k in tik n /\ prime n /\
          k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
          0 < j /\ j <= k ==> (flip (EL j ls) = EL j ls <=> j = k)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  qabbrev_tac `p = iterate_period (zagier o flip) u` >>
  qabbrev_tac `k = 1 + HALF p` >>
  rw[EQ_IMP_THM] >| [
    assume_tac tik_prime_path_not_flip_fix >>
    last_x_assum (qspecl_then [`n`, `k`, `j`] strip_assume_tac) >>
    rfs[],
    `LAST ls = EL j ls` by simp[path_last_alt, Abbr`ls`] >>
    assume_tac tik_prime_path_last_flip_fix >>
    last_x_assum (qspecl_then [`n`, `j`] strip_assume_tac) >>
    rfs[]
  ]
QED

(* Idea: for ls = path n k, iterate of hopping (SQRT n) from (HD ls) ends at some (EL w ls), a path element. *)

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in
            tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ j <= LENGTH bs ==>
            ?w. w <= k /\ FUNPOW (hopping (SQRT n)) j (HD ls) = EL w ls *)
(* Proof:
   If j = 0,
     FUNPOW (hopping (SQRT n)) 0 (HD ls)
   = HD ls                                             by FUNPOW_0
   = EL 0 ls                                           by EL
     Pick w = 0.
     Then k = 0 <=> bs = []                            by blocks_path_eq_nil
                <=> LENGTH bs = 0                      by LENGTH_EQ_0
                <=> j = LENGTH bs                      by j <= LENGTH bs

   Otherwise 0 < j.
     Let (u,v,w) = EL (j - 1) ls.
     FUNPOW (hopping (SQRT n)) j (HD ls)
   = hopping (SQRT n) (EL (FST (EL (j - 1) bs)) ls)    by hopping_sqrt_funpow, 0 < j
   = hopping (SQRT n) (EL u ls)
   = EL w ls                                           by blocks_path_third_by_hopping
     Also, w <= k                                      by blocks_path_element
     Pick this w.
*)
Theorem hopping_sqrt_funpow_at_blocks_third:
  !n k j. let ls = path n k; bs = blocks (block_pairs ls) in
          tik n /\ ~square n /\ flip (LAST ls) = LAST ls /\ j <= LENGTH bs ==>
          ?w. w <= k /\ FUNPOW (hopping (SQRT n)) j (HD ls) = EL w ls
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `f = hopping (SQRT n)` >>
  `j = 0 \/ 0 < j` by decide_tac >| [
    qexists_tac `0` >>
    simp[blocks_path_eq_nil, Abbr`bs`, Abbr`ls`],
    `j - 1 < LENGTH bs` by decide_tac >>
    `?u v w. EL (j - 1) bs = (u,v,w)` by metis_tac[triple_parts] >>
    `MEM (u,v,w) bs` by metis_tac[EL_MEM] >>
    `w <= k` by metis_tac[blocks_path_element] >>
    qexists_tac `w` >>
    `j - 1 + 1 = j` by decide_tac >>
    `FUNPOW f j (HD ls) = f (EL u ls)` by metis_tac[hopping_sqrt_funpow, FST] >>
    `_ = EL w ls` by metis_tac[blocks_path_third_by_hopping] >>
    simp[]
  ]
QED
(* Not very useful, hopping_sqrt_funpow_eqn is better. *)


(* Idea: for successive blocks (u,v,w), the third index w is monotonic. *)

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in
            flip (LAST ls) = LAST ls /\ j + 1 < LENGTH bs ==>
            SND (SND (EL j bs)) < SND (SND (EL (j + 1) bs)) *)
(* Proof:
   Let ls = path n k,
       bs = blocks (block_pairs ls)
       (u,v,w) = EL (j + 1) bs,
       (uu,vv,ww) = EL j bs.
   The goal is to show: ww < w                 by SND

   Note MEM (u,v,w) bs                         by EL_MEM, j + 1 < LENGTH bs
    and MEM (uu,vv,ww) bs                      by EL_MEM, j < LENGTH bs
   Thus u <= v /\ v < w                        by blocks_path_mem
    but u = ww                                 by blocks_next_element, FST
    ==> ww < w                                 by LESS_EQ_LESS_TRANS
*)
Theorem blocks_path_third_less:
  !n k j. let ls = path n k; bs = blocks (block_pairs ls) in
          flip (LAST ls) = LAST ls /\ j + 1 < LENGTH bs ==>
          SND (SND (EL j bs)) < SND (SND (EL (j + 1) bs))
Proof
  rw_tac std_ss[] >>
  `?u v w. EL (j + 1) bs = (u,v,w)` by metis_tac[triple_parts] >>
  `?uu vv ww. EL j bs = (uu,vv,ww)` by metis_tac[triple_parts] >>
  `j < LENGTH bs` by decide_tac >>
  `MEM (u,v,w) bs /\ MEM (uu,vv,ww) bs` by metis_tac[EL_MEM] >>
  `u <= v /\ v < w` by metis_tac[blocks_path_mem] >>
  `0 < j + 1 /\ (j + 1) - 1 = j` by decide_tac >>
  `u = ww` by metis_tac[blocks_next_element, FST, SND] >>
  fs[]
QED

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in
            flip (LAST ls) = LAST ls /\ j < h /\ h < LENGTH bs ==>
            SND (SND (EL j bs)) < SND (SND (EL h bs)) *)
(* Proof:
   Let d = h - j.
   By induction on d.
   Base: !h j. 0 = h - j /\ j < h /\ h < LENGTH bs ==> SND (SND (EL j bs)) < SND (SND (EL h bs))
      Note 0 = h - j <=> h <= j                by SUB_EQ_0
      Thus h <= j /\ j < h is false, hence true.
   Step: !h j. d = h - j /\ j < h /\ h < LENGTH bs ==> SND (SND (EL j bs)) < SND (SND (EL h bs))
     ==> !h j. SUC d = h - j /\ j < h /\ h < LENGTH bs ==> SND (SND (EL j bs)) < SND (SND (EL h bs))
      Note d = h - j - 1 = h - (j + 1)         by SUB_RIGHT_SUB
       and j < h means j + 1 <= h.
      If j + 1 < h,
           SND (SND (EL j bs))
         < SND (SND (EL (j + 1) bs))           by blocks_path_third_less, j + 1 < h < LENGTH bs
         < SND (SND (EL h bs))                 by induction hypothesis, for h and j+1

      Otherwise j + 1 = h.
           SND (SND (EL j bs))
         < SND (SND (EL h bs))                 by blocks_path_third_less, j + 1 = h < LENGTH bs
*)
Theorem blocks_path_third_monotonic:
  !n k j h. let ls = path n k; bs = blocks (block_pairs ls) in
            flip (LAST ls) = LAST ls /\ j < h /\ h < LENGTH bs ==>
            SND (SND (EL j bs)) < SND (SND (EL h bs))
Proof
  rw_tac std_ss[] >>
  Induct_on `h - j` >-
  simp[] >>
  rpt strip_tac >>
  rename1 `SUC d = _` >>
  `d = h - (j + 1)` by decide_tac >>
  `j + 1 < h \/ j + 1 = h` by decide_tac >| [
    last_x_assum (qspecl_then [`h`, `j+1`] strip_assume_tac) >>
    rfs[] >>
    `j + 1 < LENGTH bs` by decide_tac >>
    `SND (SND (EL j bs)) < SND (SND (EL (j + 1) bs))` by metis_tac[blocks_path_third_less] >>
    fs[],
    metis_tac[blocks_path_third_less]
  ]
QED

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in
            flip (LAST ls) = LAST ls /\ 0 < k /\ j < LENGTH bs ==> 0 < SND (SND (EL j bs)) *)
(* Proof:
   Let ls = path n k,
       bs = blocks (block_pairs ls),
        w = skip_idx_ping ls 1.
   Then HD bs = (0,0,w)                        by blocks_path_head
   Note bs <> []                               by blocks_path_not_nil, 0 < k
     so MEM (0,0,w) bs                         by HEAD_MEM
    ==> 0 < w                                  by blocks_path_mem
     or 0 < SND (SND (EL 0 bs))                by EL, SND

   If j = 0,
      0 < SND (SND (EL 0 bs))                  by above

   Otherwise, 0 < j.
      0 < SND (SND (EL 0 bs))                  by above
        < SND (SND (EL j bs))                  by blocks_path_third_monotonic, 0 < j
*)
Theorem blocks_path_third_pos:
  !n k j. let ls = path n k; bs = blocks (block_pairs ls) in
          flip (LAST ls) = LAST ls /\ 0 < k /\ j < LENGTH bs ==> 0 < SND (SND (EL j bs))
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `w = skip_idx_ping ls 1` >>
  `HD bs = (0,0,w)` by fs[blocks_path_head, Abbr`ls`, Abbr`bs`] >>
  `bs <> []` by fs[blocks_path_not_nil, Abbr`ls`, Abbr`bs`] >>
  `MEM (0,0,w) bs` by metis_tac[HEAD_MEM] >>
  `0 < w` by metis_tac[blocks_path_mem] >>
  `0 < SND (SND (EL 0 bs))` by fs[] >>
  `j = 0 \/ 0 < j` by decide_tac >-
  fs[] >>
  metis_tac[blocks_path_third_monotonic, LESS_TRANS]
QED

(* Idea: iterate of hopping (SQRT n) from (HD (path n k)) reaches LAST only at max. *)

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ ~square n /\
            flip (LAST ls) = LAST ls /\ 0 < j /\ j < LENGTH bs /\ ALL_DISTINCT (TL ls) ==>
            FUNPOW (hopping (SQRT n)) j (HD ls) <> LAST ls *)
(* Proof:
   Let ls = path n k,
       bs = blocks (block_pairs ls),
        t = FUNPOW (hopping (SQRT n)) j (HD ls),
       (u,v,w) = EL (j - 1) ls.
   Note t = EL w ls                            by hopping_sqrt_funpow_eqn
   Thus the goal becomes to show: EL w ls <> LAST ls.

   By contradiction, suppose EL w ls = LAST ls.
   Then EL w ls = EL k ls                      by path_last_alt
    Now 0 < LENGTH bs                          by j < LENGTH bs
     so bs <> []                               by LENGTH_EQ_0
    ==> 0 < k                                  by blocks_path_eq_nil

   Claim: w < k
   Proof: Note     j < LENGTH bs
            so j - 1 < PRE (LENGTH bs)         by PRE_SUB1, 0 < j
          Thus w = SND (SND (EL (j - 1) bs))   by SND
                 < SND (SND (LAST bs))         by blocks_path_third_monotonic, 0 < k
                 = k                           by blocks_path_last

   Claim: w = k
   Proof: Note 0 < w                           by blocks_path_third_pos, 0 < k
          Thus EL (w - 1) (TL ls) = EL (k - 1) (TL ls)
           and LENGTH (TL ls)
             = LENGTH ls - 1                   by LENGTH_TL
             = (k + 1) - 1 = k                 by path_length
           Now w - 1 < k /\ k - 1 < k
          With ALL_DISTINCT (TL ls)            by given
            so w - 1 = k - 1                   by ALL_DISTINCT_EL_IMP
            or w = k

    Thus w = k, contradicting w < k.
*)
Theorem hopping_sqrt_funpow_less:
  !n k j. let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ ~square n /\
          flip (LAST ls) = LAST ls /\ 0 < j /\ j < LENGTH bs /\ ALL_DISTINCT (TL ls) ==>
          FUNPOW (hopping (SQRT n)) j (HD ls) <> LAST ls
Proof
  rw_tac std_ss[] >>
  assume_tac hopping_sqrt_funpow_eqn >>
  last_x_assum (qspecl_then [`n`, `k`, `j-1`] strip_assume_tac) >>
  rfs[] >>
  qabbrev_tac `t = FUNPOW (hopping (SQRT n)) j (HD ls)` >>
  `?u v w. EL (j - 1) bs = (u,v,w)` by metis_tac[triple_parts] >>
  fs[] >>
  spose_not_then strip_assume_tac >>
  `_ = EL k ls` by simp[path_last_alt, Abbr`ls`] >>
  `w < k` by
  (`0 < LENGTH bs /\ j - 1 < PRE (LENGTH bs) /\ PRE (LENGTH bs) < LENGTH bs` by decide_tac >>
  `bs <> [] /\ 0 < k` by metis_tac[blocks_path_eq_nil, LENGTH_EQ_0, NOT_ZERO] >>
  `w < SND (SND (LAST bs))` by metis_tac[blocks_path_third_monotonic, LAST_EL, SND] >>
  metis_tac[blocks_path_last]) >>
  `w = k` by
    (`0 < LENGTH bs /\ j - 1 < LENGTH bs` by decide_tac >>
  `0 < k` by metis_tac[blocks_path_eq_nil, LENGTH_EQ_0, NOT_ZERO] >>
  `0 < w` by metis_tac[blocks_path_third_pos, SND] >>
  `w = SUC (w - 1) /\ k = SUC (k - 1)` by decide_tac >>
  `EL (w - 1) (TL ls) = EL (k - 1) (TL ls)` by metis_tac[EL] >>
  `LENGTH (TL ls) = k` by fs[path_length, LENGTH_TL, Abbr`ls`] >>
  `w - 1 < k /\ k - 1 < k` by decide_tac >>
  metis_tac[ALL_DISTINCT_EL_IMP]) >>
  decide_tac
QED

(* This is very good! *)

(* Idea: iterate of hopping (SQRT n) from (HD (path n k)) less than max is not a flip fix. *)

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ prime n /\
            k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
            0 < j /\ j < LENGTH bs /\ t = FUNPOW (hopping (SQRT n)) j (HD ls) ==> flip t <> t *)
(* Proof:
   Let ls = path n k,
        u = (1,1,n DIV 4),
        p = iterate_period (zagier o flip) u,
        k = 1 + HALF p,
        t = FUNPOW (hopping (SQRT n)) j (HD ls).
   The goal is to show: flip t <> t.

   Note ~square n                              by prime_non_square
    and flip (LAST ls) = LAST ls               by tik_prime_path_last_flip_fix
    and ALL_DISTINCT (TL ls)                   by path_tail_all_distinct
   Thus t <> LAST ls                           by hopping_sqrt_funpow_less
     or t <> EL k ls                           by path_last_alt

   By contradiction, suppose flip t = t.
   Let (u,v,w) = EL (j-1) bs                   by 0 < j
   Then t = EL w ls                            by hopping_sqrt_funpow_eqn
   Note 0 < k                                  by k = 1 + HALF p
     so 0 < w                                  by blocks_path_third_pos, 0 < k
   Also bs <> []                               by blocks_path_not_nil, 0 < k
    and MEM (u,v,w) bs                         by MEM_EL, j - 1 < LENGTH bs
     so w <= k                                 by blocks_path_mem
   Thus w = k                                  by tik_prime_path_flip_fix_iff
     or t = EL k ls, contradicting t <> EL k ls.
*)
Theorem tik_prime_path_hopping_not_flip_fix:
  !n k j t. let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ prime n /\
            k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
            0 < j /\ j < LENGTH bs /\ t = FUNPOW (hopping (SQRT n)) j (HD ls) ==> flip t <> t
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `p = iterate_period (zagier o flip) (1,1,n DIV 4)` >>
  qabbrev_tac `k = 1 + HALF p` >>
  qabbrev_tac `t = FUNPOW (hopping (SQRT n)) j (HD ls)` >>
  `~square n` by simp[prime_non_square] >>
  `flip (LAST ls) = LAST ls` by metis_tac[tik_prime_path_last_flip_fix] >>
  `ALL_DISTINCT (TL ls)` by metis_tac[path_tail_all_distinct] >>
  `t <> EL k ls` by metis_tac[hopping_sqrt_funpow_less, path_last_alt] >>
  `?u v w. EL (j - 1) bs = (u,v,w)` by metis_tac[triple_parts] >>
  `j - 1 < LENGTH bs /\ j - 1 + 1 = j` by decide_tac >>
  `t = EL w ls` by metis_tac[hopping_sqrt_funpow_eqn, SND] >>
  `w <= k` by metis_tac[blocks_path_not_nil, MEM_EL, blocks_path_mem] >>
  `0 < k` by fs[Abbr`k`] >>
  assume_tac blocks_path_third_pos >>
  last_x_assum (qspecl_then [`n`, `k`, `j-1`] strip_assume_tac) >>
  rfs[] >>
  metis_tac[tik_prime_path_flip_fix_iff, ADD_COMM]
QED

(* Finally, the most important result. *)

(* Theorem: let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ prime n /\
            k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
            0 < j /\ j <= LENGTH bs /\ t = FUNPOW (hopping (SQRT n)) j (HD ls) ==>
            (flip t = t <=> j = LENGTH bs) *)
(* Proof:
   Let ls = path n k,
        p = iterate_period (zagier o flip) (1,1,n DIV 4),
        k = 1 + HALF p,
        t = FUNPOW (hopping (SQRT n)) j (HD ls).
   The goal is to show: flip t = t <=> j = LENGTH bs

   If part: flip t = t ==> j = LENGTH bs
      By contradiction, suppose j <> LENGTH bs.
      Then j < LENGTH bs                       by j <= LENGTH bs
       ==> flip t <> t                         by tik_prime_path_hopping_not_flip_fix
      This contradicsts flip t = t.

   Only-if part: j = LENGTH bs ==> flip t = t
      Note ~square n                           by prime_non_square
       and flip (LAST ls) = LAST ls            by tik_prime_path_last_flip_fix
       and t = LAST ls                         by hopping_sqrt_funpow_max
      Thus flip t = t
*)
Theorem tik_prime_path_hopping_flip_fix_iff:
  !n k j t. let ls = path n k; bs = blocks (block_pairs ls) in tik n /\ prime n /\
            k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) /\
            0 < j /\ j <= LENGTH bs /\ t = FUNPOW (hopping (SQRT n)) j (HD ls) ==>
            (flip t = t <=> j = LENGTH bs)
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `p = iterate_period (zagier o flip) (1,1,n DIV 4)` >>
  qabbrev_tac `k = 1 + HALF p` >>
  qabbrev_tac `t = FUNPOW (hopping (SQRT n)) j (HD ls)` >>
  rw[EQ_IMP_THM] >| [
    spose_not_then strip_assume_tac >>
    `j < LENGTH bs` by decide_tac >>
    metis_tac[tik_prime_path_hopping_not_flip_fix],
    `flip (LAST ls) = LAST ls` by metis_tac[tik_prime_path_last_flip_fix] >>
    metis_tac[hopping_sqrt_funpow_max, prime_non_square]
  ]
QED

(* Theorem: let ls = path n k in tik n /\ prime n /\
            k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4))  *)
(* Proof:
   Let ls = path n k,
        p = iterate_period (zagier o flip) (1,1,n DIV 4),
        k = 1 + HALF p,
        g = \t. flip t <> t,
        b = hopping (SQRT n).
   The goal is to show: WHILE g b (HD ls) = LAST ls

   Note (flip (HD ls) = HD ls <=> n = 5        by tik_path_head_flip_fix

   If n = 5,
      Then p = 1                               by tik_iterate_period_eq_1
        so k = 1 + HALF 1 = 1                  by arithmetic
       and ls = path 5 1
              = [(1,5 DIV 4,1); (1,1,5 DIV 4)] by path_1
              = [(1,1,1); (1,1,1)]             by arithmetic
      Also flip (1,1,1) = (1,1,1)              by flip_fix
        so ~g (HD ls)                          by applying g
      Thus WHILE g b (HD ls) = HD ls           by iterate_while_none
                             = LAST ls         by HD, LAST_DEF
   If n <> 5,
   Let bs = blocks (block_pairs ls)
       h = LENGTH bs,
       t = FUNPOW b h (HD ls).
   Note ~square n                              by prime_non_square
    and flip (LAST ls) = LAST ls               by tik_prime_path_last_flip_fix
     so t = LAST ls                            by hopping_sqrt_funpow_max

   The goal becomes: WHILE g b (HD ls) = FUNPOW b h (HD ls)

   By iterate_while_thm, this is to show:
   (1) !j. j < h ==> g (FUNPOW b j (HD ls))
       If j = 0,
          Then FUNPOW b 0 (HD ls) = HD ls      by FUNPOW_0
           and flip (HD ls) <> HD ls           by tik_path_head_flip_fix
            so g (HD ls)                       by applying g
       Otherwise, 0 < j, this is true          by tik_prime_path_hopping_not_flip_fix
   (2) ~g (FUNPOW b h (HD ls))
       This is true                            by tik_prime_path_last_flip_fix
*)
Theorem tik_prime_path_hopping_while_thm:
  !n k. let ls = path n k in tik n /\ prime n /\
        k = 1 + HALF (iterate_period (zagier o flip) (1,1,n DIV 4)) ==>
        WHILE (\t. flip t <> t) (hopping (SQRT n)) (HD ls) = LAST ls
Proof
  rw_tac std_ss[] >>
  qabbrev_tac `p = iterate_period (zagier o flip) (1,1,n DIV 4)` >>
  qabbrev_tac `k = 1 + HALF p` >>
  qabbrev_tac `g = \t. flip t <> t` >>
  qabbrev_tac `b = hopping (SQRT n)` >>
  qabbrev_tac `bs = blocks (block_pairs ls)` >>
  qabbrev_tac `h = LENGTH bs` >>
  qabbrev_tac `t = FUNPOW b h (HD ls)` >>
  `~square n` by simp[prime_non_square] >>
  `flip (LAST ls) = LAST ls` by metis_tac[tik_prime_path_last_flip_fix] >>
  `LAST ls = t` by metis_tac[hopping_sqrt_funpow_max] >>
  simp[Abbr`t`] >>
  Cases_on `n = 5` >| [
    `p = 1` by fs[tik_iterate_period_eq_1, Abbr`p`] >>
    `k = 1` by fs[Abbr`k`] >>
    `ls = [(1,1,1); (1,1,1)]` by rfs[path_1, Abbr`ls`] >>
    `HD ls = LAST ls` by metis_tac[HD, LAST_DEF] >>
    `~g (1,1,1)` by simp[flip_fix, Abbr`g`] >>
    rfs[iterate_while_none],
    irule iterate_while_thm >>
    rw[Abbr`g`] >| [
      `j = 0 \/ 0 < j` by decide_tac >| [
        simp[] >>
        metis_tac[tik_path_head_flip_fix],
        metis_tac[tik_prime_path_hopping_not_flip_fix]
      ],
      metis_tac[tik_prime_path_last_flip_fix]
    ]
  ]
QED

(* The most desired theorem. *)

(* Theorem: tik n /\ prime n ==> two_sq_hop n IN fixes flip (mills n) *)
(* Proof:
   Let s = mills n,
       f = hopping (SQRT n),
       u = (1,1,n DIV 4),
       v = (1,n DIV 4,1).
   By two_sq_hop_def, this is to show: WHILE ($~ o found) f v IN fixes flip s

   Note (~) o found = (\t. flip t <> t)        by found_def, flip_def, FUN_EQ_THM
   Thus the goal is: WHILE (\t. flip t <> t) f v IN fixes flip s

   Let p = iterate_period (zagier o flip) u,
       k = 1 + HALF p,
       ls = path n k.
   Then HD ls = v                              by path_head
    and WHILE (\t. flip t <> t) f v = LAST ls  by tik_prime_path_hopping_while_thm

    Now LAST ls = EL k ls                      by path_last_alt
     so LAST ls IN s                           by path_element_in_mills
    and flip (LAST ls) = LAST ls               by tik_prime_path_last_flip_fix
    ==> LAST ls IN fixes flip s                by flip_fixes_alt
*)
Theorem two_sq_hop_thm:
  !n k. tik n /\ prime n ==> two_sq_hop n IN fixes flip (mills n)
Proof
  rw[two_sq_hop_def] >>
  qabbrev_tac `s = mills n` >>
  qabbrev_tac `f = hopping (SQRT n)` >>
  qabbrev_tac `u = (1,1,n DIV 4)` >>
  qabbrev_tac `v = (1,n DIV 4,1)` >>
  `(~) o found = \t. flip t <> t` by
  (rw[FUN_EQ_THM] >>
  `?x y z. t = (x,y,z)` by rw[triple_parts] >>
  simp[found_def, flip_def]) >>
  simp[] >>
  qabbrev_tac `p = iterate_period (zagier o flip) u` >>
  qabbrev_tac `k = 1 + HALF p` >>
  qabbrev_tac `ls = path n k` >>
  `HD ls = v` by fs[path_head, Abbr`ls`, Abbr`v`] >>
  `LAST ls = EL k ls` by simp[path_last_alt, Abbr`ls`] >>
  `LAST ls IN s` by fs[path_element_in_mills, Abbr`ls`, Abbr`s`] >>
  `WHILE (\t. flip t <> t) f v = LAST ls` by metis_tac[tik_prime_path_hopping_while_thm] >>
  `flip (LAST ls) = LAST ls` by metis_tac[tik_prime_path_last_flip_fix] >>
  rfs[flip_fixes_alt]
QED

(* Finally!! This shows the correctness of the hopping algorithm.  *)

(* Extract the two squares of hopping algorithm. *)
Definition two_squares_hop_def:
    two_squares_hop n = let (x,y,z) = two_sq_hop n in (x, y + z)
End

(*
> EVAL ``two_squares_hop 5``; = (1,2)
> EVAL ``two_squares_hop 13``; = (3,2)
> EVAL ``two_squares_hop 17``; = (1,4)
> EVAL ``two_squares_hop 29``; = (5,2)
> EVAL ``two_squares_hop 97``;  = (9,4)
> EVAL ``MAP two_squares_hop [5; 13; 17; 29; 37; 41; 53; 61; 73; 89; 97]``;
= [(1,2); (3,2); (1,4); (5,2); (1,6); (5,4); (7,2); (5,6); (3,8); (5,8); (9,4)]: thm
*)

(* Theorem: tik n /\ prime n ==> let (u,v) = two_squares_hop n in (n = u ** 2 + v ** 2) *)
(* Proof:
   Let t = two_sq_hop n.
   Then t IN fixes flip (mills n)        by two_sq_hop_thm
    and ?x y z. t = (x,y,z)              by triple_parts
    ==> (x,y,z) IN mills n /\ (y = z)    by flip_fixes_alt, flip_fix
     so n = windmill (x, y, y)           by mills_element
          = x ** 2 + (2 * y) ** 2        by windmill_by_squares
          = x ** 2 + (y + z) ** 2        by y = z
          = u ** 2 + v ** 2              by two_squares_hop_def
*)
Theorem two_squares_hop_thm:
  !n. tik n /\ prime n ==> let (u,v) = two_squares_hop n in (n = u ** 2 + v ** 2)
Proof
  rw[two_squares_hop_def] >>
  qabbrev_tac `t = two_sq_hop n` >>
  `t IN fixes flip (mills n)` by fs[two_sq_hop_thm, Abbr`t`] >>
  `?x y z. t = (x,y,z)` by rw[triple_parts] >>
  `(x,y,z) IN mills n /\ (y = z)` by fs[flip_fixes_alt, flip_fix] >>
  `n = windmill (x, y, y)` by fs[mills_element] >>
  simp[windmill_by_squares]
QED

(*
Note: the algorithm works for tik n /\ ~square n:

> EVAL ``two_sq_hop 65``; = (1,4,4)
> EVAL ``two_squares_hop 65``; = (1,8)

These have more than one zagier-fix, each corresponds to a flip-fix.
*)

(* ------------------------------------------------------------------------- *)

(* export theory at end *)
val _ = export_theory();

(*===========================================================================*)
