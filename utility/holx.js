//
// holx -- A cross-check utility for HOL scripts
//
// Author: Joseph Chan
// modify this file at location ~/work/project/utility/holx.js
// always copy file at location to ~/jc/bin/holx.js for execution.
//        cp -p holx.js ~/jc/bin/
//
// Note: this script is location independent.
//

// var debug = true;
var debug = false;

//requiring path and fs modules
const path = require('path');
const fs = require('fs');

// get home directory (Mac OS)
const homedir = process.env.HOME;
if (debug) console.log("home: " + homedir);

//joining path of directory 
// const directoryPath = path.join(__dirname, 'Documents');
const directoryPath = path.join(homedir, 'work/hol');

// get all files in directory
function getAllFiles (dirPath) {
	console.log("dirPath: " + dirPath);
    var files = fs.readdirSync(dirPath).filter(function(x) {
    	console.log(x + ' type: ' + typeof(x) + ' end? ' + (x.indexOf('E.md') !== -1));
    return x !== 'Holmakefile' && x !== 'temp.js';
});
    //listing all files using forEach
    files.forEach(function (file) {
        // Do whatever you want to do with the file
        var line = '' + file;
        // if (file.endsWith('.md')) console.log('MD!');
        var stats = fs.statSync(dirPath + "/" + file);
        if (!stats.isDirectory()) line += "\tsize: " + stats.size;
        console.log(line);
    });	
}

// get script size in directory from home
function getScripts (dirPath, folders) {
    if (debug) console.log("getScripts: dirPath: " + dirPath);
	// go through the folders
    folders.forEach(function (folder) {
        var path = homedir + '/' + dirPath + '/' + folder;
        var files = fs.readdirSync(path).filter(function(fn) {
            return fn.indexOf('Script.sml') !== -1;
        });
        // go through the files
        files.forEach(function (file) {
            var stats = fs.statSync(path + '/' + file);
            var line = dirPath + '/' + file + '\tsize: ' + stats.size;
            console.log(line);
        });	
    });
}
// testing
// getScripts('work/hol/algebra', ['monoid', 'lib']);

// difference of two numbers
function diff(x, y) {
    return Math.abs(y - x);
}

// padding left
String.prototype.padL = function(pad) {
    return String(pad + this).slice(-pad.length);
};
// padding right
String.prototype.padR = function(pad) {
    return String(this + pad).slice(0, pad.length);
};

// compare script size in folders for two directories
function compareScripts(dirPath1, dirPath2) {
    if (debug) console.log("path1: " + dirPath1);
    if (debug) console.log("path2: " + dirPath2);
    console.log('HOL scripts compare: ' + dirPath1 + '\t' + dirPath2);
    var folders = fs.readdirSync(homedir + '/' + dirPath1).filter(function(fn) {
    	return fs.statSync(homedir + '/' + dirPath1 + '/' + fn).isDirectory();
    }).sort();
    // go through the folders    
    // folders.forEach(function(folder) {
    folders.forEach(function(folder) {
    	if (debug) console.log('folder: ' + folder);
        var path1 = homedir + '/' + dirPath1 + '/' + folder;
        var path2 = homedir + '/' + dirPath2 + '/' + folder;
        var files = fs.readdirSync(path1).filter(function(fn) {
            return fn.indexOf('Script.sml') !== -1; // has *Script.sml
        }).sort();
        // prepare the result
        var result = [];
        // go through the files
        files.forEach(function(file) {
            // get file content
            var file1 = fs.readFileSync(path1 + '/' + file, 'utf8');
            var file2 = '';
            try {
               file2 = fs.readFileSync(path2 + '/' + file, 'utf8');
            }
            catch(err) {
               file2 = file1;
               console.log('Missing: ~/' + dirPath2 + '/' + folder + '/' + file);
            }
            if (debug) {
                var line = dirPath1 + '/' + file + '\tsize: ' + file1.length + '\n' +
                           dirPath2 + '/' + file + '\tsize: ' + file2.length;
                console.log(line);
            }
            if (file1 !== file2) {
                var line = '\t' + file.substring(0,file.indexOf('Script.sml'))
                           .padR('                    ');  // length = 20
                line += ('' + file1.length).padL('        ') +
                        ('' + file2.length).padL('        ');
                result.push(line);
            }
        });
        console.log (folder + ':' + (result.length == 0 ? ' ok' : '\n' + result.join('\n')));
    });
}

// use: node holx.js
// run the main program
compareScripts('work/hol/algebra', 'HOL/examples/algebra');
compareScripts('work/hol/algorithm', 'HOL/examples/simple_complexity');
compareScripts('work/hol/aks', 'HOL/examples/AKS');
compareScripts('work/project/fermat', 'HOL/examples/fermat');

