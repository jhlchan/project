/*
 * A Step Proof Formatter for HOL, by Joseph Chan.
 *
 * The Idea
 * . Source lines (semi-structured) --> Body lines (structured)
 * . Body lines (structured) --> Tree (an array of array of lines)
 * . Tree (an array of array of lines) --> Formatted lines (ready for one long line by simple join)
 *
 * Note:
 * . The Body lines are like a mini-language to produce the Tree.
 * . Mostly, it is just the inside of e (whatever-is-inside) --> whatever-is-inside
 * . However, it also takes hints in comment: (* >> *)  --> >>2, (* >> 5 *) --> >>5, (* << *) --> <<
 * . To transform away subgoal:
 * . . add special lines *sg* for Body
 * . . modify the Tree to output prefix ! and suffix !
 * . . parse() will format by taking care of prefix or suffix to merge lines
 *     
 * The algorithm
 * . Given a source: string, the main program is: beautify(source)
 * . beautiy(source) looks at all source lines, at the end, it does:
 *     return defn.length == 0 ? proof_text(name, goal, convert(body)) :
 *                               definition_text(name, defn, convert(body))
 * . In words, beautiy(source) performs:
 * . . first identifies: a name, a goal or definition, and a body,
 * . . then it performs: convert(body),
 * . . then combines the result with attributes depending on proof or definition.
 * . Note that name: string, goal: string, definition: string, body: string[]
 * .  
 * . convert(body) returns parse(noSubgoal(tree(body), ''), get_tab())
 * . Here get_tab() just gives the tab: string.
 * . tree(body) converts the body, a stack of lines, into a tree
 * . noSubgoal(tree, '') returns a tree that linearizes subgoals
 * . parse(tree, tab) converts the tree to a stack of formatted lines (display by simple join)
 *
 * Features:
 * 16/10/18: Add decorator, see proof_text(name, goal, decorator, body)
 * 22/11/19: Add new style: Theorem/Proof/QED.
 *
 * Notes:
 * . Definition processing should precede other line-specific processing.
 * . After any modification, always check with: node ProofFormatter.js test
 */

// <!-- Debugger -->
var debug = false // true // false;
var dummy = function(s) {} 
var log = dummy // by default
/*
if (debug) {
    document.write('<textarea id="debugger" cols="100%" rows="10">Debugger</textarea>')
    document.getElementById("debugger").value = 'Debugger'
    log = function(s) {document.getElementById("debugger").value += "\r\n" + s;}
}
*/

// Add an element to the document
function addElement(parentId, elementTag, elementId, html) {
    var p = document.getElementById(parentId);
    var newElement = document.createElement(elementTag);
    newElement.setAttribute('id', elementId);
    newElement.innerHTML = html;
    p.appendChild(newElement);
}

// Remove an element from the document
function removeElement(elementId) {
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}
        
// set debug flag
function setDebug() {
    debug = document.getElementById('debug').checked
    if (debug) {
        addElement("textarea", "textarea", "debugger", 'Debugger')
        log = function(s) {document.getElementById("debugger").value += "\r\n" + s;}
    }
    else {
        log = dummy
        removeElement("debugger")
    }    
}

// show an array of line
function showArray(a) {
   var b = []
   b.push('length=' + a.length)
   b.push('"' + a.join('"\n"') + '"')
   return b.join('\n')
}

// show an array of line, with tab indentation
function showDeepArray(a, tab) {
   var b = []
   b.push(tab + 'length=' + a.length)
   for (var j = 0; j < a.length; j++) {
      if (typeof a[j] == 'string') b.push(tab + '"' + a[j] + '"')
      else b.push(showDeepArray(a[j], tab + get_tab()))
   }
   return b.join('\n')
}

// String Prototype extension -----------------------------------------

// trim the leading whitespaces of String
String.prototype.trim = function() {
    return this.replace(/^\s+/, '')
}

// startsWith
String.prototype.startsWith = function(part) {
    if (part == null || part == '') return true
    return this.indexOf(part) == 0
}

// endsWith
String.prototype.endsWith = function(part) {
    if (part == null || part == '') return true
    if (this.indexOf(part) == -1) return false
    return this.lastIndexOf(part)+part.length == this.length
}

// prefix with char before leading spaces, keeping length
// regular expression pattern: <lead-spaces><one-space><the-rest>
String.prototype.prefix = function(ch) {
    return this.replace(/(^\s+) (.+)/, "$1"+ch+"$2")
}

// abbreviate the string to some length (set to 20)
String.prototype.abbr = function() {
    if (this.length < 20) return this
    else return this.slice(0,17)+'...'
}

// get a number from string, default is d
function get_num(s, d) {
    var n = parseInt(s)
    if (isNaN(n)) n = d
    return n
}

// global options
var options = {
    indent_size: 2,
    indent_char: ' ',
    smart_then1: true,
    always_then1: false,
    new_style: true,
    store_thm: false,
    prove_thm: false,
    lcsymtacs: true,
    nolcsymtacs: false,
    subgoal: false,
    nosubgoal: true,
    dummy: ''
}

// get the tab string
function get_tab() {
    if (options.indent_size == 1) return options.indent_char
    var t = options.indent_char
    while (t.length < options.indent_size) t += options.indent_char
    return t
}

/* Note: body is a list of standardized lines.
source lines may be:
   e (....
          ...); (* >> 3 *)
   (* comment *)
   e (decide_tac); (* << *)
body lines are:
['........',        <-- mutli-line converts to single lone
 '>>3'              <-- hints are special markers
 'decide_tac',      <-- comments are eliminated
 '<<',              <-- another hint, another marker
]

Example:
body: length=8
"rpt strip_tac"
"rw[RingEndo_def, RingHomo_def]"
">>2"
"rw[GroupHomo_def]"
"metis_tac[ring_freshman_thm]"
"<<"
"rw[MonoidHomo_def, ring_mult_monoid]"
"<<"
tree: length=3
"rpt strip_tac"
"rw[RingEndo_def, RingHomo_def]"
  length=2
    length=2
    "rw[GroupHomo_def]"
    "metis_tac[ring_freshman_thm]"
    length=1
    "rw[MonoidHomo_def, ring_mult_monoid]"
           
*/
// convert the body into a tree
function tree(body) {
    // get a line from body
    function get_line() {
        if (body.length == 0) return '<>' // end-of-proof
        return body.shift()
    }
    // build a branch by getting lines until token tok
    function branch(tok) {
        var b = [] // subtree for branch
        var s = ''
        while (true) {
            s = get_line()
            if (s == '<>') break // end marker, done
            if (s == tok) break  // end token, done
            if (s.startsWith('>>')) { // a hint
                // get number of branches from hint
                var last = parseInt(s.slice(2))
                var t = []
                for (var j = 0; j < last; j++) {
                    // get whole branch to << and put to t
                    t.push(branch('<<'))
                }
                // t has all the branches, put to subtree
                b.push(t)
            }
            else // s is just a normal line of text, put to subtree
                b.push(s)
        }
        return b
    }
    // main of tree: build a branch until end-of-proof
    return branch('<>')
}

/*
  THEN   --->    >>
  THENL  --->    >|
  THEN1  --->    >-
*/

var THEN, THENL, THEN1

// set the keywords
function setKeywords(flag) {
  if (flag) {
     THEN = '>>'
     THENL = '>|'
     THEN1 = '>-'
  }
  else {
     THEN = 'THEN'
     THENL = 'THENL'
     THEN1 = 'THEN1'
  }
}

// add tab to a string
function addTab(tab, s, err) {
	 if (typeof s !== 'string')
	    throw 'TAB add to non-string! @' + err + ' (check balance of >> and <<)'
    return tab + s
}

// peek the last of an array
function peek(a) {
    return a[a.length-1]
}

// merge one array into another
function merge(a, b, tab) {
    // while (b.length > 0)  a.push(tab+b.shift())
    while (b.length > 0)  a.push(addTab(tab, b.shift(), 'A'))
    return a
}

/*
Note: parse is simple if tree is just a list of strings.
Then it is a matter of formatting them into  tab + string + ' ' + THEN

*/
// parse the tree with tab
function parse(tree, tab) {
    var b = []
    //log('Type of tree ='+(typeof tree))
    var s = tree.shift() // this line
    var t = ''
    while (tree.length > 0) {
        t = tree.shift() // next line
        if (typeof t == 'string') {
            // if (s != '') b.push(tab+s+ ' ' + THEN)
            if (t.startsWith('!')) {
                // next is prefix, merge with this line
                s += t.slice(1)
                t = '' // t has been merged
            }
            if (s.endsWith('!')) {
                // this line is suffix, merge with next line
                t = s.slice(0,-1) + t
                s = '' // s has been merged
            }
            if (s != '') b.push(addTab(tab, s, 'B')+ ' ' + THEN)
            s = t
        }
        // now t is a branch
        else if (options.always_then1 ||
                 (options.smart_then1 && (t[0].length == 1))) { // branching with THEN1
            // b.push(tab+s+ ' '+THEN1)
            b.push(addTab(tab, s, 'C')+ ' '+THEN1)
            while (t.length > 0) {
                var c = parse(t.shift(), tab)
                // wrap c with () when more than 1 statement
                if (c.length > 1 && peek(b).endsWith(THEN1)) {
                    c.unshift(c.shift().prefix('('))
                    c.push(c.pop()+')')
                }
                merge(b, c,'') // no tab for THEN1 on merge
                if (t.length > 0) {
                    if (t.length == 1) b.push(b.pop()+' '+THEN)
                    else b.push(b.pop()+' ' +THEN1)
                }
            }
            s = '' // for next insert
        }
        else { // branching with THENL
            // b.push(tab+s+ ' '+THENL+' [')
            b.push(addTab(tab, s, 'D')+ ' '+THENL+' [')
            while (t.length > 0) {
                merge(b, parse(t.shift(), tab), tab)
                if (t.length > 0) b.push(b.pop()+',')
            }
            s = ']' // for next insert
        }
    }
    // if (s != '') b.push(tab+s)
    if (s != '') b.push(addTab(tab, s, 'E'))
    return b
}

// eliminate subgoal marks from tree with tab
function noSubgoal(tree, tab) {
    var tb = tab + get_tab()
    var b = []
    var s
    while (tree.length > 0) {
        s = tree.shift() // next line
        if (typeof s !== 'string') {
           s = noSubgoal(s, '') // ensure array s has no subgoal
           b.push(s)
        }   
        else if (s == '*sg*') {
           s = tree.shift() // next is goal with hanging by
           var t = tree.shift() // next is a two-way branch
           if (t.length !== 2) throw 'Subgoal with more than 2 branches!'
           var b1 = t[0] // first branch
           var b2 = t[1] // second branch
           // ensure b1 has no subgoal
           b1 = noSubgoal(b1, tb)
           // b.push(s + '(' + b1.join(' ' + THEN + '\n') + ')') // complete hanging by
           // complete hanging by with brackets () and joining by THEN
/* Debug of b1           
           if (typeof peek(b1) !== 'string') {
               log('>>>> Array b1')
               log(showDeepArray(b1, ''))
               log('<<<< Array b1')
               // throw 'b1 item is not string!' + b1.length
           } 
*/
           b.push(s + '\n' + tb + '(!') // use a suffix marker ! here
           b = b.concat(b1)
           b.push('!)') // use a prefix marker ! here
           // ensure b2 has no subgoal
           b2 = noSubgoal(b2, tb)
           b = b.concat(b2) // tuck in the transform
        }
        else b.push(s)
    }
    return b
}

// convert body into a proof tree then parse it
function convert(body) {
    if (debug) log('Convert(body):')
    var t = tree(body)
    if (debug) log('tree: ' + showDeepArray(t, ''))
    if (debug) log('nosubgoal: ' + options.nosubgoal)
    if (options.nosubgoal) t = noSubgoal(t, '')
    var p = parse(t, get_tab())
    if (debug) log('parse: ' + showArray(p))
    return p
    // return parse(tree(body), get_tab())
}

// generate the formatted proof
function proof_text(name, goal, decorator, body) {
    var tab = get_tab()
    var text = [] // proof text
    if (options.new_style) {
          text.push('Theorem '+name+decorator+':')
          text.push(tab + goal.slice(1,-1)) // remove quotes around goal
          text.push('Proof')
          merge(text, body, '') // no tab
          text.push('QED')
    }
    else {
       if (options.store_thm) {
          text.push('val '+name+' = store_thm(')
          text.push(tab+'"'+name+decorator+'",')
       }
       else if (options.prove_thm) {
          text.push('val '+name+' = prove(')
       }
       text.push(tab+'`'+goal+'`,')
       merge(text, body, '') // no tab
       text.push(text.pop()+');') // last line to close bracket
    }
    return text.join('\n')
}

// generate the formatted definition
function definition_text(name, defn, body) {
    var tab = get_tab()
    var text = [] // definition text
    // first line of tDefine
    text.push(defn[0].replace('Define', 'tDefine "' + name + '" '))
    // other lines except last of tDefine
    for (var j = 1; j < defn.length - 1; j++) {
       text.push(tab + defn[j])
    }
    // replace last line (defn.length[-1]) by a standard one, with open bracket
    text.push('`(')
    merge(text, body, '') // no tab
    text.push(text.pop()+');') // last line to close bracket
    return text.join('\n')    
}

// formatting the source
function beautify(source) {
    var lines = source.split('\n')
    var name = 'SOME_THEOREM'
    var goal = 'goal'
    var decorator = '' // no decorator
    var body = [] // body of proof
    var defn = [] // body of definition
    var inDefine = false

    // get name from line s
    function get_name(s) {
        // line: (* <<name_of_theorem>> *)
        var h = s.indexOf('<<')
        var k = s.indexOf('>>')
        return s.slice(h+2,k)
    }

    // get goal from line s
    function get_goal(s) {
        // line: g `statement-of-theorem`;
        var h = s.indexOf('`')
        var k = s.lastIndexOf('`')
        return s.slice(h,k+1)
    }

    // count token pairs in a line
    function pair(s, c, tok1, tok2) {
       var e
       for (var j = 0; j < s.length; j++) {
          e = s.charAt(j)
          if (tok1 == tok2) { // same token is a toggle
             if (e == tok1) if (c == 0) c++; else c--
          }
          else { // different tokens can be nested      
             if (e == tok1) c++
             if (e == tok2) c--
          }
          if (c < 0) throw 'Too many ' + tok2 + ' for ' + tok1
       }
       return c
    }
    
    // complete a partial line s with tok from lines
    function complete(s, tok) {
        // e.g. goal on multi-line:
        // g `statement-of-                <-- initial line
        //        some-very-long-thoerem`;  <-- look for ending ';'
        // e.g. e on multi-line:
        // e (expression-across-            <-- initial line
        //        many-many-lines);  <-- look for ending ';'
        // Use balanced brackets to ensure complete.
        var c0 = pair(s, 0, '`', '`') // counts of back-quotes 
        var c1 = pair(s, 0, '(', ')') // counts of brackets 
        var c2 = pair(s, 0, '[', ']') // count of square-brackets
        var c3 = pair(s, 0, '{', '}') // count of braces
        var k = s.lastIndexOf(tok)
        var t
        while ((k == -1) || ((c0 + c1 + c2 + c3) !== 0)) { // append s until ending tok
            t = lines.shift()
            s += '\n' + t
            c0 = pair(t, c0, '`', '`')
            c1 = pair(t, c1, '(', ')')
            c2 = pair(t, c2, '[', ']')
            c3 = pair(t, c3, '{', '}')
            k = t.lastIndexOf(tok)
        }
        // console.log('complete s: [' + s + '], k = ' + k + ', c = ' + (c0 + c1 + c2 + c3))
        return s
    }

    // skip a nested structure
    function skipNested(s, btok, etok) {
        // scan a string for btok to increment level, etok to decrement level
        function scanTokens(s, btok, etok, level) {
            var t
            for (var k = 0; k < s.length; k++) {
               t = s.substring(k)
               if (t.indexOf(btok) == 0) level++
               if (t.indexOf(etok) == 0) level--
            }
            return level 
        }
        // e.g. skip nested comment, btok = (*  etok = *)
        var level = 0
        level = scanTokens(s, btok, etok, level)
        var t
        while (level != 0) { // append lines until balance
            t = lines.shift()
            s += '\n' + t
            level = scanTokens(t, btok, etok, level)
        }
        return s
    }
    
    // a stack to keep track of branches
    var stack = []

    // process all source lines
    while (lines.length > 0) {
        var s = lines.shift().trim()
        if (s == '') {
            // skip, e.g. empty proof hint
        }
        else if (s.startsWith('(* <>')) {
            // end of proof
            body.push('<>')
            lines.length = 0 // force exit
        }
        else if (s.startsWith('(* <<') && s.indexOf('>> *)') > 0) {
            // get name in (* <<name>> *)
            name = get_name(s)
        }
        else if (s.startsWith('(* >>')) {
            // open branch (from edit hint)
            var k = get_num(s.slice(5),2) // how many branches, default = 2
            if (debug) log('Open branch: '+k)
            body.push('>>' + k) // put a mark in body            
            stack.push(k) // remember there are k branches
        }
        else if (s.startsWith('(* <<')) {
            // close branch (from edit hint)
            if (stack.length > 0) body.push('<<') // put a mark in body
            var more = true // assume more to close
            while (stack.length > 0 && more) {
                var k = stack.pop() // recall number of branches
                k-- // one is closed
                if (debug) log('Close branch: k='+k)
                if (k > 0) {
                    stack.push(k) // update number of remaining branches
                    more = false // that's it, exit while
                }
                else if (stack.length > 0) { // k = 0 but still more on stack
                    body.push('<<') // extra for k = 0
                    if (debug) log('Extra close')
                }
            }
        }
        // extracts decorator: (* ++decorator++ *)
        else if (s.startsWith('(* ++')) {
            var j = s.indexOf('++') + 2
            var k = s.indexOf('++', j)
            decorator = s.slice(j, k)
        }        
        // check definition first before others, as line is trimmed.
        else if (s.startsWith('val ')) {
            if (s.indexOf('Define') == 0) throw 'Wrong format for step proof.'
            inDefine = true
            defn.push(s) // collect definition lines
        }
        else if (inDefine) {
            defn.push(s) // keep collecting lines for definition
            inDefine = !s.endsWith(';') // semicolon marks definition end
        }
        // end of definition check, proceed for non-definition lines.
        else if (s.startsWith('(*')) {
            // skip comment
            s = skipNested(s, '(*', '*)')
            //log('comment='+s)
            // comment means no body.push()
        }
        else if (s.startsWith('g')) {
            // get goal
            goal = get_goal(complete(s, ';')) // get a complete goal line
        }
        else if (s.startsWith('e')) {
            // edit processing
            s = complete(s, ';') // get a complete edit line
            var h = s.indexOf('(')
            var k = s.lastIndexOf(');')
            // check () wrapping before push
            var t = checkWrap(s.slice(h+1,k))
            // check subgoal
            if (options.nosubgoal && (t.startsWith('sg'))) {
               body.push('*sg*') // a subgoal mark to be eliminated
               body.push(get_goal(t) + ' by') // a hanging 'by'
            }
            else body.push(t)
            // put hint after ; back to source lines, can be ''
            lines.unshift(s.slice(k+2).trim())
        }
        else if (s.startsWith('b')) {
            // backup
            do {
                var x = body.pop()
                if (debug) log('Backup: '+x)
                if (x.startsWith('>>')) {
                    stack.pop()
                    if (debug) log('Stack popped')
                }
            } while (x == '<<' || x.startsWith('>>'))
        }
        else if (s.startsWith('restart')) {
            // restart -- discard lines on body
            while (body.length > 0) body.pop()
        }
        else if (s.startsWith('p')) {
            // print current goal -- skip
        }
        else if (s.startsWith('drop()')) {
            // drop goal -- ignore
        }
        else throw 'Error line: ' + s            
    }
    // Here, these are ready: name, goal, and body; or defn and body
    if (debug) {
        log('Debug:')
        log('name: ' + name)
        log('goal: ' + goal)
        log('decorator: ' + decorator)
        log('defn: ' + defn)
        log('body: ' + showArray(body))
    }
    // do something with the name, goal, and body; or defn and body
    return defn.length == 0 ? proof_text(name, goal, decorator, convert(body)) :
                              definition_text(name, defn, convert(body))
}

// check if line needs wrapping
function checkWrap(line) {
    // Wrap () when line contains THEN (or THEN1)
    // Use line.replace(/`(.*)`/, '') to ignore symbols within `...`
    if (line.replace(/`(.*)`/, '').indexOf(THEN) == -1) return line
    return '(' + line + ')'
}

// main program
function run() {
    // disable formatter
    document.getElementById('formatter').disabled = true
    // update options
    options.indent_size = document.getElementById('tabsize').value
    options.smart_then1 = document.getElementById('smart_then1').checked
    options.always_then1 = document.getElementById('always_then1').checked
    options.new_style = document.getElementById('new_style').checked
    options.store_thm = document.getElementById('store_thm').checked
    options.prove_thm = document.getElementById('prove_thm').checked
    options.lcsymtacs = document.getElementById('lcsymtacs').checked
    options.nolcsymtacs = document.getElementById('nolcsymtacs').checked
    setKeywords(options.lcsymtacs)
    options.subgoal = document.getElementById('subgoal').checked
    options.nosubgoal = document.getElementById('nosubgoal').checked
    // debug checks are handled by onclick
    try {
        // get source and apply formatting
        var source = document.getElementById('content').value
        document.getElementById('content').value = beautify(source)
    }
    catch (error) {
        alert(error)
        //throw error
    }
    // enable formatter
    document.getElementById('formatter').disabled = false
    return false
}

// example database and index
var examples = []
//var example_index = 0
var example_index = 3 // start with example 3

// get example to content (index from 1)
function get_example(n) {
    example_index = n || example_index // get n if specified
    if (debug) example_index = examples.length // always run last example
    if (debug) log('Running example '+ example_index)
    document.getElementById('content').value = examples[example_index - 1] // array from 0
    example_index++ // ready for next example
    if (example_index > examples.length) example_index = 1
    // and run it if debug
    if (debug) document.getElementById('formatter').onclick()
}

// results of examples
var results = []

// Example #1 -- simple example
examples.push('\
(* Paste in your HOL step-proof and press "Run Formatter" button. *)\n\
(* <<Pythagorean_Triple>> *)\n\
g `?a b c. a*a + b*b = c*c`;\n\
e (MAP_EVERY Q.EXISTS_TAC [`3`,`4`,`5`]);\n\
e (RW_TAC arith_ss []);\
')
results.push('\
val Pythagorean_Triple = store_thm(\n\
  "Pythagorean_Triple",\n\
  ``?a b c. a*a + b*b = c*c``,\n\
  MAP_EVERY Q.EXISTS_TAC [`3`,`4`,`5`] >>\n\
  RW_TAC arith_ss []);\
')

// Example #2 -- goal on multiple lines
examples.push("\
(* <<ORBIT_STABILIZER_THM>> *)\n\
g `!f g X a. FiniteGroup g /\\ Action f g X /\\ a IN X /\\ FINITE X ==>\n\
   (CARD g.carrier = CARD (orbit f g X a) * CARD (stabilizer f g a))`;\n\
e (SRW_TAC [][FiniteGroup_def]);\n\
e (`StabilizerGroup f g a <= g`\n\
     by METIS_TAC [StabilizerGroup_subgroup]);\n\
e (`(StabilizerGroup f g a).carrier = stabilizer f g a`\n\
     by SRW_TAC [][StabilizerGroup_def]);\n\
e (`FINITE (stabilizer f g a)`\n\
     by METIS_TAC [Subgroup_def, SUBSET_FINITE]);\n\
e (`BIJ (\\x. lcoset g (stabilizer f g a) (@z. z IN g.carrier /\\ (f z a = x)))\n\
   (orbit f g X a) (partition (sreach (StabilizerGroup f g a)) g.carrier)`\n\
     by SRW_TAC [][ORBIT_STABILIZER_MAP_BIJ]);\n\
e (`FINITE (partition (sreach (StabilizerGroup f g a)) g.carrier)`\n\
     by METIS_TAC [FINITE_partition, SUBGROUP_REACH_EQUIV_ON_GROUP]);\n\
e (`FINITE (orbit f g X a)`\n\
     by METIS_TAC [ORBIT_SUBSET_TARGET, SUBSET_FINITE]);\n\
e (`CARD g.carrier = SIGMA CARD (partition (sreach (StabilizerGroup f g a)) g.carrier)`\n\
     by METIS_TAC [partition_CARD, SUBGROUP_REACH_EQUIV_ON_GROUP]);\n\
e (`_ = CARD (stabilizer f g a) * CARD (partition (sreach (StabilizerGroup f g a)) g.carrier)`\n\
     by METIS_TAC [SIGMA_CARD_CONSTANT, CARD_SUBGROUP_PARTITION_ELEMENT]);\n\
e (`_ = CARD (stabilizer f g a) * CARD (orbit f g X a)`\n\
     by METIS_TAC [FINITE_BIJ_CARD_EQ]);\n\
e (RW_TAC arith_ss []); (* solves it, product is commutative. *)\n\
")
results.push('\
val ORBIT_STABILIZER_THM = store_thm(\n\
  "ORBIT_STABILIZER_THM",\n\
  ``!f g X a. FiniteGroup g /\\ Action f g X /\\ a IN X /\\ FINITE X ==>\n\
   (CARD g.carrier = CARD (orbit f g X a) * CARD (stabilizer f g a))``,\n\
  SRW_TAC [][FiniteGroup_def] >>\n\
  `StabilizerGroup f g a <= g`\n\
     by METIS_TAC [StabilizerGroup_subgroup] >>\n\
  `(StabilizerGroup f g a).carrier = stabilizer f g a`\n\
     by SRW_TAC [][StabilizerGroup_def] >>\n\
  `FINITE (stabilizer f g a)`\n\
     by METIS_TAC [Subgroup_def, SUBSET_FINITE] >>\n\
  `BIJ (\\x. lcoset g (stabilizer f g a) (@z. z IN g.carrier /\\ (f z a = x)))\n\
   (orbit f g X a) (partition (sreach (StabilizerGroup f g a)) g.carrier)`\n\
     by SRW_TAC [][ORBIT_STABILIZER_MAP_BIJ] >>\n\
  `FINITE (partition (sreach (StabilizerGroup f g a)) g.carrier)`\n\
     by METIS_TAC [FINITE_partition, SUBGROUP_REACH_EQUIV_ON_GROUP] >>\n\
  `FINITE (orbit f g X a)`\n\
     by METIS_TAC [ORBIT_SUBSET_TARGET, SUBSET_FINITE] >>\n\
  `CARD g.carrier = SIGMA CARD (partition (sreach (StabilizerGroup f g a)) g.carrier)`\n\
     by METIS_TAC [partition_CARD, SUBGROUP_REACH_EQUIV_ON_GROUP] >>\n\
  `_ = CARD (stabilizer f g a) * CARD (partition (sreach (StabilizerGroup f g a)) g.carrier)`\n\
     by METIS_TAC [SIGMA_CARD_CONSTANT, CARD_SUBGROUP_PARTITION_ELEMENT] >>\n\
  `_ = CARD (stabilizer f g a) * CARD (orbit f g X a)`\n\
     by METIS_TAC [FINITE_BIJ_CARD_EQ] >>\n\
  RW_TAC arith_ss []);\
')

// Example #3
examples.push("\
(* <<ring_char_prime_endo>> *)\n\
g `!r:'a ring. Ring r ==> prime (char r) ==> RingEndo (\\x. x ** (char r)) r`;\n\
e (rpt strip_tac);\n\
e (rw[RingEndo_def, RingHomo_def]); (* >> knows ring_exp_element *)\n\
(* case: Ring r /\\ prime p ==> GroupHomo (\\x. x ** p) (ring_sum r) (ring_sum r) *)\n\
e (rw[GroupHomo_def]); (* knows ring_exp_element *)\n\
(* goal: Ring r /\\ prime p /\\ x IN R /\\ x' IN R ==> (x + x') ** p = x ** p + x' ** p *)\n\
e (metis_tac[ring_freshman_thm]); (* << *)\n\
(* case: Ring r ==> MonoidHomo (\\x. x ** p) r.prod r.prod *)\n\
e (rw[MonoidHomo_def, ring_mult_monoid]); (* << knows ring_exp_element, ring_mult_exp *)\n\
drop()\n\
")
results.push('\
val ring_char_prime_endo = store_thm(\n\
  "ring_char_prime_endo",\n\
  ``!r:\'a ring. Ring r ==> prime (char r) ==> RingEndo (\\x. x ** (char r)) r``,\n\
  rpt strip_tac >>\n\
  rw[RingEndo_def, RingHomo_def] >| [\n\
    rw[GroupHomo_def] >>\n\
    metis_tac[ring_freshman_thm],\n\
    rw[MonoidHomo_def, ring_mult_monoid]\n\
  ]);\
')

// Example #4 -- nested comments
examples.push("\
(* <<LOG2_n_TWICE_SQ>> *)\n\
g `!n. 1 < n ==> 4 <= (2 * (LOG2 n)) ** 2`;\n\
e (rpt strip_tac);\n\
e (`LOG2 2 = 1` by EVAL_TAC);\n\
e (`2 <= n` by decide_tac);\n\
e (`LOG2 2 <= LOG2 n` by rw[LOG2_LE]);\n\
e (`1 <= LOG2 n` by decide_tac);\n\
(* details:\n\
e (`2 * 1 <= 2 * LOG2 n` by rw[LE_MULT_LCANCEL, DECIDE``0 < 2``]);\n\
e (`2 <= 2 * LOG2 n` by decide_tac); -- ok *) (* extra *)\n\
e (`2 <= 2 * LOG2 n` by rw_tac arith_ss[LE_MULT_LCANCEL, DECIDE``0 < 2``]);\n\
(* e (`4 <= (2 * LOG2 n) ** 2` by rw_tac arith_ss[EXP_EXP_LE_MONO, DECIDE``0 < 2``]); -- no change *)\n\
(* e (`2 ** 2 <= (2 * LOG2 n) ** 2` by rw[EXP_EXP_LE_MONO, DECIDE``0 < 2``]);\n\
e (`2 ** 2 = 4` by rw_tac arith_ss[]); -- ok *)\n\
e (decide_tac);\n\
drop()\n\
")
results.push('\
val LOG2_n_TWICE_SQ = store_thm(\n\
  "LOG2_n_TWICE_SQ",\n\
  ``!n. 1 < n ==> 4 <= (2 * (LOG2 n)) ** 2``,\n\
  rpt strip_tac >>\n\
  `LOG2 2 = 1` by EVAL_TAC >>\n\
  `2 <= n` by decide_tac >>\n\
  `LOG2 2 <= LOG2 n` by rw[LOG2_LE] >>\n\
  `1 <= LOG2 n` by decide_tac >>\n\
  `2 <= 2 * LOG2 n` by rw_tac arith_ss[LE_MULT_LCANCEL, DECIDE``0 < 2``] >>\n\
  decide_tac);\
')

// Example #5 -- three branches
examples.push("\
(* <<SUBGROUP_EQCLASS_EQ_LCOSET>> *)\n\
g `!g h a. Group g /\\ h <= g  ==>\n\
   !e. e IN (partition (sreach h) g.carrier) ==>\n\
   ?a. a IN g.carrier /\\ (e = lcoset g h.carrier a)`;\n\
e (SRW_TAC [][partition_def, sreach_def]);\n\
e (Q.EXISTS_TAC `x`); (* let a = x, the equivalent class of x corresponds to the lcoset of x *)\n\
e (SRW_TAC [][lcoset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM]); (* >> 3 subgoals *)\n\
e (`x * (1/x * x') = (x * 1/x)* x'`\n\
     by SRW_TAC [][group_inv_carrier, group_assoc]);\n\
e (`_ = #e * x'` by SRW_TAC [][group_rinv]);\n\
e (`_ = x'` by SRW_TAC [][group_lid]);\n\
e (METIS_TAC [subgroup_property]); (* << 1st subgoal *)\n\
e (METIS_TAC [subgroup_element, group_mult_carrier]); (* << 2nd subgoal *)\n\
e (`x'' IN g.carrier` by METIS_TAC [subgroup_element]);\n\
e (`1/x * (x * x'') = (1/x * x) * x''`\n\
     by SRW_TAC [][group_inv_carrier, group_assoc]);\n\
e (`_ = #e * x''` by SRW_TAC [][group_linv]);\n\
e (`_ = x''` by SRW_TAC [][group_lid]);\n\
e (METIS_TAC [subgroup_property]); (* << 3rd subgoal *)\n\
")
results.push('\
val SUBGROUP_EQCLASS_EQ_LCOSET = store_thm(\n\
  "SUBGROUP_EQCLASS_EQ_LCOSET",\n\
  ``!g h a. Group g /\\ h <= g  ==>\n\
   !e. e IN (partition (sreach h) g.carrier) ==>\n\
   ?a. a IN g.carrier /\\ (e = lcoset g h.carrier a)``,\n\
  SRW_TAC [][partition_def, sreach_def] >>\n\
  Q.EXISTS_TAC `x` >>\n\
  SRW_TAC [][lcoset_def, IMAGE_DEF, EXTENSION, EQ_IMP_THM] >| [\n\
    `x * (1/x * x\') = (x * 1/x)* x\'`\n\
     by SRW_TAC [][group_inv_carrier, group_assoc] >>\n\
    `_ = #e * x\'` by SRW_TAC [][group_rinv] >>\n\
    `_ = x\'` by SRW_TAC [][group_lid] >>\n\
    METIS_TAC [subgroup_property],\n\
    METIS_TAC [subgroup_element, group_mult_carrier],\n\
    `x\'\' IN g.carrier` by METIS_TAC [subgroup_element] >>\n\
    `1/x * (x * x\'\') = (1/x * x) * x\'\'`\n\
     by SRW_TAC [][group_inv_carrier, group_assoc] >>\n\
    `_ = #e * x\'\'` by SRW_TAC [][group_linv] >>\n\
    `_ = x\'\'` by SRW_TAC [][group_lid] >>\n\
    METIS_TAC [subgroup_property]\n\
  ]);\
')

// Example #6 -- nested branching
examples.push("\
(* <<SUBLIST_LENGTH>> *)\n\
g `!p q. p <= q ==> (LENGTH p) <= (LENGTH q)`;\n\
(* Proof:\n\
   First, by induction on list p.\n\
   Then, by induction on list q.\n\
*)\n\
(* e (REPEAT STRIP_TAC); -- ??? remove for-alls *)\n\
e (Induct_on `p`);\n\
 (* >> *)\n\
e (RW_TAC arith_ss [SUBLIST, LENGTH]); (* << solves base case *)\n\
(* Step case is to prove: h::p <= q ==> LENGTH (h::p) <= LENGTH q  with inductive assumption.\n\
   Use induction on list q.\n\
*)\n\
e (Induct_on `q`); (* >> 2 subgoal *)\n\
e (PROVE_TAC [SUBLIST_NON_EMPTY, LENGTH]); (* << solves base case *)\n\
(* Now the subgoal is: h'::p <= h::q ==> LENGTH (h'::p) <= LENGTH (h::q) *)\n\
e (REPEAT STRIP_TAC); (* remove for-alls *)\n\
e (Cases_on `h' = h`); (* >> 2 subgoals *)\n\
e (RW_TAC arith_ss [SUBLIST, LENGTH]); (* simplifies a bit *)\n\
e (PROVE_TAC [SUBLIST]); (* << apply inductive assumption *)\n\
(* see if lemma can be applied *)\n\
e (PROVE_TAC [SUBLIST, LENGTH, LENGTH_LEMMA]); (* << Yes! *)\n\
(* <> *)\n\
")
results.push('\
val SUBLIST_LENGTH = store_thm(\n\
  "SUBLIST_LENGTH",\n\
  ``!p q. p <= q ==> (LENGTH p) <= (LENGTH q)``,\n\
  Induct_on `p` >-\n\
  RW_TAC arith_ss [SUBLIST, LENGTH] >>\n\
  Induct_on `q` >-\n\
  PROVE_TAC [SUBLIST_NON_EMPTY, LENGTH] >>\n\
  REPEAT STRIP_TAC >>\n\
  Cases_on `h\' = h` >| [\n\
    RW_TAC arith_ss [SUBLIST, LENGTH] >>\n\
    PROVE_TAC [SUBLIST],\n\
    PROVE_TAC [SUBLIST, LENGTH, LENGTH_LEMMA]\n\
  ]);\
')

// Example #7 -- lines with leading spaces
examples.push("\
(* Extra close example *)\n\
(* <<COMMUTING_ITSET_INSERT>> *)\n\
g `!x b. ITSET f (x INSERT s) b = ITSET f (s DELETE x) (f x b)`;\n\
e (REPEAT GEN_TAC);\n\
e (Cases_on `x IN s`); (* >> *)\n\
  e (FULL_SIMP_TAC bool_ss [ABSORPTION]);\n\
  e (Cases_on `x = y`); (* >> *)\n\
    e (POP_ASSUM SUBST_ALL_TAC);\n\
    e (Q_TAC SUFF_TAC `t = s DELETE y`); (* >> *)\n\
    e (SRW_TAC [][]); (* << *)\n\
    e (`s = y INSERT t` by PROVE_TAC [CHOICE_INSERT_REST]);\n\
    e (SRW_TAC [][GSYM DELETE_NON_ELEMENT]); (* << 1st *)\n\
    (* ~<< for BUG *)\n\
    e (`s = y INSERT t` by PROVE_TAC [CHOICE_INSERT_REST]);\n\
  e (ASM_SIMP_TAC arith_ss [DELETE_NON_ELEMENT]); (* << 2nd *)\n\
(* ~<< need manual insertion, otherwise BUG! *)\n\
  e (`x INSERT s = y INSERT t` by PROVE_TAC [CHOICE_INSERT_REST]);\n\
  e (Cases_on `x = y`); (* >> *)\n\
    e (POP_ASSUM SUBST_ALL_TAC);\n\
    e (Q_TAC SUFF_TAC `t = s` THEN1 SRW_TAC [][]);\n\
    e (FULL_SIMP_TAC bool_ss [EXTENSION, IN_INSERT]);\n\
    e (PROVE_TAC []); (* << 1st *)\n\
    e (`x IN t /\ y IN s` by PROVE_TAC [IN_INSERT]);\n\
  e (ASM_SIMP_TAC arith_ss [DELETE_NON_ELEMENT]); (* << 2nd *)\n\
(* OK *)\n\
")
results.push('\
val COMMUTING_ITSET_INSERT = store_thm(\n\
  "COMMUTING_ITSET_INSERT",\n\
  ``!x b. ITSET f (x INSERT s) b = ITSET f (s DELETE x) (f x b)``,\n\
  REPEAT GEN_TAC >>\n\
  Cases_on `x IN s` >| [\n\
    FULL_SIMP_TAC bool_ss [ABSORPTION] >>\n\
    Cases_on `x = y` >| [\n\
      POP_ASSUM SUBST_ALL_TAC >>\n\
      Q_TAC SUFF_TAC `t = s DELETE y` >-\n\
      SRW_TAC [][] >>\n\
      `s = y INSERT t` by PROVE_TAC [CHOICE_INSERT_REST] >>\n\
      SRW_TAC [][GSYM DELETE_NON_ELEMENT],\n\
      `s = y INSERT t` by PROVE_TAC [CHOICE_INSERT_REST] >>\n\
      ASM_SIMP_TAC arith_ss [DELETE_NON_ELEMENT]\n\
    ],\n\
    `x INSERT s = y INSERT t` by PROVE_TAC [CHOICE_INSERT_REST] >>\n\
    Cases_on `x = y` >| [\n\
      POP_ASSUM SUBST_ALL_TAC >>\n\
      Q_TAC SUFF_TAC `t = s` THEN1 SRW_TAC [][] >>\n\
      FULL_SIMP_TAC bool_ss [EXTENSION, IN_INSERT] >>\n\
      PROVE_TAC [],\n\
      `x IN t / y IN s` by PROVE_TAC [IN_INSERT] >>\n\
      ASM_SIMP_TAC arith_ss [DELETE_NON_ELEMENT]\n\
    ]\n\
  ]);\
')

// Example #8 -- with back steps
examples.push("\
(* <<BACKUP_EXAMPLE>> *)\n\
g `!s x b. FINITE s ==> (ITSET f (x INSERT s) b = ITSET f (s DELETE x) (f x b))`;\n\
e (REPEAT GEN_TAC); (* remove !f c. *)\n\
e (STRIP_TAC); (* separate assumption and goal *)\n\
e (Cases_on `x IN s`); (* >> *)\n\
e (FULL_SIMP_TAC bool_ss [absorption]);\n\
e (Cases_on `x = y`); (* >> *)\n\
e (POP_ASSUM SUBST_ALL_TAC);\n\
e (Q_TAC SUFF_TAC `t = s DELETE y` THEN1 SRW_TAC [][]);\n\
e (`s = y INSERT t` by METIS_TAC [NOT_IN_EMPTY, CHOICE_INSERT_REST]);\n\
e (SRW_TAC [][DELETE_INSERT, delete_non_element]); (* << 1st *)\n\
e (Q.ABBREV_TAC `u = s DELETE x`);\n\
e (`?r. (u = y INSERT r)` by Q.EXISTS_TAC `t DELETE x`); (* >> *)\n\
e (SRW_TAC [][CHOICE_INSERT_REST, Abbr`u`, insert_delete, Abbr`y`, Abbr`t`]); (* << *)\n\
b();\n\
b();\n\
e (`?r. (u = y INSERT r)` by (Q.EXISTS_TAC `t DELETE x` THEN\n\
            SRW_TAC [][CHOICE_INSERT_REST, Abbr`u`, insert_delete, Abbr`y`, Abbr`t`]));\n\
e (SRW_TAC [][delete_non_element]); (* << 2nd *)\n\
(* Case x NOTIN s *)\n\
e (SRW_TAC [][delete_non_element]);\n\
e (Cases_on `x = y`); (* >> *)\n\
e (`s = t` by METIS_TAC [DELETE_INSERT, DELETE_NON_ELEMENT]);\n\
e (SRW_TAC [][]); (* << 1st *)\n\
(* x <> y *)\n\
e (`_ = ITSET f u (f y (f x b))` by METIS_TAC [DELETE_NON_ELEMENT]);\n\
(* e (SRW_TAC [][]); -- solves it *)\n\
e (METIS_TAC []); (* << 2nd *)\n\
")
results.push('\
val BACKUP_EXAMPLE = store_thm(\n\
  "BACKUP_EXAMPLE",\n\
  ``!s x b. FINITE s ==> (ITSET f (x INSERT s) b = ITSET f (s DELETE x) (f x b))``,\n\
  REPEAT GEN_TAC >>\n\
  STRIP_TAC >>\n\
  Cases_on `x IN s` >| [\n\
    FULL_SIMP_TAC bool_ss [absorption] >>\n\
    Cases_on `x = y` >| [\n\
      POP_ASSUM SUBST_ALL_TAC >>\n\
      Q_TAC SUFF_TAC `t = s DELETE y` THEN1 SRW_TAC [][] >>\n\
      `s = y INSERT t` by METIS_TAC [NOT_IN_EMPTY, CHOICE_INSERT_REST] >>\n\
      SRW_TAC [][DELETE_INSERT, delete_non_element],\n\
      Q.ABBREV_TAC `u = s DELETE x` >>\n\
      `?r. (u = y INSERT r)` by (Q.EXISTS_TAC `t DELETE x` THEN\n\
            SRW_TAC [][CHOICE_INSERT_REST, Abbr`u`, insert_delete, Abbr`y`, Abbr`t`]) >>\n\
      SRW_TAC [][delete_non_element]\n\
    ],\n\
    SRW_TAC [][delete_non_element] >>\n\
    Cases_on `x = y` >| [\n\
      `s = t` by METIS_TAC [DELETE_INSERT, DELETE_NON_ELEMENT] >>\n\
      SRW_TAC [][],\n\
      `_ = ITSET f u (f y (f x b))` by METIS_TAC [DELETE_NON_ELEMENT] >>\n\
      METIS_TAC []\n\
    ]\n\
  ]);\
')

// Example #9 -- definition to convert to tDefine
examples.push("\
(* A definition conversion *)\n\
(* <<DILATE>> *)(*\n\
val DILATE_DEF = Define`\n\
   (DILATE e n m [] = []) /\\\n\
   (DILATE e n m [h] = [h]) /\\\n\
   (DILATE e n m (h::t) = h:: (TAKE n t ++ (GENLIST (K e) m) ++ DILATE e n m (DROP n t)))\n\
`;\n\
(*\n\
<<HOL message: inventing new type variable names: 'a>>\n\
Initial goal:\n\
\n\
?R. WF R /\\ !h v9 v8 m n e. R (e,n,m,DROP n (v8::v9)) (e,n,m,h::v8::v9)\n\
\n\
Exception raised at TotalDefn.Define:\n\
between line 4, character 3 and line 6, character 88:\n\
at TotalDefn.defnDefine:\n\
\n\
Unable to prove termination!\n\
*)\n\
g `?R. WF R /\\ !h v9 v8 m n e. R (e,n,m,DROP n (v8::v9)) (e,n,m,h::v8::v9)`;\n\
p();\n\
e (WF_REL_TAC `measure (\(a,b,c,d). LENGTH d)`);\n\
e (simp[LENGTH_DROP]);\n\
drop(); *)\n\
")
results.push('\
val DILATE_DEF = tDefine "DILATE" `\n\
  (DILATE e n m [] = []) /\\\n\
  (DILATE e n m [h] = [h]) /\\\n\
  (DILATE e n m (h::t) = h:: (TAKE n t ++ (GENLIST (K e) m) ++ DILATE e n m (DROP n t)))\n\
`(\n\
  WF_REL_TAC `measure ((a,b,c,d). LENGTH d)` >>\n\
  simp[LENGTH_DROP]);\
')

// Example #10 -- not yet optimized!
examples.push("\
(* An example of optimisation *)\n\
(* <<GENLIST_K_RANGE>> *)\n\
g `!f (e:\'a) n. (!k. 0 < k /\\ k <= n ==> (f k = e)) ==>\n\
   (GENIST ((\j. f j) o SUC) n = GENLIST (K e) n)`;\n\
e (rpt strip_tac);\n\
e (Induct_on `n`); (* >> *)\n\
e (rw[]); (* << *)\n\
e (rw[GENLIST]); (* << *)\n\
")
results.push('\
val GENLIST_K_RANGE = store_thm(\n\
  "GENLIST_K_RANGE",\n\
  ``!f (e:\'a) n. (!k. 0 < k /\\ k <= n ==> (f k = e)) ==>\n\
   (GENIST ((j. f j) o SUC) n = GENLIST (K e) n)``,\n\
  rpt strip_tac >>\n\
  Induct_on `n` >-\n\
  rw[] >>\n\
  rw[GENLIST]);\
')

// Example #11 -- with subgoal branching
examples.push('\
(* <<MOD_MULT_INV_EXISTS>> *)\n\
g `!p x. prime p /\\ 0 < x /\\ x < p ==> ?y. 0 < y /\\ y < p /\\ ((y * x) MOD p = 1)`;\n\
e (rpt strip_tac);\n\
e (`0 < p /\\ 1 < p` by metis_tac[PRIME_POS, ONE_LT_PRIME]);\n\
e (`gcd p x = 1` by metis_tac[PRIME_GCD, NOT_LT_DIVIDES]);\n\
e (`?k q. k * x = q * p + 1` by metis_tac[LINEAR_GCD, NOT_ZERO_LT_ZERO]);\n\
e (`1 = (k * x) MOD p` by metis_tac[MOD_MULT]);\n\
e (`_ = ((k MOD p) * (x MOD p)) MOD p` by metis_tac[MOD_TIMES2]);\n\
(* prove on-the-fly *)\n\
(* e (`0 < k MOD p` by rw[]); (* >> *) *)\n\
e (sg `0 < k MOD p`); (* >> *)\n\
(* case: 1 = (k MOD p * x MOD p) MOD p ==> 0 < k MOD p *)\n\
e (`1 <> 0` by decide_tac);\n\
e (`x MOD p <> 0` by metis_tac[DIVIDES_MOD_0, NOT_LT_DIVIDES]);\n\
e (`k MOD p <> 0` by metis_tac[EUCLID_LEMMA, MOD_MOD]);\n\
e (decide_tac); (* << *)\n\
(* case: 0 < k MOD p ==> ?y. 0 < y /\\ y < p /\\ ((y * x) MOD p = 1) *)\n\
e (metis_tac[MOD_LESS, LESS_MOD]); (* << *)\n\
drop();\n\
')
results.push('\
val MOD_MULT_INV_EXISTS = store_thm(\n\
  "MOD_MULT_INV_EXISTS",\n\
  ``!p x. prime p /\\ 0 < x /\\ x < p ==> ?y. 0 < y /\\ y < p /\\ ((y * x) MOD p = 1)``,\n\
  rpt strip_tac >>\n\
  `0 < p /\\ 1 < p` by metis_tac[PRIME_POS, ONE_LT_PRIME] >>\n\
  `gcd p x = 1` by metis_tac[PRIME_GCD, NOT_LT_DIVIDES] >>\n\
  `?k q. k * x = q * p + 1` by metis_tac[LINEAR_GCD, NOT_ZERO_LT_ZERO] >>\n\
  `1 = (k * x) MOD p` by metis_tac[MOD_MULT] >>\n\
  `_ = ((k MOD p) * (x MOD p)) MOD p` by metis_tac[MOD_TIMES2] >>\n\
  `0 < k MOD p` by\n\
  (`1 <> 0` by decide_tac >>\n\
  `x MOD p <> 0` by metis_tac[DIVIDES_MOD_0, NOT_LT_DIVIDES] >>\n\
  `k MOD p <> 0` by metis_tac[EUCLID_LEMMA, MOD_MOD] >>\n\
  decide_tac) >>\n\
  metis_tac[MOD_LESS, LESS_MOD]);\
')

// Example #12 -- nested subgoals
examples.push('\
(* <<power_predecessor_gcd_reduction>> *)\n\
g `!t n m. 0 < m /\\ m < n ==> (gcd (t ** n - 1) (t ** m - 1) = gcd ((t ** m - 1)) (t ** (n - m) - 1))`;\n\
e (rpt strip_tac);\n\
e (Cases_on `t = 0`); (* >> *)\n\
(* case: t = 0 ==> gcd (t ** n - 1) (t ** m - 1) = gcd (t ** n - 1) (t ** (m - n) - 1) *)\n\
e (rw[ZERO_EXP]); (* << *)\n\
(* case: t <> 0 ==> gcd (t ** n - 1) (t ** m - 1) = gcd (t ** n - 1) (t ** (m - n) - 1) *)\n\
e (Cases_on `t = 1`); (* >> *)\n\
(* case: t = 1 /\\ t <> 0 ==> gcd (t ** n - 1) (t ** m - 1) = gcd (t ** n - 1) (t ** (m - n) - 1) *)\n\
e (rw[]); (* << by EXP_1, GCD_0L *)\n\
(* case: t <> 1 /\\ t <> 0 ==> gcd (t ** n - 1) (t ** m - 1) = t ** (gcd n m) - 1 *)\n\
e (`0 < t /\\ 1 < t` by decide_tac);\n\
(* e (`t ** 0 = 1` by rw[]); (* by EXP *) -- can skip *)\n\
e (`!n. 1 <= t ** n` by rw[ONE_LE_EXP]); (* need 0 < t *)\n\
e (`!n. 0 < n ==> 1 < t ** n` by rw[ONE_LT_EXP]); (* need 1 < t *)\n\
(* Prove an identity for later use. *)\n\
e (sg `(t ** n - 1) - t ** (n - m) * (t ** m - 1) = t ** (n - m) - 1`); (* >> *)\n\
(* case: (t ** n - 1) - t ** (n - m) * (t ** m - 1) = t ** (n - m) - 1 *)\n\
e (`0 < n - m` by decide_tac); (* need 0 < m *)\n\
(* First, prove on-the-fly: t ** (n - m) - 1 <= t ** n - 1 *))\n\
e (sg `t ** (n - m) - 1 <= t ** n - 1`); (* >> *)\n\
(* case: t ** (n - m) - 1 <= t ** n - 1 *)\n\
e (`n - m <= n` by decide_tac);\n\
e (`t ** (n - m) <= t ** n` by rw[EXP_BASE_LEQ_MONO_IMP]); (* need 0 < t *)\n\
(* e (`1 <= t ** (n - m) /\\ 1 <= t ** n` by rw[]); -- can skip *)\n\
e (decide_tac); (* << *)\n\
(* proved: t ** (n - m) - 1 <= t ** n - 1 *)\n\
(* Rest of lemma: (t ** n - 1) - t ** (n - m) * (t ** m - 1) = t ** (n - m) - 1 *)\n\
e (`t ** n - 1 - t ** (n - m) * (t ** m - 1) = t ** n - 1 - (t ** (n - m) * t ** m - t ** (n - m))` by rw_tac std_ss[LEFT_SUB_DISTRIB]);\n\
e (`_ = t ** n - 1 - (t ** (n - m + m) - t ** (n - m))` by rw_tac std_ss[EXP_ADD]);\n\
e (`_ = t ** n - 1 - (t ** n - t ** (n - m))` by rw_tac std_ss[SUB_ADD, LESS_IMP_LESS_OR_EQ]); (* need m <= n from m < n *)\n\
e (`_ = t ** n - 1 - (t ** n - (t ** (n - m) - 1 + 1))` by rw_tac std_ss[SUB_ADD]); (* need 1 <= t ** (n - m) *)\n\
e (`_ = t ** n - 1 - (t ** n - (1 + (t ** (n - m) - 1)))` by rw_tac std_ss[ADD_COMM]);\n\
e (`_ = t ** n - 1 - (t ** n - 1 - (t **  (n - m) - 1))` by rw_tac std_ss[SUB_PLUS]); (* no condition *)\n\
e (`_ = t ** n - 1 + (t ** (n - m) - 1) - (t ** n - 1)` by rw_tac std_ss[SUB_SUB]); (* need t ** (n - m) - 1 <= t ** n - 1 *)\n\
e (rw_tac std_ss[ADD_SUB, ADD_COMM]); (* << no condition *)\n\
(* proved: (t ** n - 1) - t ** (n - m) * (t ** m - 1) = t ** (n - m) - 1 *)\n\
(* Rest of proof of reduction. *)\n\
e (`0 < n - m` by rw_tac arith_ss[]); (* by m < n *)\n\
e (`1 < t ** (n - m)` by rw[ONE_LT_EXP]); (* need 0 < n - m *)\n\
e (`t ** (n - m) * (t ** m - 1) <= t ** n - 1` by rw_tac arith_ss[]); (* need lemma and 1 < t ** (n - m) *)\n\
(* rest of GCD identity *)\n\
e (`gcd (t ** n - 1) (t ** m - 1) = gcd (t ** m - 1) (t ** n - 1)` by rw_tac std_ss[GCD_SYM]);\n\
e (`_ = gcd (t ** m - 1) ((t ** n - 1) - t ** (n - m) * (t ** m - 1))` by rw_tac std_ss[GCD_SUB_MULTIPLE]); (* need t ** (n - m) * (t ** m - 1) <= t ** n - 1 *)\n\
(* e (`_ = gcd ((t ** m - 1)) (t ** (n - m) - 1)` by rw_tac std_ss[]); (* in two steps, by (t ** n - 1) - t ** (n - m) * (t ** m - 1) = t ** (n - m) - 1 *) -- can skip *)\n\
e (rw_tac std_ss[]); (* << *)\n\
drop();\n\
')
results.push('\
val power_predecessor_gcd_reduction = store_thm(\n\
  "power_predecessor_gcd_reduction",\n\
  ``!t n m. 0 < m /\\ m < n ==> (gcd (t ** n - 1) (t ** m - 1) = gcd ((t ** m - 1)) (t ** (n - m) - 1))``,\n\
  rpt strip_tac >>\n\
  Cases_on `t = 0` >-\n\
  rw[ZERO_EXP] >>\n\
  Cases_on `t = 1` >-\n\
  rw[] >>\n\
  `0 < t /\\ 1 < t` by decide_tac >>\n\
  `!n. 1 <= t ** n` by rw[ONE_LE_EXP] >>\n\
  `!n. 0 < n ==> 1 < t ** n` by rw[ONE_LT_EXP] >>\n\
  `(t ** n - 1) - t ** (n - m) * (t ** m - 1) = t ** (n - m) - 1` by\n\
  (`0 < n - m` by decide_tac >>\n\
  `t ** (n - m) - 1 <= t ** n - 1` by\n\
    (`n - m <= n` by decide_tac >>\n\
  `t ** (n - m) <= t ** n` by rw[EXP_BASE_LEQ_MONO_IMP] >>\n\
  decide_tac) >>\n\
  `t ** n - 1 - t ** (n - m) * (t ** m - 1) = t ** n - 1 - (t ** (n - m) * t ** m - t ** (n - m))` by rw_tac std_ss[LEFT_SUB_DISTRIB] >>\n\
  `_ = t ** n - 1 - (t ** (n - m + m) - t ** (n - m))` by rw_tac std_ss[EXP_ADD] >>\n\
  `_ = t ** n - 1 - (t ** n - t ** (n - m))` by rw_tac std_ss[SUB_ADD, LESS_IMP_LESS_OR_EQ] >>\n\
  `_ = t ** n - 1 - (t ** n - (t ** (n - m) - 1 + 1))` by rw_tac std_ss[SUB_ADD] >>\n\
  `_ = t ** n - 1 - (t ** n - (1 + (t ** (n - m) - 1)))` by rw_tac std_ss[ADD_COMM] >>\n\
  `_ = t ** n - 1 - (t ** n - 1 - (t **  (n - m) - 1))` by rw_tac std_ss[SUB_PLUS] >>\n\
  `_ = t ** n - 1 + (t ** (n - m) - 1) - (t ** n - 1)` by rw_tac std_ss[SUB_SUB] >>\n\
  rw_tac std_ss[ADD_SUB, ADD_COMM]) >>\n\
  `0 < n - m` by rw_tac arith_ss[] >>\n\
  `1 < t ** (n - m)` by rw[ONE_LT_EXP] >>\n\
  `t ** (n - m) * (t ** m - 1) <= t ** n - 1` by rw_tac arith_ss[] >>\n\
  `gcd (t ** n - 1) (t ** m - 1) = gcd (t ** m - 1) (t ** n - 1)` by rw_tac std_ss[GCD_SYM] >>\n\
  `_ = gcd (t ** m - 1) ((t ** n - 1) - t ** (n - m) * (t ** m - 1))` by rw_tac std_ss[GCD_SUB_MULTIPLE] >>\n\
  rw_tac std_ss[]);\
')

// Example #13 -- inner nested subgoals
examples.push('\
(* <<LESS_HALF_IFF>> *)\n\
g `!n k. k < HALF n <=> k + 1 < n - k`;\n\
e (rw[EQ_IMP_THM]); (* >> *)\n\
(* case: k < HALF n ==> k + 1 < n - k *)\n\
(* prove on-the-fly: 1 < n - 2 * k *)\n\
e (sg `1 < n - 2 * k`); (* >> *)\n\
(* case: k < HALF n ==> 1 < n - 2 * k *)\n\
e (Cases_on `EVEN n`); (* >> *)\n\
(* case: EVEN n /\\ k < HALF n ==> 1 < n - 2 * k *)\n\
(* prove on-the-fly: n - TWICE k <> 0 *)\n\
e (sg `n - 2 * k <> 0`); (* >> *)\n\
(* case: EVEN n ==> n - TWICE k <> 0 *)\n\
e (spose_not_then strip_assume_tac); (* by contradiction *)\n\
e (`2 * HALF n = n` by metis_tac[EVEN_HALF]);\n\
(* e (`n <= 2 * k` by decide_tac); -- can skip *)\n\
e (decide_tac); (* << *)\n\
(* proved: n - TWICE k <> 0 *)\n\
(* prove on-the-fly: n - TWICE k <> 1 *)\n\
e (sg `n - 2 * k <> 1`); (* >> *)\n\
(* case: EVEN n ==> n - TWICE k <> 1 *)\n\
e (spose_not_then strip_assume_tac); (* by contradiction *)\n\
e (`n = 2 * k + 1` by decide_tac);\n\
e (`ODD n` by metis_tac[ODD_EXISTS, ADD1]);\n\
e (metis_tac[EVEN_ODD]); (* << *)\n\
(* proved: n - TWICE k <> 1 *)\n\
e (decide_tac); (* << *)\n\
(* proved: EVEN n /\\ k < HALF n ==> 1 < n - 2 * k *)\n\
(* case: ~EVEN n /\\ k < HALF n ==> 1 < n - 2 * k *)\n\
e (`n MOD 2 = 1` by metis_tac[EVEN_ODD, ODD_MOD2]);\n\
e (`n = 2 * HALF n + (n MOD 2)` by metis_tac[DIVISION, MULT_COMM, DECIDE``0 < 2``]);\n\
(* e (`2 * k + 1 < n` by decide_tac); -- can skip *)\n\
e (decide_tac); (* << *)\n\
(* proved: 1 < n - 2 * k *)\n\
e (decide_tac); (* << *)\n\
(* case: k + 1 < n - k ==> k < HALF n *)\n\
e (`2 * k + 1 < n` by decide_tac);\n\
e (`n = 2 * HALF n + (n MOD 2)` by metis_tac[DIVISION, MULT_COMM, DECIDE``0 < 2``]);\n\
e (`n MOD 2 < 2` by rw[]); (* by MOD_LESS, 0 < 2 *)\n\
(* e (`n <= 2 * HALF n + 1` by decide_tac); -- can skip *)\n\
e (decide_tac); (* << *)\n\
drop();\n\
')
results.push('\
val LESS_HALF_IFF = store_thm(\n\
  "LESS_HALF_IFF",\n\
  ``!n k. k < HALF n <=> k + 1 < n - k``,\n\
  rw[EQ_IMP_THM] >| [\n\
    `1 < n - 2 * k` by\n\
  (Cases_on `EVEN n` >| [\n\
      `n - 2 * k <> 0` by\n\
  (spose_not_then strip_assume_tac >>\n\
      `2 * HALF n = n` by metis_tac[EVEN_HALF] >>\n\
      decide_tac) >>\n\
      `n - 2 * k <> 1` by\n\
    (spose_not_then strip_assume_tac >>\n\
      `n = 2 * k + 1` by decide_tac >>\n\
      `ODD n` by metis_tac[ODD_EXISTS, ADD1] >>\n\
      metis_tac[EVEN_ODD]) >>\n\
      decide_tac,\n\
      `n MOD 2 = 1` by metis_tac[EVEN_ODD, ODD_MOD2] >>\n\
      `n = 2 * HALF n + (n MOD 2)` by metis_tac[DIVISION, MULT_COMM, DECIDE``0 < 2``] >>\n\
      decide_tac\n\
    ]) >>\n\
    decide_tac,\n\
    `2 * k + 1 < n` by decide_tac >>\n\
    `n = 2 * HALF n + (n MOD 2)` by metis_tac[DIVISION, MULT_COMM, DECIDE``0 < 2``] >>\n\
    `n MOD 2 < 2` by rw[] >>\n\
    decide_tac\n\
  ]);\
')

// Example #14 -- many sequential subgoals
examples.push('\
(* <<sum_image_by_composition_with_partial_inj>> *)\n\
g `!s. FINITE s ==> !f:(\'b -> bool) -> num. (f {} = 0) ==>\n\
   !g. (!t. FINITE t /\\ (!x. x IN t ==> g x <> {}) ==> INJ g t univ(:\'b -> bool)) ==>\n\
   (SIGMA f (IMAGE g s) = SIGMA (f o g) s)`;\n\
e (rpt strip_tac);\n\
(* goal: SIGMA f (IMAGE g s) = SIGMA (f o g) s *)\n\
(* construct a paritition so that images are also partitions *)\n\
e (qabbrev_tac `s1 = {x | x IN s /\\ (g x = {})}`);\n\
e (qabbrev_tac `s2 = {x | x IN s /\\ (g x <> {})}`);\n\
e (`s = s1 UNION s2` by (rw[Abbr`s1`, Abbr`s2`, EXTENSION] >> metis_tac[]));\n\
e (`DISJOINT s1 s2` by (rw[Abbr`s1`, Abbr`s2`, EXTENSION, DISJOINT_DEF] >> metis_tac[]));\n\
e (`DISJOINT (IMAGE g s1) (IMAGE g s2)` by (rw[Abbr`s1`, Abbr`s2`, EXTENSION, DISJOINT_DEF] >> metis_tac[]));\n\
e (`s1 SUBSET s /\\ s2 SUBSET s` by rw[Abbr`s1`, Abbr`s2`, SUBSET_DEF]);\n\
e (`FINITE s1 /\\ FINITE s2` by metis_tac[SUBSET_FINITE]);\n\
e (`FINITE (IMAGE g s1) /\\ FINITE (IMAGE g s2)` by rw[]); (* by IMAGE_FINITE *)\n\
(* Step 1: decompose left summation *)\n\
(* details:\n\
e (`SIGMA f (IMAGE g s) = SIGMA f (IMAGE g (s1 UNION s2))` by rw[]); (* by above *)\n\
e (`_ = SIGMA f ((IMAGE g s1) UNION (IMAGE g s2))` by rw[]); (* by IMAGE_UNION *) -- ok *)\n\
e (`SIGMA f (IMAGE g s) = SIGMA f ((IMAGE g s1) UNION (IMAGE g s2))` by rw[]); (* by IMAGE_UNION *)\n\
e (`_ = SIGMA f (IMAGE g s1) + SIGMA f (IMAGE g s2)` by rw[SUM_IMAGE_DISJOINT]);\n\
(* prove on-the-fly *)\n\
e (sg `SIGMA f (IMAGE g s1) = 0`); (* >> *)\n\
(* case: SIGMA f (IMAGE g s1) = 0 *)\n\
e (Cases_on `s1 = {}`); (* >> *)\n\
(* case: s1 = {} ==> SIGMA f (IMAGE g s1) = 0 *)\n\
e (rw[SUM_IMAGE_EMPTY]); (* << *)\n\
(* case: s1 <> {} ==> SIGMA f (IMAGE g s1) = 0 *)\n\
e (`!x. x IN s1 ==> (g x = {})` by rw[Abbr`s1`]);\n\
e (`!y. y IN (IMAGE g s1) ==> (y = {})` by metis_tac[IN_IMAGE, IMAGE_EMPTY]);\n\
e (`{} IN {{}} /\\ IMAGE g s1 <> {}` by rw[]); (* bt IMAGE_EMPTY *)\n\
(* details:\n\
e (`SING (IMAGE g s1)` by metis_tac[SING_ONE_ELEMENT]);\n\
e (`IMAGE g s1 = {{}}` by metis_tac[SING_DEF, IN_SING]); -- ok *)\n\
e (`IMAGE g s1 = {{}}` by metis_tac[SING_DEF, IN_SING, SING_ONE_ELEMENT]);\n\
e (`SIGMA f (IMAGE g s1) = f {}` by rw[SUM_IMAGE_SING]);\n\
e (rw[]); (* << by given *)\n\
(* proved: SIGMA f (IMAGE g s1) = 0 *)\n\
(* Step 2: decompose right summation *)\n\
(* details:\n\
e (`SIGMA (f o g) s = SIGMA (f o g) (s1 UNION s2)` by rw[]); (* by above *)\n\
e (`_ = SIGMA (f o g) s1 + SIGMA (f o g) s2` by rw[SUM_IMAGE_DISJOINT]); -- ok *)\n\
e (`SIGMA (f o g) s = SIGMA (f o g) s1 + SIGMA (f o g) s2` by rw[SUM_IMAGE_DISJOINT]);\n\
(* prove on-the-fly *)\n\
e (sg `SIGMA (f o g) s1 = 0`); (* >> *)\n\
(* case: SIGMA (f o g) s1 = 0 *)\n\
e (`!x. x IN s1 ==> (g x = {})` by rw[Abbr`s1`]);\n\
e (`!x. x IN s1 ==> ((f o g) x = 0)` by rw[]); (* by given *)\n\
(* details:\n\
e (`SIGMA (f o g) s1 = 0 * CARD s1` by rw[SIGMA_CONSTANT]);\n\
e (metis_tac[MULT]); -- ok *)\n\
e (metis_tac[SIGMA_CONSTANT, MULT]); (* << *)\n\
(* proved: SIGMA (f o g) s1 = 0 *)\n\
(* prove on-the-fly *)\n\
e (sg `SIGMA f (IMAGE g s2) = SIGMA (f o g) s2`); (* >> *)\n\
(* case: SIGMA f (IMAGE g s2) = SIGMA (f o g) s2 *)\n\
e (`!x. x IN s2 ==> g x <> {}` by rw[Abbr`s2`]);\n\
e (`INJ g s2 univ(:\'b -> bool)` by rw[]); (* by given *)\n\
e (rw[sum_image_by_composition]); (* << *)\n\
(* proved: SIGMA f (IMAGE g s2) = SIGMA (f o g) s2 *)\n\
e (decide_tac); (* << by arithmetic *)\n\
drop();\n\
')
results.push('\
val sum_image_by_composition_with_partial_inj = store_thm(\n\
  "sum_image_by_composition_with_partial_inj",\n\
  ``!s. FINITE s ==> !f:(\'b -> bool) -> num. (f {} = 0) ==>\n\
   !g. (!t. FINITE t /\\ (!x. x IN t ==> g x <> {}) ==> INJ g t univ(:\'b -> bool)) ==>\n\
   (SIGMA f (IMAGE g s) = SIGMA (f o g) s)``,\n\
  rpt strip_tac >>\n\
  qabbrev_tac `s1 = {x | x IN s /\\ (g x = {})}` >>\n\
  qabbrev_tac `s2 = {x | x IN s /\\ (g x <> {})}` >>\n\
  (`s = s1 UNION s2` by (rw[Abbr`s1`, Abbr`s2`, EXTENSION] >> metis_tac[])) >>\n\
  (`DISJOINT s1 s2` by (rw[Abbr`s1`, Abbr`s2`, EXTENSION, DISJOINT_DEF] >> metis_tac[])) >>\n\
  (`DISJOINT (IMAGE g s1) (IMAGE g s2)` by (rw[Abbr`s1`, Abbr`s2`, EXTENSION, DISJOINT_DEF] >> metis_tac[])) >>\n\
  `s1 SUBSET s /\\ s2 SUBSET s` by rw[Abbr`s1`, Abbr`s2`, SUBSET_DEF] >>\n\
  `FINITE s1 /\\ FINITE s2` by metis_tac[SUBSET_FINITE] >>\n\
  `FINITE (IMAGE g s1) /\\ FINITE (IMAGE g s2)` by rw[] >>\n\
  `SIGMA f (IMAGE g s) = SIGMA f ((IMAGE g s1) UNION (IMAGE g s2))` by rw[] >>\n\
  `_ = SIGMA f (IMAGE g s1) + SIGMA f (IMAGE g s2)` by rw[SUM_IMAGE_DISJOINT] >>\n\
  `SIGMA f (IMAGE g s1) = 0` by\n\
  (Cases_on `s1 = {}` >-\n\
  rw[SUM_IMAGE_EMPTY] >>\n\
  `!x. x IN s1 ==> (g x = {})` by rw[Abbr`s1`] >>\n\
  `!y. y IN (IMAGE g s1) ==> (y = {})` by metis_tac[IN_IMAGE, IMAGE_EMPTY] >>\n\
  `{} IN {{}} /\\ IMAGE g s1 <> {}` by rw[] >>\n\
  `IMAGE g s1 = {{}}` by metis_tac[SING_DEF, IN_SING, SING_ONE_ELEMENT] >>\n\
  `SIGMA f (IMAGE g s1) = f {}` by rw[SUM_IMAGE_SING] >>\n\
  rw[]\n\
  ) >>\n\
  `SIGMA (f o g) s = SIGMA (f o g) s1 + SIGMA (f o g) s2` by rw[SUM_IMAGE_DISJOINT] >>\n\
  `SIGMA (f o g) s1 = 0` by\n\
    (`!x. x IN s1 ==> (g x = {})` by rw[Abbr`s1`] >>\n\
  `!x. x IN s1 ==> ((f o g) x = 0)` by rw[] >>\n\
  metis_tac[SIGMA_CONSTANT, MULT]) >>\n\
  `SIGMA f (IMAGE g s2) = SIGMA (f o g) s2` by\n\
      (`!x. x IN s2 ==> g x <> {}` by rw[Abbr`s2`] >>\n\
  `INJ g s2 univ(:\'b -> bool)` by rw[] >>\n\
  rw[sum_image_by_composition]) >>\n\
  decide_tac);\
')

// Example #15 -- semicolon inside list
examples.push('\
(* <<fragment_n_2>> *)\n\
g `!ls n a b. n + 1 < LENGTH ls /\\ fragment ls n 2 = [a; b] ==>\n\
              fragment ls n 1 = [a] /\\ fragment ls (n + 1) 1 = [b] `;\n\
e (rpt strip_tac); (* >> *)\n\
(* case: fragment ls n 1 = [a] *)\n\
e (`fragment ls n 1 = TAKE 1 (fragment ls n 2)` by rw[TAKE_TAKE]);\n\
e (rw[]); (* << *)\n\
(* case: fragment ls (n + 1) 1 = [b] *)\n\
e (qabbrev_tac `t = DROP 2 (DROP n ls)`);\n\
e (`fragment ls (n + 1) 1 = fragment (DROP n ls) 1 1` by rw[DROP_DROP_T]);\n\
e (`_ =  fragment ([a; b] ++ t) 1 1` by metis_tac[TAKE_DROP]);\n\
e (rw[]); (* << by DROP_APPEND1, TAKE_APPEND1 *)\n\
')
results.push('\
val fragment_n_2 = store_thm(\n\
  "fragment_n_2",\n\
  ``!ls n a b. n + 1 < LENGTH ls /\\ fragment ls n 2 = [a; b] ==>\n\
              fragment ls n 1 = [a] /\\ fragment ls (n + 1) 1 = [b] ``,\n\
  rpt strip_tac >| [\n\
    `fragment ls n 1 = TAKE 1 (fragment ls n 2)` by rw[TAKE_TAKE] >>\n\
    rw[],\n\
    qabbrev_tac `t = DROP 2 (DROP n ls)` >>\n\
    `fragment ls (n + 1) 1 = fragment (DROP n ls) 1 1` by rw[DROP_DROP_T] >>\n\
    `_ =  fragment ([a; b] ++ t) 1 1` by metis_tac[TAKE_DROP] >>\n\
    rw[]\n\
  ]);\
')

// Example #16 -- edit has multiple lines
examples.push('\
(* <<cpu_sees_shrink_body>> *)\n\
g `!m n k t. m.state = RUNNING /\\ m.registers = k::t /\\\n\
    m.bp < LENGTH m.cells /\\ EL m.bp m.cells = n /\\\n\
    m sees shrink_body ==>\n\
    FUNPOW cpu_step 4 m =\n\
    m with <|registers := (k - 1)::t;\n\
             rcount := MAX m.rcount (2 + LENGTH t);\n\
             cells := LUPDATE (HALF n) m.bp m.cells;\n\
             pc := m.pc + 4; clock := m.clock + (4 + size n + size k)|>`;\n\
e (rpt strip_tac); (* to keep n *)\n\
(* Step 0: decompose *)\n\
e (imp_res_tac sees_def);\n\
e (qabbrev_tac `instructions = m.codeMap \' m.name`);\n\
e (`(instructions at m.pc) (macro_half 0) /\\\n\
    (instructions at m.pc + 3) [DEC]` by metis_tac[segment_at_append, shrink_body_def, macro_half_length]);\n\
(* Step 1: macro_half 0 *)\n\
e (`m sees (macro_half 0)` by fs[sees_def]);\n\
e (`m.bp + 0 < LENGTH m.cells /\ EL (m.bp + 0) m.cells = n` by fs[]); (* for next *)\n\
e (imp_res_tac cpu_sees_half);\n\
(* Step 2: [DEC] *)\n\
e (qabbrev_tac `m1 = FUNPOW cpu_step 3 m`);\n\
e (`m1 sees [DEC]` by fs[sees_def]);\n\
e (`cpu_step m1 =\n\
    m1 with <|registers := k - 1::t;\n\
              pc := m1.pc + 1; clock := m1.clock + (1 + size k)|>` by rw[cpu_sees_DEC]);\n\
(* Conclude *)\n\
e (qabbrev_tac `m2 = cpu_step m1`);\n\
e (`FUNPOW cpu_step 4 m = m2` by metis_tac[FUNPOW_ADD, FUNPOW_1, DECIDE``4 = 1 + 3``]);\n\
e (fs[ADD1]); (* << *)\n\
')
results.push('\
val cpu_sees_shrink_body = store_thm(\n\
  "cpu_sees_shrink_body",\n\
  ``!m n k t. m.state = RUNNING /\\ m.registers = k::t /\\\n\
    m.bp < LENGTH m.cells /\\ EL m.bp m.cells = n /\\\n\
    m sees shrink_body ==>\n\
    FUNPOW cpu_step 4 m =\n\
    m with <|registers := (k - 1)::t;\n\
             rcount := MAX m.rcount (2 + LENGTH t);\n\
             cells := LUPDATE (HALF n) m.bp m.cells;\n\
             pc := m.pc + 4; clock := m.clock + (4 + size n + size k)|>``,\n\
  rpt strip_tac >>\n\
  imp_res_tac sees_def >>\n\
  qabbrev_tac `instructions = m.codeMap \' m.name` >>\n\
  `(instructions at m.pc) (macro_half 0) /\\\n\
    (instructions at m.pc + 3) [DEC]` by metis_tac[segment_at_append, shrink_body_def, macro_half_length] >>\n\
  `m sees (macro_half 0)` by fs[sees_def] >>\n\
  `m.bp + 0 < LENGTH m.cells /\ EL (m.bp + 0) m.cells = n` by fs[] >>\n\
  imp_res_tac cpu_sees_half >>\n\
  qabbrev_tac `m1 = FUNPOW cpu_step 3 m` >>\n\
  `m1 sees [DEC]` by fs[sees_def] >>\n\
  `cpu_step m1 =\n\
    m1 with <|registers := k - 1::t;\n\
              pc := m1.pc + 1; clock := m1.clock + (1 + size k)|>` by rw[cpu_sees_DEC] >>\n\
  qabbrev_tac `m2 = cpu_step m1` >>\n\
  `FUNPOW cpu_step 4 m = m2` by metis_tac[FUNPOW_ADD, FUNPOW_1, DECIDE``4 = 1 + 3``] >>\n\
  fs[ADD1]);\
')
// Example #17: decorator
examples.push('\
(* <<valueOf_unit>> *)\n\
(* ++[simp]++ *)\n\
g `!x. valueOf (unit x) = x`;\n\
e (rw[valueOf_def, unit_def]);\n\
')
results.push('\
val valueOf_unit = store_thm(\n\
  "valueOf_unit[simp]",\n\
  ``!x. valueOf (unit x) = x``,\n\
  rw[valueOf_def, unit_def]);\
')
// Example #18: new style
examples.push('\
(* <<ROOT_EVAL>> *)\n\
(* ++[compute]++ *)\n\
g `!r n. ROOT r n =\n\
    if r = 0 then ROOT 0 n else\n\
    if n = 0 then 0 else\n\
    let m = TWICE (ROOT r (n DIV 2 ** r)) in\n\
    m + if (SUC m) ** r <= n then 1 else 0`;\n\
e (rpt strip_tac);\n\
e (Cases_on `r = 0` >> asm_simp_tac arith_ss[LET_THM]);\n\
e (`0 < r` by decide_tac);\n\
e (Cases_on `n = 0` >> asm_simp_tac arith_ss[Once ROOT_COMPUTE, LET_THM]);\n\
e (`0 DIV 2 ** r = 0` by rw_tac arith_ss[ZERO_DIV]);\n\
e (metis_tac[ROOT_COMPUTE]);\n\
')
results.push('\
Theorem ROOT_EVAL[compute]:\n\
  !r n. ROOT r n =\n\
    if r = 0 then ROOT 0 n else\n\
    if n = 0 then 0 else\n\
    let m = TWICE (ROOT r (n DIV 2 ** r)) in\n\
    m + if (SUC m) ** r <= n then 1 else 0\n\
Proof\n\
  rpt strip_tac >>\n\
  (Cases_on `r = 0` >> asm_simp_tac arith_ss[LET_THM]) >>\n\
  `0 < r` by decide_tac >>\n\
  (Cases_on `n = 0` >> asm_simp_tac arith_ss[Once ROOT_COMPUTE, LET_THM]) >>\n\
  `0 DIV 2 ** r = 0` by rw_tac arith_ss[ZERO_DIV] >>\n\
  metis_tac[ROOT_COMPUTE]\n\
QED\
')

/* Note:
   For Javascript line continuation, line must end in \, not \(some spaces).
   Anywhere in the whole conitinuation, extra space after \ will give:
   Unexpected token ILLEGAL.
   Also, any \ (e.g. \/, /\ or lambda) in the string needs escape: \\
   Also, escape 'a for its quote. Hint: turn on view of special char
*/

// check equality of two strings s1, s2
function check2(s1, s2) {
   if (s1 == s2) return '... pass' // true
   log('LENGTH ' + s1.length)
   log('LENGTH ' + s2.length)
   var m = Math.min(s1.length, s2.length)
   var k = 0
   for (var j = 0; j < m; j++) {
      if (s1.charAt(j) == s2.charAt(j)) k++
      else break
   }
   log('m=' + m + ', k=' + k)
   log('s1 ...: ' + s1.slice(k))   
   log('s2 ...: ' + s1.slice(k))   
   return '!!! fail' // false
}

/* Run from NodeJs.
$ node ProofFormatter.js run [example-number] 
*/
// check in browser (no process) or in console (with process)
if (typeof process !== 'undefined') {
   // replace log function
   log = console.log  
   function help() {
      log('Usage: node ProofFormatter.js            to give this help')
      log('       node ProofFormatter.js run [n]    to run example [n]')
      log('       node ProofFormatter.js test       to run tests')
      log('       node ProofFormatter.js debug [n]  to debug example [n]')
      log(' Note: total number of examples: ' + examples.length)   
   }

   // use default options 
   setKeywords(options.lcsymtacs)

   // run example
   function runExample(n) {
      if (isNaN(n) || n > examples.length || n < 1) {
         log('Valid example numbers for run are: 1 .. ' + examples.length)
         return
      }
      log('Running Example ' + n)
      try {
         // get source and apply formatting
         var source = examples[n-1] // index from 0
         // only example #18 uses new_style
         if (n == 18) {
            options.new_style = true
            options.store_thm = false
         }
         else {
            options.new_style = false
            options.store_thm = true
         }
         var result = beautify(source)
         var expect = results[n-1]
         log('Source:')
         log(source)
         log('Result:')
         log(result)
         log('Expect:')
         log(expect)
         log('Example ' + n + ': ' + (result == expect ? '... pass' : '!!! fail'))
      }
      catch (error) {
         throw error
      }
   }
   // debug example
   function debugExample(n) {
      var old = debug
      debug = true
      runExample(n)
      debug = old
   }
   // run tests
   function runTests() {
      log("Running Tests ...")
      for (var j = 0; j < examples.length; j++) {
         // log('Example ' + (j+1) + ': ' + (results[j]).length)
         // log('Example ' + (j+1) + ': ' + check2(beautify(examples[j]), results[j]))
         // only example #18 uses new_style
         if ((j + 1) == 18) {
            options.new_style = true
            options.store_thm = false
         }
         else {
            options.new_style = false
            options.store_thm = true
         }
         log('Example ' + (j+1) + ': ' + (beautify(examples[j]) == results[j] ? '... pass' : '!!! fail'))
      }
   }

   // main program
   var args = process.argv.slice(2)
   if (args.length == 0) help()
   else if (args[0] == 'test') runTests() 
   else if (args[0] == 'debug') debugExample(parseInt(args[1])) 
   else if (args[0] == 'run') runExample(parseInt(args[1])) 
   else help()
}