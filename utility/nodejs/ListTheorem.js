/*
 * ListTheorem.js -- listing of Theorems in .hol file
 *
 * DOS> node ListTheorem.js < file.hol
 *
 */
var readline = require('readline')

// create Readline interface
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false  // echo off
})

var count = 0    // line count
var echo = 0     // echo count
var theorem = [] // line for theorem
var partial = '' // partial line

// event handler of readline interface
rl.on('line', function(line) {
  count++
  if (partial != '') line = partial + line.replace(/^\s+/,'')
  if (line.match(/^STOP;/)) {
    console.log('Exit at line: ' + count)
    rl.close()
  }
  if (echo > 0) {
     if (echo == 2) {
       var match = line.match(/^\s+"(.+)"/)
       if (match) theorem.push(match[1]) // no quotes
       // no match, e.g. store_thm in comment
       else echo = 0 // echo will decrement to -1, no output
     }
     if (echo == 1) {
       var match = line.match(/^\s+``(.+)``/)
       if (match) {
         theorem.push(match[1]) // no double marks
         partial = ''
       }
       // no match, statement of theorem is multi-line
       else {
         line = line.replace(/\n/,' ') // remove LF
         // console.log('!! no match: ' + count + ': [' + line + ']')
	     partial = line
         echo = 2 // keep echo = 1 after decrement
       }
     }
     if (echo == 3) { // save_thm
       //console.log('line: [' + line + ']')
       var match = line.match(/> val (.+) = \|- (.+) : thm/)
       if (match) {
         theorem.push(match[1])
         theorem.push(match[2])
         echo = 1 // output when echo = 0 after decrement if match
       }
       else echo = 4 // keep echo = 3, look for match in next line
     }
     echo--
     if (echo == 0) {
       rl.output.write('val ' + theorem.join(' = |- ') + ' : thm\n')
       theorem = []
    }
  } 
  if (line.match(/store_thm/)) echo = 2 // capture next 2 lines
  if (line.match(/save_thm/)) echo = 3 // actually capture next 1 lines
}).on('close', function() {
  process.exit(0)
})
