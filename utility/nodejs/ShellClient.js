/*
 * ShellClient.js
 *
 * This is the client program for Shell Server (ShellServer.js)
 *
 * Example of use (assume Shell Server is running):
 * DOS> node ShellClient.js "/rpc/start/"   ""
 * DOS> node ShellClient.js "/rpc/command/" "2 + 3*4;"
 * DOS> node ShellClient.js "/rpc/control/" "$"
 *
 * Note:
 * JSON is built into V8.
 * http://code.google.com/p/v8/source/browse/trunk/src/json.js
 */
var http = require("http")

var host = 'localhost' // otherwise IP address
var debug = false // true or false
var LF = '\n'

// check args
var args = process.argv.slice(1)
if (args.length < 3) {
    console.log('Usage: node ShellClient.js [query] [data]         (for port 80)')
    console.log('   or  node ShellClient.js [port] [query] [data]  (for specified port)')
    return
}

// get query and data from args
var port  = args.length > 3 ? args[1] : 80  // default PORT 80
var query = args.length > 3 ? args[2] : args[1]
var data  = args.length > 3 ? args[3] : args[2]

if (debug) console.log('Shell client port: ' + port)
if (debug) console.log('Shell client query: ' + query)
if (debug) console.log('Shell client data: ' + data)

// set up POST option with query
var options = {
    host: host,
    port: port,
    path: query, // query
    headers: {}, // must be {}, not []
    method: 'POST'
}
options.headers['client-uid'] = 0

// send POST request, with response callback
var request = http.request(options, function(response) {
    if (debug) console.log('status: ' + response.statusCode)
    if (debug) console.log('headers: ' + JSON.stringify(response.headers))
    response.setEncoding('UTF8')
    response.on('data', function (chunk) {
        process.stdout.write(chunk);
    })
})
// tell request error
request.on('error', function(e) {
    console.log('problem with request: ' + e.message);
})
// send POST data and end
request.write(data)
if (query == '/rpc/command/') request.write(LF)
// LF is only for HOL, but command line cannot easily type in LF.
request.end()
