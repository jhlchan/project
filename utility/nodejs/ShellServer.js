/*
 * Objectives:
 * 1. Respond to requests from Shell.html.
 * 2. Reponses include: GET and POST.
 * 3. POST data will be commands send to Launcher, response are output from Launcher (see Documentation at the end).
 * 4. (ToDo) Server side logging (logOpen and logClose).
 *
 * DOS> node ShellServer.js "go hol" "- "
 * To kill the server:
 * DOS> taskkill /F /IM node.exe && taskkill /F /IM mosml.exe
 *
 * For logging: just highlight and copy the web display.
 *
 * Usage:  node ShellSever.js [shell] [prompt] [port]
 *
 */
var sys = require("sys"),
    http = require("http"),
    path = require("path"),
    url = require("url"),
    fs = require("fs"),
    child = require('child_process')

// check args
var args = process.argv.slice(1)
if (args.length < 3) {
    console.log('Usage: node ShellServer.js [shell] [prompt]        (for port 80)')
    console.log('   or  node ShellServer.js [shell] [prompt] [port] (for specified port)')
    return
}
var shell = args[1]
var prompt = args[2]
var port = args[3] || 80  // default PORT = 80
// where is this script
var root = (args[0].match(/(.+)\//) || args[0].match(/(.+)\\/))[1]
var cwd = process.cwd()
// process.execPath.match(/(.+)\\/)[1]) is process execution location (where go.bat is located)

console.log(' shell:', shell)
console.log('prompt:', prompt)
console.log('  port:', port)
console.log('  root:', root) // process starting root (where this script (ShellServer.js) is located)
console.log('   cwd:', cwd) // process starting location (where you type: go node shell)

var debug = true  // true or false
var start = false // set to true by /rpc/start/
var PROMPT = '#'  // common prompt between client and server
var PMATCH = new RegExp(prompt + ' $') // server match real prompt

// do GET
function doGet(request,response) {
   var name = url.parse(request.url).pathname
   if (debug) console.log('name=['+name+']')
   // special treatment for /Shell.html, /Shell.js and favicon.ico
   var dir = (name == '/Shell.html' || name == '/Shell.js' || name == 'favicon.ico') ? root : cwd
   var fileName = path.join(dir,name)
   sys.puts('GET: ' + fileName)
   fs.exists(fileName, function(exists){
      if (!exists) {
         response.writeHeader(404, {"Content-Type": "text/plain"})
         response.write("404 Not Found\n")
         response.end()
      }
      else {
         fs.readFile(fileName, "binary", function(err, file) {
            if (err) {
               response.writeHeader(500, {"Content-Type": "text/plain"})
               response.write(err + '\n')
               response.end()
            }
            else {
               response.writeHeader(200)
               response.write(file, "binary")
               response.end()
            }
         })
      }
   })
}

/* Launcher object */

// Launcher object
var Launcher = new function() {
   var map = [] // map from uid to Launcher
   var count = 0  // count number of launchers

   // get Launcher based on uid
   this.get = function(uid) {
      var launcher = map[uid]
      if (launcher == null) {
         console.log('New Launcher for ['+uid+']')
         launcher = child.exec(shell)
         map[uid] = launcher
         count++
         if (count > 3) throw "Too Many Launchers!"

         launcher.on('exit', function (code) {
            console.log('Child process ends with exit code '+code)
            map[uid] = true
            count--
         })
      }
      if (debug) console.log('Launcher for ['+uid+']')
      return launcher
   }
}

// do POST
function doPost(request,response) {
   // inspect(request, 'request')
   // wait for data
   var data = ''
   request.on('data', function (chunk) {
      data += chunk
      if (debug) console.log('data: [' + chunk + ']')
   })
   // process data on end
   request.on('end', function () {
      // get POST query and data
      var query = request.url
      console.log('POST query: [' + query + ']')
      console.log('POST data: [' + data + ']')
      // check /rpc/start/ first
      if (query == '/rpc/start/')  {
         start = true
         // marker for shell prompt of server
         response.write('!@@' + prompt)
         // get response from launcher later
         query = '/rpc/command/'
         data = ''
      }
      // get uid from request headers
      var headers = request.headers
      // console.log('headers: ' + JSON.stringify(headers))
      var type = headers['content-type']
      var length = headers['content-length']
      var uid = headers['client-uid']
      console.log('Content-type: ' + type)
      console.log('Content-length: ' + length)
      console.log('Client-UID: ' + uid)
      if (!uid) {
         // ignore snooping clients with no uid
         console.log('Unidentified client detected!')
         // no response, let it hang
         return
      }

      // check if shell is started
      if (!start) {
         console.log('Shell not started.')
         // process.exit() -- too harsh
         // marker for Javascript evaluation
         response.write('!@')
         var text = ''
         try {
            text = eval(data)
         }
         catch (error) {
            text = '! Error: ' + error
         }
         // ensure text is string
         text = text  + '\n' + PROMPT + ' '
         response.write(text)
         response.end()
         return
      }
      // get launcher from uid
      var launcher = Launcher.get(uid)
      if (launcher == true) {
         // no launcher
         response.write('!@#$')
         response.end()
         return
      }

      // process query by launcher
      if (query == '/rpc/command/') {
         // just send data to launcher for processing, wait for response
         launcher.stdin.write(data)
      }
      else if (query == '/rpc/control/') {
         // check control char and check
         if (data.match(/^\^/)) { // convert to control code
            launcher.stdin.write(data.charCodeAt(1) - 64)
         }
         else if (data == '$') {
            // close launcher (client calls for stop)
            launcher.stdin.end()
            response.write('!@#$')
            response.end()
            return
         }
         else if (data.indexOf('$') == 0) {
            // $cmd
            shell = data.slice(1) // this is not used by now??
         }
         // if (data == '^C') running = false // to shutdown this server -- an overkill?
      }
      else {
         console.log('POST: Unknown protocol: '+data)
      }

      // capture launcher.stdout via 'data' events
      launcher.stdout.on('data', function(data) {
         if (debug) console.log('launcher data: [' + data + ']')
         var match = data.match(PMATCH)
         if (match) data = data.slice(0,-2) + PROMPT + ' ' // client will match PROMPT
         response.write(data) // send modified or unmodified data
         if (match) {
            if (debug) console.log('PROMPT data = [' + data + ']')
            response.end()
         }
      })
      // finish launcher.stdout via 'end' events
      launcher.stdout.on('end', function() {
         response.write('No more input.\n')
         response.end()
      })
   })
}

// File server with request detection
http.createServer(function(request,response){
   var method = request.method
   if (method == 'GET') doGet(request,response)
   if (method == 'POST') doPost(request,response)
}).on('end', function() {
   console.log('Server dies.')
}).listen(port)

sys.puts('Shell Server Running on port ' + port)

/**
 * Documentation
 *
 * 1. GET requests are local file retrieval (i.e. from the starting directory),
 *    except for special ones: Shell.html, Shell.js and favicon.ico.
 *    These special files always come from the same location as ShellServer.js (this file).
 * 2. POST data are commands, based on query type:
 *    query: /rpc/start/     data: initialize Laucncher by starting shell (e.g. "go hol").
 *    query: /rpc/command/   data: will be input data sent to Launcher.
 *    query: /rpc/control/   data: will be special command (e.g. interrupts, exit, or CRTL-chars.
 * 3. For /rpc/command/, the response ends when the PROMPT is detected from Launcher.
 * 4. If server ends session, server sends to client (response): '!@#$' (4 top-keys).
 *    If client ends session, client sends to server  (request): CTRL-$ (simulated).
 */
