/*
 * HOLClient.js
 *
 * This is the client program for HOL Server (HOLServer.js)
 * Example of use (assume HOL Server is running):
 * $ node HOLClient.js "EVAL ``2 ** 7 - 2``;"
 * $ node HOLClient.js "g `!x y. x + y = y + x`;"
 * $ node HOLClient.js "e (SRW_TAC [][]);"
 * $ node HOLClient.js "drop();"
 * If port is not 8080, but say 81,
 * $ node HOLClient.js 81 "EVAL ``2 ** 7 - 2``;"
 * $ node HOLClient.js 81 "g `!x y. x + y = y + x`;"
 * $ node HOLClient.js 81 "e (SRW_TAC [][]);"
 * $ node HOLClient.js 81 "drop();"
 
 * Example of use (on Mac)
 * $ node HOLClient.js "3 + 4;"
 * $ node HOLClient.js "EVAL \`\`2 ** 7 - 2\`\`;"
 * $ The other command with g has a problem:
     if "!" is escaped by \!, the whole \! is sent to HOL
     if "!" is not escaped, it has special meaning to Unix
 */
var http = require("http")

var host = 'localhost' // otherwise IP address
var port = 8080  // for HOL Server
var debug = false // true or false

// check args
var args = process.argv.slice(1)
if (args.length < 2) {
    console.log('Usage: node HOLClient.js [data]          (for port 8080)')
    console.log('   or  node HOLClient.js [port] [data]   (for specified port)')
    return
}

// get input data from args
if (args.length > 2) port = args[1]
var data = args.length > 2 ? args[2] : args[1]
if (debug) console.log('HOL input: ' + data)
if (debug) console.log('HOL port: ' + port)

// set up POST option with query
var options = {
    host: host,
    port: port,
    path: '/rpc/command/', // query
    method: 'POST'
}

// send POST request, with response callback
var request = http.request(options, function(response) {
    if (debug) console.log('status: ' + response.statusCode)
    if (debug) console.log('headers: ' + JSON.stringify(response.headers))
    response.setEncoding('UTF8')
    response.on('data', function (chunk) {
        process.stdout.write(chunk)
    })
}).on('error', function(e) {
    console.log('problem with request: ' + e.message);
})
// send POST data and end
request.write(data)
request.write('\n') // LF is important for HOL session
request.end()

/* Program will stop when all pending events (e.g. response from server) are done. */
