/*
 * jsparse.js -- Parser Combinator in Javascript
 * 
 * original author: Chris Double (2007)
 *
 * DOS> node jsparse.js
 */

// ------------------------------------------------------------
// Utilities
// ------------------------------------------------------------

var Utility = new function() {

// Folding: iterate f(x,y) from x = initial for each y in list
this.fold = function(f, initial, list) {
    for(var j = 0; j < list.length; j++) initial = f(initial, list[j])
    return initial
}

this.prettyPrint = function(obj, indent) {
  if (indent == null) indent = ''
  // get type of object 
  var type = typeof(obj)
  // check primitive types
  if (type == 'function') return '[function]'
  if (type == 'string') return '"' + obj + '"'
  if (type != 'object') return obj // e.g. a number
  // pretty print object by property
  var result = [] // result buffer
  for (var property in obj) {
    var value = this.prettyPrint(obj[property], indent + '  ') // recursive call
    var key = (isFinite(property) ? '' : '"' + property + '" : ') // omit number index
    if (value != '[function]') result.push(indent + key + value) // skip functions
  }
  // different enclosing symbols for array or object
  var symbol = (obj instanceof Array) ? { open: '[', close: ']' } :
                                        { open: '{', close: '}' }
  return symbol.open + '\n' + result.join(',\n') + '\n' + indent + symbol.close
}
} // end Utility

// ------------------------------------------------------------
// ParseState object
// ------------------------------------------------------------
var ParseState = new function() {

// For use of cache
var memoize = true;

// Constructor (null index means 0)
var PS = function(input, index) {
    this.input = input
    this.index = index || 0
    this.length = input.length - this.index
    this.cache = { }
    return this
}
// Convenient constructor for PS (implicit index 0)
this.ps = function(str) {
    return new PS(str)
}
// Extensions of PS prototype
// The input substring(start, end) relative to index.
PS.prototype.substring = function(start, end) {
    return this.input.substring(start + this.index, (end || this.length) + this.index);
}
// String representation
PS.prototype.toString = function() {
    return 'PS: "' + this.substring(0) + '"'
}
// A ParseState with index advanced by amount (hence reducing its length).
PS.prototype.from = function(amount) {
    var ps = new PS(this.input, this.index + amount)
    ps.cache = this.cache
    ps.length = this.length - amount
    return ps
}
// A ParseState with input string leading spaces trimmed.
PS.prototype.trimLeft = function() {
    var s = this.substring(0)
    var m = s.match(/^\s+/)
    return m ? this.from(m[0].length) : this
}
// The input string character at (index) relative to index.
PS.prototype.at = function(index) {
    return this.input.charAt(this.index + index)
}
// Get the cache identified by pid
PS.prototype.getCached = function(pid) {
    if(!memoize) return false
    var p = this.cache[pid]
    return p ? p[this.index] : false
}
// Associate the pid with the cache
PS.prototype.putCached = function(pid, cached) {
    if(!memoize) return false
    var p = this.cache[pid]
    if (!p) p = this.cache[pid] = { }
    p[this.index] = cached
}
} // end ParseState

// ------------------------------------------------------------
// Basic Parsers
// ------------------------------------------------------------
var Parser = new function() {

// Make a parse result:
// 'r' is the remaining string to be parsed.
// 'matched' is the portion of the string that was successfully matched by the parser.
// 'ast' is the AST returned by the successfull parse.
function make_result(r, matched, ast) {
    return { remaining: r, matched: matched, ast: ast }
}

// Global counter for parser combinator
var parser_id = 0

// 'token' is a parser combinator that:
// given a string, returns a parser that parses that string value.
// The AST contains the string that was parsed.
this.token = function(s) {
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var r = state.length >= s.length && state.substring(0,s.length) == s
        cached = r ? make_result(state.from(s.length), s, s) : false
        savedState.putCached(pid, cached)
        return cached
    }
}

// Like 'token' but for a single character. Returns a parser that:
// given a string containing a single character, parses that character value.
this.ch = function(c) {
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var r = state.length > 0 && state.at(0) == c
        cached = r ? make_result(state.from(1), c, c) : false
        savedState.putCached(pid, cached)
        return cached
    }
}

// 'range' is a parser combinator that returns a single character parser (similar to 'ch').
// It parses a single character in the inclusive range of 'lower' and 'upper' bounds
// ("a" to "z" for example).
this.range = function(lower, upper) {
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        cached = false
        if(state.length > 0) {
            var c = state.at(0)
            if(c >= lower && c <= upper) cached = make_result(state.from(1), c, c)
        }
        savedState.putCached(pid, cached)
        return cached
    }
}

// ------------------------------------------------------------
// Parser Combinators
// ------------------------------------------------------------

// Helper function to convert string literals to token parsers
// and perform other implicit parser conversions.
function toParser(p) {
    return typeof(p) == "string" ? Parser.token(p) : p
}

// Returns a parser that skips whitespace before applying parser.
this.whitespace = function(p) {
    var p = toParser(p)
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        cached = p(state.trimLeft())
        savedState.putCached(pid, cached)
        return cached
    }
}

// Parser combinator that passes the AST generated from the parser 'p'
// to the function 'f'. The result of 'f' is used as the AST in the result.
this.action = function(p, f) {
    var p = toParser(p)
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        cached = false
        var x = p(state)
        if(x) {
            x.ast = f(x.ast)
            cached = x
        }
        savedState.putCached(pid, cached)
        return cached
    }
}

// Given a parser that produces an array as an ast,
// returns a parser that produces an ast with the array joined by a separator.
this.join_action = function(p, sep) {
    return action(p, function(ast) {
        return ast.join(sep)
    })
}

// Given an ast of the form [ Expression, [ a, b, ...] ],
// convert to [ [ [ Expression [ a ] ] b ] ... ]
// This is used for handling left recursive entries in the grammar. e.g.
// MemberExpression:
//   PrimaryExpression
//   FunctionExpression
//   MemberExpression [ Expression ]
//   MemberExpression . Identifier
//   new MemberExpression Arguments
this.left_factor = function(ast) {
    return Utility.fold(function(v, action) { return [v, action] },
                        ast[0],
                        ast[1])
}

// Return a parser that left factors the ast result of the original parser.
this.left_factor_action = function(p) {
    return action(p, left_factor)
}

// 'negate' will negate a single character parser.
// So given 'ch("a")' it will successfully parse any character except for 'a'.
// Or 'negate(range("a", "z"))' will successfully parse anything except the lowercase characters a-z.
this.negate = function(p) {
    var p = toParser(p)
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        cached = false
        if(state.length > 0) {
            var r = p(state)
            if(!r) cached =  make_result(state.from(1), state.at(0), state.at(0))
        }
        savedState.putCached(pid, cached)
        return cached
    }
}

// 'end_p' is a parser that is successful if the input string is empty (ie. end of parse).
this.end_p = function(state) {
    if(state.length == 0) return make_result(state, undefined, undefined)
    return false
}

// 'nothing_p' is a parser that always fails.
this.nothing_p = function(state) {
    return false
}

// 'sequence' is a parser combinator that processes a number of parsers in sequence.
// It can take any number of arguments, each one being a parser.
// The parser that 'sequence' returns succeeds if all the parsers in the sequence succeeds.
// It fails if any of them fails.
this.sequence = function() {
    var parsers = []
    for(var i = 0; i < arguments.length; ++i) parsers.push(toParser(arguments[i]))
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var ast = []
        var matched = ""
        var i
        for(i=0; i< parsers.length; ++i) {
            var parser = parsers[i]
            var result = parser(state)
            if(!result) break
            state = result.remaining
            if(result.ast != undefined) {
                ast.push(result.ast)
                matched = matched + result.matched
            }
        }
        cached = (i == parsers.length) ? make_result(state, matched, ast) : false
        savedState.putCached(pid, cached)
        return cached
    }
}

// Like sequence, but ignores whitespace between individual parsers.
this.wsequence = function() {
    var parsers = []
    for(var i = 0; i < arguments.length; ++i) {
        parsers.push(this.whitespace(toParser(arguments[i])))
    }
    return this.sequence.apply(null, parsers)
}

// 'choice' is a parser combinator that provides a choice between other parsers.
// It takes any number of parsers as arguments and returns a parser that will try
// each of the given parsers in order. The first one that succeeds results in a
// successfull parse. It fails if all parsers fail.
// Restriction? The choice(token('THEN'), token('THENL')) does not work
//          However choice(token('THENL'), token('THEN')) works!
this.choice = function() {
    var parsers = []
    for(var i = 0; i < arguments.length; ++i) parsers.push(toParser(arguments[i]))
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached
// console.log('state=' + state)
        var i
        for(i = 0; i < parsers.length; ++i) {
            var parser = parsers[i]
//console.log('parser=' + parser)
            var result = parser(state)
// console.log('result=' + JSON.stringify(result)) -- result is circular!
            if(result) break
        }
        cached = (i == parsers.length) ? false : result
        savedState.putCached(pid, cached)
        return cached
    }
}

// 'butnot' is a parser combinator that takes two parsers, 'p1' and 'p2'.
// It returns a parser that succeeds if 'p1' matches and 'p2' does not, or
// 'p1' matches and the matched text is longer that p2's.
// Useful for things like: butnot(IdentifierName, ReservedWord)
this.butnot = function(p1,p2) {
    var p1 = toParser(p1)
    var p2 = toParser(p2)
    var pid = parser_id++

    // match a but not b. if both match and b's matched text is shorter
    // than a's, a failed match is made
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var br = p2(state)
        if(!br) {
            cached = p1(state)
        } else {
            cached = false
            var ar = p1(state)
            if (ar && ar.matched.length > br.matched.length) cached = ar
        }
        savedState.putCached(pid, cached)
        return cached
    }
}

// 'difference' is a parser combinator that takes two parsers, 'p1' and 'p2'.
// It returns a parser that succeeds if 'p1' matches and 'p2' does not.
// If both match then if p2's matched text is shorter than p1's it is successfull.
this.difference = function(p1,p2) {
    var p1 = toParser(p1)
    var p2 = toParser(p2)
    var pid = parser_id++

    // match a but not b. if both match and b's matched text is shorter
    // than a's, a successfull match is made
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var br = p2(state)
        if(!br) {
            cached = p1(state)
        } else {
            var ar = p1(state)
            cached = (ar.matched.length >= br.matched.length) ? br : ar
        }
        savedState.putCached(pid, cached)
        return cached
    }
}

// 'xor' is a parser combinator that takes two parsers, 'p1' and 'p2'.
// It returns a parser that succeeds if 'p1' or 'p2' match but fails if they both match.
this.xor = function(p1, p2) {
    var p1 = toParser(p1)
    var p2 = toParser(p2)
    var pid = parser_id++

    // match a or b but not both
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var ar = p1(state)
        var br = p2(state)
        cached = (ar && br) ? false : ar || br
        savedState.putCached(pid, cached)
        return cached
    }
}

// A parser combinator that takes one parser.
// It returns a parser that looks for zero or more matches of the original parser.
this.repeat0 = function(p) {
    var p = toParser(p)
    var pid = parser_id++

    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var ast = []
        var matched = ""
        var result
        while(result = p(state)) {
            ast.push(result.ast)
            matched = matched + result.matched
            if(result.remaining.index == state.index) break
            state = result.remaining
        }
        cached = make_result(state, matched, ast)
        savedState.putCached(pid, cached)
        return cached
    }
}

// A parser combinator that takes one parser.
// It returns a parser that looks for one or more matches of the original parser.
this.repeat1 = function(p) {
    var p = toParser(p)
    var pid = parser_id++

    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var ast = []
        var matched = ""
        var result= p(state)
        if(!result)
            cached = false;
        else {
            while(result) {
                ast.push(result.ast)
                matched = matched + result.matched
                if(result.remaining.index == state.index) break
                state = result.remaining
                result = p(state)
            }
            cached = make_result(state, matched, ast)
        }
        savedState.putCached(pid, cached)
        return cached
    }
}

// A parser combinator that takes one parser.
// It returns a parser that matches zero or one matches of the original parser.
this.optional = function(p) {
    var p = toParser(p)
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var r = p(state)
        cached = r || make_result(state, "", false)
        savedState.putCached(pid, cached)
        return cached
    }
}

// A parser combinator that ensures that the given parser succeeds but
// ignores its result. This can be useful for parsing literals that you
// don't want to appear in the ast. eg:
// sequence(expect("("), Number, expect(")")) => ast: Number
this.expect = function(p) {
    return this.action(p, function(ast) { return undefined })
}

// A parse combinator that chains a parser p for an item and a parser s for a separator.
this.chain = function(p, s, f) {
    var p = toParser(p)
    return this.action(this.sequence(p, this.repeat0(this.action(this.sequence(s, p), f))),
                  function(ast) {
                      return [ast[0]].concat(ast[1])
                  })
}

// A parser combinator to do left chaining and evaluation.
// Like 'chain', it expects a parser for an item and for a seperator.
// The seperator parser's AST result should be a function of the form:
//     function(lhs,rhs) { return x; }
// where 'x' is the result of applying some operation to lhs and rhs AST's from item parser.
this.chainl = function(p, s) {
    var p = toParser(p)
    return this.action(this.sequence(p, this.repeat0(this.sequence(s, p))),
                  function(ast) {
                      return Utility.fold(function(v, action) { return action[0](v, action[1]) }, ast[0], ast[1])
                  })
}

// Puzzle: above require "this", but not below, why?
// Answer: because this.list has not been called. Once called, same error.

// A parser combinator that returns a parser that matches lists of things.
// The parser to match the list item and the parser to match the seperator need to
// be provided. The AST is the array of matched items.
this.list = function(p, s) {
    return this.chain(p, s, function(ast) { return ast[1] })
}

// Like list, but ignores whitespace between individual parsers.
this.wlist = function() {
    var parsers = []
    for(var i=0; i < arguments.length; ++i) {
        parsers.push(whitespace(arguments[i]))
    }
    return this.list.apply(null, parsers)
}

// A parser that always returns a zero length match
this.epsilon_p = function(state) {
    return make_result(state, "", undefined)
}

// Allows attaching of a function anywhere in the grammer.
// If the function returns true then parse succeeds otherwise it fails.
// Can be used for testing if a symbol is in the symbol table, etc.
this.semantic = function(f) {
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        cached = f() ? make_result(state, "", undefined) : false
        savedState.putCached(pid, cached)
        return cached
    }
}

// The and predicate asserts that a certain conditional syntax is satisfied
// before evaluating another production. Eg:
// sequence(and("0"), oct_p)
// (if a leading zero, then parse octal)
// It succeeds if 'p' succeeds and fails if 'p' fails.
// It never consume any input however, and doesn't put anything in the resulting AST.
this.and = function(p) {
    var p = toParser(p)
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        var r = p(state)
        cached = r ? make_result(state, "", undefined) : false
        savedState.putCached(pid, cached)
        return cached
    }
}

// The opposite of 'and'. It fails if 'p' succeeds and succeeds if 'p' fails.
// It never consumes any input.
// This combined with 'and' can be used for 'lookahead' and disambiguation of cases.
//
// Compare:
// sequence("a",choice("+","++"),"b")
//   parses a+b
//   but not a++b because the + matches the first part and peg's don't
//   backtrack to other choice options if they succeed but later things fail.
//
// sequence("a",choice(sequence("+", not("+")),"++"),"b")
//    parses a+b
//    parses a++b
//
this.not = function(p) {
    var p = toParser(p)
    var pid = parser_id++
    return function(state) {
        var savedState = state
        var cached = savedState.getCached(pid)
        if(cached) return cached

        cached = p(state) ? false : make_result(state, "", undefined)
        savedState.putCached(pid, cached)
        return cached
    }
}
} // end Parser

var stack = []
stack.push(1)
stack.push(2)
stack.push(3)
stack.push(4)
console.log('Stack = [1,2,3,4]')
console.log(stack.length)
console.log(Utility.prettyPrint(stack))
console.log('ParseState for: 1+2*3-4')
console.log(Utility.prettyPrint(ParseState.ps('1+2*3-4')))

// This is example3.js
// Evaluates as it parses the sample grammar
//
// From http://en.wikipedia.org/wiki/Parsing_expression_grammar
//
// Value   := [0-9]+ | '(' Expr ')'
// Product := Value   (['*' | '/'] Value)*
// Sum     := Product (['+' | '-'] Product)*
// Expr    := Sum
//

var Number = Parser.action(Parser.repeat1(Parser.range('0','9')),
                           function(ast) { return parseInt(ast.join("")) }
             )
function operator_action(p, func) {
    return Parser.action(p, function(ast) { return func })
}

var Times   = operator_action('*', function(lhs,rhs) { return lhs*rhs })
var Divides = operator_action('/', function(lhs,rhs) { return lhs/rhs })
var Plus    = operator_action('+', function(lhs,rhs) { return lhs+rhs })
var Minus   = operator_action('-', function(lhs,rhs) { return lhs-rhs })

// BNF:
// Value   := [0-9]+ | '(' Expr ')'
// Product := Value   (['*' | '/'] Value)*
// Sum     := Product (['+' | '-'] Product)*
// Expr    := Sum

// Forward definitions required due to lack of laziness in JS
var Expr = function(state) { return Expr(state) }

var Value   = Parser.choice(Number,  Parser.sequence(Parser.expect('('), Expr, Parser.expect(')')))
var Product = Parser.chainl(Value,   Parser.choice(Times, Divides))
var Sum     = Parser.chainl(Product, Parser.choice(Plus,  Minus))
var Expr    = Sum

// Usage: Expr(ps("1+2*3-4")).ast  is a value
console.log('Evaluate: 1+2*3-4')
console.log(Expr(ParseState.ps('1+2*3-4')).ast)

// This is example2.js
// Produces an AST of the expression

// AST objects
function Operator(symbol, lhs, rhs) {
    this.symbol = symbol
    this.lhs = lhs
    this.rhs = rhs
    this.toString = function() { return uneval(this) }
}

function operator_build(p) {
    return Parser.action(p, function(ast) {
              return function(lhs,rhs) { return new Operator(ast, lhs, rhs) }
           })
}

// BNF:
// Value   := [0-9]+ | '(' Expr ')'
// Product := Value   (['*' | '/'] Value)*
// Sum     := Product (['+' | '-'] Product)*
// Expr    := Sum

// Forward definitions required due to lack of laziness in JS
var BExpr = function(state) { return BExpr(state) }

var BValue   = Parser.choice(Number,   Parser.sequence(Parser.expect('('), BExpr, Parser.expect(')')))
var BProduct = Parser.chainl(BValue,   operator_build(Parser.choice('*', '/')))
var BSum     = Parser.chainl(BProduct, operator_build(Parser.choice('+', '-')))
var BExpr    = BSum

// Usage: BExpr(ps("1+2*3-4")).ast  is a tree
console.log('Parse tree for: 1+2*3-4')
console.log(Utility.prettyPrint(BExpr(ParseState.ps('1+2*3-4')).ast))

var Word     = Parser.action(Parser.repeat1(Parser.range('a','z')), function(ast) { return ast.join('') } )
var TermBody = Parser.action(Parser.repeat1(Parser.negate('``')),   function(ast) { return ast.join('') } )
var Term     = Parser.action(Parser.sequence('``', TermBody, '``'), function(ast) { return ast.join('') } )
var WTerm    = Parser.choice(Word, Term)

var CExpr    = function(state) { return CExpr(state) }
// var CList    = function(state) { return CList(state) }
var CValue   = Parser.whitespace(Parser.choice(WTerm, Parser.sequence(Parser.expect('('), CExpr, Parser.expect(')'))))
var CList    = Parser.whitespace(Parser.sequence('[', Parser.chainl(CExpr, operator_build(Parser.whitespace(','))), ']'))
var CSum     = Parser.chainl(Parser.choice(CValue, CList), operator_build(Parser.whitespace(Parser.choice('THENL', 'THEN1', 'THEN'))))
var CExpr    = CSum
console.log('Parse tree for: abc THEN1 (xyz THENL ``pqr`` THEN rst)')
console.log(Utility.prettyPrint(CExpr(ParseState.ps('abc THEN1 (xyz THENL ``pqr`` THEN rst)')).ast))
console.log('Parse tree for: another one')
console.log(Utility.prettyPrint(CExpr(ParseState.ps('abc THEN1 xyz THENL [``pqr``, abc] THEN rst')).ast))

process.exit(0)
