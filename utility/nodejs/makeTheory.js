//
// makeTheory.js -- make a HOL theory file from .hol script.
//
// This is using NodeJs to keep Unicode.
// CScript //U is not really working (maybe XP issue).
// DOS> node makeTheory.js [file]
//

/*
 Algorithm: (very simple):
 * Lines are read from standard input.
 * The line is filtered, either modified or unmodified.
 * If echo, the filtered line is sent to standard output

 The global flags are:
 * echo     true to output line, false the line is suppressed
 * stop     false will take reading, true will ignore reading, can only switch on
 * skip     false will take reading, true will ignore reading, can turn on/off
 These flags are independent, for nesting of features.
 
 The filtering: see function filter(s).
*/


/*
 Bug: when the end_mark is missing, a theorem is cutoff.
 To fix: have a mark buffer.
         When getting a start_mark, buffer should be empty.
         When getting an end_mark, buffer should be one.
 12/09/20:
 -- allow SKIP inline, (* SKIP  .... -- SKIP *)
 01/12/21:
 Bug: Overload "==" = ``fequiv`` is commented out.
 To fix: change load_mark = /load \"(.+)\"/
             to load_mark = /\sload \"(.+)\"/
 -- add DOSKIP: ..... (* DOSKIP *)   will skip entire line
 -- add UNSKIP: (* .... *) (* UNSKIP *) will echo content of preceding comment
*/

// print utility
function print(s) {
  process.stdout.write(s + '\n')
}

// trim trailing spaces
String.prototype.rtrim = function() {
  return this.replace(/\s+$/g,'')
}

// global name of theory
var theoryName = "SomeTheory"

// set name of theory from filename
function setTheoryName(s) {
  var match = /(.+)\.(.+)/.exec(s); // name.ext = $1.$2
  if (match) theoryName = match[1]  // match $1
  else theoryName = s               // no match
}

// get prefix lines
function getPrefix() {
  return "(*===========================================================================*)\n" +
         "\n" +
         "(* add all dependent libraries for script *)\n" +
         "open HolKernel boolLib bossLib Parse;\n" +
         "\n" +
         "(* declare new theory at start *)\n" +
         "val _ = new_theory \""+theoryName+"\";\n" +
         "\n" +
         "(* ------------------------------------------------------------------------- *)"
}

// get suffix lines
function getSuffix() {
  return "(* ------------------------------------------------------------------------- *)\n" +
         "\n" +
         "(* export theory at end *)\n" +
         "val _ = export_theory();\n" +
         "\n" +
         "(*===========================================================================*)"
}

// get comment line from string s
function getComment(s) {
  return "(* " + s + " *)"
}

// get comment line content in string s
function getContent(s) {
  var h = s.indexOf("(* ")
  var k = s.indexOf(" *)")
  return s.slice(h + 3, k)
}

// replace "scratch" by theoryName
function replaceScratch(s) {
   return s.replace(/\"scratch\"/, '"' + theoryName + '"');
}

var echo = true
var stop = false
var skip = false
var start_mark = /^\(\* <<(.+)>> \*\)/
var end_mark = "drop()"
var load_mark = /\sload \"(.+)\"/
var scratch_mark = /\"scratch\"/
var prefix_mark = "(* prefix *)"
var suffix_mark = "(* suffix *)"
var quiet_mark = "quietdec"
var stop_mark = /^STOP;/
var skip_start = /^\(\* SKIP/
var skip_end = /^\-\- SKIP \*\)/
var skip_inline = "-- SKIP *)"
var do_skip = "(* DOSKIP *)"
var un_skip = "(* UNSKIP *)"
var proof_line = "(* Proof: *)"
var marks = [] // to check matching start/end marks
var line_count = 0  // global line count

// print a warning
function warn(s) {
  process.stderr.write(s + '\n')
}

// check for mark lines, start_mark gives s, end_mark no s
function checkMarkLines(s) {
  var mark
  if (s) {
    // start_mark
    while (marks.length > 0) {
       mark = marks.pop()
       warn('Missing drop at line ' + mark[1] + ' for ' + mark[0])
    }
    marks.push([s, line_count])
  }
  else {
    // end_mark
    if (marks.length == 0) {
      warn('Unexpected drop at line ' + line_count)
    }
    else {
      mark = marks.pop() // ignore this
      while (marks.length > 0) {
        mark = marks.pop()
        warn('Orphan start at line ' + mark[1] + ' for ' + mark[0])
      }
    }
  }
}

// filter for .hol script
function filter(s) {
  s = s.rtrim() // trim line for pattern matching
  line_count++  // source line count

  // check skip and stop first
  if (skip && !s.match(skip_end)) return // skip takes priority
  if (stop) return // skip permanently

  // start of pre-filtering, get information and set flags
  if (s.indexOf(prefix_mark) !== -1) s = getPrefix() // encounter prefix_mark
  if (s.indexOf(suffix_mark) !== -1) s = getSuffix() // encounter suffix_mark
  if (s.match(load_mark)) s = getComment(s) // convert a load line to comment
  if (s.match(scratch_mark)) s = replaceScratch(s) // replace "scratch" by theoryName

  // start_mark is anything (* <<...>> *), mark of a theorem:
  //    . turn echo to false, put this line to marks[] stack
  //    . when end_mark  drop()  is detected, echo is true, pop off line from marks[]
  if (s.match(start_mark)) { echo = false; checkMarkLines(s) }
  if (s.match(quiet_mark)) echo = false // quiet line, no echo
  if (s.match(skip_start)) skip = true  // encounter SKIP, set skip
  if (s.indexOf(do_skip) !== -1) skip = true // encounter DOSKIP
  if (s.indexOf(un_skip) !== -1) s = getContent(s) // convert comment to content
  if (s.match(stop_mark) && !stop) { stop = true; checkMarkLines('dummy'); print(getSuffix()) }
  // end of pre-filtering

  // Now, decide whether to output line
  if (echo && !skip && !stop) print(s)

  // post-filtering, update flags
  if (s.indexOf(end_mark) == 0) { echo = true; checkMarkLines() } // encounter drop(), turn on echo
  if (s.match(quiet_mark)) echo = true // quiet line, turn back echo (only suppress this quiet line)
  if (s.match(skip_end)) skip = false // encounter -- SKIP at start, turn off skip
  if (s.indexOf(skip_inline) !== -1) skip = false // encounter -- SKIP, turn off skip
  if (s.indexOf(do_skip) !== -1) skip = false // encounter DOSKIP, turn off skip

  // check illegal line
  if (s.indexOf(proof_line) !== -1) warn(proof_line + ' at line: ' + line_count);
}

// main program
if (process.argv.length < 3) {
  console.log('Usage: node ' + process.argv[1] + ' [xyz.hol] > [xyzScript.sml]')
  process.exit(0)
}
var filename = process.argv[2]
setTheoryName(filename)
require('readline').createInterface({
  input: require('fs').createReadStream(filename),
  // encoding: "utf8", (not needed, alreay so by default?)
  terminal: false
}).on('line', function(line){
  // process.stdout.write('Line: ' + line)
  filter(line)
})
