/*
 * HOLTheoryRun.js
 *
 * This is to run a HOL Theory script through the HOL Proof Manager
 * Example of use (assume HOL Server is running):
 * DOS> node HOLTheoryRun.js demoScript.sml
 * DOS> node HOLTheoryRun.js %HOL_HOME%\examples\computability\goedelCodeScript.sml
 *
 */
var http = require("http")

var host = 'localhost' // otherwise IP address
var port = 8080  // for HOL Server
var debug = false // true or false
var echo = false  // echo input line
var LF = '\n'
var stdout = process.stdout
var LIMIT = 9 // set limit for chunks

/* The algorithm:
 * 1. Content of script (from web server) is converted to an array of lines.
 * 2. The lines are fed into a Chunk reader.
 * 3. The Chunk reader will do somethig smart when the chunk is store_thm (or Q.store_thm).
 */

// ==============================================================================
// Parser for HOL Proof
// ==============================================================================
var Node = function(value, type) {
    this.type = type
    this.head = value
    this.rest = type == 'THEN1' ? null : []
    this.toString = function() {
        return JSON.stringify(this)
    }
}

var Parser = new function() {

    // parse proof into a tree (this is hand-coding)
    // This tree parsing is somehow done by Holmake -- how does Holmake parse store_thm?
    this.parse = function(proof) {
        if (debug) console.log('proof: [' + proof + ']')
        var tree = []
        if (proof == 'GEN_TAC THEN EVAL_TAC') {
            // construct the proof tree
            tree.push(new Node('GEN_TAC', 'THEN'))
            tree.push(new Node('EVAL_TAC', 'END'))
        }
        else if (proof == 'Induct THEN1 RW_TAC list_ss [GCODE_EMPTY] THEN RW_TAC list_ss [] THEN Cases_on `h` THEN EVAL_TAC THEN RW_TAC list_ss [ZERO_LT_EXP,ZERO_LESS_MULT] THEN METIS_TAC [INDEX_LESS_PRIMES,primePRIMES,NOT_PRIME_0, DECIDE ``(x=0) \\/ 0<x``]') {
            // construct the proof tree
            tree.push(new Node('Induct', 'THEN1'))
            tree.push(new Node('RW_TAC list_ss [GCODE_EMPTY]', 'THEN'))
            tree.push(new Node('RW_TAC list_ss []', 'THEN'))
            tree.push(new Node('Cases_on `h`', 'THEN'))
            tree.push(new Node('EVAL_TAC', 'THEN'))
            tree.push(new Node('RW_TAC list_ss [ZERO_LT_EXP,ZERO_LESS_MULT]', 'THEN'))
            tree.push(new Node('METIS_TAC [INDEX_LESS_PRIMES,primePRIMES,NOT_PRIME_0, DECIDE ``(x=0) \\/ 0<x``]', 'END'))
        }
        else if (proof == 'Induct THEN RW_TAC list_ss [GCODE_EMPTY] THEN Cases_on `h` THEN EVAL_TAC THEN RW_TAC list_ss [ZERO_LT_EXP,ZERO_LESS_MULT] THEN METIS_TAC [INDEX_LESS_PRIMES,primePRIMES,NOT_PRIME_0, DECIDE ``(x=0) \\/ 0<x``]') {
            // construct the proof tree
            tree.push(new Node('Induct', 'THEN'))
            tree.push(new Node('RW_TAC list_ss [GCODE_EMPTY]', 'THEN'))
            tree.push(new Node('Cases_on `h`', 'THEN'))
            tree.push(new Node('EVAL_TAC', 'THEN'))
            tree.push(new Node('RW_TAC list_ss [ZERO_LT_EXP,ZERO_LESS_MULT]', 'THEN'))
            tree.push(new Node('METIS_TAC [INDEX_LESS_PRIMES,primePRIMES,NOT_PRIME_0, DECIDE ``(x=0) \\/ 0<x``]', 'END'))
        }
/*        
                          'RW_TAC bool_ss [GCODE_def] THEN MATCH_MP_TAC ONE_LT_MULT_IMP THEN CONJ_TAC THENL [RW_TAC bool_ss [GSYM ADD1,EXP] THEN METIS_TAC [ONE_LT_MULT_IMP,ONE_LT_PRIMES,ZERO_LT_PRIMES,ZERO_LT_EXP], METIS_TAC [ZERO_LT_GCODE]]'
*/     
        else if (proof == 'RW_TAC bool_ss [GCODE_def] THEN MATCH_MP_TAC ONE_LT_MULT_IMP THEN CONJ_TAC THENL [RW_TAC bool_ss [GSYM ADD1,EXP] THEN METIS_TAC [ONE_LT_MULT_IMP,ONE_LT_PRIMES,ZERO_LT_PRIMES,ZERO_LT_EXP], METIS_TAC [ZERO_LT_GCODE]]') {
            // construct the proof tree
            tree.push(new Node('RW_TAC bool_ss [GCODE_def]', 'THEN'))
            tree.push(new Node('MATCH_MP_TAC ONE_LT_MULT_IMP', 'THEN'))
            var branch = new Node('CONJ_TAC', 'THENL')
            tree.push(branch)
            // make branches for THENL
            branch.rest.push([
                new Node('RW_TAC bool_ss [GSYM ADD1,EXP]', 'THEN'),
                new Node('METIS_TAC [ONE_LT_MULT_IMP,ONE_LT_PRIMES,ZERO_LT_PRIMES,ZERO_LT_EXP]', 'END')
            ])
            branch.rest.push([
                new Node('METIS_TAC [ZERO_LT_GCODE]', 'END')
            ])
            // tree.push(new Node('NOOP', 'END') -- hope this is not required
        }
        if (debug) console.log('Parse tree: ' + JSON.stringify(tree))
        if (tree.length == 0) Web.doExit('No tree:\n[' + proof + ']')
        return tree
    }

    var path = '' // path of tree walk

    // walk proof tree to a string
    this.walk = function(tree) {
        // return '$' + tree.join('|\n') + '$\n'
        return JSON.stringify(tree) + '\n'
    }
}

function doTests() {
    var proofs = []
    proofs.push('GEN_TAC THEN EVAL_TAC')
    proofs.push('Induct THEN RW_TAC list_ss [GCODE_EMPTY] THEN Cases_on `h` THEN EVAL_TAC THEN RW_TAC list_ss [ZERO_LT_EXP,ZERO_LESS_MULT] THEN METIS_TAC [INDEX_LESS_PRIMES,primePRIMES,NOT_PRIME_0, DECIDE ``(x=0) \\/ 0<x``]')
    proofs.push('RW_TAC bool_ss [GCODE_def] THEN MATCH_MP_TAC ONE_LT_MULT_IMP THEN CONJ_TAC THENL [RW_TAC bool_ss [GSYM ADD1,EXP] THEN  METIS_TAC [ONE_LT_MULT_IMP,ONE_LT_PRIMES,ZERO_LT_PRIMES,ZERO_LT_EXP],  METIS_TAC [ZERO_LT_GCODE]]')

    if (debug) for (var j in proofs) console.log('proof[' + j + ']: ' + proofs[j])

    console.log(Parser.walk(Parser.parse(proofs[0])))
    console.log(Parser.walk(Parser.parse(proofs[1])))
    console.log(Parser.walk(Parser.parse(proofs[2])))

/*
[
{"head":"RW_TAC bool_ss [GCODE_def]", "rest":[]},
{"head":"MATCH_MP_TAC ONE_LT_MULT_IMP", "rest":[]},
{"head":"CONJ_TAC",
 "rest":[
  [{"head":"RW_TAC bool_ss [GSYM ADD1,EXP]","rest":[]},
   {"head":"METIS_TAC [ONE_LT_MULT_IMP,ONE_LT_PRIMES,ZERO_LT_PRIMES,ZERO_LT_EXP]","rest":[]}
  ],
  [{"head":"METIS_TAC [ZERO_LT_GCODE]","rest":[]}]
 ]
}
]
*/
}
//doTests()
//process.exit()

// ==============================================================================
// Chunk Reader
// ==============================================================================
// String prototype extension
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '')
}
String.prototype.noComment = function() {
   // comment: (*[anything]*)
   return this.replace(/\(\*(.+)\*\)/g, '')
}
String.prototype.noTrail = function() {
   return this.replace(/(\s+)$/, '')
}
String.prototype.lastChar = function() {
   return this.slice(-1, this.length)
}
String.prototype.has = function(s) {
   return this.indexOf(s) != -1
}

// A simple Event Emitter
var SimpleEE = function() {
  this.events = {}
  // with an internal buffer
  var buffer = []
  // operations on buffer
  this.put = function(line) {
     buffer.push(line)
  }
  this.getAll = function() {
     return buffer.join(LF)
  }
  this.clear = function() {
     buffer = []
  }
  this.quiet = false
  this.block = false
}

SimpleEE.prototype.on = function(eventname, callback) {
  // initialize this.events[eventname] if necessary
  this.events[eventname] || (this.events[eventname] = [])
  this.events[eventname].push(callback)
}

SimpleEE.prototype.emit = function(eventname) {
  var args = Array.prototype.slice.call(arguments, 1)
  if (this.events[eventname]) {
    /* IE does not support forEach nor apply
    this.events[eventname].forEach(function(callback) {
      callback.apply(this, args)
    })
    */
    for (var j in this.events[eventname]) {
      var callback = this.events[eventname][j]
      callback(args.toString()) // for line
    }
  }
}

// put a line, return true if chunk is reached
SimpleEE.prototype.isChunk = function(line) {
   if (echo) stdout.write(line + LF) // echo line
   // detect quiet mode
   if (line.indexOf('quietdec := true;') != -1) this.quiet = true
   if (line.indexOf('quietdec := false;') != -1) this.quiet = false
   // detect comment lines
   if (line.noComment() == '') line = ''
   // detect comment blocks
   if (line.match(/^\(\*/)) this.block = true
   // put line in buffer, if a chunk is detected do send
   if (line) { // not (null or '')
      if (!this.block) this.put(line)
      if (!this.quiet && !this.block && line.noTrail().lastChar() == ';') {
         this.emit('send')
         return true
      }
      if (line.match(/\*\)$/)) this.block = false
   }
   return line == null // false for ''
}

// Emitter to get chunks of input
// Need chunk of input (ending in ;) to get server response
// Just a line may or may not get server response
var emitter = new SimpleEE()

// put line to emitter for check
emitter.on('data', function(line) {
   emitter.isChunk(line)
})

// send a chunk for processing
emitter.on('send', function() {
   var chunk = emitter.getAll() // get all from buffer
   emitter.clear() // clear buffer
   Action.process(chunk)
})

// end of chunk input
emitter.on('end', function() {
   console.log('Bye.')
   process.exit()
})

var ChunkReader = new function() {

    var queue = [] // internal queue of lines

    // set the queue
    this.set = function(lines) {
       queue = lines
    }

    var count = 0

    // a chunk must end by semicolon, true = success
    this.doChunk = function() {
        // check LIMIT for now
        count++
        if (count > LIMIT) Web.doExit('Chunk LIMIT reached.')

        if (queue.length > 0) {
            // emitter has buffer and hasChunk checks
            while(!emitter.isChunk(queue.shift())) {}
        }
    }
}

// ==============================================================================
// Action -- to parse and act on a chunk
// ==============================================================================

var Action = new function() {

    // Batch only theories
    var BatchOnly = '/HolKernel/Parse/boolLib/bossLib/'
    // Preloaded theories
    var PreLoaded = '/arithmeticTheory/dividesTheory/pred_SetTheory/listTheory/ '

    // process a chunk
    this.process = function(chunk) {
        if (debug) stdout.write('Chunk: ' + LF)
        if (debug) stdout.write(chunk)
        if (debug) stdout.write(LF)
        // replace LF with space, and multiple spaces with a single space. then split by space
        var words = chunk.split(LF).join(' ').replace(/\s+/g, ' ').replace(';','').split(' ')
        if (debug) console.log('Number of words: ' + words.length)
        var key = words[0]
        words.shift() // discard key
        if (key == '') if (debug) console.log('Empty line!')
        if (key == '') return ChunkReader.doChunk() // next chunk
        if (key == 'open') {
            var toLoad = []
            var toOpen = []
            for (var j in words) {
                var theory = words[j]
                if (debug) console.log(theory)
                var word = '/' + theory + '/'
                if (!BatchOnly.has(word)) toOpen.push(theory)
                if (!BatchOnly.has(word) && !PreLoaded.has(word)) toLoad.push(theory)
            }
            // form a new chunk
            var chunk = []
            chunk.push('quietdec := true;')
            chunk.push('app load ["' + toLoad.join('", "') + '"];')
            chunk.push('open ' + toOpen.join(' ') + ';')
            chunk.push('quietdec := false;')
            Action.send(chunk.join(LF))
        }
        else if (key == 'val') {
            var name = words.shift()  // get variable
            words.shift()             // discard equal sign
            var value = words.shift() // get value
            if (name == '_' && value == 'new_theory') {
                // null action
                Action.send(null)
            }
            else if (words.length == 0) {
                // form a new chunk
                var chunk = []
                chunk.push('val ' + name + ' = ' + value + ';')
                Action.send(chunk.join(LF))
            }
            else if (value == 'Define') {
                // form a new chunk
                var chunk = []
                chunk.push('val ' + name + ' = ' + value + ' ' + words.join(' ') + ';')
                Action.send(chunk.join(LF))
            }
            else if (value == 'Q.store_thm' || value == 'store_thm') {
                Proof.analyze(name, value, words.join(' '))
            }
            else {
                console.log('To Do: ' + value + ' ' + words.join(' '))
            }
        }
        else {
            console.log('Not implemented: key=[' + key + ']')
        }
    }

    // send a chunk for server processing
    this.send = function(chunk) {
        if (chunk == null) return ChunkReader.doChunk()
        Web.doPost(chunk, function(data) {
           if (debug) console.log('Send Back: ')
           stdout.write(data)
           stdout.write(LF)
           ChunkReader.doChunk()
        })
    }
}

// Proof manipulator (talks to proof manager
var Proof = new function(proof) {

    // analyze a theorem store (the engine to run in Proof Manager)
    this.analyze = function(name, value, line) {
       console.log('Analyze line = [' + line + ']')
       var matches = line.match(/^\((.+?),(.+?),(.+?)\)$/) // ? for lazy = minimal match
       if (matches.length != 4) return console.log('Analyze syntax error: ' + line)
       var qname = matches[1].trim()
       var goal = matches[2].trim()
       var proof = matches[3].trim()
       if (qname != '"' + name + '"') console.log('Analyze warn - different quote name: ' + qname)
       console.log('Theorem name: [' + name + ']')
       console.log('Theorem goal: [' + goal + ']')
       console.log('Theorem proof: [' + proof + ']')
       // send a goal
       if (value == 'store_thm') goal = goal.match(/`(.+)`/)[1] // patch goal for store_thm
       Web.doPost('g ' + goal + ';', function(data) {
           if (debug) console.log('Set Goal: ')
           stdout.write(data)
           stdout.write(LF)
           if (data.has('Initial goal:')) return Proof.demo(name, proof)
           Proof.drop()
       })
    }

    // simulate proof of the given name
    this.demo = function(name, proof) {
        var tree = Parser.parse(proof) // internal proof tree
        if (debug); console.log('tree: ' + JSON.stringify(tree))
        Proof.run(tree, function(){
            Proof.done(name)
        })
    }

    // run a proof tree from stack
    this.run = function(tree, callback) {
        var index = 0 // for traversal of proof tree
        var branch = [] // the branch stack
        // for (var j in tree) Proof.step(tree[j]) -- convert to continuation
        var nextStep = function(flag) {
            index++
            if (flag == 1) index = tree.length // force branch
            if (flag > 1) { // branches
                if (debug); console.log('Branch at index = ' + index)
                for (; flag > 1; flag--) branch.push(index)
            }
            var noBranch = (flag == -1) 
            if (index < tree.length) return Proof.stepNode(tree[index], branch, noBranch, nextStep) // nextStep is recursive
            if (branch.length == 0) return callback() // no branch
            index = branch.pop()
            Proof.stepNode(tree[index], branch, noBranch, nextStep) // continue the proof
        }
        Proof.stepNode(tree[index], branch, false, nextStep)
    }

    // step a proof node
    this.stepNode = function(node, branch, noBranch, callback) {
        if (debug) console.log('node: ' + node + ' [' + (node instanceof Node) + ']')
        if (!(node instanceof Node)) Web.doExit('Error: Step to a non-node!')
        // can use shift: node.rest will not be reused
        var nextRest = function() {
            node.rest.shift() // discard used branch
            if (node.rest.length == 0) return callback(1) // 1 = branch done
            Proof.run(node.rest[0], nextRest) 
        }
        var step = node.head
        var type = node.type
        var flag = 0
        Web.doPost('e (' + step + ');', function(data) {
            if (debug) console.log('Step: ')
            stdout.write(data)
            stdout.write(LF)
            // check if goal is proved
            var ok = data.match(/\nGoal proved./)
            if (ok) {
                if (debug) console.log('Step: branch Done!')
                // indicate a branch done
                if (!noBranch) flag = 1
                // no branch for THEN1
            }
            else {
                // extract number of subgoals
                var match = data.match(/\n(\d+)\ssubgoal/)
                var ngoal = match ? match[1] : 0
                if (debug); console.log('Step: ' + type + ' subgoal = ' + ngoal)
                if (type == 'THENL') {
                    // this node has array in rest
                    if (debug); console.log('THENL: node.rest.length=' + node.rest.length)
                    if (ngoal != node.rest.length) Web.doExit('Error: THENL list does not match number of subgoals!')
                    return Proof.run(node.rest[0], nextRest)
                }
                else if (type == 'THEN' && ngoal != 1) {
                    // indicate a branch
                    flag = ngoal // ngoal > 1
                }
                else if (type == 'THEN1') {
                    // indicate next ok keep flag = 0
                    flag = -1
                }
            }
            callback(flag)
        })
    }

    // when a proof is done
    this.done = function(name) {
        stdout.write('(* Name the theorem for later reference: *)' + LF)
        Web.doPost('val ' + name + ' = save_thm("' + name + '", top_thm());', function(data) {
           if (debug) console.log('Step: ')
           stdout.write(data)
           stdout.write(LF)
           Proof.drop()
       })
    }

    // drop a proof
    this.drop = function() {
       Web.doPost('drop();', function(data) {
           if (debug) console.log('Drop Goal: ')
           stdout.write(data)
           stdout.write(LF)
           ChunkReader.doChunk()
       })
    }
}

// ==============================================================================
// Web -- using the HTTP protocol
// ==============================================================================

var Web = new function() {

    // Request GET method
    this.doGet1 = function(script, callback) {
        // set up GET option with path
        var options = {
            host: host,
            port: port,
            path: '/' + script, // filename as path
            headers: {}, // must be {}, not []
            method: 'GET'
        }
        // send GET request, with response callback
        var request = http.request(options, function(response) {
            if (debug) console.log('status: ' + response.statusCode)
            if (debug) console.log('headers: ' + JSON.stringify(response.headers))
            if (response.statusCode == 200) {
                response.setEncoding('UTF8')
                var data = []
                response.on('data', function(chunk) {
                    data.push(chunk)
                })
                response.on('end', function() {
                    callback(data)
                })
            }
        }).on('error', function(e) {
            console.log('problem with request: ' + e.message);
        })
        request.end() // to get response
    }

    // Direct GET method
    this.doGet2 = function(script, callback) {
        var href = 'http://' + host + ':' + port + '/' + script
        if (debug) console.log('href: ' + href)
        http.get(href, function(response) {
            if (debug) console.log('status: ' + response.statusCode)
            if (debug) console.log('headers: ' + JSON.stringify(response.headers))
            if (response.statusCode == 200) {
                response.setEncoding('UTF8')
                var data = []
                response.on('data', function(chunk) {
                    data.push(chunk)
                })
                response.on('end', function() {
                    callback(data)
                })
            }
        })
    }

    // GET file
    this.doGet = this.doGet2 // use Direct GET

    // POST data
    this.doPost = function(data, callback) {
        stdout.write('(* Post: *)' + LF)
        stdout.write(data)
        stdout.write(LF)
        // set up POST option with query
        var options = {
            host: host,
            port: port,
            path: '/rpc/command/', // query
            method: 'POST'
       }

        // send POST request, with response callback
        var request = http.request(options, function(response) {
            if (debug) console.log('status: ' + response.statusCode)
            if (debug) console.log('headers: ' + JSON.stringify(response.headers))
            response.setEncoding('UTF8')
            var data = []
            response.on('data', function (chunk) {
                data.push(chunk)
            })
            response.on('end', function() {
                callback(data.join(''))
            })
        }).on('error', function(e) {
            console.log('problem with request: ' + e.message);
        })
        // send POST data and end
        request.write(data)
        request.write(LF) // LF is important for HOL session
        request.end()
    }

    // for convenience
    this.doExit = function(s) {
       console.log('doExit: ' + s)
       process.exit()
    }
}

// ==============================================================================
// Main Program
// ==============================================================================

// check args
var args = process.argv.slice(1)
if (args.length < 2) {
    console.log('Usage: node HOLTheoryRun.js [script]         (for port 8080)')
    console.log('   or  node HOLTheoryRun.js [script] [port]  (for specified port)')
    return
}
// get name of script
var script = args[1]
if (debug) console.log('HOL theory script: ' + script)
// check port if present
port = args[2] || port
if (args[2]) if (debug) console.log('Listening to port: ' + port)


// Step 1: read HOL Theory script as an array of lines
// Step 2: process script data

// The script content can be obtained remotely (via HTTP) or locallty (via FileSystem)
// For the sake of browser, use HTTP to get file
Web.doGet(script, function(data) {
    if (debug) console.log('data.length: ' + data.length)
    // convert into lines
    var lines = data.join('').split(LF)
    if (debug) console.log('Number of lines: ' + lines.length)
    // put lines into ChunkReader and start
    //(new ChunkReader(lines)).more()
    ChunkReader.set(lines)
    ChunkReader.doChunk()
})

if (debug) console.log('Wait for Response.')
