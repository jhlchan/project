/**
 Windmills script

 Ideas taken from:
 * Markdeep: https://casual-effects.com/markdeep/latest/markdeep.min.js
 */

/**

 Documentation:

 Use a Panel for <windmill>:
 * a DIV with position:relative, width and height.
 * have a Title on top.
 * have a Board with canvas.
 * have canvas to plot or draw objects.

 */

/**
 Common Global Functions
*/
// convert list to an array
function toArray(list) {
    return Array.prototype.slice.call(list);
}

/**
Windmills, using the same idea.
 * set up stylesheet for Board and elements,
 * find all nodes of windmill class or tag,
 * parse the node content and generate a Board node,
 * embed the Board node inside a Panel node with title,
 * replace each <windmill> node by a Panel node.
 */

// single function application for parser
(function() {
'use strict';
/**
 A simple string parser.
 Syntax: parse(ch), where ch is a char indicator:
 * L for list of numbers :signed decimals, can be '(3,-4,1)', or '3,-4,1'
 * N for number
 * B for boolean
 * Q for quote string
 */
String.prototype.parse = function (ch) {
    switch (ch) {
    case 'L': // parse a list
        if (this.length == 0) return [];
        // extract number strings: optional sign, then digits, optional dot digits
        var nums = this.match(/[-+]?\d+(\.\d+)?/g); // group by signed decimals
        // '1,2.5,-3'.match(/[-+]?\d+(\.\d+)?/g) gives [ '1', '2.5', '-3' ]
        return nums.map(Number); //  ['1','9'].map(Number);  gives [1, 9]
    case 'N': // parse a number
        return Number(this);
    case 'B': // parse a boolean: yes or no
        return this.toLowerCase().charAt(0) == 'y';
    case 'Q': // parse a quote
        var quote = this.charAt(0);
        return this.slice(1, this.indexOf(quote, 1));
    default: return this; // no change
    }
}
// convert string to JSON object
String.prototype.toObject = function() {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // remember indices pointing to a word that begins with an alphabet
    var alpha = [];
    for (var j = 0; j < words.length; j++) {
         if (words[j].match(/^[A-Za-z]/)) alpha.push(j);
    }
    // form the JSON string
    var s = [];
    while (alpha.length > 0) {
        var j = alpha.shift(); // alpha[0] = alpha.peek()
        var k = alpha.length > 0 ? alpha[0] : words.length;
        if (k == j+1) { // two consecutive words
            s.push('"' + words[j] + '":"' + (k < words.length ? words[k] : 'null') + '"');
            alpha.shift(); // discard index k
        }
        else if (k == j+2) { // a word and a number
            s.push('"' + words[j] + '":' + words[j+1]);
        }
        else { // a word and more numbers
            s.push('"' + words[j] + '":[' + words.slice(j+1, k).join(',') + ']');
        }
    }
    s = '{ ' + s.join(', ') + ' }'; // the JSON string
    // alert('toObject: ' + this + '\n' + s);
    return JSON.parse(s); // object of JSON string
}
// Array default display
Array.prototype.toString = function() {
   return '[' + this.join(',') + ']'
}
})(); // invoke single function

/* ------------------------------------- *
 * Windmill Processing                   *
 * ------------------------------------- */

// single function application for <windmill>
(function() {
'use strict';

// Constants
var size = 300;

// load style sheet for windmill
// for .title and .info, remove: pointer-events: none;
function loadWindmillStyle() {
    var width = 660; // 660 px instead of 360px
    var s = '';
    s += '<style>'
    // s += '.frame { position:relative; width:360px; height:280px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
    s += '.frame { position:relative; width:'+(2*size+60)+'px; height:'+(size+80)+'px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
    s += '.board { position:relative; width:'+(2*size)+'px; height:'+size+'px; border:1px solid coral; display:block;}'
    s += '.piece { position:absolute; width:100%; height:100%; top:0%; left:0%; }'
    s += '.txtfont { font: 12pt arial; } '
    s += '.frame table { border-collapse:collapse; line-height:140%; page-break-inside:avoid; } '
    s += '.frame th { color:#FFF; background-color:#AAA; border:1px solid #888; padding:8px 15px 8px 15px; } '
    s += '.frame td { border:1px solid #888; padding:5px 15px 5px 15px; } '
    s += '.frame tr:nth-child(even) { background:#EEE; } '
    s += '.title { font: 12pt arial; font-weight: bold; position:absolute; top:10px; left:0px; width:'+width+'px; text-align:center;}'
    s += '.info { font: 10pt arial; font-weight: bold; color: #6600cc; position:absolute; top:31px; left:0px; width:300px; text-align:left;}'
    s += '.btn { display: inline-block; position: relative; text-align: center; margin: 2px; text-decoration: none; font: bold 14px/25px Arial, sans-serif; color: #268; border: 1px solid #88aaff; border-radius: 10px;cursor: pointer; background: linear-gradient(to top right, rgba(170,190,255,1) 0%, rgba(255,255,255,1) 100%); outline-style:none;}'
    s += '.btn:hover { background: linear-gradient(to top, rgba(255,255,0,1) 0%, rgba(255,255,255,1) 100%); }'
    s += '.noborder td {border: none}'
    s += '</style>'
    document.write(s);
}

/**
 A Panel is like the frame, having:
 * a title bar
 * a board, square piece for plotting
 */

// construct a Panel
function Panel (args, w, h) {
    w = w || size;  // default size, even 0 is size.
    h = h || size;  // default size, even 0 is size.
    // create the DOM elements for Panel
    var panel = document.createElement('div');
    panel.className = 'frame';
    var title = document.createElement('div');
    title.className = 'title';
    title.innerHTML = args.param ||'Panel Title'; // default title
    args.panel = panel;
    args.title = title;

    var board = new Board(args, w, h);

    // panel will have:
    //          title
    //          board
    // link up elements
    panel.appendChild(title);
    panel.appendChild(board);

    return panel;
}

// construct the Board
function Board (args, w, h) {

    // create the DOM elements for scene
    var board = document.createElement('div');
    board.className = 'board';
    // board.setAttribute('style', 'position:absolute; left:80px; top:60px;');
    board.setAttribute('style', 'position:absolute; left:30px; top:60px;');

    // handle arguments
    var canvas = document.createElement('canvas');
    doModes(args.mode, args.lines);
    board.appendChild(canvas);

    /* ----------------------------------------------------- *
       All handling routines.
     * ----------------------------------------------------- */

    function doModes(mode, lines) {
        // windmill based on mode
        switch(mode) {
        case 'basic' : BasicWindmill(canvas, lines); break;
        default : SampleWindmill(canvas, lines); break;
        }
    }

    // final return
    return board; // the board
}

/* ------------------------------------- *
 * Sample Windmill                       *
 * ------------------------------------- */

// Sample windmill
// * just a random sample to show gradient on canvas.
function SampleWindmill(canvas, lines) {
    // sample ignore lines

    // A sample canvas with gradient
    var ctx = canvas.getContext("2d");
    // Create gradient
    var grd = ctx.createLinearGradient(0, 0, 200, 0);
    grd.addColorStop(0, "red");
    grd.addColorStop(1, "white");
    // Fill with gradient
    ctx.fillStyle = grd;
    ctx.fillRect(10, 10, 150, 80);
} // end of SampleWindmill

/* ------------------------------------- *
 * Basic Windmill                        *
 * ------------------------------------- */

// Basic windmill
// * demo of drawing circle and polygen
function BasicWindmill(canvas, lines) {
    // should parse the following from lines
    var ratio = 0.8; // ratio for radius
    var numberOfSides = 7; // cycle period

    // set canvas dimensions
    canvas.width = size;
    canvas.height = size;
    // good for debugging
    canvas.style.border = "1px solid #88aaff";
    // get context
    var ctx = canvas.getContext("2d");

    // center and radius (size = 300)
    var center = {x: size/2, y: size/2}; // at middle
    var radius = (size/2)* ratio; // max radius is size/2

    // local abbreviations
    var pi = Math.PI, cos = Math.cos, sin = Math.sin;

    // Draw circle
    // circle = arc covering angle 2 * pi
    ctx.beginPath();
    // arc(x,y,r,startangle,endangle
    ctx.arc(center.x, center.y, radius, 0, 2 * pi);
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1;
    ctx.stroke();

    // Drawing regular polygon
    var start = -pi/2; // start vertex at top, angle is clockwise
    var increment = 2*pi/numberOfSides; // increment angle
    ctx.beginPath();
    // initial moveTo
    ctx.moveTo(center.x +  radius * cos(start),
               center.y +  radius * sin(start));
    // subsequent lineTo
    for (var j = 1; j <= numberOfSides; j++) {
        ctx.lineTo(center.x + radius * cos(start + j * increment),
                   center.y + radius * sin(start + j * increment));
    }
    ctx.strokeStyle = 'blue';
    ctx.lineWidth = 2;
    ctx.stroke();

    // add labels
    ctx.font = 'bold 15px Arial';
    ctx.fillStyle = 'green';
    ctx.fillText("Hello", 100, 100);
    ctx.fillText("Bye", 60, 50);
    drawVertex(ctx, 50, 100, 'red');
    drawLabel(ctx, 50,100, "C = 5");

    function drawVertex(ctx, x, y, color, r){
        var pointSize = r || 3; // Change according to the size of the point.
        ctx.beginPath(); //Start path
        ctx.arc(x, y, pointSize, 0, 2 * pi, true); // Draw small circle, true = counterclockwise, default false = clockwise
        ctx.save(); // save and restore later
        ctx.fillStyle = color;
        ctx.fill(); // Close the path and fill.
        ctx.restore();
    }

    function drawLabel(ctx, x, y, txt) {
       var w = ctx.measureText(txt).width;
       ctx.fillText(txt, x - w/2, y);
    }
} // end of BasicWindmill

/*

A Javascript function to generate Diophantus' identity.

function sqprod(a,b,c,d) {
  var s = []
  s.push('('+a+'² + '+b+'²)('+c+'² + '+d+'²) = '+(a*c + b*d)+'² + '+ Math.abs(a*d - b*c)+'²')
  s.push('('+a+'² + '+b+'²)('+c+'² + '+d+'²) = '+ Math.abs(a*c - b*d)+'² + '+(a*d + b*c)+'²')
  return s
}

function windmill(t) {
  return t[0] * t[0] + 4 * t[1] * t[2]
}
function flip(t) {
  return [t[0], t[2], t[1]]
}
function zagier(t) {
  var x = t[0], y = t[1], z = t[2]
  if (x < y - z) {
    return [x + 2*z, z, y - z - x]
  }
  else if (x < 2 * y) {
    return [2*y - x, y, x + z - y]
  }
  return [x - 2*y, x + z - y, y]
}
function whirl(t) { return zagier(flip(t)) }
function unwhirl(t) { return flip(zagier(t)) }

// orbit = keep whirling
function orbit(t) {
    var j = [], s = t
    j.push(t)
    while (t[1] !== t[2]) {
      t = whirl(t)
      j.push(t)
      if (t[0] == 0 || t[1] == 0 || t[2] == 0) { j.push('Square!'); break}
      if (s[0] == t[0] && s[1] == t[1] && s[2] == t[2]) { j.push('Repeat!'); break}
    }
    if (t[1] == t[2]) j.push(windmill(s) + ' = ' +t[0] + '² + ' + (t[1] + t[2]) + '²')
    else j.push(j.pop() + ' ' + windmill(s))
    return j
}
*/

/* ------------------------------------- *
 * Interactive Zagier object             *
 * ------------------------------------- */

// test if two triples are the same
function same(s, t) {
  return s[0] == t[0] && s[1] == t[1] && s[2] == t[2]
}
// test if triple is unchanged by flip
function flipfix(t) {
  return t[1] == t[2]
}
// test if triple is unchanged by zagier
function zagierfix(t) {
  return t[0] == t[1]
}
// test if triple has no arms
function noarms(t) {
  return (t[1] == 0) || (t[2] == 0)
}
// test if triple has no mind
function nomind(t) {
  return (t[0] == 0)
}
// triple form of a triple
function triple(t) {
  return '(' + t.join(',') +  ')'
}
// nice form of a triple
function nice(t) {
  return t[0] + '² + 4×' + t[1] + '×' + t[2]
}
// sum of squares of a triple
function good(t) {
  return t[0] + '² + ' + (t[1] + t[2]) + '²'
}
// sum of square of a pair
function pair(t) {
  return t[0] + '² + ' + t[1] + '²'
}
// windmill number of a triple
function windmill(t) {
  return t[0] * t[0] + 4 * t[1] * t[2]
}
// zagier of a triple
function zagier(t) {
  var x = t[0], y = t[1], z = t[2]
  if (x < y - z) return [x + 2*z, z, y - z - x]
  if (x < 2 * y) return [2*y - x, y, x + z - y]
  return [x - 2*y, x + z - y, y]
}
// flip (x,y,z) = (x,z,y)
function flip(t) {
  return [t[0], t[2], t[1]]
}
// invert (x,y,z) = (-x,z,y)
function invert(t) {
  return [-t[0], t[2], t[1]]
}
// mind of a triple
function mind(t) {
  var x = t[0], y = t[1], z = t[2]
  if (x < y - z) return x + 2 * z
  if (x < y) return 2 * y - x
  return x
}
// m-mind of a triple, taking |x|
function mmind(t) {
  var x = t[0], y = t[1], z = t[2]
  x = 0 <= x ? x : -x
  if (x < y - z) return x + 2 * z
  if (x < y) return 2 * y - x
  return x
}
// mode of a triple for zagier o flip
function mode(t) {
  var x = t[0], y = t[1], z = t[2]
  // exchange y and z in the cases for zagier
  if (x < z - y) return 'ping'
  if (x < 2 * z) return 'pong'
  return 'pung'
}
// ping (x,y,z) = (x + 2 * y,y,z - y - x)
function ping(t) {
   var x = t[0], y = t[1], z = t[2]
   return [x + 2 * y, y, z - y - x]
}
// pong (x,y,z) = (2 * z - x,z,x + y - z)
function pong(t) {
   var x = t[0], y = t[1], z = t[2]
   return [2 * z - x, z, x + y - z]
}
// pung (x,y,z) = (x - 2 * z,x + y - z,z)
function pung(t) {
   var x = t[0], y = t[1], z = t[2]
   return [x - 2 * z, x + y - z, z]
}
// check if a triple is a ping
function is_ping(t) {
   return mode(t) == 'ping'
}
// check if a triple is a ppng
function is_ping(t) {
   return mode(t) == 'pong'
}
// check if a triple is a pung
function is_ping(t) {
   return mode(t) == 'pung'
}
// skip pings from a triple
function skip_ping(t) {
   var j = 0
   while (is_ping(t)) {
     t = ping(t)
     j++
     if (j > 100) return "skip_ping error!"
   }
   return t
}
// skip pungs from a triple
function skip_pung(t) {
   var j = 0
   while (is_pung(t)) {
     t = pung(t)
     j++
     if (j > 100) return "skip_pung error!"
   }
   return t
}
// give triple a whirl = zagier p flip
function whirl(t) {
  return zagier(flip(t))
}
// integer square root of a number
function sqrt(n) {
  return Math.sqrt(n) | 0 // javascript round down
}
// integer division of a quotient x/y
function div(x,y) {
  return (x/y) | 0 // JavaScript round down
}
// list of numbers up to max m
function up_to(m) {
  var v = []
  for (var k = 0; k <= m; k++) {
    v.push(k)
  }
  return v
}
// the odd numbers up to max m
function odds_to(m) {
   var n, v = []
   for (var k = 0; (n = 2 * k + 1) <= m; k++) {
     v.push(n)
   }
   return v
}
// the even numbers up to max m
function evens_to(m) {
   var n, v = []
   for (var k = 0; (n = 2 * k) <= m; k++) {
     v.push(n)
   }
   return v
}
// divisors of a number m
function divisors(m) {
   var v = []
   for (var k = 1; k <= m; k++) {
      if (m % k == 0) v.push(k)
   }
   return v
}
// windmill triples by value x
function mill_triples(n, x) {
   var v = [], k = (n - x * x) / 4
   for (var y = 1; y <= k; y++) {
      if (k % y == 0) v.push([x, y, (k/y)|0])
   }
   return v
}
// mills of a number
function mills(n) {
  var m = n % 4, k = sqrt(n), list, v = []
  if (m == 0) list = evens_to(k) // a zig
  if (m == 1) list = odds_to(k) // a tik
  if (list) { // no foreach for nodejs
     m = list.length
     for (var j = 0; j < m; j++) {
       v = v.concat(mill_triples(n, list[j]))
     }
  }
  return v
}
// windmill iterate
function iterate(t) {
   const n = windmill(t) // number from triple
   var j = 0, s = t, m = 'start' // remember the starting triple
   while (true) {
      j++
      log(j + ': ' + t + ', by ' + m + ', mind = ' + mind(t))
      m = mode(t)
      if (j > n) { log('Cannot detect loop?'); break } // CARD (mills n) <= n
      if (nomind(s)) { s = t; log('Update start') } // x is zero
      if (noarms(t)) { log('Square!'); break } // y or z is zero
      if (flipfix(t)) { log('Sum of squares!'); break} // sum of squares
      t = zagier(flip(t)) // t = (zagier o flip) t
      if (same(s,t)) { log('Repeat!'); break} // repeat
   }
   if (flipfix(t)) log(n + ' = ' + good(t))
   if (noarms(t)) log(n + ' = ' + sqrt(n) + '²')
   log('End of iterate.')
}
// windmill inverse iterate
function inverse(t) {
   const n = windmill(t) // number from triple
   var j = 0, s = t, m = 'start' // remember the starting triple
   while (true) {
      j++
      log(j + ': ' + t + ', by ' + m + ', mind = ' + mind(t))
      m = kind(t)
      if (j > n) { log('Cannot detect loop?'); break } // CARD (mills n) <= n
      if (nomind(s)) { s = t; log('Update start') } // x is zero
      if (noarms(t)) { log('Square!'); break } // y or z is zero
      if (flipfix(t)) { log('Sum of squares!'); break} // sum of squares
      t = flip(zagier(t)) // t = (flip o zagier) t
      if (same(s,t)) { log('Repeat!'); break} // repeat
   }
   if (flipfix(t)) log(n + ' = ' + good(t))
   if (noarms(t)) log(n + ' = ' + sqrt(n) + '²')
   log('End of inverse.')
}
// windmill orbit
function orbit(t) {
   const n = windmill(t) // number from triple
   var v = [], j = 0, s = t // remember the starting triple
   while (true) {
      v.push(t)
      j++
      if (j > n) break // CARD (mills n) <= n
      if (nomind(s)) s = t // orbit only returns to a triple with mind
      if (noarms(t)) break // a square!
      // if (flipfix(t)) break // sum of squares
      t = zagier(flip(t)) // t = (zagier o flip) t
      if (same(s,t)) { v.push(s); break } // repeat
   }
   return v
}
// windmill path (only to flip fix)
function path(t) {
   const n = windmill(t) // number from triple
   var v = [], j = 0, s = t // remember the starting triple
   while (true) {
      v.push(t)
      j++
      if (j > n) break // CARD (mills n) <= n
      if (nomind(s)) s = t // orbit only returns to a triple with mind
      if (noarms(t)) break // a square!
      if (flipfix(t)) break // sum of squares
      t = zagier(flip(t)) // t = (zagier o flip) t
      if (same(s,t)) { v.push(s); break } // repeat
   }
   return v
}
// step k (x,y,z) = (x + k) DIV (2 * z)
function step(k, t) {
   var x = t[0], y = t[1], z = t[2]
   return (x + k) / (2 * z) |0  // integer division DIV in javascript
}
// hop function
function hop(m, t) {
   var x = t[0], y = t[1], z = t[2]
   if (x < 2 * m * z) return [2 * m * z - x, z, y + m * x - m * m * z] // J A A A ...
   return [x - 2 * m * z, y + m * x - m * m * z, z] // J A A A ... J
}
// one_hop (assume k is defined)
function one_hop(t) {
  return hop(step(k,t), t)
}
// fast hopping
function fast_hopping(n) {
   var t = [1,div(n,4),1], s = sqrt(n), m = 0, u = 0
   log('Start with: ' + t)
   // show the associated block (u,v,w)
   function get_block(t) {
      var uu = u, v = u + step(0,t), w = u + step(s,t)
      u = w // update u by w, uu is the old u
      return ' block: ' + [uu,v,w]
   }
   var j = 0, b = ''
   while (true) {
      if (flipfix(t)) { log('Sum of squares!'); break} // sum of squares
      b = get_block(t)
      m = step(s,t) // update m
      t = hop(m, t) // t = hopping s t
      j++
      log(j + ': ' + b + ', hop ' + m + ' to ' + t)
      if (j > n) { log('Hop Oops!'); break}
   }
   // raw hopping
   t = [1,div(n,4),1]
   log('Raw Hopping: ' + t)
   var k = 0
   j = 0
   var mh = []
   mh.push(t)
   while (true) {
     if (flipfix(t)) { log('A fip fix!'); break} // sum of squares
     j++
     if (j > n) { log('Oops'); break }
     m = step(s,t)
     mh.push(m)
     k = k + m
     var ls = []
     ls.push('hop ' + m + ' ' + t + ': ')
     t = invert(t); ls.push(t)
     Array.from(Array(m), () => {
       t = ping(t)
       ls.push(t)
     })
     mh.push(t)
     log(ls.join(' '))
   }
   // hopping summary
   log('Hopping Summary:')
   log(mh.join(' ') + ', hops: ' + j + ', total: ' + k)
}
// check if a number is a square
function is_square(n) {
   var m = sqrt(n)
   return (n == m * m)
}
// search for sum of squares from list
function square_sum(n, list) {
   var v = [], y
   list.forEach((x, i) => {
      y = n - x * x
      if (is_square(y)) v.push([x, sqrt(y)])
   });
   return v
}
// check if a number is a prime (naive method)
function prime(n) {
   // check even n
   if (n%2 == 0) return (n == 2)
   // check odd n
   if (n == 1) return false
   var m = sqrt(n)
   // for odd n
   for (var j = 3; j <= m; j += 2) {
      if (n%j == 0) return false
   }
   return true
}
// conjugate of a quadruple
function conjugate(q) {
   var a = q[0], x = q[1], b = q[2], y = q[3]
   return [y, b - a, y + x, a]
}
// transpose of a quadruple
function transpose(q) {
   var a = q[0], x = q[1], b = q[2], y = q[3]
   if (x < y) return [x, a, y, b]
   if (y < x) return [y, b, x, a]
   return [a, x, b, y]
}
// generate the transpose equal fixes for prime p
function eq_fixes(p) {
   // such fixes for a prime has the form: (a,1,b,1) where n = a + b, and a < b.
   var v = [], h = (p-1)/2 // (p-1) is even, take half of this.
   for (var j = 1; j <= h; j++) {
      v.push([j,1,p-j,1])
   }
   return v
}
/*
> eq_fixes(17)
[ [ 1, 1, 16, 1 ],
  [ 2, 1, 15, 1 ],
  [ 3, 1, 14, 1 ],
  [ 4, 1, 13, 1 ],
  [ 5, 1, 12, 1 ],
  [ 6, 1, 11, 1 ],
  [ 7, 1, 10, 1 ],
  [ 8, 1, 9, 1 ] ]
*/
// corner value of a quadruple
function corner(q) {
   var a = q[0], x = q[1], b = q[2], y = q[3]
   return a * x + b * y
}
// check if corner is a less fix
function less_fix(q) {
   var a = q[0], x = q[1], b = q[2], y = q[3]
   // less fix has the form (a,a,b,b)
   return (a == x) && (b == y)
}
// check if corner is a more fix
function more_fix(q) {
   var a = q[0], x = q[1], b = q[2], y = q[3]
   // more fix for transpose has the form (a,b,b,a), none for prime
   // but more has conjugate fix (1,h,h+1,1) for prime p, half h = (p-1)/2
   return (x == (corner(q) - 1)/2) && (b == x + 1)
}
// check if corner is an equal fix
function equal_fix(q) {
   var a = q[0], x = q[1], b = q[2], y = q[3]
   // equal fix has the form (a,c,b,c), or (a,1,b,1) for prime p = a + b
   return (x == y)
}
// corner orbit for quadruple
function corner_orbit(q) {
   const n = corner(q) // number from quadruple
   var v = [], j = 0
   v.push(q)
   while (true) {
      j++
      if (j > 100) break // emergency exit
      q = transpose(conjugate(q))
      v.push(q)
      if (less_fix(q)) v.push("less_fix")
      if (more_fix(q)) v.push("more_fix")
      if (equal_fix(q)) v.push("equal_fix")

      if (less_fix(q)) break // sum of squares! by transpose
      if (more_fix(q)) break // symmetric! by conjugate
      if (equal_fix(q)) break // almost repeats! in transpose
   }
   return v
}
/*
> corner_orbit([1,1,16,1])
[ [ 1, 1, 16, 1 ],
  [ 1, 2, 15, 1 ],
  [ 1, 3, 14, 1 ],
  [ 1, 4, 13, 1 ],
  [ 1, 5, 12, 1 ],
  [ 1, 6, 11, 1 ],
  [ 1, 7, 10, 1 ],
  [ 1, 8, 9, 1 ],
  'more_fix' ]
> corner_orbit([2,1,15,1])
[ [ 2, 1, 15, 1 ],
  [ 2, 2, 13, 1 ],
  [ 2, 3, 11, 1 ],
  [ 2, 4, 9, 1 ],
  [ 2, 5, 7, 1 ],
  [ 2, 6, 5, 1 ],
  [ 2, 7, 3, 1 ],
  [ 1, 1, 2, 8 ],
  [ 8, 1, 9, 1 ],
  'equal_fix' ]
> corner_orbit([3,1,14,1])
[ [ 3, 1, 14, 1 ],
  [ 3, 2, 11, 1 ],
  [ 3, 3, 8, 1 ],
  [ 3, 4, 5, 1 ],
  [ 2, 1, 3, 5 ],
  [ 1, 5, 2, 6 ],
  [ 6, 1, 11, 1 ],
  'equal_fix' ]
> corner_orbit([4,1,13,1])
[ [ 4, 1, 13, 1 ],
  [ 4, 2, 9, 1 ],
  [ 4, 3, 5, 1 ],
  [ 1, 1, 4, 4 ],
  'less_fix' ]
*/
// brief corner orbit for quadruple
function brief_corner_orbit(q) {
   const n = corner(q) // number from quadruple
   var v = [], j = 0
   v.push(q)
   v.push('...')
   while (true) {
      j++
      if (j > 100) break // emergency exit
      q = transpose(conjugate(q))
      if (less_fix(q)) { v.push(q); v.push("sum of squares!"); break } // sum of squares!
      if (more_fix(q)) { v.push(q); v.push("conjugate fix"); break } // symmetric!
      if (equal_fix(q)) { v.push(q); v.push("eq"); break } // almost repeats!
   }
   return v
}
// Zagier fixes in mills
function zagier_fixes(mills) {
   var v = []
   mills.forEach((item, i) => {
     if (zagierfix(item)) v.push(item)
   });
   return v
}

// Zagier object
function Zagier(num) {
  // internal n is input number, make it a constant.
  const n = num;
  // parity of num, a constant
  const parity = (n % 2 == 0) ? 'even' : 'odd';
  // quarity of num, a constant
  const quarity = (n % 4 == 0) ? 'zig' :
                  (n % 4 == 1) ? 'tik' :
                  (n % 4 == 3) ? 'tok' : 'zag';

  // string representation
  this.toString = function() {
    return n + ' = ' + nice(quarity == 'zig' ? [0,1,(n/4)|0] : [1,1,(n/4)|0])
  }
  // main action
  this.run = function() {
    log('Input number ' + n + ' is ' + parity + ', a ' + quarity + '.')
    if (quarity == 'tok') {
      log('Number ' + n + ' has the form 4k + 3.')
      log('A tok is never a sum of two squares!')
      log('A tok is never itself a square!')
    }
    if (quarity == 'zag') {
      log('Number ' + n + ' has the form 4k + 2.')
      log('A zag has no windmils, hence no orbit.')
      log('However, it can be a sum of two odd squares.')
      var k = sqrt(n)
      log('Square root of ' + n + ' = ' + k)
      log('Search for sum of squares:')
      var sums = square_sum(n, up_to(k))
      sums.forEach((p, i) => {
        log(n + ' = ' + pair(p))
      });
      log('Done.')
    }
    if (quarity == 'zig') {
      log('Number ' + n + ' has the form 4k.')
      log('A zig has windmils, may be a sum of two even squares.')
      var k = sqrt(n)
      log('Square root of ' + n + ' = ' + k)
      var evens = evens_to(k)
      log('evens up to ' + k + ': ' + evens)
      var mills = []
      evens.forEach((x, i) => {
         mills = mills.concat(mill_triples(n,x))
      });
      log('mills ' + n + ': ' + mills)
      log('number of windmills = ' + mills.length)
      log('The idea of an orbit is shaky.')
      log('Zagier:</br>' + this)
      // form a triple t = [x,y,z]
      var t = [0, 1, (n/4)|0]; // javascript quotient
      log('Starting from triple = ' + t)
      iterate(t)
      var orb = orbit(t)
      log('Orbit: ' + orb)
      log('Orbit length = ' + orb.length)
      log('Search for sum of squares:')
      var sums = square_sum(n, up_to(k))
      sums.forEach((p, i) => {
        log(n + ' = ' + pair(p))
      });
      log('Done.')
    }
    if (quarity == 'tik') {
      log('Number ' + n + ' has the form 4k+1.')
      log('A tik has windmils, may be a sum of two squares, one odd and one even.')
      var k = sqrt(n)
      log('Square root of ' + n + ' = ' + k)
      var odds = odds_to(k)
      log('odds up to ' + k + ': ' + odds)
      var mills = []
      odds.forEach((x, i) => {
         mills = mills.concat(mill_triples(n,x))
      });
      log('mills ' + n + ': ' + mills)
      log('number of windmills = ' + mills.length)
      log('The idea of an orbit is good.')
      log('Zagier:</br>' + this)
      // form a triple t = [x,y,z]
      var u = [1, 1, div(n,4)];
      log('Starting from triple = ' + u)
      iterate(u)
      // show the orbits
      log('Orbit(s) from Zagier fixes:')
      var fixes = zagier_fixes(mills)
      fixes.forEach((t, i) => {
        var orb = orbit(t)
        log('Orbit: ' + orb)
        orb.pop() // remove start from orbit
        log('Orbit length = ' + orb.length)
        // check triple t at middle
        var m = div(orb.length, 2) // integer half of length
        var p = orb[m]
        log('triple at middle ' + m + ': ' + p)
        if (flipfix(p)) {
          log('Sum of squares: ' + n + ' = ' + good(p))
        }
        else {
          log('Either orbit repeats, or orbit goes astray!')
        }
      });
      // show hopping path for tik prime
      if (prime(n)) {
        log(n + ' is a prime.')
        // fast hopping
        log('Fast Hopping:')
        fast_hopping(n)
      }
      else {
        log(n + ' is a not prime.')
      }
      log('Search for sum of squares:')
      var sums = square_sum(n, up_to(k))
      sums.forEach((p, i) => {
        log(n + ' = ' + pair(p))
      });
      // ignore these two parts
      if (false) {
        // inverse iterate
        log('Inverse iteration:')
        inverse(t)
        // corners
        var fixes = eq_fixes(n)
        log('The equal fixed points are: ' + fixes)
        log('Corner orbits for n = ' + n + ':')
        fixes.forEach((q, j) => {
           log('' + brief_corner_orbit(q))
        });
      }
      log('Done.')
    }
  }
}

/* An example:

Zagier:
37 = 1² + 4×1×9
(1,1,9) <- start
Step: 1
(1,9,1) <- flip
(3,1,7) <- zagier
Step: 2
(3,7,1) <- flip
(5,1,3) <- zagier
Step: 3
(5,3,1) <- flip
(1,3,3) <- zagier
Squares!
Done.
37 = 1² + 6²

*/

// Zagier initialization
function process(num) {
  clearLog()
  new Zagier(num).run()
}

/*  */
/* ------------- *
 * Next analysis *
 * ------------- */

// get nearest tik prime from m
function tikPrimeFrom(m) {
  var n = 1 + 4 * (m/4|0)
  // log('JC: m = ' + m + ', n = ' + n)
  if (n < 5) n = 5
  while (! prime(n)) n -= 4 // must exit by descent
  return n
}

// get the next tik prime m
function tikPrimeUp(m) {
  var n = tikPrimeFrom(m) // ensure m is a tik prime
  if (n == m) n += 4 // next multiple
  while (! prime(n)) n += 4 // must exit by arithmetic progression
  return n
}

// sum of an array
function sum(list) {
  return list.reduce((accumulator, currentValue) => accumulator + currentValue, 0)
}

const symbol = [' ▲ ', ' ⬥ ', ' ▼ ', ' ❚ ']

function type(t) {
    return t[0] < t[2] - t[1] ? 0 : t[0] < 2 * t[2] ? 1 : 2
}

// analysis for a tik prime n
function analyse(n) {
  const c = sqrt(n)
  var t = [1, (n/4)|0, 1] // flip of zagier fix
  var s = [], h = [], m = step(c, t)
  h.push(m)
  // log('JC: hop step: m = ' + m + ' initial')
  path(t).forEach(function(item, j) {
    s.push(item)
    // log('JC: now j = ' + j + ', m = ' + m)
    if (j == m) {
      s.push(' ❚ ')
      m = step(c, item)
      h.push(m)
      // log('JC: hop step: m = ' + m + ' at j = ' + j)
      m += j
      // log('JC: hop step: update m = ' + m)
    }
    s.push(symbol[type(item)])
  })
  s = s.slice(0,-1) // remove last type symbol
  h = h.slice(0,-1) // remove hop for last one
  log(n + ': ' + s.join('') + ', hop: ' + h.join(',') + ', total steps: ' + sum(h))
}

// next tik primes analysis from m for a range
function nextFrom(m, range) {
  range = range || 20
  clearLog()
  var n = tikPrimeFrom(m), m, k
  log('Symbols: ▲ ping, ⬥ pong, ▼ pung, ❚ hop')
  log('Analyis for next '+ range + ' tik primes from: ' + n)
  Array(range).fill().map(() => { // javascript repeat m times
      analyse(n)
      m = mills(n).length
      k = orbit([1,1,(n - 1)/4|0]).length - 1
      log('CARD (mills ' + n + ') = ' + m + ', LENGTH (orbit) = ' + k + (m == k ? '' : ' !!!'))
      // log('| ' + n + '| ' + m + '| ' + k + '| ' + (m == k ? ' ' : ' !!!') + '|')
      n = tikPrimeUp(n)
  })
}

/*
console.log('JC: mills 97', mills(97))                  gives Array(27)
console.log('JC: orbit 97', orbit([1,1,(97 - 1)/4|0]))  gives Array(28), back to Zagier fix.
*/

/*
Symbols: ▲ ping, ⬥ pong, ▼ pung, ❚ hop
Analyis for next 20 tik primes from: 5
5: [1,1,1], hop: , total steps: 0
13: [1,3,1] ⬥ [1,1,3] ▲ [3,1,1] ❚ , hop: 2, total steps: 2
17: [1,4,1] ⬥ [1,1,4] ▲ [3,1,2] ❚ ⬥ [1,2,2] ❚ , hop: 2,1, total steps: 3
29: [1,7,1] ⬥ [1,1,7] ▲ [3,1,5] ▲ [5,1,1] ❚ , hop: 3, total steps: 3
37: [1,9,1] ⬥ [1,1,9] ▲ [3,1,7] ▲ [5,1,3] ❚ ⬥ [1,3,3] ❚ , hop: 3,1, total steps: 4
41: [1,10,1] ⬥ [1,1,10] ▲ [3,1,8] ▲ [5,1,4] ❚ ⬥ [3,4,2] ❚ ⬥ [1,2,5] ▲ [5,2,2] ❚ , hop: 3,1,2, total steps: 6
53: [1,13,1] ⬥ [1,1,13] ▲ [3,1,11] ▲ [5,1,7] ▲ [7,1,1] ❚ , hop: 4, total steps: 4
61: [1,15,1] ⬥ [1,1,15] ▲ [3,1,13] ▲ [5,1,9] ▲ [7,1,3] ❚ ▼ [1,5,3] ⬥ [5,3,3] ❚ , hop: 4,2, total steps: 6
73: [1,18,1] ⬥ [1,1,18] ▲ [3,1,16] ▲ [5,1,12] ▲ [7,1,6] ❚ ⬥ [5,6,2] ❚ ▼ [1,9,2] ⬥ [3,2,8] ▲ [7,2,3] ❚ ▼ [1,6,3] ⬥ [5,3,4] ❚ ⬥ [3,4,4] ❚ , hop: 4,1,3,2,1, total steps: 11
89: [1,22,1] ⬥ [1,1,22] ▲ [3,1,20] ▲ [5,1,16] ▲ [7,1,10] ▲ [9,1,2] ❚ ▼ [5,8,2] ▼ [1,11,2] ⬥ [3,2,10] ▲ [7,2,5] ❚ ⬥ [3,5,4] ❚ ⬥ [5,4,4] ❚ , hop: 5,4,1,1, total steps: 11
97: [1,24,1] ⬥ [1,1,24] ▲ [3,1,22] ▲ [5,1,18] ▲ [7,1,12] ▲ [9,1,4] ❚ ▼ [1,6,4] ⬥ [7,4,3] ❚ ▼ [1,8,3] ⬥ [5,3,6] ❚ ⬥ [7,6,2] ❚ ▼ [3,11,2] ⬥ [1,2,12] ▲ [5,2,9] ▲ [9,2,2] ❚ , hop: 5,2,2,1,4, total steps: 14
101: [1,25,1] ⬥ [1,1,25] ▲ [3,1,23] ▲ [5,1,19] ▲ [7,1,13] ▲ [9,1,5] ❚ ⬥ [1,5,5] ❚ , hop: 5,1, total steps: 6
109: [1,27,1] ⬥ [1,1,27] ▲ [3,1,25] ▲ [5,1,21] ▲ [7,1,15] ▲ [9,1,7] ❚ ⬥ [5,7,3] ❚ ⬥ [1,3,9] ▲ [7,3,5] ❚ ⬥ [3,5,5] ❚ , hop: 5,1,2,1, total steps: 9
113: [1,28,1] ⬥ [1,1,28] ▲ [3,1,26] ▲ [5,1,22] ▲ [7,1,16] ▲ [9,1,8] ❚ ⬥ [7,8,2] ❚ ▼ [3,13,2] ⬥ [1,2,14] ▲ [5,2,11] ▲ [9,2,4] ❚ ▼ [1,7,4] ⬥ [7,4,4] ❚ , hop: 5,1,4,2, total steps: 12
137: [1,34,1] ⬥ [1,1,34] ▲ [3,1,32] ▲ [5,1,28] ▲ [7,1,22] ▲ [9,1,14] ▲ [11,1,4] ❚ ▼ [3,8,4] ⬥ [5,4,7] ❚ ⬥ [9,7,2] ❚ ▼ [5,14,2] ▼ [1,17,2] ⬥ [3,2,16] ▲ [7,2,11] ▲ [11,2,2] ❚ , hop: 6,2,1,5, total steps: 14
149: [1,37,1] ⬥ [1,1,37] ▲ [3,1,35] ▲ [5,1,31] ▲ [7,1,25] ▲ [9,1,17] ▲ [11,1,7] ❚ ⬥ [3,7,5] ❚ ⬥ [7,5,5] ❚ , hop: 6,1,1, total steps: 8
157: [1,39,1] ⬥ [1,1,39] ▲ [3,1,37] ▲ [5,1,33] ▲ [7,1,27] ▲ [9,1,19] ▲ [11,1,9] ❚ ⬥ [7,9,3] ❚ ▼ [1,13,3] ⬥ [5,3,11] ▲ [11,3,3] ❚ , hop: 6,1,3, total steps: 10
173: [1,43,1] ⬥ [1,1,43] ▲ [3,1,41] ▲ [5,1,37] ▲ [7,1,31] ▲ [9,1,23] ▲ [11,1,13] ▲ [13,1,1] ❚ , hop: 7, total steps: 7
181: [1,45,1] ⬥ [1,1,45] ▲ [3,1,43] ▲ [5,1,39] ▲ [7,1,33] ▲ [9,1,25] ▲ [11,1,15] ▲ [13,1,3] ❚ ▼ [7,11,3] ▼ [1,15,3] ⬥ [5,3,13] ▲ [11,3,5] ❚ ▼ [1,9,5] ⬥ [9,5,5] ❚ , hop: 7,4,2, total steps: 13
193: [1,48,1] ⬥ [1,1,48] ▲ [3,1,46] ▲ [5,1,42] ▲ [7,1,36] ▲ [9,1,28] ▲ [11,1,18] ▲ [13,1,6] ❚ ▼ [1,8,6] ⬥ [11,6,3] ❚ ▼ [5,14,3] ⬥ [1,3,16] ▲ [7,3,12] ▲ [13,3,2] ❚ ▼ [9,14,2] ▼ [5,21,2] ▼ [1,24,2] ⬥ [3,2,23] ▲ [7,2,18] ▲ [11,2,9] ❚ ⬥ [7,9,4] ❚ ⬥ [1,4,12] ▲ [9,4,7] ❚ ⬥ [5,7,6] ❚ ⬥ [7,6,6] ❚ , hop: 7,2,4,6,1,2,1,1, total steps: 24
*/

// Interactive part.
function processScreen(line) {
   var words = line.split(' ');
   // alert('There are: ' + words.length);
   var num = document.getElementById(words[0]) // first word: inNum
   var btn = document.getElementById(words[1]) // second word: inBtn
   var next = document.getElementById(words[2]) // for third word: next
   // assume external routines clearLog() and log(s)
   btn.onclick = function() {
     process(num.value);
   }
   // detect ENTER and simulate button click
   num.addEventListener("keyup", function(event) {
      event.preventDefault()
      // auto-click "enter" upon ENTER key
      if (event.key === "Enter") btn.click();
   })
   next.onclick = function() {
     nextFrom(num.value)
   }
   // return a dummy span element
   return document.createElement('span');
}


/* ------------------------------------- *
 * Node transformation and processing    *
 * ------------------------------------- */

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // alert('source: ' + source + '!');
    // minimal processing
    var lines = [];
    source.split('\n').forEach(function(line) {
        // remove comments after ;
        if (line && line.indexOf(';') !== -1) line = line.slice(0,line.indexOf(';'));
        // remove blank lines
        if (line && line.length !== 0) lines.push(line);
    });
    // alert('filtered lines: ' + lines + ', count: ' + lines.length);
    // first line is mode, with optional param separate by one space
    var first = lines.shift().match(/\w+|\s.*/g);
    // match either pattern: \w+ a word, \s.* blank then anything
    var args =
       {mode: first.length > 0 ? first[0] : '',           // start word
        param: first.length > 1 ? first[1].slice(1) : '', // no leading blank
        lines: lines }
    // special treatment for webpage
    if (args.mode == "webpage") return processScreen(lines[0]);
    return new Panel(args);
}

// process all <windmill> nodes
function processWindmill() {
   // Collect all nodes that will receive <windmill> processing
   // Process all nodes, replacing them as we progress
   toArray(document.getElementsByClassName('windmill'))
       .concat(toArray(document.getElementsByTagName('windmill')))
       .forEach(function (node) {
           node.parentNode.replaceChild(transform(node), node);
       });
}

// Start from here.
loadWindmillStyle(); // load the style CSS for panel, board, title, info, button
processWindmill();   // find and process all <windmill> nodes

})(); // invoke single function

/** End of Windmills Script processing.
 ** Author: Joseph Chan
 ** Date: 28 February 2023.
 */
