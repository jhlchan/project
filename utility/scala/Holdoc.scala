//
// A simple documentation tool for HOL
//
// How to run:
// (1) $ scala Holdoc.scala fermat little
// This will parse the theory scripts of "fermat" and "little",
// showing the dependencies of theorems and the orphans.
//
// (2) $ scala Holdoc.scala fermat little thm:FERMAT_LITTLE
// This will parse the theory scripts of "fermat" and "little",
// but skip the dependences. Instead it follows all theorems with name starting with
// FERMAT_LITTLE, and shows the orphans by these theorems, as well as other orphans.
//
import scala.io.Source

// debug flag
val debug = false;

// Tokenizer class
class Tokenizer(lines: Iterator[String]) {
  val LF = "_LF_"  // line separator
  var tokens = lines.mkString(" "+LF+" ").split(" ").toList // keep line separator
  var top = ""
  // split top when it contains a specific char ch
  private def splitTop(ch: Char) {
    val k = top.indexOf(ch)
    var tok = top.slice(k+1,top.length)
    if (tok != "") tokens = tok::tokens
    if (k > 0) {
        tokens = ch.toString::tokens
        top = top.slice(0,k)
    }
    else top = ch.toString
  }
  // return the next token
  def next(): String = {
    // peek the top to decide action
    while (!(tokens isEmpty) && tokens.head == "") tokens = tokens.tail
    if (tokens isEmpty) return ""
    top = tokens.head; tokens = tokens.tail
    if (top == "(" || top == ")" ||
        top == "[" || top == "]" ||
        top == "," || top == ";") {
      // keep this
    }
    else if (top == "()") {
      tokens = ")"::tokens
      top = "("
    }
    else if (top == "[]") {
      tokens = "]"::tokens
      top = "["
    }
    else if (top == "(*") {
      var tok = top
      while (!(tokens isEmpty) && top != "*)") {
        top = tokens.head; tokens = tokens.tail
        tok += " " + top;
      }
      top = tok
    }
    else if ((top.indexOf("(*") == 0) &&
             (top.indexOf("*)") == top.length -2)) {
      // keep this
    }
    // check ` ... ` here
    else if (top == "`" ||
            (top.length > 1 && top.charAt(0) == '`' &&
             top.charAt(top.length-1) != '`')) { // i.e. not `xxx` in one token
      var tok = top
      if (top.charAt(top.length-1) != ';') {
        var found = false;
        while (!found && !(tokens isEmpty)) {
          top = tokens.head; tokens = tokens.tail
          tok += " " + top
          found = top.indexOf('`') != -1
        }
      }
      val k = tok.lastIndexOf('`');
      top = tok.slice(0,k+1);
      tok = tok.slice(k+1,tok.length);
      if (tok != "") tokens = tok::tokens
    }
    // check [ ... ] first
    else if (top.charAt(0) == '[') {
      var tok = top.slice(1,top.length);
      val k = tok.indexOf(']');
      tokens = if (k != -1) tok.slice(0,k)::"]"::tok.slice(k+1,tok.length)::tokens
                       else tok::tokens
      top = "["
    }
    else if (top.indexOf(']') != -1) {
      splitTop(']')
    }
    // then check ( ... )
    else if (top.indexOf("()") != -1) {
      // keep ...()
    }
    else if (top.indexOf('(') != -1) {
      splitTop('(')
    }
    else if (top.indexOf(')') != -1) {
      splitTop(')')
    }
    // this ending ` check must be after single ` check above
    else if (top.charAt(0) != '`' && top.charAt(top.length - 1) == '`') {
      tokens = "`"::tokens
      top = top.slice(0, top.length - 1)
    }
    // this ending , check must be near last
    else if (top.charAt(top.length - 1) == ',') {
      tokens = ","::tokens
      top = top.slice(0, top.length - 1)
    }
    // this ending ; check must be last
    else if (top.charAt(top.length - 1) == ';') {
      tokens = ";"::tokens
      top = top.slice(0, top.length - 1)
    }
    return top
  }
}

// entry record
class Entry(k: String, d: String, n: Int) {
  var kind = k
  var defn = d
  var lines = n
  var link = List[String]()
  var tick = 0

  // reset an entry
  def reset(k: String, d: String, n: Int) {
    kind = k
    defn = d
    lines = n
  }
  // link another
  def linkTo(other: String) {
    link = other::link
  }
  // add tick
  def addTick = {
    tick += 1
  }
  // representation
  override def toString(): String = {
    if (kind == "defb") return "="+defn
    else if (kind == "thm") return link.toString
    return "Entry not implemented: "+kind
  }
}

// Analyzer object
object Analyzer {
  // the map database
  private var map = Map[String, Entry]()

  // the ignore database
  // No need to specify all to be ignored
  // As long as these are not names of entries,
  // they are ignored by marking of names.
  private var ignore = Set[String]()
  ignore += "EQ_IMP_THM"
  ignore += "ALL_TAC"
  ignore += "RES_FORALL_THM"

  private def entry(name: String, kind: String, defn: String, lines: Int) {
    // println("map <- "+kind+":"+name+"="+defn)
    if (map.contains(name)) {
      println("Reset: "+name)
      map(name).reset(kind, defn, lines)
    }
    else {
      map += name -> new Entry(kind, defn, lines)
    }
  }

  // associate a name with another
  private def associate(name: String, other: String) {
    if (!ignore.contains(other)) {
      if (map.contains(name)) {
        if (!map(name).link.contains(other)) map(name).linkTo(other)
        if (map.contains(other)) map(other).addTick
      }
      else println("! map("+name+") no entry found");
    }
  }

  // repesentation of an entry in map
  private def show(name: String) = {
    var s = name; // a separator
    val kind = map(name).kind
    if (kind == "def") s += map(name)
    else if (kind == "thm") s += " <- " + map(name)
    else s += " Show not implemented: kind="+kind
    s // return
  }

  // show the map
  private def showMap = {
    println("Map:")
    for (name <- map.keys.toList.sorted if (map(name).kind == "Define")) println(show(name))
    for (name <- map.keys.toList.sorted if (map(name).kind != "Define")) println(show(name))
    println("End of Map.")
  }

  // show the orphans
  private def showOrphans = {
    println("Orphans:")
    for (name <- map.keys.toList.sorted if (map(name).tick == 0)) println(name)
    println("End of Orphans.")
  }

  // show information
  def showInfo = {
    showMap
    println("=================")
    showOrphans
  }

  // analyze tokens, with lines counted
  def analyze(tokens: List[String], lines: Int) {
    // normalize a name
    def normalize(name: String) = {
      // This version of analyzer will ignore theory prefix,
      // so a name like arithmeticTheory.MOD_LESS will mean only MOD_LESS
      // To handle theory prefix properly may require some re-design of analyzer
      val k = name.indexOf('.')
      if (k == -1) name else name.slice(k+1,name.length)
    }
    // scan associates of name from jth token
    def scanForAssociates(name: String, k: Int) {
      var j = k // scan from kth token
      var ch = ' '
      while (j < tokens.length) {
        ch = tokens(j).charAt(0); j += 1
        if (ch == '[' || ch == ',') {
          ch = tokens(j).charAt(0)
          if (ch != ']' && ch != '`') associate(name, normalize(tokens(j)))
        }
      }
    }
    if (debug) println("val "+tokens.mkString(" ")+";")
    val kind = tokens(2)
    if (kind == "Define") {
      entry(tokens(0),"def",tokens(3), 0) // defn at 3rd token, 0 lines
    }
    else if (kind == "store_thm") {
      entry(tokens(0),"thm",tokens(6), lines) // defn at 6th token
      scanForAssociates(tokens(0),8) // scan from 8th token for associates
    }
    else if (kind == "prove") {
      entry(tokens(0),"thm",tokens(5), lines) // defn at 5th token
      scanForAssociates(tokens(0),7) // scan from 7th token for associates
    }
    else if (kind == "save_thm") {
      entry(tokens(0),"thm", tokens(6)+" "+tokens(7), 0) // defn at 6+7th tokens, 0 lines
      associate(tokens(0), normalize(tokens(7))) // name is associated with 7th token
    }
    else if (kind == "new_specification") { // def or thm?
      entry(tokens(0),"thm", tokens(7), 1) // defn at 7th token "...", taken as one line
      // hacking here ...
      if (tokens(10) == "SIMP_RULE") {
        if (debug) println("SIMP_RULE: associate to: "+tokens(15))
        associate(tokens(0), normalize(tokens(15))) // name is associated with 15th token
      }
    }
    else if (kind == "diminish_srw_ss") {
      // nothing
    }
    else println("Analyze not implemented: "+kind)
  }

  // hide the orphans
  private def hideOrphans = {
    for (name <- map.keys if (map(name).tick == 0)) map(name).tick = -1
  }

  // reset ticks
  private def resetTicks = {
    for (name <- map.keys if (map(name).tick > 0)) map(name).tick = 0
  }

  // mark a name, recursively, return total number of lines
  private def mark(name: String): Int = {
    var lines = 0
    if (map.contains(name)) {
      // if this name was a hidden orphan, it cannot be now
      if (map(name).tick == -1) map(name).tick = 0
      map(name).tick += 1
      lines += map(name).lines
      if (map(name).kind == "thm")
        for (item <- map(name).link) lines += mark(item)
    }
    return lines
  }

  // theorems database, for marking
  private var thms = List[String]()

  // check if there are theorems to mark
  def hasMark = !(thms isEmpty)

  // put theorem to be marked
  def toMark(thm: String) = {
    thms = thm::thms
    println("Will mark: "+thm)
  }

  // show those marked/unmarked by theorems
  def showAnalysis = {
    hideOrphans // once only
    resetTicks
    for (thm <- thms) {
      if (debug) println("Checking: "+thm)
      for (name <- map.keys.toList.sorted) {
        if (name.startsWith(thm)) {
          println("Marking: "+name)
          println("  Steps: "+mark(name))
        }
      }
    }
    println("-----------------------")
    println("Used by these theorems:")
    println("-----------------------")
    for (name <- map.keys.toList.sorted if (map(name).tick > 0)) println(name)
    println("--------------------------")
    println("Not used by these theorems:")
    println("--------------------------")
    for (name <- map.keys.toList.sorted if (map(name).tick == 0)) println(name)
    println("-----------")
    println("Never used:")
    println("-----------")
    for (name <- map.keys.toList.sorted if (map(name).tick == -1)) println(name)
    println("---------------")
    println("End of Analysis.")
  }
}

// Locator for files
object Locator {
  // paths from Holmakefile
  private var paths = List("") // default current directory

  // files done (or skip these)
  private var done = Set("HolKernel", "boolLib", "bossLib", "Parse")

  // locate a file given name
  private def locate(name: String): String = {
    if (!done.contains(name)) {
      for (path <- paths) {
        if (debug) println("Checking: ["+path+name+"]")
        val fullName = path+name+".sml"
        if ((new java.io.File(fullName)).exists) return fullName
      }
    }
    return "" // done or not found
  }

  // generate documentation
  private def holdoc(lines: Iterator[String]) {
    val tokenizer = new Tokenizer(lines)
    // Tokenizer will insert LF, but this part will count the LF
    // and remove them before passing to Analyzer
    val LF = tokenizer.LF
    var tok = ""
    var count = 0
    val max = 50
    var ok = true
    while (ok) {
      if (tok == "") tok = tokenizer.next
      if (tok == "") ok = false
      while (tok == LF) tok = tokenizer.next // skip LF
      if (debug) println(count+':'+tok)
      if (tok == "val") {
        tok = tokenizer.next
        // first tok is name
        val name = tok
        // grab the whole chunk of val first
        var s = List[String]()
        var lines = 1  // at least one line
        while (tok != "" && tok != ";" && tok != "val" && tok != "open") {
          if (tok == LF) lines += 1; else s = tok::s
          tok = tokenizer.next
        }
        // skip val () or val _
        if (name != "(" && name != "_") Analyzer.analyze(s reverse, lines)
      }
      else if (tok == "open") {
        tok = tokenizer.next
        while (tok != "" && tok != ";" && tok != "val" && tok != "open") {
          if (tok.endsWith("Theory")) tok = tok.slice(0,tok.indexOf("Theory"))+"Script"
          if (debug) println("Need to open "+tok)
          Locator.process(tok) // ignore return true/false
          tok = tokenizer.next
        }
      }
      // 'val' when no ending ';', otherwise '' to force next()
      if (tok != "val" || tok != "open") tok = "";
      count += 1
      if (debug) if (count > max) ok = false
    }
  }

  // process the file with name by first locate its pathName
  def process(name: String): Boolean = {
    val pathName = locate(name)
    if (pathName != "") {
      println("Reading: " + pathName)
      holdoc(Source.fromFile(pathName)("UTF8").getLines)
      println("Done: " + pathName)
      done += name
      return true
    }
    return false
  }

  // read config file if there is one
  def initz(name: String) {
    if ((new java.io.File(name)).exists) {
      val lines = Source.fromFile(name)("UTF8").getLines
      for (line <- lines) {
        val tokenizer = new Tokenizer(Iterator(line)) // tokenize one line
        var tok = tokenizer.next
        if (tok == "INCLUDES") {
          tok = tokenizer.next // skip =
          tok = tokenizer.next
          while (tok != "") {
            println(name+" include path: "+tok)
            if (!tok.endsWith("/")) tok += '/'
            paths = tok::paths
            tok = tokenizer.next
          }
        }
      }
    }
  }
}

// Main
if (args.length == 0) {
  Console.err.println("Usage: scala Holdoc.scala [theory]")
  Console.err.println("where [theory] is path/to/[theory]Script.sml")
  Console.err.println("or")
  Console.err.println("Usage: scala Holdoc.scala [theory1] [theory2 ...] thm:[name]")
  Console.err.println("to analyze theorems starting with [name] in [theory1], [theory2 ...]")
}
else {
  // get paths from Holmakefile
  Locator.initz("Holmakefile")
  // get theories from args
  for (theory <- args) {
    val name = theory // args are theories or thm:Theorem
    if (name.startsWith("thm:")) Analyzer.toMark(name.slice("thm:".length,name.length))
    else if (!Locator.process(name+"Script")) println("Not found: "+name)
  }
  println("=================")
  println("HOL Documentation")
  println("=================")
  // show results
  if (Analyzer hasMark) Analyzer showAnalysis else Analyzer showInfo
}