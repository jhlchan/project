import scala.collection.mutable._
import HOLTerm._

class VM {

  val stack = Stack[VMObject]()
  val dictionary = Map[Int,VMObject]()
  val assumptions = Set[HOLThm]()
  val theorems = Set[HOLThm]()

  def stack_ok(name:String)(command : => Unit) =
    try {
      command
    } catch {
      case e : java.util.NoSuchElementException =>
        throw new Error("Stack too small in " + name)
      case e1 : scala.MatchError =>
        throw new Error("Stack of wrong shape in " + name)
    }



  def pop() = stack.pop
  def absTerm() =
    stack_ok("absTerm") {
      val body = stack.pop
      val v = stack.pop
      (v, body) match {
        case (VMVar(nm, ty), VMTerm(t)) =>
          stack.push(AbsTm(nm, ty, t))
      }
    }

  def absThm () =
    stack_ok("absThm") {
      val thm = stack.pop
      val v = stack.pop
      (v, thm) match {
        case (VMVar(nm, ty), VMThm(thm)) =>
          stack.push(VMThm(HOLthm.ABS(VarTm(nm,ty), thm)))
      }
    }

  def appTerm () =
    stack_ok ("appTerm") {
      val x = stack.pop
      val f = stack.pop
      (f,x) match {
        case (VMTerm(ft), VMTerm(xt)) => stack.push(VMTerm(mk_comb(ft,xt)))
      }
    }

  def appThm () =
    stack_ok ("appThm") {
      val xy = stack.pop
      val fg = stack.pop
      (fg,xy) match {
        case (VMThm(fgth, xyth)) => stack.push(VMThm(MK_COMB(fgth, xyth)))
      }
    }
