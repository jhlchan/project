package OpenTheory.PrimitiveTypes

case class Name(components: List[String], main: String) {
  override def toString =
    components match {
      case List() => main
      case _ => components.mkString(".") + "." + main
    }
}



