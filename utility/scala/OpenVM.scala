/*
 * A Virtual Machine for OpenTheory articles.
 *
 * $ scala OpenVM.scala example1.art
 */
import scala.io.Source
import scala.collection.mutable._

object OpenVM {

  // Utility functions ----------------------------------------------

  // unquote
  def unquote(s: String): String = {
    val k = s.length
    if (k > 0 && s.charAt(0) == '"' && s.charAt(k-1) == '"') return s.substring(1,k-1)
    return s
  }

  // Construct Pair object ------------------------------------------
  class Pair(f: Object, s: Object) {
    // fields
    val first = f
    val second = s
    // representation
    override def toString = '<'+f.toString+','+s.toString+'>'
  }

  // Construct Typed object -----------------------------------------
  class Typed(t: String, v: Object) {
    // fields
    val tag = t
    val value = v
    // representation
    override def toString = t+'('+v.toString+')'
    // type checks
    def isInt = t == "int"
    def isName = t == "name"
    def isVar = t == "var"
    def isConst = t == "const"
    def isType = t == "type"
    def isTypeOp = t == "typeOp"
    def isList = t == "list"
    def isTerm = t == "term"
    def isThm = t == "thm"
    def isLambda = t == "lambda"
  }

  // Machine object -------------------------------------------------
  object Machine {
    // internal data structures
    val stack = Stack[Typed]()
    val dictionary = Map[Int,Typed]()
    val assumptions = Set[Typed]()
    val theorems = Set[Typed]()

    // show the machine
    override def toString =
      "Stack: "+stack+'\n'+
      "Dictionary: "+dictionary+'\n'+
      "Assumptions: "+assumptions+'\n'+
      "Theorems: "+theorems+'\n'

    // VM Execution -------------------------------------------------

    private def comment() {
        // skip comment
    }

    // Apply abstraction --------------------------------------------

    // apply result is a pair, for term: value, type
    private def apply(func:Typed,term:Typed):Pair = {
        // input term type must match input type of function
        // result type is output type of function
        // result value is determined by beta conversion between function and input term value
        val lambda = func.value.asInstanceOf[Pair]
        val vvar = lambda.first.asInstanceOf[Typed].value
        val body = lambda.second.asInstanceOf[Typed] // body is a term
        // todo: beta conversion
        // var result = beta(vvar,body,term)
        val result = body
        return new Pair(result,term)
    }

    // Stack only commands ------------------------------------------

    // do pop
    private def pop() {
        stack.pop // discard the object
    }

    // do quote
    private def quote(s: String) {
        stack.push(new Typed("name", unquote(s)))
    }

    // do number
    private def number(s: String) {
        stack.push(new Typed("int", s.toInt.asInstanceOf[java.lang.Integer])) // int to Object
    }

    // do nil
    private def nil() {
        stack.push(new Typed("list",List[Typed]()))
    }

    // do constant
    private def constant() {
        val name = stack.pop
        assert(name.isName)
        stack.push(new Typed("const",name.value))
    }

    // do typeOp
    private def typeOp() {
        val name = stack.pop
        assert(name.isName)
        stack.push(new Typed("typeOp",name.value))
    }

    // do opType
    private def opType() {
        val list = stack.pop
        val typeOp = stack.pop
        assert(list.isList)
        assert(typeOp.isTypeOp)
        val types = typeOp.value :: list.value.asInstanceOf[List[Typed]]
        stack.push(new Typed("type",types))
    }

    // do constTerm
    private def constTerm() {
        val typp = stack.pop
        val cons = stack.pop
        assert(typp.isType)
        assert(cons.isConst)
        stack.push(new Typed("term",new Pair(cons, typp)))
    }

    // do cons
    private def cons() {
        val tail = stack.pop
        val head = stack.pop
        assert(tail.isList)
        val list = head :: tail.value.asInstanceOf[List[Typed]] // head::tail
        stack.push(new Typed("list",list))
    }

    // do refl
    private def refl() {
        val term = stack.pop
        assert(term.isTerm)
        var list = new Typed("list",List[Typed]()) // no assumption
        var thm = new Typed("thm",new Pair(list,term))
        stack.push(thm)
    }

    // do variable
    private def variable() {
        val typp = stack.pop
        val name = stack.pop
        assert(typp.isType)
        assert(name.isName)
        stack.push(new Typed("var",new Pair(name, typp)))
    }

    // do varTerm
    private def varTerm() {
        val vvar = stack.pop
        assert(vvar.isVar)
        stack.push(new Typed("term",vvar.value)) // vvar.value is a Pair
    }

    // do varType
    private def varType() {
        val name = stack.pop
        assert(name.isName)
        stack.push(new Typed("type",List[Typed](name))) // type value is a List
    }

    // do absTerm
    private def absTerm() {
        val term = stack.pop
        val vval = stack.pop
        assert(term.isTerm)
        assert(vval.isVar)
        val lambda = new Pair(new Typed("lambda",vval),term) // \vval.term
        stack.push(new Typed("term",lambda)) // lambda is a Pair
    }

    // do absThm
    private def absThm() {
        val thm = stack.pop  // assum |- t = u
        val vval = stack.pop  // v
        assert(thm.isThm)
        assert(vval.isVar)
        // todo: assum |- \v.t = \v.u
        stack.push(thm) // for now
    }

    // do appTerm
    private def appTerm() {
        val term = stack.pop
        val func = stack.pop
        assert(term.isTerm)
        // assert(func.isLambda) -- skip for now to see what happens: example3.art seems to work
        stack.push(new Typed("term",apply(func,term)))
    }

    // do appThm
    private def appThm() {
        val thm1 = stack.pop  // assum1 |- x = y
        val thm2 = stack.pop  // assum2 |- f = g
        assert(thm1.isThm)
        assert(thm2.isThm)
        // todo: assum2 union assum1 |- fx = gy
        val thm = thm2 // for now
        stack.push(thm)
    }

    // do eqMp
    private def eqMp() {
        val thm1 = stack.pop  // assum1 |- phi
        val thm2 = stack.pop  // assum2 |- phi" = psi
        assert(thm1.isThm)
        assert(thm2.isThm)
        // todo: assum2 union assum1 |- psi
        val thm = thm2 // for now
        stack.push(thm)
    }

    // do assume
    private def assume() {
        val term = stack.pop // phi
        assert(term.isTerm)
        // thm: {phi} |- phi
        val list = List[Typed](term)
        val thm = new Typed("thm",new Pair(list,term))
        stack.push(thm)
    }

    // do defineConst
    private def defineConst() {
        val term = stack.pop
        val name = stack.pop
        assert(term.isTerm)
        assert(name.isName)
        val konst = new Typed("const",name.value)
        stack.push(konst)
        // thm: |- konst = term
        val assume = List[Typed]()
        val result = new Typed("eq", new Pair(konst, term))
        val thm = new Typed("thm",new Pair(assume,result))
        stack.push(thm)
    }

    // Dictionary related commands ----------------------------------

    // do def
    private def define() {
        val idx = stack.pop
        val obj = stack.pop
        assert(idx.isInt)
        stack.push(obj)
        val key = idx.value.asInstanceOf[Int]
        dictionary += (key -> obj) // put into map
    }

    // do ref
    private def ref() {
        val idx = stack.pop
        assert(idx.isInt)
        val key = idx.value.asInstanceOf[Int]
        stack.push(dictionary.apply(key)) // retrieve from dictionary
    }

    // do remove
    private def remove() {
        val idx = stack.pop
        assert(idx.isInt)
        val key = idx.value.asInstanceOf[Int]
        stack.push(dictionary.apply(key)) // retrieve from dictionary
        dictionary -= key                 // remove from dictionary
    }

    // Assumptions related commands ---------------------------------

    // do axiom
    private def axiom() {
        val term = stack.pop
        val list = stack.pop // assumptions
        assert(term.isTerm)
        assert(list.isList)
        val thm = new Typed("thm",new Pair(list,term))
        stack.push(thm)
        assumptions += thm // put into set
    }

    // Theorems related commands ------------------------------------

    // do thm
    private def thm() {
        val term = stack.pop
        val list = stack.pop
        val thm = stack.pop
        assert(term.isTerm)
        assert(list.isList)
        assert(thm.isThm)
        // to do:
        // thm = alphaConv(thm, list, term)
        theorems += thm // put into set
    }

    // do betaConv
    private def betaConv() {
        val term = stack.pop // (\v.t)u
        assert(term.isTerm)
        // thm: |- (\v. t) u = t[u/v])
        // todo: beta conversion
        // var result = beta(....)
        val assume = List[Typed]()
        val result = term
        val thm = new Typed("thm",new Pair(assume,result))
        stack.push(thm)
    }

    // TODO ---------------------------------------------------------

    private def defineTypeOp() {throw new Exception("Not Implemented.")}
    private def deductAntisym() {throw new Exception("Not Implemented.")}
    private def subst() {throw new Exception("Not Implemented.")}
    private def unknown() {throw new Exception("Unknown for VM.")}

    // VM Execution -------------------------------------------------

    // one machine step: execute a line
    def step(line: String) {
      println(line)
      val ch = line.charAt(0)
      if (ch == '#') comment()
      else if (ch == '"') quote(line)
      else if ("-0123456789".indexOf(ch) != -1) number(line)
      else if (line == "absTerm") absTerm()
      else if (line == "absThm") absThm()
      else if (line == "appTerm") appTerm()
      else if (line == "appThm") appThm()
      else if (line == "assume") assume()
      else if (line == "axiom") axiom()
      else if (line == "betaConv") betaConv()
      else if (line == "cons") cons()
      else if (line == "const") constant()
      else if (line == "constTerm") constTerm()
      else if (line == "deductAntisym") deductAntisym()
      else if (line == "def") define()
      else if (line == "defineConst") defineConst()
      else if (line == "defineTypeOp") defineTypeOp()
      else if (line == "eqMp") eqMp()
      else if (line == "nil") nil()
      else if (line == "opType") opType()
      else if (line == "pop") pop()
      else if (line == "ref") ref()
      else if (line == "refl") refl()
      else if (line == "remove") remove()
      else if (line == "subst") subst()
      else if (line == "thm") thm()
      else if (line == "typeOp") typeOp()
      else if (line == "var") variable()
      else if (line == "varTerm") varTerm()
      else if (line == "varType") varType()
      else unknown()
      if (ch != '#') println(this.toString())
    }
  }

  // Main
  def main(args: Array[String]) {
    if (args.length == 0)
      println("[Usage] scala OpenVM.scala [path/to/article]")
    else {
      val name = args(0)
      println("article: "+name)
      val lines = Source.fromFile(name)("UTF8").getLines.toList
      lines foreach Machine.step
      println("line(s) read: "+lines.length)
    }
  }
}
