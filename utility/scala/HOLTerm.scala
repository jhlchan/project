package OpenTheory.HOLTerm

import OpenTheory.PrimitiveTypes._

sealed abstract class Term {
  def type_of : Type
}

case class VarTm(n : String, ty : Type) extends Term {
  def type_of = ty
}

private case class ConstTm(name: Name, ty: Type) extends Term {
  // constructor is private because you should only be able to create
  // constants that the system already knows about (constants that are
  // "in the signature").
  //
  // The name of the constant needs to be tracked specially too because
  // people can change the signature by creating new definitions that "appear"
  // to reuse the
  def type_of = ty
}

private case class AppTm(rator: Term, rand: Term) extends Term {
  // this "constructor" has to be private because it's not legal to combine
  // any two terms to create an application.  Instead, you must check that the
  // types are compatible
  def type_of = dom_rng(rator.type_of)._2
    // take type of rator, which will be   domain --> range, and return
    // range
}

case class AbsTm(nm: String, ty: Type, body: Term) extends Term {
  def type_of = ty --> body.type_of
}

/*
object Term {
  def subst(t1: Term, v: Var, t2: Term) =
    // substitute t1 for v in t2....

  def mk_comb(rator: Term, rand : Term) =
    // check types are OK, then
    new AppTm(rator, rand)
}
*/
