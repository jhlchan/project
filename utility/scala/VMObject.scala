/*
 When reading an article file, the virtual machine processes values of type object,
 which has the following ML-like definition:

 datatype object =
      Num of int                    (* A number *)
   |  Name of string list * string  (* A name *)
   |  List of object list           (* A list (or tuple) of objects *)
   |  TypeOp of typeOp              (* A higher order logic type operator *)
   |  Type of type                  (* A higher order logic type *)
   |  Const of const                (* A higher order logic constant *)
   |  Var of var                    (* A higher order logic term variable *)
   |  Term of term                  (* A higher order logic term *)
   |  Thm of thm                    (* A higher order logic theorem *)

  */
:load HOLType.scala
:load HOLTerm.scala
:load HOLThm.scala

type Name = (List[String], String)

/* Define objects of Virtual Machine by pattern matching */
sealed abstract class VMObject

/* Patterns of VM Object --------------------------------------------------- */



/* Num of int : A number */
case class VMNum (n: Int) extends VMObject {
  // representation
  override def toString = "N(" + n + ")"
}

/* Name of (string list) * string :
   A name, the first part (string list) is the components prefix for module */
case class VMName(components : List[String], name : String) extends VMObject {
  // representation
  override def toString = "Name(" +
               (if (components.length > 0) components.reduceLeft(_ +'.'+ _) + "." else "") +
               name + ")"
}

/* List of object list : A list (or tuple) of objects */
case class VMList(objects: List[VMObject]) extends VMObject

/* TypeOp of typeOp : A higher order logic type operator */
case class VMTypeOp(tyop: TypeOp) extends VMObject

/* Type of type : A higher order logic type */
case class VMType(ty: Type) extends VMObject

/* Const of const : A higher order logic constant */
case class VMConst(k: Const) extends VMObject

/* Var of var : A higher order logic term variable */
case class VMVar(nm: String, ty:Type) extends VMObject

/* Term of term : A higher order logic term */
case class VMTerm(tm: Term) extends VMObject

/* Thm of thm : A higher order logic theorem */
case class VMThm(thm: Thm) extends VMObject

/* Public methods on VM Object --------------------------------------------- */

/* Name of VMObject */
def name(obj: VMObject) : Option[String] =
  obj match {
    case VMNum(n) => None
    case VMName(_, n) => Some(n)
    case VMList(_) => None
    case VMTypeOp(_) => None
    case VMType(_) => None
    case VMConst(_) => None
    case VMVar(_) => None
    case VMTerm(_) => None
    case VMThm(_) => None
  }

/* Stack of VM ------------------------------------------------------------- */

import scala.collection.mutable.Stack

val stack = Stack[VMObject]()

// push things to stack
stack.push(VMNum(5))  // a number
stack.push(VMName(List("com", "org"), "name")) // a qualified name
stack.push(VMName(List(), "name")) // a simple name
stack.push(VMList(List())) // empty list
// a list of VMObject
val x1 = List(VMNum(7), VMNum(11))
stack.push(VMList(x1)) // non-empty list
// type variables
val alpha = Type("alpha")
val beta = Type("beta")
// a type operator
val cross = TypeOp("*", List(alpha, beta))
stack.push(VMType(alpha)) // a type
stack.push(VMTypeOp(cross)) // a type operator
// a variable
val myVar = ("x", alpha)
val v1: Var = ("y", alpha)
stack.push(VMVar(myVar)) // a variable
stack.push(VMVar(v1)) // a variable
// a constant
val k1 = ("k", beta)
stack.push(VMConst(k1)) // a constant
// a variable term
val z = VarTm("z", alpha)
// a constant term
//val k = ConstTm("k", beta)
// a lambda term
val lambda = AbsTm(v1, z)
stack.push(VMTerm(z)) // a variable term
//stack.push(VMConst(k)) // a constant term
stack.push(VMTerm(lambda)) // a lambda term
// a theorem
val theorem = new Thm(Set(z), lambda)
stack.push(VMThm(theorem)) // a theorem

/*
    // do nil
    private def nil() {
        stack.push(VMList(List()))
    }
*/
