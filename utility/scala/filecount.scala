//
// filecount.scala -- count each part of a file using a mark.
//
// $ scala filecount.scala [mark] [file]
//
import scala.io.Source

// process a file name by function fun for each line (string)
def process(name: String, fun: String => Unit) = {
  println("Processing: "+name)
  // get file lines
  val lines = Source.fromFile(name)("UTF8").getLines
  for (line <- lines) fun(line)
}

// Filter class
class Filter(mark: String) {
  // private
  var count = 0
  var sum = 0
  var seen = false
  // public
  def grab(line: String) = {
    if (line.indexOf(mark) == 0) { // starts with mark
      if (seen) {
        println("Count: "+count)
        sum = sum + count
      }
      println(line)
      count = 0    // reset count
      seen = true
    }
    else {
      count = count + 1
    }
  }
  def summary {
    println("Total: "+sum)
  }
}

// main program
if (args.length < 2) {
  println("Usage: scala filecount.scala [mark] [file]")
}
else {
  val filter = new Filter(args(0))
  process(args(1), filter.grab)
  filter.summary
}
