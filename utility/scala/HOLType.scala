package OpenTheory.HOLType;
import OpenTheory.PrimitiveTypes._

object TypeNames {
  val FunctionTypeName = Name(List("Bool"), "fun")
  val BoolTypeName = Name(List("Bool"), "bool")
}

import TypeNames._


sealed abstract class HOLType {
  def -->(rng: HOLType) : HOLType
}

case class TypeOp(opnm: Name, args: List[HOLType]) extends HOLType {
  def --> (rng: HOLType) = TypeOp(FunctionTypeName, List(this, rng))
  override def toString = {
    (opnm,args) match {
      case (FunctionTypeName, List(d,r)) => "(" + d + " -> " + r + ")"
      case (BoolTypeName, _) => "bool"
      case (_, List()) => opnm.toString
      case _ => "(" + args.mkString(",") + ")" + opnm
    }
  }
}
case class Type(nm: String) extends HOLType {
  def --> (rng: HOLType) = TypeOp(FunctionTypeName, List(this, rng))
}

object HOLType {
  val alpha = Type("'a")
  val beta = Type("'b")
  val bool = TypeOp(BoolTypeName, List())
  def dom_rng (ty : HOLType) =
    ty match {
      case TypeOp(FunctionTypeName, args) => (args(0), args(1))
      case _ => throw new Error("dom_rng: type not a function type.")
    }
}

/*
  type thy_tyop  = {Thy:string,Tyop:string}
  type thy_const = {Thy:string,Name:string}
*/
